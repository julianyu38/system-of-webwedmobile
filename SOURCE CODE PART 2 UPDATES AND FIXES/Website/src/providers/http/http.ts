import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';

/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class HttpProvider {

  // public apiUrl: string = 'http://www.riveloper.com/client_portal/webwed-admin/public/api';
  public apiUrl: string = 'http://localhost:8888/public/api';
  public token: string = 'invalid_token';
  public headers: Headers;
  public options: RequestOptions;

  constructor(
    public http: Http
  ) {

  }

  refreshToken() : Promise<boolean> {
    return new Promise((resolve, reject) => {

    });
  }

  getRequest(url: string) : any {
    return this.http.get(this.apiUrl + url, this.options).map(res => res.json());
  }

  postRequest(url: string, data: any) : any {
    return this.http.post(this.apiUrl + url, data, this.options).map(res => res.json());
  }

  getEventSessionDetails(eventId: number, userId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/start/'+eventId+'/'+userId).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ reject(res); }
        },
        (err) => {
          reject(err);
        }
      );
    });
  }
}
