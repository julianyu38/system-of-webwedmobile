import { Component, ElementRef, ViewChild } from '@angular/core';
import { HttpProvider } from '../providers/http/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [HttpProvider]
})

export class AppComponent {
  apiKey: string = "45944252";
  sessionId: string = "1_MX40NTk0NDI1Mn5-MTUwNDE0NzA2NDU0OX5hb0x2ZnJ4RUxJb2lJSnlDVEpYOEVSUWp-fg"
  token: string = "T1==cGFydG5lcl9pZD00NTk0NDI1MiZzZGtfdmVyc2lvbj1kZWJ1Z2dlciZzaWc9ODJiZTEwODIyNzc3NTZkMDc2MmM1Yjg0YmE0MzlmMzkzNDZjMWI2ZDpzZXNzaW9uX2lkPTFfTVg0ME5UazBOREkxTW41LU1UVXdOREUwTnpBMk5EVTBPWDVoYjB4MlpuSjRSVXhKYjJsSlNubERWRXBZT0VWU1VXcC1mZyZjcmVhdGVfdGltZT0xNTA0MTQ3MDY0JnJvbGU9cHVibGlzaGVyJm5vbmNlPTE1MDQxNDcwNjQuNTc0NTUzODUyOTQwMyZleHBpcmVfdGltZT0xNTA2NzM5MDY0";

  event: number = 30;
  user: number = 1;

  session : any;
  publisher: any;
  layout: any;

  constructor(
    public httpProvider: HttpProvider
  ) {
      httpProvider.getEventSessionDetails(this.event, this.user).then(
        (res) => {
          console.log(res);
          this.sessionId = res.opentok.session_id;
          this.token = res.opentok.token;
          this.initializeSession();
        },
        (err) => {
          console.error(err);
        }
      );
  }


  getSubscriberElement = function(subscriber){
    return "layoutContainer";
  };

  getPublisherElement = function(){
    return "publisherContainer";
  };

  updateLayout(){
    this.layout();
  }

  initializeSession() {
    var layoutContainer = document.getElementById("layoutContainer");

    // Initialize the layout container and get a reference to the layout method
    this.layout = (<any>window).initLayoutContainer(layoutContainer, {
      maxRatio: 3/2,     // The narrowest ratio that will be used (default 2x3)
      minRatio: 9/16,      // The widest ratio that will be used (default 16x9)
      fixedRatio: false,  // If this is true then the aspect ratio of the video is maintained and minRatio and maxRatio are ignored (default false)
      bigClass: "OT_big", // The class to add to elements that should be sized bigger
      bigPercentage: 0.8,  // The maximum percentage of space the big ones should take up
      bigFixedRatio: false, // fixedRatio for the big ones
      bigMaxRatio: 3/2,     // The narrowest ratio to use for the big elements (default 2x3)
      bigMinRatio: 9/16,     // The widest ratio to use for the big elements (default 16x9)
      bigFirst: true        // Whether to place the big one in the top left (true) or bottom right
    }).layout;

    (<any>window).handleError = function(error){
      if (error){
        console.error(error.message);
      }
    }

    var session = (<any>window).OT.initSession(this.apiKey, this.sessionId);
    var self = this;


    this.session = session;

    this.session.on('connectionCreated', function(event){ self.connectionCreated(event); });
    this.session.on('connectionDestroyed', function(event){ self.connectionDestroyed(event); });
    this.session.on('streamCreated', function(event){ self.streamCreated(event); });
    this.session.on('streamDestroyed', function(event){ self.streamDestroyed(event); });
    this.session.on('signal:eventCompleted', function(event){ self.eventCompleted(event); });
    this.session.on('signal:publish', function(event){ self.eventPlayed(event); });
    this.session.on('signal:unpublish', function(event){ self.eventPaused(event); });


    session.connect(this.token, function(error) {
      if (error) {
        (<any>window).handleError(error);
      }
    });

    var resizeTimeout;
    (<any>window).onresize = function() {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(function () {
        self.layout();
      }, 20);
    };
  }

  connectionCreated = function(event){
    console.log('Connection Created');
  }

  connectionDestroyed = function(event){
    console.log('Connection Destroyed');
  }

  streamCreated = function(event){
    console.log('streamCreated');
    var div = this.getSubscriberElement(event);
    var subscriber = this.session.subscribe( event.stream, div, {subscribeToAudio: true, insertMode: 'append'} );
    this.updateLayout();
  }

  streamDestroyed = function(event){
    console.log('streamDestroyed');
  }

  eventCompleted = function(event){
    console.log('eventCompleted');
  }

  eventPaused = function(event) {
    console.log('eventPaused');
  }

  eventPlayed = function(event) {
    console.log('eventPlayed');
  }

}
