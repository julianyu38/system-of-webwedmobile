import { NgModule } from '@angular/core';
import { EventresponseDirective } from './eventresponse/eventresponse';
@NgModule({
	declarations: [EventresponseDirective],
	imports: [],
	exports: [EventresponseDirective]
})
export class DirectivesModule {}
