import { Directive } from '@angular/core';
import { StorageProvider } from '../../providers/storage/storage';
import { HttpProvider } from '../../providers/http/http';
/**
 * Generated class for the EventresponseDirective directive.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/DirectiveMetadata-class.html
 * for more info on Angular Directives.
 */
@Directive({
  selector: '[eventresponse]', // Attribute selector
  providers: [StorageProvider, HttpProvider]
})
export class EventresponseDirective {

  constructor(
    public httpProvider: HttpProvider,
    public storageProvider: StorageProvider
  ) {
    console.log('Hello EventresponseDirective Directive');
  }

}
