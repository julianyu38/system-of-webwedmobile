import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { OfficiantDetailsPage } from '../officiant-details/officiant-details';
import { HttpProvider } from '../../providers/http/http';
import { UserData } from '../../providers/storage/storage';

/**
 * Generated class for the PreApprovedOfficiantPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pre-approved-officiant',
  templateUrl: 'pre-approved-officiant.html',
  providers: [HttpProvider]
})
export class PreApprovedOfficiantPage {

  officiants: UserData[];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public httpProvider: HttpProvider,
    public loadingCtrl: LoadingController) {

      let loading = this.createLoading();
      loading.present();
      httpProvider.getOfficiants().then(
        (officiants) => {
          console.log(officiants);
          this.officiants = officiants;
          loading.dismiss();
        },
        (err) => {
          console.error(err);
        }
      );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreApprovedOfficiantPage');
  }

  ngAfterViewInit() {

  }
  /*officiantSelected(){
    this.navCtrl.push('OfficiantDetailsPage');
  }*/
  officiantSelectedModal() {
    let modal = this.modalCtrl.create(OfficiantDetailsPage);
    modal.present();
  }

  showOfficiantDetails(officiant){
    let modal = this.modalCtrl.create(OfficiantDetailsPage, {
      officiant: officiant
    });
    modal.onDidDismiss(data => {
      console.log(data);
      if (data){
        if (data.invite){
          //invite officiant
          // let loading = this.createLoading();
          // loading.present();
          // this.httpProvider.inviteOfficiant(data.event, data.officiant).then(
          //   (res) => {
          //     loading.dismiss();
          //     this.navCtrl.pop();
          //   },
          //   (err) => {
          //     loading.dismiss();
          //   }
          // );

          this.viewCtrl.dismiss(data);
        }else{
          //do not invite
        }
      }
    });
    modal.present();
  }

  selectOfficiant(index){

  }

  getActiveClass(index){
    return 'bright-bg';
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  dismiss(){
    this.viewCtrl.dismiss(null);
  }

}
