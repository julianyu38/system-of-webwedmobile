import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreApprovedOfficiantPage } from './pre-approved-officiant';

@NgModule({
  declarations: [
    PreApprovedOfficiantPage,
  ],
  imports: [
    IonicPageModule.forChild(PreApprovedOfficiantPage),
  ],
})
export class PreApprovedOfficiantPageModule {}
