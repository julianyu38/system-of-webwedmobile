import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

/**
 * Generated class for the SelectCourtPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-court',
  templateUrl: 'select-court.html',
  providers: [HttpProvider]
})
export class SelectCourtPage {

  eventId: number = 0;
  courts: any = [{
    id: 0,
    name: '',
    address: '',
    disabled: false
  }];

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {

      this.eventId = navParams.get('eventId');

      this.refreshCourts();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectCourtPage');
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  selectCourt(court){
    let loading = this.createLoading();
    loading.present();
    if (!court.disabled){
      this.httpProvider.selectCourt(this.eventId, court.id).then(
        (success) => {
          loading.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Court Selected',
            subTitle: 'Court has been selected for this event.',
            buttons: ['Dismiss']
          });
          alert.present();
          this.dismiss();
        }
      )
    }else{
      let alert = this.alertCtrl.create({
        title: 'Court Disabled',
        subTitle: 'This court is not available for this event.',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }

  refreshCourts() {
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getCourts(this.eventId).then(
      (courts) => {
        loading.dismiss();
        this.courts = courts.courts;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

}
