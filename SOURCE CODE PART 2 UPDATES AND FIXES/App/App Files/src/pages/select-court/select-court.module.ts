import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectCourtPage } from './select-court';

@NgModule({
  declarations: [
    SelectCourtPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectCourtPage),
  ],
})
export class SelectCourtPageModule {}
