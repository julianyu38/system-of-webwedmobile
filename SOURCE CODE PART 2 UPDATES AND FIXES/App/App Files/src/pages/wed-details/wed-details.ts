import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, LoadingController, AlertController } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular';

import { CreateAnInvitationPage } from '../create-an-invitation/create-an-invitation';
import { PreApprovedOfficiantPage } from '../pre-approved-officiant/pre-approved-officiant';
import { OfficiantDetailsPage } from '../officiant-details/officiant-details';
import { HttpProvider } from '../../providers/http/http';
import { StorageProvider, UserData, EventData, EventDetailData, ContactData } from '../../providers/storage/storage';
import { PickContactPage } from '../pick-contact/pick-contact';
import { AddManualModalPage } from '../add-manual-modal/add-manual-modal';
import { RolePopoverPage } from '../role-popover/role-popover';
import { MarriageLicensePage } from '../marriage-license/marriage-license';
import { BroadcastPage } from '../broadcast/broadcast';
import { EmailComposer } from '@ionic-native/email-composer';
import { MarriageApplicationPage } from '../marriage-application/marriage-application';
import { MarriageEducationPage } from '../marriage-education/marriage-education';
import { RegistryLinkPage } from '../registry-link/registry-link';
import { MyAccountPage } from '../my-account/my-account';
/**
 * Generated class for the WedDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-wed-details',
  templateUrl: 'wed-details.html',
  providers: [HttpProvider, StorageProvider, EmailComposer]
})
export class WedDetailsPage {

  event: EventDetailData;
  wed_details: String = 'details';
  participants: any[] = [];
  guests: ContactData[] = [];
  privacy = false;

  loadProgress: number = 0;
  State = {
    ACTIVE: 1,
    DISABLE: 2
  }
  currentState : any = 2;
  max = 100;

  currentCountDownState : any;
  maxCountDown = 100;
  countdownRemaining: number = 100;
  countdownColor = '#d6d8db';
  countdownBackgroundURL = 'assets/images/heart_active.png';
  public showCountDown: boolean = true;
  public countDownPaused: boolean = false;

  public showProgress: boolean = true;

  showLoader(){
    this.showProgress = true;
    this.loadProgress = 0;
    if(this.currentState == this.State.ACTIVE){
      var refreshIntervalId = setInterval(() => {
        if(this.loadProgress < this.max){
          this.loadProgress++;
        }else{
          this.showProgress = false;
          clearInterval(refreshIntervalId);
        }
      }, 1000);
    }
  }

  public user: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,
    public httpProvider: HttpProvider,
    private loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    private emailComposer: EmailComposer,
    public alertCtrl: AlertController
  ) {

    this.event = this.navParams.get('event');
    console.log(this.event);
    this.refreshParticipants();
    this.refreshGuests();

    this.user = this.navParams.get('user');

    // this.httpProvider.isPublic(this.event.details.id).then(
    //   (res) => {
    //     this.privacy = res;
    //   },
    //   (err) => {
    //
    //   }
    // );

    // if (this.event.details.status == 0){
    //   this.currentCountDownState = this.State.ACTIVE;
    // }else{
    //   this.currentCountDownState = this.State.DISABLE;
    //   this.countdownColor = '#00bbd3';
    //   this.countdownBackgroundURL="../http://ec2-34-228-21-16.compute-1.amazonaws.com/assets/images/heart_disable.png";
    // }
  }

  refreshParticipants() {
    this.participants = [];

    // console.log(this.event.participants);

    for (var i = 0; i < 5; i++){
      if (this.event.participants[i]){
        if (this.event.participants[i].role == 'creator') { continue; }
        var avatar = '';
        if (typeof(this.event.users[this.event.participants[i].user].personal.avatar) != 'undefined'){
          avatar = this.event.users[this.event.participants[i].user].personal.avatar;
        }else{
          avatar = this.httpProvider.apiUrl + '/user/'+this.event.users[this.event.participants[i].user].personal.id+'/avatar';
        }
        this.participants = this.participants.concat({
          empty: false,
          avatar: avatar,
          name: this.event.users[this.event.participants[i].user].personal.name.full,
          status: this.event.participants[i].status,
          role: (this.event.participants[i].role == 'unknown' ? 'Select a Role' : this.event.participants[i].role)
        });
      }else{
        this.participants = this.participants.concat({
          empty: true,
          avatar: 'add',
          name: 'Add User',
          status: '',
          role: 'Select a Role'
        });
      }
    }

    // console.log(this.participants);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad WedDetailsPage');
  }

  createInvite(){
    if (this.currentCountDownState == 2){
      return;
    }
    this.navCtrl.push(CreateAnInvitationPage, {
      event: this.event
    });
  }

  showParticipantActionSheet(index) {
    if(this.currentCountDownState == this.State.DISABLE){
      return;
    }
    this.showActionSheet(true, false).then(
      (users) => {
        this.handleContactsToImportAsParticipant(users['contacts'], index);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  showGuestActionSheet(){
    if (this.currentCountDownState == 2){
      return;
    }
    this.showActionSheet(true, true).then(
      (users) => {
        this.handleContactsToImportAsGuests(users['contacts']);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  selected_tab: number = 0;

  list_tabs(event){
    console.log(event);
    this.selected_tab = event;
  }

  actionSheet: any;

  showActionSheet(single, guests) {
    return new Promise((resolve, reject) => {
      var self = this;

      let buttons = [];

      buttons = buttons.concat({
        text: 'Add manually',
        role: 'manually',
        handler: () => {
          this.addManually().then(
            (contacts) => {
              resolve(contacts);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });

      buttons = buttons.concat({
        text: 'Add from Contacts',
        role: 'contact',
        handler: () => {
          console.log('Contacts clicked');
          if (single){
            this.pickFromContacts(1).then(
              (contacts) => {
                resolve(contacts);
              },
              (err) => {
                reject(err);
              }
            );
          }else{
            this.pickFromContacts(-1).then(
              (contacts) => {
                resolve(contacts);
              },
              (err) => {
                reject(err);
              }
            );
          }
        }
      });

      if (!guests){
        buttons = buttons.concat({
          text: 'Pre-Approved Officiants',
          icon:'star',
          role: 'pre-approve',
          handler: () => {
            console.log('Pre-approve clicked');
            // this.navCtrl.push(PreApprovedOfficiantPage);
            let modal = this.modalCtrl.create(PreApprovedOfficiantPage, { });
            modal.onDidDismiss(data => {
              if (data == null){
                return;
              }
              let loading = this.createLoading();
              loading.present();
              //console.log(data);
              this.httpProvider.addParticipant(this.event.details.id, {
                name: '',
                user: data.officiant
              }, -1).then(
                (res) => {
                  this.event = res;
                  this.refreshParticipants();
                  loading.dismiss();
                },
                (err) => {
                  console.error(err);
                  loading.dismiss();
                }
              )
            });
            modal.present();
          }
        });
      }

      this.actionSheet = this.actionSheetCtrl.create({
        title: '',
        cssClass: 'action-sheets-basic-page',
        buttons: buttons
      });
      this.actionSheet.present();
    });
  }

  clearParticipant(index) {
    var self = this;
    // setTimeout(function(){
    //   self.actionSheet.dismiss();
    // }, 500);
    // this.actionSheet.dismiss();
    this.httpProvider.removeParticipant(this.event.details.id, index).then(
      (res) => {
        this.actionSheet.dismiss();
        this.event = res;
        this.refreshParticipants();
      },
      (err) => {
        this.actionSheet.dismiss();
      }
    );
  }

  importContacts() {
    if(this.currentCountDownState == this.State.DISABLE){
      return;
    }

    this.pickFromContacts(-1).then(
      (contacts) => {
        this.handleContactsToImportAsGuests(contacts['contacts']);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  handleContactsToImportAsGuests(contacts: ContactData[]){
    let tmpContacts = [];
    let contactsLeft = contacts.length;

    while (contactsLeft != 0){
      //@TODO implement catch system for preventing additional invite to the same contact.
      for (var i = 0; i < this.guests.length; i++){
        if (this.guests[i].email == contacts[contactsLeft-1].email && this.guests[i].phone == contacts[contactsLeft-1].phone){
          contactsLeft -= 1;
          break;
        }
      }
      tmpContacts = tmpContacts.concat(contacts[contactsLeft-1]);
      contactsLeft -= 1;
      continue;
    }

    for (var i = 0; i < tmpContacts.length; i++){
      this.guests = this.guests.concat(tmpContacts[i]);
    }

    this.updateGuests();
  }

  updateGuests() {
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.updateGuestList(this.event.details.id, this.guests).then(
      (res) => {
          this.event = res;
          this.refreshGuests();
          loading.dismiss();
      },
      (err) => {
          console.error(err);
          this.refreshGuests();
          loading.dismiss();
      }
    );
  }

  refreshGuests() {
    this.guests = [];
    this.guests = this.event.guest;
  }

  handleContactsToImportAsParticipant(contacts: ContactData[], index: number){
    if (contacts.length > 0){
      let contact = contacts[0];
      let loading = this.createLoading();
      loading.present();
      this.httpProvider.addParticipant(this.event.details.id, contact, index).then(
        (res) => {
          this.event = res;
          this.refreshParticipants();
          loading.dismiss();
        },
        (err) => {
          console.error(err);
        }
      )
    }
  }

  doRefresh(refresher) {
    this.httpProvider.getEventDetailsById(this.event.details.id).then(
      (res) => {
        this.event = res;
        this.refreshParticipants();
        this.refreshGuests();
        refresher.complete();
      },
      (err) => {
          console.error(err);
      }
    );
  }

  pickFromContacts(limit) : Promise<any> {
    return new Promise((resolve, reject) => {
      let contactModal = this.modalCtrl.create(PickContactPage, {
        limit: limit
      });
      contactModal.onDidDismiss(data => {
        console.log(data);
        if (data){
          resolve(data);
        }
      });
      contactModal.present();

    });
  }

  addManually() : Promise<any> {
    return new Promise((resolve, reject) => {
      let manualModal = this.modalCtrl.create(AddManualModalPage);
      manualModal.onDidDismiss(data => {
        console.log(data);
        if (data){
          resolve(data);
        }
      });
      manualModal.present();

    });
  }

  handle(){
    let i = (<any>this).i;
    let roles = (<any>this).roles;
    let participantId = (<any>this).participantId;

    console.log(i);
    console.log(roles);
    let loading = (<any>this).self.createLoading();
    loading.present();
    if (typeof(roles) != 'undefined'){
      if (roles == null){
        loading.dismiss();
        //console.log('null');
        return;
      }
      if (roles[i] == null){
        loading.dismiss();
        return;
      }
      (<any>this).self.httpProvider.updateParticipant((<any>this).self.event.details.id, participantId, roles[i].id).then(
        (res) => {
          loading.dismiss();
          (<any>this).self.event = res;
          (<any>this).self.refreshParticipants();
          console.log('updated');
        },
        (err) => {
          console.log('error');
          loading.dismiss();
        }
      );
    }else{
      loading.dismiss();
    }
  }

  selectRole(participantId) {
    if (this.participants[participantId].empty){
      return;
    }

    let loading = this.createLoading();
    loading.present();

    this.httpProvider.getEventConfig().then(
      (res) => {
        console.log(res);
        loading.dismiss();
        var config = res;
        var roles = config.roles;

        var buttons = [];

        var self = this;

        // let updateParticipant = self.httpProvider.updateParticipant.bind(self);
        // self.handle.bind(updateParticipant, self.event.details.id, participantId, roles,i)

        for (var i = 0; i < roles.length; i++){
           let handler = {
            self: self,
            participantId: participantId,
            roles: roles,
            i: i
          };

          buttons = buttons.concat({
            text: roles[i].name,
            icon:'',
            handler: self.handle.bind(handler)
          });
        }

        this.actionSheet = this.actionSheetCtrl.create({
          title: '',
          cssClass: 'action-sheets-basic-page',
          buttons: buttons
        });
        this.actionSheet.present();
      },
      (err) => {

      }
    );
  }

  viewLicense() {
    this.navCtrl.push(MarriageLicensePage);
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }


  startCountDown(){
    this.showCountDown = true;
    this.countdownRemaining = this.maxCountDown;
    this.currentCountDownState == this.State.ACTIVE;
    this.countDownPaused = false;
    this.doCountDown();
  }

  pauseCountDown(){
    this.showCountDown = true;
    this.countDownPaused = true;
  }
  resumeCountDown(){
    this.showCountDown = true;
    this.countDownPaused = false;
    this.doCountDown();
  }

  doCountDown(){
    if(this.currentCountDownState == this.State.ACTIVE){
      var refreshCountdownIntervalId = setInterval(() => {
        if(this.countdownRemaining > 0){
          this.countdownRemaining--;
          if (this.countdownRemaining < 0.25*this.maxCountDown) {
            // red color
            this.countdownColor = '#ce4a30';
            this.countdownBackgroundURL="assets/images/heart_broadcast.png";
          } else if (this.countdownRemaining < 0.5*this.maxCountDown) {
            // amber color
            this.countdownColor = '#f9ac00';
            this.countdownBackgroundURL="assets/images/heart_active.png";
          } else {
            // normal color
            this.countdownColor = '#00bbd3';
            this.countdownBackgroundURL="assets/images/heart_disable.png";
          }

          if (this.countDownPaused == true) {
            clearInterval(refreshCountdownIntervalId);
          }

        }else{
          // this.showCountDown = false;
          clearInterval(refreshCountdownIntervalId);
        }
      }, 50);
    }
  }

  countdownClicked(){
    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      message: 'Are you ready to begin this event? This will count against your time alotment, and can not be reset.',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            if (this.currentCountDownState == 2){
              return;
            }
            this.httpProvider.getEventSessionDetails(this.event.details.id, this.user).then(
              (res) => {
                console.log(res);
                //this.sessionId = res.opentok.session_id;
                // this.sessionId =
                //this.token = res.opentok.token;

              },
              (err) => {
                console.error(err);
              }
            );
            // (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', []);
            // alert('OPENTOK SERVICE NOT AVAILABLE.');
            this.navCtrl.setRoot(BroadcastPage, {
              guest: false,
              event: this.event.details.id,
              user: this.user
            });
            // if(this.countdownRemaining == this.maxCountDown) {
            //   // not started yet, please start
            //   this.startCountDown();
            //   return;
            // } else if (this.countDownPaused == false) {
            //   this.pauseCountDown();
            //   return;
            // } else {
            //   this.resumeCountDown();
            //   return;
            // }
          }
        }
      ]
    });
    alert.present();
  }

  getHelp(){
   (<any>window).cordova.plugins.email.open({
         app: 'mailto',
         to: 'info@webwedmobile.com',
         subject: 'WebWed Mobile - App Support',
         body: 'I am having technical issues with the WebWed Mobile app.  Please get back to me at when you can. My contact information is listed below:'
       });


  }

  wait : boolean = false;

  privacyUpdate(item){
    var self = this;
    if (this.wait){ return; }
    console.log(this.privacy);
    this.httpProvider.updatePublic(this.event.details.id, this.privacy).then(
      (res) => {
        this.privacy = res;
        this.wait = true;
        setTimeout(function(){
          self.wait = false;
        }, 1500);
      },
      (err) => {

      }
    );
  }

  gotoMarriageRegistry() {
    this.navCtrl.push(RegistryLinkPage, {
      id: this.event.details.id,
      user: this.user
    });
  }

  gotoMarriageEducation() {
    this.navCtrl.push(MarriageEducationPage, {
      id: this.event.details.id,
      user: this.user
    });
  }

  gotoMarriageLicense() {
    this.navCtrl.push(MarriageLicensePage, {
      id: this.event.details.id,
      user: this.user
    });
  }

  gotoMarriageApplication() {
    this.navCtrl.push(MarriageApplicationPage, {
      id: this.event.details.id,
      user: this.user
    });
  }

  gotoOrderDetails() {
    this.navCtrl.setRoot(MyAccountPage, {
      order: true
    });
  }

}
