import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WedDetailsPage } from './wed-details';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    WedDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(WedDetailsPage),
    ComponentsModule
  ],
})
export class WedDetailsPageModule {}
