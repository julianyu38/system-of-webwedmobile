import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickContactPage } from './pick-contact';

@NgModule({
  declarations: [
    PickContactPage,
  ],
  imports: [
    IonicPageModule.forChild(PickContactPage),
  ],
})
export class PickContactPageModule {}
