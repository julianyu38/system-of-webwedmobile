import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BroadcastPage } from './broadcast';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BroadcastPage,
  ],
  imports: [
    IonicPageModule.forChild(BroadcastPage),
    ComponentsModule
  ],
})
export class BroadcastPageModule {}
