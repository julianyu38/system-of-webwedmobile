import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfficiantDetailsPage } from './officiant-details';

@NgModule({
  declarations: [
    OfficiantDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(OfficiantDetailsPage),
  ],
})
export class OfficiantDetailsPageModule {}
