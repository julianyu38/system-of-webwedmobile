import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController  } from 'ionic-angular';
import { UserData } from '../../providers/storage/storage';
import { HttpProvider } from '../../providers/http/http';

/**
 * Generated class for the OfficiantDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-officiant-details',
  templateUrl: 'officiant-details.html',
  providers: [HttpProvider]
})
export class OfficiantDetailsPage {

  officiant: UserData;
  event: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public httpProvider: HttpProvider) {

      this.event = this.navParams.get('event');
      this.officiant = this.navParams.get('officiant');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OfficiantDetailsPage');
  }

   dismiss() {
    this.viewCtrl.dismiss({
      invite: false
    });
  }

  invite() {
    // let loading = this.createLoading();
    // loading.present();
    // this.httpProvider.inviteOfficiant(this.event, this.officiant.personal.id).then(
    //   (res) => {
    //     loading.dismiss();
        this.viewCtrl.dismiss({
          invite: true,
          officiant: this.officiant.personal.id,
          event: this.event
        });
      // },
      // (err) => {
      //   loading.dismiss();
      // }
    // );
  }

}
