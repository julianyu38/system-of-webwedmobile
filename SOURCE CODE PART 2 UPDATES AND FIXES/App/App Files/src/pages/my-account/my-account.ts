import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { OrderDetailsPage } from '../order-details/order-details';
import { UserData, OrderData, StorageProvider } from '../../providers/storage/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
  providers: [HttpProvider, StorageProvider, Camera, InAppBrowser]
})
export class MyAccountPage {
  selected_tab: number = 0;
  location: String = '';
  name: String = '';
  email: String = '';
  birthday: String = '';
  phone: number = 0;
  avatar: String = '';
  notifications_push: boolean = false;
  notifications_email: boolean = false;

  password: string = '';
  confirm_password: string = '';


  user: UserData;

  orders: [OrderData];
  card: {
    number: string;
    name: string;
    expires: string;
    ccv: string;
    zip: string;
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storageProvider: StorageProvider,
    private httpProvider: HttpProvider,
    private camera: Camera,
    private loadingCtrl: LoadingController,
    private iab: InAppBrowser
  ) {
    let loading = this.createLoading();
    loading.present();

    storageProvider.getProfile().then(profile => {
      this.user = profile;
      console.log(this.user);

      this.avatar = this.httpProvider.apiUrl + '/user/'+this.user.personal.id+'/avatar';
      let dob = new Date(this.user.personal.dob*1000);
      this.birthday = dob.getMonth() + '/' + dob.getDate() + '/' + dob.getFullYear();
      this.name = this.user.personal.name.full;
      this.email = this.user.personal.email;
      this.location = this.user.personal.location.city + ', ' + this.user.personal.location.state;
      this.phone = this.user.personal.phone;
      this.notifications_push = this.user.personal.privacy.notifications.push;
      this.notifications_email = this.user.personal.privacy.notifications.email;

      let user_id = this.user.personal.id;
      this.httpProvider.getUserOrders(user_id).then(
        (res) => {
          this.orders = res;
          if (this.navParams.get('order')){
            this.selected_tab = 1;
          }
          // this.httpProvider.getPaymentInfo().then(
          //   (card) => {
          //     this.card = card;
          //     loading.dismiss();
          //   },
          //   (err) => {
          //     loading.dismiss();
          //   }
          // );
          loading.dismiss();
        },
        (err) => {
          console.error(err);
          loading.dismiss();
        }
      )
    }, err => {
      console.error(err);
      loading.dismiss();
    });
  }

  gotoOrder(order) {
    this.navCtrl.push(OrderDetailsPage, {
      order: order
    });
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }


  list_tabs(event){
    console.log(event);
    this.selected_tab = event;
  }

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: 0
  }

  updateAvatar(){
    let image = '';
    var self = this;
    this.camera.getPicture(this.options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     image=base64Image;
     let loading = this.createLoading();
     loading.present();
     self.httpProvider.updateAvatar(image).then(
       (status) => {
         self.avatar = image;
         loading.dismiss();
       },
       (err) => {
         loading.dismiss();
         console.error(err);
       }
     );
    }, (err) => {
     // Handle error
     console.error(err);
    });
  }

  updateProfile(){
    let loading = this.createLoading();
    loading.present();
    var data = {
      push_notifications: this.notifications_push,
      email_notifications: this.notifications_email,
      phone: this.phone
    };

    var self = this;
    // self.httpProvider.updateAccount(data);
    self.httpProvider.updateAccount(data).then(
      (user) => {
        self.storageProvider.setProfile(user).then((res) => {  });
        self.user = user;

        if (self.password != '' && self.password == self.confirm_password){
          //update password to firebase
        }

        loading.dismiss();
      },
      (err) => {
        console.error(err);
      }
    );
  }

  storePaymentMethod(){

  }

  termsOfService(){
    const browser = this.iab.create('https://webwedmobile.com/legal');
  }

}
