import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

/**
 * Generated class for the MarriageEducationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-marriage-education',
  templateUrl: 'marriage-education.html',
  providers: [HttpProvider]
})
export class MarriageEducationPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarriageEducationPage');
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  completedTraining(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.submitMarriageEducation(this.navParams.get('id')).then(
      (education) => {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Certificate Issued',
          subTitle: 'We have emailed you a copy of your certificate. This certificate is valid only with WebWed Mobile.',
          buttons: ['Dismiss']
        });
        alert.present();
      },
      (err) => {
        loading.dismiss();
      }
    );
    // this.httpProvider.getMarriageApplication(this.navParams.get('id')).then(
    //   (application) => {
    //     loading.dismiss();
    //     console.log(application);
    //     this.application = application;
    //     this.submitted = application.submitted;
    //     this.your_information = application.your_information;
    //     this.spouce_information = application.spouce_information;
    //     this.officiant_information = application.officiant_information;
    //     // this.attachments = application.attachments;
    //   },
    //   (err) => {
    //     loading.dismiss();
    //   }
    // );
  }

}
