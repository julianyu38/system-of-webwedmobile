import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { SelectCourtPage } from '../select-court/select-court';
import { FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

/**
 * Generated class for the MarriageApplicationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-marriage-application',
  templateUrl: 'marriage-application.html',
  providers: [HttpProvider, Camera, FileTransfer, File]
})
export class MarriageApplicationPage {

  application: any;
  court: any = {
    disabled: true,
    court: {
      id: 0,
      name: 'Select a Court'
    }
  }
  submitted: any = false;
  attachments: any = [];
  your_information: any = {
    user: {
      personal: {
        id: 0,
        name: {
          first: '',
          last: '',
          full: ''
        },
        email: '',
        avatar: ''
      }
    },
    attachments: []
  };
  spouce_information: any = {
    user: {
      personal: {
        id: 0,
        name: {
          first: '',
          last: '',
          full: ''
        },
        email: '',
        avatar: ''
      }
    },
    attachments: []
  };
  officiant_information: any = {
    user: {
      personal: {
        id: 0,
        name: {
          first: '',
          last: '',
          full: ''
        },
        email: '',
        avatar: ''
      }
    }
  };

  options: CameraOptions = {
    quality: 80,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: 0
  }

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public loadingCtrl: LoadingController,
    private camera: Camera,
    public modalCtrl: ModalController,
    private transfer: FileTransfer) {
    this.refreshApplication();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarriageApplicationPage');
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  private refreshApplication(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getMarriageApplication(this.navParams.get('id')).then(
      (application) => {
        loading.dismiss();
        console.log(application);
        this.application = application;
        this.court = application.court;
        this.submitted = application.submitted;
        this.your_information = application.your_information;
        this.spouce_information = application.spouce_information;
        this.officiant_information = application.officiant_information;
        // this.attachments = application.attachments;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  private addAttachment(person) {
    let loading = this.createLoading();
    let image = '';
    this.camera.getPicture(this.options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:

     loading.present();
     let options: FileUploadOptions = {
       fileKey: 'picture'
     };
     let fileTransfer = this.transfer.create();
     fileTransfer.upload(imageData, 'https://webwedmobile.net/api/upload/file', options)
      .then((response) => {
        var data = JSON.parse(response.response)['data'];
        var path = data['path'];

        this.httpProvider.addAttachment(this.navParams.get('id'), ('https://webwedmobile.net/storage/' + (path)), person).then(
          (application) => {
            loading.dismiss();
            console.log(application);
            this.application = application;
            this.submitted = application.submitted;
            this.attachments = application.attachments;
            this.refreshApplication();
          },
          (err) => {
            loading.dismiss();
          }
        );
      }, (err) => {
        loading.dismiss();
      });


    }, (err) => {
     // Handle error
     // alert(JSON.stringify(err));
     console.error(err);
    });
  }

  // private removeAttachment(id){
  //   let loading = this.createLoading();
  //   loading.present();
  //   this.httpProvider.removeAttachment(this.navParams.get('id'), id).then(
  //     (application) => {
  //       loading.dismiss();
  //       console.log(application);
  //       this.application = application;
  //       this.submitted = application.submitted;
  //       this.attachments = application.attachments;
  //     },
  //     (err) => {
  //       loading.dismiss();
  //     }
  //   );
  // }

  private submit(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.submitMarriageApplication(this.navParams.get('id')).then(
      (application) => {
        loading.dismiss();
        console.log(application);
        this.application = application;
        this.submitted = application.submitted;
        this.attachments = application.attachments;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  isSelectingCourt: boolean = false;
  private selectCourt(){
    // return;
    if (this.isSelectingCourt){ return; }else{ this.isSelectingCourt = true; }
    let modal = this.modalCtrl.create(SelectCourtPage, {
      eventId: this.navParams.get('id')
    });
    modal.onDidDismiss(data => {
      this.refreshApplication();
      this.isSelectingCourt = false;
    });
    modal.present();
  }

  public close(){
    this.navCtrl.pop();
  }

}
