import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarriageApplicationPage } from './marriage-application';

@NgModule({
  declarations: [
    MarriageApplicationPage,
  ],
  imports: [
    IonicPageModule.forChild(MarriageApplicationPage),
  ],
})
export class MarriageApplicationPageModule {}
