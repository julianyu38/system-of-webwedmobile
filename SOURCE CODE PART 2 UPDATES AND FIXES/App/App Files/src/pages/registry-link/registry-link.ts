import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';

@Component({
  selector: 'page-registry-link',
  templateUrl: 'registry-link.html',
  providers: [HttpProvider]
})
export class RegistryLinkPage {

  registry_link: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {
      this.getRegistryLink();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarriageEducationPage');
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  getRegistryLink(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getRegistryLink(this.navParams.get('id')).then(
      (registry) => {
        console.log(registry);
        loading.dismiss();
        this.registry_link = registry.registry_link;
      },
      (err) => {
        console.error(err);
        loading.dismiss();
      }
    );
  }

  

  save(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.saveRegistryLink(this.navParams.get('id'), this.registry_link).then(
      (registry) => {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Registry Saved',
          subTitle: 'Your registry has been saved. You can update this at anytime.',
          buttons: ['Okay']
        });
        alert.present();
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

}
