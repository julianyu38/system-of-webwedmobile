import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DashboardPage } from './dashboard';
import { CWedCardComponent } from '../../components/c-wed-card/c-wed-card';
import { CGuestCardComponent } from '../../components/c-guest-card/c-guest-card';
import { MainNavigationComponent } from '../../components/main-navigation/main-navigation';
import { HttpModule } from '@angular/http';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DashboardPage,
    CWedCardComponent,
    CGuestCardComponent
  ],
  imports: [
    IonicPageModule.forChild(DashboardPage), HttpModule, ComponentsModule
  ],
})
export class DashboardPageModule {}
