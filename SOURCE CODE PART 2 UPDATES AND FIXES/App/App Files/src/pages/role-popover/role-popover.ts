import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { EventConfigData } from '../../providers/storage/storage';

/**
 * Generated class for the RolePopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-role-popover',
  templateUrl: 'role-popover.html',
  providers: [HttpProvider]
})
export class RolePopoverPage {

  config: EventConfigData;
  roles: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController
  ) {
    let loading = this.createLoading();
    loading.present();
    httpProvider.getEventConfig().then(
      (res) => {
        console.log(res);
        loading.dismiss();
        this.config = res;
        this.roles = this.config.roles;
      },
      (err) => {

      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RolePopoverPage');
  }

  dismiss(roleId) {
    this.viewCtrl.dismiss({
      role: roleId
    });
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

}
