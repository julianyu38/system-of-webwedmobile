import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RolePopoverPage } from './role-popover';

@NgModule({
  declarations: [
    RolePopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(RolePopoverPage),
  ],
})
export class RolePopoverPageModule {}
