import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, LoadingController } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular';

import { CreateAnInvitationPage } from '../create-an-invitation/create-an-invitation';
import { PreApprovedOfficiantPage } from '../pre-approved-officiant/pre-approved-officiant';
import { OfficiantDetailsPage } from '../officiant-details/officiant-details';
import { HttpProvider } from '../../providers/http/http';
import { StorageProvider, UserData, EventData, EventDetailData, ContactData } from '../../providers/storage/storage';
import { PickContactPage } from '../pick-contact/pick-contact';
import { AddManualModalPage } from '../add-manual-modal/add-manual-modal';
import { RolePopoverPage } from '../role-popover/role-popover';
import { BroadcastPage } from '../broadcast/broadcast';
import { EmailComposer } from '@ionic-native/email-composer';
import { MarriageApplicationPage } from '../marriage-application/marriage-application';
import { MarriageEducationPage } from '../marriage-education/marriage-education';
import { RegistryLinkPage } from '../registry-link/registry-link';

@Component({
  selector: 'page-marriage-license',
  templateUrl: 'marriage-license.html',
  providers: [HttpProvider, StorageProvider, EmailComposer]
})
export class MarriageLicensePage {

  histories: any = [
    {
      title: '',
      date: ''
    }
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,
    public httpProvider: HttpProvider,
    private loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    private emailComposer: EmailComposer
  ) {
    this.refreshLicense();
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  private refreshLicense(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getMarriageLicense(this.navParams.get('id')).then(
      (license) => {
        loading.dismiss();
        console.log(license);
        this.histories = license.history;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  goHome(){
    this.navCtrl.popToRoot();
  }

}
