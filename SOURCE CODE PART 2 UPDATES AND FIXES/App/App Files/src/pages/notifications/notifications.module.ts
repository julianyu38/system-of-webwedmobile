import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificationsPage } from './notifications';
import { MainNavigationComponent } from '../../components/main-navigation/main-navigation';
import { ComponentsModule } from '../../components/components.module';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    NotificationsPage
  ],
  imports: [
    IonicPageModule.forChild(NotificationsPage), HttpModule, ComponentsModule
  ],
})
export class NotificationsPageModule {}
