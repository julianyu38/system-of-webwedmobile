import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PickEmojiPage } from './pick-emoji';

@NgModule({
  declarations: [
    PickEmojiPage,
  ],
  imports: [
    IonicPageModule.forChild(PickEmojiPage),
  ],
})
export class PickEmojiPageModule {}
