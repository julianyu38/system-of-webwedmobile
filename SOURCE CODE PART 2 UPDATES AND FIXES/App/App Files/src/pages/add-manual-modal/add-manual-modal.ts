import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactData } from '../../providers/storage/storage';
/**
 * Generated class for the AddManualModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-manual-modal',
  templateUrl: 'add-manual-modal.html',
})
export class AddManualModalPage {

  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController
  ) {

  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('',
        [Validators.required]),
      email: new FormControl('',
        []),
      phone: new FormControl('',
        [])
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddManualModalPage');
  }

  getContactData() : ContactData{
    return {
      name: this.form.controls.name.value,
      email: this.form.controls.email.value,
      phone: this.form.controls.phone.value
    };
  }

  blankDismiss() {
    this.viewCtrl.dismiss({
      'contacts':[]
    });
  }

  dismiss() {
    if (this.form.controls.email.value == '' && this.form.controls.phone.value == ''){
      //alert('Enter an email or phone number to continue.');
    }else{
     this.viewCtrl.dismiss({ 'contacts': [
       this.getContactData()
     ] });
    }
  }

}
