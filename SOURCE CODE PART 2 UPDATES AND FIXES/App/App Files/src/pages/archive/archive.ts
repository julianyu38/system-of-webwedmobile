import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { HttpProvider } from '../../providers/http/http';
import { EventCompletedPage } from '../../pages/event-completed/event-completed';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the ArchivePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-archive',
  templateUrl: 'archive.html',
  providers: [HttpProvider, ScreenOrientation, InAppBrowser]
})
export class ArchivePage {

  video_url = '';
  numberOfParticipants: number = 1;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    private screenOrientation: ScreenOrientation,
    public platform: Platform,
  private iab: InAppBrowser) {
      // if (!platform.is('MacIntel')){
      //   if (this.numberOfParticipants==1){
      //     this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
      //   }else{
      //     this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      //   }
      // }
      httpProvider.getArchive(navParams.get('event')).then(
        (res) => {
          console.log(res);
          this.video_url = res;
        },
        (err) => {
          this.close();
        }
      );
  }

  ionViewWillLeave() {
    // if (!this.platform.is('MacIntel')){
    //   this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    // }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArchivePage');
  }

  download() {
    //const browser = this.iab.create(this.video_url, "_system");
    let callback = function(a){console.log(a);};
    (<any>window).cordova.exec(callback, callback, 'InAppBrowser', 'open', [this.video_url, '_system', '']);
  }

  close(){
    this.navCtrl.setRoot(DashboardPage);
  }

  ended() {
    this.navCtrl.setRoot(DashboardPage);
  }

}
