import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { RegistryLinkPage } from '../registry-link/registry-link';
import { MarriageLicensePage } from '../marriage-license/marriage-license';
import { HttpProvider } from '../../providers/http/http';
import { PackageData, AddonData, StorageProvider, EventConfigData, EventData } from '../../providers/storage/storage';
import { ImagePicker } from '@ionic-native/image-picker';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Renderer } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CheckoutDetailsPage } from '../checkout-details/checkout-details';
import { FileTransfer, FileUploadOptions } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-create-an-event',
  templateUrl: 'create-an-event.html',
  providers: [HttpProvider, FileTransfer, File, StorageProvider, ImagePicker, Camera, InAppBrowser]
})
export class CreateAnEventPage {
  premium_features_available: any;
  config: EventConfigData;
  packages: [PackageData];
  addons: [AddonData];
  public today = new Date();

  photo: string = '';
  title: string;
  date: string;
  time: string;
  package: number = 1;
  total: number = 0;
  myDate: any;
  current_iso: string;
  current_date: string;
  current_year: string;
  yesterday_iso: string;
  tomorrow_iso: string;

  isDisabled: boolean = true;

  form: FormGroup;

  user: any;

  options: CameraOptions = {
    quality: 70,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: 0
  }


  constructor(public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private httpProvider: HttpProvider,
    private camera: Camera,
    private statusBar: StatusBar,
    public renderer: Renderer,
    private iab: InAppBrowser,
    private transfer: FileTransfer,
    private storageProvider: StorageProvider
    ) {
      // let status bar overlay webview
      this.statusBar.overlaysWebView(false);

      //White text
      this.statusBar.styleLightContent();

      // set status bar to white
      this.statusBar.backgroundColorByHexString('#5aabd3');
    // ) {
    let loading = this.createLoading();
    loading.present();

    this.httpProvider.getEventConfig().then(
      (res) => {
        console.log(res);
        this.config = res;
        this.packages = res.packages;
        this.addons = res.addons;
        for (var i = 0; i < this.addons.length; i++){
          this.form.addControl('addon'+i.toString(), new FormControl(false,[]));
        }
        this.calculateTotal();
        loading.dismiss();
      },
      (err) => {
        console.error(err);
        loading.dismiss();
      }
    );

    storageProvider.getProfile().then(profile => {
      this.user = profile;
    }, err => {
      console.error(err);
    });

    setTimeout(function(){
      this.disabled = false;
    }, 3000);
  }

  ngOnInit() {
      this.current_iso =  this.today.toISOString();
      this.current_date =  this.current_iso.substring(0,10);
      this.current_year =  this.current_iso.substring(0,4);

      let yesterday =  new Date(this.today);
      yesterday.setDate(yesterday.getDate()-1);
      this.yesterday_iso = yesterday.toISOString();

      let tomorrow =  new Date(this.today);
      tomorrow.setDate(tomorrow.getDate()+1);
      this.tomorrow_iso = tomorrow.toISOString();


    this.form = new FormGroup({
      title: new FormControl('',
        [Validators.required]),
      date: new FormControl('',
        [Validators.required]),
      time: new FormControl('',
          [Validators.required])

    });
  }

  pickImage() {
    var self = this;

    let loading = this.createLoading();
    this.camera.getPicture(this.options).then((imageData) => {

    loading.present();
    let options: FileUploadOptions = {
      fileKey: 'picture'
    };
    let fileTransfer = this.transfer.create();
    fileTransfer.upload(imageData, 'https://webwedmobile.net/api/upload/file', options)
     .then((response) => {
       var data = JSON.parse(response.response)['data'];
       var path = data['path'];
       self.photo = 'https://webwedmobile.net/storage/' + (path);
       loading.dismiss();
     }, (err) => {
       loading.dismiss();
     })
    });
  }

  getBackgroundStyle() {
    if (this.photo != ''){
      return { 'background-image': 'url("'+this.photo+'")', 'background-size': '50%', 'background-position': 'center' };
    }else{
      return { 'background-color': '#043f7b' };
    }
  }

  goToRegistryLink(params){
    if (!params) params = {};
    this.navCtrl.push(RegistryLinkPage);
  }

  gotoRegistry(){
    this.navCtrl.push(RegistryLinkPage);
  }

  gotoMarrigeLicense(){
    this.navCtrl.push(MarriageLicensePage);
  }

  changePackage(id){
    this.package = id;
    this.calculateTotal();
  }

  changing: boolean = false;
  test: any = false;

  updateAdddonSelection(index,event){
    console.log(index);
    console.log(event);
    // alert(JSON.stringify(this.form.controls['addon0'].value));
    // if (this.changing){
    //   return;
    // }
    //
    // this.changing = true;
    // var self = this;
    // setTimeout(function(){
    //   self.changing = false;
    // }, 5000)
    //
    // if (typeof(this.addons[index].selected) == 'undefined'){
    //   this.addons[index].selected = true;
    // }else{
    //   this.addons[index].selected = !this.addons[index].selected;
    // }
    // this.calculateTotal();
  }

  calculateTotal(){
    console.log(this.packages);
    console.log(this.package);
    let _package = this.packages[this.package-1].price;
    let addons = 0;

    for (var i = 0; i < this.addons.length; i++) {
      let addon = this.addons[i];
      //this.addons[i].selected = this.form.controls['addon'+i].value;
      if (addon.selected){
        addons += addon.price;
      }
    }

    let total = _package + addons;
    this.total = total;
  }

  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = encodeURI(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
  }

  submit(){

    /*this.openCheckout().then(
      (token) => {*/
        //console.log(token);
        // let loading = this.createLoading();
        // loading.present();

        let eventData : EventData = new EventData;
        eventData.token = 'test';
        eventData.title = this.form.controls.title.value;
        eventData.date = (new Date(this.form.controls.date.value)).getTime()/1000;
        eventData.time = (new Date("1/1/2017 " + this.form.controls.time.value)).getTime()/1000;
        eventData.photo = this.photo;
        eventData.package = {
          id: this.packages[this.package-1].id
        };

        var _addons : AddonData[] = [];
        for (var i = 0; i < this.addons.length; i++){
          let addon = this.addons[i];
          let formControl = this.form.controls['addon'+i.toString()];
          if (formControl.value == false){ continue; }
          _addons = _addons.concat(addon);
        }

        eventData.addons = _addons;

        let file = null;
        if (this.photo != ''){
          file = this.dataURItoBlob(this.photo);
        }else{
          file = null;
        }

        this.navCtrl.push(CheckoutDetailsPage, {
          eventData: eventData,
          package: this.packages[this.package-1],
          file: file,
          user: this.user
        });

        // this.httpProvider.createEvent(eventData, file).then(
        //   (res) => {
        //     console.log(res);
        //     this.navCtrl.pop();
        //     console.log(res);
        //     loading.dismiss();
        //   },
        //   (err) => {
        //     console.error(err);
        //     loading.dismiss();
        //   }
        // );
      /*},
      (err) => {

      }
    );*/
  }

  globalListener : any;

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  openCheckout() {
    return new Promise((resolve, reject) => {
      let handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_OtZkFKLIU1WBuonz6xbk6UQB',
        locale: 'en',
        token: function (token: any) {
          resolve(token);
        }
      });

      handler.open({
        name: 'Web Wed Mobile',
        description: 'Event Package Purchase',
        amount: this.total * 100,
        currency: 'usd',
        email: this.user.personal.email,
        image: 'assets/icon/favicon.ico',
        ['allow-remember-me']: true
      });

      this.globalListener = this.renderer.listenGlobal('window', 'popstate', () => {
        handler.close();
      });
    });
  }

  termsOfService(){
    this.iab.create('http://www.webwedmobile.com/legal');
  }

  legal(){
    // const browser = this.iab.create('https://webwedmobile.com/legal');
    let callback = function(a){console.log(a);};
    (<any>window).cordova.exec(callback, callback, 'InAppBrowser', 'open', ['http://webwedmobile.com/legal', '_system', '']);
  }

  selectCheckBox(index){
    // alert('Selected index: ' + index);
  }

}
