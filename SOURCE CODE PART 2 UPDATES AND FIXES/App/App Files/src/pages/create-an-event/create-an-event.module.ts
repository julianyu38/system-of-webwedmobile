import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateAnEventPage } from './create-an-event';


@NgModule({
  declarations: [
    CreateAnEventPage
  ],
  imports: [
    IonicPageModule.forChild(CreateAnEventPage),
  ],
})
export class CreateAnEventPageModule {}
