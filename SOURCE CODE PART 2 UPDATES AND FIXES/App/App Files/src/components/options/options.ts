import { Component, Input } from '@angular/core';

/**
 * Generated class for the OptionsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'options',
  templateUrl: 'options.html'
})
export class OptionsComponent {

  @Input('event') eventId: any;
  hasShare: boolean = false;

  constructor() {
    // console.log('Hello OptionsComponent Component');
  }

}
