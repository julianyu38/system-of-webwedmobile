import { Component, Input, Output, EventEmitter, ContentChild } from '@angular/core';
import { MyAccountPage } from '../../pages/my-account/my-account';
import { NotificationsPage } from '../../pages/notifications/notifications';
import { NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

/**
 * Generated class for the MainNavigationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'sub-navigation',
  templateUrl: 'sub-navigation.html'
})
export class SubNavigationComponent {

  @Input('title') title
  @Input('tabs') tabs?: any;
  @Output('selected-tab') selected_tab? : EventEmitter<number> = new EventEmitter<number>();

  navTitle: string;
  selected_tab_identifier: string = '';

  constructor(public navCtrl: NavController, private statusBar: StatusBar) {

  }

  ngAfterViewInit(){
    this.navTitle = this.title;

    // let status bar overlay webview
    this.statusBar.overlaysWebView(false);

    //White text
    this.statusBar.styleLightContent();

    // set status bar to white
    this.statusBar.backgroundColorByHexString('#1658a0');

    if (this.tabs){
      this.selectTab(0);
    }
  }

  gotoNotifications(){
    this.navCtrl.setRoot(NotificationsPage);
  }

  gotoProfile(){
    this.navCtrl.setRoot(MyAccountPage);
  }

  selectTab(index: number){
    this.selected_tab_identifier = this.tabs[index];
    this.selected_tab.emit(index);
  }

}
