import { Component, Input } from '@angular/core';
import { ArchivePage } from '../../pages/archive/archive';
import { AttendEventModalPage } from '../../pages/attend-event-modal/attend-event-modal';
import { WedDetailsPage } from '../../pages/wed-details/wed-details';
import { HttpProvider } from '../../providers/http/http';
import { LoadingController, NavController, ModalController } from  'ionic-angular';
/**
 * Generated class for the CWedCardComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */

@Component({
  selector: 'c-wed-card',
  templateUrl: 'c-wed-card.html',
  providers: [HttpProvider]
})

export class CWedCardComponent {
  @Input('cWedCardImage') wedCardImage
  @Input('cWedCardHeader') wedCardHeader
  @Input('cWedCardSubHeader') wedCardSubHeader
  @Input('cWedCardParticipants') wedCardParticipants
  @Input('cWedCardEvent') wedCardEvent: any
  @Input('user') user: number


  cardImage: string;
  cardHeader: string;
  cardSubHeader: string;
  cardParticipants: any[];
  type: string = 'invite';
  event_status: number;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public httpProvider: HttpProvider,
    public modalCtrl: ModalController
  ) {

  }

  ngAfterContentInit() {

    this.cardImage = this.wedCardImage;
    //this.cardHeader = this.wedCardHeader;
    this.cardSubHeader = this.wedCardSubHeader;
    this.cardParticipants = this.wedCardParticipants;
    // console.log('participants');
    // console.log(this.wedCardEvent.participants);

    let status = this.wedCardEvent.event_status;
    this.event_status = status;

    if ((this.wedCardEvent != undefined) &&
      (this.wedCardEvent.photo != undefined) &&
      (this.wedCardEvent.photo.length > 1)) {
      //TODO: convert cover_photo to image-url to show here
      this.cardImage = this.wedCardEvent.photo;
    } else {
      this.cardImage = this.wedCardImage;
    }

    if ((this.wedCardEvent != undefined) &&
      (this.wedCardEvent.title != undefined) &&
      (this.wedCardEvent.title.length > 0)) {
      this.cardHeader = this.wedCardEvent.title;
    } else {
      this.cardHeader = "";
    }

    if ((this.wedCardEvent != undefined) &&
      (this.wedCardEvent.date != undefined)) {
      this.cardSubHeader = this.wedCardEvent.date;
    } else {
      this.cardSubHeader = "";
    }


    if ((this.wedCardEvent.participants != undefined) &&
      (this.wedCardEvent.participants.length > 0)) {
      this.cardParticipants = [];
      for (var aa = 0; aa < this.wedCardEvent.participants.length; aa++) {
        var aParticipant = this.wedCardEvent.participants[aa];
        if ((aParticipant != undefined) &&
          (aParticipant.personal != undefined)) {
          this.cardParticipants.push(aParticipant);
        }
      }
      // console.log(this.cardParticipants);
    } else {
      this.cardParticipants = [];
    }

    if (this.wedCardEvent.type != undefined){
      this.type = this.wedCardEvent.type;
    }

    //this.cardImage = this.wedCardEvent.details.title;
    //this.cardSubHeader = this.wedCardEvent.;
    //this.cardParticipants = this.wedCardEvent.;
  }

  getBackgroundStyle() {
    return { 'background-image': 'url("'+this.cardImage+'")', 'background-position': 'center', 'background-size': '400px', 'background-repeat': 'no-repeat', 'min-height': '300px' };
  }

  play() {
    this.navCtrl.push(ArchivePage, {
      event: this.wedCardEvent.id
    });
  }

  gotoWedDetails = function() {
    if (this.event_status == 2){
      return;
      // let modal = this.modalCtrl.create(AttendEventModalPage, {
      //   event: this.wedCardEvent.id
      // });
      // let me = this;
      // modal.onDidDismiss(data => {
      //   console.log(data);
      // });
      // modal.present();
    }

    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getEventDetailsById(this.wedCardEvent.id).then(
      (res) => {
        this.navCtrl.push(WedDetailsPage, {
          event: res,
          user: this.user
        });
        loading.dismiss();
      },
      (err) => {
        console.error(err);
      }
    );
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

}
