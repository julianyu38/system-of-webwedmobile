import { Component, Input } from '@angular/core';

/**
 * Generated class for the CircularProgressBarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'heart-button',
  templateUrl: 'circular-progress-bar.html'
})
export class CircularProgressBarComponent {

  @Input('progress') progress = 0;
  @Input('width') width;
  @Input('color') color = '#00bbd3';
  @Input('placeholder-color') placeholderColor = '#d6d8db';
  @Input('state') state = 1;
  @Input('max') max = '100';

  backgroundUrl : any;
  imageWidth : any

  constructor() {
    // console.log('Hello CircularProgressBarComponent Component');
    this.width = 250;
  }

  ngAfterContentInit(){
    if(this.state == 1){
      this.backgroundUrl="assets/images/heart_active.png";
    }else{
      this.backgroundUrl="assets/images/heart_disable.png";
    }
    this.imageWidth = this.width;
  }

  progressValueChanged(){
    if(this.progress > 101)
      {
        this.backgroundUrl="assets/images/heart_broadcast.png";
        this.imageWidth = this.width + 2;
      }
  }

}
