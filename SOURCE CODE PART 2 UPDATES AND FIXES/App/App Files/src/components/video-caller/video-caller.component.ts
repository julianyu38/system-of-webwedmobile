import {Component} from "@angular/core";
import {VideoCallWidgetComponent} from "../shared/video-call-widget/video-call-widget.component";
import {VideoCallStateManagerService} from "../../services/video-call-state-manager.service";
import {VideoCallManager} from "../../services/video-call-manager.service";

@Component({
  selector: 'video-caller',
  providers: [VideoCallManager, VideoCallStateManagerService],
  templateUrl: 'video-caller.component.html',
  styleUrls: ['video-caller.component.css'],
  viewProviders: [VideoCallWidgetComponent]
})
export class VideoCallerComponent {

   //sessionId = "1_MX40NTg5NzI0Mn5-MTQ5ODIwODc1NzU0M354STByZDE0M080SEg0MzBCdXl2cFJWc2d-UH4";
  //////valid until 25 August
  // token = "T1==cGFydG5lcl9pZD00NTg5NzI0MiZzaWc9YWViNTljN2MyMjBjOWNjNjExMzU2Yjc1NjE3OTc3MjZjNDkxMzk0ZTpzZXNzaW9uX2lkPTFfTVg0ME5UZzVOekkwTW41LU1UUTVPREl3T0RjMU56VTBNMzU0U1RCeVpERTBNMDgwU0VnME16QkNkWGwyY0ZKV2MyZC1VSDQmY3JlYXRlX3RpbWU9MTUwMDk3NDI0MyZub25jZT0wLjgwMjM2ODA4ODE4MDg5MDYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTUwMzU2NjE1MiZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=="
  sessionId = "1_MX40NTk0NDI1Mn5-MTUwMzQ1NTI5NjU4OX5OY2poZW1iM05iT2w1UHQ1MHFTUXJZNWF-QX4";
  token = "T1==cGFydG5lcl9pZD00NTk0NDI1MiZzaWc9ZWNmYzRjYTFlOGY0YmQ2YmI1MzZiN2E5ZDIzZTE3ZWU0NjdiMjU0ZDpzZXNzaW9uX2lkPTFfTVg0ME5UazBOREkxTW41LU1UVXdNelExTlRJNU5qVTRPWDVPWTJwb1pXMWlNMDVpVDJ3MVVIUTFNSEZUVVhKWk5XRi1RWDQmY3JlYXRlX3RpbWU9MTUwMzQ1NTI5NiZyb2xlPW1vZGVyYXRvciZub25jZT0xNTAzNDU1Mjk2LjYxNTgwODUwNzImZXhwaXJlX3RpbWU9MTUwNDA2MDA5NiZjb25uZWN0aW9uX2RhdGE9bmFtZSUzREpvaG5ueQ==";

}
