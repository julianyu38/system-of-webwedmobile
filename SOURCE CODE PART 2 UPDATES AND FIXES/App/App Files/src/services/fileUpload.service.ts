import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class UploadService {
  progress : any;
  progressObserver: Observable<any>;

  constructor () {
    this.progress = Observable.create(observer => {
      this.progressObserver = observer
    }).share();
  }

  makeFileRequest (url: string, params: string[], files: File[], token: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();

      // for (let i = 0; i < files.length; i++) {
      //   formData.append("uploads", files[i], files[i].name);
      // }

      for (let i = 0; i < Object.keys(params).length; i++){
        formData.append(Object.keys(params)[i], params[i]);
      }

      formData.append('file', files[0], 'file');

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            console.log(xhr.response);
            resolve(xhr.response);
          } else {
            reject(xhr.response);
          }
        }
      };

      xhr.upload.onprogress = (event) => {
        this.progress = Math.round(event.loaded / event.total * 100);

        //this.progressObserver.next(this.progress);
        console.log('progress: ' + this.progress);
      };

      xhr.open('POST', url, true);
      // xhr.setRequestHeader('Content-Type', 'multipart/form-data');
      xhr.setRequestHeader("X-Requested-Auth", "eyJhbGciOiJSUzI1NiIsImtpZCI6ImY3ZGFjOGY4MzYwNjVjODgwODNkMzkwNzZhODU4OWEzMTk5NWZjNDQifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcGxheWdyb3VuZC1kZDFkOCIsImF1ZCI6InBsYXlncm91bmQtZGQxZDgiLCJhdXRoX3RpbWUiOjE1MDM1ODI5MjYsInVzZXJfaWQiOiJDSHBPOUdQcGFGZ2tzMU4wN2syOThNVmQ4dVYyIiwic3ViIjoiQ0hwTzlHUHBhRmdrczFOMDdrMjk4TVZkOHVWMiIsImlhdCI6MTUwMzU4MzEyMCwiZXhwIjoxNTAzNTg2NzIwLCJlbWFpbCI6InN3ZWF0LmF1c3RpbkByaXZlbG9wZXIuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInN3ZWF0LmF1c3RpbkByaXZlbG9wZXIuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.Y1d8CKLdM6aBw9-mBKSVAOlZEpoVeHMBFXyn2RxqW-BuDpS7etkPMTlKj8CkpP5D_90dhPbOc_RTezC7TX4UbiUEKBtBaaQzSGUbB5h8lb8zKot7L-GuKYqQjUpMIhyBUkmoTs2FUKCf3L3jPoEH5165IenkehjNaNU9H8mDelbnVo0KGfPnkiFa6A4qKGnFWF0PEBpdmvjzBk2wrbb3kPC2mzni9WSEwVISyquuO79fv3GRzdf4ydOIZiL_elle64bj3fZHVyj9pmTeGOqs0LrQ9dROqUDgGdZdfQyOFRV8jZOCcES1Rly0-70sxKghVvRVEJxJR1_2w8_ao7dd4A");
      xhr.send(formData);
    });
  }
}
