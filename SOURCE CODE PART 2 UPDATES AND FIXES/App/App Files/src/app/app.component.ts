import { Component, ViewChild, enableProdMode } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController, MenuController } from 'ionic-angular';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { LoginPage } from '../pages/login/login';
import { MyAccountPage } from '../pages/my-account/my-account'
import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from './../pages/home/home';
import { Keyboard } from '@ionic-native/keyboard';
import { CreateAnEventPage } from '../pages/create-an-event/create-an-event';
import { HttpProvider } from '../providers/http/http';
import { StorageProvider, NotificationData } from '../providers/storage/storage';
import { Deeplinks } from '@ionic-native/deeplinks';
import { TourProvider } from '../providers/tour/tour';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';

@Component({
  templateUrl: 'app.html',
  providers: [HttpProvider, StorageProvider, Deeplinks, TourProvider, InAppBrowser, EmailComposer]
})
export class MyApp {
  @ViewChild('content') nav: NavController;
  rootPage:any = WalkthroughPage;
  // rootPage:any = LoginPage;

  user: any = {
    avatar: '',
    name: ''
  };

  auth: AngularFireAuth;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard,
    auth: AngularFireAuth,
    public menuCtrl: MenuController,
    public httpProvider: HttpProvider,
    public storageProvider: StorageProvider,
    private deeplinks: Deeplinks,
    public tourProvider: TourProvider,
    private iab: InAppBrowser,
    private emailComposer: EmailComposer) {

      this.auth = auth;
      var self = this;
    platform.ready().then(() => {
      const authObserver = auth.authState.subscribe(user => {
        if (user) {
          storageProvider.getSeenWalkthrough().then(seen => {
            if (seen){
              this.rootPage = LoginPage;
            }else{
              this.rootPage = WalkthroughPage;
            }
          });
          // this.rootPage = DashboardPage;
          // authObserver.unsubscribe();
          // if (self.rootPage == DashboardPage){
            setTimeout(function(){
              self.storageProvider.getProfile().then(profile => {
                console.log(profile);
                if (profile == null){
                  return;
                }
                self.user = {
                  avatar: self.httpProvider.apiUrl + '/user/' + profile.personal.id + '/avatar',
                  name: profile.personal.name.full
                };
              }, err => {
                console.error(err);
              });
            }, 1000);
          // }

        } else {
          this.rootPage = LoginPage;
          // authObserver.unsubscribe();
        }
      });
      keyboard.disableScroll(true);
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.deeplinks.route({
         '/event/share/:eventId': LoginPage
       }).subscribe((match) => {
         // match.$route - the route we matched, which is the matched entry from the arguments to route()
         // match.$args - the args passed in the link
         // match.$link - the full link data
         console.log('Successfully matched route', match);
       }, (nomatch) => {
         // nomatch.$link - the full link data
         console.error('Got a deeplink that didn\'t match', nomatch);
       });
    });
  }

  createEvent(){
    this.nav.push(CreateAnEventPage);
    this.menuCtrl.close();
  }

  myEvents(){
    this.nav.setRoot(DashboardPage);
    this.menuCtrl.close();
  }

  orderHistory(){
    this.nav.setRoot(MyAccountPage, {
      order: true
    });
    this.menuCtrl.close();
  }

  myAccount(){
    this.nav.setRoot(MyAccountPage);
    this.menuCtrl.close();
  }

  legal(){
    // const browser = this.iab.create('https://webwedmobile.com/legal');
    let callback = function(a){console.log(a);};
    (<any>window).cordova.exec(callback, callback, 'InAppBrowser', 'open', ['http://webwedmobile.com/legal', '_system', '']);
  }

  termsOfService(){
    // const browser = this.iab.create('https://webwedmobile.com/legal');
    let callback = function(a){console.log(a);};
    (<any>window).cordova.exec(callback, callback, 'InAppBrowser', 'open', ['http://webwedmobile.com/legal', '_system', '']);
  }

  signOut(){
    this.menuCtrl.close();
    this.auth.auth.signOut().then(() => {
       console.log('Signed Out');
       this.storageProvider.clearAll();
       this.nav.setRoot(LoginPage);
    });
  }

  becomeOfficiant(){
    (<any>window).cordova.plugins.email.open({
          app: 'mailto',
          to: 'officiants@webwedmobile.com',
          cc: 'info@webwedmobile.com',
          subject: 'WebWedMobile - Become Officiant',
          body: 'Hello, I want to become an officiant.',
          isHtml: false
        });
  }
}
