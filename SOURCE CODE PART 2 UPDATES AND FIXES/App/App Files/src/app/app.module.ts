import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Keyboard } from '@ionic-native/keyboard';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { AuthProvider } from '../providers/auth/auth';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { MyApp } from './app.component';
import { IntroPage } from '../pages/intro/intro';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { LoginPage } from '../pages/login/login';
import { LoginPageModule } from '../pages/login/login.module';
import { MyAccountPage } from '../pages/my-account/my-account'
import { NotificationsPage } from '../pages/notifications/notifications';
import { NotificationsPageModule } from '../pages/notifications/notifications.module';
import { RegisterPage } from '../pages/register/register';
import { RegisterPageModule } from '../pages/register/register.module';
import { DashboardPageModule } from '../pages/dashboard/dashboard.module';
import { WedDetailsPage } from '../pages/wed-details/wed-details';
import { WedDetailsPageModule } from '../pages/wed-details/wed-details.module';
import { CreateAnEventPage } from '../pages/create-an-event/create-an-event';
import { CreateAnEventPageModule } from '../pages/create-an-event/create-an-event.module';
import { RegistryLinkPage } from '../pages/registry-link/registry-link';
import { MarriageLicensePage } from '../pages/marriage-license/marriage-license';
import { OrderDetailsPage } from '../pages/order-details/order-details';
import { CreateAnInvitationPage } from '../pages/create-an-invitation/create-an-invitation';
import { PreApprovedOfficiantPage } from '../pages/pre-approved-officiant/pre-approved-officiant';
import { OfficiantDetailsPage } from '../pages/officiant-details/officiant-details';
import { IonicStorageModule } from '@ionic/storage';
import { StorageProvider } from '../providers/storage/storage';
import { ComponentsModule } from '../components/components.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PickContactPageModule } from '../pages/pick-contact/pick-contact.module';
import { AutocompletePageModule } from '../pages/autocomplete/autocomplete.module';
import { AttendEventModalPageModule } from '../pages/attend-event-modal/attend-event-modal.module';
import { AddManualModalPageModule } from '../pages/add-manual-modal/add-manual-modal.module';
import { RolePopoverPageModule } from '../pages/role-popover/role-popover.module';
import { BroadcastPageModule } from '../pages/broadcast/broadcast.module';
import { ArchivePageModule } from '../pages/archive/archive.module';
import { EventCompletedPageModule } from '../pages/event-completed/event-completed.module';
import { PickEmojiPageModule } from '../pages/pick-emoji/pick-emoji.module';
import { WalkthroughPageModule } from '../pages/walkthrough/walkthrough.module';
import { CheckoutDetailsPageModule } from '../pages/checkout-details/checkout-details.module';
import { MarriageApplicationPageModule } from '../pages/marriage-application/marriage-application.module';
import { MarriageEducationPageModule } from '../pages/marriage-education/marriage-education.module';
import { SelectCourtPageModule } from '../pages/select-court/select-court.module';

// import { OpentokModule } from "ng2-opentok/dist/opentok.module"
import { TourProvider } from '../providers/tour/tour';

import { PayPage } from '../pages/pay/pay';

export const firebaseConfig = {
  apiKey: "AIzaSyBHLBBX-AwKAfE2VX2K1twMo14QuUh86F8",
  authDomain: "playground-dd1d8.firebaseapp.com",
  databaseURL: "https://playground-dd1d8.firebaseio.com",
  projectId: "playground-dd1d8",
  storageBucket: "playground-dd1d8.appspot.com",
  messagingSenderId: "328542486655"
};


@NgModule({
  declarations: [
    MyApp,
    IntroPage,
    MyAccountPage,
    RegistryLinkPage,
    MarriageLicensePage,
    OrderDetailsPage,
    CreateAnInvitationPage,
    PreApprovedOfficiantPage,
    OfficiantDetailsPage,
    PayPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md'
    },
    {
      links: [
        // { loadChildren: '../pages/home/home.module#HomePageModule', name: 'home', segment: 'home', priority: 'low', defaultHistory: [] },
        //{ loadChildren: '../pages/register/register.module', name: 'register', segment: 'register', priority: 'low', defaultHistory: [] },
        // { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'login', segment: 'login', priority: 'low', defaultHistory: [] }
      ]
    }),
    IonicStorageModule.forRoot(),
    ReactiveFormsModule,
    RegisterPageModule,
    LoginPageModule,
    DashboardPageModule,
    WedDetailsPageModule,
    CreateAnEventPageModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NotificationsPageModule,
    ComponentsModule,
    BrowserAnimationsModule,
    PickContactPageModule,
    AutocompletePageModule,
    AttendEventModalPageModule,
    AddManualModalPageModule,
    RolePopoverPageModule,
    BroadcastPageModule,
    ArchivePageModule,
    EventCompletedPageModule,
    PickEmojiPageModule,
    WalkthroughPageModule,
    CheckoutDetailsPageModule,
    MarriageApplicationPageModule,
    MarriageEducationPageModule,
    SelectCourtPageModule
    // OpentokModule.forRoot({apiKey: "45944252"})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    IntroPage,
    DashboardPage,
    LoginPage,
    MyAccountPage,
    NotificationsPage,
    RegisterPage,
    WedDetailsPage,
    CreateAnEventPage,
    RegistryLinkPage,
    MarriageLicensePage,
    OrderDetailsPage,
    CreateAnInvitationPage,
    PreApprovedOfficiantPage,
    OfficiantDetailsPage,
    PayPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    Facebook,
    GooglePlus,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    StorageProvider,
    TourProvider
  ]
})
export class AppModule {}
