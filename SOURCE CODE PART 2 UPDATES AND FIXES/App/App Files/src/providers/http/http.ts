import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import { StorageProvider, OrderData, EventData, EventDetailData, AddonData, EventConfigData, NotificationData, UserData, ContactData, AllEvents } from '../storage/storage';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { UploadService } from '../../services/fileUpload.service';
import { AlertController } from 'ionic-angular';


/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class HttpProvider {

  // public apiUrl: string = 'http://www.riveloper.com/client_portal/webwed-admin/public/api';
  public apiUrl: string = 'https://webwedmobile.net/api';
  // public apiUrl: string = 'http://localhost:8888/api';
  public token: string = 'invalid_token';
  public headers: Headers;
  public options: RequestOptions;

  public storageProvider: StorageProvider;
  public uploadService: UploadService;

  constructor(
    public http: Http,
    public storage: Storage,
    private fbAuth: AngularFireAuth,
    private fbDb: AngularFireDatabase,
    private alertCtrl: AlertController
  ) {
    this.storageProvider = new StorageProvider(http, storage);
    this.storageProvider.getToken().then(
      (result) => {
        this.token = atob(result);
        this.headers = new Headers({
          'X-Requested-Auth': this.token
        });
        this.options = new RequestOptions({ headers: this.headers });
      },
      (err) => {
        console.error(err);
        this.headers = new Headers({
          'X-Requested-Auth': this.token
        });
        this.options = new RequestOptions({ headers: this.headers });
      }
    );
  }

  getOptions() : Promise<RequestOptions> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.headers = new Headers({
            'X-Requested-Auth': this.token
          });
          var options = new RequestOptions({ headers: this.headers });
          resolve(options);
        },
        (err) => {
          console.error(err);
          this.headers = new Headers({
            'X-Requested-Auth': this.token
          });
          var options = new RequestOptions({ headers: this.headers });
          resolve(options);
        }
      );
    });
  }


  refreshToken() : Promise<boolean> {
    return new Promise((resolve, reject) => {

    });
  }

  getRequest(url: string) : any {
    console.log('url: ' + url);
    /*if (typeof(this.options) == 'undefined'){
      this.getOptions().then(
        (options) => {
          console.log(options);
          return this.http.get(this.apiUrl + url, options).map(res => res.json());
        },
        (err) => {
          //ignore
          return this.http.get(this.apiUrl + url, this.options).map(res => res.json());
        }
      );
    }else{*/
      return this.http.get(this.apiUrl + url, this.options).map(res => res.json());
    /*}*/
  }

  postRequest(url: string, data: any) : any {
    console.log('url: ' + url);
    /*if (typeof(this.options) == 'undefined'){
      this.getOptions().then(
        (options) => {
          console.log(options);
          return this.http.post(this.apiUrl + url, data, options).map(res => res.json());
        },
        (err) => {
          //ignore
          return this.http.post(this.apiUrl + url, data, this.options).map(res => res.json());
        }
      );
    }else{*/
      return this.http.post(this.apiUrl + url, data, this.options).map(res => res.json());
    /*}*/
  }

  handleError(data: any) : any {
    if (data['error']){
      let alert = this.alertCtrl.create({
        title: data['error']['title'],
        subTitle: data['error']['message'],
        buttons: ['Dismiss']
      });
      alert.present();
    }else{
      let alert = this.alertCtrl.create({
        title: 'Unknown Error',
        subTitle: 'An unknown error occurred, this could be a network or server error.',
        buttons: ['Dismiss']
      });
      alert.present();

      console.log('handlingError: ' + JSON.stringify(data));
    }
    console.log('alertCtrl');
  }

  uploadRequest(url: string, data: any, file: File) : any {
    this.uploadService = new UploadService();
    let files = [];
    if (file == null){
      files = [];
    }else{
      files = [file];
    }
    this.uploadService.makeFileRequest(this.apiUrl + url, data, files, this.token).then(
      (res) => {
        //console.log(res);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  getEventDetailsById(id: number) : Promise<EventDetailData>{
    return new Promise((resolve, reject) => {
      this.getRequest('/event/'+id+'/details').subscribe(
          res => {
            console.log(res);
            if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
          },
          err => {
            console.error(err);
            this.handleError(err); reject(err);
          },
          () => {
              //console.log('getEventDetailsById completed');
          }
        );
    });
  }

  getEventConfig() : Promise<EventConfigData>{
    return new Promise((resolve, reject) => {
      this.getRequest('/event/config').subscribe(
          res => {
            //console.log(res);
            if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
          },
          err => {
            console.error(err);
            this.handleError(err); reject(err);
          },
          () => {
              //console.log('getUserOrders completed');
          }
        );
    });
  }

  createEvent(params: EventData) : Promise<EventDetailData> {
    // return this.getEventDetailsById(1);
    return new Promise((resolve, reject) => {
      /*this.uploadRequest('/event/create', params, photo);/*.then(
        (res) => {
          //console.log(res);
        },
        (err) => {
          console.error(err);
        }
      )*/
      this.postRequest('/event/create', params).subscribe(
          res => {
            //console.log(res);
            if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
          },
          err => {
            console.error(err);
            this.handleError(err); reject(err);
          },
          () => {
              //console.log('createEvent completed');
          }
      );
    });
  };

  getUserEvents(id: number) : Promise<AllEvents>{
    return new Promise((resolve, reject) => {
      this.getRequest('/user/'+id.toString()+'/events').subscribe(
        res => {
          console.log(res);
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        err => {
          console.error(err);
          this.handleError(err); reject(err);
        },
        () => {
          //console.log('getUserEvents completed');
        }
      );
    });
  }

  getUserData(id: String){
    return this.getRequest('/user/'+id);
  }

  signIn(accessToken: String) : Promise<any>{
    return new Promise((resolve, reject) => {
      this.getRequest('/auth/login?id_token='+accessToken).subscribe(
        res => {
          if (!res['success']){ this.handleError(res); reject(res); }
          resolve(res['data']);
        },
        err => {
          this.handleError(err); reject(err);
        }
      )
    });
  }

  getUserOrders(id: number): Promise<[OrderData]>{
    return new Promise((resolve, reject) => {
      this.getRequest('/user/'+id+'/orders').subscribe(
          res => {
            console.log(res);
            if (!res['success']){ this.handleError(res); reject(res); }
            resolve(res['data']['orders']);
          },
          err => {
            console.error(err);
            this.handleError(err); reject(err);
          },
          () => {
              //console.log('getUserOrders completed');
          }
        );
    });
  }

  //@TODO add error handling to all requests.

  getNotifications(id: number): Promise<[NotificationData]>{
    return new Promise((resolve, reject) => {
      this.getRequest('/user/'+id+'/notifications').subscribe(
        res => {
          //console.log(res);
          if (!res['success']){ this.handleError(res); reject(res); }
          resolve(res['notifications']);
        },
        err => {
          console.error(err);
          this.handleError(err); reject(err);
        },
        () => {
          //console.log('getNotifications completed');
        }
      )
    });
  }

  clearNotification(user_id: number, notification_id: number): Promise<[NotificationData]> {
    return new Promise((resolve, reject) => {
      this.getRequest('/user/'+user_id+'/notification/'+notification_id+'/clear').subscribe(
        res => {
          //console.log(res);
          if (!res['success']){ this.handleError(res); reject(res); }
          resolve(res['notifications']);
        },
        err => {
          console.error(err);
          this.handleError(err); reject(err);
        },
        () => {
          //console.log('getNotifications completed');
        }
      )
    });
  }

  requestSubmitUserData(accessToken: string, data: any){
    return this.postRequest('/auth/login?id_token='+accessToken, data);
  }

  isPublic(eventId: any): Promise<boolean>{
    return new Promise((resolve, reject) => {
      this.getRequest('/public/'+eventId.toString()).subscribe(
        (res) => {
          if (!res['success']){ this.handleError(res); reject(res); }

          if (res['data']['public']){
            resolve(true);
          }else{
            resolve(false);
          }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  updatePublic(eventId: any, privacy: boolean) : Promise<any>{
    return new Promise((resolve, reject) => {
      this.getRequest('/public/'+eventId.toString()+'/'+(privacy ? 1 : 0)).subscribe(
        (res) => {
          if (!res['success']){ this.handleError(res); reject(res); }

          if (res['data']['public']){
            resolve(true);
          }else{
            resolve(false);
          }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  addParticipant(eventId: number, user: ContactData, index: number): Promise<EventDetailData>{
    return new Promise((resolve, reject) => {
      this.postRequest('/event/'+eventId+'/participants/add', {user: user, index: index}).subscribe(
        (res) => {
          if (!res['success']){ this.handleError(res); reject(res); }

          // //console.log(res);
          // return;
          if (res['success']){
            if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
          }else{
            reject(res);
          }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  updateParticipant(eventId: number, participantId: number, roleId: number): Promise<EventDetailData>{
    return new Promise((resolve, reject) => {
      this.postRequest('/event/'+eventId+'/participants/'+participantId+'/update', {
        role: roleId
      }).subscribe(
        (res) => {
          if (!res['success']){ this.handleError(res); reject(res); }

          if (res['success']){
            if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
          }else{
            reject(res);
          }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  removeParticipant(eventId: number, participantId: number): Promise<EventDetailData>{
    return new Promise((resolve, reject) => {
      this.getRequest('/event/'+eventId+'/participants/'+participantId+'/remove').subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    })
  }

  updateGuestList(eventId: number, users: ContactData[]): Promise<EventDetailData> {
    return new Promise((resolve, reject) => {
      this.postRequest('/event/'+eventId+'/guests/update', {contacts: users}).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  getOfficiants(): Promise<UserData[]> {
    return new Promise((resolve, reject) => {
      this.getRequest('/officiants').subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']['officiants']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  respondToInvite(response: number, eventId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/event/'+eventId.toString()+'/invite/'+response.toString()).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']['events']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  sendInvite(eventId: number, subject: string, message: string, video: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/event/'+eventId.toString()+'/send_invite', {
        subject: subject,
        message: message,
        video: video
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(true); }else{ this.handleError(res); reject(false); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      )
    });
  }

  getEventSessionDetails(eventId: number, userId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/start/'+eventId+'/'+userId).subscribe(
        (res) => {
          console.log(res);
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  activateEvent(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
      // this.fbDb.object(`/events/${eventId}`).set({
      //   activate: true
      // }).then(() => {
      //   resolve();
      // }, reject);
    })
  }

  deactivateEvent(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      resolve();
      // this.fbDb.object(`/events/${eventId}`).set({
      //   activate: false
      // }).then(() => {
      //   resolve();
      // }, reject);
    })
  }

  isActive(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      const _data = this.fbDb.object(`/events/${eventId}`).subscribe((data) => {
        _data.unsubscribe();
        console.log(data);
        if (data['activate']){
          resolve(true);
        }else{
          resolve(false);
          // resolve(false);
        }
      }, reject);
    })
  }

  getBroadcastInformation(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      const _data = this.fbDb.object(`/events/${eventId}`).subscribe((data) => {
        _data.unsubscribe();
        if (data['activate']){
          resolve(data['broadcastUrl']);
        }else{
          resolve(false);
        }
      }, reject);
    })
  }

  updateAccount(data: any) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/myaccount/update', data).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']['user']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  inviteOfficiant(eventId: any, officiantId: any) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/officiant/invite', {
        event: eventId,
        officiant: officiantId
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(true); }else{ this.handleError(res); reject(false); }
        },
        (err) => {
          this.handleError(err); reject(false);
        }
      );
    });
  }

  updateAvatar(data: any) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/avatar/update', {
        image: data
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(true); }else{ this.handleError(res); reject(false); }
        },
        (err) => {
          this.handleError(err); reject(false);
        }
      );
    });
  }

  getArchive(eventId:number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/archive/'+eventId.toString()).subscribe(
        (res) => {
          console.log(res);
          if (res['success']){ resolve(res['video']); }else{ this.handleError(res); reject(false); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      )
    });
  }

  getEventSpecifications(eventId:number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/stream/'+eventId.toString()).subscribe(
        (res) => {
          // resolve(res['minutes']);
          console.log(res);
        },
        (err) => {
          // this.handleError(err); reject(err);
        }
      )

      const _data = this.fbDb.object(`/events/${eventId}`).subscribe((data) => {
        _data.unsubscribe();
        resolve(data);
      }, reject);
    });
  }

  pauseStream(eventId:number, userId:number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.deactivateEvent(eventId).then(
        (res) => {
          this.getRequest('/stop_stream/'+eventId.toString()+'/'+userId.toString()).subscribe(
            (res) => {
              resolve(true);
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          );
        },
        (err) => {

        }
      );
    });
  }

  getEmojis() : Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/emojis').subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']['images']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      )
    });
  }

  getPaymentInfo() : Promise<any> {
    return new Promise((resolve, reject) => {
      this.getRequest('/myaccount/payment_info').subscribe(
        (res) => {
          if (res['success']){ resolve(res['card']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  getMarriageApplication(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.postRequest('/marriage/application?id_token=' + this.token, {
            id: eventId
          }).subscribe(
            (res) => {
              if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          );
        },
        (err) => {
          console.error(err);
        }
      );
    });
  }

  addAttachment(eventId: number, file: string, person: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/application/attachment/add', {
        id: eventId,
        file: file,
        person: person
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  removeAttachment(eventId: number, file: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/application/attachment/remove', {
        id: eventId,
        file: file
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  submitMarriageApplication(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/application/submit', {
        eventId: eventId
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  getMarriageLicense(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.postRequest('/marriage/license/'+eventId.toString()+'?id_token='+this.token, {
            id: eventId
          }).subscribe(
            (res) => {
              if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          );
        }
      );
    });
  }

  addSigner(eventId: number, signer: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/license/signer/add', {
        id: eventId,
        signer: signer
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  removeSigner(eventId: number, signer: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/license/signer/remove', {
        id: eventId,
        signer: signer
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  submitMarriageLicense(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/license/submit', {
        id: eventId
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      );
    });
  }

  getRegistryLink(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.getRequest('/marriage/registry/'+eventId.toString()+'?id_token='+this.token).subscribe(
            (res) => {
              console.log(res);
              if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          )
        },
        (err) => {
          console.error(err);
        }
      );

    });
  }

  saveRegistryLink(eventId: number, link: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.postRequest('/marriage/registry/'+eventId.toString()+'?id_token='+this.token, {
            event: eventId,
            link: link
          }).subscribe(
            (res) => {
              if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          )
        },
        (err) => {
          console.error(err);
        }
      );

    });
  }

  submitMarriageEducation(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.postRequest('/marriage/education/submit', {
        id: eventId
      }).subscribe(
        (res) => {
          if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
        },
        (err) => {
          this.handleError(err); reject(err);
        }
      )
    });
  }

  selectCourt(eventId: number, courtId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.postRequest('/marriage/courts/'+eventId.toString()+'?id_token='+this.token, {
            eventId: eventId,
            courtId: courtId
          }).subscribe(
            (res) => {
              console.log(res);
              if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          )
        },
        (err) => {
          console.error(err);
        }
      );

    });
  }

  getCourts(eventId: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.getRequest('/marriage/courts/'+eventId.toString()+'?id_token='+this.token).subscribe(
            (res) => {
              console.log(res);
              if (res['success']){ resolve(res['data']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          )
        },
        (err) => {
          console.error(err);
        }
      );

    });
  }

  generateStripeToken(name: string, card: number, exp_month: number, exp_year: number, ccv: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.postRequest('/stripe/generate_token?id_token='+this.token, {
            name: name,
            card: card,
            exp_month: exp_month,
            exp_year: exp_year,
            ccv: ccv
          }).subscribe(
            (res) => {
              console.log(res);
              if (res['success']){ resolve(res['token']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          )
        },
        (err) => {
          console.error(err);
        }
      );
    });
  }

  chargeCard(tokenId: string, amount: number) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.storageProvider.getToken().then(
        (result) => {
          this.token = atob(result);
          this.postRequest('/stripe/charge?id_token='+this.token, {
            token: tokenId,
            amount: amount
          }).subscribe(
            (res) => {
              console.log(res);
              if (res['success']){ resolve(res['success']); }else{ this.handleError(res); reject(res); }
            },
            (err) => {
              this.handleError(err); reject(err);
            }
          )
        },
        (err) => {
          console.error(err);
        }
      );
    });
  }


}
