import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Platform, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/map';

import { HttpProvider } from '../http/http';
import { StorageProvider, UserData, EventData } from '../../providers/storage/storage';
import { Storage } from '@ionic/storage';


@Injectable()
export class AuthProvider {
  public httpProvider: HttpProvider;
  public storageProvider: StorageProvider;

  constructor(
    private fbAuth: AngularFireAuth,
    private fbDb: AngularFireDatabase,
    private platform: Platform,
    private fb: Facebook,
    private googlePlus: GooglePlus,
    public http: Http,
    public storage: Storage,
    public alertCtrl: AlertController
  ) {
    this.storageProvider = new StorageProvider(http, storage);
    this.httpProvider = new HttpProvider(http, storage, fbAuth, fbDb, alertCtrl);
  }


  /**
   * Sign in with facebook
   */
  signInWithFacebook(): Promise<firebase.User> {
    return new Promise((resolve, reject) => {
      if (this.platform.is('cordova')) {
        return this.fb.login(['email', 'public_profile']).then(res => {
          const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
          return firebase.auth().signInWithCredential(facebookCredential).then((res) => {
            resolve(res);
          }, reject);
        }, reject);
      } else {
        return this.fbAuth.auth
          .signInWithPopup(new firebase.auth.FacebookAuthProvider())
          .then(res => {
            resolve(res.user);
          }, reject);
      }
    })
  }

  /**
   * Sign In with Google
   */
  signInWithGoogle(): Promise<firebase.User> {
    return new Promise((resolve, reject) => {
      if (this.platform.is('cordova')) {
        this.googlePlus.login({
          webClientId: '81627619045-811d7vrpjc2rru27diiqhalrv1h1h7jg.apps.googleusercontent.com',
          offline: true
        }).then((res) => {
          const googleCredential = firebase.auth.GoogleAuthProvider.credential(res.idToken);
          return firebase.auth().signInWithCredential(googleCredential).then((res) => {
            resolve(res);
          }, reject);
        }, reject)
      } else {
        return this.fbAuth.auth
          .signInWithPopup(new firebase.auth.GoogleAuthProvider())
          .then((res) => {
            resolve(res.user);
          }, reject);
      }
    });
  }

  /**
   * Sign in with Email and Password
   * @param email Email Address
   * @param pass Password
   */
  signInWithEmail(email: string, pass: string): firebase.Promise<any>  {
    let credential = firebase.auth.EmailAuthProvider.credential(email, pass);
    return this.fbAuth.auth.signInWithCredential(credential)
  }

  /**
   * Sign up with Email and Password
   * @param email Email Address
   * @param pass Password
   */
  signUpWithEmail(email: string, pass: string): firebase.Promise<any>  {
    return this.fbAuth.auth.createUserWithEmailAndPassword(email, pass);
  }

  /**
   * Get User Data
   * @param accessToken Firebase oAuth Access Token
   */
  getUserData(accessToken: string): Promise<UserData> {
    return new Promise((resolve, reject) => {
      this.httpProvider.signIn(accessToken).then(
        (result) => {
          this.storageProvider.setToken(result['token']).then(() => {
            resolve(result['user']);
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  /**
   * Set User Data
   * @param uid Firebase User ID
   * @param userData User Data
   */
  setUserData(accessToken: string, userData: any): Promise<UserData> {
    return new Promise((resolve, reject) => {
      const _data = this.httpProvider.requestSubmitUserData(accessToken, userData).subscribe(
        result => {
          _data.unsubscribe();
          this.storageProvider.setToken(result['data']['token']).then(() => {
            resolve(result['data']['user']);
          });
        },
        err => {
          reject(err['message']);
        },
        () => {
          console.log('setUserData completed');
        }
      );
    });
  }


  linkAccount() {

  }

  forgotPassword(email: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fbAuth.auth.sendPasswordResetEmail(email).then(
        (result) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        }
      )
    });
  }

}



// WEBPACK FOOTER //
// ./src/providers/auth/auth.ts
