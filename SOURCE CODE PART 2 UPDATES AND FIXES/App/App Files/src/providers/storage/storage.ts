import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

export class AllEvents {
  future: [EventData];
  past: [EventData];
}

export class ContactData {
  user?: number;
  name: string;
  email?: string;
  phone?: string;
}

export class NotificationData {
    id: number;
    avatar?: string;
    title: string;
    message: string;
    status: string;
    timestamp: number;
}

export class EventPackageData {
  name: string;
  remaining_balance: number;
  used_balance: number;
}

export class EventConfigData {
  packages: [PackageData];
  addons: [AddonData];
  roles?: [{
    id: number;
    name: string;
  }];
}

export class AddonData {
  id?: string;
  title?: string;
  price?: number;
  name?: string;
  value?: string;
  selected?: boolean = false;
}

export class PackageData {
    id?: number;
    title?: string;
    price?: number;
}

export class OrderData {
  date: number;
  details: {
    package: PackageData;
    addons: [AddonData]
  };
  price: number;
  title: string;
}

export class EventDetailData {
  details: {
    id: number;
    date: number;
    package: EventPackageData;
    participants: [UserData];
    photo: string;
    premium: [AddonData];
    title: string;
    type: string;
    status?: any;
    hasResponded?: any;
    privacy?: any;
  };
  guest: [any];
  participants: [any];
  users: [any];
}

export class EventData {
  id?: number;
  token?: string;
  photo?: string;
  title: string;
  date: number;
  time?: number;
  package?: PackageData;
  addons?: AddonData[];
  participants?: [UserData];
  type?: string;
  status?: any;
  event_status?: any;
}

export class OfficiantData {
  prefix: string;
  description: string;
  questions: [{
    question: string,
    answer?: string
  }];
  fee: number;
  availability: [{
    day: boolean
  }];
}

export class UserData {
  personal: {
    id?: number;
    avatar?: string;
    dob?: number;
    email?: string;
    location?: {
      address?: string;
      city?: string;
      state?: string;
      zip?: number;
    };
    name: {
      first?: string;
      last?: string;
      full: string;
    };
    phone?: number;
    privacy?: {
      notifications?: {
        push?: boolean;
        email?: boolean;
      }
    };
  };
  events?: [EventData];
  officiant?: OfficiantData
}

@Injectable()
export class StorageProvider {

  constructor(public http: Http, public storage: Storage) {

  }

  clearAll(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.clear();
      resolve(true);
    });
  }

  setSeenWalkthrough(seen: boolean): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.set('walkthrough', seen);
      resolve(true);
    });
  }

  getSeenWalkthrough(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.storage.get('walkthrough').then((val) => {
        resolve(val);
      }, reject);
    });
  }

  setToken(token: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.set('token', token);
      resolve(true);
    });
  }

  getToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      this.storage.get('token').then((val) => {
        resolve(val);
      }, reject);
    });
  }

  /**
   * Sets the current logged in users profile
   * @param  {UserData} user User instance - UserData class
   * @return {Promise}              Javascript Promise is returned
   */
  setProfile(user: UserData) {
    return new Promise((resolve, reject) => {
      this.storage.set('user', JSON.stringify(user));
      resolve(true);
    });
  }

  /**
   * Gets the current logged in users profile
   * @return {Promise<UserData>}  Javascript promise is returned, resolves to UserData class
   */
  getProfile(): Promise<UserData>{
    return new Promise((resolve, reject) => {
      this.storage.get('user').then((val) => {
        let user = JSON.parse(val);
        resolve(user);
      }, reject);
    });
  }

}
