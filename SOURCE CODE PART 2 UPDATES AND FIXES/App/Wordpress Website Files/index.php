<?php
   session_start();

   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <title>Web Wed Mobile - Marriage Ceremonies Shared Live.</title>
      <!-- Search engines -->
      <meta name="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
      <!-- Google Plus -->
      <meta itemprop="name" content="Web Wed Mobile - Live stream weddings.">
      <meta itemprop="description" content="When love can't wait. Web Wed Mobile is the first of it's kind to allow not only guest to be in remote locations, but the officiate, witnesses and even your fiancee. A memorable affordable way to exchange vows and share your special moment to the world.">
      <meta itemprop="image" content="images/social_share.png">
      <!-- Mobile Specific Meta -->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <!--[if IE]>
      <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>
      <![endif]-->
      <?php require_once 'includes/Mobile_Detect.php'; $detect = new Mobile_Detect;?>
      <!--web wed style-->
      <link rel="stylesheet" id="owl.carousel-css" href="http://webwedmobile.com/blog/wp-content/themes/bride/assets/css/owl.carousel.css?ver=1.0.0" type="text/css" media="all">
      <link rel="stylesheet" id="slick-css" href="http://webwedmobile.com/blog/wp-content/themes/bride/assets/css/slick.css?ver=1.0.0" type="text/css" media="all">
      <link rel="stylesheet" id="venobox-css" href="http://webwedmobile.com/blog/wp-content/themes/bride/assets/venobox/venobox.css?ver=1.0.0" type="text/css" media="all">
      <link rel="stylesheet" id="animate-css" href="http://webwedmobile.com/blog/wp-content/themes/bride/assets/css/animate.css?ver=1.0.0" type="text/css" media="all">
      <link rel="stylesheet" id="bride-slider-css" href="http://webwedmobile.com/blog/wp-content/themes/bride/assets/css/slider.css?ver=1.0.0" type="text/css" media="all">
      <link rel="stylesheet" id="layers-google-fonts-css" href="//fonts.googleapis.com/css?family=Lato%3Aregular%2Citalic%2C700%2C100%2C100italic%2C300%2C300italic%2C700italic%2C900%2C900italic%7CPoppins%3Aregular%2C700%2C300%2C400%2C500%2C600&amp;ver=2.0.2" type="text/css" media="all">
      <link rel="stylesheet" id="layers-framework-css" href="http://webwedmobile.com/blog/wp-content/themes/layerswp/assets/css/framework.css?ver=2.0.2" type="text/css" media="all">
      <link rel="stylesheet" id="layers-components-css" href="http://webwedmobile.com/blog/wp-content/themes/layerswp/assets/css/components.css?ver=2.0.2" type="text/css" media="all">
      <link rel="stylesheet" id="layers-responsive-css" href="http://webwedmobile.com/blog/wp-content/themes/layerswp/assets/css/responsive.css?ver=2.0.2" type="text/css" media="all">
      <link rel="stylesheet" id="layers-icon-fonts-css" href="http://webwedmobile.com/blog/wp-content/themes/layerswp/assets/css/layers-icons.css?ver=2.0.2" type="text/css" media="all">
      <link rel="stylesheet" id="layers-style-css" href="http://webwedmobile.com/blog/wp-content/themes/bride/style.css?ver=2.0.2" type="text/css" media="all">
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">
      <link rel="stylesheet" href="css/main.css">
      <!-- Fonts -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
      <link href="https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script" rel="stylesheet" type="text/css">
      <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
      <script src="js/jquery.browser.js"></script>
      <style>
        .fixed {
          position:fixed;
          top:0;
          z-index:99999;
          background-color: rgba(243, 243, 243, 1) !important;
        }
        .fixed2 {
          position:fixed;
          top:0;
          z-index:99999;
        }
      </style>
   </head>
   <body>
      <div id="barwrap" style="display:none;">
         <div class="bar"> <span id="head-image"><i class="fa fa-heart color_navy"></i></span> <span id="text">
            <?php if($detect->isMobile()){
               echo "You will need to download the mobile application, or view this site from a desktop browser for streaming.";
               } else {
               echo "This site is running as a Beta Application. Some functionality has been disabled for demonstration purpose.";
               } ?>
            </span> <span id="otherimg"></span> <span id="ok"><a href="#">X</a></span>
         </div>
      </div>
      <!-- HEADER START -->
      <div class="header-secondary sticky-scroll-box2 content-small invert">
         <div class=" clearfix">
            <nav class="pull-left">
               <ul id="menu-top-left-menu" class="nav nav-horizontal">
                  <li id="menu-item-1068" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1068"><a href="tel:+18559329333">Call Us : +1 (855) WEB-WEDD</a></li>
               </ul>
            </nav>
            <nav class="pull-right">
               <ul id="menu-top-right-menu" class="nav nav-horizontal">
                  <li id="menu-item-1252" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1252"><a href="#">718 7th St NW, Washington DC</a></li>
                  <li id="menu-item-1070" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1070"><a href="mailto:info@webwedmobile.com">info@webwedmobile.com</a></li>
                  <li id="menu-item-1254" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1254"><a href="#">8:00am – 8:00pm All days of week</a></li>
               </ul>
            </nav>
         </div>
      </div>
      <section class="header-site header-sticky sticky-scroll-box content header-left">
         <div class=" header-block">
            <div class="logo">
               <a href="http://webwedmobile.com/" class="custom-logo-link" rel="home" itemprop="url"><img width="250" height="93" src="http://webwedmobile.com/blog/wp-content/uploads/2018/02/cropped-aJ6cre41z.png" class="custom-logo" alt="WebWedMobile" itemprop="logo"></a>
               <div class="site-description">
               </div>
            </div>
            <nav class="nav nav-horizontal">
               <ul id="menu-main-menu-1" class="menu">
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-1365"><a href="http://webwedmobile.com/">Home</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1499"><a href="/blog/blog/">Blog</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1499"><a href="/blog/courts/">Courts</a></li>
                  <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1496"><a href="/app/">Account</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1688"><a href="/blog/faqs/">FAQs</a></li>
                  <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1364"><a href="/blog/contact-us/">Contact Us</a></li>
               </ul>
               <a class="responsive-nav" data-toggle="#off-canvas-right" data-toggle-class="open">
               <span class="l-menu"></span>
               </a>
            </nav>
         </div>
      </section>
      <!-- HEADER END -->
      <div id="video_nav_tweet_wrapper" class="wrapper" style="height:80vh; background-image: url('http://webwedmobile.net/images/login/webwedring.svg');
         background-repeat: no-repeat;
         background-color: #c9d3de !important;
         background-size: 15% !important;
         background-position: 50% bottom !important;">
         <div class="wrapper">
            <!-- Action bars that only appear on mobile at the right time -->
            <div class="action-bar hidden"> <span class="pull-left">Return Comments</span> <a href="#"> <em id="chatArrow" class="fa fa-arrow-circle-o-up fa-rotate-180 pull-right"></em> </a> </div>
            <!-- /.background-carousel end -->
            <div id="video_container" class="master_video_margin" style="margin-top:2% !important;">
               <div class="row row-one">
                  <div id="you" class="vide-cell">
                     <video autoplay  loop id='youVideoElement' ><source src='demo_videos/groom.mp4' type='video/mp4'> </video>
                  </div>
                  <div id="yours" class="vide-cell">
                     <video autoplay  loop id='yoursVideoElement'><source src='demo_videos/bride.mp4' type='video/mp4'> </video>
                  </div>
               </div>
               <div id="officiate" class="vide-cell">
                  <video autoplay  loop id='officalVideoElement' ><source src='demo_videos/officiate.mp4' type='video/mp4'> </video>
               </div>
               <div class="row row-two">
                  <div id="witness1" class="vide-cell">
                     <video autoplay  loop id='witness1VideoElement'><source src='demo_videos/witness1.mp4' type='video/mp4'> </video>
                  </div>
                  <div id="witness2" class="vide-cell">
                     <video autoplay  loop id='witness2VideoElement'><source src='demo_videos/witness2.mp4' type='video/mp4'> </video>
                  </div>
               </div>
            </div>
            <!--WebWed Controls-->
            <div id="webwed_control" style="height: auto !important;display:none;">
               <ul class="list-inline">
                  <li> <a href="#" id="chat" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Chat with other guests." onClick="toggle_visibility('chatWindow')"> <img src="images/tab-chat-white.svg" width="35" class="webwed_icon"onClick="toggle_visibility('chatWindow')"> </a></li>
                  <li> <a href="#" id="gift" data-toggle="tooltip" onClick="showGiftRegistry()" class="webwed_icon top disabled" data-placement="top"  data-original-title="Public registry."> <img src="images/tab-gift-white.svg" width="35" class="webwed_icon"> </a></li>
                  <li>
                     <a href="#" id="wwHeart" class="webwed_icon">
                        <div class="icon-heart"> <i  id="forever_heart" class="material-icons blue"> </i> <i  id="together_heart" class="material-icons yellow"> </i> <i  id="whereever_heart" class="material-icons fav disabled"> </i> </div>
                     </a>
                  </li>
                  <li> <a href="#" id="event" class="webwed_icon top disabled" data-placement="top" data-toggle="tooltip" data-original-title="Download To Calendar"> <img src="images/tab-event-white.svg" width="35" class="webwed_icon"> </a></li>
                  <li> <a href="#" id="sharelink" data-toggle="modal" data-target="#wwSocialShare" class="disabled"> <img src="images/tab-link-white.svg" width="35" class="webwed_icon"> </a> </li>
               </ul>
            </div>
            <!--Chat Dialog-->
            <div class="chat_window" id="chatWindow">
               <ul class="messages">
               </ul>
               <div class="bottom_wrapper clearfix">
                  <div class="message_input_wrapper">
                     <input class="message_input" placeholder="Share a message..." id="messagetoshare">
                  </div>
                  <div class="send_message" id="share">
                     <div class="icon"></div>
                     <div class="text">Share</div>
                  </div>
               </div>
            </div>
            <div class="message_template">
               <li class="message">
                  <div class="avatar"></div>
                  <div class="text_wrapper">
                     <div class="text"></div>
                  </div>
               </li>
            </div>
            <!--Terminate Chat-->
         </div>
         <!-- wrapper end -->
      </div>
      <!-- video_nav_tweet_wrapper end -->

      <!-- .rsvpLink start -->
      <div class="bg_heart">
         <section id="rsvpLink" class="rsvpLink" style="background-color:#fafafa;">
            <div class="color-overlay">
               <style>
                  .border_frame {
                  border: 9px double #3c8dbc !important;
                  }
                  .blue {
                  color: rgb(10, 58, 112);
                  }
               </style>
               <div class="row">
                  <h2 class="color_teal text-center" id="story-title" style="color: rgb(204, 81, 102);">Download our app today!</h2>
               </div>
               <div class="row">
                  <div class="col-sm-3 col-sm-offset-3">
                  	 <a href="https://itunes.apple.com/us/app/webwed-mobile/id1086176524?mt=8"><img src="https://www.halstead.com/app/assets/img/creative/AppStore.png" style="width:230px;"></a>
                  </div>
                  <div class="col-sm-3">
                      <a href="https://play.google.com/store/apps/details?id=com.webwed.android&amp;hl=en"><img src="https://www.ft.com/__origami/service/image/v2/images/raw/https%253A%252F%252Fwww.ft.com%252F__assets%252Fcreatives%252Ftour%252Fapps%252Fgoogle-play-badge-3x.png?source=next-tour-page" style="margin-top:5px; width:230px;"></a>
                  </div>
               </div><br/>
               <div class="row">
                  <div class="col-sm-4 text-center" style="color:white;">
                     <div class="border_frame text-center">
                        <img src="https://static.wixstatic.com/media/1eadcf_d834756d448f4cc080be89376c5bc821~mv2.png/v1/fill/w_72,h_72,al_c,usm_0.66_1.00_0.01/1eadcf_d834756d448f4cc080be89376c5bc821~mv2.png"/>
                        <h1 class="blue">100% Legal</h1>
                        <p class="blue">Apply for a Marriage License, Upload Required Documents, & Same Day Approval</p>
                     </div>
                  </div>
                  <div class="col-sm-4 text-center" style="color:white;">
                     <div class="border_frame text-center">
                        <img src="https://static.wixstatic.com/media/1eadcf_53f0fc4b2fb747ff88827f95032fbf8a~mv2.png/v1/fill/w_76,h_76,al_c,usm_0.66_1.00_0.01/1eadcf_53f0fc4b2fb747ff88827f95032fbf8a~mv2.png"/>
                        <h1 class="blue">Archives</h1>
                        <p class="blue">Save your event even after its happened. You can download an archive at any point.</p>
                     </div>
                  </div>
                  <div class="col-sm-4 text-center" style="color:white;">
                     <div class="border_frame text-center">
                        <img src="https://static.wixstatic.com/media/1eadcf_548a45e3e4ff4a7f85a427adc43a1a74~mv2.png/v1/fill/w_75,h_75,al_c,usm_0.66_1.00_0.01/1eadcf_548a45e3e4ff4a7f85a427adc43a1a74~mv2.png"/>
                        <h1 class="blue">E-Vites</h1>
                        <p class="blue">Send a custom 30 second video inviation to your friends and family. Add a WebWed Flair to any event!</p>
                     </div>
                  </div>
               </div>
               <br/>
               <div class="row">
                  <div class="col-sm-6 text-center" style="color:white;">
                     <div class="border_frame text-center">
                        <img src="https://static.wixstatic.com/media/1eadcf_9dd26bcbdceb4dd09fd8e2de5aa9204a~mv2.png/v1/fill/w_76,h_76,al_c,usm_0.66_1.00_0.01/1eadcf_9dd26bcbdceb4dd09fd8e2de5aa9204a~mv2.png"/>
                        <h1 class="blue">Stream Special Moments!</h1>
                        <p class="blue">Stream any event including birthdays, celebrations of life, proposals or any type of event you can imagine.</p>
                     </div>
                  </div>
                  <div class="col-sm-6 text-center" style="color:white;">
                     <div class="border_frame text-center">
                        <img src="https://static.wixstatic.com/media/1eadcf_dd9db948a1904cb0818a9a9addc45ddd~mv2.png/v1/fill/w_73,h_73,al_c,usm_0.66_1.00_0.01/1eadcf_dd9db948a1904cb0818a9a9addc45ddd~mv2.png"/>
                        <h1 class="blue">Snapchat filters!</h1>
                        <p class="blue">Snapchat your love with WebWed! Custom filters for your special event. Available based on your events location.</p>
                     </div>
                  </div>
               </div>
               <br/>
            </div>
         </section>
      </div>
      <!-- .rsvpLink end -->
      <!-- banner end -->
      <!-- story start -->
      <section id="story" class="story lightsmoke">
         <div class="container">
            <div>
               <div class="col-sm-12 text-center">
                  <div class="wow animated fadeInUp text-center">
                     <h3 class="color_navy">Together. Wherever. Whenever. Forever.</h3>
                  </div>
                  <!-- /.heading end -->
               </div>
            </div>
            <div class="row">
               <h2 class="color_teal text-center" id="story-title">What love brought together, no one can keep apart.</h2>
               <div class="video-container1">
                  <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/7KErsFY54To?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
               </div>
            </div>
            <br/><br/>
            <div>
               <div class="col-sm-12 text-center">
                  <div class="wow animated fadeInUp text-center">
                     <h3 class="color_navy">Got More Questions About Web Wed?​</h3>
                  </div>
                  <!-- /.heading end -->
               </div>
            </div>
            <div class="row">
               <h2 class="color_teal text-center" id="story-title" style="color: rgb(204, 81, 102);">Check Out Our Tech Love Story When "Roaming-0 Meets Emoji-et"</h2>
            </div>
         </div>
      </section>
      <!-- story end -->
      <!-- people start --><!-- people end -->
      <!-- event start -->
      <section id="event" class="event lightsmoke" style="background-color:#3c8dbc;">
         <div class="container" style="overflow:visible;">
           <div class="row">
             <div class="col-sm-6">
                <img src="http://webwedmobile.com/images/jc_randy.jpg" style="width:100%;height:100%;">
             </div>
             <div class="col-sm-4 col-sm-offset-1">
                <br><h2 style="color:white;text-align:center;line-height: 50px;">Bankable Love</h2><br>
                <a href="http://webwedmobile.com/blog/our-story" class="btn btn-lg btn-block" style="background-color:#fff" data-original-title="" title="">LEARN MORE</a>
             </div>
          </div>
         </div>
      </section>
      <!-- event end -->
      <section id="story" class="story lightsmoke hidden-xs hidden-sm">
         <div class="container">
         <div>
            <div class="col-sm-12 text-center">
               <div class="wow animated fadeInUp text-center">
                  <h3 class="color_navy">WebWed Moments</h3>
               </div>
               <!-- /.heading end -->
            </div>
         </div>
         <div class="row">
            <h2 class="color_teal text-center" id="story-title">Beautiful WebWed powered moments</h2>
            <div class="video-container1">
               <div class="row">
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfFKKmshOd2/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfEu09SBlef/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BfEutYiBVC7/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfEum-nh1iK/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <script async defer src="//www.instagram.com/embed.js"></script>
               </div>
               <br/>
               <div class="row">
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfJ_pO-hayI/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:35.714285714285715% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfI-eaxByA7/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfFKcbyBWSF/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
                  <div class="col-sm-3" style="min-height:320px;">
                     <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfDtyg4BGHn/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
                        <div style="padding:8px;">
                           <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;">
                              <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div>
                           </div>
                        </div>
                     </blockquote>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- contact start -->
      <section id="contact" class="contact">
         <div class="container">
            <div class="row">
               <h3 class="color_navy" id="contact-title" align="center">Free <span class="color_navy wow pulse" data-wow-iteration="infinite" data-wow-duration="1200"><i class="fa fa-heart"></i></span> Hashtag Generator</h3>
               <div class="row">
                  <div class="col-sm-12">
                     <div class="contact-block">
                        <h5><strong>Share Your Wedding In Style</strong><br>
                        </h5>
                        <form class="contact_form" role="form" onsubmit='event.preventDefault();'>
                           <div class="row">
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="contactnamefield" name="name" placeholder="Your Full Name" required="required">
                                 </div>
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="contactnamefield" name="name" placeholder="Your Spouces Full Name" required="required">
                                 </div>
                                 <!-- <div class="form-group">
                                    <input type="text" class="form-control" id="contactemailfield" name="name" placeholder="Enter your email address" required="required">
                                    </div> -->
                              </div>
                              <div class="col-sm-6">
                                 <div class="form-group">
                                    <h5><strong>#FreeWeddings4Life</strong></h5>
                                 </div>
                                 <div class="form-group">
                                    <input type="submit" name="submit" onclick="submitcontact();" class="btn btn-lg btn-block" value="Generate Tags" style="background-color:#0e3562">
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                     <!-- /.contact-block end -->
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- contact end -->
      <!-- footer start -->
      <footer style="background-image: url('http://webwedmobile.com/images/splash.jpg');
         background-size: 30% !important;
         background-repeat: no-repeat;
         background-position: 0 bottom">
         <div>
            <div class="container">
               <div class="row">
                  <div class="col-sm-12 text-center"> <span class="footer-logo"> <img src="http://webwedmobile.net/images/login/webwedlogo.svg" alt="WebWed" width="250"> </span> </div>
                  <h2 class="color_teal medium">When Love Can't Wait.</h2>
                  <div class="col-sm-offset-3 col-sm-6">
                    <a href="https://www.facebook.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-twitter"></i></a>
                    <a href="https://instagram.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-instagram"></i></a>
                    <a href="https://pinterest.com/webwed1906" class="wow animated rollIn"><i class="fa fa-pinterest-p"></i></a>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12 text-center">
                     <ul class="nav nav-pills">
                        <li role="presentation"><a style="border:0;" href="/blog/our-story">About</a></li>
                        <li role="presentation"><a style="border:0;" href="/blog/media">Media</a></li>
                        <li role="presentation"><a style="border:0;" href="/blog/contact-us">Contact Us</a></li>
                        <li role="presentation"><a style="border:0;" href="/blog/legal">Legal Resources</a></li>
                     </ul>
                     <!-- footer nav end -->
                  </div>
                  <div class="col-sm-12 text-center">
                     <p class="copyright">
                        &copy; 2012&#x2011;<?php echo date("Y"); ?>  WebWed, INC. All rights reserved. Provisional Patent Awarded. Service available only within the contiguous United States. A state issued marriage license is required to use this service. Please note that it is the registers/users responsibility to verify and abide by state,county,and armed forces marriage laws prior to using this service.   Use of this website, app and any mentioned services constitutes that you agree to our terms of service and privacy policy.  ( NO FRAUD/FORGED DOCUMENTS ,NON CONSENTED, UNAUTHORIZED, UNDER AGE, OR ILLEGAL MARRIAGES PERMITTED/VIOLATION WILL BE REPORTED TO THE APPROPRIATE LAW ENFORCEMENT AGENCY IF APPLICABLE)Web or App related technical issues can be sent to <a href="mailto:techsupport@webwedmobile.com">techsupport@webwedmobile.com</a>
                        <br>
                  </div>
               </div>
            </div>
            <div id="go-to-top"> <a href="#banner"><i class="fa fa-angle-up"></i></a> </div>
         </div>
         <!-- /.color-overlay end -->
      </footer>
      <!-- footer end -->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <script src="js/modernizr.js"></script>
      <script src="js/background.js"></script>
      <script src="js/index.js"></script>
      <script src="assets/js/smoothscroll.js"></script>
      <script src="assets/js/jquery.fitvids.js"></script>
      <script src="assets/js/jquery.nav.js"></script>
      <!-- <script src="assets/js/modal-center.js"></script> -->
      <script src="assets/js/jquery.sticky.js"></script>
      <!-- <script type="text/javascript" src="https://rawgithub.com/nwcell/ics.js/master/ics.deps.min.js"></script> -->
      <script src="js/webwed_webview.js"></script>
      <script type="text/javascript" src="js/social-share-kit.min.js"></script>
      <script>
         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

         ga('create', 'UA-74133839-1', 'auto');
         ga('send', 'pageview');

         // SocialShareKit.init();

         $(document).ready(function () {
         var top = $('.sticky-scroll-box').offset().top;

         $(window).scroll(function (event) {
           var y = $(this).scrollTop();
           if (y >= top){
             $('.sticky-scroll-box').addClass('fixed');
           }else{
             $('.sticky-scroll-box').removeClass('fixed');
             $('.sticky-scroll-box').width($('.sticky-scroll-box').parent().width());
           }
          });
         });
      </script>
      <!-- Start of webwed Zendesk Widget script -->
      <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="webwed.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
         /*]]>*/
      </script>
      <!-- End of webwed Zendesk Widget script -->
   </body>
</html>
