<?php session_start(); ?>

<!doctype html>
<html style="background-color: #023a7b;">
<head>
<meta charset="UTF-8">
<title>Web Wed Mobile - Browser Not Supported</title>
<!-- Search engines -->

<!-- Google Plus -->

<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Oranienbaum|Roboto:400,300|Roboto+Condensed|Clicker+Script" rel="stylesheet" type="text/css">

<!--WebWed Icons and Style Sheets -->

<!--Fav and touch icons-->
<link rel="apple-touch-icon" sizes="57x57" href="images/icon/favicon//apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/icon/favicon//apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/icon/favicon//apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/icon/favicon//apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/icon/favicon//apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/icon/favicon//apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/icon/favicon//apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/icon/favicon//apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/icon/favicon//apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/icon/favicon//android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon//favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon//favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon//favicon-16x16.png">

<!--web wed style-->
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/join.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


</head>
<body>
<div id="wrapper">
<div class="container"><br>
<h2><img src="images/web-wed-logo-white.svg"/></h2>
<h2>Browser Not Supported</h2>
<p>Web Wed Mobile is a great way to share a special moment with a private group, or the entire world. Currently Microsoft Internet Explorer is not supported by some of the technology used on our site. </p>
<p>You can access this site by using Google Chrome, Safari or Firefox.</p>
  
  

  
  </div>
</div>
</body>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 

 
</html>