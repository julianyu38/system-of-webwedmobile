<?php
/**
 * Standard blog index page
 *
 * @package Layers
 * @since Layers 1.0.0
 */

get_header(); ?>

<div class="container content-main archive standard clearfix blog-pages">
    <?php if( have_posts() ) : ?>
	<div class="grid blog_masonry">

        <?php while( have_posts() ) : the_post(); ?>
			<div class="column span-4 sk grid_item">
				<?php get_template_part( 'partials/content' , 'blog' ); ?>
			</div>
        <?php endwhile; // while has_post(); ?>
			     
	</div>
	<div class="extra-pagination">
		<?php the_posts_pagination(); ?>
	</div>	
    <?php endif; // if has_post() ?>	
</div>

<?php get_footer();