<?php



/**

 * Your theme name Custom Functions

**/



//Setup Constants.

define( 'BRIDE_VERSION', '1.0.0' );

require_once( trailingslashit( get_stylesheet_directory() ). '/includes/breadcrumb.php' );
require_once( trailingslashit( get_stylesheet_directory() ). '/includes/custom-footer.php' );
require_once( trailingslashit( get_stylesheet_directory() ). '/includes/class-activation.php' );
require_once( trailingslashit( get_stylesheet_directory() ). '/includes/demo-import.php' );
require_once( trailingslashit( get_stylesheet_directory() ). '/includes/presets.php' );






//Localize

if ( ! function_exists( 'bride_localize' ) ) {

	

	function bride_localize() {

		

		load_child_theme_textdomain( 'bride', get_stylesheet_directory() . '/languages' );

	

	}

	

}

add_action( 'after_setup_theme', 'bride_localize' );



/* Set Font and Theme Defaults

** http://docs.layerswp.com/reference/layers_customizer_defaults/

*  Since 1.0

*/

add_filter( 'layers_customizer_control_defaults', 'bride_customizer_defaults' );



if( ! function_exists( 'bride_customizer_defaults' ) ) {

	function bride_customizer_defaults( $defaults ){



	 	$defaults = array(

	       'body-fonts' => 'Poppins',

	       'form-fonts' => 'Poppins',

	       'header-menu-layout' => 'header-logo-left',

	       'header-background-color' => '#FFFFFF',

	       'site-accent-color' => '#A499C9',

	       'header-width' => 'layout-boxed',

	       'header-sticky' => '1',

		   'header-overlay' => '0',

	       'heading-fonts' => 'Vidaloka',

	       'header-sticky-breakpoint' => '400',

	       'footer-sidebar-count' => '0',

		   'footer-background-color' => '#232323',

		   'footer-link-color' => '#FFFFFF',

		   'footer-body-color' => '#FFFFFF'

	 	);



	 return $defaults;

	}

}

	

/**

* Load Styles

*/

 

if( ! function_exists( 'bride_styles' ) ) {	



	function bride_styles() {	

	

		wp_enqueue_style( 'layers-font-awesome',

			get_template_directory_uri() . '/core/assets/plugins/font-awesome/font-awesome.min.css',

			array() 

		);					

		wp_enqueue_style(

			'owl.carousel',

			get_stylesheet_directory_uri() . '/assets/css/owl.carousel.css',

			array(), BRIDE_VERSION

		);

		wp_enqueue_style(

			'slick',

			get_stylesheet_directory_uri() . '/assets/css/slick.css',

			array(), BRIDE_VERSION

		);

		wp_enqueue_style(

			'venobox',

			get_stylesheet_directory_uri() . '/assets/venobox/venobox.css',

			array(), BRIDE_VERSION

		);

		wp_enqueue_style(

			'animate',

			get_stylesheet_directory_uri() . '/assets/css/animate.css',

			array(), BRIDE_VERSION

		);	

		wp_enqueue_style(

			'bride-slider',

			get_stylesheet_directory_uri() . '/assets/css/slider.css',

			array(), BRIDE_VERSION

		);

		

		

		

	}

	

}

add_action( 'wp_enqueue_scripts', 'bride_styles' );



/**

* Load Scripts

*/	

if( ! function_exists( 'bride_scripts' ) ) {

		

	function bride_scripts() {

		

		wp_enqueue_script('modernizr', get_stylesheet_directory_uri() . '/assets/js/vendor/modernizr-2.8.3.min.js', array('jquery'), '', true  );

		wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '', true  );

		wp_enqueue_script('imagesloaded');

		wp_enqueue_script('owl.carousel', get_stylesheet_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '', true  );

		wp_enqueue_script('countdown', get_stylesheet_directory_uri() . '/assets/js/jquery.countdown.min.js', array('jquery'), '', true  );

		wp_enqueue_script('counterup', get_stylesheet_directory_uri() . '/assets/js/jquery.counterup.min.js', array('jquery'), '', true  );

		wp_enqueue_script('query.mixitup', get_stylesheet_directory_uri() . '/assets/js/isotope.pkgd.min.js', array('jquery'), '', true  );

		wp_enqueue_script('venobox', get_stylesheet_directory_uri() . '/assets/venobox/venobox.min.js', array('jquery'), '', true  );

		wp_enqueue_script('waypoints', get_stylesheet_directory_uri() . '/assets/js/waypoints.min.js', array('jquery'), '', true  );

		wp_enqueue_script('wow', get_stylesheet_directory_uri() . '/assets/js/wow.min.js', array('jquery'), '', true  );

		wp_enqueue_script('plugins', get_stylesheet_directory_uri() . '/assets/js/plugins.js', array('jquery'), '', true  );

		wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/assets/js/slick.min.js', array('jquery'), '', true  );

		wp_enqueue_script('scrollUp', get_stylesheet_directory_uri() . '/assets/js/jquery.scrollUp.min.js', array('jquery'), '', true  );

		wp_enqueue_script('bride_theme',	get_stylesheet_directory_uri() . '/assets/js/theme.js', array('jquery'), '', true  );

			

	}	

}

add_action('wp_enqueue_scripts', 'bride_scripts'); 

 

// Add Breadcrumb

if( ! function_exists('bride_breadcrumb') ) {

	function bride_breadcrumb(){

		get_template_part( 'partials/header' , 'page-title' );

	}

}

add_action( 'layers_after_header', 'bride_breadcrumb' );





// image crop

if(!function_exists('bride_theme_image_crop')){

	function bride_theme_image_crop() {

	add_image_size( 'bride_about1_image', 1167, 340, true );

	add_image_size( 'bride_about2_image', 640, 515, true );

	}

}

add_action( 'after_setup_theme', 'bride_theme_image_crop' );


/**

* Welcome Redirect

* http://docs.layerswp.com/how-to-add-help-pages-onboarding-to-layers-themes-or-extensions/

*/

if( ! function_exists('bride_setup') ) {

	

	function bride_setup(){

		

		if( isset($_GET["activated"]) && $pagenow = "themes.php" ) { //&& '' == get_option( 'layers_welcome' )

			update_option( 'layers_welcome' , 1);

			wp_safe_redirect( admin_url('admin.php?page=layers-child-get-started'));

		}

	}

}

add_action( 'after_setup_theme', 'bride_setup', 20 );



// Unset page templates inherited from the parent theme.

add_filter( 'theme_page_templates', 'bride_theme_remove_page_template' );



if( ! function_exists('bride_theme_remove_page_template') ) {

 

	function bride_theme_remove_page_template( $page_templates ) {

	    

		unset( $page_templates['template-left-sidebar.php'] );

		unset( $page_templates['template-right-sidebar.php'] );

		unset( $page_templates['template-both-sidebar.php'] );

		return $page_templates; 

	 

	}

 

}



/**

 * Prints Comment HTML

 *

 * @param    object          $comment        Comment objext

 * @param    array           $args           Configuration arguments.

 * @param    int             $depth          Current depth of comment, for example 2 for a reply

 * @echo     string                          Comment HTML

 */ 

if( !function_exists( 'bride_comment' ) ) {

	

	function bride_comment($comment, $args, $depth) {

		$GLOBALS['comment'] = $comment;?>

	

		<li>

		<div class="grid comments-nested push-top">



			<div <?php comment_class( 'content well' ); ?> id="comment-<?php comment_ID(); ?>">

				

				<div class="avatar push-bottom clearfix">

					<?php edit_comment_link(esc_html__('(Edit)' , 'bride' ),'<small class="pull-right">','</small>') ?>

					<a class="avatar-image" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">

						<?php echo get_avatar($comment, $size = '70'); ?>

					</a>

					

					<div class="avatar-body">

						<h5 class="avatar-name"><?php echo get_comment_author_link(); ?></h5>

						<small><?php printf(esc_html__('%1$s at %2$s' , 'bride' ), get_comment_date(),  get_comment_time()) ?></small>

					</div>

				</div>



				<div class="copy small">

					<?php if ($comment->comment_approved == '0') : ?>

						<em><?php esc_html_e('Your comment is awaiting moderation.' , 'bride' ) ?></em>

						<br />

					<?php endif; ?>

					<?php comment_text() ?>

					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>

				</div>

			</div>

		</div>



	<?php }

}



/**

 * Add color styling from theme

 */



 if( !function_exists( 'bride_styles_method' ) ) {

function bride_styles_method() {

		wp_enqueue_style(

			'bride-breadcrumbs',

			get_stylesheet_directory_uri() . '/assets/css/bride-breadcrumbs.css'

		);

			

	$bread_padding_top  = get_post_meta( get_the_ID(),'_bride_bread_padding_top', true );

	$bread_padding_bottom  = get_post_meta( get_the_ID(),'_bride_bread_padding_bottom', true );

	$page_page_image  = get_post_meta( get_the_ID(),'_bride_pageimagess', true );

	$page_background  = get_post_meta( get_the_ID(),'_bride_page_background', true );

	$Single_post_image  = get_post_meta( get_the_ID(),'_bride_post_image', true ); 

	$text_color_page  = get_post_meta( get_the_ID(),'_bride_text_color_page', true );

	$text_current_color_page  = get_post_meta( get_the_ID(),'_bride_current_color_page', true );

	$text_align_page  = get_post_meta( get_the_ID(),'_bride_page_text_align', true );

	$page_text_transform  = get_post_meta( get_the_ID(),'_bride_page_text_transform', true );

	$text_size_page  = get_post_meta( get_the_ID(),'_bride_text_size_page', true );



	if( $bread_padding_top ){

		$bread_padding_top= $bread_padding_top;

	}

	if( $bread_padding_bottom ){

		$bread_padding_bottom= $bread_padding_bottom;

	}

	if( $page_page_image ){

		$page_page_image= $page_page_image;

	} 

	if( $page_background ){

		$page_background= $page_background;

	} 



	if( $Single_post_image ){

		$post_banner= $Single_post_image;

	} 

	if (isset( $text_color_page ) && !empty( $text_color_page)){

		$text_color_page= $text_color_page;

	}

	if (isset( $text_current_color_page ) && !empty( $text_current_color_page)){

		$text_current_color_page= $text_current_color_page;

	}



	if (isset( $text_align_page ) && !empty( $text_align_page)){

		$text_align_page = $text_align_page;

	}

	if (isset( $page_text_transform ) && !empty( $page_text_transform)){

		$page_text_transform = $page_text_transform;

	}

	if (isset( $text_size_page ) && !empty( $text_size_page)){

		$text_size_page = $text_size_page;

	}

		  



        $custom_css = "

                .breadcrumb-area,

				.breadcrumb-area.single-page{										

					background: {$page_background  };

					background-image:url({$page_page_image});  

					padding-top:{$bread_padding_top}px;

					padding-bottom:{$bread_padding_bottom}px;						

                }		

				.breadcrumb-padding.pages-p{										

					text-align: {$text_align_page  };

					text-transform:{$page_text_transform};					

                }

				.breadcrumb-list ul,

				.breadcrumb-list li,

				.breadcrumb-list li a

				{					

					font-size:{$text_size_page}px;

				}				

				.breadcrumb-list ul,

				.breadcrumb-list li,

				.breadcrumb-list li a,

				.breadcrumb-list a,

				.breadcrumb-list a

				{

					color: {$text_color_page};

				}				

				.breadcrumb-list li:last-child

				{

					color: {$text_current_color_page};

				}";

				

				

        wp_add_inline_style( 'bride-breadcrumbs', $custom_css );

	}

}

add_action( 'wp_enqueue_scripts', 'bride_styles_method',200 );