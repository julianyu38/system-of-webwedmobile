<section class="layers-area-wrapper l_admin-content">

    <div class="layers-onboard-wrapper">

        <div class="layers-onboard-slider">

            <!-- STEP 1 -->
            <div class="layers-onboard-slide layers-animate layers-onboard-slide-current">
                <div class="layers-column layers-span-4 l_admin-panel l_admin-content l_admin-push-bottom postbox">
                    <div class="layers-content-large">
                        <!-- Your content goes here -->
                        <div class="layers-section-title layers-small layers-no-push-bottom">
                            
                            <h3 class="layers-heading">
                                <?php esc_html_e( 'Thanks for Installing the Grant Foundation Child Theme!' , 'bride' ); ?>
                            </h3>
                            <div class="layers-excerpt">
                                <p>
                                    <?php esc_html_e( 'The following links will guide you to customize your theme in just the way you want. Just visit the links and explore.' , 'bride' ); ?>
                                </p>
                                <a href="http://devitem.com/wp/grant-preview/doc" target="_blank" class="button button-primary btn-large"><?php esc_html_e( 'Online Documentation' , 'bride' ); ?></a>     
                            </div>
                            <h3 class="layers-heading">
                                <?php esc_html_e( 'Layerswp Installation Guide.' , 'bride' ); ?>
                            </h3>
                            <div class="layers-excerpt">
                                <p>
                                    <?php esc_html_e( 'Grant Foundation is a child theme of layerswp parent theme. For using grant foundation child you will have to install layerswp theme first. Visit the link below to see layerswp installation guide.' , 'bride' ); ?>
                                </p>
                                <a href="http://docs.layerswp.com/doc/install-layers/" target="_blank" class="button button-primary btn-large"><?php esc_html_e( 'Layerswp Installation Guide' , 'bride' ); ?></a>  
                            </div>
                             
                        </div>
                    </div>   
                </div>
            </div>

        </div>
        
    </div>

</section>