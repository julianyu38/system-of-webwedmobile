<?php 

// Footer Social media Menu
if( !function_exists( 'hast_social_menu' ) ) {
function hast_social_menu(){
  register_nav_menu('hast-social-menu' ,'Footer Social Menu');
}}
add_action( 'after_setup_theme' , 'hast_social_menu' );


// Footer top styles
add_filter('layers_customizer_sections','layers_child_customizer_sections', 100);
add_filter( 'layers_customizer_controls', 'layers_child_customizer_controls', 100 );

if( !function_exists( 'layers_child_customizer_sections' ) ) {
   function layers_child_customizer_sections( $sections ){
      $sections[ 'hastech-footer-top' ] = array(
        'title' => esc_html__( 'Footer Top Styling' , 'bride' ),
        'panel' => 'footer',
      );
      return $sections;
   }
}


if( !function_exists( 'layers_child_customizer_controls' ) ) {
  function layers_child_customizer_controls( $controls ){
  	// Add Theme Badge
	
    $controls[ 'hastech-footer-top' ] = array(

    	'footer-top-showhide' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Show Footer Top', 'bride' ),
			'class' => 'group',
			'default' => 'no',
			'choices' => array(
				'yes' => esc_html__( 'Yes', 'bride' ),
				'no' => esc_html__( 'no', 'bride' ),
			),
		),

		'social_separetor_foo_top5' => array(
			'type'		=> 'layers-seperator'
		),
		'show-foo-top-logo' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Show logo', 'bride' ),
			'class' => 'group',
			'default' => 'yes',
			'choices' => array(
				'yes' => esc_html__( 'Yes', 'bride' ),
				'no' => esc_html__( 'no', 'bride' ),
			),
		),
		'the_custom_footer_top_logo' => array(
			'label' => esc_html__('Upload your logo', 'bride'),
			'type'		=> 'layers-select-images',
			'linked' => array(
				'show-if-selector' => "#layers-show-foo-top-logo",
				'show-if-value' => 'yes',
			),

		),
		'footer-top-logo-size' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Logo Size', 'bride' ),
			'class' => 'group layers-push-top',
			'choices' => array(
				'' => esc_html__( 'Auto', 'bride' ),
				'small' => esc_html__( 'Small', 'bride' ),
				'medium' => esc_html__( 'Medium', 'bride' ),
				'large' => esc_html__( 'Large', 'bride' ),
				'massive' => esc_html__( 'Massive', 'bride' ),
				'custom' => esc_html__( 'Custom', 'bride' ),
			),
			'linked' => array(
				'show-if-selector' => "#layers-show-foo-top-logo",
				'show-if-value' => 'yes',
			),
		),
		'footer-top-logo-size-custom' => array(
			'type'  => 'layers-range',
			'label' => esc_html__( 'Logo Custom Size', 'bride' ),
			'class' => 'group',
			'min' => 1,
			'max' => 200,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-footer-top-logo-size",
				'show-if-value' => 'custom',
			),
		),
		'footer-top-logo-padding-top' => array(
			'type'  => 'layers-range',
			'label' => esc_html__( 'Padding Top (px)', 'bride' ),
			'class' => 'group',
			'default' => 10,
			'min' => 1,
			'max' => 200,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-show-foo-top-logo",
				'show-if-value' => 'yes',
			),
		),
		'footer-top-logo-padding-bottom' => array(
			'type'  => 'layers-range',
			'label' => esc_html__( 'Padding Bottom (px)', 'bride' ),
			'class' => 'group',
			'default' => 10,
			'min' => 1,
			'max' => 200,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-show-foo-top-logo",
				'show-if-value' => 'yes',
			),
		),


		'social_separetor_foo_top4' => array(
			'type'		=> 'layers-seperator'
		),
      	'foo-top-single-info11' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Information Type', 'bride' ),
			'class' => 'group',
			'default' => '',
			'choices' => array(
				'' => esc_html__( 'None', 'bride' ),
				'text' => esc_html__( 'Plain Text', 'bride' ),
				'email' => esc_html__( 'Email', 'bride' ),
				'phone' => esc_html__( 'Phone', 'bride' ),
			),
		),
      	// text choose
		'foo-top-single-info1-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'map-marker',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'text',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single-info1-value' => array(
			'type'  => 'layers-rte',
			'label' => esc_html__( 'Insert Your Text', 'bride' ),
			'class' => 'text',
			'default' => 'Floor. 4 House. 15, Block C Banasree Main Rd, Dhaka.',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'text',
			),
		),
		// email choose 
		'foo-top-single-info2-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'envelope',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'email',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single-info2-value' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Insert Your Email', 'bride' ),
			'class' => 'text',
			'default' => 'admin@hastech.company',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'email',
			),
		),
		// phone choose 
		'foo-top-single-info3-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'phone',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'phone',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single-info3-value-cc' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Insert Your Country Code', 'bride' ),
			'class' => 'text',
			'default' => '+880',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'phone',
			),
		),
		'foo-top-single-info3-value' => array(
			'type'  => 'layers-number',
			'label' => esc_html__( 'Insert Your Phone Number', 'bride' ),
			'class' => 'text',
			'default' => 1973833508,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single-info11",
				'show-if-value' => 'phone',
			),
		),
		'social_separetor_foo_top1' => array(
			'type'		=> 'layers-seperator'
		),
		// Start footer top 2nd information
      	'foo-top-single2-info22' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Information Type', 'bride' ),
			'class' => 'group ',
			'default' => '',
			'choices' => array(
				'' => esc_html__( 'None', 'bride' ),
				'text' => esc_html__( 'Plain Text', 'bride' ),
				'email' => esc_html__( 'Email', 'bride' ),
				'phone' => esc_html__( 'Phone', 'bride' ),
			),
		),
      	// text choose
		'foo-top-single2-info1-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'map-marker',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'text',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single2-info1-value' => array(
			'type'  => 'layers-rte',
			'label' => esc_html__( 'Insert Your Text', 'bride' ),
			'class' => 'text',
			'default' => 'Floor. 4 House. 15, Block C Banasree Main Rd, Dhaka.',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'text',
			),
		),
		// email choose 
		'foo-top-single2-info2-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'envelope',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'email',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single2-info2-value' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Insert Your Email', 'bride' ),
			'class' => 'text',
			'default' => 'admin@hastech.company',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'email',
			),
		),
		// phone choose 
		'foo-top-single2-info3-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'mobile',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'phone',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single2-info3-value-cc' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Insert Your Country Code', 'bride' ),
			'class' => 'text',
			'default' => '+880',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'phone',
			),
		),
		'foo-top-single2-info3-value' => array(
			'type'  => 'layers-number',
			'label' => esc_html__( 'Insert Your Phone Number', 'bride' ),
			'class' => 'text',
			'default' => 1973833508,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single2-info22",
				'show-if-value' => 'phone',
			),
		),
		'social_separetor_foo_top2' => array(
			'type'		=> 'layers-seperator'
		),
		// Start footer top 3nd information
      	'foo-top-single3-info33' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Information Type', 'bride' ),
			'class' => 'group ',
			'default' => '',
			'choices' => array(
				'' => esc_html__( 'None', 'bride' ),
				'text' => esc_html__( 'Plain Text', 'bride' ),
				'email' => esc_html__( 'Email', 'bride' ),
				'phone' => esc_html__( 'Phone', 'bride' ),
			),
		),
      	// text choose
		'foo-top-single3-info1-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'map-marker',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'text',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single3-info1-value' => array(
			'type'  => 'layers-rte',
			'label' => esc_html__( 'Insert Your Text', 'bride' ),
			'class' => 'text',
			'default' => 'Floor. 4 House. 15, Block C Banasree Main Rd, Dhaka.',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'text',
			),
		),

		// email choose 
		'foo-top-single3-info2-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'envelope',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'email',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single3-info2-value' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Insert Your Email', 'bride' ),
			'class' => 'text',
			'default' => 'admin@hastech.company',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'email',
			),
		),

		// phone choose 
		'foo-top-single3-info3-icon' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Icon Name', 'bride' ),
			'class' => 'text',
			'default' => 'mobile',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'phone',
			),
			'description' => __( 'You want more icons <a  href="http://fontawesome.io/icons" target="_blank">Click here</a>.', 'bride' ),
		),
		'foo-top-single3-info3-value-cc' => array(
			'type'  => 'layers-text',
			'label' => esc_html__( 'Insert Your Country Code', 'bride' ),
			'class' => 'text',
			'default' => '+880',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'phone',
			),
		),
		'foo-top-single3-info3-value' => array(
			'type'  => 'layers-number',
			'label' => esc_html__( 'Insert Your Phone Number', 'bride' ),
			'class' => 'text',
			'default' => 1973833508,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-single3-info33",
				'show-if-value' => 'phone',
			),
		),


		'separetor_foo_top77' => array(
			'type'		=> 'layers-seperator'
		),
		
		'foo-top-information-option' => array(
			'type'  => 'layers-heading',
			'label' => esc_html__('Information Customize Option', 'bride'),
			'description' => esc_html__( 'Customize options for your footer top information settings.' , 'bride' ),
			
		),
		'foo-top-information-option-set' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Information Design', 'bride' ),
			'class' => 'group',
			'default' => '',
			'choices' => array(
				'' => esc_html__( 'Default Style', 'bride' ),
				'ftcustomize' => esc_html__( 'Custom Style', 'bride' ),
			),
		),
		'foo-top-information-font-size' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Font size (px)' , 'bride' ),
			'default' => 14,
			'min' => 0,
			'max' => 100,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-information-option-set",
				'show-if-value' => 'ftcustomize',
			),
		),
		'foo-top-information-font-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Text Color' , 'bride' ),
			'default' => '#2b2b2b',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-information-option-set",
				'show-if-value' => 'ftcustomize',
			),
		),
		'foo-top-information-font-transform' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Text Transform', 'bride' ),
			'class' => 'group',
			'default' => 'none',
			'choices' => array(
				'none' => esc_html__( 'None', 'bride' ),
				'capitalize' => esc_html__( 'Capitalize', 'bride' ),
				'uppercase' => esc_html__( 'Uppercase', 'bride' ),
				'lowercase' => esc_html__( 'Lowercase', 'bride' ),
				'full-width' => esc_html__( 'Full-width', 'bride' ),
				'inherit' => esc_html__( 'Inherit', 'bride' ),
				'initial' => esc_html__( 'Initial', 'bride' ),
				'unset' => esc_html__( 'Unset', 'bride' ),
			),
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-information-option-set",
				'show-if-value' => 'ftcustomize',
			),
		),
		'foo-top-information-display' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Icon Position', 'bride' ),
			'class' => 'group',
			'default' => 'inline',
			'choices' => array(
				'inline' => esc_html__( 'Icon on Left', 'bride' ),
				'block' => esc_html__( 'Icon on Top', 'bride' ),
			),
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-information-option-set",
				'show-if-value' => 'ftcustomize',
			),
		),
		'foo-top-information-display-imb' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Icon Size (px)' , 'bride' ),
			'default' => 14,
			'min' => 0,
			'max' => 200,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-information-display",
				'show-if-value' => 'block',
			),
		),
		'foo-top-information-display-fmb' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Margin Bottom (px)' , 'bride' ),
			'default' => 10,
			'min' => 0,
			'max' => 100,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-information-display",
				'show-if-value' => 'block',
			),
		),





		'separetor_foo_top777' => array(
			'type'		=> 'layers-seperator'
		),

		'foo-top-styleing-option' => array(
			'type'  => 'layers-heading',
			'label' => esc_html__('Footer Top Setting', 'bride'),
			'description' => esc_html__( 'Customize options for your footer top settings.' , 'bride' ),
			
		),
		'separetor_foo_top7' => array(
			'type'		=> 'layers-seperator'
		),
		'foo-top-content-alignment' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Content Alignment', 'bride' ),
			'class' => 'group ',
			'default' => 'text-center',
			'choices' => array(
				'text-left' => esc_html__( 'Left', 'bride' ),
				'text-center' => esc_html__( 'Center', 'bride' ),
				'text-right' => esc_html__( 'Right', 'bride' ),
			),
		),
		'foo-top-background-type' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Background Type', 'bride' ),
			'class' => 'group ',
			'default' => 'ftbcolor',
			'choices' => array(
				'' => esc_html__( 'None', 'bride' ),
				'ftbcolor' => esc_html__( 'Background Color', 'bride' ),
				'ftbimage' => esc_html__( 'Background Image', 'bride' ),
			),
		),
		'foo-top-background-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Background Color' , 'bride' ),
			'default' => '#2b2b2b',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-background-type",
				'show-if-value' => 'ftbcolor',
			),
		),
		
		'foo-top-background-image' => array(
			'label' => esc_html__('Upload Background Image', 'bride'),
			'type'		=> 'layers-select-images',
			'default'		=> '',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-background-type",
				'show-if-value' => 'ftbimage',
			),
		),
		'foo-top-background-overlay-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Overlay Color' , 'bride' ),
			'default' => '#2b2b2b',
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-background-type",
				'show-if-value' => 'ftbimage',
			),
		),
		'foo-top-background-overlay-opacity' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Overlay Opacity' , 'bride' ),
			'default' => 5,
			'min' => 0,
			'max' => 10,
			'step' => 1,

			'linked' => array(
				'show-if-selector' => "#layers-foo-top-background-type",
				'show-if-value' => 'ftbimage',
			),
		),


		// basic setting


		'foo-top-padding-margin-option' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Padding / Margin', 'bride' ),
			'class' => 'group ',
			'default' => '',
			'choices' => array(
				'' => esc_html__( 'Default Option', 'bride' ),
				'footoppaddingmargin' => esc_html__( 'Custom Option', 'bride' ),
			),
		),
		'foo-top-padding-option-top' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Padding Top (px)' , 'bride' ),
			'default' => 50,
			'min' => 0,
			'max' => 250,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-padding-margin-option",
				'show-if-value' => 'footoppaddingmargin',
			),
		),
		'foo-top-padding-option-bottom' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Padding Bottom (px)' , 'bride' ),
			'default' => 50,
			'min' => 0,
			'max' => 250,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-padding-margin-option",
				'show-if-value' => 'footoppaddingmargin',
			),
		),
		// Margin option
		'foo-top-margin-option-top' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Margin Top (px)' , 'bride' ),
			'default' => 50,
			'min' => 0,
			'max' => 250,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-padding-margin-option",
				'show-if-value' => 'footoppaddingmargin',
			),
		),
		'foo-top-margin-option-bottom' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Margin Bottom (px)' , 'bride' ),
			'default' => 50,
			'min' => 0,
			'max' => 250,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-foo-top-padding-margin-option",
				'show-if-value' => 'footoppaddingmargin',
			),
		),

    );
    return $controls;
  }
}













// Footer Standard styles
add_filter('layers_customizer_sections','layers_child_customizer_sections_foo_standard', 100);
add_filter( 'layers_customizer_controls', 'layers_child_customizer_controls_foo_standard', 100 );

if( !function_exists( 'layers_child_customizer_sections_foo_standard' ) ) {
   function layers_child_customizer_sections_foo_standard( $sections ){
      $sections[ 'hastech-footer-standard' ] = array(
        'title' => esc_html__( 'Footer Styling' , 'bride' ),
        'panel' => 'footer',
      );
      return $sections;
   }
}

if( !function_exists( 'layers_child_customizer_controls_foo_standard' ) ) {
  function layers_child_customizer_controls_foo_standard( $controls ){
 	 	// Add Theme Badge
	
    $controls[ 'hastech-footer-standard' ] = array(
      	// Footer top start

		'hastech_option_title1' => array(
			'type'  => 'layers-heading',
			'label' => esc_html__('Footer Setting', 'bride'),
			'description' => esc_html__( 'Customize options for your footer settings.' , 'bride' ),
			
		),
		'footer-widget-show-hide' => array(
			'type'     => 'layers-select',
			'heading_divider'    => __( 'Widget Areas Show/Hide' , 'bride' ),
			'description' => __( 'Choose how many widget areas apear in the footer. Go here to <a class="customizer-link" href="#accordion-panel-widgets">customize footer widgets</a>.', 'bride' ),
			'default' => 'no',
			'choices' => array(
				'no' => __( 'No' , 'bride' ),
				'yes' => __( 'Yes' , 'bride' ),
			),
		),
		'hastech_footer_top_widget_title' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Widget Title Color' , 'bride' ),
			'default' => '#fff',
		),
		'hastech_footer_top_widget_content_color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Widget Content Color' , 'bride' ),
			'default' => '#fff',
		),
		'hastech_footer_top_widget_title_font_size' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Widget Title font size' , 'bride' ),
			'default' => 14,
			'max' => 100,
			'min' => 0,
		),


		'foo-background-type' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Background Type', 'bride' ),
			'class' => 'group',
			'default' => 'foobcolor',
			'choices' => array(
				'' => esc_html__( 'None', 'bride' ),
				'foobcolor' => esc_html__( 'Background Color', 'bride' ),
				'foobimage' => esc_html__( 'Background Image', 'bride' ),
			),
		),
		'foo-background-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Background Color' , 'bride' ),
			'default' => '#2b2b2b',
			'linked' => array(
				'show-if-selector' => "#layers-foo-background-type",
				'show-if-value' => 'foobcolor',
			),
		),
		
		'foo-background-image' => array(
			'label' => esc_html__('Upload Background Image', 'bride'),
			'type'		=> 'layers-select-images',
			'default'		=> '',
			'linked' => array(
				'show-if-selector' => "#layers-foo-background-type",
				'show-if-value' => 'foobimage',
			),
		),
		'foo-background-overlay-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Overlay Color' , 'bride' ),
			'default' => '#2b2b2b',
			'linked' => array(
				'show-if-selector' => "#layers-foo-background-type",
				'show-if-value' => 'foobimage',
			),
		),
		'foo-background-overlay-opacity' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Overlay Opacity' , 'bride' ),
			'default' => 5,
			'min' => 0,
			'max' => 10,
			'step' => 1,

			'linked' => array(
				'show-if-selector' => "#layers-foo-background-type",
				'show-if-value' => 'foobimage',
			),
		),

		'hastech_top_footer_top_padding' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Padding Top' , 'bride' ),
			'default' => 30,
			'max' => 250,
			'min' => 0,
		),
		'hastech_top_footer_bottom_padding' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Padding Bottom' , 'bride' ),
			'default' => 20,
			'max' => 250,
			'min' => 0,
		),
		'hastech_top_footer_top_margin' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Margin Top' , 'bride' ),
			'default' => 0,
			'max' => 200,
			'min' => 0,
			'step' => 1,
		),
		'hastech_top_footer_bottom_margin' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Margin Bottom' , 'bride' ),
			'default' => 0,
			'max' => 200,
			'min' => 0,
			'step' => 1,
		),
    );
    return $controls;
  }
}






// Footer bottom styles
add_filter('layers_customizer_sections','layers_child_customizer_sections_foo_bottom', 100);
add_filter( 'layers_customizer_controls', 'layers_child_customizer_controls_foo_bottom', 100 );

if( !function_exists( 'layers_child_customizer_sections_foo_bottom' ) ) {
   function layers_child_customizer_sections_foo_bottom( $sections ){
      $sections[ 'hastech-footer-bottom' ] = array(
        'title' => esc_html__( 'Copyright Styling' , 'bride' ),
        'panel' => 'footer',
      );
      return $sections;
   }
}

if( !function_exists( 'layers_child_customizer_controls_foo_bottom' ) ) {
  function layers_child_customizer_controls_foo_bottom( $controls ){
 
 	// Add Theme Badge
	
    $controls[ 'hastech-footer-bottom' ] = array(
		// Footer bottom start
		'hastech_option_title2' => array(
			'label' => esc_html__('Copyright Setting', 'bride'),
			'description' => esc_html__( 'Customize options for your copyright section settings.' , 'bride' ),  
			'type'  => 'layers-heading',
		),

		'footer_content_alignment' => array(
			'type'     => 'layers-select',
			'heading_divider'    => esc_html__( 'Content Align' , 'bride' ),
			'default' => 'text-center',
			'sanitize_callback' => FALSE,
			'choices' => array(
				'text-left' => esc_html__( 'Left' , 'bride' ),
				'text-right' => esc_html__( 'Right' , 'bride' ),
				'text-center' => esc_html__( 'Center' , 'bride' ),
			),
		),



		'copyright-background-type' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Select Background Type', 'bride' ),
			'class' => 'group',
			'default' => 'copybcolor',
			'choices' => array(
				'' => esc_html__( 'None', 'bride' ),
				'copybcolor' => esc_html__( 'Background Color', 'bride' ),
				'copybimage' => esc_html__( 'Background Image', 'bride' ),
			),
		),
		'copyright-background-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Background Color' , 'bride' ),
			'default' => '#59535F',
			'linked' => array(
				'show-if-selector' => "#layers-copyright-background-type",
				'show-if-value' => 'copybcolor',
			),
		),
		'copyright-background-image' => array(
			'label' => esc_html__('Upload Background Image', 'bride'),
			'type'		=> 'layers-select-images',
			'default'		=> '',
			'linked' => array(
				'show-if-selector' => "#layers-copyright-background-type",
				'show-if-value' => 'copybimage',
			),
		),
		'copyright-background-overlay-color' => array(
			'type'     => 'layers-color',
			'label'    => esc_html__( 'Overlay Color' , 'bride' ),
			'default' => '#2b2b2b',
			'linked' => array(
				'show-if-selector' => "#layers-copyright-background-type",
				'show-if-value' => 'copybimage',
			),
		),
		'copyright-background-overlay-opacity' => array(
			'type'     => 'layers-range',
			'label'    => esc_html__( 'Overlay Opacity' , 'bride' ),
			'default' => 5,
			'min' => 0,
			'max' => 10,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-copyright-background-type",
				'show-if-value' => 'copybimage',
			),
		),

		'custom_footer_padding_top' => array(
			'label' => esc_html__('Padding Top', 'bride'), 
			'type'		=> 'layers-range',
			'default' => 0,
			'max' => 250,
			'min' => 0,
			'step' => 1,
		),
		'custom_footer_padding_bottom' => array(
			'label' => esc_html__('Padding Bottom', 'bride'),
			'type'		=> 'layers-range',
			'default' => 0,
			'max' => 250,
			'min' => 0,
			'step' => 1
		),
		'custom_footer_margin_top' => array(
			'label' => esc_html__('Margin Top', 'bride'),
			'type'		=> 'layers-range',
			'default' => 0,
			'max' => 250,
			'min' => 0,
			'step' => 1
		),
		'custom_footer_margin_bottom' => array(
			'label' => esc_html__('Margin Bottom', 'bride'),
			'type'		=> 'layers-range',
			'default' => 0,
			'max' => 250,
			'min' => 0,
			'step' => 1
		),
  		'social_separetor8' => array(
			'type'		=> 'layers-seperator'
		),
  		// Footer bottom end
		'show_foo_logo' => array(
			'label' => esc_html__('Show footer logo', 'bride'),
			'type'		=> 'layers-checkbox',
			'default' => true
		),
		'the_custom_footer_logo' => array(
			'label' => esc_html__('Upload your logo', 'bride'),
			'type'		=> 'layers-select-images',
		),
		'footer-logo-size' => array(
			'type'  => 'layers-select',
			'label' => esc_html__( 'Logo Size', 'bride' ),
			'class' => 'group layers-push-top',
			'choices' => array(
				'' => esc_html__( 'Auto', 'bride' ),
				'small' => esc_html__( 'Small', 'bride' ),
				'medium' => esc_html__( 'Medium', 'bride' ),
				'large' => esc_html__( 'Large', 'bride' ),
				'massive' => esc_html__( 'Massive', 'bride' ),
				'custom' => esc_html__( 'Custom', 'bride' ),
			),
		),
		'footer-logo-size-custom' => array(
			'type'  => 'layers-range',
			'label' => esc_html__( 'Logo Custom Size', 'bride' ),
			'class' => 'group',
			'min' => 1,
			'max' => 200,
			'step' => 1,
			'linked' => array(
				'show-if-selector' => "#layers-footer-logo-size",
				'show-if-value' => 'custom',
			),
		),
		'social_separetor6' => array(
			'type'		=> 'layers-seperator'
		),


  		'heading-title' => array(
			'type'  => 'layers-heading',
			'label'    => esc_html__( 'Social Option' , 'bride' ),
			'description' => esc_html__( 'Design Option to change the styles and appearance of your social icons.' , 'bride' ),  
		),
  		'social_separetor3' => array(
			'type'		=> 'layers-seperator'
		),
		'show_footer_social' => array(
			'label' => esc_html__('Show Social media', 'bride' ),
			'type'		=> 'layers-checkbox',
			'default' => true,
		),
		'footer_social_icon_color' => array(
			'label' => esc_html__('Icon Color', 'bride' ),
			'type'		=> 'layers-color',
			'default'	=> '#666',
		),
		'footer_social_icon_hover_color' => array(
			'label' => esc_html__('Hover Color', 'bride' ),
			'type'		=> 'layers-color',
			'default'	=> '#fff',
		),
		'footer_social_icon_bg_color' => array(
			'label' => esc_html__('background', 'bride' ),
			'type'		=> 'layers-color',
			'default'	=> '',
		),
		'footer_social_icon_bg_hover_color' => array(
			'label' => esc_html__('Hover background', 'bride' ),
			'type'		=> 'layers-color',
			'default'	=> '#3a3ac0',
		),
		'footer_social_icon_border_color' => array(
			'label' => esc_html__('Border Color', 'bride' ),
			'type'		=> 'layers-color',
			'default'	=> '#f3f3f3',
		),
		'footer_social_icon_border_hover_color' => array(
			'label' => esc_html__('Border Hover Color', 'bride' ),
			'type'		=> 'layers-color',
			'default'	=> '#3a3ac0',
		),
		'footer_social_icon_font_size' => array(
			'label' => esc_html__('Font Size (px)', 'bride' ),
			'type'		=> 'layers-range',
			'default'	=> 25,
			'max'	=> 50,
			'min'	=> 5,
		),
		'footer_social_icon_width' => array(
			'label' => esc_html__('Icon width (px)', 'bride' ),
			'type'		=> 'layers-range',
			'default'	=> 40,
			'max'	=> 80,
			'min'	=> 10,
		),
		'footer_social_icon_border_size' => array(
			'label' => esc_html__('Border size (px)', 'bride' ),
			'type'		=> 'layers-range',
			'default' => 1,
			'max' => 10,
			'min' => 0
		),
		'footer_social_icon_rounded_size' => array(
			'label' => esc_html__('Rounded Corner Size (%)', 'bride' ),
			'type'		=> 'layers-range',
			'default' => 10,
			'max' => 100,
			'min' => 0
		),
		'footer_social_menu_link_spacing' => array(
			'label' => esc_html__('Link Spacing (px)', 'bride' ),
			'type'		=> 'layers-range',
			'default'	=> 15,
			'max'	=> 100,
			'min'	=> 0,
			'step'	=> 1
		),

  		// Footer bottom end
		'social_separetor66' => array(
			'type'		=> 'layers-seperator'
		),
		'payment-heading-title' => array(
			'type'  => 'layers-heading',
			'label'    => esc_html__( 'Credit Cards Option' , 'bride' ),
			'description' => esc_html__( 'Customize options for your payment section settings. ' , 'bride' ),  
		),
		'social_separetor5' => array(
			'type'		=> 'layers-seperator'
		),
		
		
		'footer-menu-area' => array(
			'type'  => 'layers-heading',
			'label'    => esc_html__( 'Footer Menu style' , 'bride' ),
			'description' => esc_html__( 'Customize options for your footer menu style. ' , 'bride' ),  
		),
		'social_separetor44' => array(
			'type'		=> 'layers-seperator'
		),
		'show-footer-menu' => array(
			'label' => esc_html__('Show Footer Menu', 'bride'),
			'type'		=> 'layers-checkbox',
			'default' => true
		),
		'footer-menu-font-size' => array(
			'label' => esc_html__('Font Size', 'bride' ),
			'type'		=> 'layers-range',
			'default'	=> 12,
			'max'	=> 40,
			'min'	=> 0,
			'step'	=> 1
		),
		'footer-menu-text-color' => array(
			'label' => esc_html__('Text color', 'bride'),
			'type'		=> 'layers-color',
			'default'	=> '#f1f1f1',
		),
		'footer-menu-hover-text-color' => array(
			'label' => esc_html__('Hover Text color', 'bride'),
			'type'		=> 'layers-color',
			'default'	=> '#f1f1f1',
		),
		'footer-menu-hover-text-color' => array(
			'label' => esc_html__('Hover Text color', 'bride'),
			'type'		=> 'layers-color',
			'default'	=> '#f1f1f1',
		),
		'footer-menu-text-transform' => array(
			'type'     => 'layers-select',
			'heading_divider'    => esc_html__( 'Text Transform' , 'bride' ),
			'default' => 'normal',
			'sanitize_callback' => FALSE,
			'choices' => array(
				'normal' => esc_html__( 'Normal' , 'bride' ),
				'uppercase' => esc_html__( 'Uppercase' , 'bride' ),
				'capitalize' => esc_html__( 'Capitalize' , 'bride' ),
				'lowercase' => esc_html__( 'Lowercase' , 'bride' ),
			),
		),
		'footer-menu-link-spacing' => array(
			'label' => esc_html__('Link Spacing', 'bride' ),
			'type'		=> 'layers-range',
			'default'	=> 25,
			'max'	=> 100,
			'min'	=> 0,
			'step'	=> 1
		),
    );
    return $controls;
  }
}



// Costomizer API layers
add_filter( 'layers_customizer_controls' , 'hastech_customizer_footer_sections', 100 );
if( !function_exists( 'hastech_customizer_footer_sections' ) ) {
   function hastech_customizer_footer_sections( $controls){
	
   		// Unset footer default style
	   	unset($controls['footer-layout']['show-layers-badge']);
	   	unset($controls['footer-layout']['header-position-styling']);
	   	unset($controls['footer-layout']['footer-background-color']);
	   	unset($controls['footer-layout']['footer-height']);

	   	// Add Theme Badge
		

      	$hastech_controls = array(

      		'footer-copyright-text' => array(
				'type'     => 'layers-text',
				'label'    => esc_html__( 'Copyright Text' , 'bride' ),
				'default' => ' Made at the tip of Bangladesh. &copy;',
				'sanitize_callback' => FALSE
			),
      		// Start copy right option
	  		'heading-title-copyright' => array(
				'type'  => 'layers-heading',
				'label'    => esc_html__( 'Copyright text Setting' , 'bride' ),
				'description' => esc_html__( 'Change the font size and color of your copyright test.' , 'bride' ),  
			),
			'footer_font_size' => array(
				'label' => esc_html__('Font Size', 'bride' ),
				'type'		=> 'layers-range',
				'default'	=> 14,
				'max'	=> 30,
				'min'	=> 5,
			),
	  		'footer_text_color' => array(
				'label' => esc_html__('Copyright color', 'bride'),
				'type'		=> 'layers-color',
				'default'	=> '#7a7a7a',
			),
	  		// end copy right option
	  		
		);

    $controls['footer-layout'] = array_merge( $controls['footer-layout'], $hastech_controls );
 
    return $controls;
    }
}


//output result	
add_action( 'wp_enqueue_scripts', 'hastech_child_customizer_styles', 100 );

if( !function_exists( 'hastech_child_customizer_styles' ) ) {
	
    function hastech_child_customizer_styles() {
    	// Add Theme Badge
		

		/**
		 * Footer - information
		 */
		$foo_top_info_font_size = layers_get_theme_mod( 'foo-top-information-font-size');
		$foo_top_info_design_option = layers_get_theme_mod( 'foo-top-information-option-set');
		
		if ( 'ftcustomize' === $foo_top_info_design_option && '' !== $foo_top_info_font_size ) {
			// Apply Styles.
			layers_inline_styles( '.footer-top-info .info-sin, .footer-top-info .info-sin a, .footer-top-info .info-sin a i', array( 'css' => array(
				'font-size'    => "{$foo_top_info_font_size}"."px",
			) ) );
		}

		$foo_top_info_transform = layers_get_theme_mod( 'foo-top-information-font-transform');
		
		if ( 'ftcustomize' === $foo_top_info_design_option && '' !== $foo_top_info_transform ) {
			// Apply Styles.
			layers_inline_styles( '.footer-top-info .info-sin', array( 'css' => array(
				'text-transform'    => "{$foo_top_info_transform}",
			) ) );
		}

		$foo_top_info_font_color = layers_get_theme_mod( 'foo-top-information-font-color');
		if ( 'ftcustomize' === $foo_top_info_design_option && '' !== $foo_top_info_font_color ) {
			// Apply Styles.
			layers_inline_styles( '.footer-top-info .info-sin, .footer-top-info .info-sin a i, .footer-top-info .info-sin a', array( 'css' => array(
				'color'    => "{$foo_top_info_font_color}",
			) ) );
		}

		$foo_top_info_display = layers_get_theme_mod( 'foo-top-information-display');
		if ( 'ftcustomize' === $foo_top_info_design_option && '' !== $foo_top_info_display ) {
			// Apply Styles.
			layers_inline_styles( '.footer-top-info .info-sin a i, .footer-top-info .info-sin i', array( 'css' => array(
				'display'    => "{$foo_top_info_display}",
			) ) );
		}

		$foo_top_info_display_isz = layers_get_theme_mod( 'foo-top-information-display-imb');
		$foo_top_icon_display_block = layers_get_theme_mod( 'foo-top-information-display');
		if ( 'ftcustomize' === $foo_top_info_design_option && 'inline' !== $foo_top_icon_display_block &&  '' !== $foo_top_info_display_isz ) {
			// Apply Styles.
			layers_inline_styles( '.footer-top-info .info-sin a i, .footer-top-info .info-sin i', array( 'css' => array(
				'font-size'    => "{$foo_top_info_display_isz}"."px",
			) ) );
		}
		$foo_top_info_display_fmb = layers_get_theme_mod( 'foo-top-information-display-fmb');
		if ( 'ftcustomize' === $foo_top_info_design_option && '' !== $foo_top_info_display_fmb ) {
			// Apply Styles.
			layers_inline_styles( '.footer-top-info .info-sin a i, .footer-top-info .info-sin i', array( 'css' => array(
				'margin-bottom'    => "{$foo_top_info_display_fmb}"."px",
			) ) );
		}




		/**
		* Footer overlay color
		*/
		$footer_overlay_color = layers_get_theme_mod( 'foo-background-overlay-color');
		if ( '' !== $footer_overlay_color ) {
			// Apply Styles.
			layers_inline_styles( '[data-foo-overlay]:before', array( 'css' => array(
				'background'    => "{$footer_overlay_color}",
			) ) );
		}
		/**
		 * Footer - Top overlay color
		 */
		$theme_overlay_color = layers_get_theme_mod( 'foo-top-background-overlay-color');
		
		if ( '' !== $theme_overlay_color ) {
			// Apply Styles.
			layers_inline_styles( '[data-top-overlay]:before', array( 'css' => array(
				'background'    => "{$theme_overlay_color}",
			) ) );
		}
		/**
		 * Copyright - overlay color
		 */
		$copyright_overlay_color = layers_get_theme_mod( 'copyright-background-overlay-color');
		
		if ( '' !== $copyright_overlay_color ) {
			// Apply Styles.
			layers_inline_styles( '[data-copyright-overlay]:before', array( 'css' => array(
				'background'    => "{$copyright_overlay_color}",
			) ) );
		}
		/**
		 * Footer - Logo Size
		 */
		$ftsize = layers_get_theme_mod( 'footer-top-logo-size');
		$ftmax_height = layers_get_theme_mod( 'footer-top-logo-size-custom');
		if ( 'custom' === $ftsize && '' !== $ftmax_height ) {

			// Apply Styles.
			layers_inline_styles( '.footer-top-logo .footer-logo-custom img', array( 'css' => array(
				'width' => 'auto',
				'max-height'    => "{$ftmax_height}px",
			) ) );
		}
		/**
		 * Footer - Top margin padding
		 */
		$foo_top_logo_padding_top = layers_get_theme_mod( 'footer-top-logo-padding-top');
		$showhide_foo_tpo_logo = layers_get_theme_mod( 'show-foo-top-logo');
		if ( 'yes' === $showhide_foo_tpo_logo && '' !== $foo_top_logo_padding_top ) {

			// Apply Styles.
			layers_inline_styles( '.footer-top-logo', array( 'css' => array(
				'padding-top'    => "{$foo_top_logo_padding_top}px",
			) ) );
		}
		$foo_top_logo_padding_bottom = layers_get_theme_mod( 'footer-top-logo-padding-bottom');
		if ( 'yes' === $showhide_foo_tpo_logo && '' !== $foo_top_logo_padding_bottom ) {

			// Apply Styles.
			layers_inline_styles( '.footer-top-logo', array( 'css' => array(
				'padding-bottom'    => "{$foo_top_logo_padding_bottom}px",
			) ) );
		}

		/**
		 * Footer - Logo Size
		 */
		$fbsize = layers_get_theme_mod( 'footer-logo-size');
		$fbmax_height = layers_get_theme_mod( 'footer-logo-size-custom');
		if ( 'custom' === $fbsize && '' !== $fbmax_height ) {

			// Apply Styles.
			layers_inline_styles( '.footer-logo img', array( 'css' => array(
				'width' => 'auto',
				'max-height'    => "{$fbmax_height}px",
			) ) );
		}

		/**
		 * Footer - Cadits cards size
		 */
		$cdsize = layers_get_theme_mod( 'the-custom-credit-cards-size');
		$cdmax_height = layers_get_theme_mod( 'the-custom-credit-cards-size-custom');
		if ( 'custom' === $cdsize && '' !== $cdmax_height ) {

			// Apply Styles.
			layers_inline_styles( '.credit-card img', array( 'css' => array(
				'width' => 'auto',
				'max-height'    => "{$cdmax_height}px",
			) ) );
		}

		/**
		 * Footer - menu style
		 */
		$textcolor = layers_get_theme_mod( 'footer-menu-text-color');
		if (isset($textcolor)) {

			// Apply Styles.
			layers_inline_styles( '.copyright .nav-horizontal.footermenu a', array( 'css' => array(
				'color'    => "{$textcolor}",
			) ) );
		}

		$hovertextcolor = layers_get_theme_mod( 'footer-menu-hover-text-color');
		if (isset($hovertextcolor)) {

			// Apply Styles.
			layers_inline_styles( '.copyright .nav-horizontal.footermenu a:hover', array( 'css' => array(
				'color'    => "{$hovertextcolor}",
			) ) );
		}

		$texttransform = layers_get_theme_mod( 'footer-menu-text-transform');
		switch ($texttransform) {
		    case "normal":
		        layers_inline_styles( '.copyright .nav-horizontal.footermenu a', array( 'css' => array(
						'text-transform'    => "{$texttransform}",
					) ) );
		        break;
		    case "uppercase":
		        layers_inline_styles( '.copyright .nav-horizontal.footermenu a', array( 'css' => array(
						'text-transform'    => "{$texttransform}",
					) ) );
		        break;
		    case "capitalize":
		        layers_inline_styles( '.copyright .nav-horizontal.footermenu a', array( 'css' => array(
						'text-transform'    => "{$texttransform}",
					) ) );
		        break;
		    case "lowercase":
		        layers_inline_styles( '.copyright .nav-horizontal.footermenu a', array( 'css' => array(
						'text-transform'    => "{$texttransform}",
					) ) );
		        break;
		    default:
		        layers_inline_styles( '.copyright .nav-horizontal.footermenu a', array( 'css' => array(
						'text-transform'    => "normal",
					) ) );
		}

		$linkspaching = layers_get_theme_mod( 'footer-menu-link-spacing');
		if ( '' !== $linkspaching ) {

			// Apply Styles.
			layers_inline_styles( '.copyright .nav-horizontal li', array( 'css' => array(
				'margin-left'    => "{$linkspaching}px",
			) ) );
		}

		$footermenufontsize = layers_get_theme_mod( 'footer-menu-font-size');
		if ( '' !== $footermenufontsize ) {

			// Apply Styles.
			layers_inline_styles( '.copyright .nav-horizontal li a', array( 'css' => array(
				'font-size'    => "{$footermenufontsize}px",
			) ) );
		}








        $hastech_controls_footer_color = layers_get_theme_mod( 
            'footer_text_color' , 
             TRUE 
        );
			
		// inline css 		
		if( '' != $hastech_controls_footer_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.site-text
			'),
				'css' => array(
				'color' => $hastech_controls_footer_color,
				),
			  )		
			);// End theme color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_color = layers_get_theme_mod( 
            'footer_social_icon_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a i
			'),
				'css' => array(
				'color' => $hastech_controls_footer_social_icon_color,
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_hover_color = layers_get_theme_mod( 
            'footer_social_icon_hover_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_hover_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a:hover i
			'),
				'css' => array(
				'color' => $hastech_controls_footer_social_icon_hover_color,
				),
			  )		
			);// End social color		
		} // edn check condition function



        $hastech_controls_footer_social_icon_bg_color = layers_get_theme_mod( 
            'footer_social_icon_bg_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_bg_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a
			'),
				'css' => array(
				'background-color' => $hastech_controls_footer_social_icon_bg_color,
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_bg_hover_color = layers_get_theme_mod( 
            'footer_social_icon_bg_hover_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_bg_hover_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a:hover
			'),
				'css' => array(
				'background-color' => $hastech_controls_footer_social_icon_bg_hover_color,
				),
			  )		
			);// End social color		
		} // edn check condition function



        $hastech_controls_footer_social_icon_border_size = layers_get_theme_mod( 
            'footer_social_icon_border_size' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_border_size ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a, .footer-icon ul li a:hover
			'),
				'css' => array(
				'border-width' => $hastech_controls_footer_social_icon_border_size.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_border_color = layers_get_theme_mod( 
            'footer_social_icon_border_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_border_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a
			'),
				'css' => array(
				'border-color' => $hastech_controls_footer_social_icon_border_color,
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_border_hover_color = layers_get_theme_mod( 
            'footer_social_icon_border_hover_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_border_hover_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a:hover
			'),
				'css' => array(
				'border-color' => $hastech_controls_footer_social_icon_border_hover_color,
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_rounded_size = layers_get_theme_mod( 
            'footer_social_icon_rounded_size' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_rounded_size ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a
			'),
				'css' => array(
				'border-radius' => $hastech_controls_footer_social_icon_rounded_size.'%',
				),
			  )		
			);// End social color		
		} // edn check condition function

        $hastech_controls_footer_social_icon_link_spacing = layers_get_theme_mod( 
            'footer_social_menu_link_spacing' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_link_spacing ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li + li
			'),
				'css' => array(
				'margin-left' => $hastech_controls_footer_social_icon_link_spacing.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_font_size = layers_get_theme_mod( 
            'footer_font_size' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_font_size ){
			layers_inline_styles( array(
			'selectors' => array('
				.copyright .site-text
			'),
				'css' => array(
				'font-size' => $hastech_controls_footer_font_size.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function

        $hastech_controls_footer_social_icon_font_size = layers_get_theme_mod( 
            'footer_social_icon_font_size' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_font_size ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a
			'),
				'css' => array(
				'font-size' => $hastech_controls_footer_social_icon_font_size.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_social_icon_size = layers_get_theme_mod( 
            'footer_social_icon_width' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_social_icon_size ){
			$validefine = 2;
			$lineheight = $hastech_controls_footer_social_icon_size;
			$lineheightresult = $lineheight - $validefine;

			layers_inline_styles( array(
			'selectors' => array('
				.footer-icon ul li a
			'),
				'css' => array(
				'height' => $hastech_controls_footer_social_icon_size.'px',
				'width' => $hastech_controls_footer_social_icon_size.'px',
				'line-height' => $lineheightresult.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function



        $hastech_controls_footer_top_top_padding = layers_get_theme_mod( 
            'hastech_top_footer_top_padding' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_top_top_padding ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .grid.footer-top-grid
			'),
				'css' => array(
				'padding-top' => $hastech_controls_footer_top_top_padding.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function

        $hastech_controls_footer_bottom_top_padding = layers_get_theme_mod( 
            'hastech_top_footer_bottom_padding' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_bottom_top_padding ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .grid.footer-top-grid
			'),
				'css' => array(
				'padding-bottom' => $hastech_controls_footer_bottom_top_padding.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function

		$hastech_controls_footer_top_top_margin = layers_get_theme_mod( 
            'hastech_top_footer_top_margin' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_top_top_margin ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .grid.footer-top-grid
			'),
				'css' => array(
				'margin-top' => $hastech_controls_footer_top_top_margin.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function


        $hastech_controls_footer_top_bottom_margin = layers_get_theme_mod( 
            'hastech_top_footer_bottom_margin' , 
             TRUE 
        );
        // inline css 		
		if( '' != $hastech_controls_footer_top_bottom_margin ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .grid.footer-top-grid
			'),
				'css' => array(
				'margin-bottom' => $hastech_controls_footer_top_bottom_margin.'px',
				),
			  )		
			);// End social color		
		} // edn check condition function



        $hastech_controls_footer_top_background = layers_get_theme_mod( 
            'hastech_footer_top_background' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_top_background ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site
			'),
				'css' => array(
				'background' => $hastech_controls_footer_top_background,
				),
			  )		
			);// End social color		
		} // edn check condition function
        $hastech_controls_footer_top_widget_title_color = layers_get_theme_mod( 
            'hastech_footer_top_widget_title' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_top_widget_title_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .section-nav-title, .footer-site.invert .section-nav-title
			'),
				'css' => array(
				'color' => $hastech_controls_footer_top_widget_title_color,
				),
			  )		
			);// End social color		
		} // edn check condition function
        $hastech_controls_footer_top_widget_content_color = layers_get_theme_mod( 
            'hastech_footer_top_widget_content_color' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_top_widget_content_color ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .widget ul li a, .footer-site .widget ul li, .footer-site .widget ul li p, .footer-site .widget p, .footer-site .widget table th , .footer-site .widget table td , .footer-site .widget caption
			'),
				'css' => array(
				'color' => $hastech_controls_footer_top_widget_content_color,
				),
			  )		
			);// End social color		
		} // edn check condition function
        $hastech_controls_footer_top_widget_title_font_size = layers_get_theme_mod( 
            'hastech_footer_top_widget_title_font_size' , 
             TRUE 
        );
		// inline css 		
		if( '' != $hastech_controls_footer_top_widget_title_font_size ){
			layers_inline_styles( array(
			'selectors' => array('
				.footer-site .section-nav-title
			'),
				'css' => array(
				'font-size' => $hastech_controls_footer_top_widget_title_font_size .'px',
				),
			  )		
			);// End social color		
		} // edn check condition function

	}// end function
} // edn condition function