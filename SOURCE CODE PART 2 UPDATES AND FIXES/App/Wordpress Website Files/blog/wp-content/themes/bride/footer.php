
			<div id="back-to-top">
				<a href="#top"><?php esc_html_e( 'Back to top' , 'bride' ); ?></a>
			</div> <!-- back-to-top -->

			<?php if ( 'header-sidebar' == layers_get_theme_mod( 'header-menu-layout' ) ) { ?>
				<?php //get_template_part( 'partials/footer' , 'standard' ); ?>
			<?php } ?>

		</footer>



		<!-- footer end -->
		<?php if ( 'header-sidebar' == layers_get_theme_mod( 'header-menu-layout' ) ) { ?>
			</div><!-- /header side wrapper -->
		<?php } else {
			// get_template_part( 'partials/footer' , 'standard' );
		}?>

	</div><!-- END / MAIN SITE #wrapper -->
	<?php do_action( 'layers_after_site_wrapper' ); ?>
	<?php wp_footer(); ?>
	<!-- footer start -->
	<!--footer>
		<div>
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center"> <span class="footer-logo"> <img src="http://localhost:8888/images/icon/WebWed_logo_white.svg" alt="WebWed" width="250"> </span> </div>
					<h2 class="color_teal medium">When Love Can't Wait.</h2>
					<div class="col-sm-offset-3 col-sm-6"> <a href="https://www.facebook.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-facebook"></i></a> <a href="https://twitter.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-twitter"></i></a> <a href="https://instagram.com/webwedmobile" class="wow animated rollIn"><i class="fa fa-instagram"></i></a> </div>
				</div>
				<div class="row">
					<div class="col-sm-12 text-center">
						<ul class="nav nav-pills">
							<li role="presentation"><a style="border:0;" href="#story">About</a></li>
							<li role="presentation"><a style="border:0;" href="#rsvplink">RSVP</a></li>
							<li role="presentation"><a style="border:0;" href="#" data-toggle="modal" data-target="#legal-modal">Legal Resources</a></li>
						</ul>
					</div>
					<div class="col-sm-12 text-center">
						<p class="copyright">&copy;
							2012&#x2011;<? echo date("Y"); ?> Web Wed, LLC. All rights reserved. Provisional Patent Awarded. Service available only within the contiguous United States. A <a href="http://www.marriagelicensenow.com">state issued marriage license</a> is required to use this service. Use of this site and mentioned services constitutes that you agree to our terms of service and privacy policy.</a></p>
						<p class="copyright"><a href="#" data-toggle="modal" data-target="#privacy-modal">Privacy Policy</a> | <a href="#" data-toggle="modal" data-target="#terms-modal">Terms of Service</a> | <a href="#" data-toggle="modal" data-target="#disclaimer-modal">Disclaimer</a></p>
						<br>
					</div>
				</div>
			</div>
			<div id="go-to-top"> <a href="#banner"><i class="fa fa-angle-up"></i></a> </div>
		</div>
	</footer-->
</body>
</html>
