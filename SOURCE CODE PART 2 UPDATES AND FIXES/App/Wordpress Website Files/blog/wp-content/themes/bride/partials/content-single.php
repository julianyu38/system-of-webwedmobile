<?php
/**
 * This partial is used for displaying single post (or page) content
 *
 * @package Layers
 * @since Layers 1.0.0
 */

global $post, $layers_post_meta_to_display, $layers_page_title_shown;

// Make sure $layers_page_title_shown exists before we check it.
if ( ! isset( $layers_page_title_shown ) ) $layers_page_title_shown = FALSE;

do_action('layers_before_single_post');


// copy title
if ( ! $layers_page_title_shown ) { ?>
	<?php do_action('layers_before_single_post_title'); ?>

	<header <?php echo layers_wrapper_class( 'single_post_title', 'single-post-title' ); ?>>
	
		<?php do_action('layers_before_single_title'); ?>
			<h1 class="single-title-heading"><?php the_title(); ?></h1>
		<?php do_action('layers_after_single_title'); ?>
		
	</header>

	<?php do_action('layers_after_single_post_title'); ?>
	<?php

	// Record that we have shown page title - to avoid double titles showing.
	$layers_page_title_shown = TRUE;
}
// end copy title



// copy post meta
if( 'post' == get_post_type() ) {

	layers_post_meta( get_the_ID() );
	
}
// end copy post meta




// copy image

echo layers_post_featured_media( array( 'postid' => get_the_ID(), 'wrap_class' => 'thumbnail push-bottom', 'size' => 'large' ) );

// end copy image


// copy content
if ( '' != get_the_content() ) { ?>
	<?php do_action('layers_before_single_content'); ?>

	<?php if( 'template-blank.php' != get_page_template_slug() ) { ?>
		<div class="story">
	<?php } ?>

	<?php the_content(); ?>

	<?php /**
	* Display In-Post Pagination
	*/
	wp_link_pages( array(
		'link_before'   => '<span>',
		'link_after'    => '</span>',
		'before'        => '<p class="inner-post-pagination">' . __('<span>Pages:</span>', 'layerswp'),
		'after'     => '</p>'
	)); ?>

	<?php if( 'template-blank.php' != get_page_template_slug() ) { ?>
		</div>
	<?php } ?>

	<?php do_action('layers_after_single_content'); ?>
<?php } 
// end copy content




/**
* Display the Post Comments
*/
comments_template();

do_action('layers_after_single_post');