<?php
/**
 * This template is used for displaying posts in post lists
 *
 * @package Layers
 * @since Layers 1.0.0
 */

global $post, $layers_post_meta_to_display; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'push-bottom-large' ); ?>>


	<!--  copy title  -->
	<?php do_action('layers_before_list_post_title'); ?>
	
	<header class="blog-heading-title ">
		<?php do_action('layers_before_list_title'); ?>
		
			<h2 class="heading">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
		
		<?php do_action('layers_after_list_title'); ?>
		
	</header>
	<?php do_action('layers_after_list_post_title'); ?>
	<!--  end copy title  -->
	
	
	<!--  copy meta field  -->
	<?php do_action('layers_before_list_post_meta'); ?>
		<?php /**
		* Display the Post Meta
		*/
		layers_post_meta( get_the_ID(), NULL, 'footer', 'meta-info push-bottom' ); ?>
	<?php do_action('layers_after_list_post_meta'); ?>
	<!--  end copy meta field  -->
	
	
	<!--  copy image  -->
	<?php /**
	* Display the Featured Thumbnail
	*/
	echo layers_post_featured_media( array( 'postid' => get_the_ID(), 'wrap_class' => 'thumbnail push-bottom', 'size' => 'large' ) ); ?>

	<!--  end copy image  -->
	
	<!--  copy content  -->
	<?php if( '' != get_the_excerpt() || '' != get_the_content() ) { ?>
	
	
		<?php do_action('layers_before_list_post_content'); ?>
		
			<?php do_action('layers_list_post_content'); ?>
		
		<?php do_action('layers_after_list_post_content'); ?>
	<?php } ?>
	<!-- end copy content  -->
	
	
	
	
	<!--  copy readmore  -->
	<?php do_action('layers_before_list_read_more'); ?>
	
		<?php do_action('layers_list_read_more'); ?>
	
	<?php do_action('layers_after_list_read_more'); ?>
	<!--  end copy readmore  -->
	
	
	
</article>