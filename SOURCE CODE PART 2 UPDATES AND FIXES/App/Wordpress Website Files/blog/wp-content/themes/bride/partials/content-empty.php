<?php
/**
 * This partial is used for displaying empty page content
 *
 * @package Layers
 * @since Layers 1.0.0
 */ ?>
 
 
 <div class="not-found-area">
	<header class="not-found-title">
		<?php do_action('layers_before_single_title'); ?>
		
			<h1 class="subtitle"><?php esc_html_e( 'No posts found' , 'bride' ) ; ?></h1>
			
		<?php do_action('layers_after_single_title'); ?>
	</header>

	<div class="not-found-subtitle">

		<p><?php esc_html_e( 'Use the search form below to find the page you\'re looking for:' , 'bride' ) ; ?></p>
		<?php get_search_form(); ?>
		
	</div>
</div>
