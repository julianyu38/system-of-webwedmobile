<?php
/**
 * This partial is used for displaying the Site Header when in archive pages
 *
 * @package Layers
 * @since Layers 1.0.0
 */

$show_page  = get_post_meta( get_the_ID(),'_bride_breadcrumbs', true );  
?>

<!-- breadcrumb-area start -->
<?php if(!is_front_page() && is_page())  :  ?>
  <?php if( $show_page == 0 ) { ?>
    <div class="breadcrumb-area pages-area">
        <div class="container">
            <div class="grid">
				<div class="column span-12">
					<div class="breadcrumb-padding pages-p">
						<div class="breadcrumb-list">
							<?php bride_breadcrumbs();?>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
<?php } ?>

<?php elseif( is_archive() ) : ?>
	<div class="breadcrumb-area archive-page">
        <div class="container">
            <div class="grid">
				<div class="column span-12">
					<div class="breadcrumb-padding pages-p">
						<div class="breadcrumb-list">
							<?php bride_breadcrumbs();?>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>
	
<?php elseif(is_single()) : ?>
	<?php if( $show_page == 0 ) { ?>
		<div class="breadcrumb-area single-page">
			<div class="container">
				<div class="grid">
					<div class="column span-12">
						<div class="breadcrumb-padding pages-p">
							<div class="breadcrumb-list">
								<?php bride_breadcrumbs();?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	
<?php elseif(is_404()) : ?>

	<div class="breadcrumb-area notfound-page">
		<div class="container">
			<div class="grid">
				<div class="column span-12">
					<div class="breadcrumb-padding pages-p">
						<div class="breadcrumb-list">
							<?php bride_breadcrumbs();?>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<?php elseif(is_search()) : ?>
    <div class="breadcrumb-area search-page">
        <div class="container">
            <div class="grid">
				<div class="column span-12">
					<div class="breadcrumb-padding pages-p">
						<div class="breadcrumb-list">
							<?php bride_breadcrumbs();?>
						</div>
					</div>
				</div>
            </div>
        </div>
    </div>

<?php else : ?>

<?php endif; ?>
<!-- breadcrumb-area end -->