(function ($) {
"use strict";

/*--
Hero Slider
-------------------------*/
$('.hero-slider').slick({
arrows: false,
autoplay: false,
dots: true,
fade: true,
infinite: true,
slidesToShow: 1,
responsive: [
	{
	  breakpoint: 600,
	  settings: {
		dots: false,
		autoplay: false,
		autoplaySpeed: 7000,
	  }
	},
]
});


/*----------------------------
Owl active
------------------------------ */  
$(".brand-carousel").owlCarousel({
	autoPlay: false, 
	slideSpeed:2000,
	pagination:false,
	navigation:false,	  
	items : 6,
	itemsDesktop : [1199,3],
	itemsDesktopSmall : [980,2],
	itemsTablet: [768,2],
	itemsMobile : [479,1],
});
/*----------------------------
Blog Carousel
------------------------------ */  
$(".blog-carousel").owlCarousel({
	autoPlay: false, 
	slideSpeed:2000,
	pagination:false,
	navigation:true,	  
	items : 4,
	navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	itemsDesktop : [1199,4],
	itemsDesktopSmall : [980,2],
	itemsTablet: [768,2],
	itemsMobile : [479,1],
});

/*--------------------------------
	Testimonial Slick Carousel
-----------------------------------*/
    $('.testimonial-text-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        draggable: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

/*------------------------------------
	Testimonial Slick Carousel as Nav
--------------------------------------*/
    $('.testimonial-image-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.testimonial-text-slider',
        centerMode: true,
        vertical: true,
        verticalSwiping: true,
        verticalScrolling: true,
        autoplay: false,
        autoplaySpeed: 5000,
        dots: false,
        draggable: true,
        arrows: false,
        focusOnSelect: true,
        centerPadding: '10px',
        responsive: 
        [
            {
              breakpoint: 450,
              settings: {
                dots: false,
                slidesToShow: 3,  
                centerPadding: '0px',
                }
            },
            {
              breakpoint: 420,
              settings: {
                autoplay: true,
                dots: false,
                slidesToShow: 1,
                centerMode: false,
                }
            }
        ]
    });

/*--------------------------
    Countdown
---------------------------- */	
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
        $this.html(event.strftime('<div class="cdown days"><span class="counting">%-D</span>days</div><div class="cdown hours"><span class="counting">%-H</span>hrs</div><div class="cdown minutes"><span class="counting">%M</span>mins</div><div class="cdown seconds"><span class="counting">%S</span>secs</div>'));
        });
    });	

/*--------------------------------
	Testimonial Slick Carousel
-----------------------------------*/
    $('.testimonial-text-slider-two').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        draggable: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    
/*------------------------------------
	Testimonial Slick Carousel as Nav
--------------------------------------*/
    $('.testimonial-image-slider-two').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.testimonial-text-slider-two',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        centerPadding: '10px',
        responsive: [
            {
              breakpoint: 450,
              settings: {
                dots: false,
                slidesToShow: 3,  
                centerPadding: '0px',
                }
            },
            {
              breakpoint: 420,
              settings: {
                autoplay: true,
                dots: false,
                slidesToShow: 1,
                centerMode: false,
                }
            }
        ]
    }); 











/*--------------------------
Countdown
---------------------------- */	
$('[data-countdown]').each(function() {
	var $this = $(this), finalDate = $(this).data('countdown');
	$this.countdown(finalDate, function(event) {
	$this.html(event.strftime('<div class="cdown days"><span class="counting">%-D</span>days</div><div class="cdown hours"><span class="counting">%-H</span>hrs</div><div class="cdown minutes"><span class="counting">%M</span>mins</div><div class="cdown seconds"><span class="counting">%S</span>secs</div>'));
	});
});	

/*--------------------------
ScrollUp
---------------------------- */	
$.scrollUp({
	scrollText: '<i class="fa fa-angle-up"></i>',
	easingType: 'linear',
	scrollSpeed: 900,
	animation: 'fade'
}); 	   

/*--------------------------------
Testimonial Small Carousel
-----------------------------------*/
$('.testimonial-small-text-slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: false,
	draggable: false,
	fade: true,
	asNavFor: '.slider-nav'
});
/*------------------------------------
Testimonial Small Carousel as Nav
--------------------------------------*/
$('.testimonial-small-image-slider').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	asNavFor: '.testimonial-small-text-slider',
	dots: false,
	arrows: false,
	centerMode: true,
	focusOnSelect: true,
	centerPadding: '10px',
	responsive: [
		{
		  breakpoint: 450,
		  settings: {
			dots: false,
			slidesToShow: 3,  
			centerPadding: '0px',
			}
		},
		{
		  breakpoint: 420,
		  settings: {
			autoplay: true,
			dots: false,
			slidesToShow: 1,
			centerMode: false,
			}
		}
	]
});

/*--------------------------
	Portfolio Isotope
---------------------------- */
	$('.new').imagesLoaded( function() {
		if($.fn.isotope){
			var $portfolio = $('.new');
			$portfolio.isotope({
				itemSelector: '.grid-item',
				filter: '*',
				resizesContainer: true,
				layoutMode: 'masonry',
				transitionDuration: '0.8s'			
			});
			
			
			
			$('.filter_menu li').on('click', function(){
				$('.filter_menu li').removeClass('current_menu_item');
				$(this).addClass('current_menu_item');
				var selector = $(this).attr('data-filter');
				$portfolio.isotope({
					filter: selector,
				});
			});
		};
	});	


/*--------------------------
Venubox
---------------------------- */	
$('.venobox').venobox({    
	border: '10px',          
	titleattr: 'data-title',  
	numeratio: true,           
	infinigall: true      
});

 // Portfolio Isotope
 $('.blog_masonry').imagesLoaded( function() {
  if($.fn.isotope){
   var $portfolio = $('.blog_masonry');
   $portfolio.isotope({
    itemSelector: '.grid_item',
    filter: '*',
    resizesContainer: true,
    layoutMode: 'masonry',
    transitionDuration: '0.8s'   
   });
  };
 });


/*--------------------------
WOW
---------------------------- */		
new WOW().init();

})(jQuery); 