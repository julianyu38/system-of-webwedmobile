#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-05-18 09:38+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: footer.php:3
msgid "Back to top"
msgstr ""

#: functions.php:162
msgid "bride Theme"
msgstr ""

#: functions.php:163
msgid "bride Documentation"
msgstr ""

#: functions.php:233
msgid "(Edit)"
msgstr ""

#: functions.php:240
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: functions.php:246
msgid "Your comment is awaiting moderation."
msgstr ""

#. Name of the template
msgid "Blog"
msgstr ""

#: search.php:35 includes/breadcrumb.php:15 partials/content-empty.php:14
msgid "No posts found"
msgstr ""

#: search.php:37
msgid ""
"There are no posts which match your query, please try a different search "
"term."
msgstr ""

#: comments.php:30
msgid "&larr;"
msgstr ""

#: comments.php:31
msgid "&rarr;"
msgstr ""

#: comments.php:39
msgid "Comments are closed."
msgstr ""

#: includes/class-activation.php:13
msgid "Bride Layers Extension"
msgstr ""

#: includes/class-activation.php:21
msgid "Bride Metaboxes"
msgstr ""

#: includes/class-activation.php:29
msgid "Layers Pro Notices Unset"
msgstr ""

#: includes/class-activation.php:37
msgid "Contact form 7"
msgstr ""

#: includes/class-activation.php:42
msgid "Cmb2"
msgstr ""

#: includes/class-activation.php:47
msgid "WooCommerce"
msgstr ""

#: includes/class-activation.php:52
msgid "WooCommerce PayPal Express Checkout Gateway"
msgstr ""

#: includes/custom-footer.php:18
msgid "Footer Top Styling"
msgstr ""

#: includes/custom-footer.php:34
msgid "Show Footer Top"
msgstr ""

#: includes/custom-footer.php:38 includes/custom-footer.php:52
#: includes/custom-footer.php:697
msgid "Yes"
msgstr ""

#: includes/custom-footer.php:39 includes/custom-footer.php:53
msgid "no"
msgstr ""

#: includes/custom-footer.php:48
msgid "Show logo"
msgstr ""

#: includes/custom-footer.php:57 includes/custom-footer.php:946
msgid "Upload your logo"
msgstr ""

#: includes/custom-footer.php:67 includes/custom-footer.php:951
msgid "Logo Size"
msgstr ""

#: includes/custom-footer.php:70 includes/custom-footer.php:954
msgid "Auto"
msgstr ""

#: includes/custom-footer.php:71 includes/custom-footer.php:955
msgid "Small"
msgstr ""

#: includes/custom-footer.php:72 includes/custom-footer.php:956
msgid "Medium"
msgstr ""

#: includes/custom-footer.php:73 includes/custom-footer.php:957
msgid "Large"
msgstr ""

#: includes/custom-footer.php:74 includes/custom-footer.php:958
msgid "Massive"
msgstr ""

#: includes/custom-footer.php:75 includes/custom-footer.php:959
msgid "Custom"
msgstr ""

#: includes/custom-footer.php:84 includes/custom-footer.php:964
msgid "Logo Custom Size"
msgstr ""

#: includes/custom-footer.php:96 includes/custom-footer.php:598
msgid "Padding Top (px)"
msgstr ""

#: includes/custom-footer.php:109 includes/custom-footer.php:610
msgid "Padding Bottom (px)"
msgstr ""

#: includes/custom-footer.php:127 includes/custom-footer.php:219
#: includes/custom-footer.php:311
msgid "Select Information Type"
msgstr ""

#: includes/custom-footer.php:131 includes/custom-footer.php:223
#: includes/custom-footer.php:315 includes/custom-footer.php:448
#: includes/custom-footer.php:535 includes/custom-footer.php:725
#: includes/custom-footer.php:859
msgid "None"
msgstr ""

#: includes/custom-footer.php:132 includes/custom-footer.php:224
#: includes/custom-footer.php:316
msgid "Plain Text"
msgstr ""

#: includes/custom-footer.php:133 includes/custom-footer.php:225
#: includes/custom-footer.php:317
msgid "Email"
msgstr ""

#: includes/custom-footer.php:134 includes/custom-footer.php:226
#: includes/custom-footer.php:318
msgid "Phone"
msgstr ""

#: includes/custom-footer.php:140 includes/custom-footer.php:162
#: includes/custom-footer.php:184 includes/custom-footer.php:232
#: includes/custom-footer.php:254 includes/custom-footer.php:276
#: includes/custom-footer.php:324 includes/custom-footer.php:347
#: includes/custom-footer.php:370
msgid "Icon Name"
msgstr ""

#: includes/custom-footer.php:147 includes/custom-footer.php:169
#: includes/custom-footer.php:191 includes/custom-footer.php:239
#: includes/custom-footer.php:261 includes/custom-footer.php:283
#: includes/custom-footer.php:331 includes/custom-footer.php:354
#: includes/custom-footer.php:377
msgid ""
"You want more icons <a  href=\"http://fontawesome.io/icons\" "
"target=\"_blank\">Click here</a>."
msgstr ""

#: includes/custom-footer.php:151 includes/custom-footer.php:243
#: includes/custom-footer.php:335
msgid "Insert Your Text"
msgstr ""

#: includes/custom-footer.php:173 includes/custom-footer.php:265
#: includes/custom-footer.php:358
msgid "Insert Your Email"
msgstr ""

#: includes/custom-footer.php:195 includes/custom-footer.php:287
#: includes/custom-footer.php:381
msgid "Insert Your Country Code"
msgstr ""

#: includes/custom-footer.php:205 includes/custom-footer.php:297
#: includes/custom-footer.php:391
msgid "Insert Your Phone Number"
msgstr ""

#: includes/custom-footer.php:407
msgid "Information Customize Option"
msgstr ""

#: includes/custom-footer.php:408
msgid "Customize options for your footer top information settings."
msgstr ""

#: includes/custom-footer.php:413
msgid "Select Information Design"
msgstr ""

#: includes/custom-footer.php:417
msgid "Default Style"
msgstr ""

#: includes/custom-footer.php:418
msgid "Custom Style"
msgstr ""

#: includes/custom-footer.php:423
msgid "Font size (px)"
msgstr ""

#: includes/custom-footer.php:435
msgid "Text Color"
msgstr ""

#: includes/custom-footer.php:444 includes/custom-footer.php:1111
msgid "Text Transform"
msgstr ""

#: includes/custom-footer.php:449 includes/custom-footer.php:1117
msgid "Capitalize"
msgstr ""

#: includes/custom-footer.php:450 includes/custom-footer.php:1116
msgid "Uppercase"
msgstr ""

#: includes/custom-footer.php:451 includes/custom-footer.php:1118
msgid "Lowercase"
msgstr ""

#: includes/custom-footer.php:452
msgid "Full-width"
msgstr ""

#: includes/custom-footer.php:453
msgid "Inherit"
msgstr ""

#: includes/custom-footer.php:454
msgid "Initial"
msgstr ""

#: includes/custom-footer.php:455
msgid "Unset"
msgstr ""

#: includes/custom-footer.php:464
msgid "Icon Position"
msgstr ""

#: includes/custom-footer.php:468
msgid "Icon on Left"
msgstr ""

#: includes/custom-footer.php:469
msgid "Icon on Top"
msgstr ""

#: includes/custom-footer.php:478
msgid "Icon Size (px)"
msgstr ""

#: includes/custom-footer.php:490 includes/custom-footer.php:635
msgid "Margin Bottom (px)"
msgstr ""

#: includes/custom-footer.php:511
msgid "Footer Top Setting"
msgstr ""

#: includes/custom-footer.php:512
msgid "Customize options for your footer top settings."
msgstr ""

#: includes/custom-footer.php:520
msgid "Content Alignment"
msgstr ""

#: includes/custom-footer.php:524 includes/custom-footer.php:845
msgid "Left"
msgstr ""

#: includes/custom-footer.php:525 includes/custom-footer.php:847
msgid "Center"
msgstr ""

#: includes/custom-footer.php:526 includes/custom-footer.php:846
msgid "Right"
msgstr ""

#: includes/custom-footer.php:531 includes/custom-footer.php:721
#: includes/custom-footer.php:855
msgid "Select Background Type"
msgstr ""

#: includes/custom-footer.php:536 includes/custom-footer.php:542
#: includes/custom-footer.php:726 includes/custom-footer.php:732
#: includes/custom-footer.php:860 includes/custom-footer.php:866
msgid "Background Color"
msgstr ""

#: includes/custom-footer.php:537 includes/custom-footer.php:727
#: includes/custom-footer.php:861
msgid "Background Image"
msgstr ""

#: includes/custom-footer.php:551 includes/custom-footer.php:741
#: includes/custom-footer.php:874
msgid "Upload Background Image"
msgstr ""

#: includes/custom-footer.php:561 includes/custom-footer.php:751
#: includes/custom-footer.php:884
msgid "Overlay Color"
msgstr ""

#: includes/custom-footer.php:570 includes/custom-footer.php:760
#: includes/custom-footer.php:893
msgid "Overlay Opacity"
msgstr ""

#: includes/custom-footer.php:588
msgid "Padding / Margin"
msgstr ""

#: includes/custom-footer.php:592
msgid "Default Option"
msgstr ""

#: includes/custom-footer.php:593
msgid "Custom Option"
msgstr ""

#: includes/custom-footer.php:623
msgid "Margin Top (px)"
msgstr ""

#: includes/custom-footer.php:670
msgid "Footer Styling"
msgstr ""

#: includes/custom-footer.php:686
msgid "Footer Setting"
msgstr ""

#: includes/custom-footer.php:687
msgid "Customize options for your footer settings."
msgstr ""

#: includes/custom-footer.php:692
msgid "Widget Areas Show/Hide"
msgstr ""

#: includes/custom-footer.php:693
msgid ""
"Choose how many widget areas apear in the footer. Go here to <a "
"class=\"customizer-link\" href=\"#accordion-panel-widgets\">customize footer "
"widgets</a>."
msgstr ""

#: includes/custom-footer.php:696
msgid "No"
msgstr ""

#: includes/custom-footer.php:702
msgid "Widget Title Color"
msgstr ""

#: includes/custom-footer.php:707
msgid "Widget Content Color"
msgstr ""

#: includes/custom-footer.php:712
msgid "Widget Title font size"
msgstr ""

#: includes/custom-footer.php:774 includes/custom-footer.php:905
msgid "Padding Top"
msgstr ""

#: includes/custom-footer.php:781 includes/custom-footer.php:913
msgid "Padding Bottom"
msgstr ""

#: includes/custom-footer.php:788 includes/custom-footer.php:921
msgid "Margin Top"
msgstr ""

#: includes/custom-footer.php:796 includes/custom-footer.php:929
msgid "Margin Bottom"
msgstr ""

#: includes/custom-footer.php:819
msgid "Copyright Styling"
msgstr ""

#: includes/custom-footer.php:834
msgid "Copyright Setting"
msgstr ""

#: includes/custom-footer.php:835
msgid "Customize options for your copyright section settings."
msgstr ""

#: includes/custom-footer.php:841
msgid "Content Align"
msgstr ""

#: includes/custom-footer.php:941
msgid "Show footer logo"
msgstr ""

#: includes/custom-footer.php:981
msgid "Social Option"
msgstr ""

#: includes/custom-footer.php:982
msgid "Design Option to change the styles and appearance of your social icons."
msgstr ""

#: includes/custom-footer.php:988
msgid "Show Social media"
msgstr ""

#: includes/custom-footer.php:993
msgid "Icon Color"
msgstr ""

#: includes/custom-footer.php:998
msgid "Hover Color"
msgstr ""

#: includes/custom-footer.php:1003
msgid "background"
msgstr ""

#: includes/custom-footer.php:1008
msgid "Hover background"
msgstr ""

#: includes/custom-footer.php:1013
msgid "Border Color"
msgstr ""

#: includes/custom-footer.php:1018
msgid "Border Hover Color"
msgstr ""

#: includes/custom-footer.php:1023
msgid "Font Size (px)"
msgstr ""

#: includes/custom-footer.php:1030
msgid "Icon width (px)"
msgstr ""

#: includes/custom-footer.php:1037
msgid "Border size (px)"
msgstr ""

#: includes/custom-footer.php:1044
msgid "Rounded Corner Size (%)"
msgstr ""

#: includes/custom-footer.php:1051
msgid "Link Spacing (px)"
msgstr ""

#: includes/custom-footer.php:1065
msgid "Credit Cards Option"
msgstr ""

#: includes/custom-footer.php:1066
msgid "Customize options for your payment section settings. "
msgstr ""

#: includes/custom-footer.php:1075
msgid "Footer Menu style"
msgstr ""

#: includes/custom-footer.php:1076
msgid "Customize options for your footer menu style. "
msgstr ""

#: includes/custom-footer.php:1082
msgid "Show Footer Menu"
msgstr ""

#: includes/custom-footer.php:1087 includes/custom-footer.php:1165
msgid "Font Size"
msgstr ""

#: includes/custom-footer.php:1095
msgid "Text color"
msgstr ""

#: includes/custom-footer.php:1100 includes/custom-footer.php:1105
msgid "Hover Text color"
msgstr ""

#: includes/custom-footer.php:1115
msgid "Normal"
msgstr ""

#: includes/custom-footer.php:1122
msgid "Link Spacing"
msgstr ""

#: includes/custom-footer.php:1154
msgid "Copyright Text"
msgstr ""

#: includes/custom-footer.php:1161
msgid "Copyright text Setting"
msgstr ""

#: includes/custom-footer.php:1162
msgid "Change the font size and color of your copyright test."
msgstr ""

#: includes/custom-footer.php:1172
msgid "Copyright color"
msgstr ""

#: includes/theme-help.php:15
msgid "Thanks for Installing the Grant Foundation Child Theme!"
msgstr ""

#: includes/theme-help.php:19
msgid ""
"The following links will guide you to customize your theme in just the way "
"you want. Just visit the links and explore."
msgstr ""

#: includes/theme-help.php:21
msgid "Online Documentation"
msgstr ""

#: includes/theme-help.php:24
msgid "Layerswp Installation Guide."
msgstr ""

#: includes/theme-help.php:28
msgid ""
"Grant Foundation is a child theme of layerswp parent theme. For using grant "
"foundation child you will have to install layerswp theme first. Visit the "
"link below to see layerswp installation guide."
msgstr ""

#: includes/theme-help.php:30
msgid "Layerswp Installation Guide"
msgstr ""

#: includes/breadcrumb.php:9
msgid "Home"
msgstr ""

#: includes/breadcrumb.php:10
#, php-format
msgid "Archive by Category \"%s\""
msgstr ""

#: includes/breadcrumb.php:11
#, php-format
msgid "Archive for \"%s"
msgstr ""

#: includes/breadcrumb.php:12
#, php-format
msgid "Search Results for \"%s\" Query"
msgstr ""

#: includes/breadcrumb.php:13
#, php-format
msgid "Posts Tagged \"%s\""
msgstr ""

#: includes/breadcrumb.php:14
#, php-format
msgid "Articles Posted by %s"
msgstr ""

#: includes/breadcrumb.php:121
msgid "Page"
msgstr ""

#: includes/presets.php:10
msgid "Bride - Home Layout 1"
msgstr ""

#: includes/presets.php:16
msgid "Bride - Home Layout 2"
msgstr ""

#: includes/presets.php:22
msgid "Bride - Home Layout 3"
msgstr ""

#: includes/presets.php:28
msgid "Bride - Our Story"
msgstr ""

#: includes/presets.php:34
msgid "Bride - Gallery"
msgstr ""

#: includes/presets.php:40
msgid "Bride - Contact"
msgstr ""

#: partials/content-empty.php:21
msgid "Use the search form below to find the page you're looking for:"
msgstr ""

#: partials/footer-standard.php:124 partials/footer-standard.php:169
#: partials/footer-standard.php:213
msgid "mailto:"
msgstr ""

#: partials/footer-standard.php:126 partials/footer-standard.php:171
#: partials/footer-standard.php:215
msgid "tel:"
msgstr ""

#. Name of the theme
msgid "Bride"
msgstr ""

#. Description of the theme
msgid "Bride Wedding Oganizer Company."
msgstr ""

#. Author of the theme
msgid "HasTech"
msgstr ""

#. Author URI of the theme
msgid "http://bootexperts.com/"
msgstr ""
