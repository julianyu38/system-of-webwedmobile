<?php
/*
* Plugin Name: layers Pro Notices Unset
* Plugin URI: http://bootexperts.com/
* Description: This plugin is Unset layers Pro Notices.
* Version: 1.0
* Author: HasTech
* Author URI: http://bootexperts.com/
* Text Domain: lnpa
* Domain Path: /lang/
*
*/


define( 'LPNU_EXTENSION_VER', '1.0.0' );
define( 'LPNU_EXTENSION_DIR' , trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'LPNU_EXTENSION_URI' , trailingslashit( plugin_dir_url( __FILE__ ) ) );

require_once( LPNU_EXTENSION_DIR. 'includes/lpnu-class.php' );


// Instantiate Plugin
if ( !function_exists('lpnu_extension_init') ) {
	function lpnu_extension_init() {

		global $Lpnu_Layers_Extension;

		$Lpnu_Layers_Extension = Lpnu_Layers_Extension::get_instance();
		// Localization		
		
	}
}
add_action( 'plugins_loaded', 'lpnu_extension_init' );












// Unset layer Pro Notices buttons upsell
add_filter( 'layerswp_background_component_args' , 'hastech_unset_pro_notices_bg_parallax', 100 );
if( !function_exists( 'hastech_unset_pro_notices_bg_parallax' ) ) {
   function hastech_unset_pro_notices_bg_parallax( $defaults){

   		//background parallax
   		unset($defaults['elements']['background-parallax']);
	    return $defaults;
    }
}


// Unset layer Pro Notices buttons upsell
add_filter( 'layerswp_button_colors_component_args' , 'hastech_unset_pro_notices_btn', 100 );
if( !function_exists( 'hastech_unset_pro_notices_btn' ) ) {
   function hastech_unset_pro_notices_btn( $defaults){

   		//Side buttons upsell
   		unset($defaults['elements']['buttons-upsell']);
	    return $defaults;
    }
}


// Unset layer Pro Notices customiser sections
add_filter( 'layers_customizer_sections' , 'hastech_unset_pro_notices_sections', 100 );
if( !function_exists( 'hastech_unset_pro_notices_sections' ) ) {
   function hastech_unset_pro_notices_sections( $sections){

   		// Site Settings 
   		unset($sections['buttons']);
   		unset($sections['blog-styling']);

	    return $sections;
    }
}


// Unset layer Pro Notices customiser controls
add_filter( 'layers_customizer_controls' , 'hastech_unset_pro_notices_controls', 100 );
if( !function_exists( 'hastech_unset_pro_notices_controls' ) ) {
   function hastech_unset_pro_notices_controls( $controls){

   		// Site Settings -> Logo & Title
   		unset($controls['title_tagline']['logo-upsell-layers-pro']);

   		// Site Settings -> Layout
		unset($controls['header-layout']['header-upsell-layers-pro']);
		unset($controls['header-menu-styling']['menu-upsell-layers-pro']);

		// Site Settings -> Sidebars
		unset($controls['blog-single']['blog-single-upsell-layers-pro']);
		unset($controls['blog-archive']['blog-archive-upsell-layers-pro']);

		

		// Site Settings -> Colors
		unset($controls['site-colors']['colors-upsell-layers-pro']);


		// Footer -> Layout
		unset($controls['footer-layout']['footer-upsell-layers-pro']);
		unset($controls['footer-layout']['show-layers-badge']);


	    return $controls;
    }
}

