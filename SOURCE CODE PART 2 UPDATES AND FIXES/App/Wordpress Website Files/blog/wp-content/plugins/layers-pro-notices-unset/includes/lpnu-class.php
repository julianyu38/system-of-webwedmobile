<?php
/**
* layers Pro Notices Unset Extension
* http://docs.layerswp.com/create-an-extension-setup-your-plugin-class/#setup-class
*/
if( !class_exists( 'Lpnu_Layers_Extension' ) ) {
	class Lpnu_Layers_Extension {

		// Setup Instance
		private static $_instance=null;
		
		public static function get_instance() {
			if ( is_null( self::$_instance ) )
				
				self::$_instance = new Lpnu_Layers_Extension();

			return self::$_instance;
		}
		
		// Constructor		
		private function __construct() {							
			// Register custom widgets
			add_action( 'widgets_init' , array( $this, 'lpnu_register_widgets' ), 50 );	
		}

		// Register Widgets		
		public function lpnu_register_widgets(){
			
		}
	
	} // END Class
	
} 





// Script
function hastech_unset_pro_notices_sctipts(){
	wp_enqueue_script('hastech-unset-pro-notices-sctipts', LPNU_EXTENSION_URI.'/assets/js/layers-pro-notices-unset.js', array('jquery', 'customize-preview') );
}
add_action('admin_enqueue_scripts', 'hastech_unset_pro_notices_sctipts');


// Style
if( ! function_exists( 'layers_pro_notices_unset' ) ) {
	function layers_pro_notices_unset(){
		wp_enqueue_style(
			'pro-notices-unset',
			LPNU_EXTENSION_URI . '/assets/css/layers-pro-notices-unset.css',
			array(), LPNU_EXTENSION_VER
		);
	}
}
add_action( 'admin_enqueue_scripts' , 'layers_pro_notices_unset' );



