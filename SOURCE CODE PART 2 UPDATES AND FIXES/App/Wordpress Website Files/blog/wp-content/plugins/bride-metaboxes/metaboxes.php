<?php
/*
**	Setting up custom fields for custom post types belongs to bride layers child theme
*/
if ( !function_exists('register_bride_metabox') ) {
	function register_bride_metabox() {
		
		$prefix = '_bride_';
		
		//page metabox
		$page_breadcrumb = new_cmb2_box( array(
			'id'            => $prefix . 'pageid1',
			'title'         => __( 'Breadcumb Option', 'grant' ),
			'object_types'  => array( 'post','page' ), // Post type
			'priority'   => 'high',
		) );
		$page_breadcrumb->add_field( array(
			'name'    => 'Breadcrumb',
			'id'      => $prefix . 'breadcrumbs',
			'type'    => 'radio_inline',
			'options' => array(
				'0' => __( 'Show breadcrumb', 'grant' ),
				'1'   => __( 'Hide breadcrumb', 'grant' ),
			),
			'default' =>0,
		) );
		$page_breadcrumb->add_field(array(
			'name' => __( 'Breadcrumb Padding Top', 'grant' ),
			'id'   => $prefix .'bread_padding_top',
			'desc'  => __( 'Set padding ex-"100"', 'grant' ),		
			'type'  => 'text',
		) );
		$page_breadcrumb->add_field(array(
			'name' => __( 'Breadcrumb Padding Bottom', 'grant' ),
			'id'   => $prefix .'bread_padding_bottom',
			'desc'  => __( 'Set padding ex-"100"', 'grant' ),		
			'type'  => 'text',
		) );				
		$page_breadcrumb->add_field(array(
			'name' => __( 'Page Breadcrumb Image', 'grant' ),
			'id'   => $prefix .'pageimagess',
			'desc'       => __( 'insert image here', 'grant' ),		
			'type' => 'file',
		) );
		$page_breadcrumb->add_field(array(
			'name' => __( 'Page Background', 'grant' ),
			'id'   => $prefix .'page_background',
			'desc'  => __( 'Set background color', 'grant' ),		
			'type'  => 'colorpicker',
			'default' => '#dcdcdc',
		) );				
		$page_breadcrumb->add_field(array(
			'name' => __( 'Page Text Color', 'grant' ),
			'id'   => $prefix .'text_color_page',
			'desc'       => __( 'Set title color', 'grant' ),		
			 'type'    => 'colorpicker',
			'default' => '#5e5e5e',
		) );
		$page_breadcrumb->add_field(array(
			'name' => __( 'Current Page Text Color', 'grant' ),
			'id'   => $prefix .'current_color_page',
			'desc'       => __( 'Set Current color', 'grant' ),		
			 'type'    => 'colorpicker',
			'default' => '#686868',
		) );				
		$page_breadcrumb->add_field( array(
			'name'             => 'Text Align',
			'desc'             => 'Select an option',
			'id'   => $prefix .'page_text_align',
			'type'             => 'select',
			'show_option_none' => true,
			'default'          => 'center',
			'options'          => array(
				'left' => __( 'Align Left', 'grant' ),
				'center'   => __( 'Align Middle', 'grant' ),
				'right'     => __( 'Alige Right', 'grant' ),
			),
		) );
		$page_breadcrumb->add_field( array(
			'name'             => 'Text Transform',
			'desc'             => 'Select an option',
			'id'   => $prefix .'page_text_transform',
			'type'             => 'select',
			'show_option_none' => true,
			'default'          => 'uppercase',
			'options'          => array(
				'lowercase' => __( 'Transform lowercase', 'grant' ),
				'uppercase'   => __( 'Transform uppercase', 'grant' ),
				'capitalize'     => __( 'Transform capitalize', 'grant' ),
			),
		) );


		
		//post metabox
		$postbreadcrum = new_cmb2_box( array(
			'id'            => $prefix . 'post_bread',
			'title'         => __( 'Breadcumb Option', 'bride' ),
			'object_types'  => array( 'post', ), // Post type
			'priority'   => 'high',
		) );
		$postbreadcrum->add_field( array(
			'name'    => 'Single Page Breadcrumb',
			'id'      => $prefix . 'post_bread',
			'type'    => 'radio_inline',
			'options' => array(
				'0' => __( 'Show breadcrumb', 'bride' ),
				'1'   => __( 'Hide breadcrumb', 'bride' ),
			),
		) );	
		$postbreadcrum->add_field(array(
			'name' => __( 'Insert Breadcrumb Image', 'bride' ),
			'id'   => $prefix .'post_image',
			'desc'       => __( 'insert image here', 'bride' ),		
			'type' => 'file',
		) );							
		$postbreadcrum->add_field(array(
			'name' => __( 'Section Gradient Overlay Color', 'bride' ),
			'id'   => $prefix .'section_gradien_teft_overlay_color',
			'desc'       => __( 'Section Gradient Left Overlay Color ex:- #9f8447', 'bride' ),	
			'default'=>'#71b100',	
			'type' => 'colorpicker',
		) );
		$postbreadcrum->add_field(array(
			'name' => __( 'Section Gradient Overlay Color', 'bride' ),
			'id'   => $prefix .'section_gradien_right_overlay_color',
			'desc'       => __( 'Section Gradient Right Overlay Color ex:- #9f8447', 'bride' ),	
			'default'=>'#0071b0',		
			'type' => 'colorpicker',
		) );

		//Slider Metabox
		$htslider = new_cmb2_box( array(
			'id'            => $prefix . 'htsliders',
			'title'         => __( 'Sliders Option', 'bride' ),
			'object_types'  => array( 'ht_slider', ), // Post type
			'priority'   => 'high',
		) );
		$htslider->add_field(array(
			'name' => __( 'Heading Text Color', 'bride' ),
			'id'   => $prefix .'ht_title_color',
			'desc'       => __( 'Insert title color ex:- #ffffff', 'bride' ),		
			'type' => 'colorpicker',
		) );
		$htslider->add_field(array(
			'name' => __( 'Heading Text Size', 'bride' ),
			'id'   => $prefix .'ht_title_size',
			'desc'       => __( 'Insert title Size ex:- 15px', 'bride' ),		
			'type' => 'text',
		) );				
		$htslider->add_field(array(
			'name' => __( 'Heading Line Height', 'bride' ),
			'id'   => $prefix .'ht_title_lineh',
			'desc'       => __( 'Insert line height ex:- 45px', 'bride' ),		
			'type' => 'text',
			'default'=>'45px'
		) );				

		$htslider->add_field( array(
			'name'       => __( 'Sub Title', 'textdomain' ),
			'desc'       => __( 'insert name here', 'textdomain' ),
			'id'         => $prefix . 'sub_title',
			'type'       => 'text',
		) );
		$htslider->add_field(array(
			'name' => __( 'Sub Heading Text Color', 'bride' ),
			'id'   => $prefix .'ht_subtitle_color',
			'desc'       => __( 'Insert Sub title color ex:- #ffffff', 'bride' ),		
			'type' => 'colorpicker',
		) );
		$htslider->add_field(array(
			'name' => __( 'Sub Heading Text Size', 'bride' ),
			'id'   => $prefix .'ht_subtitle_size',
			'desc'       => __( 'Insert sub title Size ex:- 15px', 'bride' ),		
			'type' => 'text',
		) );				
		$htslider->add_field( array(
			'name'       => __( 'Paragraph Text', 'textdomain' ),
			'desc'       => __( 'insert paragraph here', 'textdomain' ),
			'id'         => $prefix . 'paragraph_text',
			'type'       => 'textarea',
		) );
		$htslider->add_field(array(
			'name' => __( 'Paragraph Text Color', 'bride' ),
			'id'   => $prefix .'paragraph_color',
			'desc'       => __( 'Insert Paragraph color ex:- #ffffff', 'bride' ),		
			'type' => 'colorpicker',
		) );
		$htslider->add_field(array(
			'name' => __( 'Paragraph Text Size', 'bride' ),
			'id'   => $prefix .'Paragraph_size',
			'desc'       => __( 'Insert paragraph Size ex:- 15px', 'bride' ),		
			'type' => 'text',
		) );					
		
		$htslider->add_field( array(
			'name'       => __( 'Button Text', 'textdomain' ),
			'desc'       => __( 'insert button text here ex:- readmore', 'textdomain' ),
			'id'         => $prefix . 'slide_btn',
			'type'       => 'text',
		) );
		$htslider->add_field( array(
			'name'       => __( 'Button Icon', 'textdomain' ),
			'desc'       => __( 'insert icon here ex:- long-arrow-right', 'textdomain' ),
			'id'         => $prefix . 'btn_icon',
			'type' => 'text',
			'default' =>'',
		) );
		$htslider->add_field( array(
			'name'       => __( 'Button URL', 'textdomain' ),
			'desc'       => __( 'insert button here', 'textdomain' ),
			'id'         => $prefix . 'slide_url',
			'type' => 'text_url',
		) );								
		$htslider->add_field(array(
			'name' => __( 'Section BG Color', 'bride' ),
			'id'   => $prefix .'section_bg_color',
			'desc'       => __( 'Insert section bg color ex:- #9f8447', 'bride' ),		
			'type' => 'colorpicker',
		) );
		$htslider->add_field(array(
			'name' => __( 'Section BG Image', 'bride' ),
			'id'   => $prefix .'section_bg_image',
			'desc'       => __( 'insert section bg image here', 'bride' ),		
			'type' => 'file',
		) );

		
		/* Testimonials Designation metabox */  
		$bride_testimonials = new_cmb2_box( array(
			'id'            => $prefix . 'testimonials',
			'title'         => esc_html__( 'Testimonials Designation Metabox ', 'bride' ),
			'object_types'  => array( 'bride_all_say', ), // Post type
		) );

		$bride_testimonials->add_field( array(
			'name'       => esc_html__( 'Client Designation', 'bride' ),
			'desc'       => esc_html__( 'Insert your Client Designation here', 'bride' ),
			'id'         => $prefix . 'bride_designation',
			'type'       => 'text',
		) );
		
		//Testimonials  Metaboxes for group
		$bride_testimonials = new_cmb2_box( array(
			'id'            => $prefix . 'testimonial_social_title',
			'title'         => esc_html__( 'Testimonial Social List', 'bride' ),
			'object_types'  => array( 'bride_all_say', ), // Post type
		) );

		$bride_testimonials_group = $bride_testimonials->add_field( array(
			'id'          => $prefix . 'testimonial_social_list',
			'type'        => 'group',
			'repeatable'  => true, // use false if you want non-repeatable group
			'options'     => array(
				'group_title'   => esc_html__( 'Social icon {#}', 'bride' ), // since version 1.1.4, {#} gets replaced by row number
				'add_button'    => esc_html__( 'Add Another icon', 'bride' ),
				'remove_button' => esc_html__( 'Remove icon', 'bride' ),
				'sortable'      => true, // beta
				// 'closed'     => true, // true to have the groups closed by default
			),
		) );

		// Id's for group's fields only need to be unique for the group. Prefix is not needed.
		$bride_testimonials->add_group_field( $bride_testimonials_group, array(
			'name' => 'Social icon name',
			'id'   => $prefix . 's_icon_name',
			'desc'       => esc_html__( 'You add icon from here fontawesome', 'bride' ),
			'type' => 'text',
		) );
		$bride_testimonials->add_group_field( $bride_testimonials_group, array(
			'name' => 'Social icon link',
			'id'   => $prefix . 's_icon_link',
			'type' => 'text',
		) );
		
		
		

	}
}
// Hook
add_action( 'cmb2_admin_init', 'register_bride_metabox' );