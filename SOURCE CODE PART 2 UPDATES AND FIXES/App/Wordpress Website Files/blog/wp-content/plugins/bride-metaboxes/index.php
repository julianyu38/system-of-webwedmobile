<?php

/*
Plugin Name: Bride Metaboxes
Description: For creating bride metaboxes 
Version: 1.0.0
Author: BootExperts
Author URI: http://wwww.bootexperts.com
License: GPL2
*/

add_action( 'admin_init', 'bride_child_plugin_has_parent_plugin' );

if ( !function_exists('bride_child_plugin_has_parent_plugin') ) {
    function bride_child_plugin_has_parent_plugin() {
        if ( is_admin() && current_user_can( 'activate_plugins' ) && !is_plugin_active( 'cmb2/init.php' ) ) {
            add_action( 'admin_notices', 'bride_child_plugin_notice' );
            deactivate_plugins( plugin_basename( __FILE__ ) ); 
            if ( isset( $_GET['activate'] ) ) {
                unset( $_GET['activate'] );
            }
        }
    }
}
function bride_child_plugin_notice(){ ?>
    <div class="error">
        <p>
            <strong><?php esc_html_e('Sorry, but Installing bride metaboxes Plugin, requires the CMB2 plugin to be installed and active.', 'bride') ?></strong>
        </p>
    </div>
    <?php
}
require_once( plugin_dir_path( __FILE__ ) . 'metaboxes.php');