<?php  /**
 * Blog  Widget
 *
 * This file is used to register and display the Layers - blog widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( class_exists('Layers_Widget') && !class_exists( 'Bride_Blog_Widget_2' ) ) {	
	class Bride_Blog_Widget_2 extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function __construct(){

			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = __( 'Bride :: Blog Widget #2' , 'bride' );
			$this->widget_id = 'htblogs';
			$this->post_type = 'post';
			$this->taxonomy = 'category';
			$this->checkboxes = array(
					'text_style',
					'carouselshow',
					'show_media',
					'show_content',
					'show_meta',
					'show_overlay',
					'show_blog_text',
					'show_titles',
					'show_excerpts',
					'show_call_to_action',
					'call_to_action',
					'show_icon_field',
					'show_icon_link',
					'show_pagination',
				); // @TODO: Try make this more dynamic, or leave a different note reminding users to change this if they add/remove checkboxes

			/* Widget settings. */
			$widget_ops = array(

				'classname'   => 'obox-layers-' . $this->widget_id .'-widget',
				'description' => __( 'This widget is used to display your posts in a flexible grid.', 'bride' ),
			);

			/* Widget control settings. */
			$control_ops = array(
				'width'   => LAYERS_WIDGET_WIDTH_SMALL,
				'height'  => NULL,
				'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
			);

			/* Create the widget. */
			parent::__construct(
				LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
				$this->widget_title,
				$widget_ops,
				$control_ops
			);

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => __( 'OUR BLOG', 'bride' ),
				'excerpt' => __( 'We provide the venue for the most amazing wedding reception', 'bride' ),
				'text_style' => 'style1',
				'carouselshow' => 'on',				
				'category' => 0,
				'show_media' => 'on',
				'show_content' => 'on',				
				'show_meta' => 'on',				
				'show_overlay' => '',
				'show_blog_text' => 'on',
				'show_titles' => 'on',
				'show_excerpts' => 'on',
				'excerpt_length' => 130,
				'show_call_to_action' => 'on',
				'call_to_action' => __( 'Read More' , 'bride' ),
				'show_icon_field' => 'off',
				'show_icon_link' => 'link',			
				'show_pagination' => 'off',		
				'bgoverlay_color' => '#a499c9',				
				'show_opacity' => '0',
				'o_opacity' => '.5',
				'o_tb' => '0',
				'o_lr' => '0',
				'o_lr' => '0',
				'overlay_color' => '#ffffff',
				'circle_hover_color' => '#ffffff',
				'bg_circle_color' => '#a499c9',
				'bg_circle_hover_color' => '#000000',
				'overlay_style' => '',
				'overlay_radius' => '0',
				'meta_color_icon' => '#888888',
				'meta_color' => '#888888',
				'meta_font_size' => '14',
				'title_color' => '#353535',
				'title_hover_color' => '#a499c9',
				'title_font_size' => '24',
				'content_color' => '#757575',
				'content_font_size' => '14',
				'readmore_style' => '',
				'btn_text_hover_color' => '#ffffff',
				'btn_hover_color' => '#a499c9',				
				'posts_per_page' => get_option( 'posts_per_page' ),
				'design' => array(
					'layout' => 'layout-Full-Width',
					'textalign' => 'text-center',
					'columns' => '3',
					'gutter' => 'on',
					'background' => array(
						'position' => 'center',
						'repeat' => 'no-repeat'
					),
					'fonts' => array(
						'align' => 'text-center',
						'size' => 'medium',
						'color' => NULL,
						'shadow' => NULL,
						'heading-type' => 'h3',
					)
				)
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {		
		
			global $wp_customize;

			$this->backup_inline_css();

			// Turn $args array into variables.
			extract( $args );

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			// Set the span class for each column
			if( isset( $instance['design'][ 'columns']  ) ) {
				$col_count = str_ireplace('columns-', '', $instance['design'][ 'columns']  );
				$span_class = 'span-' . ( 12/ $col_count );
			} else {
				$col_count = 3;
				$span_class = 'span-4';
			}

			// Apply Styling
			$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $instance['design'][ 'background' ] ) );
			$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'color', array( 'selectors' => array( '.section-title h3' , '.section-title p' ) , 'color' => $instance['design']['fonts'][ 'color' ] ) );
			
			
			$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'selectors' => array( '.thumbnail-body' ) , 'background' => array( 'color' => $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ) );
			
			// Apply Button Styling.
			$button_size = '';
			if ( function_exists( 'layers_pro_apply_widget_button_styling' ) ) {
				// Apply Layers Pro Button Styling.
				$this->inline_css .= layers_pro_apply_widget_button_styling( $this, $instance, array( "#{$widget_id} .thumbnail-body a.button" ) );
				$button_size = $this->check_and_return( $instance, 'design', 'buttons-size' ) ? 'btn-' . $this->check_and_return( $instance, 'design', 'buttons-size' ) : '' ;
			}
			else {
				// Apply Button Styling.
				$this->inline_css .= layers_inline_button_styles( "#{$widget_id}", 'button', array( 'selectors' => array( '.thumbnail-body a.button' ) ,'button' => $this->check_and_return( $instance, 'design', 'buttons' ) ) );
			}

			// Begin query arguments
			$query_args = array();
			if( get_query_var('paged') ) {
				$query_args[ 'paged' ] = get_query_var('paged') ;
			} else if ( get_query_var('page') ) {
				$query_args[ 'paged' ] = get_query_var('page');
			} else {
				$query_args[ 'paged' ] = 1;
			}
			
			$query_args[ 'post_type' ] = $this->post_type;
			$query_args[ 'posts_per_page' ] = $instance['posts_per_page'];
			if( isset( $instance['order'] ) ) {

				$decode_order = json_decode( $instance['order'], true );

				if( is_array( $decode_order ) ) {
					foreach( $decode_order as $key => $value ){
						$query_args[ $key ] = $value;
					}
				}
			}

			// Do the special taxonomy array()
			if( isset( $instance['category'] ) && '' != $instance['category'] && 0 != $instance['category'] ){

				$query_args['tax_query'] = array(
					array(
						"taxonomy" => $this->taxonomy,
						"field" => "id",
						"terms" => $instance['category']
					)
				);
			} elseif( !isset( $instance['hide_category_filter'] ) ) {
				$terms = get_terms( $this->taxonomy );
			} // if we haven't selected which category to show, let's load the $terms for use in the filter

			// Do the WP_Query
			$post_query = new WP_Query( $query_args );

			/**
			* Generate the widget container class
			*/
			$widget_container_class = array();

			$widget_container_class[] = 'widget';
			$widget_container_class[] = 'layers-post-widget';
			$widget_container_class[] = 'bride_post_widget';
			$widget_container_class[] = 'content-vertical-massive';
			$widget_container_class[] = 'clearfix';
			$widget_container_class[] = ( 'on' == $this->check_and_return( $instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
			$widget_container_class[] = $this->check_and_return( $instance , 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
			$widget_container_class[] = $this->get_widget_spacing_class( $instance );

			$widget_container_class = apply_filters( 'layers_post_widget_container_class' , $widget_container_class, $this, $instance );
			$widget_container_class = implode( ' ', $widget_container_class ); ?>
			<?php echo $this->custom_anchor( $instance ); ?>
			<div id="<?php echo esc_attr( $widget_id ); ?>" class="<?php echo esc_attr( $widget_container_class ); ?>">

				<?php do_action( 'layers_before_post_widget_inner', $this, $instance ); ?>

				<?php if( '' != $this->check_and_return( $instance , 'title' ) ||'' != $this->check_and_return( $instance , 'excerpt' ) ) { ?>
					<div class="container clearfix">
						<?php /**
						* Generate the Section Title Classes
						*/
						$section_title_class = array();
						$section_title_class[] = 'section-title clearfix';
						$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'size' );
						$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'align' );
						$section_title_class[] = ( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ? 'invert' : '' );
						$section_title_class = implode( ' ', $section_title_class ); ?>
						<div class="<?php echo $section_title_class; ?>">
							<?php if( '' != $this->check_and_return( $instance, 'title' )  ) { ?>
								<<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?> class="heading">
									<?php echo $instance['title'] ?>
								</<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?>>
							<?php } ?>
							<?php if( '' != $this->check_and_return( $instance, 'excerpt' )  ) { ?>
								<div class="excerpt"><?php echo layers_the_content( $instance['excerpt'] ); ?></div>
							<?php } ?>
						</div>
					</div>
					
				<?php } ?>
				
					<?php if( $post_query->have_posts() ) { ?>
						<div class="<?php echo $this->get_widget_layout_class( $instance ); ?>">
					
							<?php if( 'style1' == $this->check_and_return( $instance, 'text_style' ) ){?>
								<div class="grid">			
								<?php while( $post_query->have_posts() ) {
									$post_query->the_post();

										/**
										* Set Individual Column CSS
										*/
										$post_column_class = array();
										$post_column_class[] = 'single-blog-item overlay-hover';
										$post_column_class[] = 'single-blog-pading';
										$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
										$post_column_class[] = $span_class;
										$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
										$post_column_class = implode( ' ' , $post_column_class );
										?>																			
										<div class="<?php echo $post_column_class; ?>" data-cols="<?php echo $col_count; ?>">
										
										
										<!-- start copy -->		
										<div class="ht-blog style-2-blogs">
																			

										<?php if ($post_query->current_post % 2 == 0): ?>	
											<!-- blog image option -->				
											<?php if( isset( $instance['show_media'] ) ) { // condition media ?>
											
												
												<?php if(has_post_thumbnail()){ ?>
												
													<!-- blog image -->
													<div class="ht-blog-thumb">
													
														<?php the_post_thumbnail(); ?>
																
															<?php if(  $instance['show_overlay'] !=''  ) {?>
															
																<?php if( isset( $instance['show_icon_field'] ) ) {?>	
																<!-- blog icon text  -->
																<span class="post_dates <?php echo esc_html( $instance['overlay_style'] ); ?>" >
																	<a href="<?php the_permalink();?>"><i class="fa fa-<?php echo esc_html( $instance['show_icon_link'] ); ?>"></i></a>
																</span>
																<?php }else{ ?>
																	<!-- blog icon text  -->
																	<span class="post_dates <?php echo esc_html( $instance['overlay_style'] ); ?>" >
																		<?php echo get_the_time('j M'); ?>
																		
																	</span>	
															<?php }} ?>
													</div>
													<!-- end blog image -->		
												<?php }else{?>
													<!-- blog video -->
													<div class="single-themb">
														<?php echo layers_post_featured_media( array( 'postid' => get_the_ID(), 'wrap_class' => 'overlay-effect', 'size' => '' ) ); ?>
													</div>
													<!-- end blog video -->	
												<?php } ?>
											<?php } // end condition media  ?>
											<!-- end blog image option -->	
											
											
											<?php if( isset( $instance['show_content'] ) ) { ?>
												<!-- blog post content area -->
												<div class="ht-single-content thumbnail-body">
													<?php if( isset( $instance['show_meta'] ) ) {?>
													<!-- blog post meta -->
													<div class="ht-post-meta">	
														<div class="ht-blog-date">		
														
															<span>
																<i class="fa fa-calendar"></i>
																<?php echo get_the_time(get_option('date_format')); ?>
															</span>									
															<span>
																<i class="fa fa-folder"></i>
																<?php the_category(' , '); ?>
															</span>
															<span>
																<i class="fa fa-comment"></i> 
																<?php comments_number( '0', '1', '%' ); ?>
															</span>
															<span>
																<i class="fa fa-user"></i> 
																<a href="#"><?php the_author(); ?></a>
															</span>
														</div>
													</div>
													<!-- end blog post meta -->
													<?php } //end media ?>
													<?php if( isset( $instance['show_blog_text'] ) ) {?>
													<!-- blog post text area -->
													<div class="ht-content-text">
														<?php if( isset( $instance['show_titles'] ) ) { ?>
															<!-- blog title -->
																<h2>
																	<a href="<?php the_permalink();?>"><?php the_title();?></a>
																</h2>
															<!-- end blog title -->
															
														<?php } //end title ?>
														
														<!-- blog paragraph -->
														<p>
														<?php if( isset( $instance['show_excerpts'] ) ) {
															if( isset( $instance['excerpt_length'] ) && '' == $instance['excerpt_length'] ) {
																
																	the_content();
																
															} else if( isset( $instance['excerpt_length'] ) && 0 != $instance['excerpt_length'] && strlen( get_the_excerpt() ) > $instance['excerpt_length'] ){
																echo  substr( get_the_excerpt() , 0 , $instance['excerpt_length'] ) . '&#8230';
															} else if( '' != get_the_excerpt() ){
																echo get_the_excerpt();
															}
														}; //end pra ?>
														
														</p>
														<!-- end blog paragraph -->
														
														<?php if( isset( $instance['show_call_to_action'] ) && $this->check_and_return( $instance , 'call_to_action' ) ) {?>
														<!-- blog button -->
														<div class="ht-blog-button <?php echo esc_html( $instance['readmore_style'] ); ?>">
															<a href="<?php the_permalink(); ?>" class="button <?php echo $button_size; ?>">
																<?php echo esc_html( $instance['call_to_action'] ); ?>
															</a>
														</div>
														<!-- end blog button  -->
														<?php } //end button ?>
													</div>
													<!-- end blog post text -->
													
													<?php } //end blog text ?>
												</div>
												<!-- end blog post content area -->
												
											<?php } //end show content ?>
											
											
											<?php else:?>
												<?php if( isset( $instance['show_content'] ) ) { ?>
												<!-- blog post content area -->
												<div class="ht-single-content thumbnail-body">
													<?php if( isset( $instance['show_meta'] ) ) {?>
													<!-- blog post meta -->
													<div class="ht-post-meta">	
														<div class="ht-blog-date">		
														
															<span>
																<i class="fa fa-calendar"></i>
																<?php echo get_the_time(get_option('date_format')); ?>
															</span>									
															<span>
																<i class="fa fa-folder"></i>
																<?php the_category(' , '); ?>
															</span>
															<span>
																<i class="fa fa-comment"></i> 
																<?php comments_number( '0', '1', '%' ); ?>
															</span>
															<span>
																<i class="fa fa-user"></i> 
																<a href="#"><?php the_author(); ?></a>
															</span>
														</div>
													</div>
													<!-- end blog post meta -->
													<?php } //end media ?>
													<?php if( isset( $instance['show_blog_text'] ) ) {?>
													<!-- blog post text area -->
													<div class="ht-content-text">
														<?php if( isset( $instance['show_titles'] ) ) { ?>
															<!-- blog title -->
																<h2>
																	<a href="<?php the_permalink();?>"><?php the_title();?></a>
																</h2>
															<!-- end blog title -->
															
														<?php } //end title ?>
														
														<!-- blog paragraph -->
														<p>
														<?php if( isset( $instance['show_excerpts'] ) ) {
															if( isset( $instance['excerpt_length'] ) && '' == $instance['excerpt_length'] ) {
																
																	the_content();
																
															} else if( isset( $instance['excerpt_length'] ) && 0 != $instance['excerpt_length'] && strlen( get_the_excerpt() ) > $instance['excerpt_length'] ){
																echo  substr( get_the_excerpt() , 0 , $instance['excerpt_length'] ) . '&#8230';
															} else if( '' != get_the_excerpt() ){
																echo get_the_excerpt();
															}
														}; //end pra ?>
														
														</p>
														<!-- end blog paragraph -->
														
														<?php if( isset( $instance['show_call_to_action'] ) && $this->check_and_return( $instance , 'call_to_action' ) ) {?>
														<!-- blog button -->
														<div class="ht-blog-button <?php echo esc_html( $instance['readmore_style'] ); ?>">
															<a href="<?php the_permalink(); ?>" class="button <?php echo $button_size; ?>">
																<?php echo esc_html( $instance['call_to_action'] ); ?>
															</a>
														</div>
														<!-- end blog button  -->
														<?php } //end button ?>
													</div>
													<!-- end blog post text -->
													
													<?php } //end blog text ?>
												</div>
												<!-- end blog post content area -->
												
											<?php } //end show content ?>
											
											
											<!-- blog image option -->				
											<?php if( isset( $instance['show_media'] ) ) { // condition media ?>
											
												
												<?php if(has_post_thumbnail()){ ?>
												
													<!-- blog image -->
													<div class="ht-blog-thumb">
													
														<?php the_post_thumbnail(); ?>
																
															<?php if(  $instance['show_overlay'] !=''  ) {?>
															
																<?php if( isset( $instance['show_icon_field'] ) ) {?>	
																<!-- blog icon text  -->
																<span class="post_dates <?php echo esc_html( $instance['overlay_style'] ); ?>" >
																	<a href="<?php the_permalink();?>"><i class="fa fa-<?php echo esc_html( $instance['show_icon_link'] ); ?>"></i></a>
																</span>
																<?php }else{ ?>
																	<!-- blog icon text  -->
																	<span class="post_dates <?php echo esc_html( $instance['overlay_style'] ); ?>" >
																		<?php echo get_the_time('j M'); ?>
																		
																	</span>	
															<?php }} ?>
													</div>
													<!-- end blog image -->		
												<?php }else{?>
													<!-- blog video -->
													<div class="single-themb">
														<?php echo layers_post_featured_media( array( 'postid' => get_the_ID(), 'wrap_class' => 'overlay-effect', 'size' => '' ) ); ?>
													</div>
													<!-- end blog video -->	
												<?php } ?>
											<?php } // end condition media  ?>
											<!-- end blog image option -->
											
											<?php endif;?>



											
															
															
															
															
															
															
															
																					
										</div>
										<!-- end copy -->	
						
						
					
							</div>														
								<?php }; // while have_posts ?>
							</div><!-- /row -->
								<?php if( isset( $instance['show_pagination'] ) ) { ?>
									<div class="container">					
										<div class="pagination-content">
											<div class="pagination-button">
											<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
											</div>
										</div>						
									</div>
								<?php } //end pagination?>						
						
						<?php }elseif( 'style2' == $this->check_and_return( $instance, 'text_style' ) ){?>
							<?php if( isset( $instance['carouselshow'] ) ) { ?>
								<div class="grid">
									<div class="blog-carousel carousel-style-one">
							 <?php } else { ?>	
								<div class="noclass">
									<div class="grid">
							 <?php } //end carsol show/hide ?>					
								<?php while( $post_query->have_posts() ) {
									$post_query->the_post();

										/**
										* Set Individual Column CSS
										*/
										$post_column_class = array();
										$post_column_class[] = 'single-blog-item overlay-hover';
										$post_column_class[] = 'single-blog-pading';
										$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
										$post_column_class[] = $span_class;
										$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
										$post_column_class = implode( ' ' , $post_column_class );
										?>																			
										<div class="<?php echo $post_column_class; ?>" data-cols="<?php echo $col_count; ?>">

										
											<!-- paste here -->		
										
										
									</div>														
								<?php }; // while have_posts ?>
							</div><!-- /carsol -->
						</div><!-- /row -->	
						<?php if( isset( $instance['show_pagination'] ) ) { ?>
							<div class="container">					
								<div class="pagination-content">
									<div class="pagination-button">
									<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
									</div>
								</div>						
							</div>
						<?php } //end pagination?>						
						<?php }elseif( 'style3' == $this->check_and_return( $instance, 'text_style' ) ){?>
							
							<div class="grid">
								<div class="column span-4 pull-left sidebar">
									<?php if ( is_active_sidebar( 'layers-left-sidebar' ) ) : ?>

										<?php dynamic_sidebar( 'layers-left-sidebar' ); ?>

									<?php endif; ?>
								</div>
								<div class="column span-8">
									<?php while( $post_query->have_posts() ) {
										$post_query->the_post(); ?>									
										<div class="single-blog-items">
									
											<!-- paste here -->
										
										</div>														
									<?php }; // while have_posts ?>							
								</div>
							</div>
							<?php if( isset( $instance['show_pagination'] ) ) { ?>
								<div class="container">					
									<div class="pagination-content">
										<div class="pagination-button">
										<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
										</div>
									</div>						
								</div>
							<?php } //end pagination?>						
						<?php }elseif( 'style4' == $this->check_and_return( $instance, 'text_style' ) ){?>
							<div class="grid">
								<div class="column span-8">
									<?php while( $post_query->have_posts() ) {
										$post_query->the_post(); ?>													
										<div class="single-blog-items">

											<!-- paste here -->
										
										</div>														
									<?php }; // while have_posts ?>							
								</div>
								<div class="column span-4 pull-right sidebar">
									<?php if ( is_active_sidebar( 'layers-right-sidebar' ) ) : ?>

										<?php dynamic_sidebar( 'layers-right-sidebar' ); ?>

									<?php endif; ?>
								</div>							
							</div>
							<?php if( isset( $instance['show_pagination'] ) ) { ?>
								<div class="container">					
									<div class="pagination-content">
										<div class="pagination-button">
										<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
										</div>
									</div>						
								</div>
							<?php } //end pagination?>						
						<?php }elseif( 'style5' == $this->check_and_return( $instance, 'text_style' ) ){?>
								<div class="grid">
									<div class="column span-4 pull-left sidebar">
										<?php if ( is_active_sidebar( 'layers-left-sidebar' ) ) : ?>

											<?php dynamic_sidebar( 'layers-left-sidebar' ); ?>

										<?php endif; ?>
									</div>
									<div class="column span-8">
										<div class="grid">	
								<?php while( $post_query->have_posts() ) {
									$post_query->the_post();

										/**
										* Set Individual Column CSS
										*/
										$post_column_class = array();
										$post_column_class[] = 'single-blog-item overlay-hover';
										$post_column_class[] = 'single-blog-pading';
										$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
										$post_column_class[] = $span_class;
										$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
										$post_column_class = implode( ' ' , $post_column_class );
										?>																			
										<div class="<?php echo $post_column_class; ?>" data-cols="<?php echo $col_count; ?>">
										
											<!-- paste here -->
										
										</div>														
									<?php }; // while have_posts ?>
									</div><!-- /grid -->
									<?php if( isset( $instance['show_pagination'] ) ) { ?>
										<div class="container">					
											<div class="pagination-content">
												<div class="pagination-button">
												<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
												</div>
											</div>						
										</div>
									<?php } //end pagination?>									
								</div><!-- /column -->
							</div><!-- /row -->
												
						<?php }elseif( 'style6' == $this->check_and_return( $instance, 'text_style' ) ){?>
								<div class="grid">
									<div class="column span-8">
										<div class="grid">	
								<?php while( $post_query->have_posts() ) {
									$post_query->the_post();

										/**
										* Set Individual Column CSS
										*/
										$post_column_class = array();
										$post_column_class[] = 'single-blog-item overlay-hover';
										$post_column_class[] = 'single-blog-pading';
										$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
										$post_column_class[] = $span_class;
										$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
										$post_column_class = implode( ' ' , $post_column_class );
										?>																			
										<div class="<?php echo $post_column_class; ?>" data-cols="<?php echo $col_count; ?>">
								

											<!-- paste here -->


										</div>														
									<?php }; // while have_posts ?>
									</div><!-- /grid -->
									<?php if( isset( $instance['show_pagination'] ) ) { ?>
										<div class="container">					
											<div class="pagination-content">
												<div class="pagination-button">
												<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
												</div>
											</div>						
										</div>
									<?php } //end pagination?>									
								</div><!-- /column -->
									<div class="column span-4 pull-right sidebar">
										<?php if ( is_active_sidebar( 'layers-right-sidebar' ) ) : ?>

											<?php dynamic_sidebar( 'layers-right-sidebar' ); ?>

										<?php endif; ?>
									</div>								
							</div><!-- /row -->
						<?php }elseif( 'style7' == $this->check_and_return( $instance, 'text_style' ) ){?>
								<div class="grid">
									<div class="column span-3 pull-left sidebar">
										<?php if ( is_active_sidebar( 'layers-right-sidebar' ) ) : ?>

											<?php dynamic_sidebar( 'layers-right-sidebar' ); ?>

										<?php endif; ?>
									</div>								
								
								
									<div class="column span-6">
										<div class="grid">	
								  <?php while( $post_query->have_posts() ) {
									$post_query->the_post();

										/**
										* Set Individual Column CSS
										*/
										$post_column_class = array();
										$post_column_class[] = 'single-blog-item overlay-hover';
										$post_column_class[] = 'single-blog-pading';
										$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
										$post_column_class[] = $span_class;
										$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
										$post_column_class = implode( ' ' , $post_column_class );
										?>																			
										<div class="<?php echo $post_column_class; ?>" data-cols="<?php echo $col_count; ?>">

											<!-- paste here -->
										
										</div>														
									<?php }; // while have_posts ?>
									</div><!-- /grid -->
									<?php if( isset( $instance['show_pagination'] ) ) { ?>
										<div class="container">					
											<div class="pagination-content">
												<div class="pagination-button">
												<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>							  
												</div>
											</div>						
										</div>
									<?php } //end pagination?>									
								</div><!-- /column -->
									<div class="column span-3 pull-right sidebar">
										<?php if ( is_active_sidebar( 'layers-right-sidebar' ) ) : ?>

											<?php dynamic_sidebar( 'layers-right-sidebar' ); ?>

										<?php endif; ?>
									</div>								
							</div><!-- /row -->														
						<?php } ?>
					</div>
				<?php }; // if have_posts ?>
				<?php do_action( 'layers_after_post_widget_inner', $this, $instance );
				// Print the Inline Styles for this Widget
				$this->print_inline_css();	?>
				<style type="text/css">
				
					#<?php echo esc_attr( $widget_id ); ?> .ht-single-content {
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-thumb span.post_dates					
					{
						border-radius: <?php echo esc_html( $instance['overlay_radius'] ); ?>%;						
						color: <?php echo esc_html( $instance['overlay_color'] ); ?>;
						background:<?php echo esc_html( $instance['bg_circle_color'] ); ?>;						
						font-size: 18.6px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-thumb span.post_dates a{
						color: <?php echo esc_html( $instance['overlay_color'] ); ?>;	
					}					
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-thumb span.post_dates:hover
					{
						color: <?php echo esc_html( $instance['circle_hover_color'] ); ?>;
					    background:<?php echo esc_html( $instance['bg_circle_hover_color'] ); ?>;	
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-thumb span.post_dates:hover a{
						color: <?php echo esc_html( $instance['circle_hover_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-date span i {
					  color:<?php echo esc_html( $instance['meta_color_icon'] ); ?>;
					}						
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-date span,
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-date a
					{
						  color:<?php echo esc_html( $instance['meta_color'] ); ?>;
						  font-size:<?php echo esc_html( $instance['meta_font_size'] ); ?>px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-date a:hover{
						color:<?php echo esc_html( $instance['bg_circle_color'] ); ?>;	
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-content-text h2, 
					#<?php echo esc_attr( $widget_id ); ?> .ht-content-text h2 a 
					{
					  color: <?php echo esc_html( $instance['title_color'] ); ?>;
					  font-size: <?php echo esc_html( $instance['title_font_size'] ); ?>px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-content-text h2:hover, 
					#<?php echo esc_attr( $widget_id ); ?> .ht-content-text h2 a:hover 
					{
					  color: <?php echo esc_html( $instance['title_hover_color'] ); ?>;
					}
					
					#<?php echo esc_attr( $widget_id ); ?> .ht-content-text p {
					  color: <?php echo esc_html( $instance['content_color'] ); ?>;
					  font-size: <?php echo esc_html( $instance['content_font_size'] ); ?>px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .thumbnail-body a.button{
						padding:<?php echo esc_html( $instance['pading_tb'] ); ?>px <?php echo esc_html( $instance['pading_lr'] ); ?>px;
						font-size:<?php echo esc_html( $instance['btn_font_size'] ); ?>px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .thumbnail-body a.button:hover{
						color:<?php echo esc_html( $instance['btn_text_hover_color'] ); ?>;
						background:<?php echo esc_html( $instance['btn_hover_color'] ); ?>;
						border-color:<?php echo esc_html( $instance['btn_hover_color'] ); ?>;
					}			
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog-thumb::before {
					  background:<?php echo esc_html( $instance['bgoverlay_color'] ); ?>;
					  bottom: <?php echo esc_html( $instance['o_tb'] ); ?>px;
					  left: <?php echo esc_html( $instance['o_lr'] ); ?>px;
					  opacity:<?php echo esc_html( $instance['show_opacity'] ); ?>;
					  right: <?php echo esc_html( $instance['o_lr'] ); ?>px;
					  top: <?php echo esc_html( $instance['o_tb'] ); ?>px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .ht-blog:hover .ht-blog-thumb::before{
						opacity:<?php echo esc_html( $instance['o_opacity'] ); ?>;
					}			
					#<?php echo esc_attr( $widget_id ); ?> .section-title h3::before{
					  background: <?php echo esc_html( $instance['title_border_bottom_color'] ); ?>;
					}
					
					#<?php echo esc_attr( $widget_id ); ?> .section-title h3::after {
					  color: <?php echo esc_html( $instance['title_border_bottom_color'] ); ?>;
					}


					
				</style>							
			</div>

			<?php // Reset WP_Query
			wp_reset_postdata();

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $instance );
		}

		/**
		*  Widget update
		*/

		function update($new_instance, $old_instance) {

			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			$this->design_bar(
				'side', // CSS Class Name
				array( // Widget Object
					'name' => $this->get_layers_field_name( 'design' ),
					'id' => $this->get_layers_field_id( 'design' ),
					'widget_id' => $this->widget_id,
				),
				$instance, // Widget Values
				apply_filters( 'layers_post_widget_design_bar_components' , array( // Components
					'layout',
					'display' => array(
						'icon-css' => 'icon-display',
						'label' => __( 'Display', 'bride' ),
						'elements' => array(
							'text_style' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'text_style' ) ,
								'id' => $this->get_layers_field_id( 'text_style' ) ,
								'value' => ( isset( $instance['text_style'] ) ) ? $instance['text_style'] : NULL,
								'label' => __( 'Select Style' , 'bride' ),
								'options' => array(
										'style1' => __( 'Blog Grid' , 'bride' ),
										'style2' => __( 'Blog grid with carousel' , 'bride' ),
										'style3' => __( 'Blog List left Sidebar' , 'bride' ),
										'style4' => __( 'Blog List Right Sidebar' , 'bride' ),
										'style5' => __( 'Blog Column left Sidebar' , 'bride' ),
										'style6' => __( 'Blog Column Right Sidebar' , 'bride' ),
										'style7' => __( 'Blog Blog Sidebar' , 'bride' ),
								)
							),							
							'carouselshow' => array(
							'type' => 'checkbox',
							'name' => $this->get_layers_field_name( 'carouselshow' ) ,
							'id' => $this->get_layers_field_id( 'carouselshow' ) ,
							'value' => ( isset( $instance['carouselshow'] ) ) ? $instance['carouselshow'] : NULL,
							'label' => __( 'Show/hide Carousel' , 'bride' )
							),						
							'show_media' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_media' ) ,
								'id' => $this->get_layers_field_id( 'show_media' ) ,
								'value' => ( isset( $instance['show_media'] ) ) ? $instance['show_media'] : NULL,
								'label' => __( 'Show Featured Images' , 'bride' )
							),
							'show_content' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_content' ) ,
								'id' => $this->get_layers_field_id( 'show_content' ) ,
								'value' => ( isset( $instance['show_content'] ) ) ? $instance['show_content'] : NULL,
								'label' => __( 'Show  Content' , 'bride' )
							),
							'show_meta' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_meta' ) ,
								'id' => $this->get_layers_field_id( 'show_meta' ) ,
								'value' => ( isset( $instance['show_meta'] ) ) ? $instance['show_meta'] : NULL,
								'label' => __( 'Show  Post Meta' , 'bride' )
							),
							'show_overlay' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_overlay' ) ,
								'id' => $this->get_layers_field_id( 'show_overlay' ) ,
								'value' => ( isset( $instance['show_overlay'] ) ) ? $instance['show_overlay'] : NULL,
								'label' => __( 'Show  Circle Overlay' , 'bride' )
							),
							
							'show_icon_field' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_icon_field' ) ,
								'id' => $this->get_layers_field_id( 'show_icon_field' ) ,
								'value' => ( isset( $instance['show_icon_field'] ) ) ? $instance['show_icon_field'] : NULL,
								'label' => __( 'Set Icon' , 'layerswp' ),
							),						
							'show_icon_link' => array(
								'type' => 'text',
								'name' => $this->get_layers_field_name( 'show_icon_link' ) ,
								'id' => $this->get_layers_field_id( 'show_icon_link' ) ,
								'value' => ( isset( $instance['show_icon_link'] ) ) ? $instance['show_icon_link'] : NULL,
								'label' => __( 'Inset Link Icon' , 'layerswp' ),
								'data' => array( 'show-if-selector' => '#' . $this->get_layers_field_id( 'show_icon_field' ), 'show-if-value' => 'true' ),
								'placeholder' =>'link',
							),							
							'show_blog_text' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_blog_text' ) ,
								'id' => $this->get_layers_field_id( 'show_blog_text' ) ,
								'value' => ( isset( $instance['show_blog_text'] ) ) ? $instance['show_blog_text'] : NULL,
								'label' => __( 'Show Post Text' , 'bride' )
							),
							'show_titles' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_titles' ) ,
								'id' => $this->get_layers_field_id( 'show_titles' ) ,
								'value' => ( isset( $instance['show_titles'] ) ) ? $instance['show_titles'] : NULL,
								'label' => __( 'Show  Post Titles' , 'bride' )
							),
							'show_excerpts' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_excerpts' ) ,
								'id' => $this->get_layers_field_id( 'show_excerpts' ) ,
								'value' => ( isset( $instance['show_excerpts'] ) ) ? $instance['show_excerpts'] : NULL,
								'label' => __( 'Show Post Excerpts' , 'bride' )
							),
							'excerpt_length' => array(
								'type' => 'number',
								'name' => $this->get_layers_field_name( 'excerpt_length' ) ,
								'id' => $this->get_layers_field_id( 'excerpt_length' ) ,
								'min' => 0,
								'max' => 10000,
								'value' => ( isset( $instance['excerpt_length'] ) ) ? $instance['excerpt_length'] : NULL,
								'label' => __( 'Excerpts Length' , 'bride' ),
								'data' => array( 'show-if-selector' => '#' . $this->get_layers_field_id( 'show_excerpts' ), 'show-if-value' => 'true' ),
							),							
							'show_call_to_action' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_call_to_action' ) ,
								'id' => $this->get_layers_field_id( 'show_call_to_action' ) ,
								'value' => ( isset( $instance['show_call_to_action'] ) ) ? $instance['show_call_to_action'] : NULL,
								'label' => __( 'Show "Read More" Buttons' , 'layerswp' ),
							),
							'call_to_action' => array(
								'type' => 'text',
								'name' => $this->get_layers_field_name( 'call_to_action' ) ,
								'id' => $this->get_layers_field_id( 'call_to_action' ) ,
								'value' => ( isset( $instance['call_to_action'] ) ) ? $instance['call_to_action'] : NULL,
								'label' => __( '"Read More" Text' , 'layerswp' ),
								'data' => array( 'show-if-selector' => '#' . $this->get_layers_field_id( 'show_call_to_action' ), 'show-if-value' => 'true' ),
							),							
							'show_pagination' => array(
							'type' => 'checkbox',
							'name' => $this->get_layers_field_name( 'show_pagination' ) ,
							'id' => $this->get_layers_field_id( 'show_pagination' ) ,
							'value' => ( isset( $instance['show_pagination'] ) ) ? $instance['show_pagination'] : NULL,
							'label' => __( 'show pagination' , 'bride' )
							),
							
							),
						),
					'colorown' => array(
						'icon-css' => 'icon-call-to-action',
						'label' => __( 'Color Style', 'bride' ),
						'elements' => array(
							'bgoverlay_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'bgoverlay_color' ) ,
								'id' => $this->get_layers_field_id( 'bgoverlay_color' ) ,
								'value' => ( isset( $instance['bgoverlay_color'] ) ) ? $instance['bgoverlay_color'] : NULL,
								'label' => __( 'BG Overlay Color' , 'bride' ),
							),
							'show_opacity' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '1',
								'step' => '0.1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'show_opacity' ) ,
								'id' => $this->get_layers_field_id( 'show_opacity' ) ,
								'value' => ( isset( $instance['show_opacity'] ) ) ? $instance['show_opacity'] : NULL,
								'label' => __( 'BG Default Overlay' , 'bride' ),
							),							
							'o_opacity' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '1',
								'step' => '0.1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'o_opacity' ) ,
								'id' => $this->get_layers_field_id( 'o_opacity' ) ,
								'value' => ( isset( $instance['o_opacity'] ) ) ? $instance['o_opacity'] : NULL,
								'label' => __( 'BG Hover Overlay' , 'bride' ),
							),							
							'o_tb' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'o_tb' ) ,
								'id' => $this->get_layers_field_id( 'o_tb' ) ,
								'value' => ( isset( $instance['o_tb'] ) ) ? $instance['o_tb'] : NULL,
								'label' => __( 'Overlay Top/Bottom' , 'bride' ),
							),
							'o_lr' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'o_lr' ) ,
								'id' => $this->get_layers_field_id( 'o_lr' ) ,
								'value' => ( isset( $instance['o_lr'] ) ) ? $instance['o_lr'] : NULL,
								'label' => __( 'Overlay Left/Right' , 'bride' ),
							),							
							'overlay_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'overlay_color' ) ,
								'id' => $this->get_layers_field_id( 'overlay_color' ) ,
								'value' => ( isset( $instance['overlay_color'] ) ) ? $instance['overlay_color'] : NULL,
								'label' => __( 'Circle color' , 'bride' ),
							),
							'circle_hover_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'circle_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'circle_hover_color' ) ,
								'value' => ( isset( $instance['circle_hover_color'] ) ) ? $instance['circle_hover_color'] : NULL,
								'label' => __( 'Circle Hover color' , 'bride' ),
							),							
							'bg_circle_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'bg_circle_color' ) ,
								'id' => $this->get_layers_field_id( 'bg_circle_color' ) ,
								'value' => ( isset( $instance['bg_circle_color'] ) ) ? $instance['bg_circle_color'] : NULL,
								'label' => __( 'BG Circle color' , 'bride' ),
							),
							'bg_circle_hover_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'bg_circle_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'bg_circle_hover_color' ) ,
								'value' => ( isset( $instance['bg_circle_hover_color'] ) ) ? $instance['bg_circle_hover_color'] : NULL,
								'label' => __( 'BG Circle Hover color' , 'bride' ),
							),							
							'overlay_style' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'overlay_style' ) ,
								'id' => $this->get_layers_field_id( 'overlay_style' ) ,
								'value' => ( isset( $instance['overlay_style'] ) ) ? $instance['overlay_style'] : NULL,
								'label' => __( 'Select Circle Style' , 'bride' ),
								'options' => array(
										'' => __( 'Select Options' , 'bride' ),
										'post_datelt' => __( 'Circle Left Top' , 'bride' ),
										'post_dateslb' => __( 'Circle Left Bottom' , 'bride' ),
										'post_datesr' => __( 'Circle Right Top' , 'bride' ),
										'post_datesrb' => __( 'Circle Right Bottom' , 'bride' ),
										'post_datescenter' => __( 'Circle Center' , 'bride' ),
								)
							),
							'overlay_radius' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'overlay_radius' ) ,
								'id' => $this->get_layers_field_id( 'overlay_radius' ) ,
								'value' => ( isset( $instance['overlay_radius'] ) ) ? $instance['overlay_radius'] : NULL,
								'label' => __( 'overlay Border Radius' , 'bride' ),
							),
							'meta_color_icon' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'meta_color_icon' ) ,
								'id' => $this->get_layers_field_id( 'meta_color_icon' ) ,
								'value' => ( isset( $instance['meta_color_icon'] ) ) ? $instance['meta_color_icon'] : NULL,
								'label' => __( 'Meta Icon color' , 'bride' ),
							),
							'meta_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'meta_color' ) ,
								'id' => $this->get_layers_field_id( 'meta_color' ) ,
								'value' => ( isset( $instance['meta_color'] ) ) ? $instance['meta_color'] : NULL,
								'label' => __( 'Meta Text color' , 'bride' ),
							),
							'meta_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',
								'default' => '0',
								'name' => $this->get_layers_field_name( 'meta_font_size' ) ,
								'id' => $this->get_layers_field_id( 'meta_font_size' ) ,
								'value' => ( isset( $instance['meta_font_size'] ) ) ? $instance['meta_font_size'] : NULL,
								'label' => __( 'Meta Font Size' , 'bride' ),
							),
							
							'title_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'title_color' ) ,
								'id' => $this->get_layers_field_id( 'title_color' ) ,
								'value' => ( isset( $instance['title_color'] ) ) ? $instance['title_color'] : NULL,
								'label' => __( 'title color' , 'bride' ),
							),
							'title_hover_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'title_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'title_hover_color' ) ,
								'value' => ( isset( $instance['title_hover_color'] ) ) ? $instance['title_hover_color'] : NULL,
								'label' => __( 'Title Hover Color' , 'bride' ),
							),
							'title_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',
								'default' => '0',
								'name' => $this->get_layers_field_name( 'title_font_size' ) ,
								'id' => $this->get_layers_field_id( 'title_font_size' ) ,
								'value' => ( isset( $instance['title_font_size'] ) ) ? $instance['title_font_size'] : NULL,
								'label' => __( 'Title Font Size' , 'bride' ),
							),							
							'content_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'content_color' ) ,
								'id' => $this->get_layers_field_id( 'content_color' ) ,
								'value' => ( isset( $instance['content_color'] ) ) ? $instance['content_color'] : NULL,
								'label' => __( 'content Color' , 'bride' ),
							),
							'content_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',
								'default' => '0',
								'name' => $this->get_layers_field_name( 'content_font_size' ) ,
								'id' => $this->get_layers_field_id( 'content_font_size' ) ,
								'value' => ( isset( $instance['content_font_size'] ) ) ? $instance['content_font_size'] : NULL,
								'label' => __( 'Content Font Size' , 'bride' ),
							),								
							'readmore_style' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'readmore_style' ) ,
								'id' => $this->get_layers_field_id( 'readmore_style' ) ,
								'value' => ( isset( $instance['readmore_style'] ) ) ? $instance['readmore_style'] : NULL,
								'label' => __( 'Select Button Style' , 'bride' ),
								'options' => array(
										'' => __( 'Select Opions' , 'bride' ),
										'btn_left' => __( 'Button Left' , 'bride' ),
										'btn_middle' => __( 'Button Middle' , 'bride' ),
										'btn_right' => __( 'Button Right' , 'bride' ),
								)
							),
							'btn_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',
								'default' => '14',	
								'name' => $this->get_layers_field_name( 'btn_font_size' ) ,
								'id' => $this->get_layers_field_id( 'btn_font_size' ) ,
								'value' => ( isset( $instance['btn_font_size'] ) ) ? $instance['btn_font_size'] : NULL,
								'label' => __( 'Button Font Size' , 'bride' ),
							),
							'btn_text_hover_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'btn_text_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'btn_text_hover_color' ) ,
								'value' => ( isset( $instance['btn_text_hover_color'] ) ) ? $instance['btn_text_hover_color'] : NULL,
								'label' => __( 'Button Text Hover' , 'bride' ),
							),							
							'btn_hover_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'btn_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'btn_hover_color' ) ,
								'value' => ( isset( $instance['btn_hover_color'] ) ) ? $instance['btn_hover_color'] : NULL,
								'label' => __( 'Button BG Hover' , 'bride' ),
							),							
							'pading_tb' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',
								'name' => $this->get_layers_field_name( 'pading_tb' ) ,
								'id' => $this->get_layers_field_id( 'pading_tb' ) ,
								'value' => ( isset( $instance['pading_tb'] ) ) ? $instance['pading_tb'] : NULL,
								'label' => __( 'Padding Top/Bottom' , 'bride' ),
							),
							'pading_lr' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',	
								'name' => $this->get_layers_field_name( 'pading_lr' ) ,
								'id' => $this->get_layers_field_id( 'pading_lr' ) ,
								'value' => ( isset( $instance['pading_lr'] ) ) ? $instance['pading_lr'] : NULL,
								'label' => __( 'Padding Left/Right' , 'bride' ),
							),
							
							
							
						),
						
					),						
					'columns',
					'buttons',					
					'background',
					'advanced',
				), $this, $instance )
			); ?>
			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' =>  __( 'Bride Blog Widget' , 'bride' ),
					'icon_class' =>'post'
				) ); ?>

				<section class="layers-accordion-section layers-content">

					<div class="layers-row layers-push-bottom">
						<div class="layers-form-item">

							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'title' ) ,
									'id' => $this->get_layers_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'bride' ),
									'value' => ( isset( $instance['title'] ) ) ? $instance['title'] : NULL,
									'class' => 'layers-text layers-large'
								)
							); ?>

							<?php $this->design_bar(
								'top', // CSS Class Name
								array( // Widget Object
									'name' => $this->get_layers_field_name( 'design' ),
									'id' => $this->get_layers_field_id( 'design' ),
									'widget_id' => $this->widget_id,
									'show_trash' => FALSE,
									'inline' => TRUE,
									'align' => 'right',
								),
								$instance, // Widget Values
								apply_filters( 'layers_post_widget_inline_design_bar_components', 
								array( // Components
								'fonts' => array(
									'elements' => array(
										'title_border_bottom_color' => array(
											'type' => 'color',
											'label' => __( 'Section Title border bottom color' , 'sopnovilla' ),
											'name' => $this->get_layers_field_name( 'title_border_bottom_color' ),
											'id' => $this->get_layers_field_id(title_border_bottom_color),
											'value' => ( isset( $item_instance['title_border_bottom_color'] ) ) ? $item_instance['title_border_bottom_color'] : NULL,
										),
									),
								  ),
								),
								
								$this, $instance )
							); ?>

						</div>
						<div class="layers-form-item">

							<?php echo $this->form_elements()->input(
								array(
									'type' => 'rte',
									'name' => $this->get_layers_field_name( 'excerpt' ) ,
									'id' => $this->get_layers_field_id( 'excerpt' ) ,
									'placeholder' => __( 'Short Excerpt' , 'bride' ),
									'value' => ( isset( $instance['excerpt'] ) ) ? $instance['excerpt'] : NULL,
									'class' => 'layers-textarea layers-large'
								)
							); ?>

						</div>
						<?php // Grab the terms as an array and loop 'em to generate the $options for the input
						$terms = get_terms( $this->taxonomy , array( 'hide_empty' => false ) );
						if( !is_wp_error( $terms ) ) { ?>
							<p class="layers-form-item">
								<label for="<?php echo $this->get_layers_field_id( 'category' ); ?>"><?php echo __( 'Category to Display' , 'bride' ); ?></label>
								<?php $category_options[ 0 ] = __( 'All' , 'bride' );
								foreach ( $terms as $t ) $category_options[ $t->term_id ] = $t->name;
								echo $this->form_elements()->input(
									array(
										'type' => 'select',
										'name' => $this->get_layers_field_name( 'category' ) ,
										'id' => $this->get_layers_field_id( 'category' ) ,
										'placeholder' => __( 'Select a Category' , 'bride' ),
										'value' => ( isset( $instance['category'] ) ) ? $instance['category'] : NULL,
										'options' => $category_options,
									)
								); ?>
							</p>
						<?php } // if !is_wp_error ?>
						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'posts_per_page' ); ?>"><?php echo __( 'Number of items to show' , 'bride' ); ?></label>
							<?php $select_options[ '-1' ] = __( 'Show All' , 'bride' );
							$select_options = $this->form_elements()->get_incremental_options( $select_options , 1 , 20 , 1);
							echo $this->form_elements()->input(
								array(
									'type' => 'number',
									'name' => $this->get_layers_field_name( 'posts_per_page' ) ,
									'id' => $this->get_layers_field_id( 'posts_per_page' ) ,
									'value' => ( isset( $instance['posts_per_page'] ) ) ? $instance['posts_per_page'] : NULL,
									'min' => '-1',
									'max' => '100'
								)
							); ?>
						</p>

						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'order' ); ?>"><?php echo __( 'Sort by' , 'bride' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'select',
									'name' => $this->get_layers_field_name( 'order' ) ,
									'id' => $this->get_layers_field_id( 'order' ) ,
									'value' => ( isset( $instance['order'] ) ) ? $instance['order'] : NULL,
									'options' => $this->form_elements()->get_sort_options()
								)
							); ?>
						</p>
					</div>
				</section>
			</div>
		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Bride_Blog_Widget_2");
}