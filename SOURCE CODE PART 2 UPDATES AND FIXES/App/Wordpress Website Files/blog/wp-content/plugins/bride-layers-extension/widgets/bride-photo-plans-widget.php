<?php  /**
 * Gallery Widget
 *
 * This file is used to register and display the Layers - Photo plan widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
 
if( class_exists('Layers_Widget') && !class_exists( 'bride_photo_plan_widget' ) ) {	
	class bride_photo_plan_widget extends Layers_Widget {

	/**
	*  Widget construction
	*/
	function __construct(){

		/**
		* Widget variables
		*
		* @param  	string    		$widget_title    	Widget title
		* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
		* @param  	string    		$post_type    		(optional) Post type for use in widget options
		* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
		* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
		*/
		$this->widget_title = __( 'Bride :: Wedding Photo Plans Widget' , 'bride' );
		$this->widget_id = 'photo_plans';
		$this->post_type = 'bride_photo_plans';
		$this->taxonomy = 'bride_gallery_category';
		$this->checkboxes = array(
			'gallery_style',
			'filter_show',					
			'text_style',
			'show_cat',
			'show_media',
			'show_content',
			'show_icon',
			'show_titles',
			'show_detail',
			'show_pagination'			
		); // @TODO: Try make this more dynamic, or leave a different note reminding users to change this if they add/remove checkboxes

		/* Widget settings. */
		$widget_ops = array(
			'classname'   => 'obox-layers-' . $this->widget_id .'-widget',
			'description' => __( 'This widget is used to display your posts in a flexible grid.', 'bride' ),
		);

		/* Widget control settings. */
		$control_ops = array(
			'width'   => LAYERS_WIDGET_WIDTH_SMALL,
			'height'  => NULL,
			'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
		);

		/* Create the widget. */
		parent::__construct(
			LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
			$this->widget_title,
			$widget_ops,
			$control_ops
		);

		/* Setup Widget Defaults */
		$this->defaults = array (
			'title' => __( 'wedding plans', 'bride' ),
			'excerpt' => __( 'We provide the venue for the most amazing wedding reception', 'bride' ),
			'category' => 0,
			'gallery_style' => 'gallery1',
			'filter_show' => 'fhide',
			'text_style' => 'filter_menu',
			'show_media' => 'on',
			'show_content' => 'on',
			'show_icon' => 'on',
			'show_titles' => 'on',
			'show_detail' => 'on',
			'show_cat' => '',
			'show_pagination' => 'off',
			'section_title_color' => '#4a4a4a',
			'gallery_text_align' => 'left',
			'fontsize' => '14',
			'readmore_color' => '#535353',
			'readmore_hover_color' => '#a499c9',
			'boder_width' => '',
			'boder_radius' => '3',
			'border_color' => '',
			'hover_color' => '#a499c9',
			'pading_tb' => '',
			'pading_lr' => '',
			'bg_overlay_color' => '#fff',
			'bgoverlay_gradient_right_color' => '#0071b0',
			'o_opacity_default' => '0',
			'o_opacity' => '0.80',
			'o_tb' => '0',
			'o_lr' => '0',
			'icon_color' => '#ffffff',
			'icon_bg_color' => '#a499c9',
			'icon_hover_text_color' => '#fff',
			'icon_hover_color' => '#a499c9',
			'icon_border_color' => '#a499c9',
			'icon_font_size' => '15',
			'icon_box_shadow' => '',
			'set_icon' => 'plus',
			'icon_border_radius' => '100',
			'text_bg_color' => '#a499c9',
			'text_bg_opacity' => '0.8',
			'title_text_transform' => 'capitalize',	
			'title_color' => '#535353',
			'title_font_size' => '22',
			'title_hover' => '#a499c9',
			'cat_color' => '#535353',
			'cat_text_transform' => 'capitalize',
			'cat_font_size' => '15',
			'posts_per_page' => get_option( 'posts_per_page' ),
			'design' => array(
				'layout' => 'layout-Full-Width',
				'textalign' => 'text-left',
				'columns' => '4',
				'gutter' => 'on',
				'background' => array(
					'position' => 'center',
					'repeat' => 'no-repeat'
				),
				'fonts' => array(
					'align' => 'text-center',
					'size' => 'medium',
					'color' => NULL,
					'shadow' => NULL,
					'heading-type' => 'h2',
				)
			)
		);
	}

	/**
	*  Widget front end display
	*/
	function widget( $args, $instance ) {
		
		global $wp_customize;

		$this->backup_inline_css();

		// Turn $args array into variables.
		extract( $args );

		// Use defaults if $instance is empty.
		if( empty( $instance ) && ! empty( $this->defaults ) ) {
			$instance = wp_parse_args( $instance, $this->defaults );
		}
		
		// Mix in new/unset defaults on every instance load (NEW)
		$instance = $this->apply_defaults( $instance );

		// Set the span class for each column
		if( isset( $instance['design'][ 'columns']  ) ) {
			$col_count = str_ireplace('columns-', '', $instance['design'][ 'columns']  );
			$span_class = 'span-' . ( 12/ $col_count );
		} else {
			$col_count = 3;
			$span_class = 'span-4';
		}

		// Apply Styling
		$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $instance['design'][ 'background' ] ) );
		$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'color', array( 'selectors' => array( '.section-title h2' , '.section-title p' ) , 'color' => $instance['design']['fonts'][ 'color' ] ) );
		$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'selectors' => array( '.thumbnail-body' ) , 'background' => array( 'color' => $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ) );

		// Begin query arguments
		$query_args = array();
		if( get_query_var('paged') ) {
			$query_args[ 'paged' ] = get_query_var('paged') ;
		} else if ( get_query_var('page') ) {
			$query_args[ 'paged' ] = get_query_var('page');
		} else {
			$query_args[ 'paged' ] = 1;
		}			

		$query_args[ 'post_type' ] = $this->post_type;
		$query_args[ 'posts_per_page' ] = $instance['posts_per_page'];
		if( isset( $instance['order'] ) ) {

			$decode_order = json_decode( $instance['order'], true );

			if( is_array( $decode_order ) ) {
				foreach( $decode_order as $key => $value ){
					$query_args[ $key ] = $value;
				}
			}
		}

		// Do the special taxonomy array()
		if( isset( $instance['category'] ) && '' != $instance['category'] && 0 != $instance['category'] ){

			$query_args['tax_query'] = array(
				array(
					"taxonomy" => $this->taxonomy,
					"field" => "id",
					"terms" => $instance['category']
				)
			);
		} elseif( !isset( $instance['hide_category_filter'] ) ) {
			$terms = get_terms( $this->taxonomy );
		} // if we haven't selected which category to show, let's load the $terms for use in the filter

		// Do the WP_Query
		$post_query = new WP_Query( $query_args );

		// Set the meta to display
		global $layers_post_meta_to_display;
		$layers_post_meta_to_display = array();
		if( isset( $instance['show_dates'] ) ) $layers_post_meta_to_display[] = 'date';
		if( isset( $instance['show_author'] ) ) $layers_post_meta_to_display[] = 'author';
		if( isset( $instance['show_categories'] ) ) $layers_post_meta_to_display[] = 'categories';
		if( isset( $instance['show_tags'] ) ) $layers_post_meta_to_display[] = 'tags';

		/**
		* Generate the widget container class
		*/
		$widget_container_class = array();

		$widget_container_class[] = 'widget';
		$widget_container_class[] = 'clearfix';
		$widget_container_class[] = 'content-vertical-massive';			
		$widget_container_class[] = ( 'on' == $this->check_and_return( $instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
		$widget_container_class[] = $this->check_and_return( $instance , 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
		$widget_container_class[] = $this->get_widget_spacing_class( $instance );

		$widget_container_class = apply_filters( 'layers_post_widget_container_class' , $widget_container_class, $this, $instance );
		$widget_container_class = implode( ' ', $widget_container_class ); ?>
		<?php echo $this->custom_anchor( $instance ); ?>
		<div id="<?php echo esc_attr( $widget_id ); ?>" class="pt90 pb80 gallery-area <?php echo esc_attr( $widget_container_class ); ?>">

			<?php do_action( 'layers_before_post_widget_inner', $this, $instance ); ?>

			<?php if( '' != $this->check_and_return( $instance , 'title' ) ||'' != $this->check_and_return( $instance , 'excerpt' ) ) { ?>
				
			<div class="container clearfix">
				<?php /**
				* Generate the Section Title Classes
				*/
				$section_title_class = array();
				$section_title_class[] = 'section-title clearfix';
				$section_title_class[] = 'section-title-wrapper clearfix';
				$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'size' );
				$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'align' );
				$section_title_class[] = ( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ? 'invert' : '' );
				$section_title_class = implode( ' ', $section_title_class ); ?>
				<div class="<?php echo $section_title_class; ?>">

					<?php if( '' != $this->check_and_return( $instance, 'title' )  ) { ?>
						<<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?> class="heading">
							<?php echo $instance['title'] ?>
						</<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?>>
					<?php } ?>
					<?php if( '' != $this->check_and_return( $instance, 'excerpt' )  ) { ?>
						<div class="excerpt"><?php echo layers_the_content( $instance['excerpt'] ); ?></div>
					<?php } ?>

					
				</div>
			</div>
				
			<?php } ?>
			
			
			<!-- style filter  -->		
			<?php if( '' == $this->check_and_return( $instance, 'filter_show' )  ) { ?>							
			<!-- Filter Nav Cat-->							
			<div class="container">
			<?php if( 'filter_menu_left' == $this->check_and_return( $instance, 'text_style' )  ) { ?>	
				<div class="gallery-menu filter_menu_left">
			<?php }elseif( 'filter_menu' == $this->check_and_return( $instance, 'text_style' )  ) { ?>
				 <div class="gallery-menu ">
			<?php }elseif( 'filter_menu_right' == $this->check_and_return( $instance, 'text_style' )  ) { ?>	 
				 <div class="gallery-menu filter_menu_right">
			<?php }else{ ?>	
				  <div class="gallery-menu ">
			<?php } ?>
				<ul id="filter" class="filter_menu  nrb_menu_f">
					<li class="current_menu_item" data-filter="*"><?php esc_html_e( 'All' , 'bride' ); ?></li>
					<?php foreach( $terms as $cats ) {							
						$slug = $cats->slug;
						$name = $cats->name;									
					?>
					<li class="filter" data-filter=".<?php echo $slug ;?>"><?php echo $name; ?></li>
					<?php } ?>
				</ul>
				</div><!-- gallery_menu End --> 				
			</div>								
			<?php }elseif( 'fshow' == $this->check_and_return( $instance, 'filter_show' )  ) { ?>
			<!-- Filter Nav Cat-->							
			<div class="container">
				<?php if( 'filter_menu_left' == $this->check_and_return( $instance, 'text_style' )  ) { ?>	
					<div class="gallery-menu filter_menu_left">
				<?php }elseif( 'filter_menu' == $this->check_and_return( $instance, 'text_style' )  ) { ?>
					 <div class="gallery-menu ">
				<?php }elseif( 'filter_menu_right' == $this->check_and_return( $instance, 'text_style' )  ) { ?>	 
					 <div class="gallery-menu filter_menu_right">
				<?php }else{ ?>	
					  <div class="gallery-menu ">
				<?php } ?>
					<ul id="filter" class="filter_menu nrb_menu_f">
						<li class="current_menu_item" data-filter="*"><?php esc_html_e( 'All' , 'bride' ); ?></li>
						<?php foreach( $terms as $cats ) {							
							$slug = $cats->slug;
							$name = $cats->name;									
						?>
						<li class="filter" data-filter=".<?php echo $slug ;?>"><?php echo $name; ?></li>
						<?php } ?>
					</ul>
				</div><!-- gallery_menu End --> 				
			</div>	
			<?php }elseif( 'fhide' == $this->check_and_return( $instance, 'filter_show' )  ) { ?>
			
			<?php } ?>	

			<!-- Filter post Query-->
			<?php if( $post_query->have_posts() ) { ?>
			<div class="<?php echo $this->get_widget_layout_class( $instance ); ?>">
			<?php if( 'gallery1' == $this->check_and_return( $instance, 'gallery_style' ) ) { ?>
				<div class="grid new style_1_gl">							 
					<?php while( $post_query->have_posts() ) {
						$post_query->the_post();
							/**
							* Set Individual Column CSS
							*/
							$post_column_class = array();
							$post_column_class[] = 'gallery-hover';
							$post_column_class[] = 'cols';
							$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
							$post_column_class[] = $span_class;
							$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
							$post_column_class = implode( ' ' , $post_column_class );
					?>		
					<?php 
						$ht_categories = get_the_terms(get_the_id(),'bride_gallery_category');
					?>									
					<div class="grid-item  <?php echo $post_column_class; ?>" data-cols="<?php echo $col_count; ?>">
						<!-- start copy -->	
						<div class="ht-gallery thumbnail-body">
							<!-- single gallery item -->	
							<div class="gallery-effect">
								<?php if( isset( $instance['show_media'] ) ) {
									if ( has_post_thumbnail() ) {?>
									<div class="ht-gallery-thumb">
										<?php  the_post_thumbnail(); ?>
										<?php if( isset( $instance['show_content'] ) ) {?>	
										<!-- gallery content  area -->
										<div class="gallery-hover-effect">
											<div class="ht-gallery-content">
												<?php if( isset( $instance['show_icon'] ) ) {?>
												<!-- gallery icon -->
												<a class="gallery-icon venobox" data-gall="mygallery" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ), 'full' );?>">
												    <i class="fa fa-<?php echo esc_html( $instance['set_icon'] ); ?>"></i>
												</a>												
												<!-- end gallery icon -->
												<?php } //end condition show icon ?>
												
												
												<?php if( $instance['show_titles'] != ''  ) { ?>
													<!-- gallery title -->
													<h2>
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</h2>
													<!-- end gallery title -->
												<?php } // end condition title?>

												<?php if( $instance['show_detail'] != ''  ) { ?>
													<!-- Show content -->
													<?php the_content(); ?>
													<!-- end gallery title -->
												<?php } // end condition title?>

												<?php if( $instance['show_cat'] != ''  ) { ?>	
												<!-- gallery category -->
												<?php if( $ht_categories ){
												foreach( $ht_categories as $single_slugs ){?>
												<span class="category-name">
												   <?php echo $single_slugs->name ;?>
												</span>		
												<?php }}?>
												<!-- end gallery category -->	
												<?php } // end condition category ?>
												
												
												
											</div>
										</div>
										<!-- end gallery content  area -->
										<?php } //end condition show content ?>
									</div>
									<?php } // end condition has image ?>
								<?php } //end condition show meadia ?>
							</div>
							<!-- end single gallery item -->	
						</div>
						<!-- end copy -->
					</div>										
					<?php }; // while have_posts ?>
				</div><!-- /row -->
						
				<?php }elseif( 'gallery2' == $this->check_and_return( $instance, 'gallery_style' ) ){ ?>
				<div class="grid new">							 
					<?php while( $post_query->have_posts() ) {
						$post_query->the_post();
							/**
							* Set Individual Column CSS
							*/
							$post_column_class = array();
							$post_column_class[] = 'gallery-hover';
							$post_column_class[] = 'cols';
							$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
							$post_column_class[] = $span_class;
							$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
							$post_column_class = implode( ' ' , $post_column_class );
					?>																								
					<?php 
						$ht_categories = get_the_terms(get_the_id(),'gallery_category');
					?>									
					<div class="grid-item  <?php echo $post_column_class; ?> <?php foreach($ht_categories as $single_slug){echo $single_slug->slug. ' ';}	?>" data-cols="<?php echo $col_count; ?>">
						<!-- start copy -->	
						<div class="ht-gallery thumbnail-body">
							<!-- single gallery item -->	
							<div class="gallery-effect">
								<?php if( isset( $instance['show_media'] ) ) {
									 if ( has_post_thumbnail() ) {?>
										<div class="ht-gallery-thumb">
											<?php  the_post_thumbnail(); ?>
											<?php if( isset( $instance['show_content'] ) ) {?>	
											<!-- gallery content  area -->
											<div class="gallery-hover-effect">
												<div class="ht-gallery-content">
												<?php if( isset( $instance['show_icon'] ) ) {?>
													<!-- gallery icon -->
													<a class="gallery-icon venobox" data-gall="mygallery" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ), 'full' );?>">
												    <i class="fa fa-<?php echo esc_html( $instance['set_icon'] ); ?>"></i>
													</a>												
													<!-- end gallery icon -->
													<?php } //end condition show icon ?>
												</div>
											</div>
											<!-- end gallery content  area -->
											<?php } //end condition show content ?>
										</div>
									<?php } // end condition has image ?>
								<?php } //end condition show meadia ?>
								<div class="ht-gallery-content2">
										<?php if( $instance['show_titles'] != ''  ) { ?>
										<!-- gallery title -->
										<h2>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h2>
										<!-- end gallery title -->
									<?php } // end condition title?>

									<?php if( $instance['show_detail'] != ''  ) { ?>
										<!-- Show content -->
										<?php the_content(); ?>
										<!-- end gallery title -->
									<?php } // end condition title?>

									<?php if( $instance['show_cat'] != ''  ) { ?>	
									<!-- gallery category -->
									<?php if( $ht_categories ){
									foreach( $ht_categories as $single_slugs ){?>
										<span class="category-name">
										   <?php echo $single_slugs->name ;?>
										</span>
									<?php }}?>
									<!-- end gallery category -->	
									<?php } // end condition category ?>						
								</div>
							</div>
							<!-- end single gallery item -->	
						</div>
						<!-- end copy -->
					</div>										
				<?php }; // while have_posts ?>
				</div><!-- /row -->
				<?php } elseif( 'gallery3' == $this->check_and_return( $instance, 'gallery_style' ) ) { ?>
				<div class="grid new">							 
					<?php while( $post_query->have_posts() ) {
						$post_query->the_post();
							/**
							* Set Individual Column CSS
							*/
							$post_column_class = array();
							$post_column_class[] = 'gallery-hover';
							$post_column_class[] = 'cols';
							$post_column_class[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
							$post_column_class[] = $span_class;
							$post_column_class[] = ( '' != $this->check_and_return( $instance, 'design', 'column-background-color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'column-background-color' ) ) ? 'invert' : '' );
							$post_column_class = implode( ' ' , $post_column_class );
					?>		
					<?php 
						$ht_categories = get_the_terms(get_the_id(),'gallery_category');
					?>									
					<div class="grid-item  <?php echo $post_column_class; ?> <?php foreach($ht_categories as $single_slug){echo $single_slug->slug. ' ';}	?>" data-cols="<?php echo $col_count; ?>">
						<!-- start copy -->	
						<div class="ht-gallery-3 ht-gallery thumbnail-body">
							<!-- single gallery item -->	
							<div class="gallery-effect">
								<?php if( isset( $instance['show_media'] ) ) {
									if ( has_post_thumbnail() ) {?>
									<div class="ht-gallery-thumb">
										<?php  the_post_thumbnail(); ?>
										<?php if( isset( $instance['show_content'] ) ) {?>	
										<!-- gallery content  area -->
										<div class="gallery-hover-effect">
											<div class="ht-gallery-content">
												<?php if( isset( $instance['show_icon'] ) ) {?>
												<!-- gallery icon -->
												<a class="gallery-icon venobox" data-gall="mygallery" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ), 'full' );?>">
													<i class="icofont <?php echo esc_html( $instance['set_icon'] ); ?>"></i>
												</a>												
												<!-- end gallery icon -->
												<?php } //end condition show icon ?>
	
											</div>
											<div class="ht-gallery-content-deltails">
												<?php if( $instance['show_titles'] != ''  ) { ?>
													<!-- gallery title -->
													<h2>
														<a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
													</h2>
													<!-- end gallery title -->
												<?php } // end condition title?>

												<?php if( $instance['show_detail'] != ''  ) { ?>
 													<?php the_content(); ?>
												<?php } // end condition title?>

												<?php if( $instance['show_cat'] != ''  ) { ?>	
												<!-- gallery category -->
												<?php if( $ht_categories ){
												foreach( $ht_categories as $single_slugs ){?>
												<span class="category-name">
												   <?php echo $single_slugs->name ;?>
												</span>		
												<?php }}?>
												<!-- end gallery category -->	
												<?php } // end condition category ?>												
											</div>
										</div>
										<!-- end gallery content  area -->
										<?php } //end condition show content ?>
									</div>
									<?php } // end condition has image ?>
								<?php } //end condition show meadia ?>
							</div>
							<!-- end single gallery item -->	
						</div>
						<!-- end copy -->
					</div>										
					<?php }; // while have_posts ?>
				</div><!-- /row -->
						
				<?php } ?>	
			</div>
			<?php }; // if have_posts ?>	
			<?php if( isset( $instance['show_pagination'] ) ) { ?>
				<div class="container">					
					<div class="pagination-content pagination2">
						<div class="pagination-button">
						<?php layers_pagination( array( 'query' => $post_query ), 'div', 'pagination clearfix' );?>					  
						</div>
					</div>						
				</div>
			<?php } //end pagination?>					
			<?php do_action( 'layers_after_post_widget_inner', $this, $instance );

			// Print the Inline Styles for this Widget
			$this->print_inline_css();			
			
			?>
			
			<style type="text/css">	
			
				#<?php echo esc_attr( $widget_id ); ?> ul#filter.nrb_menu_f.filter_menu li {	  
					color: <?php echo esc_html( $instance['readmore_color'] ); ?>;
					font-size: <?php echo esc_html( $instance['fontsize'] ); ?>px;
					padding: 0 <?php echo esc_html( $instance['pading_lr'] ); ?>px;	  
				}
				#<?php echo esc_attr( $widget_id ); ?> ul#filter.nrb_menu_f.filter_menu li.current_menu_item,
				#<?php echo esc_attr( $widget_id ); ?> ul#filter.nrb_menu_f.filter_menu li:hover
				 {
				  color:<?php echo esc_html( $instance['readmore_hover_color'] ); ?>;	
				  border-color:<?php echo esc_html( $instance['hover_color'] ); ?>;
				}
				
				
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-thumb::before{
				  background:<?php echo esc_html( $instance['bg_overlay_color'] ); ?>;
				  opacity:<?php echo esc_html( $instance['o_opacity_default'] ); ?>;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery:hover .ht-gallery-thumb::before{
					opacity: <?php echo esc_html( $instance['o_opacity'] ); ?>;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .gallery-icon {
				  font-size: <?php echo esc_html( $instance['icon_font_size'] ); ?>px;
				  border-radius: <?php echo esc_html( $instance['icon_border_radius'] ); ?>%;
				  color: <?php echo esc_html( $instance['icon_color'] ); ?>;
				  box-shadow:<?php echo esc_html( $instance['icon_box_shadow'] ); ?>;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .gallery-icon:before {
				  background:<?php echo esc_html( $instance['icon_bg_color'] ); ?>;
				  border-radius: <?php echo esc_html( $instance['icon_border_radius'] ); ?>%;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .gallery-icon {
				  background:<?php echo esc_html( $instance['icon_bg_color'] ); ?>;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .gallery-icon:hover{
					color:<?php echo esc_html( $instance['icon_hover_text_color'] ); ?>;
					background:<?php echo esc_html( $instance['icon_hover_color'] ); ?>;
					border-color:<?php echo esc_html( $instance['icon_hover_color'] ); ?>;
				}
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content > h2,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content-deltails > h2,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content-deltails > h2 > a,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content > h2 a,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content2 > h2,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content2 > h2 a	
				{
				  color: <?php echo esc_html( $instance['title_color'] ); ?>;
				  font-size: <?php echo esc_html( $instance['title_font_size'] ); ?>px;
				}
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content > h2:hover,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content-deltails > h2:hover a,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content-deltails > h2 > a:hover,				
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content > h2 a:hover,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content2 > h2:hover,
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content2 > h2 a:hover	
				{
				  color: <?php echo esc_html( $instance['title_hover'] ); ?>;
				}
				#<?php echo esc_attr( $widget_id ); ?> .category-name {
				  color: <?php echo esc_html( $instance['cat_color'] ); ?>;
				  font-size: <?php echo esc_html( $instance['cat_font_size'] ); ?>px;
				}
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content > h2, .ht-gallery-content2 > h2, .category-name{
				  text-align: <?php echo esc_html( $instance['gallery_text_align'] ); ?>;
				}
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-content > h2, .ht-gallery-content2 > h2, .ht-gallery-content-deltails > h2 {
				  text-transform: <?php echo esc_html( $instance['title_text_transform'] ); ?>;
				}
				#<?php echo esc_attr( $widget_id ); ?> .category-name {
				  text-transform: <?php echo esc_html( $instance['cat_text_transform'] ); ?>;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-3 .ht-gallery-content-deltails{
				  left: <?php echo esc_html( $instance['o_lr'] ); ?>px;
				  bottom: <?php echo esc_html( $instance['o_tb'] ); ?>px;
				  right: <?php echo esc_html( $instance['o_lr'] ); ?>px;
				}	
				#<?php echo esc_attr( $widget_id ); ?> .section-title h2::before{
				  background: <?php echo esc_html( $instance['title_border_bottom_color'] ); ?>;
				}
				
				#<?php echo esc_attr( $widget_id ); ?> .section-title h2::after {
				  color: <?php echo esc_html( $instance['title_border_bottom_color'] ); ?>;
				}
				#<?php echo esc_attr( $widget_id ); ?> .ht-gallery-3 .ht-gallery-content-deltails::before{
				  background-color:<?php echo esc_html( $instance['text_bg_color'] ); ?>;
				  opacity:<?php echo esc_html( $instance['text_bg_opacity'] ); ?>;
				}

				<?php if( isset( $instance['title_border_bottom_image'] ) ) {  ?>
					#<?php echo esc_attr( $widget_id ); ?> .section-title h2.heading::after {
						<?php if( isset( $instance['title_border_bottom_image'] ) ) { 
							$title_border_bottom_image =  wp_get_attachment_url($instance['title_border_bottom_image'], 'full');
						} ?>
					  background: rgba(0, 0, 0, 0) url("<?php echo esc_url( $title_border_bottom_image ); ?> ") no-repeat scroll 0 0;
					}
				<?php 	} ?>
				
				
			</style>
		</div>
		<?php // Reset WP_Query
		wp_reset_postdata();

		// Apply the advanced widget styling
		$this->apply_widget_advanced_styling( $widget_id, $instance );

	}

	/**
	*  Widget update
	*/

	function update($new_instance, $old_instance) {

		if ( isset( $this->checkboxes ) ) {
			foreach( $this->checkboxes as $cb ) {
				if( isset( $old_instance[ $cb ] ) ) {
					$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
				}
			} // foreach checkboxes
		} // if checkboxes
		return $new_instance;
	}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			$this->design_bar(
				'side', // CSS Class Name
				array( // Widget Object
					'name' => $this->get_layers_field_name( 'design' ),
					'id' => $this->get_layers_field_id( 'design' ),
					'widget_id' => $this->widget_id,
				),
				$instance, // Widget Values
				apply_filters( 'layers_post_widget_design_bar_components' , array( // Components
					'layout',
					'display' => array(
						'icon-css' => 'icon-display',
						'label' => __( 'Display', 'bride' ),
						'elements' => array(
														
							'show_media' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_media' ) ,
								'id' => $this->get_layers_field_id( 'show_media' ) ,
								'value' => ( isset( $instance['show_media'] ) ) ? $instance['show_media'] : NULL,
								'label' => __( 'Show Featured Images' , 'bride' )
							),
							'show_content' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_content' ) ,
								'id' => $this->get_layers_field_id( 'show_content' ) ,
								'value' => ( isset( $instance['show_content'] ) ) ? $instance['show_content'] : NULL,
								'label' => __( 'Show Content' , 'bride' )
							),
							'show_icon' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_icon' ) ,
								'id' => $this->get_layers_field_id( 'show_icon' ) ,
								'value' => ( isset( $instance['show_icon'] ) ) ? $instance['show_icon'] : NULL,
								'label' => __( 'Show Icon' , 'bride' )
							),							
							'show_titles' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_titles' ) ,
								'id' => $this->get_layers_field_id( 'show_titles' ) ,
								'value' => ( isset( $instance['show_titles'] ) ) ? $instance['show_titles'] : NULL,
								'label' => __( 'Show Title' , 'bride' )
							),					
							'show_detail' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_detail' ) ,
								'id' => $this->get_layers_field_id( 'show_detail' ) ,
								'value' => ( isset( $instance['show_detail'] ) ) ? $instance['show_detail'] : NULL,
								'label' => __( 'Show Detail' , 'bride' )
							),
							
							'show_cat' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_cat' ) ,
								'id' => $this->get_layers_field_id( 'show_cat' ) ,
								'value' => ( isset( $instance['show_cat'] ) ) ? $instance['show_cat'] : NULL,
								'label' => __( 'Show Category' , 'bride' )
							),							
							'show_pagination' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_pagination' ) ,
								'id' => $this->get_layers_field_id( 'show_pagination' ) ,
								'value' => ( isset( $instance['show_pagination'] ) ) ? $instance['show_pagination'] : NULL,
								'label' => __( 'show pagination' , 'bride' )
							),					
						),
					),
					'colorown' => array(
						'icon-css' => 'icon-call-to-action',
						'label' => __( 'Color Style', 'bride' ),
						'elements' => array(
							'gallery_text_align' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'gallery_text_align' ) ,
								'id' => $this->get_layers_field_id( 'gallery_text_align' ) ,
								'value' => ( isset( $instance['gallery_text_align'] ) ) ? $instance['gallery_text_align'] : NULL,
								'label' => __( 'Text Align' , 'bride' ),
								'default' => 'center',
								'options' => array(
										'left' => __( 'Left' , 'bride' ),
										'center' => __( 'Center' , 'bride' ),
										'right' => __( 'Right' , 'bride' ),
								)
							),
							'fontsize' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',									
								'name' => $this->get_layers_field_name( 'fontsize' ) ,
								'id' => $this->get_layers_field_id( 'fontsize' ) ,
								'value' => ( isset( $instance['fontsize'] ) ) ? $instance['fontsize'] : NULL,
								'label' => __( 'Filter Font Size' , 'bride' ),
							),						
							'readmore_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'readmore_color' ) ,
								'id' => $this->get_layers_field_id( 'readmore_color' ) ,
								'value' => ( isset( $instance['readmore_color'] ) ) ? $instance['readmore_color'] : NULL,
								'label' => __( 'Filter Text Color' , 'bride' ),
							),
							'readmore_hover_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'readmore_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'readmore_hover_color' ) ,
								'value' => ( isset( $instance['readmore_hover_color'] ) ) ? $instance['readmore_hover_color'] : NULL,
								'label' => __( 'Filter Hover Text Color' , 'bride' ),
							),
							'pading_lr' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '50',
								'step' => '1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'pading_lr' ) ,
								'id' => $this->get_layers_field_id( 'pading_lr' ) ,
								'value' => ( isset( $instance['pading_lr'] ) ) ? $instance['pading_lr'] : NULL,
								'label' => __( 'Filter Padding Left/Right' , 'bride' ),
							),														
							'bg_overlay_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'bg_overlay_color' ) ,
								'id' => $this->get_layers_field_id( 'bg_overlay_color' ) ,
								'value' => ( isset( $instance['bg_overlay_color'] ) ) ? $instance['bg_overlay_color'] : NULL,
								'label' => __( 'BG Overlay Color' , 'bride' ),
							),												
							'o_opacity_default' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '1',
								'step' => '0.1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'o_opacity_default' ) ,
								'id' => $this->get_layers_field_id( 'o_opacity_default' ) ,
								'value' => ( isset( $instance['o_opacity_default'] ) ) ? $instance['o_opacity_default'] : NULL,
								'label' => __( 'Overlay Opacity Frontend ' , 'bride' ),
							),							
							'o_opacity' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '1',
								'step' => '0.1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'o_opacity' ) ,
								'id' => $this->get_layers_field_id( 'o_opacity' ) ,
								'value' => ( isset( $instance['o_opacity'] ) ) ? $instance['o_opacity'] : NULL,
								'label' => __( 'Overlay Hover Opacity' , 'bride' ),
							),							
							'icon_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'icon_color' ) ,
								'id' => $this->get_layers_field_id( 'icon_color' ) ,
								'value' => ( isset( $instance['icon_color'] ) ) ? $instance['icon_color'] : NULL,
								'label' => __( 'Icon color' , 'bride' ),
							),
							'icon_bg_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'icon_bg_color' ) ,
								'id' => $this->get_layers_field_id( 'icon_bg_color' ) ,
								'value' => ( isset( $instance['icon_bg_color'] ) ) ? $instance['icon_bg_color'] : NULL,
								'label' => __( 'Icon Bg color' , 'bride' ),
							),
							'icon_hover_text_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'icon_hover_text_color' ) ,
								'id' => $this->get_layers_field_id( 'icon_hover_text_color' ) ,
								'value' => ( isset( $instance['icon_hover_text_color'] ) ) ? $instance['icon_hover_text_color'] : NULL,
								'label' => __( 'Icon  Color On Hover' , 'bride' ),
							),							
							'icon_hover_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'icon_hover_color' ) ,
								'id' => $this->get_layers_field_id( 'icon_hover_color' ) ,
								'value' => ( isset( $instance['icon_hover_color'] ) ) ? $instance['icon_hover_color'] : NULL,
								'label' => __( 'Icon BG Color On Hover' , 'bride' ),
							),								
							'icon_box_shadow' => array(
								'type' => 'text',
								'name' => $this->get_layers_field_name( 'icon_box_shadow' ) ,
								'id' => $this->get_layers_field_id( 'icon_box_shadow' ) ,
								'value' => ( isset( $instance['icon_box_shadow'] ) ) ? $instance['icon_box_shadow'] : NULL,
								'label' => __( 'Icon Box Shadow' , 'bride' ),
							),
							'set_icon' => array(
								'type' => 'text',
								'name' => $this->get_layers_field_name( 'set_icon' ) ,
								'id' => $this->get_layers_field_id( 'set_icon' ) ,
								'value' => ( isset( $instance['set_icon'] ) ) ? $instance['set_icon'] : NULL,
								'label' => __( 'Inset Icon Name "plus"' , 'bride' ),
								'placeholder' =>'plus',
								
							),							
							'icon_border_radius' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'icon_border_radius' ) ,
								'id' => $this->get_layers_field_id( 'icon_border_radius' ) ,
								'value' => ( isset( $instance['icon_border_radius'] ) ) ? $instance['icon_border_radius'] : NULL,
								'label' => __( 'Icon Border Radius' , 'bride' ),
							),					
							'icon_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'icon_font_size' ) ,
								'id' => $this->get_layers_field_id( 'icon_font_size' ) ,
								'value' => ( isset( $instance['icon_font_size'] ) ) ? $instance['icon_font_size'] : NULL,
								'label' => __( 'Icon Font Size' , 'bride' ),
							),							
							'text_bg_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'text_bg_color' ) ,
								'id' => $this->get_layers_field_id( 'text_bg_color' ) ,
								'value' => ( isset( $instance['text_bg_color'] ) ) ? $instance['text_bg_color'] : NULL,
								'label' => __( 'Text bg color' , 'bride' ),
							),
							'text_bg_opacity' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '1',
								'step' => '0.1',
								'default' => '0',	
								'name' => $this->get_layers_field_name( 'text_bg_opacity' ) ,
								'id' => $this->get_layers_field_id( 'text_bg_opacity' ) ,
								'value' => ( isset( $instance['text_bg_opacity'] ) ) ? $instance['text_bg_opacity'] : NULL,
								'label' => __( 'Text bg opacity' , 'bride' ),
							),
							'title_text_transform' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'title_text_transform' ) ,
								'id' => $this->get_layers_field_id( 'title_text_transform' ) ,
								'value' => ( isset( $instance['title_text_transform'] ) ) ? $instance['title_text_transform'] : NULL,
								'label' => __( 'Text Transform' , 'bride' ),
								'default' => 'capitalize',
								'options' => array(
										'none' => __( 'None' , 'bride' ),
										'uppercase' => __( 'Uppercase' , 'bride' ),
										'lowercase' => __( 'lowercase' , 'bride' ),
										'capitalize' => __( 'Capitalize' , 'bride' ),
								)
							),								
							'title_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'title_color' ) ,
								'id' => $this->get_layers_field_id( 'title_color' ) ,
								'value' => ( isset( $instance['title_color'] ) ) ? $instance['title_color'] : NULL,
								'label' => __( 'title color' , 'bride' ),
							),
							'title_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',
								'name' => $this->get_layers_field_name( 'title_font_size' ) ,
								'id' => $this->get_layers_field_id( 'title_font_size' ) ,
								'value' => ( isset( $instance['title_font_size'] ) ) ? $instance['title_font_size'] : NULL,
								'label' => __( 'title font size' , 'bride' ),
							),
							
							'title_hover' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'title_hover' ) ,
								'id' => $this->get_layers_field_id( 'title_hover' ) ,
								'value' => ( isset( $instance['title_hover'] ) ) ? $instance['title_hover'] : NULL,
								'label' => __( 'title Hover color' , 'bride' ),
							),		
							'cat_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'cat_color' ) ,
								'id' => $this->get_layers_field_id( 'cat_color' ) ,
								'value' => ( isset( $instance['cat_color'] ) ) ? $instance['cat_color'] : NULL,
								'label' => __( 'Category Color' , 'bride' ),
							),
							'cat_text_transform' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'cat_text_transform' ) ,
								'id' => $this->get_layers_field_id( 'cat_text_transform' ) ,
								'value' => ( isset( $instance['cat_text_transform'] ) ) ? $instance['cat_text_transform'] : NULL,
								'label' => __( 'Text Transform' , 'bride' ),
								'default' => 'capitalize',
								'options' => array(
										'none' => __( 'None' , 'bride' ),
										'uppercase' => __( 'Uppercase' , 'bride' ),
										'lowercase' => __( 'lowercase' , 'bride' ),
										'capitalize' => __( 'Capitalize' , 'bride' ),
								)
							),								
							'cat_font_size' => array(
								'type' => 'range',
								'min' => '0',
								'max' => '100',
								'step' => '1',
								'default' => '0',
								'name' => $this->get_layers_field_name( 'cat_font_size' ) ,
								'id' => $this->get_layers_field_id( 'cat_font_size' ) ,
								'value' => ( isset( $instance['cat_font_size'] ) ) ? $instance['cat_font_size'] : NULL,
								'label' => __( 'Category font size' , 'bride' ),
							),
						),
					),											
					'columns',
					'background',
					'advanced',
				), $this, $instance )
			); ?>
			<div class="layers-container-large">
				<?php $this->form_elements()->header( array(
					'title' =>  __( 'Bride Wedding plans Widget' , 'bride' ),
					'icon_class' =>'post'
				) ); ?>
				<section class="layers-accordion-section layers-content">
					<div class="layers-row layers-push-bottom">
						<div class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'title' ) ,
									'id' => $this->get_layers_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'bride' ),
									'value' => ( isset( $instance['title'] ) ) ? $instance['title'] : NULL,
									'class' => 'layers-text layers-large'
								)
							); ?>

							<?php $this->design_bar(
								'top', // CSS Class Name
								array( // Widget Object
									'name' => $this->get_layers_field_name( 'design' ),
									'id' => $this->get_layers_field_id( 'design' ),
									'widget_id' => $this->widget_id,
									'show_trash' => FALSE,
									'inline' => TRUE,
									'align' => 'right',
								),
								$instance, // Widget Values
								
							apply_filters( 'layers_column_widget_inline_design_bar_components', 
							
							array( // Components
							'fonts' => array(
								'elements' => array(
									'title_border_bottom_image' => array(
										'type' => 'image',
										'name' => $this->get_layers_field_name( 'title_border_bottom_image' ) ,
										'id' => $this->get_layers_field_id( 'title_border_bottom_image' ) ,
										'value' => ( isset( $instance['title_border_bottom_image'] ) ) ? $instance['title_border_bottom_image'] : NULL,
										'label' => __( 'title border bottom image Add Here' , 'bride' )
									),
								),
							  ),
							),

							$this, $instance )
							); ?>
						</div>
						<div class="layers-form-item">
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'rte',
									'name' => $this->get_layers_field_name( 'excerpt' ) ,
									'id' => $this->get_layers_field_id( 'excerpt' ) ,
									'placeholder' => __( 'Short Excerpt' , 'bride' ),
									'value' => ( isset( $instance['excerpt'] ) ) ? $instance['excerpt'] : NULL,
									'class' => 'layers-textarea layers-large'
								)
							); ?>
						</div>
						<?php // Grab the terms as an array and loop 'em to generate the $options for the input
						$terms = get_terms( $this->taxonomy , array( 'hide_empty' => false ) );
						if( !is_wp_error( $terms ) ) { ?>
							<p class="layers-form-item">
								<label for="<?php echo $this->get_layers_field_id( 'category' ); ?>"><?php echo __( 'Category to Display' , 'bride' ); ?></label>
								<?php $category_options[ 0 ] = __( 'All' , 'bride' );
								foreach ( $terms as $t ) $category_options[ $t->term_id ] = $t->name;
								echo $this->form_elements()->input(
									array(
										'type' => 'select',
										'name' => $this->get_layers_field_name( 'category' ) ,
										'id' => $this->get_layers_field_id( 'category' ) ,
										'placeholder' => __( 'Select a Category' , 'bride' ),
										'value' => ( isset( $instance['category'] ) ) ? $instance['category'] : NULL,
										'options' => $category_options,
									)
								); ?>
							</p>
						<?php } // if !is_wp_error ?>
						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'posts_per_page' ); ?>"><?php echo __( 'Number of items to show' , 'bride' ); ?></label>
							<?php $select_options[ '-1' ] = __( 'Show All' , 'bride' );
							$select_options = $this->form_elements()->get_incremental_options( $select_options , 1 , 20 , 1);
							echo $this->form_elements()->input(
								array(
									'type' => 'number',
									'name' => $this->get_layers_field_name( 'posts_per_page' ) ,
									'id' => $this->get_layers_field_id( 'posts_per_page' ) ,
									'value' => ( isset( $instance['posts_per_page'] ) ) ? $instance['posts_per_page'] : NULL,
									'min' => '-1',
									'max' => '100'
								)
							); ?>
						</p>

						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'order' ); ?>"><?php echo __( 'Sort by' , 'bride' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'select',
									'name' => $this->get_layers_field_name( 'order' ) ,
									'id' => $this->get_layers_field_id( 'order' ) ,
									'value' => ( isset( $instance['order'] ) ) ? $instance['order'] : NULL,
									'options' => $this->form_elements()->get_sort_options()
								)
							); ?>
						</p>
					</div>
				</section>
			</div>
		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("bride_photo_plan_widget");
}