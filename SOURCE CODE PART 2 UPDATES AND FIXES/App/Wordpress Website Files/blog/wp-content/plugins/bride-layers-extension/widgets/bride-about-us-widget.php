<?php  /**
 * Counter Widget
 *
 * This file is used to register and display the Bride about widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( class_exists('Layers_Widget') && !class_exists( 'bride_About_Us_Widget' ) ) {
	class bride_About_Us_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function __construct() {

			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = esc_html__( 'Bride :: About Us Widget' , 'bride' );
			$this->widget_id = 'bride_about_us';
			$this->post_type = '';
			$this->taxonomy = '';
			$this->checkboxes = array();

			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'obox-layers-' . $this->widget_id .'-widget',
				'description' => esc_html__( 'This widget is used to display counter in a flexible grid.', 'bride' ),
				'customize_selective_refresh' => TRUE,
			);

			/* Widget control settings. */
			$control_ops = array(
				'width' => LAYERS_WIDGET_WIDTH_LARGE,
				'height' => NULL,
				'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
			);

			/* Create the widget. */
			parent::__construct(
				LAYERS_THEME_SLUG . '-widget-' . $this->widget_id ,
				$this->widget_title,
				$widget_ops,
				$control_ops
			);

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => __( 'about us', 'bride' ),
				'excerpt' => __( 'The key is to enjoy life, because they don’t want you to enjoy life. I promise you, they don’t want you to jetski, they don’t want you to smile. The first of the month is coming, we have to get money, we have no choice. It cost money to eat and they don’t want you to eat. Learning is coo.', 'bride' ),
				'design' => array(
					'layout' => 'layout-Full-Width',
					'gutter' => 'on',
					'background' => array(
						'position' => 'center',
						'repeat' => 'no-repeat'
					),
					'fonts' => array(
						'align' => 'text-center',
						'size' => 'medium',
						'color' => NULL,
						'shadow' => NULL,
						'heading-type' => 'h2',
					)
				),
			);

			/* Setup Widget Repeater Defaults */
			$this->register_repeater_defaults( 'column', 4, array(
				'title' => esc_html__( 'Wedding Party', 'bride' ),
				'single_about_details' => esc_html__( 'Get planning help from the wedding experts.', 'bride' ),
				'enna_fun_fact_counter_amount' => esc_html__( '300', 'bride' ),
				'width' => '3',
				'design' => array(
					'background' => NULL,
					'fonts' => array(
						'align' => 'text-center',
						'size' => 'medium',
						'color' => NULL,
						'shadow' => NULL,
						'heading-type' => 'h3',
					),
				),
			) );
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			global $wp_customize;

			$this->backup_inline_css();

			// Turn $args array into variables.
			extract( $args );

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}

			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			wp_enqueue_script( LAYERS_THEME_SLUG . '-map-api' );

			// Set the background styling
			if( !empty( $instance['design'][ 'background' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $instance['design'][ 'background' ] ) );
			if( !empty( $instance['design']['fonts'][ 'color' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id, 'color', array( 'selectors' => array( '.section-title .heading' , '.section-title div.excerpt', '.single-counter .achievement-title' ) , 'color' => $instance['design']['fonts'][ 'color' ] ) );

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $instance );

			/**
			* Generate the widget container class
			*/
			$widget_container_class = array();
			$widget_container_class[] = 'widget';
			$widget_container_class[] = ( 'on' == $this->check_and_return( $instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
			$widget_container_class[] = $this->check_and_return( $instance , 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
			$widget_container_class[] = $this->get_widget_spacing_class( $instance );

			$widget_container_class = apply_filters( 'layers_content_widget_container_class' , $widget_container_class, $this, $instance );
			$widget_container_class = implode( ' ', $widget_container_class );

			// Custom Anchor
			echo $this->custom_anchor( $instance ); ?>

			<div id="<?php echo esc_attr( $widget_id ); ?>" class="about-area fix <?php echo esc_attr( $widget_container_class ); ?>" <?php $this->selective_refresh_atts( $args ); ?>>

				<?php do_action( 'layers_before_content_widget_inner', $this, $instance ); ?>

			<?php if( 'about1' == $this->check_and_return( $instance, 'about_style' ) ) { ?>				

				
				
				
				<?php if ( NULL !== $this->check_and_return( $instance , 'title' ) || NULL !== $this->check_and_return( $instance , 'excerpt' ) ) { ?>



					<div class="full-width about_one clearfix">
						<?php
						/**
						* Generate the Section Title Classes
						*/
						$section_title_class = array();
						$section_title_class[] = 'section-title clearfix';
						$section_title_class[] = 'bride_title';
						$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'size' );
						$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'align' );
						$section_title_class[] = ( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ? 'invert' : '' );
						$section_title_class = implode( ' ', $section_title_class );
						?>
						<div class="<?php echo $section_title_class; ?>">
						
							<div class="column  span-12">
								<div class="about-banner">
									<?php if( isset( $instance['about_images'] ) ) { 
										$about_images =  wp_get_attachment_url($instance['about_images'], 'about1_image');
									} ?>
									<img src="<?php echo esc_url( $about_images ); ?>" alt="about-us-image">
								</div>
								<div class="about-content">
									<div class="section-title">
										<div class="section-logo">
											<?php if( isset( $instance['about_logo'] ) ) { 
												$about_logo =  wp_get_attachment_url($instance['about_logo'], 'full');
											} ?>
											<img src="<?php echo esc_url( $about_logo ); ?>" alt="">
										</div>
										<?php if( '' != $this->check_and_return( $instance, 'title' ) ) { ?>
										<<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?> class="heading">
											<?php echo $instance['title'] ?>
										</<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?>>
										<?php } ?>
										<?php if( '' != $this->check_and_return( $instance, 'excerpt' ) ) { ?>
											<div class="excerpt"><?php echo layers_the_content( $instance['excerpt'] ); ?></div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>

				<?php } ?>

				<?php if ( ! empty( $instance[ 'columns' ] ) ) {

					$column_ids = explode( ',', $instance[ 'column_ids' ] );

					// Set total width
					$col_no = 0;
					$first_last_class = '';
					$row_width = 0; ?>
					<div class="<?php echo $this->get_widget_layout_class( $instance ); ?> <?php echo $this->check_and_return( $instance , 'design', 'liststyle' ); ?>">
						<div class="grid">
							<?php foreach ( $column_ids as $column_key ) {

								// Make sure we've got a column going on here
								if( !isset( $instance[ 'columns' ][ $column_key ] ) ) continue;

								// Setup Internal Vars.
								$item_instance = $instance[ 'columns' ][ $column_key ];
								$item_id_attr  = "{$widget_id}-tabs-{$column_key}";

								// Mix in new/unset defaults on every instance load (NEW)
								$item_instance = $this->apply_defaults( $item_instance, 'column' );

								// Get the Next Column for use later.
								if( isset( $column_ids[ ($col_no+1) ] ) && isset( $instance[ 'columns' ][ $column_ids[ ($col_no+1) ] ] ) ) {
									$next_item = $instance[ 'columns' ][ $column_ids[ ($col_no+1) ] ];
								}

								// Set the background styling
								if( !empty( $item_instance['design'][ 'background' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id . '-' . $column_key , 'background', array( 'background' => $item_instance['design'][ 'background' ] ) );
								if( !empty( $item_instance['design']['fonts'][ 'color' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id . '-' . $column_key , 'color', array( 'selectors' => array( '.heading a', '.heading' , 'div.excerpt' , 'div.excerpt p' ) , 'color' => $item_instance['design']['fonts'][ 'color' ] ) );
								if( !empty( $item_instance['design']['fonts'][ 'shadow' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id . '-' . $column_key , 'text-shadow', array( 'selectors' => array( '.heading a', '.heading' , 'div.excerpt' , 'div.excerpt p' )  , 'text-shadow' => $item_instance['design']['fonts'][ 'shadow' ] ) );

								// Set column margin & padding
								if ( !empty( $item_instance['design']['advanced']['margin'] ) ) $this->inline_css .= layers_inline_styles( "#{$widget_id}-{$column_key}", 'margin', array( 'margin' => $item_instance['design']['advanced']['margin'] ) );
								if ( !empty( $item_instance['design']['advanced']['padding'] ) ) $this->inline_css .= layers_inline_styles( "#{$widget_id}-{$column_key}", 'padding', array( 'padding' => $item_instance['design']['advanced']['padding'] ) );

								if( !isset( $item_instance[ 'width' ] ) ) $item_instance[ 'width' ] = $this->column_defaults[ 'width' ];

								// Set the button styling
								$button_size = '';
								if ( function_exists( 'layers_pro_apply_widget_button_styling' ) ) {
									$this->inline_css .= layers_pro_apply_widget_button_styling( $this, $item_instance, array( "#{$widget_id}-{$column_key} .button" ) );
									$button_size = $this->check_and_return( $item_instance , 'design' , 'buttons-size' ) ? 'btn-' . $this->check_and_return( $item_instance , 'design' , 'buttons-size' ) : '' ;
								}

								// Add the correct span class
								$span_class = 'span-' . $item_instance[ 'width' ];

								$col_no++;
								$max = 12;
								$initial_width = $row_width;
								$item_width = $item_instance[ 'width' ];
								$next_item_width = ( isset( $next_item[ 'width' ] ) ? $next_item[ 'width' ] : 0 );
								$row_width += $item_width;

								if(  $max == $row_width ){
									$first_last_class = 'last';
									$row_width = 0;
								} elseif(  $max < $next_item_width + $row_width ){
									$first_last_class = 'last';
									$row_width = 0;
								} elseif( 0 == $initial_width ){
									$first_last_class = 'first';
								} else {
									$first_last_class = '';
								}

								// Set Featured Media
								$featureimage = $this->check_and_return( $item_instance , 'design' , 'featuredimage' );
								$featurevideo = $this->check_and_return( $item_instance , 'design' , 'featuredvideo' );

								// Calculate which cut based on ratio.
								if( isset( $item_instance['design'][ 'imageratios' ] ) ){

									// Translate Image Ratio into something usable
									$image_ratio = layers_translate_image_ratios( $item_instance['design'][ 'imageratios' ] );

									if( !isset( $item_instance[ 'width' ] ) ) $item_instance[ 'width' ] = 6;

									if( 4 >= $item_instance['width'] && 'layout-fullwidth' != $this->check_and_return( $instance, 'design', 'layout' ) ) $use_image_ratio = $image_ratio . '-medium';

									else $use_image_ratio = $image_ratio . '-large';

								} else {
									if( 4 > $item_instance['width'] ) $use_image_ratio = 'medium';
									else $use_image_ratio = 'full';
								}

								$media = layers_get_feature_media(
									$featureimage ,
									$use_image_ratio ,
									$featurevideo
								);

								// Set Image Size
								if( isset( $item_instance['design']['featuredimage-size'] ) && 0 != $item_instance['design']['featuredimage-size'] && '' != $item_instance['design']['featuredimage-size'] ) {
									$image_width = $item_instance['design'][ 'featuredimage-size' ].'px';
									$this->inline_css .= layers_inline_styles( "#{$widget_id}-{$column_key} .media-image img { max-width : {$image_width}; }");
								}

								// Get the link array.
								$link_array       = $this->check_and_return_link( $item_instance, 'button' );
								$link_href_attr   = ( $link_array['link'] ) ? 'href="' . esc_url( $link_array['link'] ) . '"' : '';
								$link_target_attr = ( '_blank' == $link_array['target'] ) ? 'target="_blank"' : '';

								/**
								* Set Individual Column CSS
								*/
								$classes = array();
								$classes[] = 'layers-masonry-column';
								$classes[] = $this->id_base . '-' . $column_key;
								$classes[] = $span_class;
								$classes[] = ( 'on' == $this->check_and_return( $item_instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
								$classes[] = ( '' != $first_last_class ? $first_last_class : '' );
								$classes[] = ( 'list-masonry' == $this->check_and_return( $instance, 'design', 'liststyle' ) ? '' : '' );
								$classes[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
								$classes[] = $this->check_and_return( $item_instance, 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
								if( $this->check_and_return( $item_instance, 'design' , 'background', 'image' ) || '' != $this->check_and_return( $item_instance, 'design' , 'background', 'color' ) )
									$classes[] = 'content';
								if( false != $media )
									$classes[] = 'has-image';

								$classes = apply_filters( 'layers_content_widget_item_class', $classes, $this, $item_instance );
								$classes = implode( ' ', $classes ); ?>

								<div id="<?php echo $widget_id; ?>-<?php echo $column_key; ?>" class="<?php echo esc_attr( $classes ); ?>">
									<?php
									/**
									* Set Overlay CSS Classes
									*/
									$column_inner_classes = array();
									$column_inner_classes[] = 'media';
									$column_inner_classes[] = 'About_single';
									if( !$this->check_and_return( $instance, 'design', 'gutter' ) ) {
										$column_inner_classes[] = 'no-push-bottom';
									}
									if( $this->check_and_return( $item_instance, 'design', 'background' , 'color' ) ) {
										if( 'dark' == layers_is_light_or_dark( $this->check_and_return( $item_instance, 'design', 'background' , 'color' ) ) ) {
											$column_inner_classes[] = 'invert';
										}
									} else {
										if( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ) {
											$column_inner_classes[] = 'invert';
										}
									}

									$column_inner_classes[] = $this->check_and_return( $item_instance, 'design', 'imagealign' );
									$column_inner_classes[] = $this->check_and_return( $item_instance, 'design', 'fonts' , 'size' );
									$column_inner_classes = implode( ' ', $column_inner_classes );
									?>

									<div class="single-about ptb-50 plr-40">
										<div class="single-about-icon">
											<?php if( NULL != $media ) { ?>
												<div class="media-image <?php echo ( ( isset( $item_instance['design'][ 'imageratios' ] ) && 'image-round' ==          $item_instance['design'][ 'imageratios' ] ) ? 'image-rounded' : '' ); ?>">
													<?php if ( $link_array['link'] ) { ?>
														<a <?php echo $link_href_attr; ?> <?php echo $link_target_attr; ?>>
													<?php  } ?>
														<?php echo $media; ?>
													<?php if ( $link_array['link'] ) { ?>
														</a>
													<?php  } ?>
												</div>
											<?php } ?>
										</div> 
										<div class="single-about-content text-left">
											<?php if( $this->check_and_return( $item_instance, 'title') ) { ?>
											<<?php echo $this->check_and_return( $item_instance, 'design', 'fonts', 'heading-type' ) ?> class="achievement-title heading">
												<?php echo $item_instance['title']; ?>
											</<?php echo $this->check_and_return( $item_instance, 'design', 'fonts', 'heading-type' ) ?>>
											<?php } ?>
											<?php echo $item_instance['single_about_details']; ?>
										</div>   
									</div>
									
								</div>
							<?php } ?>
						</div><!-- /row -->
					</div>
				<?php } ?>

			<?php }elseif( 'about2' == $this->check_and_return( $instance, 'about_style' ) ) { ?>

				<?php if ( NULL !== $this->check_and_return( $instance , 'title' ) || NULL !== $this->check_and_return( $instance , 'excerpt' ) ) { ?>

					<div class="container about2 clearfix">

                        <div class="grid">
	                    	<?php
							/**
							* Generate the Section Title Classes
							*/
							$section_title_class = array();
							$section_title_class[] = 'section-title clearfix';
							$section_title_class[] = 'bride_title';
							$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'size' );
							$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'align' );
							$section_title_class[] = ( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ? 'invert' : '' );
							$section_title_class = implode( ' ', $section_title_class );
							?>
							<div class="<?php echo $section_title_class; ?>">
	                            <div class="span-12 column">
	                                <div class="about-content-two">
	                                    <div class="section-title">
											<?php if( '' != $this->check_and_return( $instance, 'title' ) ) { ?>
											<<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?> class="heading">
												<?php echo $instance['title'] ?>
											</<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?>>
											<?php } ?>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <?php } ?>
						</div>
                        <div class="grid">
                            <div class="span-7 column">
                                <div class="about-content-two mr-30">
                                    <div class="about-content-img pull-left">
                                        <?php if( isset( $instance['about_images'] ) ) { 
											$about_images =  wp_get_attachment_url($instance['about_images'], 'about2_image');
										} ?>
										<img src="<?php echo esc_url( $about_images ); ?>" alt="about-us-image">
                                    </div>
                                    <div class="about-content-info pull-left text-left mt-50">
									   <div class="about-right-pull">
											<?php if( '' != $this->check_and_return( $instance, 'excerpt' ) ) { ?>
											 <div class="excerpt"><?php echo layers_the_content( $instance['excerpt'] ); ?></div>
										    <?php } ?>
									   </div> 
                                    </div>
                                </div>
                            </div> 
                            <div class="span-5 column">

							       	<?php if ( ! empty( $instance[ 'columns' ] ) ) {

										$column_ids = explode( ',', $instance[ 'column_ids' ] );

										// Set total width
										$col_no = 0;
										$first_last_class = '';
										$row_width = 0; ?>
										<div class="<?php echo $this->get_widget_layout_class( $instance ); ?> <?php echo $this->check_and_return( $instance , 'design', 'liststyle' ); ?>">
											<div class="grid">
											
												<?php foreach ( $column_ids as $column_key ) {

													// Make sure we've got a column going on here
													if( !isset( $instance[ 'columns' ][ $column_key ] ) ) continue;

													// Setup Internal Vars.
													$item_instance = $instance[ 'columns' ][ $column_key ];
													$item_id_attr  = "{$widget_id}-tabs-{$column_key}";

													// Mix in new/unset defaults on every instance load (NEW)
													$item_instance = $this->apply_defaults( $item_instance, 'column' );

													// Get the Next Column for use later.
													if( isset( $column_ids[ ($col_no+1) ] ) && isset( $instance[ 'columns' ][ $column_ids[ ($col_no+1) ] ] ) ) {
														$next_item = $instance[ 'columns' ][ $column_ids[ ($col_no+1) ] ];
													}

													// Set the background styling
													if( !empty( $item_instance['design'][ 'background' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id . '-' . $column_key , 'background', array( 'background' => $item_instance['design'][ 'background' ] ) );
													if( !empty( $item_instance['design']['fonts'][ 'color' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id . '-' . $column_key , 'color', array( 'selectors' => array( '.heading a', '.heading' , 'div.excerpt' , 'div.excerpt p' ) , 'color' => $item_instance['design']['fonts'][ 'color' ] ) );
													if( !empty( $item_instance['design']['fonts'][ 'shadow' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id . '-' . $column_key , 'text-shadow', array( 'selectors' => array( '.heading a', '.heading' , 'div.excerpt' , 'div.excerpt p' )  , 'text-shadow' => $item_instance['design']['fonts'][ 'shadow' ] ) );

													// Set column margin & padding
													if ( !empty( $item_instance['design']['advanced']['margin'] ) ) $this->inline_css .= layers_inline_styles( "#{$widget_id}-{$column_key}", 'margin', array( 'margin' => $item_instance['design']['advanced']['margin'] ) );
													if ( !empty( $item_instance['design']['advanced']['padding'] ) ) $this->inline_css .= layers_inline_styles( "#{$widget_id}-{$column_key}", 'padding', array( 'padding' => $item_instance['design']['advanced']['padding'] ) );

													if( !isset( $item_instance[ 'width' ] ) ) $item_instance[ 'width' ] = $this->column_defaults[ 'width' ];

													// Set the button styling
													$button_size = '';
													if ( function_exists( 'layers_pro_apply_widget_button_styling' ) ) {
														$this->inline_css .= layers_pro_apply_widget_button_styling( $this, $item_instance, array( "#{$widget_id}-{$column_key} .button" ) );
														$button_size = $this->check_and_return( $item_instance , 'design' , 'buttons-size' ) ? 'btn-' . $this->check_and_return( $item_instance , 'design' , 'buttons-size' ) : '' ;
													}

													// Add the correct span class
													$span_class = 'span-' . $item_instance[ 'width' ];

													$col_no++;
													$max = 12;
													$initial_width = $row_width;
													$item_width = $item_instance[ 'width' ];
													$next_item_width = ( isset( $next_item[ 'width' ] ) ? $next_item[ 'width' ] : 0 );
													$row_width += $item_width;

													if(  $max == $row_width ){
														$first_last_class = 'last';
														$row_width = 0;
													} elseif(  $max < $next_item_width + $row_width ){
														$first_last_class = 'last';
														$row_width = 0;
													} elseif( 0 == $initial_width ){
														$first_last_class = 'first';
													} else {
														$first_last_class = '';
													}

													// Set Featured Media
													$featureimage = $this->check_and_return( $item_instance , 'design' , 'featuredimage' );
													$featurevideo = $this->check_and_return( $item_instance , 'design' , 'featuredvideo' );

													// Calculate which cut based on ratio.
													if( isset( $item_instance['design'][ 'imageratios' ] ) ){

														// Translate Image Ratio into something usable
														$image_ratio = layers_translate_image_ratios( $item_instance['design'][ 'imageratios' ] );

														if( !isset( $item_instance[ 'width' ] ) ) $item_instance[ 'width' ] = 6;

														if( 4 >= $item_instance['width'] && 'layout-fullwidth' != $this->check_and_return( $instance, 'design', 'layout' ) ) $use_image_ratio = $image_ratio . '-medium';

														else $use_image_ratio = $image_ratio . '-large';

													} else {
														if( 4 > $item_instance['width'] ) $use_image_ratio = 'medium';
														else $use_image_ratio = 'full';
													}

													$media = layers_get_feature_media(
														$featureimage ,
														$use_image_ratio ,
														$featurevideo
													);

													// Set Image Size
													if( isset( $item_instance['design']['featuredimage-size'] ) && 0 != $item_instance['design']['featuredimage-size'] && '' != $item_instance['design']['featuredimage-size'] ) {
														$image_width = $item_instance['design'][ 'featuredimage-size' ].'px';
														$this->inline_css .= layers_inline_styles( "#{$widget_id}-{$column_key} .media-image img { max-width : {$image_width}; }");
													}

													// Get the link array.
													$link_array       = $this->check_and_return_link( $item_instance, 'button' );
													$link_href_attr   = ( $link_array['link'] ) ? 'href="' . esc_url( $link_array['link'] ) . '"' : '';
													$link_target_attr = ( '_blank' == $link_array['target'] ) ? 'target="_blank"' : '';

													/**
													* Set Individual Column CSS
													*/
													$classes = array();
													$classes[] = 'layers-masonry-column';
													$classes[] = $this->id_base . '-' . $column_key;
													$classes[] = $span_class;
													$classes[] = ( 'on' == $this->check_and_return( $item_instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
													$classes[] = ( '' != $first_last_class ? $first_last_class : '' );
													$classes[] = ( 'list-masonry' == $this->check_and_return( $instance, 'design', 'liststyle' ) ? '' : '' );
													$classes[] = 'column' . ( 'on' != $this->check_and_return( $instance, 'design', 'gutter' ) ? '-flush' : '' );
													$classes[] = $this->check_and_return( $item_instance, 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
													if( $this->check_and_return( $item_instance, 'design' , 'background', 'image' ) || '' != $this->check_and_return( $item_instance, 'design' , 'background', 'color' ) )
														$classes[] = 'content';
													if( false != $media )
														$classes[] = 'has-image';

													$classes = apply_filters( 'layers_content_widget_item_class', $classes, $this, $item_instance );
													$classes = implode( ' ', $classes ); ?>

													<div id="<?php echo $widget_id; ?>-<?php echo $column_key; ?>" class="span-12 column  ">
														<?php
														/**
														* Set Overlay CSS Classes
														*/
														$column_inner_classes = array();
														$column_inner_classes[] = 'media';
														$column_inner_classes[] = 'About_single';
														if( !$this->check_and_return( $instance, 'design', 'gutter' ) ) {
															$column_inner_classes[] = 'no-push-bottom';
														}
														if( $this->check_and_return( $item_instance, 'design', 'background' , 'color' ) ) {
															if( 'dark' == layers_is_light_or_dark( $this->check_and_return( $item_instance, 'design', 'background' , 'color' ) ) ) {
																$column_inner_classes[] = 'invert';
															}
														} else {
															if( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ) {
																$column_inner_classes[] = 'invert';
															}
														}

														$column_inner_classes[] = $this->check_and_return( $item_instance, 'design', 'imagealign' );
														$column_inner_classes[] = $this->check_and_return( $item_instance, 'design', 'fonts' , 'size' );
														$column_inner_classes = implode( ' ', $column_inner_classes );
														?>

														<div class="single-about  ptb-50 plr-40">
															<div class="single-about-icon">
																<?php if( NULL != $media ) { ?>
																	<div class="media-image <?php echo ( ( isset( $item_instance['design'][ 'imageratios' ] ) && 'image-round' ==          $item_instance['design'][ 'imageratios' ] ) ? 'image-rounded' : '' ); ?>">
																		<?php if ( $link_array['link'] ) { ?>
																			<a <?php echo $link_href_attr; ?> <?php echo $link_target_attr; ?>>
																		<?php  } ?>
																			<?php echo $media; ?>
																		<?php if ( $link_array['link'] ) { ?>
																			</a>
																		<?php  } ?>
																	</div>
																<?php } ?>
															</div> 
															<div class="single-about-content text-left">
																<?php if( $this->check_and_return( $item_instance, 'title') ) { ?>
																<<?php echo $this->check_and_return( $item_instance, 'design', 'fonts', 'heading-type' ) ?> class="achievement-title heading">
																	<?php echo $item_instance['title']; ?>
																</<?php echo $this->check_and_return( $item_instance, 'design', 'fonts', 'heading-type' ) ?>>
																<?php } ?>
																<?php echo $item_instance['single_about_details']; ?>
															</div>   
														</div>
														
													</div>
												<?php } ?>
												
												
											</div><!-- /row -->
										</div>
									<?php } ?>
								</div>        
							</div>	
						</div>
						<?php } ?>	


				
			<?php	do_action( 'layers_after_content_widget_inner', $this, $instance );


				// Print the Inline Styles for this Widget
				$this->print_inline_css(); ?>
				<style>

					
					#<?php echo esc_attr( $widget_id ); ?> 	.section-title p,.about-area .about-content .excerpt {
					  color: <?php echo esc_html( $instance['title_content_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .single-about-content h3::after {
					  background: <?php echo esc_html( $instance['single_about_title_bottom_border_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .single-about:hover .single-about-content h3::after {
					  background: <?php echo esc_html( $instance['single_about_title_bottom_border_color_on_hover'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .single-about-content h3 {
					  color: <?php echo esc_html( $instance['single_about_title_color'] ); ?>;
					}					
					#<?php echo esc_attr( $widget_id ); ?> .single-about-content p {
					  color: <?php echo esc_html( $instance['single_about_content_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .single-about:hover {
					  border-color: <?php echo esc_html( $instance['single_about_border_color_on_hover'] ); ?>;
					}					
					
					<?php if( isset( $instance['title_border_bottom_image'] ) ) {  ?>
						#<?php echo esc_attr( $widget_id ); ?> .section-title h2.heading::after {
							<?php if( isset( $instance['title_border_bottom_image'] ) ) { 
								$title_border_bottom_image =  wp_get_attachment_url($instance['title_border_bottom_image'], 'full');
							} ?>
						  background: rgba(0, 0, 0, 0) url("<?php echo esc_url( $title_border_bottom_image ); ?> ") no-repeat scroll 0 0;
						}
					<?php 	} ?>
					
					
					
				</style>



	
			</div>
		<?php 	}

		/**
		*  Widget update
		*/

		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regular HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}

			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			$this->design_bar(
				'side', // CSS Class Name
				array( // Widget Object
					'name' => $this->get_layers_field_name( 'design' ),
					'id' => $this->get_layers_field_id( 'design' ),
					'widget_id' => $this->widget_id,
				),
				$instance, // Widget Values
				apply_filters( 'layers_column_widget_design_bar_components', array( // Components
					'layout',

					'display' => array(
						'icon-css' => 'icon-display',
						'label' => __( 'Display', 'bride' ),
						'elements' => array(
							'about_style' => array(
								'type' => 'select',
								'name' => $this->get_layers_field_name( 'about_style' ) ,
								'id' => $this->get_layers_field_id( 'about_style' ) ,
								'value' => ( isset( $instance['about_style'] ) ) ? $instance['about_style'] : NULL,
								'label' => __( 'Select about Style' , 'bride' ),
								'options' => array(
										'about1' => __( 'About Style 1' , 'bride' ),
										'about2' => __( 'About Style 2' , 'bride' ),
								)
							)						
					
						),
					),

					'colorown' => array(
						'icon-css' => 'icon-settings',
						'label' => esc_html__( 'Color Settings', 'bride' ),
						'elements' => array(	

							'title_content_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'title_content_color' ) ,
								'id' => $this->get_layers_field_id( 'title_content_color' ) ,
								'value' => ( isset( $instance['title_content_color'] ) ) ? $instance['title_content_color'] : NULL,
								'label' => esc_html__( 'Title Content Color' , 'bride' ),
							),		
							'single_about_title_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'single_about_title_color' ) ,
								'id' => $this->get_layers_field_id( 'single_about_title_color' ) ,
								'value' => ( isset( $instance['single_about_title_color'] ) ) ? $instance['single_about_title_color'] : NULL,
								'label' => esc_html__( 'single About Title Color' , 'bride' ),
							),	
							'single_about_title_bottom_border_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'single_about_title_bottom_border_color' ) ,
								'id' => $this->get_layers_field_id( 'single_about_title_bottom_border_color' ) ,
								'value' => ( isset( $instance['single_about_title_bottom_border_color'] ) ) ? $instance['single_about_title_bottom_border_color'] : NULL,
								'label' => esc_html__( 'single About Title Bottom Border Color' , 'bride' ),
							),	
							'single_about_title_bottom_border_color_on_hover' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'single_about_title_bottom_border_color_on_hover' ) ,
								'id' => $this->get_layers_field_id( 'single_about_title_bottom_border_color_on_hover' ) ,
								'value' => ( isset( $instance['single_about_title_bottom_border_color_on_hover'] ) ) ? $instance['single_about_title_bottom_border_color_on_hover'] : NULL,
								'label' => esc_html__( 'single About Title Bottom Border Color On Hover' , 'bride' ),
							),	
							'single_about_content_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'single_about_content_color' ) ,
								'id' => $this->get_layers_field_id( 'single_about_content_color' ) ,
								'value' => ( isset( $instance['single_about_content_color'] ) ) ? $instance['single_about_content_color'] : NULL,
								'label' => esc_html__( 'single About Content Color' , 'bride' ),
							),
							'single_about_border_color_on_hover' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'single_about_border_color_on_hover' ) ,
								'id' => $this->get_layers_field_id( 'single_about_border_color_on_hover' ) ,
								'value' => ( isset( $instance['single_about_border_color_on_hover'] ) ) ? $instance['single_about_border_color_on_hover'] : NULL,
								'label' => esc_html__( 'single About Border Color On Hover' , 'bride' ),
							),
							

						),
						
					),
					'background',
					'advanced',
				), $this, $instance )
			); ?>
			<div class="layers-container-large" id="layers-column-widget-<?php echo $this->number; ?>">

				<?php $this->form_elements()->header( array(
				
					'title' =>'Bride About Widget',
					'icon_class' =>'post'
				) ); ?>


				<section class="layers-accordion-section layers-content">
					<div class="layers-form-item">

						<div class="layers-form-item">

							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'title' ) ,
									'id' => $this->get_layers_field_id( 'title' ) ,
									'placeholder' => __( 'Enter title here' , 'bride' ),
									'value' => ( isset( $instance['title'] ) ) ? $instance['title'] : NULL,
									'class' => 'layers-text layers-large'
								)
							); ?>

							<?php $this->design_bar(
								'top', // CSS Class Name
								array( // Widget Object
									'name' => $this->get_layers_field_name( 'design' ),
									'id' => $this->get_layers_field_id( 'design' ),
									'widget_id' => $this->widget_id,
									'show_trash' => FALSE,
									'inline' => TRUE,
									'align' => 'right',
								),
								$instance, // Widget Values
								apply_filters( 'layers_post_widget_inline_design_bar_components', 
								
								array( // Components
								'fonts' => array(
									'elements' => array(
										'title_border_bottom_image' => array(
											'type' => 'image',
											'name' => $this->get_layers_field_name( 'title_border_bottom_image' ) ,
											'id' => $this->get_layers_field_id( 'title_border_bottom_image' ) ,
											'value' => ( isset( $instance['title_border_bottom_image'] ) ) ? $instance['title_border_bottom_image'] : NULL,
											'label' => __( 'title border bottom image Add Here' , 'bride' )
										),
									),
								  ),
								),
								
								$this, $instance )
							); ?>

						</div>
						
						
						
						<div class="layers-form-item">

							<?php echo $this->form_elements()->input(
								array(
									'type' => 'rte',
									'name' => $this->get_layers_field_name( 'excerpt' ) ,
									'id' => $this->get_layers_field_id( 'excerpt' ) ,
									'placeholder' => __( 'Short Excerpt' , 'bride' ),
									'value' => ( isset( $instance['excerpt'] ) ) ? $instance['excerpt'] : NULL,
									'class' => 'layers-textarea layers-large'
								)
							); ?>

						</div>
						
						<div class="layers-form-item">
							<label>
								<?php _e( 'About Image' , 'bride' ); ?>
							</label>
							<?php echo $this->form_elements()->input(
							 array(
								'type' => 'image',
								'name' => $this->get_layers_field_name( 'about_images' ) ,
								'id' => $this->get_layers_field_id( 'about_images' ) ,
								'value' => ( isset( $instance['about_images'] ) ) ? $instance['about_images'] : NULL,
								'label' => __( 'About Image Add Here' , 'bride' )
							   )
							); ?>

						</div>
						
						<div class="layers-form-item">
							<label>
								<?php _e( 'About Title Top Logo' , 'bride' ); ?>
							</label>
							<?php echo $this->form_elements()->input(
							 array(
								'type' => 'image',
								'name' => $this->get_layers_field_name( 'about_logo' ) ,
								'id' => $this->get_layers_field_id( 'about_logo' ) ,
								'value' => ( isset( $instance['about_logo'] ) ) ? $instance['about_logo'] : NULL,
								'label' => __( 'About Title Top Logo Add Here' , 'bride' )
							   )
							); ?>

						</div>
						
						<?php $this->repeater( 'column', $instance ); ?>

					</div>
				</section>
				

				
				

			</div>

		<?php }

		function column_item( $item_guid, $item_instance ) {

			// Mix in new/unset defaults on every instance load (NEW)
			$item_instance = $this->apply_defaults( $item_instance, 'column' );
			?>
			<li class="layers-accordion-item" data-guid="<?php echo esc_attr( $item_guid ); ?>">
				<a class="layers-accordion-title">
					<span>
						<?php esc_html_e( 'Single About' , 'bride' ); ?><span class="layers-detail"><?php echo ( isset( $item_instance['title'] ) ? ': ' . substr( stripslashes( strip_tags( $item_instance['title'] ) ), 0 , 50 ) : NULL ); ?><?php echo ( isset( $item_instance['title'] ) && strlen( $item_instance['title'] ) > 50 ? '...' : NULL ); ?></span>
					</span>
				</a>
				<section class="layers-accordion-section layers-content">
					<?php $this->design_bar(
						'top', // CSS Class Name
						array( // Widget Object
							'name' => $this->get_layers_field_name( 'design' ),
							'id' => $this->get_layers_field_id( 'design' ),
							'widget_id' => $this->widget_id . '_item',
							'number' => $this->number,
							'show_trash' => TRUE,
						),
						$item_instance, // Widget Values
						apply_filters( 'layers_column_widget_column_design_bar_components', array( // Components
							'background',

							'featuredimage',
							'fonts',
							'width' => array(
								'icon-css' => 'icon-columns',
								'label' => 'Column Width',
								'elements' => array(
									'layout' => array(
										'type' => 'select',
										'label' => esc_html__( '' , 'bride' ),
										'name' => $this->get_layers_field_name( 'width' ),
										'id' => $this->get_layers_field_id( 'width' ),
										'value' => ( isset( $item_instance['width'] ) ) ? $item_instance['width'] : NULL,
										'options' => array(
											'1' => esc_html__( '1 of 12 columns' , 'bride' ),
											'2' => esc_html__( '2 of 12 columns' , 'bride' ),
											'3' => esc_html__( '3 of 12 columns' , 'bride' ),
											'4' => esc_html__( '4 of 12 columns' , 'bride' ),
											'5' => esc_html__( '5 of 12 columns' , 'bride' ),
											'6' => esc_html__( '6 of 12 columns' , 'bride' ),
											'7' => esc_html__( '7 of 12 columns' , 'bride' ),
											'8' => esc_html__( '8 of 12 columns' , 'bride' ),
											'9' => esc_html__( '9 of 12 columns' , 'bride' ),
											'10' => esc_html__( '10 of 12 columns' , 'bride' ),
											'11' => esc_html__( '11 of 12 columns' , 'bride' ),
											'12' => esc_html__( '12 of 12 columns' , 'bride' )
										)
									)
								)
							),

						), $this, $item_instance )
					); ?>
					<div class="layers-row">
						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'title' ); ?>"><?php esc_html_e( 'About Title Add Here' , 'bride' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'title' ),
									'id' => $this->get_layers_field_id( 'title' ),
									'placeholder' => esc_html__( 'Enter About title here' , 'bride' ),
									'value' => ( isset( $item_instance['title'] ) ) ? $item_instance['title'] : NULL ,
									'class' => 'layers-text'
								)
							); ?>
						</p>
						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'single_about_details' ); ?>"><?php esc_html_e( 'About Single Details Add Here' , 'bride' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'rte',
									'name' => $this->get_layers_field_name( 'single_about_details' ),
									'id' => $this->get_layers_field_id( 'single_about_details' ),
									'placeholder' => esc_html__( 'Enter About icon name here' , 'bride' ),
									'value' => ( isset( $item_instance['single_about_details'] ) ) ? $item_instance['single_about_details'] : NULL ,
									'class' => 'layers-text'
								)
							); ?>
						</p>
					</div>
				</section>
			</li>
			<?php
		}

	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("bride_About_Us_Widget");
}

