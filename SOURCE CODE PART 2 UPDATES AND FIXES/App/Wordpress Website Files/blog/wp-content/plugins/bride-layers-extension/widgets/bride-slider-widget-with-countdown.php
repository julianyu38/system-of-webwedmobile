<?php  /**
 * Slider  Widget
 *
 * This file is used to register and display the Layers - Slider widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( class_exists('Layers_Widget') && !class_exists( 'ht_slider_widget_with_counter' ) ) {	
	class ht_slider_widget_with_counter extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function __construct(){

			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = __( 'Bride :: Slider With Timer' , 'bride' );
			$this->widget_id = 'ht_slider_widget_with_counter';
			$this->post_type = 'ht_slider';
			$this->taxonomy = 'ht_slider_category';
			$this->checkboxes = array(
				'slick_color',
				'slick_active_color',
				'show_hide_slider_nav',
			
			); // @TODO: Try make this more dynamic, or leave a different note reminding users to change this if they add/remove checkboxes

			/* Widget settings. */
			$widget_ops = array(

				'classname'   => 'obox-layers-' . $this->widget_id .'-widget',
				'description' => __( 'This widget is used to display your posts in a flexible grid.', 'bride' ),
			);

			/* Widget control settings. */
			$control_ops = array(
				'width'   => LAYERS_WIDGET_WIDTH_SMALL,
				'height'  => NULL,
				'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
			);

			/* Create the widget. */
			parent::__construct(
				LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
				$this->widget_title,
				$widget_ops,
				$control_ops
			);

			/* Setup Widget Defaults */
			$this->defaults = array (		
				'category' => 0,
				'slick_color' => '#fff',
				'slick_active_color' => '#a499c9 ',
				'btn_font_size' => '14',
				'btn_textr_color' => '#ffffff',
				'btn_bg_color_gradient_right_color' => '#a499c9 ',
				'btn_text_hover_bg' => '#fff',
				'btn_hover_border_color' => '#a499c9',
				'btn_text_hover_color' => '#a499c9',
				'arrow_button_opacity' => '0.5',
				'btn_hover_color' => '#f00',
				'show_hide_slider_nav' => '',
				'btn_border_width' => '',
				'btn_border_color' => '#a499c9',
				'btn_border_radius' => '',
				'pading_tb' => '12',
				'pading_lr' => '40',				
				'posts_per_page' => get_option( 'posts_per_page' ),
				'design' => array(
					'layout' => 'layout-fullwidth',
					'textalign' => 'text-center',
				)
			);
		}



		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {		
		
			global $wp_customize;

			$this->backup_inline_css();

			// Turn $args array into variables.
			extract( $args );

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );
			
				// Apply Button Styling.
				$this->inline_css .= layers_inline_button_styles( "#{$widget_id}", 'button', array( 'selectors' => array( '.thumbnail-body .slider-btns a.button.effect-sweep' ) ,'button' => $this->check_and_return( $instance, 'design', 'buttons' ) ) );
			
			// Begin query arguments
			$query_args = array();
			$query_args[ 'post_type' ] = $this->post_type;
			$query_args[ 'posts_per_page' ] = $instance['posts_per_page'];
			if( isset( $instance['order'] ) ) {

				$decode_order = json_decode( $instance['order'], true );

				if( is_array( $decode_order ) ) {
					foreach( $decode_order as $key => $value ){
						$query_args[ $key ] = $value;
					}
				}
			}

			// Do the special taxonomy array()
			if( isset( $instance['category'] ) && '' != $instance['category'] && 0 != $instance['category'] ){

				$query_args['tax_query'] = array(
					array(
						"taxonomy" => $this->taxonomy,
						"field" => "id",
						"terms" => $instance['category']
					)
				);
			} elseif( !isset( $instance['hide_category_filter'] ) ) {
				$terms = get_terms( $this->taxonomy );
			} // if we haven't selected which category to show, let's load the $terms for use in the filter

			// Do the WP_Query
			$post_query = new WP_Query( $query_args );

			/**
			* Generate the widget container class
			*/
			$widget_container_class = array();

			$widget_container_class[] = 'widget';
			$widget_container_class[] = 'layers-post-widget';
			$widget_container_class[] = 'content-vertical-massive';
			$widget_container_class[] = 'hero__slider';
			$widget_container_class[] = 'content-pb0-massive';
			$widget_container_class[] = 'clearfix';
			$widget_container_class[] = ( 'on' == $this->check_and_return( $instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
			$widget_container_class[] = $this->check_and_return( $instance , 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
			$widget_container_class[] = $this->get_widget_spacing_class( $instance );

			$widget_container_class = apply_filters( 'layers_post_widget_container_class' , $widget_container_class, $this, $instance );
			$widget_container_class = implode( ' ', $widget_container_class ); ?>
			<?php echo $this->custom_anchor( $instance ); ?>
			<div id="<?php echo esc_attr( $widget_id ); ?>" class="<?php echo esc_attr( $widget_container_class ); ?>">

				<?php do_action( 'layers_before_post_widget_inner', $this, $instance ); ?>
				
					<?php if( $post_query->have_posts() ) { ?>
						<div class="<?php echo $this->get_widget_layout_class( $instance ); ?>">
					
						<!--Hero Area Start-->
						<div id="hero-area" class="hero-area section">
						   
							<!--Hero Slider-->
							<div class="hero-slider <?php if(  $instance['show_hide_slider_nav'] != '' ) { echo esc_attr ('hidenav'); } ?>">
									<?php while( $post_query->have_posts() ) {
										$post_query->the_post(); ?>

									<?php 
									
									$ht_title_color = get_post_meta(get_the_id(),'_bride_ht_title_color',true);   
									$ht_title_size = get_post_meta(get_the_id(),'_bride_ht_title_size',true);   
									$ht_title_lineh = get_post_meta(get_the_id(),'_bride_ht_title_lineh',true);   
									
									$sub_title = get_post_meta(get_the_id(),'_bride_sub_title',true); 	
									$ht_subtitle_color = get_post_meta(get_the_id(),'_bride_ht_subtitle_color',true);  
									$ht_subtitle_size = get_post_meta(get_the_id(),'_bride_ht_subtitle_size',true);  
									
									$paragraph_text = get_post_meta(get_the_id(),'_bride_paragraph_text',true); 
									$paragraph_color = get_post_meta(get_the_id(),'_bride_paragraph_color',true); 
									$Paragraph_size = get_post_meta(get_the_id(),'_bride_Paragraph_size',true); 
									
									$slide_btn = get_post_meta(get_the_id(),'_bride_slide_btn',true);   
									$slide_url = get_post_meta(get_the_id(),'_bride_slide_url',true);   
									$btn_icon = get_post_meta(get_the_id(),'_bride_btn_icon',true);     
									$section_bg_color = get_post_meta(get_the_id(),'_bride_section_bg_color',true);   
									$section_bg_image = get_post_meta(get_the_id(),'_bride_section_bg_image',true);   
									?>


									<!--Hero Slide Item Start-->
									<?php if($section_bg_image){?>
									
										<div class="hs-item flex <?php if( isset( $instance['show_hide_overlay'] ) ) { echo esc_attr ('overlay'); } ?> " style="background-image:url(<?php echo esc_html($section_bg_image); ?>);">
									
									<?php }else{ ?>
									
										<div class="hs-item  " style="background-color:<?php echo esc_html($section_bg_color); ?>;">
										
									<?php } ?>
									
									
										<!--Hero Slide Content-->
											<div class="hs-item-wrapper clearfix container">
												<div class="grid  <?php echo esc_html($ht_image_align); ?>">

												   <div class="column  span-12">
														<div class="hs-content">										
														  <div class="hs-thumb">		
															<?php	
																if ( has_post_thumbnail() ) {
																	the_post_thumbnail();
																} 
															?>
														  </div>	
															  
															<h1 style="color:<?php echo esc_attr($ht_title_color); ?>;line-height:<?php echo esc_attr($ht_title_lineh); ?>;font-size:<?php echo esc_attr($ht_title_size); ?>"><?php the_title(); ?></h1>
															
															<?php if( $sub_title ){?>
															
																<h2 style="color:<?php echo esc_attr($ht_subtitle_color); ?>;font-size:<?php echo esc_attr($ht_subtitle_size); ?>"><?php echo esc_html($sub_title); ?></h2>
																
															<?php } ?>
															
															<?php if( $paragraph_text ){?>
															
																<p style="color:<?php echo esc_attr($paragraph_color); ?>;font-size:<?php echo esc_attr($Paragraph_size); ?>">
																	<?php echo esc_html($paragraph_text); ?>
																</p>
															
															<?Php } ?>
															
															<?php if( $this->check_and_return( $instance, 'dates' ) ) { ?>
																<div class="timer">
																	<div data-countdown="<?php echo $instance['dates']; ?>" class="timer-grid"></div>
																</div>
															<?php } ?>	
															
															
														   <!-- slider button style -->	
															<div class="add_your_class ">												
																
																<!-- blog button -->
																<div class="slider-btns">
																	 
																	<?php if( $slide_url ){?>
																	
																		<a   class="button-primary button effect-sweep" href="<?php echo esc_url($slide_url); ?>" data-scroll >
																		
																		<?php echo esc_html($slide_btn); ?>
																		
																											
																		<?php if(isset($btn_icon)){?>
																		
																			<i class="fa fa-<?php echo esc_attr( $btn_icon ); ?>"></i>
																			
																		<?php } ?>
																		
																		</a>
																		
																	<?php } ?>

																</div>														
																<!-- end blog button  -->
																
															</div>
															<!-- end pricing bottom section-->		

															
														</div>
													</div>
												
												</div>
										
											</div>
									</div>
									<!--Hero Slide Item End-->								
														
						<?php }; // while have_posts ?>

					</div>
					
				</div>
				<!--Hero Area End-->		
									
			</div>


				<?php }; // if have_posts ?>


				<?php do_action( 'layers_after_post_widget_inner', $this, $instance );
				// Print the Inline Styles for this Widget
				$this->print_inline_css();	?>
				<style type="text/css">				
					#<?php echo esc_attr( $widget_id ); ?> .slick-dots button{
						background:<?php echo esc_html( $instance['slick_color'] ); ?>;
					}
					
					#<?php echo esc_attr( $widget_id ); ?>  .hs-content > h1 span {
					  color: <?php echo esc_html( $instance['ht_title_highlight_colors'] ); ?> 
					}

					#<?php echo esc_attr( $widget_id ); ?>  .hero-slider .slick-dots li.slick-active button,.hero-slider .slick-dots li:hover button {
						background:<?php echo esc_html( $instance['slick_active_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .slick-arrow {
						background: <?php echo esc_html( $instance['slick_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .slider-btns a {
					  background:<?php echo esc_html( $instance['bottom_section_bg'] ); ?>;
					  padding: <?php echo esc_html( $instance['bottom_section_padding'] ); ?>px 0;
					}					
					#<?php echo esc_attr( $widget_id ); ?>  .slider-btns a{
						padding:<?php echo esc_html( $instance['pading_tb'] ); ?>px <?php echo esc_html( $instance['pading_lr'] ); ?>px;
						font-size:<?php echo esc_html( $instance['btn_font_size'] ); ?>px;
						border-width:<?php echo esc_html( $instance['btn_border_width'] ); ?>px;
						border-style:solid;
						border-color:<?php echo esc_html( $instance['btn_border_color'] ); ?>;
						border-radius:<?php echo esc_html( $instance['btn_border_radius'] ); ?>px;
					}
					#<?php echo esc_attr( $widget_id ); ?> .slider-btns a{
						color:<?php echo esc_html( $instance['btn_text_color'] ); ?>;
					    background: <?php echo esc_html( $instance['btn_bg_color_gradient_right_color'] ); ?>;
					}						
					#<?php echo esc_attr( $widget_id ); ?> .slider-btns a:hover{
 						   background: <?php echo esc_html( $instance['btn_text_hover_bg'] ); ?>;
						   border-color: <?php echo esc_html( $instance['btn_hover_border_color'] ); ?>;
						   color: <?php echo esc_html( $instance['btn_text_hover_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> #hero-area .slick-arrow {
					  opacity: <?php echo esc_html( $instance['arrow_button_opacity'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .button-primary.button.effect-sweep {
					  border-color: <?php echo esc_html( $instance['btn_border_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .overlay::before {
					  background: rgba(0, 0, 0, 0) linear-gradient(to right, <?php echo esc_html( $instance['btn_hover_bg_color_gradient_left_color'] ); ?>  0%,
					   <?php echo esc_html( $instance['btn_hover_bg_color_gradient_right_color'] ); ?> 100%) repeat scroll 0 0;
					  opacity: 0.8;
					}
					#<?php echo esc_attr( $widget_id ); ?> .hs-content::before {
					  border: <?php echo esc_html( $instance['slider_border_squer_width'] ); ?>px solid <?php echo esc_html( $instance['slider_border_squer_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .hs-item::after {
					  opacity: <?php echo esc_html( $instance['background_opcity'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .timer-grid > div {
						background: <?php echo esc_html( $instance['timer_bg_color'] ); ?>;
						color: <?php echo esc_html( $instance['timer_text_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .timer .timer-grid span {
						color: <?php echo esc_html( $instance['timer_text_color'] ); ?>;
					}
					
					
					
				</style>							
			</div>

			<?php // Reset WP_Query
			wp_reset_postdata();

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $instance );
		}

		/**
		*  Widget update
		*/

		function update($new_instance, $old_instance) {

			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			$this->design_bar(
				'side', // CSS Class Name
				array( // Widget Object
					'name' => $this->get_layers_field_name( 'design' ),
					'id' => $this->get_layers_field_id( 'design' ),
					'widget_id' => $this->widget_id,
				),
				$instance, // Widget Values
				apply_filters( 'layers_post_widget_design_bar_components' , array( // Components
					'layout',
					'display' => array(
						'icon-css' => 'icon-display',
						'label' => __( 'Display', 'bride' ),
						'elements' => array(
							'background_opcity' => array(
								'type' => 'range',
								'min' => '.0',
								'max' => '1',
								'step' => '.1',
								'default' => '.5',	
								'name' => $this->get_layers_field_name( 'background_opcity' ) ,
								'id' => $this->get_layers_field_id( 'background_opcity' ) ,
								'value' => ( isset( $instance['background_opcity'] ) ) ? $instance['background_opcity'] : NULL,
								'label' => __( 'Background Opcity' , 'bride' ),
							),
							'ht_title_highlight_colors' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'ht_title_highlight_colors' ) ,
								'id' => $this->get_layers_field_id( 'ht_title_highlight_colors' ) ,
								'value' => ( isset( $instance['ht_title_highlight_colors'] ) ) ? $instance['ht_title_highlight_colors'] : NULL,
								'label' => __( 'Main Title Highlight Color' , 'bride' ),
							),	
							'show_hide_slider_nav' => array(
								'type' => 'checkbox',
								'name' => $this->get_layers_field_name( 'show_hide_slider_nav' ) ,
								'id' => $this->get_layers_field_id( 'show_hide_slider_nav' ) ,
								'value' => ( isset( $instance['show_hide_slider_nav'] ) ) ? $instance['show_hide_slider_nav'] : NULL,
								'label' => __( 'Show/Hide Slider Navigation ' , 'bride' )
							),
							'slick_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'slick_color' ) ,
								'id' => $this->get_layers_field_id( 'slick_color' ) ,
								'value' => ( isset( $instance['slick_color'] ) ) ? $instance['slick_color'] : NULL,
								'default' => '#fff',
								'label' => __( 'slider Navigation Color' , 'bride' )
							),
							'slick_active_color' => array(
								'type' => 'color',
								'name' => $this->get_layers_field_name( 'slick_active_color' ) ,
								'id' => $this->get_layers_field_id( 'slick_active_color' ) ,
								'value' => ( isset( $instance['slick_active_color'] ) ) ? $instance['slick_active_color'] : NULL,
								'default' => '#71b100',
								'label' => __( 'slider Navigation Active Color' , 'bride' )
							),
							),
						),
						'colorown' => array(
							'icon-css' => 'icon-call-to-action',
							'label' => __( 'Slider Style', 'bride' ),
							'elements' => array(						
								'btn_font_size' => array(
									'type' => 'range',
									'min' => '0',
									'max' => '100',
									'step' => '1',
									'default' => '0',	
									'name' => $this->get_layers_field_name( 'btn_font_size' ) ,
									'id' => $this->get_layers_field_id( 'btn_font_size' ) ,
									'value' => ( isset( $instance['btn_font_size'] ) ) ? $instance['btn_font_size'] : NULL,
									'label' => __( 'Button Font Size' , 'bride' ),
								),
								'btn_text_color' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'btn_text_color' ) ,
									'id' => $this->get_layers_field_id( 'btn_text_color' ) ,
									'value' => ( isset( $instance['btn_text_color'] ) ) ? $instance['btn_text_color'] : NULL,
									'label' => __( 'Button Text Color' , 'bride' ),
								),							
								'btn_bg_color_color' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'btn_bg_color_gradient_right_color' ) ,
									'id' => $this->get_layers_field_id( 'btn_bg_color_gradient_right_color' ) ,
									'value' => ( isset( $instance['btn_bg_color_gradient_right_color'] ) ) ? $instance['btn_bg_color_gradient_right_color'] : NULL,
									'label' => __( 'Button BG Color' , 'bride' ),
								),								
								'btn_text_hover_bg' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'btn_text_hover_bg' ) ,
									'id' => $this->get_layers_field_id( 'btn_text_hover_bg' ) ,
									'value' => ( isset( $instance['btn_text_hover_bg'] ) ) ? $instance['btn_text_hover_bg'] : NULL,
									'label' => __( 'Button Hover BG' , 'bride' ),
								),						
								'btn_text_hover_color' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'btn_text_hover_color' ) ,
									'id' => $this->get_layers_field_id( 'btn_text_hover_color' ) ,
									'value' => ( isset( $instance['btn_text_hover_color'] ) ) ? $instance['btn_text_hover_color'] : NULL,
									'label' => __( 'Button Text On Hover' , 'bride' ),
								),					
								'btn_hover_border_color' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'btn_hover_border_color' ) ,
									'id' => $this->get_layers_field_id( 'btn_hover_border_color' ) ,
									'value' => ( isset( $instance['btn_hover_border_color'] ) ) ? $instance['btn_hover_border_color'] : NULL,
									'label' => __( 'Button Border Color On Hover' , 'bride' ),
								),	
								'pading_tb' => array(
									'type' => 'range',
									'min' => '0',
									'max' => '250',
									'step' => '1',
									'default' => '0',	
									'name' => $this->get_layers_field_name( 'pading_tb' ) ,
									'id' => $this->get_layers_field_id( 'pading_tb' ) ,
									'value' => ( isset( $instance['pading_tb'] ) ) ? $instance['pading_tb'] : NULL,
									'label' => __( 'Padding Top/Bottom' , 'bride' ),
								),
								'btn_border_radius' => array(
									'type' => 'range',
									'min' => '0',
									'max' => '100',
									'step' => '1',
									'default' => '6',	
									'name' => $this->get_layers_field_name( 'btn_border_radius' ) ,
									'id' => $this->get_layers_field_id( 'btn_border_radius' ) ,
									'value' => ( isset( $instance['btn_border_radius'] ) ) ? $instance['btn_border_radius'] : NULL,
									'label' => __( 'Button Border Radius' , 'bride' ),
								),
								'pading_lr' => array(
									'type' => 'range',
									'min' => '0',
									'max' => '250',
									'step' => '1',
									'default' => '0',	
									'name' => $this->get_layers_field_name( 'pading_lr' ) ,
									'id' => $this->get_layers_field_id( 'pading_lr' ) ,
									'value' => ( isset( $instance['pading_lr'] ) ) ? $instance['pading_lr'] : NULL,
									'label' => __( 'Padding Left/Right' , 'bride' ),
								),
								'timer_bg_color' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'timer_bg_color' ) ,
									'id' => $this->get_layers_field_id( 'timer_bg_color' ) ,
									'value' => ( isset( $instance['timer_bg_color'] ) ) ? $instance['timer_bg_color'] : NULL,
									'label' => __( 'Timer BG Color' , 'bride' ),
								),
								'timer_text_color' => array(
									'type' => 'color',	
									'name' => $this->get_layers_field_name( 'timer_text_color' ) ,
									'id' => $this->get_layers_field_id( 'timer_text_color' ) ,
									'value' => ( isset( $instance['timer_text_color'] ) ) ? $instance['timer_text_color'] : NULL,
									'label' => __( 'Timer Text Color' , 'bride' ),
								),
								
								
								

							),
							
						),										
						'advanced',
					), $this, $instance )
				); ?>
				<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' =>  __( 'Brid Slider With Timer' , 'bride' ),
					'icon_class' =>'post'
				) ); ?>

				<section class="layers-accordion-section layers-content">

					<div class="layers-row layers-push-bottom">
						<div class="layers-form-item">

						<?php // Grab the terms as an array and loop 'em to generate the $options for the input
						$terms = get_terms( $this->taxonomy , array( 'hide_empty' => false ) );
						if( !is_wp_error( $terms ) ) { ?>
							<p class="layers-form-item">
								<label for="<?php echo $this->get_layers_field_id( 'category' ); ?>"><?php echo __( 'Category to Display' , 'bride' ); ?></label>
								<?php $category_options[ 0 ] = __( 'All' , 'bride' );
								foreach ( $terms as $t ) $category_options[ $t->term_id ] = $t->name;
								echo $this->form_elements()->input(
									array(
										'type' => 'select',
										'name' => $this->get_layers_field_name( 'category' ) ,
										'id' => $this->get_layers_field_id( 'category' ) ,
										'placeholder' => __( 'Select a Category' , 'bride' ),
										'value' => ( isset( $instance['category'] ) ) ? $instance['category'] : NULL,
										'options' => $category_options,
									)
								); ?>
							</p>
						<?php } // if !is_wp_error ?>
						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'posts_per_page' ); ?>"><?php echo __( 'Number of items to show' , 'bride' ); ?></label>
							<?php $select_options[ '-1' ] = __( 'Show All' , 'bride' );
							$select_options = $this->form_elements()->get_incremental_options( $select_options , 1 , 20 , 1);
							echo $this->form_elements()->input(
								array(
									'type' => 'number',
									'name' => $this->get_layers_field_name( 'posts_per_page' ) ,
									'id' => $this->get_layers_field_id( 'posts_per_page' ) ,
									'value' => ( isset( $instance['posts_per_page'] ) ) ? $instance['posts_per_page'] : NULL,
									'min' => '-1',
									'max' => '100'
								)
							); ?>
						</p>

						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'order' ); ?>"><?php echo __( 'Sort by' , 'bride' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'select',
									'name' => $this->get_layers_field_name( 'order' ) ,
									'id' => $this->get_layers_field_id( 'order' ) ,
									'value' => ( isset( $instance['order'] ) ) ? $instance['order'] : NULL,
									'options' => $this->form_elements()->get_sort_options()
								)
							); ?>
						</p>
						
						
						<p class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'dates' ); ?>"><?php echo __( 'Set Your Countdown Date' , 'bride' ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'dates' ) ,
									'id' => $this->get_layers_field_id( 'dates' ) ,
									'placeholder' =>  __( 'Enter date ex:"2019/07/01"' , 'dates' ),
									'value' => ( isset( $instance['dates'] ) ) ? $instance['dates'] : NULL ,
									'class' => 'layers-text'
								)
							); ?>
						</p>
						
					</div>
				</section>
			</div>
		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("ht_slider_widget_with_counter");
}