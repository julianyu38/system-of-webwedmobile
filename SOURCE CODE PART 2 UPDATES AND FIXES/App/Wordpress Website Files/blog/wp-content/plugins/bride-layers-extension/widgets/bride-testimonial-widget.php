<?php  /**
 * Testimonial Widget
 *
 * This file is used to register and display the Layers - Team widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( class_exists('Layers_Widget') && !class_exists( 'Bride_Testimonial_Widget' ) ) {
	class Bride_Testimonial_Widget extends Layers_Widget {

		/**
		*  Widget construction
		*/
		function __construct() {

			/**
			* Widget variables
			*
			* @param  	string    		$widget_title    	Widget title
			* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
			* @param  	string    		$post_type    		(optional) Post type for use in widget options
			* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
			* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
			*/
			$this->widget_title = __( 'Bride :: Testimonial Widget' , bride );
			$this->widget_id = 'bride_testimonials';
			$this->post_type = 'bride_all_say';
			$this->taxonomy = '';
			$this->checkboxes = array(

					'show_titles'
				); // @TODO: Try make this more dynamic, or leave a different note reminding users to change this if they add/remove checkboxes

			/* Widget settings. */
			$widget_ops = array(
				'classname' => 'obox-layers-' . $this->widget_id .'-widget',
				'description' => __( 'This widget is used to display your  Service post.', bride ),
				'customize_selective_refresh' => TRUE,
			);

			/* Widget control settings. */
			$control_ops = array(
				'width' => LAYERS_WIDGET_WIDTH_SMALL,
				'height' => NULL,
				'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
			);

			/* Create the widget. */
			parent::__construct(
				LAYERS_THEME_SLUG . '-widget-' . $this->widget_id,
				$this->widget_title,
				$widget_ops,
				$control_ops
			);

			/* Setup Widget Defaults */
			$this->defaults = array (
				'title' => __( 'Clients', bride ),
				'title_step2' => __( 'happy', bride ),
				'excerpt' => __( '', bride ),
				'category' => 0,
				'show_media' => '',
				'show_titles' => 'on',
				'show_pagination' => 'on',
				'show_hide_slide_dot' => 'off',
				'posts_per_page' => get_option( 'posts_per_page' ),
				'design' => array(
					'layout' => 'layout-Full-Width',
					'textalign' => 'text-left',
					'columns' => '3',
					'gutter' => 'on',
					'background' => array(
						'position' => 'center',
						'repeat' => 'no-repeat',
						'stretch' => 'on'
					),
					'fonts' => array(
						'align' => 'text-center',
						'size' => 'medium',
						'color' => NULL,
						'shadow' => NULL,
						'heading-type' => 'h3',
					),
				),
				'section_icone_color' =>'#fff',
				'section_icone_border_color' =>'#fff',
			);
		}

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			global $wp_customize;

			$this->backup_inline_css();

			// Turn $args array into variables.
			extract( $args );

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}

			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			// Apply Styling
			$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $instance['design'][ 'background' ] ) );
			$this->inline_css .= layers_inline_styles( '#' . $widget_id, 'color', array( 'selectors' => array( '.section-title .heading' , '.section-title div.excerpt' ) , 'color' => $instance['design']['fonts'][ 'color' ] ) );
			

			$query_args[ 'post_type' ] = $this->post_type;
			$query_args[ 'posts_per_page' ] = $instance['posts_per_page'];
			if( isset( $instance['order'] ) ) {

				$decode_order = json_decode( $instance['order'], true );

				if( is_array( $decode_order ) ) {
					foreach( $decode_order as $key => $value ){
						$query_args[ $key ] = $value;
					}
				}
			}

			// Do the special taxonomy array()
			if( isset( $instance['category'] ) && '' != $instance['category'] && 0 != $instance['category'] ){

				$query_args['tax_query'] = array(
					array(
						"taxonomy" => $this->taxonomy,
						"field" => "id",
						"terms" => $instance['category']
					)
				);
			} elseif( !isset( $instance['hide_category_filter'] ) ) {
				$terms = get_terms( $this->taxonomy );
			} // if we haven't selected which category to show, let's load the $terms for use in the filter

			// Do the WP_Query
			$post_query = new WP_Query( $query_args );

			// Apply the advanced widget styling
			$this->apply_widget_advanced_styling( $widget_id, $instance );

			/**
			* Generate the widget container class
			*/
			$widget_container_class = array();

			$widget_container_class[] = 'widget';
			$widget_container_class[] = 'bride-testimonials-widget';
			$widget_container_class[] = 'content-vertical-massive';
			$widget_container_class[] = 'clearfix';
			$widget_container_class[] = ( 'on' == $this->check_and_return( $instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
			$widget_container_class[] = $this->check_and_return( $instance , 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
			$widget_container_class[] = $this->get_widget_spacing_class( $instance );

			$widget_container_class = apply_filters( 'layers_post_widget_container_class' , $widget_container_class, $this, $instance );
			$widget_container_class = implode( ' ', $widget_container_class );

			// Custom Anchor
			echo $this->custom_anchor( $instance ); ?>

			<div id="<?php echo esc_attr( $widget_id ); ?>" class="<?php echo esc_attr( $widget_container_class ); ?>" <?php $this->selective_refresh_atts( $args ); ?>>

				<?php do_action( 'layers_before_post_widget_inner', $this, $instance ); ?>

				
				<?php if( $post_query->have_posts() ) { ?>
					<div class="<?php echo $this->get_widget_layout_class( $instance ); ?>">
						<div class="grid">

	                        <div class="testimonial-left pull-left ptb-50">
							
		                            <h2 class="top-to-bottom"><?php echo $instance['title'] ?></h2>
		                            <h1 class="happy"><?php echo $instance['title_step2'] ?></h1>
							
	                        </div>

	                        <div class="testimonial-right pull-right">
	                            <div class="testimonial-image-slider text-center">
									<?php while( $post_query->have_posts() ) {
									$post_query->the_post();?>
		                                <div class="sin-testiImage">
                                		
										<?php if ( has_post_thumbnail() ) { ?>
		                                    <div class="sin-opacity">
		                                       <?php 
												if ( has_post_thumbnail() ) {
													the_post_thumbnail();
												} ?>
		                                    </div> 
		                                    <?php } ?>
		                                </div>

									<?php } ?>
	                            </div>

	                            <div class="testimonial-text-slider text-center ptb-118">
									<?php while( $post_query->have_posts() ) {
									$post_query->the_post();?>
		                                <div class="sin-testiText">
		                                    <h2><?php the_title();?></h2>
		                                    <?php 
												$designation = get_post_meta( get_the_ID(),'_bride_bride_designation', true ); 
											 ?>
		                                    <h6><?php echo esc_html($designation); ?></h6>
		                                    <?php the_content();?>

											<?php
											 $social__icon = get_post_meta( get_the_ID(),'_bride_testimonial_social_list', true );

 											 $testimonial_social_list  = get_post_meta( get_the_ID(),'_bride_testimonial_social_list', true );

											 ?>
												<ul class="testimonial-social-icon pt-30">

													<?php foreach( (array) $testimonial_social_list as $ticonskey => $ticons ){
														$ticons1 = $ticons2 ='';
														if ( isset( $ticons['_bride_s_icon_link'] ) ) {
															$ticons1 =  $ticons['_bride_s_icon_link'];	
														}
														if ( isset( $ticons['_bride_s_icon_name'] ) ) {
															$ticons2 =  $ticons['_bride_s_icon_name'];	
														}?>	

														<li>
															<a target="_blank" href="<?php echo esc_url( $ticons1 );?>" tabindex="0">
																<i class="fa fa-<?php echo $ticons2;?>"></i>
															</a>
														</li>
													
													<?php } ?>	
													
												</ul>
		                                </div>
									<?php } ?>
	                            </div>  
	                        </div>
						</div>							
					</div>
				<?php }; // if have_posts ?>

				<?php do_action( 'layers_after_post_widget_inner', $this, $instance );

				// Print the Inline Styles for this Widget
				$this->print_inline_css();?>
					<style type="text/css">
						
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .testimonial-left h2,#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .testimonial-left h1.happy {
						  color: <?php echo esc_html( $instance['testimonial_area_title_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .testimonial-left h1.happy::after {
						  color: <?php echo esc_html( $instance['title_quote_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .sin-testiText > h2 {
						  color: <?php echo esc_html( $instance['testimonial_author_name_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .sin-testiText > h6 {
						  color: <?php echo esc_html( $instance['testimonial_author_designation_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .sin-testiText > p {
						  color: <?php echo esc_html( $instance['testimonial_content_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget ul.testimonial-social-icon li i {
						  color: <?php echo esc_html( $instance['testimonial_social_icon_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget ul.testimonial-social-icon li:hover i {
						  color: <?php echo esc_html( $instance['testimonial_social_icon_color_on_hover'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .testimonial-left {
						  background: <?php echo esc_html( $instance['testimonial_area_left_bg_color'] ); ?>;
						}
						#<?php echo esc_attr( $widget_id ); ?>.bride-testimonials-widget .testimonial-right {
						  background: <?php echo esc_html( $instance['testimonial_area_right_bg_color'] ); ?>;
						}
					</style>				

			</div>

			<?php // Reset WP_Query
			wp_reset_postdata();

		}

		/**
		*  Widget update
		*/

		function update($new_instance, $old_instance) {

			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}

			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			$this->design_bar(
				'side', // CSS Class Name
				array( // Widget Object
					'name' => $this->get_layers_field_name( 'design' ),
					'id' => $this->get_layers_field_id( 'design' ),
					'widget_id' => $this->widget_id,
				),
				$instance, // Widget Values
				apply_filters( 'layers_post_widget_design_bar_components' , array( // Components
					'layout',
					'colorown' => array(
						'icon-css' => 'icon-call-to-action',
						'label' => __( 'Color Style', 'bride' ),
						'elements' => array(
						
							'testimonial_area_title_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_area_title_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_area_title_color' ) ,
								'value' => ( isset( $instance['testimonial_area_title_color'] ) ) ? $instance['testimonial_area_title_color'] : NULL,
								'label' => __( 'Testimonial Area Title Color' , 'bride' ),
							),	
						
							'title_quote_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'title_quote_color' ) ,
								'id' => $this->get_layers_field_id( 'title_quote_color' ) ,
								'value' => ( isset( $instance['title_quote_color'] ) ) ? $instance['title_quote_color'] : NULL,
								'label' => __( 'Testimonial Quote Color' , 'bride' ),
							),	
							'testimonial_author_name_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_author_name_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_author_name_color' ) ,
								'value' => ( isset( $instance['testimonial_author_name_color'] ) ) ? $instance['testimonial_author_name_color'] : NULL,
								'label' => __( 'Testimonial Author Name Color' , 'bride' ),
							),	
							'testimonial_author_designation_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_author_designation_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_author_designation_color' ) ,
								'value' => ( isset( $instance['testimonial_author_designation_color'] ) ) ? $instance['testimonial_author_designation_color'] : NULL,
								'label' => __( 'Testimonial Author Designation Color' , 'bride' ),
							),
							'testimonial_content_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_content_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_content_color' ) ,
								'value' => ( isset( $instance['testimonial_content_color'] ) ) ? $instance['testimonial_content_color'] : NULL,
								'label' => __( 'Testimonial Content Color' , 'bride' ),
							),
							'testimonial_social_icon_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_social_icon_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_social_icon_color' ) ,
								'value' => ( isset( $instance['testimonial_social_icon_color'] ) ) ? $instance['testimonial_social_icon_color'] : NULL,
								'label' => __( 'Testimonial Social Icon Color' , 'bride' ),
							),	
							'testimonial_social_icon_color_on_hover' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_social_icon_color_on_hover' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_social_icon_color_on_hover' ) ,
								'value' => ( isset( $instance['testimonial_social_icon_color_on_hover'] ) ) ? $instance['testimonial_social_icon_color_on_hover'] : NULL,
								'label' => __( 'Testimonial Social Icon Color On Hover' , 'bride' ),
							),	
							'testimonial_area_left_bg_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_area_left_bg_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_area_left_bg_color' ) ,
								'value' => ( isset( $instance['testimonial_area_left_bg_color'] ) ) ? $instance['testimonial_area_left_bg_color'] : NULL,
								'label' => __( 'Testimonial Area Left BG Color' , 'bride' ),
							),	
							'testimonial_area_right_bg_color' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'testimonial_area_right_bg_color' ) ,
								'id' => $this->get_layers_field_id( 'testimonial_area_right_bg_color' ) ,
								'value' => ( isset( $instance['testimonial_area_right_bg_color'] ) ) ? $instance['testimonial_area_right_bg_color'] : NULL,
								'label' => __( 'Testimonial Area Right BG Color' , 'bride' ),
							),	
						),
					),					
					'columns',
					'background',
					'advanced',
				), $this, $instance )
			); ?>
			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' =>  __( 'Bride Testimonial Widget' , bride ),
					'icon_class' =>'post'
				) ); ?>

				<section class="layers-accordion-section layers-content">

					<div class="layers-row layers-push-bottom">
						<div class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'category' ); ?>"><?php echo __( ' 1st step title add here' , bride ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'title_step2' ) ,
									'id' => $this->get_layers_field_id( 'title_step2' ) ,
									'placeholder' => __( 'Enter 2nd step title here' , bride ),
									'value' => ( isset( $instance['title_step2'] ) ) ? $instance['title_step2'] : NULL,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</div>

						<div class="layers-form-item">
							<label for="<?php echo $this->get_layers_field_id( 'category' ); ?>"><?php echo __( ' 2nd step title add here' , bride ); ?></label>
							<?php echo $this->form_elements()->input(
								array(
									'type' => 'text',
									'name' => $this->get_layers_field_name( 'title' ) ,
									'id' => $this->get_layers_field_id( 'title' ) ,
									'placeholder' => __( 'Enter 1st step title here' , bride ),
									'value' => ( isset( $instance['title'] ) ) ? $instance['title'] : NULL,
									'class' => 'layers-text layers-large'
								)
							); ?>
						</div>
						<?php // Grab the terms as an array and loop 'em to generate the $options for the input
						$terms = get_terms( $this->taxonomy , array( 'hide_empty' => false ) );
						if( !is_wp_error( $terms ) ) { ?>
							<p class="layers-form-item">
								<label for="<?php echo $this->get_layers_field_id( 'category' ); ?>"><?php echo __( ' Select Category' , bride ); ?></label>
								<?php $category_options[ 0 ] = __( 'All' , bride );
								foreach ( $terms as $t ) $category_options[ $t->term_id ] = $t->name;
								echo $this->form_elements()->input(
									array(
										'type' => 'select',
										'name' => $this->get_layers_field_name( 'category' ) ,
										'id' => $this->get_layers_field_id( 'category' ) ,
										'placeholder' => __( 'Select a Category' , bride ),
										'value' => ( isset( $instance['category'] ) ) ? $instance['category'] : NULL,
										'options' => $category_options,
									)
								); ?>
							</p>
						<?php } // if !is_wp_error ?>
					</div>
				</section>

			</div>
		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	 register_widget("Bride_Testimonial_Widget");
}