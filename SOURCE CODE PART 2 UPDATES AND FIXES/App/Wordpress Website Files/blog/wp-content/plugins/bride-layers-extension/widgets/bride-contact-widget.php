<?php  /**
 * Contact Us
 *
 * This file is used to register and display the Layers - Contact widget.
 *
 * @package Layers
 * @since Layers 1.0.0
 */
if( class_exists('Layers_Widget') && !class_exists( 'Bride_Contact_Widget' ) ) { 
	class Bride_Contact_Widget extends Layers_Widget {
		/**
		*  Widget construction
		*/
		function __construct(){

			/**
			* Widget variables
			*
		 	* @param  	string    		$widget_title    	Widget title
		 	* @param  	string    		$widget_id    		Widget slug for use as an ID/classname
		 	* @param  	string    		$post_type    		(optional) Post type for use in widget options
		 	* @param  	string    		$taxonomy    		(optional) Taxonomy slug for use as an ID/classname
		 	* @param  	array 			$checkboxes    	(optional) Array of checkbox names to be saved in this widget. Don't forget these please!
		 	*/
		 	$this->widget_title = __( 'Bride :: Contact Widget' , 'bride' );
			$this->widget_id = 'contact_widget_id';
		 	$this->post_type = '';
		 	$this->taxonomy = '';
		 	$this->checkboxes = array();

		 	/* Widget settings. */
		 	$widget_ops = array(
		 		'classname'   => 'obox-layers-' . $this->widget_id .'-widget',
		 		'description' => __( 'This widget is used to display your ', 'bride' ) . $this->widget_title . '.',
		 		);

		 	/* Widget control settings. */
		 	$control_ops = array( 'width' => LAYERS_WIDGET_WIDTH_SMALL,
		 		'height'  => NULL,
		 		'id_base' => LAYERS_THEME_SLUG . '-widget-' . $this->widget_id
		 		);

		 	/* Create the widget. */
		 	parent::__construct( LAYERS_THEME_SLUG . '-widget-' . $this->widget_id ,
		 		$this->widget_title,
		 		$widget_ops,
		 		$control_ops
		 		);

		 	/* Setup Widget Defaults */
		 	$this->defaults = array (
		 		'title' => __( 'Get in touch', 'bride' ),
				'excerpt' => __( 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi.', 'bride' ),
		 		'contact_form' => NULL,
		 		'icon1' => 'icofont-phone',
		 		'icon2' => 'icofont-envelope',
		 		'icon3' => 'icofont-location-pin',
		 		'addresss' => 'call us',
		 		'addresss2' => 'your message',
		 		'addresss3' => 'our location',
		 		'addresssline2' => '+ 1 432 789 5647 <br> + 1 432 789 5673',
		 		'addresss2line2' => 'info@alfresco.com <br> admin@alfresco.com',
		 		'addresss3line2' => '3756 Melrose place,<br> CA 87031, Australia',
		 		'design' => array(
		 			'layout' => 'layout-boxed',
		 			'background' => array(
		 				'position' => 'center',
		 				'repeat' => 'no-repeat'
		 				),
		 			'fonts' => array(
		 				'align' => 'text-center',
		 				'size' => 'medium',
		 				'color' => NULL,
		 				'shadow' => NULL,
		 				'heading-type' => 'h2',
		 				)
		 			)
		 		);
		 }

		/**
		*  Widget front end display
		*/
		function widget( $args, $instance ) {
			global $wp_customize;


			$this->backup_inline_css();

			// Turn $args array into variables.
			extract( $args );

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );

			// Set the background styling
			if( !empty( $instance['design'][ 'background' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id, 'background', array( 'background' => $instance['design'][ 'background' ] ) );
			if( !empty( $instance['design']['fonts'][ 'color' ] ) ) $this->inline_css .= layers_inline_styles( '#' . $widget_id, 'color', array( 'selectors' => array( '.section-title .heading' , '.section-title div.excerpt' ) , 'color' => $instance['design']['fonts'][ 'color' ] ) );
			
			
			// contact us
			if( isset( $instance['addresss'] ) && ( '' != $instance['addresss'] ) || isset( $instance['addresssline2'] ) && ( '' != $instance['addresssline2'] ) || isset( $instance['addresss2'] ) && ( '' != $instance['addresss2'] ) || isset( $instance['addresss2line2'] ) && ( '' != $instance['addresss2line2'] ) || isset( $instance['addresss3'] ) && ( '' != $instance['addresss3'] ) || isset( $instance['addresss3line2'] ) && ( '' != $instance['addresss3line2'] ) || isset( $instance['icon1'] ) && ( '' != $instance['icon1'] ) || isset( $instance['icon2'] ) && ( '' != $instance['icon2'] )  || isset( $instance['icon3'] ) && ( '' != $instance['icon3'] )         ) {
				$addresss = true;
			}



			// contact us check
			if(isset( $addresss ) ) {
				$form_classs = 'span-6';
			} else {
				$form_classs = 'span-12';
			}
			$full_contact = 'span-12';


			/**
			* Generate the widget container class
			*/
			$widget_container_class = array();
			$widget_container_class[] = 'widget';
			$widget_container_class[] = 'clearfix';
			$widget_container_class[] = 'contact-area';
			$widget_container_class[] = 'content-vertical-massive';
			$widget_container_class[] = 'bg-gray';
			$widget_container_class[] = ( 'on' == $this->check_and_return( $instance , 'design', 'background', 'darken' ) ? 'darken' : '' );
			$widget_container_class[] = $this->check_and_return( $instance , 'design', 'advanced', 'customclass' ); // Apply custom class from design-bar's advanced control.
			$widget_container_class[] = $this->get_widget_spacing_class( $instance );


			$widget_container_class = apply_filters( 'layers_contact_widget_container_class' , $widget_container_class, $this, $instance );
			$widget_container_class = implode( ' ', $widget_container_class ); ?>

			<?php echo $this->custom_anchor( $instance ); ?>
			<div id="<?php echo esc_attr( $widget_id ); ?>" class="<?php echo esc_attr( $widget_container_class ); ?>">

				<?php do_action( 'layers_before_contact_widget_inner', $this, $instance ); ?>

				<div class="<?php echo $this->get_widget_layout_class( $instance ); ?>">
					<!-- Section title start -->
					<div class="container clearfix">
						<?php /**
						* Generate the Section Title Classes
						*/
						$section_title_class = array();
						$section_title_class[] = 'section-title clearfix';
						$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'size' );
						$section_title_class[] = $this->check_and_return( $instance , 'design', 'fonts', 'align' );
						$section_title_class[] = ( $this->check_and_return( $instance, 'design', 'background' , 'color' ) && 'dark' == layers_is_light_or_dark( $this->check_and_return( $instance, 'design', 'background' , 'color' ) ) ? 'invert' : '' );
						$section_title_class = implode( ' ', $section_title_class ); ?>
						<div class="<?php echo $section_title_class; ?>">
							<?php if( '' != $this->check_and_return( $instance, 'title' )  ) { ?>
								<<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?> class="heading">
									<?php echo $instance['title'] ?>
								</<?php echo $this->check_and_return( $instance, 'design', 'fonts', 'heading-type' ); ?>>
							<?php } ?>
							<?php if( '' != $this->check_and_return( $instance, 'excerpt' )  ) { ?>
								<div class="excerpt"><?php echo layers_the_content( $instance['excerpt'] ); ?></div>
							<?php } ?>
						</div>
					</div>
					<!-- Section title end -->
					<div class="grid">
						<?php $full_contact = 'span-12'; ?>
						<?php if( isset( $addresss ) ) { ?>			
							<div class="contact-form form text-left column span-12">	
								<?php if( $this->check_and_return( $instance, 'contact_form' ) ) { ?>
									<?php echo do_shortcode( $instance['contact_form'] ); ?>
								<?php } ?>				
							</div>	
						<?php } else { ?>							
							<div class="contact-form full-width form text-left column <?php echo $full_contact;?> ">	
							<?php if( $this->check_and_return( $instance, 'contact_form' ) ) { ?>
								<?php echo do_shortcode( $instance['contact_form'] ); ?>
								<?php } ?>				
							</div>	
						<?php } ?>
					</div>
				</div>
				
				<?php do_action( 'layers_after_contact_widget_inner', $this, $instance );

				// Print the Inline Styles for this Widget
				$this->print_inline_css(); ?>
				
				<style type="text/css">
					#<?php echo esc_attr( $widget_id ); ?> .section-title h2::before, .section-title h3::before{
					  background: <?php echo esc_html( $instance['title_border_bottom_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .section-title h2::after, .section-title h3::after {
					  color: <?php echo esc_html( $instance['title_border_bottom_color'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .contact-form input[type="submit"] {
					  color: <?php echo esc_html( $instance['button_bg'] ); ?>;
					}
					#<?php echo esc_attr( $widget_id ); ?> .contact-form input[type="submit"]:hover{
					  color: <?php echo esc_html( $instance['button_bg_hover'] ); ?>;
					}
					
				</style>
				
				
				
				
				
				
				
			</div>
		<?php
		// Apply the advanced widget styling
		$this->apply_widget_advanced_styling( $widget_id, $instance );
	}

		/**
		*  Widget update
		*/
		function update($new_instance, $old_instance) {
			if ( isset( $this->checkboxes ) ) {
				foreach( $this->checkboxes as $cb ) {
					if( isset( $old_instance[ $cb ] ) ) {
						$old_instance[ $cb ] = strip_tags( $new_instance[ $cb ] );
					}
				} // foreach checkboxes
			} // if checkboxes
			return $new_instance;
		}

		/**
		*  Widget form
		*
		* We use regulage HTML here, it makes reading the widget much easier than if we used just php to echo all the HTML out.
		*
		*/
		function form( $instance ){

			// Use defaults if $instance is empty.
			if( empty( $instance ) && ! empty( $this->defaults ) ) {
				$instance = wp_parse_args( $instance, $this->defaults );
			}
			
			// Mix in new/unset defaults on every instance load (NEW)
			$instance = $this->apply_defaults( $instance );
			
			$this->design_bar(
				'side', // CSS Class Name
				array( // Widget Object
					'name' => $this->get_layers_field_name( 'design' ),
					'id' => $this->get_layers_field_id( 'design' ),
					'widget_id' => $this->widget_id,
					),
				$instance, // Widget Values
				apply_filters( 'layers_map_widget_design_bar_components' , array( // Components
					'layout',
					'colorown' => array(
						'icon-css' => 'icon-call-to-action',
						'label' => __( 'Color Style', 'bride' ),
						'elements' => array(
												
							'button_bg' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'button_bg' ) ,
								'id' => $this->get_layers_field_id( 'button_bg' ) ,
								'value' => ( isset( $instance['button_bg'] ) ) ? $instance['button_bg'] : NULL,
								'label' => __( 'Button Bg Color' , 'bride' ),
							),
							'button_bg_hover' => array(
								'type' => 'color',	
								'name' => $this->get_layers_field_name( 'button_bg_hover' ) ,
								'id' => $this->get_layers_field_id( 'button_bg_hover' ) ,
								'value' => ( isset( $instance['button_bg_hover'] ) ) ? $instance['button_bg_hover'] : NULL,
								'label' => __( 'Button Bg Hover Color' , 'bride' ),
							),
							
							
							
						),
						
					),
					'background',
					'advanced'
					) )
				); ?>

			<div class="layers-container-large">

				<?php $this->form_elements()->header( array(
					'title' => __( 'bride Contact Widget' , 'bride' ),
					'icon_class' =>'location'
					) ); ?>

				<section class="layers-accordion-section layers-content">
					<div class="layers-row layers-push-bottom clearfix">
						<div class="layers-form-item">
						<?php echo $this->form_elements()->input(
							array(
								'type' => 'text',
								'name' => $this->get_layers_field_name( 'title' ) ,
								'id' => $this->get_layers_field_id( 'title' ) ,
								'placeholder' => __( 'Enter title here' , 'bride' ),
								'value' => ( isset( $instance['title'] ) ) ? $instance['title'] : NULL ,
								'class' => 'layers-text layers-large'
								)
							); ?>

						<?php $this->design_bar(
							'top', // CSS Class Name
							array( // Widget Object
								'name' => $this->get_layers_field_name( 'design' ),
								'id' => $this->get_layers_field_id( 'design' ),
								'widget_id' => $this->widget_id,
								'show_trash' => FALSE,
								'inline' => TRUE,
								'align' => 'right',
								),
							$instance, // Widget Values
							apply_filters( 'layers_map_widget_inline_design_bar_components',


								array( // Components
								'fonts' => array(
									'elements' => array(
										'title_border_bottom_color' => array(
											'type' => 'color',
											'label' => __( 'Section Title border bottom color' , 'bride' ),
											'name' => $this->get_layers_field_name( 'title_border_bottom_color' ),
											'id' => $this->get_layers_field_id(title_border_bottom_color),
											'value' => ( isset( $item_instance['title_border_bottom_color'] ) ) ? $item_instance['title_border_bottom_color'] : NULL,
										),
									),
								  ),
								), 
								
								
								$this, $instance )
							); ?>
						</div>
						<div class="layers-form-item">
							<?php echo $this->form_elements()->input(
							array(
								'type' => 'rte',
								'name' => $this->get_layers_field_name( 'excerpt' ) ,
								'id' => $this->get_layers_field_id( 'excerpt' ) ,
								'placeholder' =>  __( 'Short Excerpt' , 'bride' ),
								'value' => ( isset( $instance['excerpt'] ) ) ? $instance['excerpt'] : NULL ,
								'class' => 'layers-textarea layers-large'
								)
							); ?>
						</div>
					</div>

					<div class="layers-row clearfix">
						<div class="layers-panel">
							<div class="layers-content">
								<p class="layers-form-item">
									<label for="<?php echo $this->get_layers_field_id( 'icon1' ); ?>"><?php _e( 'contact form shortcode' , 'bride' ); ?></label>
									<?php echo $this->form_elements()->input(
										array(
											'type' => 'textarea',
											'name' => $this->get_layers_field_name( 'contact_form' ) ,
											'id' => $this->get_layers_field_id( 'contact_form' ) ,
											'placeholder' =>  __( 'Contact form shortcode add here' , 'bride' ),
											'value' => ( isset( $instance['contact_form'] ) ) ? $instance['contact_form'] : NULL ,
											'class' => 'layers-textarea'
											)
										); ?>
										<small class="layers-small-note">
											<?php _e( sprintf( 'Need to create a contact form? Try <a href="%1$s" target="ejejcsingle">Contact Form 7</a>', 'https://wordpress.org/plugins/contact-form-7/' ) , 'bride' ); ?>
										</small>
								</p>
							</div>
						</div>
					</div>

				</section>
			</div>

		<?php } // Form
	} // Class

	// Add our function to the widgets_init hook.
	register_widget("Bride_Contact_Widget");
}