<?php 
/*
* Plugin Name: Bride Layers Extension
* Version: 1.0
* Plugin URI: http://www.bootexperts.com
* Description: This plugin is mandatory for Grant Foundation Child Theme. This plugin is managing custom content such as Slider, Blog Posts, Call to action, Fun Facts Counter, Portfolio, Pricing Tables, Services, Skills Progress, Creative Team, Testimonials, Working Policy etc.
* Author: BootExperts
* Author URI: http://www.bootexperts.com/
* Text Domain: bride
* Domain Path: /languages/
*/

// Secure it
if ( ! defined( 'ABSPATH' ) ) exit;

// define constants 
define( 'BRIDE_LAYERS_EXTENSION_SLUG' , 'bride' );
define( 'BRIDE_LAYERS_EXTENSION_DIR' , trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'BRIDE_LAYERS_EXTENSION_URI' , trailingslashit( plugin_dir_url( __FILE__ ) ) );

// Load plugin class files
require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/class-layers-bride-extension.php' );
require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/products-carousel-widget.php' );
require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/bride_recent_post_widget.php' );
//require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/footer_about_us_widget.php' );
//require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/footer-instagram.php' );
require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/bride-post-type.php' );
require_once( BRIDE_LAYERS_EXTENSION_DIR. 'includes/shortcode.php' );

// Instantiate Plugin
if ( !function_exists('bride_extension_init') ) {
	
	function bride_extension_init() {
		global $bride_extension;
		$bride_extension = Bride_Layers_Extension::get_instance();
		// Localization
		load_plugin_textdomain('bride', FALSE, dirname(plugin_basename(__FILE__)) . "/languages");
	}
}
// Load plugin
add_action( 'plugins_loaded', 'bride_extension_init' );