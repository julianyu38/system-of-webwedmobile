<?php
// About us widget for footer 
add_action('widgets_init', 'enaa_about_us_widgets');

if(!function_exists('enaa_about_us_widgets')){
	
	function enaa_about_us_widgets(){
		
		register_widget('enaa_about_us_Widget');
		
	}
}

if(!class_exists('enaa_about_us_Widget')){
	
	class enaa_about_us_Widget extends WP_Widget {

		public function __construct(){
			$widget_ops = array('classname' => 'about_us', 'description' => '');
			$control_ops = array('id_base' => 'about_us-widget');
			parent::__construct('about_us-widget', 'Enaa: Footer Address', $widget_ops, $control_ops);
		}
		public function widget($args, $instance){
			extract($args);
			$title = apply_filters('widget_title', $instance['title']);
			echo $args['before_widget'];
			if($title) {
				echo $args['before_title'] . $title . $args['after_title'];
			}
			?>
			<!-- About Widget -->
				<div class="about-footer">
					<div class="footer-widget address">
						<div class="footer-address">
							<?php if(isset($instance['address1'])): ?>
								<div class="single_fadd">
								 <?php if ( isset($instance['address_icon']) ) { ?>
									<div style="color:<?php echo $instance['icon_color']; ?>" class="footer-add-icon">
										<i class="zmdi zmdi-<?php echo $instance['address_icon']; ?>"></i> 
									</div>	
								  <?php } ?>
									<div class="footer-add-info">				
										<p><?php echo $instance['address1']; ?></p>
									</div> 
								</div>
							<?php endif; ?>

							<?php if(isset($instance['telephone'])): ?>
								<div class="single_fadd">
								 <?php if ( isset($instance['telephone_icon']) ) { ?>
									<div style="color:<?php echo $instance['icon_color']; ?>"  class="footer-add-icon">
										<i class="zmdi zmdi-<?php echo $instance['telephone_icon']; ?>"></i> 
									</div>	
								  <?php } ?>
									<div class="footer-add-info">   
											<p><?php echo $instance['telephone']; ?></p>
									</div>
								</div>	
							<?php endif; ?>

							<?php if(isset($instance['fax'])): ?>
								<div class="single_fadd">
								 <?php if ( isset($instance['fax_icon']) ) { ?>
									<div  style="color:<?php echo $instance['icon_color']; ?>"  class="footer-add-icon">
										<i class="zmdi zmdi-<?php echo $instance['fax_icon']; ?>"></i> 
									</div>	
								  <?php } ?>
									<div class="footer-add-info">  
										<p><?php echo $instance['fax']; ?></p>									
									</div>
								</div>	
							<?php endif; ?>
							
							<?php if(isset($instance['email'])): ?>
								<div class="single_fadd">
								 <?php if ( isset($instance['email_icon']) ) { ?>
									<div style="color:<?php echo $instance['icon_color']; ?>" class="footer-add-icon">
										<i class="zmdi zmdi-<?php echo $instance['email_icon']; ?>"></i> 
									</div>	
								  <?php } ?>
									<div class="footer-add-info">  
										<p><?php echo $instance['email']; ?></p>									
									</div>
								</div>	
							<?php endif; ?>


						</div>
					</div>
				</div>	

			<?php
			echo $args['after_widget'];
		}


       // update section

		public function update($new_instance, $old_instance){

			$instance = $old_instance;
			$instance['title'] = $new_instance['title'];
			$instance['address1'] = $new_instance['address1'];
			$instance['address_icon'] = $new_instance['address_icon'];
			$instance['telephone'] = $new_instance['telephone'];
			$instance['telephone_icon'] = $new_instance['telephone_icon'];
			$instance['email'] = $new_instance['email'];
			$instance['email_icon'] = $new_instance['email_icon'];
			$instance['fax'] = $new_instance['fax'];
			$instance['fax_icon'] = $new_instance['fax_icon'];
			$instance['icon_color'] = $new_instance['icon_color'];
			return $instance;

		}

	    // admin form section

		public function form($instance){

			$defaults = array(
		       'title' => 'contact us ',
			   'content' => '', 
			   'address1' => '8901 Marmora Raod,<br>Glasgow, D04  89GR',
			   'address_icon' => 'pin',
			   'telephone' => '01922262289',
   			   'telephone_icon' => 'phone',
			   'email' => 'Infor@bootexperts.com',
  			   'email_icon' => 'email',
			   'fax' => 'I55 44 77 62',
			   'fax_icon' => 'reader',
			   'icon_color' => '#fff'
			   );

			$instance = wp_parse_args((array) $instance, $defaults); ?>

			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('address1')); ?>"><?php esc_html_e('Address:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('address1')); ?>" name="<?php echo esc_attr($this->get_field_name('address1')); ?>" value="<?php echo esc_attr($instance['address1']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('address_icon')); ?>"><?php esc_html_e('Address Icon:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('address_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('address_icon')); ?>" value="<?php echo esc_attr($instance['address_icon'] ); ?>" />
				<span class="layers-form-item-description">
					<?php _e( 'If you want more icons please click <a target="_blank" href="https://zavoloklom.github.io/material-design-iconic-font/icons.html"> here </a>.', 'ENAA_LAYERS_EXTENSION_SLUG' ); ?>
				</span>
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('telephone')); ?>"><?php esc_html_e('Telephone:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('telephone')); ?>" name="<?php echo esc_attr($this->get_field_name('telephone')); ?>" value="<?php echo esc_attr($instance['telephone']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('telephone_icon')); ?>"><?php esc_html_e('Telephone Icon:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('telephone_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('telephone_icon')); ?>" value="<?php echo esc_attr($instance['telephone_icon']); ?>" />
				<span class="layers-form-item-description">
					<?php _e( 'If you want more icons please click <a target="_blank" href="https://zavoloklom.github.io/material-design-iconic-font/icons.html"> here </a>.', 'ENAA_LAYERS_EXTENSION_SLUG' ); ?>
				</span>
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('fax')); ?>"><?php esc_html_e('Fax:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('fax')); ?>" name="<?php echo esc_attr($this->get_field_name('fax')); ?>" value="<?php echo esc_attr($instance['fax']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('fax_icon')); ?>"><?php esc_html_e('Fax Icon:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('fax_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('fax_icon')); ?>" value="<?php echo esc_attr($instance['fax_icon']); ?>" />
				<span class="layers-form-item-description">
					<?php _e( 'If you want more icons please click <a target="_blank" href="https://zavoloklom.github.io/material-design-iconic-font/icons.html"> here </a>.', 'ENAA_LAYERS_EXTENSION_SLUG' ); ?>
				</span>
			</p>

			<p>
				<label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email & Web:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" value="<?php echo esc_attr($instance['email']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('email_icon')); ?>"><?php esc_html_e('Email Icon:','enaa');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('email_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('email_icon')); ?>" value="<?php echo esc_attr($instance['email_icon']); ?>" />
				<span class="layers-form-item-description">
					<?php _e( 'If you want more icons please click <a target="_blank" href="https://zavoloklom.github.io/material-design-iconic-font/icons.html"> here </a>.', 'ENAA_LAYERS_EXTENSION_SLUG' ); ?>
				</span>
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('icon_color')); ?>"><?php esc_html_e('Icons Color:','enaa');?></label>
				<input class="wp-color-result widefat" type="color" id="<?php echo esc_attr($this->get_field_id('icon_color')); ?>" name="<?php echo esc_attr($this->get_field_name('icon_color')); ?>" value="<?php echo esc_attr($instance['icon_color']); ?>" />
			</p>

		  <?php

		}

	}
}
?>