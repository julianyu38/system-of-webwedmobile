<?php  

// Register bride Post Types
// http://docs.layerswp.com/create-an-extension-adding-custom-post-types-page-templates/

 // HT SLIDER post type
if ( !post_type_exists('ht_slider') ) {
	function ht_default_slider() {
	
	$labels = array(
		'name'               => _x( 'Sliders', 'bride' ),
		'singular_name'      => _x( 'Slider', 'bride' ),
		'menu_name'          => _x( 'Slider', 'bride' ),
		'name_admin_bar'     => _x( 'Slider', 'bride' ),
	);

	$args = array(
		'labels'             => $labels,
        'description'        => esc_html__( 'Description.', 'bride' ),
		'public'             => true,
		'menu_icon'     => 'dashicons-images-alt2',		
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ht_slider' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title','thumbnail' )
	);
	// post type testimonial
	register_post_type( 'ht_slider', $args );	


	$args = array(
		'hierarchical'      => true,
		'labels'            => array(
				'name'              => _x( 'Slider Categories', 'bride' ),
				'singular_name'     => _x( 'Slider Category', 'bride' ),
				'menu_name'         => esc_html__( 'Slider Category' ),
			),
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'ht_slider_category' ),
	);
	// Taxonomy testimonial
	register_taxonomy('ht_slider_category','ht_slider',$args);;
	}	
} // END if
add_action('init','ht_default_slider');


// Gallery post type
if ( !post_type_exists('bride_gallery') ) {
 function bride_gallery_post() {
 
 $labels = array(
  'name'               => _x( 'wedding plans ', 'bride' ),
  'singular_name'      => _x( 'wedding plans ', 'bride' ),
  'menu_name'          => _x( 'wedding plans ', 'bride' ),
  'name_admin_bar'     => _x( 'wedding plans ', 'bride' ),
  'add_new'            => _x( 'Add New wedding plans ', 'bride' ),
  'add_new_item'       => esc_html__( 'Add New wedding plans ', 'bride' ),
  'new_item'           => esc_html__( 'New wedding plans ', 'bride' ),
  'edit_item'          => esc_html__( 'Edit wedding plans ', 'bride' ),
  'view_item'          => esc_html__( 'View wedding plans ', 'bride' ),
  'all_items'          => esc_html__( 'All wedding plans ', 'bride' ),
  'search_items'       => esc_html__( 'Search wedding plans ', 'bride' ),
  'parent_item_colon'  => esc_html__( 'Parent wedding plans :', 'bride' ),
  'not_found'          => esc_html__( 'No wedding plans  found.', 'bride' ),
  'not_found_in_trash' => esc_html__( 'No wedding plans  found in Trash.', 'bride' )
 );

 $args = array(
  'labels'             => $labels,
        'description'        => esc_html__( 'Description.', 'bride' ),
  'public'             => true,
  'menu_icon'     => 'dashicons-images-alt',  
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'bride_gallery' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
 );
 
 
 // post type testimonial
 register_post_type( 'bride_gallery', $args ); 
 
 $labels = array(
  'name'              => _x( 'Wedding plans  Categories', 'bride' ),
  'singular_name'     => _x( 'Wedding plans  Category', 'bride' ),
  'search_items'      => esc_html__( 'Search Category' ),
  'all_items'         => esc_html__( 'All Category' ),
  'parent_item'       => esc_html__( 'Parent Category' ),
  'parent_item_colon' => esc_html__( 'Parent Category:' ),
  'edit_item'         => esc_html__( 'Edit Category' ),
  'update_item'       => esc_html__( 'Update Category' ),
  'add_new_item'      => esc_html__( 'Add New Category' ),
  'new_item_name'     => esc_html__( 'New Category Name' ),
  'menu_name'         => esc_html__( 'Wedding plans  Category' ),
 );

 $args = array(
  'hierarchical'      => true,
  'labels'            => $labels,
  'show_ui'           => true,
  'show_admin_column' => true,
  'query_var'         => true,
  'rewrite'           => array( 'slug' => 'bride_gallery_category' ),
 );
 // Taxonomy testimonial
 register_taxonomy('bride_gallery_category','bride_gallery',$args);
 } 
} // END if
add_action('init','bride_gallery_post');


// Gallery post type
if ( !post_type_exists('bride_gallery_2') ) {
 function bride_gallery_post_2() {
 
 $labels = array(
  'name'               => _x( 'Gallery', 'bride' ),
  'singular_name'      => _x( 'Gallery', 'bride' ),
  'menu_name'          => _x( 'Gallery', 'bride' ),
  'name_admin_bar'     => _x( 'Gallery', 'bride' ),
  'add_new'            => _x( 'Add New Gallery', 'bride' ),
  'add_new_item'       => esc_html__( 'Add New Gallery', 'bride' ),
  'new_item'           => esc_html__( 'New Gallery', 'bride' ),
  'edit_item'          => esc_html__( 'Edit Gallery', 'bride' ),
  'view_item'          => esc_html__( 'View Gallery', 'bride' ),
  'all_items'          => esc_html__( 'All Gallery', 'bride' ),
  'search_items'       => esc_html__( 'Search Gallery', 'bride' ),
  'parent_item_colon'  => esc_html__( 'Parent Gallery:', 'bride' ),
  'not_found'          => esc_html__( 'No Gallery found.', 'bride' ),
  'not_found_in_trash' => esc_html__( 'No Gallery found in Trash.', 'bride' )
 );

 $args = array(
  'labels'             => $labels,
        'description'        => esc_html__( 'Description.', 'bride' ),
  'public'             => true,
  'menu_icon'     => 'dashicons-images-alt',  
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'bride_gallery_2' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
 );
 
 
 // post type testimonial
 register_post_type( 'bride_gallery_2', $args ); 
 
 $labels = array(
  'name'              => _x( 'Gallery Categories', 'bride' ),
  'singular_name'     => _x( 'Gallery Category', 'bride' ),
  'search_items'      => esc_html__( 'Search Category' ),
  'all_items'         => esc_html__( 'All Category' ),
  'parent_item'       => esc_html__( 'Parent Category' ),
  'parent_item_colon' => esc_html__( 'Parent Category:' ),
  'edit_item'         => esc_html__( 'Edit Category' ),
  'update_item'       => esc_html__( 'Update Category' ),
  'add_new_item'      => esc_html__( 'Add New Category' ),
  'new_item_name'     => esc_html__( 'New Category Name' ),
  'menu_name'         => esc_html__( 'Gallery Category' ),
 );

 $args = array(
  'hierarchical'      => true,
  'labels'            => $labels,
  'show_ui'           => true,
  'show_admin_column' => true,
  'query_var'         => true,
  'rewrite'           => array( 'slug' => 'bride_gallery_category' ),
 );
 // Taxonomy testimonial
 register_taxonomy('bride_gallery_category','bride_gallery_2',$args);
 } 
} // END if
add_action('init','bride_gallery_post_2');


// Gallery post type
if ( !post_type_exists('bride_photo_plans') ) {
 function bride_photo_plans_post() {
 
 $labels = array(
  'name'               => _x( 'Photo Plan', 'bride' ),
  'singular_name'      => _x( 'Photo Plan', 'bride' ),
  'menu_name'          => _x( 'Photo Plan', 'bride' ),
  'name_admin_bar'     => _x( 'Photo Plans', 'bride' ),
  'add_new'            => _x( 'Add New Photo Plan', 'bride' ),
  'add_new_item'       => esc_html__( 'Add New Photo Plan', 'bride' ),
  'new_item'           => esc_html__( 'New Photo Plan', 'bride' ),
  'edit_item'          => esc_html__( 'Edit Photo Plan', 'bride' ),
  'view_item'          => esc_html__( 'View Photo Plan', 'bride' ),
  'all_items'          => esc_html__( 'All Photo Plan', 'bride' ),
  'search_items'       => esc_html__( 'Search Photo Plan', 'bride' ),
  'parent_item_colon'  => esc_html__( 'Parent Photo Plan:', 'bride' ),
  'not_found'          => esc_html__( 'No Photo Plan found.', 'bride' ),
  'not_found_in_trash' => esc_html__( 'No Photo found in Trash.', 'bride' )
 );

 $args = array(
  'labels'             => $labels,
        'description'        => esc_html__( 'Description.', 'bride' ),
  'public'             => true,
  'menu_icon'     => 'dashicons-camera',  
  'publicly_queryable' => true,
  'show_ui'            => true,
  'show_in_menu'       => true,
  'query_var'          => true,
  'rewrite'            => array( 'slug' => 'bride_photo_plans' ),
  'capability_type'    => 'post',
  'has_archive'        => true,
  'hierarchical'       => false,
  'menu_position'      => null,
  'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
 );
 
 
 // post type testimonial
 register_post_type( 'bride_photo_plans', $args ); 
 
 $labels = array(
  'name'              => _x( 'Photo Categories', 'bride' ),
  'singular_name'     => _x( 'Photo Category', 'bride' ),
  'search_items'      => esc_html__( 'Search Category' ),
  'all_items'         => esc_html__( 'All Category' ),
  'parent_item'       => esc_html__( 'Parent Category' ),
  'parent_item_colon' => esc_html__( 'Parent Category:' ),
  'edit_item'         => esc_html__( 'Edit Category' ),
  'update_item'       => esc_html__( 'Update Category' ),
  'add_new_item'      => esc_html__( 'Add New Category' ),
  'new_item_name'     => esc_html__( 'New Category Name' ),
  'menu_name'         => esc_html__( 'Gallery Category' ),
 );

 $args = array(
  'hierarchical'      => true,
  'labels'            => $labels,
  'show_ui'           => true,
  'show_admin_column' => true,
  'query_var'         => true,
  'rewrite'           => array( 'slug' => 'bride_photo_category' ),
 );
 // Taxonomy testimonial
 register_taxonomy('bride_photo_category','bride_photo_plans',$args);
 } 
} // END if
add_action('init','bride_photo_plans_post');



// bride Testimonia post type
if ( !post_type_exists('bride_all_say') ) {
	function register_layers_testimonial_post() {
	

	$labels = array(
		'name'               => _x( 'Testimonial', 'bride' ),
		'singular_name'      => _x( 'Testimonial', 'bride' ),
		'menu_name'          => _x( 'Testimonial', 'bride' ),	
		'name_admin_bar'     => _x( 'Testimonial', 'bride' ),
		'add_new'            => _x( 'Add New Testimonial', 'testimonial', 'bride' ),
		'add_new_item'       => esc_html__( 'Add New Testimonial', 'bride' ),
		'new_item'           => esc_html__( 'New Testimonial', 'bride' ),
		'edit_item'          => esc_html__( 'Edit Testimonial', 'bride' ),
		'view_item'          => esc_html__( 'View Testimonial', 'bride' ),
		'all_items'          => esc_html__( 'All Testimonial', 'bride' ),
		'search_items'       => esc_html__( 'Search Testimonial', 'bride' ),
		'parent_item_colon'  => esc_html__( 'Parent Testimonial:', 'bride' ),
		'not_found'          => esc_html__( 'No Testimonial found.', 'bride' ),
		'not_found_in_trash' => esc_html__( 'No Testimonial found in Trash.', 'bride' )
	);

	$args = array(
		'labels'             => $labels,
        'description'        => esc_html__( 'Description.', 'bride' ),
		'public'             => true,
		'publicly_queryable' => true,
		'menu_icon'     	 => 'dashicons-editor-quote',
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'bride_all_say' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array(  'title', 'thumbnail', 'editor')
	);
	// post type service
	register_post_type( 'bride_all_say', $args );	
	
	}
} // END if

add_action('init','register_layers_testimonial_post');






































