<?php
// About us widget for footer 
add_action('widgets_init', 'ROVAST_ABOUT_US_WIDGETS');

if(!function_exists('ROVAST_ABOUT_US_WIDGETS')){
	
	function ROVAST_ABOUT_US_WIDGETS(){
		
		register_widget('rovast_about_us_Widget');
		
	}
}

if(!class_exists('rovast_about_us_Widget')){
	
	class rovast_about_us_Widget extends WP_Widget {

		public function __construct(){
			$widget_ops = array('classname' => 'about_us', 'description' => '');
			$control_ops = array('id_base' => 'about_us-widget');
			parent::__construct('about_us-widget', 'Rovast: Footer Address', $widget_ops, $control_ops);
		}
		public function widget($args, $instance){
			extract($args);
			$title = apply_filters('widget_title', $instance['title']);
			echo $args['before_widget'];
			if($title) {
				echo $args['before_title'] . $title . $args['after_title'];
			}
			?>
			<!-- About Widget -->
				<div class="about-footer">
					<div class="footer-widget address">
						<div class="footer-logo">
								<img src="<?php echo esc_url($instance['image_uri']); ?>" alt="">
							<p><?php echo $instance['content']; ?></p>
						</div>
						<div class="footer-address">
							<?php if(isset($instance['address1'])): ?>
								<div class="footer-add-icon">
									<i class="fa fa-map-marker"></i>
								</div>									
								<div class="footer-add-info">				
									<p><?php echo $instance['address1']; ?></p>
								</div> 
							<?php endif; ?>
							<?php if(isset($instance['telephone'])): ?>
								<div class="footer-add-icon">
									<i class="fa fa-phone"></i>
								</div> 									
								<div class="footer-add-info">   
										<p><?php echo $instance['telephone']; ?></p>
								</div>
							<?php endif; ?>
							<?php if(isset($instance['email'])): ?>
								<div class="footer-add-icon"> 
									<i class="fa fa-globe"></i>
								</div> 
								<div class="footer-add-info">  
									<p><?php echo $instance['email']; ?></p>									
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>	

			<?php
			echo $args['after_widget'];
		}


		public function update($new_instance, $old_instance){

			$instance = $old_instance;
			$instance['title'] = $new_instance['title'];
			$instance['content'] = $new_instance['content'];
			$instance['image_uri'] = $new_instance['image_uri'];
			$instance['address1'] = $new_instance['address1'];
			$instance['telephone'] = $new_instance['telephone'];
			$instance['email'] = $new_instance['email'];

			return $instance;

		}

		public function form($instance){

			$defaults = array('title' => '', 'content' => 'Lorem ipsum dolor sit amet, consetur acing elit, sed do eiusmod tempor Lorem ipsum dolor sit amet, consetur acing elit, sed do eiusmod tempor', 'image_uri' => '','address1' => '8901 Marmora Raod,<br>Glasgow, D04  89GR','telephone' => 'Telephone : (801) 2223 3337<br>Telephone : (801) 4256  9658','email' => 'Infor@bootexperts.com<br>Web : www.rovast.com');

			$instance = wp_parse_args((array) $instance, $defaults); ?>

			<p>
				<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:','rovast');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
			</p>

			<p>
				<label style="display:block;" for="<?php echo esc_attr($this->get_field_id('image_uri')); ?>"><?php esc_html_e('Upload Image:','rovast');?></label>
				
				<img class="custom_media_image" src="<?php if(!empty($instance['image_uri'])){echo $instance['image_uri'];} ?>" style="margin:0;padding:0;max-width:100px;display:inline-block" />
				
				<input type="text" class="widefat custom_media_url" name="<?php echo esc_attr($this->get_field_name('image_uri')); ?>" id="<?php echo esc_attr($this->get_field_id('image_uri')); ?>" value="<?php echo esc_attr($instance['image_uri']); ?>">
				<a href="#" id="custom_media_button" style="margin-top:10px;" class="button button-primary custom_media_button"><?php esc_html_e('Upload', 'rovast'); ?></a>
			</p>

			<p>

				<label for="<?php echo esc_attr($this->get_field_id('content')); ?>"><?php esc_html_e('About Text:','rovast');?></label>
				<textarea class="widefat" rows="5" type="text" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" ><?php echo $instance['content']; ?></textarea>
				<span> <b><?php esc_html_e('Note:','rovast');?></b><?php esc_html_e('use','rovast');?> <b> &ltbr&gt </b><?php esc_html_e(' for line break.','rovast');?></span>

			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('address1')); ?>"><?php esc_html_e('Address:','rovast');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('address1')); ?>" name="<?php echo esc_attr($this->get_field_name('address1')); ?>" value="<?php echo esc_attr($instance['address1']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('telephone')); ?>"><?php esc_html_e('Telephone:','rovast');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('telephone')); ?>" name="<?php echo esc_attr($this->get_field_name('telephone')); ?>" value="<?php echo esc_attr($instance['telephone']); ?>" />
			</p>
			<p>
				<label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email & Web:','rovast');?></label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" value="<?php echo esc_attr($instance['email']); ?>" />
			</p>
		<?php

		}

	}
}
?>