<?php

// Creating the widget 
if( class_exists('WP_Widget') && !class_exists( 'Bride_Product_Carousel_Widget' ) ) {
	class Bride_Product_Carousel_Widget extends WP_Widget {

	public function __construct() {
		parent::__construct(
		'Bride_Product_Carousel_Widget', 
		esc_html__('bride Product Carousel Widget', 'bride'), 
		array( 'description' => esc_html__( 'This widget is used to display products carousel', 'bride' ), ) 
		);
	}

	// Creating widget front-end
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$subtitle = ! empty( $instance['subtitle'] ) ? $instance['subtitle'] : ''; 
		$shortcode_field = ! empty( $instance['shortcode_field'] ) ? $instance['shortcode_field'] : '';
		
		// This is where you run the code and display the output
		
		do_action( 'layers_before_index' ); ?>

		<div class="product-carousel-area">
			<div class="container content-main archive clearfix">
				<div class="grid">
					<div class="column span-12">
						<div class="section-title">
							<?php if ( ! empty( $title ) ) { ?>
								<h2 class="heading"><?php echo esc_html( $title );?></h2>
							<?php } ?>
							<?php if ( ! empty( $subtitle ) ) { ?>
								<div class="excerpt"><?php echo esc_html( $subtitle );?></div>
							<?php } ?>
						</div>
					</div>
					<div class="column span-12">			
						<?php echo do_shortcode( $shortcode_field ); ?>
					</div>
				</div>  
			</div>
		</div><!-- end product-carousel-area -->

		<?php do_action( 'layers_after_index' ); 

	}
			
	// Widget Backend 
	public function form( $instance ) {

		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$subtitle = ! empty( $instance['subtitle'] ) ? $instance['subtitle'] : '';
		$shortcode_field = ! empty( $instance['shortcode_field'] ) ? $instance['shortcode_field'] : ''; ?>

	<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'bride' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'subtitle' ); ?>"><?php esc_html_e( 'Subtitle:', 'bride' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'subtitle' ); ?>" name="<?php echo $this->get_field_name( 'subtitle' ); ?>" type="text" value="<?php echo esc_attr( $subtitle ); ?>" />
	</p>
	<p>
		<label for="<?php echo $this->get_field_id( 'shortcode_field' ); ?>"><?php esc_html_e( 'Shortcode:', 'bride' ); ?></label>
		<textarea name="<?php echo $this->get_field_name( 'shortcode_field' ); ?>" id="<?php echo $this->get_field_id( 'shortcode_field' ); ?>">
			<?php echo esc_html( $shortcode_field ); ?>
		</textarea>
	</p>
		
	<?php 

	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['subtitle'] = ( ! empty( $new_instance['subtitle'] ) ) ? strip_tags( $new_instance['subtitle'] ) : '';
		$instance['shortcode_field'] = ( ! empty( $new_instance['shortcode_field'] ) ) ? strip_tags( $new_instance['shortcode_field'] ) : '';
		return $instance;
	}

	} // Class Bride_Product_Carousel_Widget ends here
}

// Register and load the widget
function bride_load_product_carousel_widget() {
	register_widget( 'Bride_Product_Carousel_Widget' );
}
add_action( 'widgets_init', 'bride_load_product_carousel_widget' );