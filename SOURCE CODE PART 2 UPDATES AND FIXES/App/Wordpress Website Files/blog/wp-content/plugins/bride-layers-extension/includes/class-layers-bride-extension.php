<?php
/**
* Bride Layers Extension
* http://docs.layerswp.com/create-an-extension-setup-your-plugin-class/#setup-class
*/
if( !class_exists('Bride_Layers_Extension') ) {
	class Bride_Layers_Extension {

		// Setup Instance
		private static $_instance=null;
		
		public static function get_instance() {
			
			if ( is_null( self::$_instance ) )
				self::$_instance = new Bride_Layers_Extension();

			return self::$_instance;
		}

		// Constructor
		private function __construct() {
			// Register custom widgets
			add_action( 'widgets_init' , array( $this, 'bride_register_widgets' ), 50 );		
		}

		// Register Widgets
		public function bride_register_widgets(){
			
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-slider-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-about-us-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-wedding-plans-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-photo-of-wedding-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-photo-plans-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-testimonial-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-blog-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-testimonial-widget-style-2.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-blog-widget-style-2.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-slider-widget-with-countdown.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-groom-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-location-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-wedding-schedule-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-gallery-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-product-widget.php' );
		   require_once( BRIDE_LAYERS_EXTENSION_DIR. 'widgets/bride-contact-widget.php' );
		   
		   



		   
		}
		
	} // END Class
}