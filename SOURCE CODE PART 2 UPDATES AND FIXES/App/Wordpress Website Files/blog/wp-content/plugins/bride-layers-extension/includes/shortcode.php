<?php 
// shortcode Progess bar
if ( !function_exists('progres_bar_shortcode') ) {
	function progres_bar_shortcode($atts,$content=null){
		extract(shortcode_atts(
			array(
			'id' => 'prog',				
			'height' => '6',
			'bgcolor' => '#fff',
			'bwidth' => '0',
			'bdcolor' => '',		
			'color' => '#337ab7',
			'tcolor' => '#444',
			'title' => 'Web Development',
			'value' => '80',			
			'duration' => '1.5',			
			'delay' => '1.2'			
			),$atts));

		return '
			<style type="text/css">
				#progressid'.$id.' {border-width:'.$bwidth.';border-style:solid; border-color:'.$bdcolor.';height:'.$height.'px;background-color:'.$bgcolor.'}		
				#progressbar'.$id.' {background-color:'.$color.'}
				#tooltipbg'.$id.' {background-color:'.$color.';color:'.$tcolor.'}
				#tooltipbg'.$id.'::before {border-color:'.$color.' transparent transparent;}
				#textcolor'.$id.'  {color:'.$tcolor.'}
			</style>	
			
		<div class="skill-bars">
			<div class="skill">
				<div class="progress" id="progressid'.$id.'">
				 <div id="textcolor'.$id.'" class="lead">'.$title.'</div>
				 <div class="progress-bar wow fadeInLeft" id="progressbar'.$id.'" data-progress="'.$value.'%" style="width: '.$value.'%;" data-wow-duration="'.$duration.'s" data-wow-delay="'.$delay.'s"> <span id="tooltipbg'.$id.'">'.$value.'%</span></div>
				</div>
			</div>
		</div>              	
		';

	}//[progress id="p1" height="6" bgcolor="#f00" bwidth="1" color="#f00" title="web development" tcolor="#000" " value="80" duration="1.5" delay="1.2"]
}//end if	
add_shortcode('progress','progres_bar_shortcode');

// animate text
if(!function_exists('nrb_animate_text')){
	function nrb_animate_text($atts,$content=null){
		extract(shortcode_atts(array(
			'text1' 		=>'',
			'text2'			=>'',
			'text3'			=>'',
		),$atts));
		ob_start();
		?>
		
		<span class="cd-words-wrapper">
			<b class="is-visible"> <?php esc_html_e($text1,'rovast');?></b>
			<b><?php esc_html_e($text2,'rovast');?></b>
			<b><?php esc_html_e($text3,'rovast');?></b>
		</span>
			
		<?php
		return ob_get_clean();
	}
}
add_shortcode('animate','nrb_animate_text');
