/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var publisher;
var session;

var app = {
  // Application Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'app.receivedEvent(...);'
  onDeviceReady: function() {

      // Sign up for an OpenTok API Key at: https://tokbox.com/signup
      // Then generate a sessionId and token at: https://dashboard.tokbox.com
      /*var apiKey = "45431142"; // INSERT YOUR API Key
      var sessionId = "2_MX40NTQzMTE0Mn5-MTQ0OTY0MjQ1ODM0MX5IREV1NEMwWFhPMkQyMHdWcFA2ZFMyZE5-UH4"; // INSERT YOUR SESSION ID
      var token = "T1==cGFydG5lcl9pZD00NTQzMTE0MiZzaWc9M2IyNWE3ODRiYTkyOTE3NWEyYjM0NDUzMTk2YmQ1Mjc0ZTgwYmViMDpyb2xlPXB1Ymxpc2hlciZzZXNzaW9uX2lkPTJfTVg0ME5UUXpNVEUwTW41LU1UUTBPVFkwTWpRMU9ETTBNWDVJUkVWMU5FTXdXRmhQTWtReU1IZFdjRkEyWkZNeVpFNS1VSDQmY3JlYXRlX3RpbWU9MTQ0OTY0MjQ3NSZub25jZT0wLjIyMTYyNTIwNDkzNjk5OTIyJmV4cGlyZV90aW1lPTE0NTAyNDcyNDImY29ubmVjdGlvbl9kYXRhPQ=="; // INSERT YOUR TOKEN

      // Very simple OpenTok Code for group video chat
      
      session = TB.initSession( apiKey, sessionId ); 
      session.on({
        'streamCreated': function( event ){
            var div = document.getElementById('yours');
            //div.setAttribute('id', 'player1');
            //document.body.appendChild(div);
            session.subscribe( event.stream, div.id, {subscribeToAudio: true, insertMode: "append"} );
        }
      });

      publisher = TB.initPublisher(apiKey,'you');
      session.connect(token, function(){
        session.publish( publisher );
      });*/

  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
  }
};
