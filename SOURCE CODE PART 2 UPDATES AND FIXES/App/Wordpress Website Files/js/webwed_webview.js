window.socket = null;
window.sessionId = null;
window.viewerCount = 0;
window.streams = {};
window.weddingReady = false;

function sendMessage(message) {
    window.socket.emit('message', message);
};

function sendChatMessage(body) {
    //var body = ''; //Value for textbox element
    $('.messages').append('<li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
    window.sendMessage({
        id: 'post',
        text: body
    });
};

function showGiftRegistry(){
    var giftlink = JSON.parse(getCookie('wedding')).giftlink;
    if (giftlink != ''){
        displayIFrameModal(giftlink);
    }
}

function loadScript(url, callback) {
    jQuery.ajax({
        url: url,
        dataType: 'script',
        success: callback,
        async: true
    });
};

var apiEndpoint = 'api/v1/api.php';

function loginAsGuest(){
    login($('#emailfield').val(), $('#passcodefield').val());
}

function login(email, passcode) {
    console.log('Email: ' + email);
    console.log('Passcode: ' + passcode);
    passcode = passcode.toUpperCase();
    $.ajax({
        url: apiEndpoint + "?cmd=login",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        },
        method: 'POST',
        data: {
            type: 'guest',
            email: email,
            weddingcode: passcode
        }
    }).done(function(data) {
        console.log(data);
        if (data.errorCode == null || data.errorCode == undefined) {
            successGuestLogin(data.data);
        } else {
            failureGuestLogin(data);
        }
    });
};

var name = getCookie('name');
var token = getCookie('token');
var wedding = getCookie('wedding');
var settings = getCookie('settings');

if (name != '' && token != '' && wedding != '' && settings != ''){
    assemble();
    document.getElementById("video_container").style.opacity = 0;
}else{
    document.getElementById("video_container").style.opacity = 1;
}

function successGuestLogin(data) {
    setCookie('name', JSON.stringify(data.name), 1);
    setCookie('token', JSON.stringify(data.token), 1);
    setCookie('wedding', JSON.stringify(data.wedding), 1);
    setCookie('settings', JSON.stringify(data.settings), 1);

    console.log(data);

    assemble();
};

function setSettings(){
    var public = JSON.parse(getCookie('settings')).public;
    var chat = JSON.parse(getCookie('settings')).chat;
    var gifts = JSON.parse(getCookie('settings')).gifts;

    var giftlink = JSON.parse(getCookie('wedding')).giftlink;
    if (giftlink == '' || (gifts == 0)){
        $('#gift').addClass('disabled');
    }else{
        $('#gift').removeClass('disabled');
    }

    if ((chat == 0)){
        $("#chat").addClass('disabled');
    }else{
        $("#chat").removeClass('disabled');
    }

    if ((public == 0)){
        $("#sharelink").addClass('disabled');
    }else{
        $("#sharelink").removeClass('disabled');
    }

    $("#event").removeClass("disabled");

    $("#sociallink").attr('data-url', JSON.parse(getCookie('wedding')).ceremonylink);

    $("#HideText").hide();
    $("#CoupleName").text(JSON.parse(getCookie('wedding')).yours1.name + " & " + JSON.parse(getCookie('wedding')).yours2.name);
    $("#WeddingDate").text(JSON.parse(getCookie('wedding')).date.computed);
    $("#sign-in-red").attr('onClick', 'signoutOnClick(event)');
    $("#sign-in-red").removeAttr('data-target');
    $("#sign-in-red").removeAttr('data-toggle');
    $("#accountname").text('Sign-out');
}

$("#sign-in-red").click(function(){
    $(".modal-backdrop").click(function(){ hideModals(); });
});

function signoutOnClick(event){
    event.preventDefault();
    signout();
    
    setTimeout(function(){
        $("#sign-in-red").attr('data-target', '#sign-in-modal');
        $("#sign-in-red").attr('data-toggle', 'modal');
    }, 500);
    
    hideModals();
    document.getElementById("video_container").style.opacity = 1;
    $(".modal-backdrop").click(function(){ hideModals(); });
}

function addModalBackdropHide(){
    setTimeout(function(){
        $(".modal-backdrop").click(function(){ hideModals(); });
    }, 500);
}

function signout(){
    sendMessage({
        id: 'leaveRoom'
    });
    window.socket.disconnect();
    $('#gift').addClass('disabled');
    $('#chat').addClass('disabled');
    $('#sharelink').addClass('disabled');
    $('#event').addClass('disabled');
    $("#HideText").show();
    $("#CoupleName").text('TO OUR PREVIEW');
    $("#WeddingDate").text('DEMO CEREMONY\nVIEW THE LIVE STREAM FEED BY PRESSING THE BLUE HEART ABOVE.');
    $("#sign-in-red").removeAttr('onClick');
    $("#sign-in-red").attr('onClick', 'addModalBackdropHide();');
    $("#accountname").text('Account');
    deleteAllCookies();
}

function deleteAllCookies() {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i];
        var eqPos = cookie.indexOf("=");
        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
}

function addEvent(){
    var time = new Date(JSON.parse(getCookie('wedding')).date.computed);
    var starttime = addMinutes(time, -30);
    var endtime = addMinutes(time, 30);
    var name = 'WebWedMobile Marriage';
    var summary = JSON.parse(getCookie('wedding')).yours1.name + ' & ' + JSON.parse(getCookie('wedding')).yours2.name + ' are getting hitched with WebWedMobile.';
    window.open ('ics.php?start=' + starttime + '&end=' + endtime + '&name=' + name + '&summary=' + summary,'_blank',false);
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}

function submitcontact(){
    var name = $("#contactnamefield").val();
    var email = $("#contactemailfield").val();
    var question = $("#contactquestionfield").val();
    var message = $("#contactmessage").val();

    var messageComposed = '';

    $.ajax({
        url: apiEndpoint + "?cmd=contactus",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        },
        method: 'POST',
        data: {
            name: name,
            email: email,
            message: messageComposed
        }
    }).done(function(data) {
        $('.contact-success').show();
        $('.contact-error').hide();
        $("#contactnamefield").val('');
        $("#contactemailfield").val('');
        $("#contactquestionfield").val('');
        $("#contactmessage").val('');

    });
}

function assemble(){
    loadScripts(function() {
        init(function() {
            setSettings();
            $("#event").click(function(){
                addEvent();
            });

            console.log('Initilized the guest server connection...');

            window.socket.on('id', function(id) {
                console.log('Receive id: ' + id);
                window.sessionId = id;
                window.register(); //Join as viewer.
            });

            window.socket.on('message', function(message) {
                console.log('received : ' + message);
                switch (message.id) {
                    case "registered":
                        console.log('Registered to Guest Server.');
                        if (message.weddingStarted) {
                            console.log('Wedding is started, streams are available.');
                            window.streams = message.weddingStreams;
                            console.log('Currently have : ' + message.count + ' viewers.');
                            window.updateViewercount(message.count);
                            setTimeout(function() {
                                alert('The wedding has began, if you would like to begin watching this wedding please tap the heart below.');
                                window.weddingReady = true;
                            }, 15000); //Delay 15seconds
                        }
                        window.updateViewercount(message.count);
                        break;

                    case "weddingStarted":
                        console.log('Wedding is starting, streams are available.');
                        window.streams = message.weddingStreams;
                        console.log('Currently have : ' + message.count + ' viewers.');
                        window.updateViewercount(message.count);
                        setTimeout(function() {
                            alert('The wedding has began, if you would like to begin watching this wedding please tap the heart below.');
                            window.weddingReady = true;
                        }, 15000); //Delay 15seconds
                        break;

                    case "weddingStopped":
                        console.log('Wedding is stopped, streams are considered inactive.');
                        window.streams = {};
                        window.disregardVideoStreams();
                        break;

                    case "viewerCount":
                        console.log('Currently have: ' + message.count + ' number of viewers.');
                        window.updateViewercount(message.count);
                        break;

                    case "messagePosted":
                        window.messagePosted(message.from, message.text);
                        break;

                    case "surpriseProposal":
                        //Show proposal modal
                        break;

                    default:
                        break;
                }
            });

            alert('You will be notified when this wedding is ready.');
            document.getElementById("video_container").style.opacity = 0;
            hideModals();
        });
    });
}

function failureGuestLogin(data) {
    alert('Incorrect login, please try again. If you continue to receive this message email info@webwedmobile.com.');
};

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
};

function init(callback) {
    window.socket = io.connect('http://endpoint.webwedmobile.com:7999/');

    window.onbeforeunload = function() {
        window.socket.disconnect();
    };

    callback();
};

function loadScripts(callback) {
    loadScript('http://endpoint.webwedmobile.com:7999/socket.io/socket.io.js', function() {
        callback();
    });
};

function messagePosted(from, body) {
    $('.messages').append('<li class="message left appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">' + body + '</div></div></li>');
};

function register() {
    window.sendMessage({
        id: 'register',
        wedding: JSON.parse(getCookie('wedding')).code,
        name: JSON.parse(getCookie('name'))
    });
};

/*function disregardVideoStreams() {
    alert('It appears someone in this wedding has left, we will let you know once the wedding is available again!');
};*/

function updateViewercount(count) {
    window.viewerCount = count;
};

function joinWeddingViaButton() {
    if (window.weddingReady) {
        window.renderVideoStreams();
    } else {
        alert('The wedding does not seem to be available just yet, you will be notified when it is available to be joinned.');
    }
};

function goChat() {
    toggle_visibility('chatWindow');
};

function disregardVideoStreams() {
    document.getElementById('youVideoElement').src = '';
    document.getElementById('yoursVideoElement').src = '';
    document.getElementById('witness1VideoElement').src = '';
    document.getElementById('witness2VideoElement').src = '';
    document.getElementById('officalVideoElement').src = '';
    alert('It appears someone in this wedding has left, we will let you know once the wedding is available again!');
};

function renderVideoStreams() {
    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
    var isFirefox = typeof InstallTrigger !== 'undefined';
    // At least Safari 3+: "[object HTMLElementConstructor]"
    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // Internet Explorer 6-11
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    // Chrome 1+
    var isChrome = !!window.chrome && !!window.chrome.webstore;
    // Blink engine detection
    var isBlink = (isChrome || isOpera) && !!window.CSS;

    var index = 0;
    if (isSafari || isFirefox || isChrome || isIE || isOpera) {
        index = 0;
    } else {
        index = 1;
    }

    if (window.streams.yours[index] !== '') {
        var yoursWebLink = window.streams.yours[index];
        document.getElementById('youVideoElement').src = yoursWebLink;
    }

    if (window.streams.yours2[index] !== '') {
        var yoursWebLink = window.streams.yours2[index];
        document.getElementById('yoursVideoElement').src = yoursWebLink;
    }

    if (window.streams.witness[index] !== '') {
        var witness1WebLink = window.streams.witness[index];
        document.getElementById('witnessVideoElement').src = witness1WebLink;
    }

    if (window.streams.witness2[index] !== '') {
        var witness2WebLink = window.streams.witness2[index];
        document.getElementById('witness2VideoElement').src = witness2WebLink;
    }

    if (window.streams.official[index] !== '') {
        var officialWebLink = window.streams.official[index];
        document.getElementById('officalVideoElement').src = officialWebLink;
    }
};

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

$('#loginBtn').click(function() {
    var email = document.getElementById('loginEmail').value;
    var code = document.getElementById('loginCode').value;
    code = code.toUpperCase();
    login(email, code);
});

$('#chat').click(function() {
    goChat();
});

$("#share").click(function() {
    var text = $("#messagetoshare").val();
    $("#messagetoshare").val('');
    sendChatMessage(text);
});

$("#messagetoshare").on("keypress", function(e) {
    if (e.keyCode == 13) {
        $("#share").click();
    }
});

function showHideVideoContainer() {
    if (
        $('#video_container').css('opacity') == '0' ||
        $("#video_container").css('opacity') == 0
    ) {
        $('#video_container').css('opacity', 1);
        document.getElementById('officalVideoElement').play();
    } else {
        $('#video_container').css('opacity', 0);
        document.getElementById('officalVideoElement').pause();
    }
}

window.onblur = function() {
    window.blurred = true;
};

window.onfocus = function() {
    window.blurred = false;
};

// app.initialize();


/*$(function () {
    $('.expander').simpleexpand();
    prettyPrint();
});*/

window.blurred = false;

if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) //IF IE > 10
{} else {
    var love = setInterval(function() {
        if (!blurred){
            var r_num = Math.floor(Math.random() * 10) + 1;
            var r_size = Math.floor(Math.random() * 35) + 10;
            var r_left = Math.floor(Math.random() * 80) + 1;
            var r_bg = Math.floor(Math.random() * 15) + 100;
            var r_time = Math.floor(Math.random() * 5) + 5;

            $('.bg_heart').append("<div class='heart' style='width:" + r_size + "px;height:" + r_size + "px;left:" + r_left + "%;background:#023a7b;-webkit-animation:love " + r_time + "s ease;-moz-animation:love " + r_time + "s ease;-ms-animation:love " + r_time + "s ease;animation:love " + r_time + "s ease'></div>");

            $('.bg_heart').append("<div class='heart' style='width:" + (r_size - 10) + "px;height:" + (r_size - 10) + "px;left:" + (r_left + r_num) + "%;background:#023a7b;-webkit-animation:love " + (r_time + 5) + "s ease;-moz-animation:love " + (r_time + 5) + "s ease;-ms-animation:love " + (r_time + 5) + "s ease;animation:love " + (r_time + 5) + "s ease'></div>");

            $('.heart').each(function() {
                var top = $(this).css("top").replace(/[^-\d\.]/g, '');
                var width = $(this).css("width").replace(/[^-\d\.]/g, '');
                if (top <= -100 || width >= 150) {
                    $(this).detach();
                }
            });
        }
    }, 1500);
}



$('a').tooltip();
//@ sourceURL=pen.js

$(function() {
    setTimeout(function() {
        return $(".bar").animate({
            height: "toggle"
        }, "slow")
    }, 0);
    return $("#ok").on("click", function() {
        $("#barwrap").css("margin-bottom", "0px");
        $(".bar").animate({
            height: "toggle"
        }, "slow");
        return !1
    })
});

$(function() {

    $('.toggle').click(function(event) {
        event.preventDefault();
        var target = $(this).attr('href');
        $(target).toggleClass('show');
    });

});

function toggle_visibility(id) {
    var e = document.getElementById(id);
    if (e.style.opacity == 100){
        e.style.opacity = 0;
        e.style.display = 'none';
    }else{
        e.style.opacity = 100;
        e.style.display = 'block';
    }
}

//*Turn sound on or off for the active demo or wedding*//
function mute(){
	if ($('#icon_sound').hasClass('fa-volume-off')) {
        $('#icon_sound').removeClass('fa-volume-off');
        $('#icon_sound').addClass('fa-volume-up');
        document.getElementById('youVideoElement').muted = false;
        document.getElementById('yoursVideoElement').muted = false;
        document.getElementById('officalVideoElement').muted = false;
        document.getElementById('witness1VideoElement').muted = false;
        document.getElementById('witness2VideoElement').muted = false;
    } else {
        $('#icon_sound').removeClass('fa-volume-up');
        $('#icon_sound').addClass('fa-volume-off');
        document.getElementById('youVideoElement').muted = true;
        document.getElementById('yoursVideoElement').muted = true;
        document.getElementById('officalVideoElement').muted = true;
        document.getElementById('witness1VideoElement').muted = true;
        document.getElementById('witness2VideoElement').muted = true;
    }
}

$('.icon-heart').on('click', function() {
    $('.yellow').toggleClass('active');
    $('.blue').toggleClass('active');
    if ($('.fav').hasClass('active')) {
        $('.fav').removeClass('active  icon-animation').addClass('unlike');
    } else {
        $('.fav').removeClass('unlike').addClass('active icon-animation');

    }

    if (name != '' && token != '' && wedding != '' && settings != ''){
    	alert('Please wait until the wedding has began.');
	}else{
		showHideVideoContainer();
	}
})


var frameSrc = "/login";

$('#gift').click(function() {
    $('#wwRegister').on('show', function() {

        $('iframe').attr("http://www.myregistry.com", frameSrc);

    });
    $('#wwRegister').modal({
        show: true
    });
});

function displayIFrameModal(src){
    document.getElementById('idFrame').src = src;
    hideModals();
    $("#account-modal").modal({
        show: true
    });
    addModalBackdropHide();
    var e = document.getElementById('chatWindow');
    e.style.opacity = 0;
    e.style.display = 'none';
};

function hideModals(){
    $('.modal').modal('hide');
}

function hideLoginModal(){
    $('#sign-in-modal').modal('hide');
}


$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

$(document).ready(function() {
    var scroll_start = 0;
    var startchange = $('#rsvpLink');
    var offset = startchange.offset();
    if (startchange.length) {
        $(document).scroll(function() {
            scroll_start = $(this).scrollTop();
            if (scroll_start < offset.top) {
                $(".navbar").css('background-color', '#023a7b)');
            }
        });
    }

    if (getParameterByName('id')) {
        if (getParameterByName('email')){  
            login(getParameterByName('email'), getParameterByName('id'));
        }else{
            login('info@webwedmobile.com', getParameterByName('id'));
        }
    }

    //toggle_visibility('guest_login');

    $(".modal-backdrop").click(function(){ hideModals(); });
});
    
setInterval(function(){
    if ($(".modal-backdrop").length > 0){
        addModalBackdropHide();
    }
}, 500);

