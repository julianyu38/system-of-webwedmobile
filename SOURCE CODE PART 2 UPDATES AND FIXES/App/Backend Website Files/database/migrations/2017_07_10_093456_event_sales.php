<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventSales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('event_sales', function (Blueprint $table) {
          $table->increments('id');
          $table->string('uid');
          $table->integer('event')->unique();
          $table->double('price_paid');
          $table->string('txn_id');
          $table->string('coupon_code');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_sales');
    }
}
