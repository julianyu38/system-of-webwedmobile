<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FirebaseUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('firebase_users', function (Blueprint $table) {
          $table->increments('id');
          $table->string('uid');
          $table->string('first_name');
          $table->string('last_name');
          $table->string('email')->unique();
          $table->string('phone');
          $table->string('roles');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('firebase_users');
    }
}
