<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Laravel CORS
     |--------------------------------------------------------------------------
     |
     | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
     | to accept any value.
     |
     */
     'supportsCredentials' => true,
     'allowedOrigins' => ['http://localhost:8101', 'https://localhost:8101', '*', 'http://192.168.1.223', 'http://192.168.223:8100'],
     'allowedHeaders' => ['Origin', 'Content-Type', 'X-Auth-Token', 'X-Requested-Auth', 'Authorization', 'X-XSRF-TOKEN'],
     'allowedMethods' => ['GET', 'POST', 'PUT',  'DELETE'],
     'exposedHeaders' => [],
     'maxAge' => 0,
     'hosts' => [],
];
