<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'firebase' => [
        'api_key' => 'AIzaSyBHLBBX-AwKAfE2VX2K1twMo14QuUh86F8', // Only used for JS integration
        'auth_domain' => 'playground-dd1d8.firebaseapp.com', // Only used for JS integration
        'database_url' => 'https://playground-dd1d8.firebaseio.com',
        'secret' => '3XySAMNG71GwtHvjcd7GR5TaVtwpWiy6Pz89KrCm',
        'storage_bucket' => 'playground-dd1d8.appspot.com', // Only used for JS integration
    ]

];
