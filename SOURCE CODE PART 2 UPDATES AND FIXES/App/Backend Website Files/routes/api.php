<?php
ini_set('display_errors',1);
error_reporting(E_ALL);

use Illuminate\Http\Request;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkException;
use Api\PremiumAddons\MarriageEducationHandlerController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::match(['get','post'], '/{t}/{ta?}/{tb?}/{tc?}', function(){
//   return response()->json([
//     'success' => false,
//     'data' => array(
//       'error' => array(
//         'title' => 'Under Maintenance',
//         'message' => 'App is under maintenance, please try again later.'
//       )
//     ),
//     'error' => array(
//       'title' => 'Under Maintenance',
//       'message' => 'App is under maintenance, please try again later.'
//     )
//   ]);
// });

Route::match(['get','post'], '/auth/login', 'Api\NewAuthenticationController@login')->middleware('cors', 'cors-preflight');

Route::match(['get', 'post'], '/', function(Request $request){
  echo 'WEB WED MOBILE API V2';
});

// All Event Routes

Route::get('/event/config', 'Api\EventController@getEventConfigs')->middleware('cors', 'cors-preflight');
Route::post('/event/create', 'Api\EventController@createEvent')->middleware('api-auth', 'cors', 'cors-preflight');
Route::put('/event/create', 'Api\EventController@createEvent')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/send_invite', 'Api\EventController@configureInvite')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/{id}/invite', 'Api\EventController@inviteGuests')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/{id}/invite/{response}', 'Api\EventController@respondToInvite')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/{id}/{type?}', 'Api\EventController@getEvent')->middleware('cors', 'cors-preflight');
Route::get('/public/{id}/{privacy?}', 'Api\EventController@getEventPrivacy')->middleware('cors', 'cors-preflight');
Route::post('/event/{id}/participants/add', 'Api\EventController@addParticipant')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/participants/{participantId}/update', 'Api\EventController@updateParticipant')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/participants/{participantId}/remove', 'Api\EventController@removeParticipant')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/event/{id}/guests/update', 'Api\EventController@updateGuests')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/event/share/{id}', function(){ return view('welcome'); })->middleware('cors', 'cors-preflight');
Route::get('/start/{id}/{userId}', 'Api\EventController@startEvent')->middleware('cors', 'cors-preflight');
Route::get('/archive/{id}', 'Api\EventController@getArchive')->middleware('cors', 'cors-preflight');
Route::get('/stream/{id}', 'Api\EventController@getMinutes')->middleware('cors', 'cors-preflight');
Route::get('/stop_stream/{id}/{userId}', 'Api\EventController@stopEvent')->middleware('cors', 'cors-preflight');
Route::get('/emojis', 'Api\EventController@getEmojis')->middleware('cors', 'cors-preflight');
Route::get('/event/{id}/stream', 'Api\EventController@streamEvent')->middleware('cors', 'cors-preflight');
Route::post('/officiant/invite', 'Api\EventController@inviteOfficiant')->middleware('cors', 'cors-preflight');

//Webhooks
Route::post('/webhooks/opentok', 'Api\EventController@opentokWebhook')->middleware('cors', 'cors-preflight');

//Upload handler
Route::post('/upload/file', 'Api\EventController@handleFileUpload')->middleware('cors', 'cors-preflight');

//All User Routes

Route::get('/sample_broadcast', 'Api\EventController@broadcast')->middleware('cors', 'cors-preflight');
Route::get('/user/{id}', 'Api\NewUserController@getUser')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/events', 'Api\EventController@getEvents')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/orders', 'Api\OrderController@getOrders')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/notifications', 'Api\NewUserController@getNotifications')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/notification/{notificationid?}/{type?}', 'Api\NewUserController@getNotifications')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/user/{id}/avatar', 'Api\NewUserController@getAvatar')->middleware('cors', 'cors-preflight');
Route::get('/officiants', 'Api\NewUserController@listOfficiants')->middleware('cors', 'cors-preflight');
Route::post('/myaccount/update', 'Api\NewUserController@updateAccount')->middleware('api-auth', 'cors', 'cors-preflight');

/* OLD MARRIAGE APPLICATION / LICENSE */
Route::get('/marriage/license/{event}', 'Api\PremiumAddons\MarriageApplicationHandlerController@history')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/marriage/license/{event}', 'Api\PremiumAddons\MarriageApplicationHandlerController@history')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/marriage/application', 'Api\PremiumAddons\MarriageApplicationHandlerController@handle')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/marriage/application/submit', 'Api\PremiumAddons\MarriageApplicationHandlerController@submitApplication')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/marriage/application/attachment/add', 'Api\PremiumAddons\MarriageApplicationHandlerController@handleAttachment')->middleware('api-auth', 'cors', 'cors-preflight');

Route::post('/marriage/education/submit', 'Api\PremiumAddons\MarriageEducationHandlerController@handle')->middleware('api-auth', 'cors', 'cors-preflight');
Route::get('/marriage/education/retrive/{name}', 'Api\PremiumAddons\MarriageEducationHandlerController@retrive')->middleware('cors', 'cors-preflight');

Route::get('/marriage/registry/{event}', 'Api\PremiumAddons\MarriageRegistryHandlerController@handle')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/marriage/registry/{event}', 'Api\PremiumAddons\MarriageRegistryHandlerController@save')->middleware('api-auth', 'cors', 'cors-preflight');

Route::get('/marriage/courts/{event}', 'Api\PremiumAddons\MarriageApplicationHandlerController@courts')->middleware('api-auth', 'cors', 'cors-preflight');
Route::post('/marriage/courts/{event}', 'Api\PremiumAddons\MarriageApplicationHandlerController@selectCourt')->middleware('api-auth', 'cors', 'cors-preflight');
/* NEW MARRIAGE APPLICATION / LICENSE */

Route::get('/addons/{addon}', 'Api\AddonHandlerController@handle');
Route::post('/addons/{addon}', 'Api\AddonHandlerController@handle');
