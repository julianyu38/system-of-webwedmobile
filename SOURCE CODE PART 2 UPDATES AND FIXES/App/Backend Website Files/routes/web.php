<?php

use Illuminate\Support\Facades\Log;
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Headers: *');
// header('Access-Control-Allow-Methods: *');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/invoice/{invoice_id}', function() {
//   return view('invoice');
// });
//
// Route::post('/invoice', function(Request $request){
//   return dd($request);
//   return view('invoice_paid');
// });
//
// Route::get('/pay/{token}', function($token, Request $request){
//   Log::info('TOKEN INFORMATION: '.$token);
//   return view('invoice_paid');
// });

// --------------------
// Backpack\Demo routes
// --------------------
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['admin'],
    'namespace'  => 'Admin',
], function () {
    // CRUD resources and other admin routes
    CRUD::resource('monster', 'MonsterCrudController');
    CRUD::resource('user', 'FirebaseUsersCrudController');
    // CRUD::resource('admins', 'UserCrudController');
    CRUD::resource('officiants', 'OfficiantsCrudController');
    CRUD::resource('marriageapplication', 'MarriageApplicationCrudController');
    CRUD::resource('marriagelicense', 'MarriageLicenseCrudController');
    CRUD::resource('coupons', 'CouponsCrudController');

    CRUD::resource('events', 'EventsCrudController');
    CRUD::resource('event_options', 'Event_optionsCrudController');

    CRUD::resource('event_sales', 'Event_salesCrudController');
    CRUD::resource('preimum_addon_sales', 'Preimum_addon_salesCrudController');

    CRUD::resource('packages', 'PackagesCrudController');
    CRUD::resource('preimum_addons', 'Premium_addonsCrudController');

    CRUD::resource('emojis', 'EmojisCrudController');
    CRUD::resource('officiant_questions', 'OfficiantQuestionsCrudController');
    CRUD::resource('officiantquestions', 'OfficiantQuestionsCrudController');
    CRUD::resource('courts', 'CourtsCrudController');
    CRUD::resource('eventroles', 'EventRolesCrudController');

});

// Route::get('admin/{test}', function(){
//   return view('maintaince');
// });

Route::get('api/article', 'Api\ArticleController@index');
Route::get('api/article/{id}', 'Api\ArticleController@show');

Route::get('email/view_video/{url}', function($url){
  return view('view_video', [
    'url' => urldecode(base64_decode($url))
  ]);
});
