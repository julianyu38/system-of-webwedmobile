<?php

namespace Vinkas\Firebase\Auth;

class User extends Authenticatable
{
    public $table = 'firebase_users';

    // protected $events = [
    //   'created' => '',
    //   'updated' => '',
    //   'deleted' => ''
    // ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uid', 'first_name', 'last_name', 'email', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
