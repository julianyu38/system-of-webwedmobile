<?php

namespace Vinkas\Firebase\Auth;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Auth;

/**
* Class AuthenticatesUsers.
*/
trait AuthenticatesUsers
{

  public function getAuth(Request $request) {
    return view('vinkas.firebase.auth');
  }

  public function postAuth(Request $request) {
    $data = $request->all();
    //$validator = $this->validator($data);
    //if ($validator->fails())
    //return $this->onFail($validator->errors()->first());

    JWT::$leeway = 8;
    $content = file_get_contents("https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com");
    $kids = json_decode($content, true);
    if ($request->has('id_token')){
      $id_token = $request->input('id_token');
    }else{
      $id_token = $request->header('X-Requested-Auth');
    }

    if ($id_token == '' || $id_token == null){
      return $this->onFail('No Access Token Provided.');
    }

    $jwt = JWT::decode($id_token, $kids, array('RS256'));
    $fbpid = config('vinkas.firebase.auth.project_id');
    $issuer = 'https://securetoken.google.com/' . $fbpid;
    if($jwt->aud != $fbpid)
    return $this->onFail('Invalid audience');
    elseif($jwt->iss != $issuer)
    return $this->onFail('Invalid issuer');
    elseif(empty($jwt->sub))
    return $this->onFail('Invalid user');
    else {
      if (@$jwt->email != ""){
        $email = $jwt->email;
      }else{
        $email = "";
      }

      if (@$jwt->name != ""){
        $name = $jwt->name;
      }else{
        $name = "";
      }

      $uid = $jwt->sub;
      $user = $this->firebaseLogin($uid, $request, $id_token, $email, $name);

      if($user)
      return array('success' => true, 'user' => $user);
      else
      return $this->onFail('Error');
    }
  }

  protected function onFail($message) {
    return array('success' => false, 'message' => $message);
  }

  protected function firebaseLogin($uid, $request, $id_token, $email = "", $name = "") {
    $user = Auth::getProvider()->retrieveById($uid);

    if (is_null($user)){
      $this->firebaseRegister($uid, $request, $email, $name);
    }else{
      if ($request->has('location')){
        $user->first()->updateAddress(json_decode($request->input('location')));
      }
    }

    $user = Auth::getProvider()->retrieveById($uid);
    if (!is_null($user)){
      //Update token session.
      $user->token = base64_encode($id_token);
      $user->save();
    }

    $remember = $request->has('remember') ? $request->input('remember') : false;
    return Auth::loginUsingId($uid, $remember);
  }

  protected function firebaseRegister($uid, $request, $email = "", $name = "") {
    $data['uid'] = $uid;
    $data['name'] = $request->has('name') ? $request->input('name') : $name;
    $data['first_name'] = $request->has('name') ? explode(' ', @$data['name'])[0] : @$data['name'];
    $data['last_name'] = $request->has('name') ? explode(' ', @$data['name'])[1] : '';
    $data['email'] = $request->has('email') ? $request->input('email') : $email;
    $data['phone'] = $request->has('phone') ? $request->input('phone') : '';
    $data['dob'] = $request->has('dob') ? $request->input('dob') : '';
    $data['location'] = $request->has('location') ? $request->input('location') : '';

    $this->create($data);
  }

}
