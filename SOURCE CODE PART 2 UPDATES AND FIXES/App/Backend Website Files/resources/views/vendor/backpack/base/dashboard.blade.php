@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.dashboard') }}<small>Web Wed Mobile</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
@endsection


@section('content')
<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>0</h3>

          <p>Active Users</p>
        </div>
        <div class="icon">
          <i class="fa fa-heartbeat"></i>
        </div>

      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>0</h3>

          <p> Total Events</p>
        </div>
        <div class="icon">
          <i class="fa fa-calendar"></i>
        </div>

      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>0</h3>

          <p>Officiants</p>
        </div>
        <div class="icon">
          <i class="fa fa-university"></i>
        </div>

      </div>
    </div>
    <!-- ./col -->

  </div>
  <div class="row">
    <div class="col-md-12">
        <img src="http://www.riveloper.com/client_portal/webwed-admin/public/images/login/dashboard.png" style="max-width:600px;" />
    </div>
  </div>
@endsection
