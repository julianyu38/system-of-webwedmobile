<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  {{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <title>
    {{ isset($title) ? $title.' :: '.config('backpack.base.project_name').' Admin' : config('backpack.base.project_name').' Admin' }}
  </title>

  @yield('before_styles')

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="{{ asset('vendor/adminlte/') }}/plugins/pace/pace.min.css">
  <link rel="stylesheet" href="{{ asset('vendor/backpack/pnotify/pnotify.custom.min.css') }}">

  <!-- BackPack Base CSS -->
  <link rel="stylesheet" href="{{ asset('vendor/backpack/backpack.base.css') }}">

  @yield('after_styles')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- iCheck -->
  <!-- <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css"> -->
  <style>
  .login-box-body, .register-box-body {
    background: #fff;
    padding: 20px;
    border-top: 0;
    color: #666;
    min-height: 400px;
    justify-content: center;
    vertical-align: middle;
    display: flex;
    flex-direction: column;
    box-shadow: 0px 3px 9px rgb(186, 186, 186);
    text-align: center;
}

.login-page, .register-page {
    background: #c9d3de;
}
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page" style="background-image:url('../images/login/webwedring.svg'); background-repeat: no-repeat; background-position: 120% bottom; background-size: 50%;">
<div class="login-box">
  <div class="login-logo">
    <img src="../images/login/webwedlogo-dark.svg" style="width:180px;">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body" style="min-height:auto;">
  <h3 class="textcenter blue">Want a better experience?</h3>
  <p class="login-box-msg">Download our free app using the links below. Register using your email and phone number today!</p>

    <form role="form" method="POST" action="{{ url(config('backpack.base.route_prefix').'/login') }}">

      <!-- {!! csrf_field() !!}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input name="email" type="email" class="form-control" placeholder="Admin Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-faeedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div> -->
      <div class="row">


        <!-- /.col -->
        <div class="col-xs-12">
          <!-- <button type="submit" class="btn btn-primary btn-block btn-flat">Download the Free App</button> -->
          <a href="https://itunes.apple.com/us/app/webwed-mobile/id1086176524?mt=8"><img src="https://www.halstead.com/app/assets/img/creative/AppStore.png" style="width:230px;"/></a><br/>
          <a href="https://play.google.com/store/apps/details?id=com.webwed.android&hl=en"><img src="https://www.ft.com/__origami/service/image/v2/images/raw/https%253A%252F%252Fwww.ft.com%252F__assets%252Fcreatives%252Ftour%252Fapps%252Fgoogle-play-badge-3x.png?source=next-tour-page" style="margin-top:5px; width:230px;"/></a>
        </div>
        <!-- /.col -->
      </div>
    </form>
<br>

    <!-- /.social-auth-links -->

    <!-- <a href="#">Take me to standard user login</a><br> -->


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
