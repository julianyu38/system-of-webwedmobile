<HTML>
<HEAD>
<TITLE>WebWed Mobile Invitation Video</TITLE>
<style>
body
{
  background-color: #90b1cf;
}
.center-div
{
  position: absolute;
  margin: auto;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  min-width: 100px;
  min-height: 100px;

}
</style>
<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
</HEAD>
<BODY>


<video controls="controls" autoplay="autoplay" width="640" height="360">
        <source src="{{$url}}" type="video/mp4" />
        <span title="No video playback capabilities, please download the video below">WebWed Mobile</span>
</video>


<script>
var $video  = $('video'),
    $window = $(window);

$(window).resize(function(){

    var height = $window.height();
    $video.css('height', height);

    var videoWidth = $video.width(),
        windowWidth = $window.width(),
    marginLeftAdjust =   (windowWidth - videoWidth) / 2;

    $video.css({
        'height': height,
        'marginLeft' : marginLeftAdjust
    });
}).resize();
</script>

</BODY>
</HTML>
