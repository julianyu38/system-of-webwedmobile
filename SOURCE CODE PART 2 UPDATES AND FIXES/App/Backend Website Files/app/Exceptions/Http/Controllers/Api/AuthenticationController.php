<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vinkas\Firebase\Auth\AuthenticatesUsers;
use Vinkas\Firebase\Auth\User;

class AuthenticationController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request){

      return dd($this->postAuth($request));
      try {
        $response = json_decode($this->postAuth($request)->data);
        return dd($response);
        if ($response['success']){
          return response()->json(array(
              'success' => true,
              'data' => array()
            )
          );
        }else{
          return response()->json(array(
              'success' => false,
              'message' => $response['message']
            )
          );
        }
      } catch (Exception $e) {
        return dd($e);
      }
    }

    public function signup(Request $request){
      return $this->login($request);
    }

    public function create($data){
      $user = User::create([
        'uid' => $data['uid'],
        'first_name' => 'John',
        'last_name' => 'Doe2',
        'email' => 'email@email.com',
        'phone' => '6784678435'
      ]);
      return $user; //dd($data);
    }
}
