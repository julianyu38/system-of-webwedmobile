<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FirebaseUsersRequest as StoreRequest;
use App\Http\Requests\FirebaseUsersRequest as UpdateRequest;

class FirebaseUsersCrudController extends CrudController
{
    private $tabs = [ 'Account Details', 'Officiant Details', 'Events', 'Officating Events', 'Payments' ];

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\FirebaseUsers');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('Account', 'Accounts');
        $this->crud->denyAccess(['create']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->setColumns([
            [
                'name'  => 'first_name',
                'label' => 'First',
            ],
            [
                'name'  => 'last_name',
                'label' => 'Last',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
            ],
            [
                'name'  => 'phone',
                'label' => 'Phone',
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->data['entry'] = $this->crud->getEntry($id);

        // dd($this->crud->getEntry($id)->events());

        // $this->crud->addField([ // image
        //     'label' => "Profile Photo",
        //     'name' => "id",
        //     'type' => 'image',
        //     'upload' => true,
        //     'value' => 'https://placehold.it/160x160/00a65a/ffffff/&text=J',
        //     'crop' => true, // set to true to allow cropping, false to disable
        //     'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        //     'tab' => $this->tabs[0]
        //     // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
        // ]);

        //User Details Tab
        $this->crud->addField([
            'name' => 'first_name',
            'label' => "First Name",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'last_name',
            'label' => "Last Name",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => "Email",
            'type' => 'email',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'label' => "Phone",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'password',
            'fake' => true,
            'label' => "Password",
            'type' => 'password',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'confirm_password',
            'fake' => true,
            'label' => "Confirm Password",
            'type' => 'password',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'address',
            'fake' => true,
            'label' => "Address",
            'type' => 'address',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'id3',
            'fake' => true,
            'label' => "Enable Officiant",
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'id',
            'fake' => true,
            'label' => "Enable Officiant",
            'type' => 'select_from_array',
            'options' => [0 => 'Disabled', 1 => 'Enabled'],
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
            'tab' => $this->tabs[0]
        ]);

        // $this->crud->addField([
        //     'name' => 'id5',
        //     'fake' => true,
        //     'type' => 'custom_html',
        //     'value' => '<div class="form-group col-md-10"> <a href="" class="btn btn-xs btn-default"><i class="fa fa-gear"></i></a> </div>' ,
        //
        //     'tab' => $this->tabs[0]
        // ]);

        // $this->crud->addField(
        //   [
        //      // MANDATORY
        //     'name'  => 'push_notifications', // DB column name (will also be the name of the input)
        //     'fake' => true,
        //     'label' => 'Push Notifications', // the human-readable label for the input
        //     'type'  => 'select_from_array', // the field type (text, number, select, checkbox, etc)
        //     'options' => [0 => 'Disabled', 1 => 'Enabled'],
        //
        //      'wrapperAttributes' => [
        //        'class' => 'form-group col-md-6'
        //      ],
        //      'tab' => $this->tabs[0]
        //   ]
        // );
        //
        // $this->crud->addField(
        //   [
        //      // MANDATORY
        //     'name'  => 'email_notifications', // DB column name (will also be the name of the input)
        //     'fake' => true,
        //     'label' => 'Email Notifications', // the human-readable label for the input
        //     'type'  => 'select_from_array', // the field type (text, number, select, checkbox, etc)
        //     'options' => [0 => 'Disabled', 1 => 'Enabled'],
        //
        //      'wrapperAttributes' => [
        //        'class' => 'form-group col-md-6'
        //      ],
        //      'tab' => $this->tabs[0]
        //   ]
        // );

        // $this->crud->addField([
        //     'label'     => 'Roles',
        //     'type'      => 'checklist',
        //     'name'      => 'admin',
        //     'entity'    => 'roles',
        //     'attribute' => 'name',
        //     'model'     => "Backpack\PermissionManager\app\Models\Role",
        //     'pivot'     => true,
        //     'tab' => $this->tabs[0]
        // ]);

        //Officiant Details Tab

        // $this->crud->addField([
        //     'name' => 'status',
        //     'fake' => true,
        //     'label' => "Status",
        //     'type' => 'select2_from_array',
        //     'options' => ['inactive' => 'Inactive','active' => 'Active'],
        //     'allows_null' => false,
        //     'tab' => $this->tabs[1]
        // ]);







        //Events Tab


        $eventsTable = '
          <table class="table datatable">
            <thead>
              <tr>
                <th>Event</th>
                <th>Payment</th>
                <th>Scheduled</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><a href="'.url("admin/events/1/edit").'">Our Wedding</a></td>
                <td><span class="label label-success">Paid 7/11/2017</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
            </tbody>
          </table>
        ';

        $this->crud->addField([
            'name' => 'events',
            'fake' => true,
            'label' => "Events",
            'type' => 'custom_html',
            'value' => $eventsTable,
            'tab' => $this->tabs[2]
        ]);


        //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
