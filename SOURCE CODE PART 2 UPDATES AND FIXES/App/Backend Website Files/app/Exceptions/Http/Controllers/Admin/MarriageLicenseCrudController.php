<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MarriageLicenseRequest as StoreRequest;
use App\Http\Requests\MarriageLicenseRequest as UpdateRequest;

class MarriageLicenseCrudController extends CrudController
{
    public $tabs = [ 'Signatures', 'Documents', 'Manage' ];
    public function setup()
    {


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\MarriageLicense');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/marriagelicense');
        $this->crud->setEntityNameStrings('Marriage License', 'Marriage Licenses');
        $this->crud->denyAccess(['list']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $manageDocuments = '
        <div class="modal fade" id="addDocumentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Add Document</h3>
              </div>
              <div class="modal-body">

                <div class="form-group">
                  <label>Title</label>
                  <input type="text" class="form-control"/>
                </div>
                <div class="form-group">
                  <label>File</label>
                  <input type="file"/>
                </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        ';

        $manageCourt = '
        <div class="modal fade" id="courtModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Select a Court</h3>
              </div>
              <div class="modal-body">
                <p>Type the courts name, email address, or phone number.</p>
                <input type="text" class="form-control" />
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        ';

        // $this->crud->setFromDb();

        $this->crud->addField([
          'name' => 'marriageapplication',
          'fake' => true,
          'type' => 'custom_html',
          'value' => '<a href="#">View Marriage Application</a>',
          'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
          'name' => 'id1',
          'fake' => true,
          'type' => 'custom_html',
          'value' => '
          <table class="table datatable">
            <thead>
              <tr>
                <th>Account</th>
                <th>Response</th>
                <th>Date & Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
            </tbody>
          </table>
          ',
          'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
          'name' => 'id2',
          'fake' => true,
          'type' => 'custom_html',
          'value' => $manageDocuments.'

          <a onClick="event.preventDefault();" data-toggle="modal" data-target="#addDocumentModal">Add Document</a>

          <table class="table datatable">
            <thead>
              <tr>
                <th>Document</th>
                <th>File</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Birth Certificate</td>
                <td><a href="#">Download</a></td>
              </tr>
              <tr>
                <td>Government ID</td>
                <td><a href="#">Download</a></td>
              </tr>
              <tr>
                <td>Divorce Decree</td>
                <td><a href="#">Download</a></td>
              </tr>
              <tr>
                <td>Proof of Residency</td>
                <td><a href="#">Download</a></td>
              </tr>
              <tr>
                <td>Blood Test</td>
                <td><a href="#">Download</a></td>
              </tr>
              <tr>
                <td>Marriage Education</td>
                <td><a href="#">Download</a></td>
              </tr>

            </tbody>
          </table>
          ',
          'tab' => $this->tabs[1]
        ]);

        $manageCourtHtml = $manageCourt.'

        <div class="row">
          <div class="col-md-3">
            <p><b><a onClick="event.preventDefault();" data-toggle="modal" data-target="#courtModal">Court</a></b></p>
          </div>
          <div class="col-md-9">
            <p><a href="'.url("admin/courts/1/edit").'">Gwinnett County & Admin</a></p>
          </div>
        </div>
        ';

        $this->crud->addField([
          'name' => 'Court',
          'fake' => true,
          'type' => 'custom_html',
          'value' => $manageCourtHtml,
          'tab' => $this->tabs[2]
        ]);

        $this->crud->addField([
          'name' => 'submittocourt',
          'fake' => true,
          'type' => 'custom_html',
          'value' => '<button onClick="event.preventDefault()" class="btn btn-primary btn-block">Submit to Court</button>',
          'tab' => $this->tabs[2]
        ]);

        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
