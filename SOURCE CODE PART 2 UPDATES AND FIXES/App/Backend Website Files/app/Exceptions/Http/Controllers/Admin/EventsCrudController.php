<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EventsRequest as StoreRequest;
use App\Http\Requests\EventsRequest as UpdateRequest;
use Illuminate\Support\Facades\Auth;

class EventsCrudController extends CrudController
{
    public $tabs = ['Details', 'Event Actions', 'Participants', 'Messages', 'Chat', 'Premium Addons', 'Payment'];
    public $court_tabs = ['Participants', 'Legal Documents'];

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Events');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/events');
        

        if (Auth::User()->hasRole('Admin')){
          $this->crud->denyAccess(['create']);
          $this->crud->setEntityNameStrings('Event', 'Events');
        }else{
          $this->crud->denyAccess(['create', 'delete']);
          $this->crud->setEntityNameStrings('Application', 'Applications');
        }

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();
        if (Auth::User()->hasRole('Admin')){
          $this->crud->setColumns([
              [
               'name' => 'title', // The db column name
               'label' => "Title", // Table column heading
               'type' => 'Text'
              ],
              [
               'name' => 'event_code', // The db column name
               'label' => "ID", // Table column heading
               'type' => 'Text'
              ],
              [
                'label' => "User", // Table column heading
                'type' => "model_function",
                'function_name' => 'getUserProfile', // the method in your Model
              ],
              [
                 'label' => "Date", // Table column heading
                 'type' => "text",
                 'name' => 'scheduled_date'
              ],
              [
                 'label' => "Status", // Table column heading
                 'name' => 'status',
                 'type' => "boolean",
                 'options' => [0 => 'Inactive', 1 => 'Active']
              ]
          ]);
      }else{
        $this->crud->setColumns([
            [
              'label' => "User", // Table column heading
              'type' => "model_function",
              'function_name' => 'getUserProfile2', // the method in your Model
            ],
            [
               'label' => "Date", // Table column heading
               'type' => "text",
               'name' => 'scheduled_date'
            ]
        ]);
      }

        // dd($this->crud->model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
      $this->data['entry'] = $this->crud->getEntry($id);

      

      if (Auth::User()->hasRole('Admin')){

        $eventDetails = '
          <div class="row">
            <div class="col-md-3">
              <p><b>Administrator Account</b></p>
            </div>
            <div class="col-md-9">
              <p><a href="'.url("admin/user/1/edit").'">John Doe</a></p>
            </div>
          </div>


          <div class="row">
            <div class="col-md-3">
              <p><b>Event Code</b></p>
            </div>
            <div class="col-md-9">
              <p>12445</p>
            </div>
          </div>
          ';

        $this->crud->addField([
            'name' => 'uid',
            'type' => 'custom_html',
            'value' => $eventDetails,
            'tab' => $this->tabs[0]
        ]);

        // $this->crud->addField([ // image
        //     'label' => "Cover Photo",
        //     'name' => "id",
        //     'type' => 'image',
        //     'upload' => true,
        //     'value' => 'https://placehold.it/160x160/00a65a/ffffff/&text=J',
        //     'crop' => true, // set to true to allow cropping, false to disable
        //     'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        //     'tab' => $this->tabs[0]
        //     // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
        // ]);

        // $this->crud->addField([
        //     'name' => 'event_code',
        //     'type' => 'text',
        //     'label' => 'Code',
        //     'attributes' => [
        //        'readonly' => 'readonly'
        //      ],
        //      'wrapperAttributes' => [
        //         'class' => 'form-group col-md-6'
        //       ],
        //     'tab' => $this->tabs[0]
        // ]);

        $this->crud->addField([
            'name' => 'title',
            'type' => 'text',
            'label' => 'Title',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
          'name' => 'scheduled_date',
           'type' => 'date_picker',
           'label' => 'Date',
           // optional:
           'date_picker_options' => [
              'todayBtn' => true,
              'format' => 'mm/dd/yyyy',
              'language' => 'en'
           ],
           'wrapperAttributes' => [
              'class' => 'form-group col-md-6'
            ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
          'name' => 'scheduled_date2',
          'fake' => true,
           'type' => 'time',
           'label' => 'Time',
           'wrapperAttributes' => [
              'class' => 'form-group col-md-6'
            ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'package', // DB column name (will also be the name of the input)
            'label' => 'Package', // the human-readable label for the input
            'type'  => 'select_from_array', // the field type (text, number, select, checkbox, etc)
            'options' => [0 => '15 Minutes', 1 => '30 Minutes', 2 => '45 Minutes'],

             'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
          ]
        );

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'status2', // DB column name (will also be the name of the input)
            'label' => 'Privacy', // the human-readable label for the input
            'fake' => true,
            'type'  => 'select_from_array', // the field type (text, number, select, checkbox, etc)
            'options' => [0 => 'Private', 1 => 'Public'],

             'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
          ]
        );

        $remainingTime = '<div class="row">
          <div class="col-md-3">
            <p><b>Remaining Time</b></p>
          </div>
          <div class="col-md-9">
            <p><span class="label label-success">+15 Minutes</span></p>
          </div>
        </div>';

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'remaining_time', // DB column name (will also be the name of the input)
            'label' => 'Privacy', // the human-readable label for the input
            'fake' => true,
            'type'  => 'custom_html', // the field type (text, number, select, checkbox, etc)
            'value' => $remainingTime,
             'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'tab' => $this->tabs[0]
          ]
        );

        // $eventControls = '
        //
        //
        // <div class="row">
        //   <div class="col-md-12">
        //     <div class="callout callout-danger">
        //       <h4>Wait!</h4>
        //
        //       <p>These actions can not be undone.</p>
        //     </div>
        //   </div>
        // </div>
        // <br/>
        // <div class="row">
        //   <div class="col-md-12">
        //     <button disabled="disabled" class="btn btn-block disabled btn-primary">Stop Event</button>
        //   </div>
        // </div>
        // <br/>
        //
        // <div class="row">
        //   <div class="col-md-12">
        //     <button disabled="disabled" class="btn btn-block disabled btn-primary">Disable Event</button>
        //   </div>
        // </div>
        // <br/>
        //
        //
        //   ';
        //
        // $this->crud->addField([
        //     'name' => 'event_controls',
        //     'fake' => true,
        //     'type' => 'custom_html',
        //     'value' => $eventControls,
        //     'tab' => $this->tabs[1]
        // ]);


      //- [ ] Bride, Groom, Meid of honor, man of honor, bridesmaid, junior bridesmaid, best man, best woman, groomsman, officiant, other, family, friend

      $rolesSelect = '
        <select class="form-control">
          <option>Bride</option>
          <option>Groom</option>
          <option>Maid of Honor</option>
          <option>Man of Honor</option>
          <option>Bridesmaid</option>
          <option>Junior Bridesmaid</option>
          <option>Best Man</option>
          <option>Best Woman</option>
          <option>Groomsman</option>
          <option>Family Friend</option>
          <option>Other</option>
        </select>
      ';

      $participantsTable = '
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3>Select an account</h3>
            </div>
            <div class="modal-body">
              <p>Type the users name, email address, or phone number.</p>
              <input type="text" class="form-control" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->

        <table class="table datatable">
          <thead>
            <tr>
              <th>Account/Contact</th>
              <th>Role</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>'.$rolesSelect.'</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>'.$rolesSelect.'</td>
              <td><button class="btn" data-toggle="modal" data-target="#myModal" onClick="event.preventDefault()"><i class="fa fa-btn fa-pencil-square"></i></button></td>
            </tr>
            <tr>
              <td>info@webwedmobile.com</td>
              <td>'.$rolesSelect.'</td>
              <td><button class="btn" data-toggle="modal" data-target="#myModal" onClick="event.preventDefault()"><i class="fa fa-btn fa-pencil-square"></i></button></td>
            </tr>
            <tr>
              <td>(855) 267-2777</td>
              <td>'.$rolesSelect.'</td>
              <td><button class="btn" data-toggle="modal" data-target="#myModal" onClick="event.preventDefault()"><i class="fa fa-btn fa-pencil-square"></i></button></td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>'.$rolesSelect.'</td>
              <td><button class="btn" data-toggle="modal" data-target="#myModal" onClick="event.preventDefault()"><i class="fa fa-btn fa-pencil-square"></i></button></td>
            </tr>
          </tbody>
        </table>
      ';

      $this->crud->addField([
          'name' => 'options2',
          'type' => 'custom_html',
          'value' => $participantsTable,
          'fake' => true,
          'tab' => $this->tabs[2]
      ]);

      // $guestsTable = '
      //   <table class="table datatable">
      //     <thead>
      //       <tr>
      //         <th>Account/Contact</th>
      //         <th>Response</th>
      //       </tr>
      //     </thead>
      //     <tbody>
      //       <tr>
      //         <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
      //         <td><span class="label label-success">Accepted</span></td>
      //       </tr>
      //       <tr>
      //         <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
      //         <td><span class="label label-success">Accepted</span></td>
      //       </tr>
      //       <tr>
      //         <td>info@webwedmobile.com</td>
      //         <td><span class="label label-warning">Pending</span></td>
      //       </tr>
      //       <tr>
      //         <td>(855) 267-2777</td>
      //         <td><span class="label label-warning">Pending</span></td>
      //       </tr>
      //       <tr>
      //         <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
      //         <td><span class="label label-success">Accepted</span></td>
      //       </tr>
      //     </tbody>
      //   </table>
      // ';
      //
      // $this->crud->addField([
      //     'name' => 'first_name3',
      //     'label' => "Guests",
      //     'type' => 'custom_html',
      //     'value' => $guestsTable,
      //     'fake' => true,
      //     'tab' => $this->tabs[3]
      // ]);

      $chatTable = '
        <table class="table datatable">
          <thead>
            <tr>
              <th>Account</th>
              <th>Message</th>
              <th>Date & Time</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>WebWed Rocks! I am so happy!</td>
              <td>July 10th 2017 1:24 PM</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>This is so awesome! Super happy!</td>
              <td>July 10th 2017 1:20 PM</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>So happy for the lovely couple!</td>
              <td>July 10th 2017 1:18 PM</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>I am so using this for my birthday!</td>
              <td>July 10th 2017 1:14 PM</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>This is so awesome! Super happy!</td>
              <td>July 10th 2017 1:12 PM</td>
            </tr>
          </tbody>
        </table>
      ';

      $chatTable2 = '

                    <!-- DIRECT CHAT -->
                    <!--<div class="box box-warning direct-chat direct-chat-warning">
                      <div class="box-header with-border">
                        <h3 class="box-title">Direct Chat</h3>

                        <div class="box-tools pull-right">
                          <span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                          </button>
                          <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts"
                                  data-widget="chat-pane-toggle">
                            <i class="fa fa-comments"></i></button>
                          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                          </button>
                        </div>
                      </div>-->
                      <!-- /.box-header -->
                      <!--<div class="box-body">-->
                        <!-- Conversations are loaded here -->
                        <div class="direct-chat-messages">
                          <!-- Message. Default to the left -->
                          <div class="direct-chat-msg">
                            <div class="direct-chat-info clearfix">
                              <span class="direct-chat-name pull-left">John Doe</span>
                              <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="https://placehold.it/160x160/00a65a/ffffff/&text=J" alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                              WebWed is unbeliveable!
                            </div>
                            <!-- /.direct-chat-text -->
                          </div>
                          <!-- /.direct-chat-msg -->

                          <!-- Message to the right -->
                          <div class="direct-chat-msg right">
                            <div class="direct-chat-info clearfix">
                              <span class="direct-chat-name pull-right">Jane Doe</span>
                              <span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                            </div>
                            <!-- /.direct-chat-info -->
                            <img class="direct-chat-img" src="https://placehold.it/160x160/00a65a/ffffff/&text=J" alt="message user image">
                            <!-- /.direct-chat-img -->
                            <div class="direct-chat-text">
                              I love you guys!
                            </div>
                            <!-- /.direct-chat-text -->
                          </div>
                          <!-- /.direct-chat-msg -->



                        </div>
                        <!--/.direct-chat-messages-->
                      <!--</div>-->
                      <!-- /.box-body -->
                    <!--</div>-->
                    <!--/.direct-chat -->

      ';

      $this->crud->addField([
          'name' => 'options',
          'type' => 'custom_html',
          'value' => $chatTable,
          'fake' => true,
          'tab' => $this->tabs[3]
      ]);

      $addonDetails = '

      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3>Registry Link</h3>
            </div>
            <div class="modal-body">
              <p>Provide registry link.</p>
              <input type="text" class="form-control" />
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">Save</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->


        <div class="row">
          <div class="col-md-12">
            <p><input type="checkbox" value="true"/> &nbsp; <b><a data-toggle="modal" data-target="#myModal2" onClick="event.preventDefault()">Registry Link</a></b></p>
          </div>
          <!--<div class="col-md-9">
            <p><input type="text" class="form-control" value="http://target.com"/></p>
          </div>-->
        </div>
        <div class="row">
          <div class="col-md-12">
            <p><input type="checkbox" value="on"/> &nbsp; <b><a href="'.url("admin/marriagelicense/1/edit").'">Marriage License</a></b></p>
          </div>
          <!--<div class="col-md-9">
            <p>
              <select class="form-control">
                <option>Atlanta, GA Courthouse</option>
              </select>
            </p>
          </div>-->
        </div>
        <br/>
        <!--table class="table datatable">
          <thead>
            <tr>
              <th>Account/Contact</th>
              <th>Role</th>
              <th>Response</th>
              <th>Response Date</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>Bride</td>
              <td><span class="label label-success">Signed</span></td>
              <td>July 11th, 2017 5:00 PM EST</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>Groom</td>
              <td><span class="label label-success">Signed</span></td>
              <td>July 11th, 2017 5:00 PM EST</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>Officiant</td>
              <td><span class="label label-success">Signed</span></td>
              <td>July 11th, 2017 5:00 PM EST</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>Witness</td>
              <td><span class="label label-warning">Pending</span></td>
              <td>July 11th, 2017 5:00 PM EST</td>
            </tr>
            <tr>
              <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
              <td>Witness</td>
              <td><span class="label label-success">Signed</span></td>
              <td>July 11th, 2017 5:00 PM EST</td>
            </tr>
          </tbody>
        </table-->
      ';

      $this->crud->addField([
          'name' => 'event_code2',
          'fake' => true,
          'type' => 'custom_html',
          'value' => $addonDetails,
          'tab' => $this->tabs[5]
      ]);

      $invoiceDetails = '
      <section class="">
    <!-- info row -->
    <div class="row invoice-info">
      <!-- /.col -->
      <div class="col-sm-12 invoice-col">
        <b>Order ID:</b> 4F3S8J<br>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Description</th>
            <th>Subtotal</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>1</td>
            <td>Event Purchase</td>
            <td>30 Minute Event</td>
            <td>$50.00</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Preimum Addon</td>
            <td>Marriage License</td>
            <td>$150.00</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Preimum Addon</td>
            <td>Registry Link</td>
            <td>$5.00</td>
          </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- accepted payments column -->
      <!-- /.col -->
      <div class="col-xs-12">
        <div class="table-responsive">
          <table class="table">
            <tr>
              <th>Total:</th>
              <td>$169.41</td>
            </tr>
            <tr>
              <th>Status:</th>
              <td><label class="label label-success">Paid 7/11/2017</label></td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


  </section>
      ';

      $this->crud->addField([
          'name' => 'event_code3',
          'fake' => true,
          'type' => 'custom_html',
          'value' => $invoiceDetails,
          'tab' => $this->tabs[6]
      ]);
    }else{
      $marriageLicense = '
      <table class="table datatable">
        <thead>
          <tr>
            <th>Account/Contact</th>
            <th>Role</th>
            <th>Response</th>
            <th>Response Date</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Bride</td>
            <td><span class="label label-success">Signed</span></td>
            <td>July 11th, 2017 5:00 PM EST</td>
          </tr>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Groom</td>
            <td><span class="label label-success">Signed</span></td>
            <td>July 11th, 2017 5:00 PM EST</td>
          </tr>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Officiant</td>
            <td><span class="label label-success">Signed</span></td>
            <td>July 11th, 2017 5:00 PM EST</td>
          </tr>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Witness</td>
            <td><span class="label label-success">Signed</span></td>
            <td>July 11th, 2017 5:00 PM EST</td>
          </tr>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Witness</td>
            <td><span class="label label-success">Signed</span></td>
            <td>July 11th, 2017 5:00 PM EST</td>
          </tr>
        </tbody>
      </table>
      ';
      // $this->crud->addField([
      //     'name' => 'event_code3',
      //     'fake' => true,
      //     'type' => 'custom_html',
      //     'value' => $marriageLicense,
      //     'tab' => $this->court_tabs[0]
      // ]);

      $files = '
      <table class="table datatable">
        <thead>
          <tr>
            <th>Account</th>
            <th>Document</th>
            <th>File</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Birth Certificate</td>
            <td><a href="#">Download</a></td>
          </tr>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
            <td>Marriage License</td>
            <td><a href="#">Download</a></td>
          </tr>
          <tr>
            <td><a href="'.url("admin/user/1/edit").'">Jane Doe</a></td>
            <td>Birth Certificate</td>
            <td><a href="#">Download</a></td>
          </tr>
        </tbody>
      </table>
      ';

      // $this->crud->addField([
      //     'name' => 'event_code4',
      //     'fake' => true,
      //     'type' => 'custom_html',
      //     'value' => $files,
      //     'tab' => $this->court_tabs[1]
      // ]);

      $customHtml = '
        <div class="row text-center">
          <h4 style="font-size: 28px; text-align: center; padding: 7px 10px; margin-top: 0;">
            JOHN DOE AND JANE DOE
          </h4>
          <a href="'.url("images/APPLICATION.pdf").'"><img src="'.url("images/download.svg").'" alt="INSPINIA" style="width: 150px;height: auto;border-radius: 4px;box-shadow: 0 1px 3px rgba(0,0,0,.15);"></a><br/>
          <br/><a href="'.url("images/APPLICATION.pdf").'" class="btn btn-lg btn-primary">
            DOWNLOAD PACKET
          </a><br/><br/>
          <h4 class="text-center" style="margin-top: 0">Applicaton for Marriage Licence Attached</h4>

                                    <p class="text-center">Download this pack and print out a copy. Once reviewed you can select one of the options listed below.</p>
                                    <p class="text-center" style="margin-bottom: 0">
                                        <button class="btn btn-success btn-lg">Approved</button>
                                        <button class="btn btn-danger btn-lg">Rejected</button>
                                    </p>
        </div>

        
        ';

        $this->crud->addField([
          'name' => 'event_code4',
          'fake' => true,
          'type' => 'custom_html',
          'value' => $customHtml,
          // 'tab' => $this->court_tabs[1]
      ]);
    }

      $this->data['crud'] = $this->crud;
      $this->data['saveAction'] = $this->getSaveAction();
      $this->data['fields'] = $this->crud->getUpdateFields($id);
      $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

      $this->data['id'] = $id;

      // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
      return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
