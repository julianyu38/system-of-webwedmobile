<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\OfficiantsRequest as StoreRequest;
use App\Http\Requests\OfficiantsRequest as UpdateRequest;

class OfficiantsCrudController extends CrudController
{

    public $tabs = [ 'Officiant Details', 'Events' ];
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Officiants');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/officiants');
        $this->crud->setEntityNameStrings('Officiant', 'Officiants');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->addField([
            'label' => "User",
            'type' => 'custom_html',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'value' => '<div class="row">
               <div class="col-md-3">
                 <p><b>Account</b></p>
               </div>
               <div class="col-md-9">
                 <p><a href="'.url("admin/user/1/edit").'">John Doe</a></p>
               </div>
             </div>',
             'name' => 'uid', // the db column for the foreign key
             'tab' => $this->tabs[0] // foreign key model
        ]);

        $this->crud->addField([
            'name' => 'active',
            'label' => "Status",
            'type' => 'select_from_array',
            'options' => ['inactive' => 'Inactive','active' => 'Active'],
            'allows_null' => false,
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'address',
            'fake' => true,
            'label' => "Address",
            'type' => 'address',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => 'Officating Fee',
            'fake' => true,
            'type' => 'text',
            'prefix' => '<i class="fa fa-usd"></i>',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'details',
            'fake' => true,
            'label' => "Bio",
            'type' => 'textarea',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'tab' => $this->tabs[0]
        ]);


        $this->crud->addField([
               // Checkbox
              'name' => 'day_m',
              'label' => 'Monday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
               // Checkbox
              'name' => 'day_t',
              'label' => 'Tuesday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
               // Checkbox
              'name' => 'day_w',
              'label' => 'Wednesday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
               // Checkbox
              'name' => 'day_th',
              'label' => 'Thursday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
               // Checkbox
              'name' => 'day_f',
              'label' => 'Friday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);
        $this->crud->addField([
               // Checkbox
              'name' => 'day_s',
              'label' => 'Saturday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
               // Checkbox
              'name' => 'day_ss',
              'label' => 'Sunday',
              'type' => 'checkbox'
            ,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-3'
             ],
             'tab' => $this->tabs[0]
        ]);



        $eventsTable = '
          <table class="table datatable">
            <thead>
              <tr>
                <th>Event</th>
                <th>Scheduled</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><a href="'.url("admin/events/1/edit").'">Our Wedding</a></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
            </tbody>
          </table>
        ';

        $this->crud->addField([
            'name' => 'events',
            'fake' => true,
            'label' => "Events",
            'type' => 'custom_html',
            'value' => $eventsTable,
            'tab' => $this->tabs[1]
        ]);

        $this->crud->setColumns([
            [
                'name'  => 'uid',
                'label' => 'User',
            ],
            [
                'name'  => 'active',
                'label' => 'Active',
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
