<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\Premium_addonsRequest as StoreRequest;
use App\Http\Requests\Premium_addonsRequest as UpdateRequest;

class Premium_addonsCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Premium_addons');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/preimum_addons');
        $this->crud->setEntityNameStrings('Premium Addon', 'Premium Addons');
        $this->crud->denyAccess(['delete', 'create']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'title', // DB column name (will also be the name of the input)
            'label' => 'Title', // the human-readable label for the input
            'type'  => 'text', // the field type (text, number, select, checkbox, etc)

             'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ]
          ]
        );

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'price', // DB column name (will also be the name of the input)
            'label' => 'Price', // the human-readable label for the input
            'type'  => 'number', // the field type (text, number, select, checkbox, etc)

             'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],

             'prefix' => '<i class="fa fa-usd"></i>',
             'suffix' => '.00'
          ]
        );

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'status', // DB column name (will also be the name of the input)
            'label' => 'Status', // the human-readable label for the input
            'type'  => 'select_from_array', // the field type (text, number, select, checkbox, etc)
            'options' => [0 => 'Inactive', 1 => 'Active'],

             'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ]
          ]
        );

        $this->crud->setColumns([
            [
                'name'  => 'title',
                'label' => 'Title',
            ],
            [
                'name'  => 'price',
                'label' => 'Price',
                'type' => 'model_function',
                'function_name' => 'getPrice'
            ],
            [
                'name'  => 'status',
                'label' => 'Status',
                'type' => 'boolean',
                'options' => [0 => 'Inactive', 1 => 'Active']
            ]
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->data['entry'] = $this->crud->getEntry($id);

        //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
