<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserNotifications extends Model
{
    protected $table = 'user_notifications';
    protected $fillable = ['uid','avatar','title','message','timestamp','status'];
}
