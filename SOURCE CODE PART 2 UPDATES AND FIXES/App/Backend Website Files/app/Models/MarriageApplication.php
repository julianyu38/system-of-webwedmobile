<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class MarriageApplication extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'marriage_applications';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['event','uid','court','attachments','history','status'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

   public function getEvent(){
     return @$this->_event->title;
   }

   public function getSpouce(){
     return @$this->_spouce->first_name . ' ' . @$this->_spouce->last_name;
   }

   public function getYour(){
     return @$this->_your->first_name . ' ' . @$this->_your->last_name;
   }

   public function getOfficiant(){
     return @$this->_officiant->first_name . ' ' . @$this->_officiant->last_name;
   }

   public function _spouce(){
     return $this->hasOne('App\Models\Users', 'uid', 'spouce');
   }

   public function _your(){
     return $this->hasOne('App\Models\Users', 'uid', 'uid');
   }

   public function _officiant(){
     return $this->hasOne('App\Models\Users', 'uid', 'officiant');
   }

   public function _event(){
     return $this->hasOne('App\Models\Events', 'id', 'event');
   }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
