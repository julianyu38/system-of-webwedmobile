<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Preimum_addon_sales extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'preimum_addon_saless';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getUserProfile() {
        return '<a href="'.url("admin/users/1/edit").'">John Doe</a>';
    }

    public function getPrice(){
      return '$'.$this->price_paid.'.00';
    }

    public function getEventLink() {
        // $package = $this->package;
        return '<a href="'.url("admin/events/1/edit").'">12445</a>';
    }

    public function getAddonLink() {
        // $package = $this->package;
        return '<a href="'.url("admin/preimum_addons/1/edit").'">Registry Link</a>';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
