<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Users;
use App\Models\Images;
use App\Models\Participants;
use App\Models\OpenTokController;
use Mpociot\Firebase\SyncsWithFirebase;
use Firebase\FirebaseInterface;
use Firebase\FirebaseLib;

class Events extends Model
{
    use CrudTrait;
    use SyncsWithFirebase;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'eventss';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    public $type = 'event';

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getUserProfile() {
        $user = $this->_user;
        //return dd($this);
        // return '';
        return '<a href="'.url("admin/user/".@$this->_user->id."/edit").'">' . @$this->_user->first_name . ' ' . @$this->_user->last_name .'</a>';
    }

    public function getUserProfile2() {
        $user = $this->user;
        return $user->first_name . ' ' . $user->last_name;
    }

    public function formatDate(){
      return '';
    }

    // public function getPackageLink() {
    //     // $package = $this->package;
    //     return '<a href="'.url("admin/packages/1/edit").'">15 Minutes</a>';
    // }

    public function getParticipants(){
      $tmpParticipants = $this->_participants;
      $participants = array();
      foreach ($tmpParticipants as $participant) {
        $user = $participant->_user;
        if (is_null($user)){
          $participants = array_merge($participants, array(
            array('personal' => array(
                  'id' => 0,
                  'avatar' => url(env('APP_URL').'/api/user/'.@explode(' ', $participant->name)[0].'_'.@explode(' ', $participant->name)[1].'/avatar'),
                  'name' => array(
                    'first' => @explode(' ', $participant->name)[0],
                    'last' => @explode(' ', $participant->name)[1],
                    'full' => $participant->name
                  )
                )
              )
            )
          );
        }else{
          $participants = array_merge($participants, array($user->getPublicProfile()));
        }
      }

      return $participants;
    }

    public function getPhoto(){
      $images = Images::find('event'.$this->id);
      if ($images){
        return $images->value;
      }else{
        return 'assets/images/Placeholder.svg';
      }
    }

    /**
     * Makes array of public properties depending on type supplied.
     * If the type is minimal, basic information is supplied,
     * detailed provides a more indepth look at an event.
     * @param  string $type Minimal, or Detailed (Lowercase)
     * @return array       Array containing properties returned.
     */
    public function makeArray($type = 'minimal', $isInvite = false){
      if ($isInvite){
        $typeOfEvent = 'invite';
      }else{
        $typeOfEvent = 'event';
      }
      // return dd($this);
      $details = array(
        'id' => $this->id,
        'date' => ($this->scheduled_date),
        'participants' => $this->getParticipants(),
        'photo' => $this->getPhoto(),
        'title' => $this->title,
        'type' => $typeOfEvent,
        'privacy' => $this->privacy(),
        'status' => $this->status
      );

      if ($typeOfEvent == 'invite'){
        $details['status'] = (new Participants)->getStatusForEvent($user, $event->id);
      }

      if ($type == 'details'){
        //change packages from static, and Premium
        $package = $this->_package;
        $details['package'] = array(
          'name' => $package->getTimeAlotment(),
          'remaining_balance' => 30,
          'used_balance' => 0
        );
        $details['premium'] = array(
          array(
            'id' => 'registry',
            'name' => 'Link to a Registry',
            'value' => ''
          )
        );

        $addons = $this->getOption('addons');
        if ($addons == false){
          $details['addons']['registry'] = false;
        	$details['addons']['marriage_application'] = false;
        	$details['addons']['marriage_education'] = false;
        	$details['addons']['marriage_license'] = false;
        }else{
          $addons = json_decode($addons);
          $details['addons']['registry'] = false;
        	$details['addons']['marriage_application'] = false;
        	$details['addons']['marriage_education'] = false;
        	$details['addons']['marriage_license'] = false;
          foreach($addons as $addon){
            if (@$addon->id == 1){
              $details['addons']['registry'] = true;
            }

            if (@$addon->id == 2){
              $details['addons']['marriage_application'] = true;
            }

            if (@$addon->id == 3){
              $details['addons']['marriage_education'] = true;
            }

            if (@$addon->id == 4){
              $details['addons']['marriage_license'] = true;
            }
          }
        }

        $users = array();

        $allParticipants = array();
        $participants = $this->_participants;

        foreach ($participants as $participant) {
          if (!is_null($participant->_user)){
            $userId = $participant->_user->id;
          }else{
            $userId = $participant->id;
          }
          //@TODO implement status, and last response for participant replies.
          $allParticipants = array_merge($allParticipants, array(
              array(
                "user" => $userId,
                "status" => $participant->status,
                "role" => $participant->role,
                "last_response" => strtotime('now')
              )
            )
          );

          if (!is_null($participant->_user)){
            $users[$userId] = $participant->_user->getPublicProfile();
          }else{
            $users[$userId] = array('personal' => array(
                'id' => $participant->id,
                'avatar' => url(env('APP_URL').'/api/user/'.@explode(' ', $participant->name)[0].'_'.@explode(' ', $participant->name)[1].'/avatar'),
                'name' => array(
                 'first' => @explode(' ', $participant->name)[0],
                 'last' => @explode(' ', $participant->name)[1],
                 'full' => $participant->name
                )
              ));
          }
        }

        return array(
          'details' => $details,
          'participants' => $allParticipants,
          'guest' => $this->_guests,
          'users' => $users
        );
      }else{
        return $details;
      }
    }

    public function privacy(){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == 'privacy'){
          return boolval($option['option_value']);
        }
      }

      return false;
    }

    public function sessionId(){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == 'session_id'){
          return ($option['option_value']);
        }
      }

      return (new OpenTokController)->generateSessionId();
    }

    public function participantNumber($userId){
      $user = new Users();
      $user = $user->where('id','=',$userId);
      if ($user->exists()){
        $user = $user->first();
        $participants = $this->_participants;
        $index = 1;
        foreach($participants as $participant){
          if ($participant->uid == $user->uid){
            return 'participant'.$index;
          }

          $index += 1;
        }
      }else{
        return 'participant0';
      }
    }

    public function updateOption($key, $value){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == $key){
          $option['option_value'] = $value;
          $option->save();
        }
      }
    }

    public function getOption($key){
      $eventOptions = $this->_options;
      foreach($eventOptions as $option){
        if ($option['option_key'] == $key){
          return $option['option_value'];
        }
      }

      return false;
    }

    public function makeDefaultOptions(){
      $this->_options()->create([
        'option_key' => 'privacy',
        'option_value' => '1'
      ]);
      $this->_options()->create([
        'option_key' => 'total_viewers',
        'option_value' => '0'
      ]);

      $this->_options()->create([
        'option_key' => 'current_viewers',
        'option_value' => '0'
      ]);
      $sessionId = (new OpenTokController)->generateSessionId();
      $this->_options()->create([
        'option_key' => 'session_id',
        'option_value' => $sessionId
      ]);
      // $this->_options()->create([
      //   'option_key' => 'archive_id',
      //   'option_value' => (new OpenTokController)->generateArchiveId($sessionId)
      // ]);
    }

    /**
     * @return array
     */
    public function getFirebaseSyncData()
    {
        if ($fresh = $this->fresh()) {
            //return $fresh->toArray();
            return array(
              'activate' => true,
              'active_admin' => 0,
              'permissions' => array(
                4
              ),
              'remaining_time' => 30,
              'total_time' => 30,
              'broadcastId' => (new OpenTokController)->getBroadcastId($fresh->toArray()['id']),
              'broadcastUrl' => (new OpenTokController)->getBroadcastUrl($fresh->toArray()['id'])
            );
        }
        return [];
    }
    /**
     * @param $mode
     */
    public function saveToFirebase($mode)
    {
        if (is_null($this->firebaseClient)) {
            $this->firebaseClient = new FirebaseLib(config('services.firebase.database_url'), config('services.firebase.secret'));
        }
        $path = 'events' . '/' . $this->getKey();
        if ($mode === 'set') {
            $this->firebaseClient->set($path, $this->getFirebaseSyncData());
        } elseif ($mode === 'update') {
            $this->firebaseClient->update($path, $this->getFirebaseSyncData());
        } elseif ($mode === 'delete') {
            $this->firebaseClient->delete($path);
        }
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function _user() {
      return $this->hasOne('App\Models\Users', 'id'); //Not correct it is using ID not user_id
    }

    public function _package() {
      return $this->hasOne('App\Models\Packages', 'id', 'package');
    }

    public function _participants() {
      return $this->hasMany('App\Models\Participants', 'event', 'id');
    }

    public function _guests() {
      return $this->hasMany('App\Models\Guests', 'event', 'id');
    }

    public function _options() {
      return $this->hasMany('App\Models\Event_options', 'event', 'id');
    }

    public function chat() {

    }

    public function _saleRecord() {
      return $this->hasOne('App\Models\Event_sales', 'event', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
