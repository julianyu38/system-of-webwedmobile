<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users;

class UserOptions extends Model
{
    protected $table = 'firebase_users_options';
    protected $primaryKey = 'uid';
    protected $guarded = ['id'];

    public function getOption($key){
      $userOptions = $this->get();
      foreach($userOptions as $option){
        if ($option['option_key'] == $key){
          return $option['option_value'];
        }
      }

      return '';
    }

    public function address(){
      return json_decode($this->getOption('address'));
    }

    public function privacy(){
      return array(
        'notifications' => array(
          'push' => boolval($this->getOption('push_notifications')),
          'email' => boolval($this->getOption('email_notifications'))
        )
      );
    }

    public function dob(){
      return $this->getOption('dob');
    }

    public function user() {
      return $this->hasOne('App\Models\Users', 'id', 'uid');
    }
}
