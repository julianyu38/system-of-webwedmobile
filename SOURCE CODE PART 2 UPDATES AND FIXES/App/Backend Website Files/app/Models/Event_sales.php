<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Event_sales extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'event_saless';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function getUserProfile() {
        return '<a href="'.url("admin/users/1/edit").'">John Doe</a>';
    }

    public function getPrice(){
      return '$'.$this->price_paid.'.00';
    }

    public function getEventLink() {
        // $package = $this->package;
        return '<a href="'.url("admin/events/1/edit").'">12445</a>';
    }

    public function getPackageLink() {
        // $package = $this->package;
        return '<a href="'.url("admin/packages/1/edit").'">15 Minutes</a>';
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

   public function _event() {
     return $this->hasOne('App\Models\Events', 'id', 'event');
   }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
