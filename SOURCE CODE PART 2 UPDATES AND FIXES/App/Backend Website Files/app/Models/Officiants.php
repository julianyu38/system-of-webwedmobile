<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Officiants extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'officiantss';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['created_at', 'deleted_at'];
    protected $fillable = ['uid','details','active','locations'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function user(){
      return $this->hasOne('App\Models\Users', 'uid', 'uid');
    }

    public function getName(){
      return $this->user->first_name . ' ' . $this->user->last_name;
    }

    public function getActiveStatus(){
      return ($this->active == 1 ? 'Active' : 'Inactive');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
