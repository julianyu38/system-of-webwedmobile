<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invites extends Model
{
    protected $table = 'event_invitions';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $fillable = [ 'uid', 'event', 'video', 'title', 'description' ];

    public function user() {
      return $this->hasOne('App\Models\User', 'id', 'uid');
    }

    public function event() {
      return $this->hasOne('App\Models\Events', 'id', 'event');
    }
}
