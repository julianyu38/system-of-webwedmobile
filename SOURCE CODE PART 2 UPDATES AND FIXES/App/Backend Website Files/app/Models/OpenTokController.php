<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;
use OpenTok\OutputMode;
use App\Models\Events;


class OpenTokController extends Model
{
    private $apiKey;
    private $apiSecret;
    private $opentok;

    public function __construct(){
      $this->apiKey = getenv('OPENTOK_API_KEY');
      $this->apiSecret = getenv('OPENTOK_SECRET');
      // return dd($this->apiSecret);
      $this->opentok = new OpenTok($this->apiKey, $this->apiSecret);
    }

    public function generateSessionId(){
      $sessionOptions = array(
          'mediaMode' => MediaMode::ROUTED,
          'archiveMode' => ArchiveMode::ALWAYS
      );
      $session = $this->opentok->createSession($sessionOptions);
      return $session->getSessionId();
    }

    public function generateArchiveId($sessionId){
      $archiveOptions = array(
          'name' => 'Example Event',     // default: null
          'hasAudio' => true,                     // default: true
          'hasVideo' => true,                     // default: true
          'outputMode' => OutputMode::COMPOSED  // default: OutputMode::COMPOSED
      );
      // $archive = $this->opentok->startArchive($sessionId, $archiveOptions);
      return '';
      // return $archive->id;
    }

    public function create($eventId, $userId){
      // Store this sessionId in the database for later use
      $event = Events::find($eventId);

      $token = $this->opentok->generateToken($event->sessionId(), array(
          'role'       => Role::MODERATOR,
          'expireTime' => time()+(7 * 24 * 60 * 60), // in one week
          'data'       => $event->participantNumber($userId)
      ));

      return array(
        'session_id' => $event->sessionId(),
        'token' => $token,
        'role' => $event->participantNumber($userId)
      );
    }

    public function getArchive($eventId){
      $event = Events::find($eventId);
      $archiveId = $event->getOption('archive_id');
      if ($archiveId == ''){ return ''; }
      $archive = $this->opentok->getArchive($archiveId);

      return $archive->toArray()['url'];
    }

    public function stop($eventId,$userId){

    }

    public function startLiveStream($eventId, $sessionId){
        // $event = Events::find($eventId);
        // $token = array(
        //     'ist' => 'project',
        //     'iss' => getenv('OPENTOK_API_KEY'),
        //     'iat' => time(), // this is in seconds
        //     'exp' => time()+(5 * 60),
        //     'jti' => uniqid(),
        // );
        // $token = \Firebase\JWT\JWT::encode($token, getenv('OPENTOK_SECRET'));
        // $arr = array(
        //   "sessionId" => $sessionId,
        //   "layout" => array(
        //     "type" => "bestFit"
        //   ),
        //   "outputs" => array(
        //     "hls" => array(),
        //     "rtmp" => array()
        //   )
        // );
        // $data_string = json_encode($arr);
        // $data_string = str_replace("\"hls\":[]", "\"hls\":{}", $data_string);
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, "https://api.opentok.com/v2/project/".getenv("OPENTOK_API_KEY")."/broadcast");
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        // 'X-OPENTOK-AUTH: ' . $token,
        // 'Content-Type: application/json',
        // 'Content-Length: ' . strlen($data_string))
        // );
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        // $output = curl_exec($ch);
        // curl_close($ch);
        //
        // $json = json_decode($output, true);
        //
        // if (@$json['message'] == 'Not found. No clients are actively connected to the OpenTok session.'){
        //   return;
        // }
        //
        // if (@$json['message'] == 'Existing broadcast on the session'){
        //   $event->status = 9;
        //   return;
        // }
        //
        // if (@$json['id']){
        //   $broadcastId = @$json['id'];
        //   $broadcastUrl = @$json['broadcastUrls']['hls'];
        //
        //   //save broadcast id, and url.
        //   $broadcastIdOption = $event->getOption('broadcastId');
        //   $broadcastUrlOption = $event->getOption('broadcastUrl');
        //   if ($broadcastIdOption==false){
        //     $event->_options()->create([
        //       'option_key' => 'broadcastId',
        //       'option_value' => $broadcastId
        //     ]);
        //     $event->_options()->create([
        //       'option_key' => 'broadcastUrl',
        //       'option_value' => $broadcastUrl
        //     ]);
        //     $event->status = 9;
        //     $event->save();
        //   }
        // }
    }

    public function stopLiveStream($eventId, $sessionId){
        // $event = Events::find($eventId);
        // $token = array(
        //     'ist' => 'project',
        //     'iss' => getenv('OPENTOK_API_KEY'),
        //     'iat' => time(), // this is in seconds
        //     'exp' => time()+(5 * 60),
        //     'jti' => uniqid(),
        // );
        // $token = \Firebase\JWT\JWT::encode($token, getenv('OPENTOK_SECRET'));
        // $arr = array(
        //   "sessionId" => $sessionId,
        //   "layout" => array(
        //     "type" => "bestFit"
        //   ),
        //   "outputs" => array(
        //     "hls" => array(),
        //     "rtmp" => array()
        //   )
        // );
        // $data_string = json_encode($arr);
        // $data_string = str_replace("\"hls\":[]", "\"hls\":{}", $data_string);
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, "https://api.opentok.com/v2/project/".getenv("OPENTOK_API_KEY")."/broadcast/stop");
        // curl_setopt($ch, CURLOPT_POST, 1);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        // 'X-OPENTOK-AUTH: ' . $token,
        // 'Content-Type: application/json',
        // 'Content-Length: ' . strlen($data_string))
        // );
        // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        // $output = curl_exec($ch);
        // curl_close($ch);
        //
        // $json = json_decode($output, true);
        //
        // if (@$json['message'] == 'Not found. No clients are actively connected to the OpenTok session.'){
        //   return;
        // }
        //
        // if (@$json['message'] == 'Existing broadcast on the session'){
        //   return;
        // }
        //
        // if (@$json['id']){
        //   $broadcastId = @$json['id'];
        //   $broadcastUrl = @$json['broadcastUrls']['hls'];
        //
        //   //save broadcast id, and url.
        // }
    }

    public function liveStreamDetails($broadcastId){
      //   $event = Events::find($eventId);
      //   $token = array(
      //       'ist' => 'project',
      //       'iss' => getenv('OPENTOK_API_KEY'),
      //       'iat' => time(), // this is in seconds
      //       'exp' => time()+(5 * 60),
      //       'jti' => uniqid(),
      //   );
      //   $token = \Firebase\JWT\JWT::encode($token, getenv('OPENTOK_SECRET'));
      //
      //   $ch = curl_init();
      //   curl_setopt($ch, CURLOPT_URL, "https://api.opentok.com/v2/project/".getenv("OPENTOK_API_KEY")."/broadcast/".$broadcastId);
      //   curl_setopt($ch, CURLOPT_POST, 0);
      //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      //   curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      //   'X-OPENTOK-AUTH: ' . $token
      // ));
      //   $output = curl_exec($ch);
      //   curl_close($ch);
      //
      //   $json = json_decode($output, true);
      //
      //   if (@$json['id']){
      //     $broadcastId = @$json['id'];
      //     $broadcastUrl = @$json['broadcastUrls']['hls'];
      //
      //     //save broadcast id, and url.
      //   }
    }

    public function getBroadcastId($eventId){
      $event = Events::find($eventId);
      $broadcastIdOption = $event->getOption('broadcastId');
      $broadcastUrlOption = $event->getOption('broadcastUrl');
      return $broadcastIdOption;
    }

    public function getBroadcastUrl($eventId){
      $event = Events::find($eventId);
      $broadcastIdOption = $event->getOption('broadcastId');
      $broadcastUrlOption = $event->getOption('broadcastUrl');
      return $broadcastUrlOption;
    }
}
