<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use Auth;
use App\Models\UserOptions;
use App\Models\Events;
use App\Models\Guests;
use App\Models\Officiants;
use App\Models\Participants;
use Illuminate\Notifications\Notifiable;
use App\Notifications\GuestInvite;
use App\Notifications\InviteResponse;
use App\Notifications\OfficiantMissing;
use App\Models\UserNotifications;
use Illuminate\Support\Facades\Log;

class Users extends Model
{
    use CrudTrait;
    use Notifiable;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'firebase_users';
    protected $primaryKey = 'uid';
    public $timestamps = true;
    public $incrementing = false;
    // protected $guarded = ['id'];
    protected $fillable = [ 'first_name', 'last_name', 'email', 'phone', 'token' ];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

   public function getUserProfile(){
     return '<a href="#">User</a>';
   }

   public function isMyAccount($id){
     if ($this->id == $id){
       return true;
     }else{
       return false;
     }
   }

   public static function getMyAccount(){
     $userId = Auth::user()->uid;
     $user = (new Users)->where('uid','=',$userId);
     if ($user->exists()){
       return $user->first();
     }else{
       return null;
     }
   }

   public function registerAsCreator($user){
     $participants = $user->participants;
     $events = Events::where('uid', '=', $user->uid)->get();
     foreach($events as $event){
       $participant = Participants::where(array(array('uid','=',$user->uid), array('event','=',$event->id)));
       if (!$participant->exists()){
         $participants = $event->_participants()->create([
           'identifier' => $user->email,
           'name' => '',
           'uid' => $user->uid,
           'role' => 'creator',
           'status' => 1
         ]);
       }
     }
   }

   public function getEvents($user = null, $wantsPast = false) {
     if ($user == null){
       $user = $this;
     }

     $this->registerAsCreator($user);

     $data = array();

     // Log::info('GUESTS: '.json_encode($user->guests));
     // Log::info('PARTICIPANTS: '.json_encode($user->participants));

    // return dd($user->guests);
    Log::info('SIZE ID: '.sizeof($user->participants));
    Log::info('USER ID: '.json_encode($user));
     foreach ($user->participants as $participant) {
       $event = $participant->_event;
       Log::info('PARTICIPANT ID: '.($event->id));

       if ($participant->role == 'creator'){
         $typeOfEvent = 'event';
       }else{
         $typeOfEvent = 'invite';
       }

       $past = false;

       if (strtotime('now') > $event->scheduled_date || $event->status > 1){
         $past = true;
       }else{
         $past = false;
       }

       // if ($wantsPast == false ) { continue; }

       if (($past == $wantsPast) || ($wantsPast == null)){
         $details = array(
             "id" => $event->id,
             "photo" => $event->getPhoto(),
             "title" => $event->title,
             "date" => ($event->scheduled_date),
             "past" => $past,
             "participants" => $event->getParticipants(),
             "type" => $typeOfEvent,
             "event_status" => $event->status,
             "hasResponded" => $participant->status,
             "privacy" => $event->privacy()
           );
         if ($typeOfEvent == 'invite'){
           $details['status'] = (new Participants)->getStatusForEvent($user, $event->id);
         }
         $data = array_merge($data, array($details));
       }
     }



     foreach ($user->guests as $guest) {
       $event = Events::find($guest->event);
       $typeOfEvent = 'invite';


       if (strtotime('now') > $event->scheduled_date){
         $past = true;
       }else{
         $past = false;
       }

       if ($past == $wantsPast || $wantsPast == null){
         $details = array(
             "id" => $event->id,
             "photo" => $event->getPhoto(),
             "title" => $event->title,
             "date" => ($event->scheduled_date),
             "past" => $past,
             "participants" => $event->getParticipants(),
             "type" => $typeOfEvent,
             "event_status" => $event->status
           );
         if ($typeOfEvent == 'invite'){
           $details['status'] = (new Guests)->getStatusForEvent($user, $event->id);
         }
         $data = array_merge($data, array($details));
       }
     }

     usort($data, function($a, $b) {
          return $b['date'] - $a['date'];
     });

     return $data;
   }

   public function getPublicProfile() {
     $personal = array(
       'id' => $this->id,
        'avatar' => url(env('APP_URL').'/api/user/'.urlencode($this->id).'/avatar'),
        'name' => array(
          'first' => $this->first_name,
          'last' => $this->last_name,
          'full' => $this->first_name . " " . $this->last_name
        ),
        'email' => $this->email
    );

     return array(
       'personal' => $personal
     );
   }

   public function getProfile() {
     $requestingUser = Auth::user();

     $personal = $this->getPublicProfile()['personal'];

    if ($requestingUser == null){
      $requestingUid = -1;
    }else{
      $requestingUid = $requestingUser->uid;
    }

    $address = UserOptions::where(array('uid' => $this->id, 'option_key' => 'address'))->first();
    $dob = UserOptions::where(array('uid' => $this->id, 'option_key' => 'dob'))->first();

    $personal['email'] = $this->email;
    $personal['dob'] = @$dob->option_value;
    $personal['phone'] = $this->phone;
    $personal['location'] = @json_decode($address->option_value);
    $personal['privacy'] = array('notifications' => array(
      'push' => true,
      'email' => true
    ));

     if ($requestingUid == $this->uid){
       return array(
         'personal' => $personal,
         'events' => $this->getEvents($this, false)
       );
     }else{
       return array(
         'personal' => $personal,
         'officiant' => array('fee' => 50),
       );
     }
   }

   public function getNotifications() {
     $requestingUser = Auth::user();
     $requestedUser = $this;

     if ($requestingUser->id == $requestedUser->id){
       return array(
	       'success' => true,
         'notifications' => UserNotifications::where(array('uid' => $requestingUser->uid))->orderBy('timestamp','desc')->get()
       );
     }
   }

   //@TODO problem updating options
   public function updateOption($key, $value){
     $userOptions = $this->options;
     if (sizeof($userOptions) == 0){ return; }
     foreach($userOptions as $option){
       if ($option['option_key'] == $key){
         $option['option_value'] = $value;
        //  $option->save();
       }
     }
   }

   public function makeDefaultOptions($data){
     $arr = array(
       "address" => @explode(',', $data['location'])[0],
       "city" => @explode(',', $data['location'])[1],
       "state" => @explode(',', $data['location'])[2],
       "zip" => "30011"
     );

     $arr = json_encode($arr);

     $this->options()->create([
       'option_key' => 'address',
       'option_value' => $arr
     ]);
     $this->options()->create([
       'option_key' => 'dob',
       'option_value' => strtotime(@$data['dob'])
     ]);
     $this->options()->create([
       'option_key' => 'email_notifications',
       'option_value' => '1'
     ]);
     $this->options()->create([
       'option_key' => 'push_notifications',
       'option_value' => '1'
     ]);
   }

   public function updateAddress($data){
     $arr = array(
       "address" => @explode(',', $data['location'])[0],
       "city" => @explode(',', $data['location'])[1],
       "state" => @explode(',', $data['location'])[2],
       "zip" => "30011"
     );

     $arr = json_encode($arr);

     $address = $this->options()->where(array('option_key', '=', 'address'));
     if ($address->exists()){
       $address = $address->first();
       $address->option_value = $arr;
       $address->save();
     }
   }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function participants(){
      return $this->hasMany('App\Models\Participants', 'uid', 'uid');
    }

    public function isOfficiant(){
      $officiants = Officiants::where('uid','=',$this->uid);
      if ($officiants->exists()){
        return true;
      }else{
        return false;
      }
    }

    public function setOfficiantStatus($status){
      if ($status == 0){
        if ($this->isOfficiant()){
            //delete officiant role
            $officiants = Officiants::where('uid','=',$this->uid);
            if ($officiants->exists()){
              $officiants = $officiants->first();
              $officiants->remove();
            }
        }
      }else{
        if (!$this->isOfficiant()){
          //add officiant role
          Officiants::create([
            'uid' => $this->uid,
            'details' => '',
            'active' => 1,
            'locations' => json_encode(array())
          ]);
        }
      }
    }

    public function events(){
      return $this->hasMany('App\Models\Events', 'uid', 'uid');
    }

    public function guests(){
      return $this->hasMany('App\Models\Guests', 'uid', 'id');
    }

    public function orders(){
      return $this->hasMany('App\Models\Event_sales', 'uid', 'uid');
    }

    public function options(){
      return $this->hasMany('App\Models\UserOptions', 'uid', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
