<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StripeController extends Model
{
    private $stripe;

    public function __construct(){
      //$this->stripe = new \Stripe\Stripe;
      //$this->stripe->setApiKey('');
    }

    public function makePayment($price, $tokenId){
      return array(
        'txn_id' => 'TXN_ID',
        'price' => $price
      );
    }
}
