<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
use App\Models\Users;
use App\Models\Events;

class Participants extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'event_participants';
    protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

   public function assignInvitedEvents($user){
     $allEvents = $this->where('identifier','=',$user->email)->orWhere('identifier','=',$user->phone);

    //  if ($allEvents->exists()){
       $allEvents = $allEvents->get();
       foreach($allEvents as $event){
         if ($event->uid == '' || $event->uid == 0){
           $event->uid = $user->uid;
           $event->name = $user->first_name . ' ' . $user->last_name;
           $event->save();
         }
       }
    //  }
   }

   public function getStatusForEvent($user, $eventId){
     $event = $this->where([['event', '=', $eventId],['uid','=',$user->uid]]);
     if ($event->exists()){
       $event = $event->first();
       $title = '';
       $color = 'green';
       switch ($event->status) {
         case 0:
           $title = 'INVITED YOU';
           $color = 'gray';
           break;

         case 1:
           $title = 'ACCEPTED';
           $color = 'green';
           break;

         case 2:
           $title = 'TENATIVE';
           $color = 'green';
           break;

         case 3:
           $title = 'DECLINED';
           $color = 'red';
           break;

         default:
           $title = 'INVITED YOU';
           $color = 'gray';
           break;
       }

       return array(
         'title' => $title,
         'color' => $color
       );
     }else{
       return array(
         'title' => 'unknown',
         'color' => 'gray'
       );
     }
   }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function _user() {
      return $this->hasOne('App\Models\Users', 'uid', 'uid'); //Not correct it is using ID not user_id
    }

    public function _event() {
      return $this->belongsTo('App\Models\Events', 'event', 'id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
