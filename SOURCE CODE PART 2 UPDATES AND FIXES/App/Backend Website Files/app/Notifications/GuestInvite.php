<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GuestInvite extends Notification
{
    use Queueable;

    private $user;
    private $event;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $event)
    {
        $this->user = $user;
        $this->event = $event;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('You are invited to our event at ' . date('m/d/Y h:i A', $this->event->scheduled_date))
                    ->action('View Invite', url('/event/share/'.$this->event->id))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
          'avatar' => url(env('APP_URL').'/api/user/'.$this->user->id.'/avatar'),
          'title' => 'Invited To ' . $this->event->name,
          'message' => 'You are invited to our event at ' . date('m/d/Y h:i A', $this->event->scheduled_date),
          'timestamp' => strtotime('now'),
          'status' => 'unread'
        ];
    }
}
