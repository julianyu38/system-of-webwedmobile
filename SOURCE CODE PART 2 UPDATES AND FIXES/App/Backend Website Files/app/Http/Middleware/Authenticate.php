<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;
use Vinkas\Firebase\Auth\AuthenticatesUsers;
use \Illuminate\Http\Request;

class Authenticate
{
    use AuthenticatesUsers;

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        //DISABLE FOR MAIN ADMIN PANEL
        // $access_token = $request->header('X-Requested-Auth');
        // return dd($access_token);
        // return dd($request);
        if ($request->has('id_token') || ($request->header('X-Requested-Auth') != 'invalid_token')){
          return dd($this->postAuth($request));
          if (!$this->postAuth($request)['success']){
            return response()->json(['success' => false, 'message' => 'Unauthorized.'], 401);
          }else{
            return $next($request);
          }
        }

        //DISABLED AUTHENTICATION
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json(['success' => false, 'message' => 'Unauthorized.'], 401);
            } else {
                return redirect()->guest('login');
                // return response()->json(['success' => false, 'message' => 'Unauthorized.'], 401);
            }
        }

        return $next($request);
    }
}
