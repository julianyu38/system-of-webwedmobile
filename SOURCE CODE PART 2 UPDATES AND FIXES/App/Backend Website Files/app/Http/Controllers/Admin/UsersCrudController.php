<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UsersRequest as StoreRequest;
use App\Http\Requests\UsersRequest as UpdateRequest;

class UsersCrudController extends CrudController
{

    private $tabs = [ 'Details', 'Officiant Details', 'Events' ];

    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Users');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admins');
        $this->crud->setEntityNameStrings('Admin Accounts', 'Admin Account');
        $this->crud->denyAccess(['create', 'delete']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // $this->crud->setColumns([
        //     [
        //         'name'  => 'first_name',
        //         'label' => 'First Name',
        //     ],
        //     [
        //         'name'  => 'last_name',
        //         'label' => 'Last Name',
        //     ],
        //     [
        //         'name'  => 'email',
        //         'label' => 'Email',
        //     ],
        //     [
        //         'name'  => 'phone',
        //         'label' => 'Phone',
        //     ]
        // ]);
    }

    public function index()
    {
      // $this->crud->setFromDb();

      return parent::index();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    // public function edit($id)
    // {
    //     $this->data['entry'] = $this->crud->getEntry($id);
    //
    //     // dd($this->crud->getEntry($id)->events());
    //
    //
    //     //User Details Tab
    //     $this->crud->addField([
    //         'name' => 'first_name',
    //         'label' => "First Name",
    //         'type' => 'text',
    //         'tab' => $this->tabs[0]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'last_name',
    //         'label' => "Last Name",
    //         'type' => 'text',
    //         'tab' => $this->tabs[0]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'email',
    //         'label' => "Email",
    //         'type' => 'email',
    //         'tab' => $this->tabs[0]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'phone',
    //         'label' => "Phone",
    //         'type' => 'text',
    //         'tab' => $this->tabs[0]
    //     ]);
    //
    //     $this->crud->addField([
    //         'label'     => 'Roles',
    //         'type'      => 'checklist',
    //         'name'      => 'admin',
    //         'entity'    => 'roles',
    //         'attribute' => 'name',
    //         'model'     => "Backpack\PermissionManager\app\Models\Role",
    //         'pivot'     => true,
    //         'tab' => $this->tabs[0]
    //     ]);
    //
    //     //Officiant Details Tab
    //
    //     $this->crud->addField([ // image
    //         'label' => "Profile Image",
    //         'fake' => true,
    //         'name' => "image",
    //         'type' => 'browse',
    //         'upload' => true,
    //         'crop' => true, // set to true to allow cropping, false to disable
    //         'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
    //         // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'random',
    //         'fake' => true,
    //         'label' => "Description",
    //         'type' => 'textarea',
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'price',
    //         'label' => 'Officating Price',
    //         'fake' => true,
    //         'type' => 'text',
    //         'tab' => $this->tabs[1],
    //         'prefix' => '<i class="fa fa-usd"></i>'
    //     ]);
    //
    //     $questions = [
    //       'one' => 'Do you service areas outside of your hometown?',
    //       'two' => 'Should a couple know you before you perform ceremony?',
    //       'three' => 'Will you only marry couples of like faith?'
    //     ];
    //
    //     $this->crud->addField([
    //         'name' => 'question_1',
    //         'fake' => true,
    //         'label' => "Question 1",
    //         'type' => 'select2_from_array',
    //         'options' => $questions,
    //         'allows_null' => false,
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'question_1_answer',
    //         'label' => '',
    //         'fake' => true,
    //         'type' => 'text',
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'question_2',
    //         'fake' => true,
    //         'label' => "Question 2",
    //         'type' => 'select2_from_array',
    //         'options' => $questions,
    //         'allows_null' => false,
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'question_2_answer',
    //         'label' => '',
    //         'fake' => true,
    //         'type' => 'text',
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'question_3',
    //         'fake' => true,
    //         'label' => "Question 3",
    //         'type' => 'select2_from_array',
    //         'options' => $questions,
    //         'allows_null' => false,
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'question_3_answer',
    //         'label' => '',
    //         'fake' => true,
    //         'type' => 'text',
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //     $this->crud->addField([
    //         'name' => 'status',
    //         'fake' => true,
    //         'label' => "Status",
    //         'type' => 'select2_from_array',
    //         'options' => ['inactive' => 'Inactive','active' => 'Active'],
    //         'allows_null' => false,
    //         'tab' => $this->tabs[1]
    //     ]);
    //
    //
    //
    //     //Events Tab
    //     $this->crud->addField([
    //         'name' => 'id',
    //         'label' => "First Name",
    //         'type' => 'text',
    //         'tab' => $this->tabs[2]
    //     ]);
    //
    //
    //     //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
    //     $this->data['crud'] = $this->crud;
    //     $this->data['saveAction'] = $this->getSaveAction();
    //     $this->data['fields'] = $this->crud->getUpdateFields($id);
    //     $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
    //
    //     $this->data['id'] = $id;
    //
    //     // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
    //     return view($this->crud->getEditView(), $this->data);
    // }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
