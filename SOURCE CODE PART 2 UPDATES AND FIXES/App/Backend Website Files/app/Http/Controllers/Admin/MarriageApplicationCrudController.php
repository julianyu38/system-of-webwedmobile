<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MarriageApplicationRequest as StoreRequest;
use App\Http\Requests\MarriageApplicationRequest as UpdateRequest;
use App\Models\MarriageApplication;

class MarriageApplicationCrudController extends CrudController
{
    public $tabs = [ 'Documents', 'History' ];
    public function setup()
    {


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\MarriageApplication');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/marriageapplication');
        $this->crud->setEntityNameStrings('Marriage Application', 'Marriage Applications');
        $this->crud->denyAccess(['add', 'create', 'delete']);

        // $this->crud->setFromDb();
        //
        $this->crud->setColumns([
          [
              'name'  => 'event',
              'label' => 'Event',
              'type' => 'model_function',
              'function_name' => 'getEvent'
          ],
          [
              'name'  => 'your',
              'label' => 'User',
              'type' => 'model_function',
              'function_name' => 'getYour'
          ]
        ]);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $marriageApplication = MarriageApplication::find($id);

        $attachments = json_decode($marriageApplication->attachments, true);

        $images = '';
        foreach($attachments['your'] as $image){
          if ($image['value'] == ''){ continue; }
          $images .= '<img style="padding: 20px; width:100px; height: 100px; float:left;" src="'.$image['value'].'" />';
        }

        foreach($attachments['spouce'] as $image){
          if ($image['value'] == ''){ continue; }
          $images .= '<img style="padding: 20px; width:100px; height: 100px; float:left;" src="'.$image['value'].'" />';
        }

        $this->data['entry'] = $this->crud->getEntry($id);

        $this->crud->addField([
          'name' => 'id',
          'fake' => true,
          'type' => 'custom_html',
          'value' => '
          <h2>Marriage Application</h2><br/>
          <p>Below are a copy of the attachments.</p>

          <h4>Attachments</h4>
          <div>
            '.$images.'
          </div>'
        ]);

        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
