<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AdminsRequest as StoreRequest;
use App\Http\Requests\AdminsRequest as UpdateRequest;

class AdminsCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Admins');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/admins');
        $this->crud->setEntityNameStrings('admins', 'admins');
        $this->crud->denyAccess(['create']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->setColumns([
            [
                'name'  => 'first_name',
                'label' => 'First Name',
            ],
            [
                'name'  => 'last_name',
                'label' => 'Last Name',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
            ],
            [
                'name'  => 'phone',
                'label' => 'Phone',
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
