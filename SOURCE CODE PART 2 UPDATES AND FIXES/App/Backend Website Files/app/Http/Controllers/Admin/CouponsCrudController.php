<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CouponsRequest as StoreRequest;
use App\Http\Requests\CouponsRequest as UpdateRequest;

class CouponsCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Coupons');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/coupons');
        $this->crud->setEntityNameStrings('Discount Code', 'Discount Codes');


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'code', // DB column name (will also be the name of the input)
            'label' => 'Code', // the human-readable label for the input
            'type'  => 'text', // the field type (text, number, select, checkbox, etc)

             'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ]
          ]
        );

        $this->crud->addField(
          [
             // MANDATORY
            'name'  => 'price', // DB column name (will also be the name of the input)
            'label' => 'Discount', // the human-readable label for the input
            'type'  => 'number', // the field type (text, number, select, checkbox, etc)

             'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],

             'prefix' => '<i class="fa fa-usd"></i>',
             'suffix' => '.00'
          ]
        );


        $this->crud->setColumns([
            [
               'label' => "Coupon Code", // Table column heading
               'type' => "text",
               'name' => 'code'
            ],
            [
              'label' => "Discount", // Table column heading
              'type' => "model_function",
              'function_name' => 'getPrice', // the method in your Model
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
