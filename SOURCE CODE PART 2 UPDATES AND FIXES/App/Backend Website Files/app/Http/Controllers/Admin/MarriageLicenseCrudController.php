<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MarriageLicenseRequest as StoreRequest;
use App\Http\Requests\MarriageLicenseRequest as UpdateRequest;

class MarriageLicenseCrudController extends CrudController
{
    public $tabs = [ 'Signatures', 'Documents', 'Manage' ];
    public function setup()
    {


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\MarriageLicense');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/marriagelicense');
        $this->crud->setEntityNameStrings('Marriage License', 'Marriage Licenses');
        $this->crud->denyAccess(['add', 'create', 'delete']);

        // $this->crud->setFromDb();

        $this->crud->setColumns([
            
        ]);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */


        // $this->crud->setFromDb();


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->data['entry'] = $this->crud->getEntry($id);

        $this->crud->addField([
          'name' => 'id1',
          'fake' => true,
          'type' => 'custom_html',
          'value' => '
          <table class="table datatable">
            <thead>
              <tr>
                <th>Account</th>
                <th>Response</th>
                <th>Date & Time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td><a href="'.url("admin/user/1/edit").'">John Doe</a></td>
                <td><span class="label label-success">Signed</span></td>
                <td>July 11th, 2017 5:00 PM</td>
              </tr>
            </tbody>
          </table>
          ',

        ]);
        //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
