<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CourtsRequest as StoreRequest;
use App\Http\Requests\CourtsRequest as UpdateRequest;

class CourtsCrudController extends CrudController
{
    private $tabs = ['Details', 'Processing'];
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Courts');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/courts');
        $this->crud->setEntityNameStrings('Court', 'Courts');
        $this->crud->denyAccess(['create', 'delete']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->setColumns([
            [
             'name' => 'name', // The db column name
             'label' => "Court", // Table column heading
             'type' => 'Text'
           ],
           [
            'name' => 'email', // The db column name
            'label' => "Representive", // Table column heading
            'type' => 'Text'
          ],
          [
           'name' => 'phone', // The db column name
           'label' => "Phone", // Table column heading
           'type' => 'Text'
          ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->data['entry'] = $this->crud->getEntry($id);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Court',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'address',
            'fake' => true,
            'type' => 'text',
            'label' => 'County',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'address2',
            'fake' => true,
            'type' => 'number',
            'label' => 'Court Fee',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'email',
            'type' => 'text',
            'label' => 'Email',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'type' => 'text',
            'label' => 'Phone',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
             'tab' => $this->tabs[0]
        ]);

        $acceptanceMethods = '

          <div class="row">
            <div class="col-md-3">
              <input type="checkbox" />
              <label> Accepted E-Signatures</label>

            </div>

            <div class="col-md-4">
              <input type="checkbox" />
              <label> Accepts Online Marriage Applications</label>
            </div>

          </div>
        ';

        $this->crud->addField([
            'name' => 'name2',
            'fake' => true,
            'type' => 'custom_html',
            'value' => $acceptanceMethods,
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'name3',
            'fake' => true,
            'type' => 'custom_html',
            'value' => '$this->data[\'entry\']->generateProcessingTable()',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
             'tab' => $this->tabs[1]
        ]);

        //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
