<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\EmojisRequest as StoreRequest;
use App\Http\Requests\EmojisRequest as UpdateRequest;

class EmojisCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Emojis');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/emojis');
        $this->crud->setEntityNameStrings('Emoji', 'Emojis');
        $this->crud->denyAccess(['delete']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->addField(
            [
              'name' => 'name',
              'label' => "Title" // Table column heading
            ]);
        // $this->crud->addField(
        //     [
        //       'name' => 'filename',
        //       'label' => "Image", // Table column heading
        //       'type' => "browse",
        //       'upload' => true,
        //       'crop' => true, // set to true to allow cropping, false to disable
        //       'aspect_ratio' => 1
        //     ]
        // );

        $this->crud->addField([ // image
            'label' => "Emoji",
            'name' => "id",
            'type' => 'image',
            'upload' => false,
            'value' => 'http://cdn.appshopper.com/icons/117/2073443_larger.png',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
        ]);

        $this->crud->setColumns([
            [
              'name' => 'name',
              'label' => "Name", // Table column heading
              'type' => "text"
            ]
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
