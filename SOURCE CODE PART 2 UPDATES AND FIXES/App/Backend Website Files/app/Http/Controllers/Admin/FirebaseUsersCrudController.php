<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FirebaseUsersRequest as StoreRequest;
use App\Http\Requests\FirebaseUsersRequest as UpdateRequest;
use App\Models\Users;
use App\Models\UserOptions;

class FirebaseUsersCrudController extends CrudController
{
    private $tabs = [ 'Account Details', 'Officiant Details', 'Events', 'Officating Events', 'Payments' ];

    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\FirebaseUsers');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/user');
        $this->crud->setEntityNameStrings('Account', 'Accounts');
        $this->crud->denyAccess(['create']);

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        // $this->crud->setFromDb();

        $this->crud->setColumns([
            [
                'name'  => 'first_name',
                'label' => 'First',
            ],
            [
                'name'  => 'last_name',
                'label' => 'Last',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
            ],
            [
                'name'  => 'phone',
                'label' => 'Phone',
            ],
            [
              'name' => 'created_at',
              'label' => 'Created At'
            ]
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {


        $this->data['entry'] = $this->crud->getEntry($id);
        $eventDetails = '
          <div class="row">
            <div class="col-md-3">
              <p><b>Address</b></p>
            </div>
            <div class="col-md-9">
              <p>'.@json_decode(@UserOptions::find(@$this->data['entry']->id)->option_value, true)['address'].'</p>
            </div>
          </div>
          ';

        $this->crud->addField([
            'name' => 'uid',
            'type' => 'custom_html',
            'value' => $eventDetails,
            'tab' => $this->tabs[0]
        ]);
        // dd($this->crud->getEntry($id)->events());

        // $this->crud->addField([ // image
        //     'label' => "Profile Photo",
        //     'name' => "id",
        //     'type' => 'image',
        //     'upload' => true,
        //     'value' => 'https://placehold.it/160x160/00a65a/ffffff/&text=J',
        //     'crop' => true, // set to true to allow cropping, false to disable
        //     'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
        //     'tab' => $this->tabs[0]
        //     // 'prefix' => 'uploads/images/profile_pictures/' // in case you only store the filename in the database, this text will be prepended to the database value
        // ]);

        //User Details Tab
        $this->crud->addField([
            'name' => 'first_name',
            'label' => "First Name",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'last_name',
            'label' => "Last Name",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => "Email",
            'type' => 'email',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        $this->crud->addField([
            'name' => 'phone',
            'label' => "Phone",
            'type' => 'text',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-6'
             ],
            'tab' => $this->tabs[0]
        ]);

        // $this->crud->addField([
        //     'name' => 'password',
        //     'fake' => true,
        //     'label' => "Password",
        //     'type' => 'password',
        //     'wrapperAttributes' => [
        //        'class' => 'form-group col-md-6'
        //      ],
        //     'tab' => $this->tabs[0]
        // ]);
        //
        // $this->crud->addField([
        //     'name' => 'confirm_password',
        //     'fake' => true,
        //     'label' => "Confirm Password",
        //     'type' => 'password',
        //     'wrapperAttributes' => [
        //        'class' => 'form-group col-md-6'
        //      ],
        //     'tab' => $this->tabs[0]
        // ]);

        $this->crud->addField([
            'name' => 'id3',
            'fake' => true,
            'label' => "Enable Officiant",
            'type' => 'custom_html',
            'value' => '<hr>',
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
            'tab' => $this->tabs[0]
        ]);

        $user = Users::where('uid','=',$this->data['entry']->uid)->first();

        $this->crud->addField([
            'name' => 'officiant',
            'fake' => true,
            'label' => "Enable Officiant",
            'type' => 'select_from_array',
            'options' => [0 => 'Disabled', 1 => 'Enabled'],
            'value' => ($user->isOfficiant() ? 1 : 0),
            'wrapperAttributes' => [
               'class' => 'form-group col-md-12'
             ],
            'tab' => $this->tabs[0]
        ]);


        //Events Tab
        $eventsTable = '
          <table class="table datatable">
            <thead>
              <tr>
                <th>Event</th>
                <th>Payment</th>
              </tr>
            </thead>
            <tbody>';


        $events = $user->events;

        if (!is_null($events)){
          $limit = 4;
          foreach($events as $event){
            $limit = $limit - 1;
            if ($limit==0){ continue; }

            /*if ($event['role'] != 'creator'){
              continue;
            }*/
            $saleRecord = @$event->_saleRecord;
            $eventsTable .= '
                  <tr>
                    <td><a href="'.url("admin/events/".$event->id."/edit").'">'.$event->title.'</a></td>
                    <td><span class="label label-success">Paid : '.date('m/d/Y h:i A', $event->scheduled_date).'</span></td>
                  </tr>';
          }
        }

        $eventsTable .= '
            </tbody>
          </table>
        ';

        $this->crud->addField([
            'name' => 'events',
            'fake' => true,
            'label' => "Events",
            'type' => 'custom_html',
            'value' => $eventsTable,
            'tab' => $this->tabs[2]
        ]);


        //$this->crud->addField((array) json_decode($this->data['entry']->field)); // <---- this is where it's different
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getUpdateFields($id);
        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view($this->crud->getEditView(), $this->data);
    }

    public function store(StoreRequest $request)
    {

        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        $user = Users::where('id','=',$request->request->get('id'))->first();

        if ($request->request->has('officiant')){
          $officiantStatus = $request->request->get('officiant');
          $user->setOfficiantStatus($officiantStatus);
        }

        $request->request->remove('officiant');
        // return dd($request);


        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
