<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Vinkas\Firebase\Auth\AuthenticatesUsers;
use Vinkas\Firebase\Auth\User;
use App\Models\Users;
use App\Models\Participants;
use App\Models\Guests;

class NewAuthenticationController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Login method using Vinkas JWT Authentication.
     * @param  Request $request        Laravel Request, validated by $this->validator method.
     * @return Response $response      JSON Response
     */
    public function login(Request $request){
      try {
        $response = $this->postAuth($request);

        if ($response['success']){
          return response()->json(array(
              'success' => true,
              'data' => array(
                'user' => Users::find($response['user']->uid)->getProfile(),
                'token' => base64_encode($request->input('id_token'))
              )
            )
          );
        }else{
          return response()->json(array(
              'success' => false,
              'message' => $response['message']
            )
          );
        }
      } catch (Exception $e) {
        return dd($e);
      }
    }

    /**
     * Puesdo for Login method
     */
    public function signup(Request $request){
      return $this->login($request);
    }

    /**
     * Creates User Instance and stores in database.
     * @TODO Integrate emails, and cross database checks for invites etc.
     * @param  Array $data    Array contains userData
     * @return User $user     Vinkas User Instance Returned
     */
    public function create($data){
      $user = new User;
      $user = $user->where('uid', '=', $data['uid']);
      if ($user->exists()){
        return $user->first();
      }

      if ($data['email'] == null || $data['email'] == ""){ return $user; }

      $user = User::create([
        'uid' => $data['uid'],
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'phone' => $data['phone']
      ]);

      (new Participants)->assignInvitedEvents($user);
      (new Guests)->assignInvitedEvents($user);

      Users::find($data['uid'])->makeDefaultOptions($data);

      return $user; //dd($data);
    }
}
