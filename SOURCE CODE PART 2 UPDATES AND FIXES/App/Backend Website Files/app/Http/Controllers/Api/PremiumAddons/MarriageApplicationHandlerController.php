<?php

namespace App\Http\Controllers\Api\PremiumAddons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use App\Models\Users;
use App\Models\Events;
use App\Models\MarriageApplication;
use App\Models\Courts;

class MarriageApplicationHandlerController extends Controller
{
    public function handle(Request $request){
      if ($request->method() == 'GET'){
          return $this->handlePOST($request);
      }else if ($request->method() == 'POST'){
          return $this->handlePOST($request);
      }
    }

    public function handleGET(Request $request){
      return response()->json(array(
          "court" => array(),
          "your_information" => array(),
          "spouce_information" => array(),
          "officiant_information" => array(),
          "status" => "sent"
        )
      );
    }

    public function handlePOST(Request $request){
      $uid = Auth::user()->uid;
      $user = Users::getMyAccount();
      $eventId = $request->input('id');

      $marriageApplication = MarriageApplication::where(array(array('event','=',$eventId),array('uid','=',$uid)));
      if (!$marriageApplication->exists()){
        $marriageApplication = new MarriageApplication();
        $marriageApplication->event = $eventId;
        $marriageApplication->uid = $uid;
        $marriageApplication->court = 0;
        $marriageApplication->attachments = json_encode(array(
          'your' => array(),
          'spouce' => array()
        ));
        $marriageApplication->history = json_encode(array(array(
          "title" => "Pending Application",
          "date" => date('m/d/Y')
        )));
        $marriageApplication->save();
      }else{
        $marriageApplication = $marriageApplication->first();
      }


      return response()->json([
        'success' => true,
        'data' => array(
          'submitted' => (($marriageApplication->status == 0) ? false : true),
          'court' => $this->getCourt($marriageApplication->court),
          'your_information' => array(
            'user' => $user->getPublicProfile(),
            'attachments' => $this->getAttachments('your', json_decode($marriageApplication->attachments, true))
          ),
          'spouce_information' => array(
            'user' => $this->getSpouce($eventId),
            'attachments' => $this->getAttachments('spouce', json_decode($marriageApplication->attachments, true))
          ),
          'officiant_information' => array(
            'user' => $this->getOfficiant($eventId),
          )
        )
      ]);
    }

    public function getCourt($courtId){
      if ($courtId == 0){ $courtId = 1; }
      $court = Courts::find($courtId);

      return array(
        'disabled' => true,
        'court' => array(
          'id' => $court->id,
          'name' => $court->name
        )
      );
    }

    public function getSpouce($eventId){
      $event = Events::find($eventId);
      $participants = $event->_participants;
      $participant = null;
      foreach($participants as $_participant){
        if ($_participant->role == 'Bride' || $_participant->role == 'Groom'){
          $participant = $_participant;
        }
      }

      if (is_null($participant)){
        return array(
          'personal' => array(
            'id' => 0,
            'name' => array(
              'first' => 'Add',
              'last' => 'Spouce',
              'full' => 'Add Spouce'
            ),
            'email' => 'Select your spouce from the participants tab.',
            'avatar' => 'http://webwedmobile.net/images/notification_icon.png'
          )
        );
      }else{
        if ($participant->uid == "0"){
          return array(
            'personal' => array(
              'id' => 0,
              'name' => array(
                'first' => @explode(' ', $participant->name)[0],
                'last' => @explode(' ', $participant->name)[1],
                'full' => $participant->name
              ),
              'email' => $participant->identifier,
              'avatar' => url('api/user/'.str_replace(' ', '_', $participant->name).'/avatar')
            )
          );
        }else{
          return $participant->_user->getPublicProfile();
        }
      }
    }

    public function getOfficiant($eventId){
      $event = Events::find($eventId);
      $participants = $event->_participants;
      $participant = null;
      foreach($participants as $_participant){
        if (strtolower($_participant->role) == 'officiant'){
          $participant = $_participant;
        }
      }


      if (is_null($participant)){
        return array(
          'personal' => array(
            'id' => 0,
            'name' => array(
              'first' => 'Add',
              'last' => 'Officiant',
              'full' => 'Add Officiant'
            ),
            'email' => 'Select your officiant from the participants tab.',
            'avatar' => 'http://webwedmobile.net/images/notification_icon.png'
          )
        );
      }else{
        if ($participant->uid == "0"){
          return array(
            'personal' => array(
              'id' => 0,
              'name' => array(
                'first' => @explode(' ', $participant->name)[0],
                'last' => @explode(' ', $participant->name)[1],
                'full' => $participant->name
              ),
              'email' => $participant->identifier,
              'avatar' => url('api/user/'.str_replace(' ', '_', $participant->name).'/avatar')
            )
          );
        }else{
          return $participant->_user->getPublicProfile();
        }
      }
    }

    public function getAttachments($role, $attachments){
      $attachments = $attachments[$role];
      $missing = 4 - sizeof($attachments);
      for ($i=0; $i < $missing; $i++) {
        $attachments = array_merge($attachments, array(array(
          'id' => 1,
          'value' => '',
          'date' => strtotime('now')
        )));
      }

      return $attachments;
    }

    public function handleAttachment(Request $request){
      $uid = Auth::user()->uid;
      $user = Users::getMyAccount();
      $eventId = $request->input('id');
      $file = $request->input('file');
      $person = $request->input('person');

      $marriageApplication = MarriageApplication::where(array(array('event','=',$eventId),array('uid','=',$uid)));
      $marriageApplication = $marriageApplication->first();
      $attachments = json_decode($marriageApplication->attachments, true);

      if ($person == 'your'){
        $attachments['your'] = array_merge($attachments['your'], array(array(
          'id' => 1,
          'value' => $file,
          'date' => strtotime('now')
        )));
      }else{
        $attachments['spouce'] = array_merge($attachments['spouce'], array(array(
          'id' => 1,
          'value' => $file,
          'date' => strtotime('now')
        )));
      }

      $marriageApplication->attachments = json_encode($attachments);
      $marriageApplication->save();

      return $this->handlePOST($request);
    }

    public function courts(Request $request){
      $courts = Courts::all();
      $_courts = array();

      foreach($courts as $court){
        $_courts = array_merge($_courts, array(
          array(
            'id' => $court->id,
            'name' => $court->name,
            'address' => $court->address,
            'disabled' => false
          )
        ));
      }

      return response()->json(array(
        'success' => true,
        'data' => array(
          'courts' => $_courts
        )
      ));
    }

    public function selectCourt(Request $request){
      $uid = Auth::user()->uid;
      $user = Users::getMyAccount();
      $eventId = $request->input('eventId');
      $courtId = $request->input('courtId');
      $event = Events::find($eventId);
      $court = Courts::find($courtId);
      $marriageApplication = MarriageApplication::where(array(array('event','=',$eventId),array('uid','=',$uid)))->first();

      $marriageApplication->court = $courtId;
      $marriageApplication->save();

      return response()->json(array(
        'success' => true,
        'data' => array(
          'selected' => true
        )
      ));
    }

    public function submitApplication(Request $request){
      $uid = Auth::user()->uid;
      $user = Users::getMyAccount();
      $eventId = $request->input('eventId');
      $event = Events::find($eventId);
      $marriageApplication = MarriageApplication::where(array(array('event','=',$eventId),array('uid','=',$uid)))->first();

      $marriageApplication->status = 1;
      $marriageApplication->save();

      $this->addToHistory($eventId, 'Application Submitted');
      $this->addToHistory($eventId, 'Awaiting E-Signatures');

      return response()->json(array(
        'success' => true,
        'data' => array(
          'submitted' => true
        )
      ));
    }

    public function history($eventId, Request $request){
      $uid = Auth::user()->uid;
      $user = Users::getMyAccount();
      $event = Events::find($eventId);

      $marriageApplication = MarriageApplication::where('event', '=', $eventId)->first();

      return response()->json(array(
        'success' => true,
        'data' => array(
          'history' => array(
            array(
              "title" => "Pending Application",
              "date" => date('m/d/Y')
            )
          )
        )
      ));
    }

    public function receivedEsignatures(Request $request){
      $this->addToHistory($eventId, 'E-Signatures Received');
    }

    public function licenseIssued(Request $request){
      $this->addToHistory($eventId, 'License Issued');
    }

    public function addToHistory($eventId, $title){
      $uid = Auth::user()->uid;
      $user = Users::getMyAccount();
      $event = Events::find($eventId);
      $marriageApplication = MarriageApplication::where(array(array('event','=',$eventId),array('uid','=',$uid)))->first();
      $histories = json_decode($marriageApplication->history, true);
      $histories = array_merge($histories, array(array(
        'title' => $title,
        'date' => date('m/d/Y')
      )));
      $marriageApplication->history = json_encode($histories);
      $marriageApplication->save();
    }
}
