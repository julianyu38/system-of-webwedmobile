<?php

namespace App\Http\Controllers\Api\PremiumAddons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Events;

class MarriageRegistryHandlerController extends Controller
{
    public function handle($eventId, Request $request){
      $event = Events::find($eventId);
      $options = $event->_options;
      $link = '';

      foreach($options as $option){
        if ($option['option_key'] == 'registry_link'){
          $link = $option['option_value'];
        }
      }

      return response()->json(array(
        'success' => true,
        'data' => array(
          'registry_link' => $link
        )
      ));
    }

    public function save($eventId, Request $request){
      $event = Events::find($eventId);
      $options = $event->_options()->where('option_key', '=', 'registry_link');
      $link = $request->input('link');

      if ($options->exists()){
        $options = $options->first();
        $options['option_value'] = $link;
        $options->save();
      }else{
        $event->_options()->create([
          'option_key' => 'registry_link',
          'option_value' => $link
        ]);
      }

      return response()->json(array(
        'success' => true,
        'data' => array(
          'registry_link' => $link
        )
      ));
    }
}
