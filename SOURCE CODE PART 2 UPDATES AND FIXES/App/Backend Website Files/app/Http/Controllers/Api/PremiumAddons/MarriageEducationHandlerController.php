<?php

namespace App\Http\Controllers\Api\PremiumAddons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkAttachment;
use App\Models\Users;
use Auth;

class MarriageEducationHandlerController extends Controller
{
    public function handle(Request $request){
      $user = Users::find(Auth::user()->uid);
      $this->sendEmail($user->first_name . ' ' . $user->last_name, $user->email);

      return response()->json([
        'success' => true,
        'data' => array(
          'status' => 'completed'
        )
      ]);
    }

    public function sendEmail($name, $email){
      try{
        // Create Client
        $client = new PostmarkClient("75a49590-d333-446b-9613-29a333504d71");

        // Make a request
        $sendResult = $client->sendEmailWithTemplate(
          "noreply@webwedmobile.com",
          $email,
          "4617561",
          [
            "invite_sender_name" => $name,
            "action_url" => url('/assets/certificate_of_completion.pdf'),
            "product_name" => "WebWedMobile",
            "invite_sender_organization_name" => "WebWedMobile",
            "product_url" => "http://webwedmobile.com",
            "name" => $name,
            "support_email" => "John Smith",
            "live_chat_url" => "John Smith",
            "help_url" => "John Smith",
            "company_name" => "WebWedMobile",
            "company_address" => "John Smith"
          ]
        );

        //echo $sendResult->message ."\r\n";

      }catch(PostmarkException $ex){
      	// If client is able to communicate with the API in a timely fashion,
      	// but the message data is invalid, or there's a server error,
      	// a PostmarkException can be thrown.
      	// echo $ex->httpStatusCode;
      	// echo $ex->message;
      	// echo $ex->postmarkApiErrorCode;

      }catch(Exception $generalException){
      	// A general exception is thrown if the API
      	// was unreachable or times out.
      }
    }

    public function retrive(Request $request){
      return view('welcome');
    }
}
