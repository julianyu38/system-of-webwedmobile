<?php


namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Events;
use App\Models\Users;
use Auth;
use App\Models\Images;
use App\Models\OpenTokController;
use App\Models\EventRoles;
use App\Models\Packages;
use App\Models\Premium_addons;
use App\Models\Event_options;
use App\Models\Invites;
use App\Models\Emojis;
use App\Models\StripeController;
use Illuminate\Support\Facades\Log;
use Postmark\PostmarkClient;
use Postmark\Models\PostmarkAttachment;
use App\Models\UserNotifications;
use Postmark\Models\PostmarkException;
use Twilio\Rest\Client;
require __DIR__ . '/../../../../twilio-php-master/Twilio/autoload.php';

class EventController extends Controller
{
    public function __construct(){

    }

    public function sendEmail($template, $email, $name, $subject = '', $body = '', $eventId = 0, $video = ''){
      $template_id = 3606261;
      if ($template == 'guest_invite') { $template_id = 3606261; }
      if ($template == 'participant_invite') { $template_id = 3606262; }

      try{
        // Create Client
        $client = new PostmarkClient("75a49590-d333-446b-9613-29a333504d71");

        // Make a request
        $sendResult = $client->sendEmailWithTemplate(
          "noreply@webwedmobile.com",
          $email,
          $template_id,
          [
            "invite_sender_name" => $name,
            "custom_subject" => ($subject == '' ? "WebWedMobile Event" : $subject),
            "custom_body" => ($body == '' ? "You have been invited to participant in a WebWedMobile Event." : $body),
            "action_url" => url('/api/join/'.$eventId),
            "product_name" => "WebWedMobile",
            "invite_sender_organization_name" => "WebWedMobile",
            "product_url" => "http://webwedmobile.com",
            "name" => $name,
            "support_email" => "John Smith",
            "live_chat_url" => "John Smith",
            "help_url" => "John Smith",
            "company_name" => "WebWedMobile",
            "company_address" => "John Smith",
            "video_url" => ($video != '' ? url('email/view_video/'.base64_encode(urlencode($video))) : '')
          ]
        );

        //echo $sendResult->message ."\r\n";

      }catch(PostmarkException $ex){
      	// If client is able to communicate with the API in a timely fashion,
      	// but the message data is invalid, or there's a server error,
      	// a PostmarkException can be thrown.
      	// echo $ex->httpStatusCode;
      	// echo $ex->message;
      	// echo $ex->postmarkApiErrorCode;

      }catch(Exception $generalException){
      	// A general exception is thrown if the API
      	// was unreachable or times out.
      }
    }

    /**
     * getEvent provides an interface for getting an event with an id and type.
     * @param  int $id      Public id of the event.
     * @param  string $type minimal or detailed
     * @return Response       Returns Laravel JSON Response or array if requested.
     */
    public function getEvent($id, $type = 'minimal', $wantsArray = false){
      if ($type == 'share'||$id == 'share'){ return view('welcome'); }

      $event = Events::find($id);

      if ($wantsArray){
        return $event->makeArray($type);
      }else{
        return response()->json(array(
          'success' => true,
          'data' => $event->makeArray($type)
        ));
      }
    }

    public function getEvents($id, Request $request){
      $user = Users::find(Auth::user()->uid);

      if ($id == $user->id){
        return response()->json([
          'success' => true,
          'data' => array(
            "future" => $user->getEvents($user, false),
            "past" => array()
          )
        ]);
      }else{
        return response()->json([
          'success' => false,
          'data' => array()
        ]);
      }
    }

    public function getEventConfigs(Request $request){
      $addons = Premium_addons::all();
      $availableAddons = array();

      foreach($addons as $addon){
        $availableAddons = array_merge($availableAddons, array(
            array(
              "id" => $addon->id,
              "title" => $addon->title,
              "price" => $addon->price,
              "name" => $addon->title
            )
          )
        );
      }

      //Disabled Premium addons

      return response()->json(array(
          'success' => true,
          'data' => array(
            'packages' => (new Packages())->getPackages(),
            'addons' => $availableAddons,
            'roles' => EventRoles::get()
          )
        )
      );
    }

    private function generateEventCode(){
      return mt_rand(9999,99999);
    }

    public function createEvent(Request $request){
      if ($request->has('photo')){
        $path = $request->input('photo');//$request->file('photo')->store('photos');
      }else{
        $path = 'assets/images/Placeholder.svg';
      }

      $data = array(
        'photo' => $path,
        'title' => $request->input('title'),
        'date' => strtotime(($request->has('date') ? date('m/d/Y', $request->input('date')) : '') . ' ' . ($request->has('time') ? date('h:i A', $request->input('time')) : '')),
        'package' => $request->input('package'),
        'addons' => $request->input('addons')
      );


      $stripeToken = $request->input('token');
      $uid = Auth::user()->uid;
      $package = Packages::find($data['package']['id']);
      @(new StripeController)->makePayment(@$package->price*100, @$stripeToken);


      //Create Event Instance
      $event = new Events([
        'title' => $data['title'],
        'event_code' => $this->generateEventCode(),
        'uid' => $uid,
        'package' => $data['package']['id'],
        'scheduled_date' => $data['date'],
        'status' => 1
      ]);

      //Save prior to creating sale record
      $event->save();

      //Save image
      if ($data['photo']){
        $image = new Images([
          'name' => 'event'.$event->id,
          'value' => $data['photo']
        ]);
        $image->save();
      }

      //Create Sale Record
      //Notify webwed of sale record
      //N
      $total = 0;
      $addons = json_encode($data['addons']);
      $addons = json_decode($addons);
      foreach($addons as $addon){
        $total += $addon->price;
      }
      $total += $package->price;
      $event->_saleRecord()->create([
        'uid' => $uid,
        'price_paid' => $total,
        'txn_id' => 'TXNID',
        'coupon_code' => (@$transaction['coupon'] ? @$transaction['coupon'] : '')
      ]);

      $event->_options()->create([
        'option_key' => 'addons',
        'option_value' => json_encode($data['addons'])
      ]);

      //Add user as participant
      $event->_participants()->create([
        'uid' => $uid,
        'role' => 'creator'
      ]);

      $event->_options()->create([
        'option_key' => 'remaining_time',
        'option_value' => @strval(intval($package->time_alotment))
      ]);

      //Assign options
      $event->makeDefaultOptions();

      UserNotifications::create([
        'uid' => $uid,
        'avatar' => url('images/notification_icon.png'),
        'title' => 'Event Created',
        'message' => 'Your event "'.htmlentities($event->title).'" has been successfully created.',
        'timestamp' => strtotime('now'),
        'status' => 'unread'
      ]);

      return response()->json([
        'success' => true,
        'data' => $event->makeArray('details')
      ]);
    }

    function getUserIdFromIdentifier($email, $phone){
      if ($phone == '' || $phone == ' '){ $phone = '-1'; }
      $user = Users::where('email', $email)->orWhere('phone', $phone);
      if ($user->exists()){
        $user = $user->first();
        Log::info('USER: ' . $email . ' - ' . $phone . ' - ' . $user['uid']);
        return $user['uid'];
      }else{
        return 0;
      }
    }

    public function configureInvite($eventId, Request $request){
      $subject = $request->input('subject');
      $body = $request->input('message');
      $event = Events::find($eventId);

      $invites = Invites::where('event','=',$eventId);
      if (!$invites->exists()){
        $invites = Invites::create([
          'uid' => Auth::user()->id,
          'event' => $eventId,
          'title' => $subject,
          'description' => $body
        ]);
      }else{
        $invites = $invites->first();
      }

      $guests = $event->_guests;
      foreach($guests as $guest){
        if ($request->has('video')){
          $video = $request->input('video');
          $this->sendEmail('guest_invite', $guest->identifier, $guest->name, $subject, $body, $event->id, $video);
        }else{
          $this->sendEmail('guest_invite', $guest->identifier, $guest->name, $subject, $body, $event->id);
        }
      }

      UserNotifications::create([
        'uid' => Auth::user()->uid,
        'avatar' => url('images/notification_icon.png'),
        'title' => 'Evite Sent',
        'message' => 'Your evite for "'.htmlentities($event->title).'" has been successfully sent to all guests.',
        'timestamp' => strtotime('now'),
        'status' => 'unread'
      ]);

      return response()->json([
        'success' => true
      ]);
    }

    public function inviteOfficiant(Request $request){
      $eventId = $request->input('event');
      $officiantId = $request->input('officiant');

      return response()->json([
        'success' => false,
        'error' => array(
          'title' => 'Error',
          'message' => json_encode($request->all())
        )
      ]);
    }

    public function inviteGuests($eventId, $request){
      $contacts = $request->input('contacts');
      $event = Events::find($eventId);
      $guests = $contacts;

      $tmp = array();

      $totalImported = 0;

      foreach ($guests as $guest) {
        if (isset($guest['identifier'])){
          $email = $guest['identifier'];
          $phone = $guest['identifier'];
        }else{
          $email = (is_null($guest['email']) ? $guest['phone'] : $guest['email']);
          $phone = (is_null($guest['phone']) ? $guest['email'] : $guest['phone']);
        }

        if ($email == '' && $phone == '' && $guest['name'] == ''){ continue; }

        if ($phone == '' && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
          return response()->json([
            'success' => false,
            'error' => array(
              'title' => 'Invalid Email',
              'message' => 'Provide a valid email address or phone number and try again.'
            )
          ]);
        }

        $exists = false;

        $user = new Users;
        $user = $user->where('email','=',$email)->orWhere('phone','=',$phone);
        if ($user->exists()){
          $user = $user->first();
          $hasUser = true;
        }else{
          $hasUser = false;
        }

        foreach($event->_guests as $g){

          if ($hasUser){
            if ($g['uid'] == $user->uid){
              $exists = true;
            }

            if ($g['identifier'] == $email || $g['identifier'] == $phone){
              $exists = true;
            }
          }else{
            if ($g['identifier'] == $email || $g['identifier'] == $phone){
              $exists = true;
            }
          }
        }

        if (!$exists){
          $userId = $this->getUserIdFromIdentifier($email, $phone);
          if ($userId != 0){
            $user = User::find($userId);

            if (!($event->_guests()->where('uid',$userId)->exists())){
              $event->_guests()->create([
                'identifier' => (is_null($email) ? $phone : $email),
                'uid' => $userId,
                'name' => $guest['name'],
                'status' => 0
              ]);
            }
          }else{
            $event->_guests()->create([
              'identifier' => (is_null($email) ? $phone : $email),
              'uid' => 0,
              'name' => $guest['name'],
              'status' => 0
            ]);
          }

          $invite = Invites::where('event','=',$event->id);
          if ($invite->exists()){
            $invite = $invite->first();
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $this->sendEmail('guest_invite', $email, $guest['name'], $invite->title, $invite->description, $event->id);
            }else{
              $sid = 'ACb0efa2a2ed954d08e59f6ad3292f85e7';
              $token = 'fae018d4ae769cddb183056b298f09b0';
              $client = new Client($sid, $token);

              $client->messages->create(
                  '+1'.str_replace('+1', '', str_replace('-', '', str_replace(' ', '', $phone))),
                  array(
                      'from' => '+12028167772',
                      'body' => "You have been invited as a guest to a WebWed Mobile event! Download the app now, and sign up using your phone number and email to gain access to this event. http://webwedmobile.net"
                  )
              );
            }
          }
        }
      }

      return response()->json([
        'success' => true
      ]);
    }

    public function respondToInvite($eventId, $response, Request $request){
      $user = Auth::user();

      $event = Events::find($eventId);
      $participants = $event->_participants;
      $isParticipant = false;

      foreach ($participants as $participant) {
        if ($participant->uid == $user->uid){
          $participant->status = $response;
          $participant->save();
          $isParticipant = true;
        }
      }

      $guests = $event->_guests;

      foreach ($guests as $guest) {
        if ($guest->uid == $user->id){
          $guest->status = $response;
          $guest->save();
        }
      }

      $eventUser = $event->_user;
      // $eventUser->notify(new \App\Notifications\InviteResponse($user, $response));

      if ($isParticipant){
        UserNotifications::create([
          'uid' => $eventUser->uid,
          'avatar' => url('images/notification_icon.png'),
          'title' => 'Participant Responded',
          'message' => $user->first_name . ' responded to your participant invite for "'.htmlentities($event->title).'".',
          'timestamp' => strtotime('now'),
          'status' => 'unread'
        ]);
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function getEventPrivacy($eventId, $privacy = null){
      $event = Events::find($eventId);

      if (is_null($event)){
        return response()->json([
          'success' => false,
          'data' => array(

          ),
          'error' => array(
            'title' => 'Invalid Event',
            'message' => 'That event does not exist. Enter another event id and try agian.'
          )
        ]);
      }

      if (!is_null($privacy)){
        $event->updateOption('privacy', ($privacy == 1 ? 0 : 1));
        return response()->json([
          'success' => true,
          'data' => array(
            'public' => $event->privacy()
          )
        ]);
      }else{
        if ($event->privacy()){
          return response()->json([
            'success' => true,
            'data' => array(
              'public' => $event->privacy()
            )
          ]);
        }else{
          return response()->json([
            'success' => false,
            'data' => array(
              'public' => $event->privacy()
            ),
            'error' => array(
              'title' => 'Event Private',
              'message' => 'This event is private, and you have not been invited.'
            )
          ]);
        }
      }
    }

    public function addParticipant($eventId, Request $request){
      Log::info('REQUEST INFORMATION: '.json_encode($request->all()));
      if ($request->has('index')){
        $index = $request->input('index');
      }else{
        $index = -1;
      }

      if ($request->has('user')){
        $user = $request->input('user');

        $event = Events::find($eventId);

        if (is_null($event)){
          return response()->json([
            'success' => false,
            'data' => array(),
            'error' => array(
              'title' => 'No event exists',
              'message' => 'You have to provide a valid event.'
            )
          ]);
        }

        if (isset($user['user'])){
          if (is_int($user['user'])){
            //Assume officiant
            $_user = new Users;
            $_user = $_user->where('id','=',$user['user']);
            if ($_user->exists()){
              $_user = $_user->first();
              $email = $_user->email;
              $phone = $_user->phone;
              $name = stripslashes(@$_user->first_name) . ' ' . stripslashes(@$_user->last_name);
              $userId = $_user->uid;
            }else{
              return response()->json([
                'success' => false,
                'error' => array(
                  'title' => 'No officiant exists',
                  'message' => 'You have to provide a valid officiant.'
                )
              ]);
            }
          }
        }else{
          if (isset($user['identifier'])){
            $email = $user['identifier'];
            $phone = $user['identifier'];
          }else{
            $email = (is_null($user['email']) ? $user['phone'] : $user['email']);
            $phone = (is_null($user['phone']) ? $user['email'] : $user['phone']);
            $name = ((!empty($user['name'])) ? stripslashes($user['name']) : 'John Doe');
          }

          if ($email == '' && $phone == ''){
            return response()->json([
              'success' => false,
              'error' => array(
                'title' => 'No valid email or phone',
                'message' => 'Provide a valid email or phone number and try again.'
              )
            ]);
          }

          $userId = $this->getUserIdFromIdentifier($email, $phone);
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL) && !filter_var($phone, FILTER_SANITIZE_NUMBER_INT)) {
          return response()->json([
            'success' => false,
            'error' => array(
              'title' => 'No valid email or phone',
              'message' => 'Provide a valid email or phone number and try again.'
            )
          ]);
        }



        if ($userId == 0){
          //User doesnt exist.
          //$_user = User::find($userId);
          //$_user->notify(new GuestInvite($_user, Events::find($eventId)));'
          // Notification::send()
        }else{
          //User does exist.
          $_user = Users::find($userId);
          $_user->notify(new GuestInvite($_user, Events::find($eventId)));
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
          try{
            $this->sendEmail('participant_invite', $email, $name, '', '', $event->id);
          }catch(PostmarkException $ex){
            return response()->json([
              'success' => false,
              'data' => Events::find($eventId)->makeArray('details'),
              'error' => array(
                'title' => 'Email is marked inactive',
                'message' => 'This email has been marked as inactive by our system. Try a different email.'
              )
            ]);
          }
        }else{
          $sid = 'ACb0efa2a2ed954d08e59f6ad3292f85e7';
          $token = 'fae018d4ae769cddb183056b298f09b0';
          $client = new Client($sid, $token);

          $client->messages->create(
              '+1'.str_replace('+1', '', str_replace('-', '', str_replace(' ', '', $phone))),
              array(
                  'from' => '+12028167772',
                  'body' => "You have been invited to participant in a WebWed Mobile event! Download the app now, and sign up using your phone number and email to gain access to this event. http://webwedmobile.net"
              )
          );
        }

        if (isset($user['user'])){
          $role = 'officiant';
        }else{
          $role = 'unknown';
        }

        $i = 0;
        $updated = false;
        $participants = $event->_participants;
        foreach($participants as $participant){
          if ($participant->role == 'creator'){ continue; }
          if ($i == $index){
            $participant->identifier = $email;
            $participant->name = $name;
            $participant->uid = $this->getUserIdFromIdentifier($email, $phone);
            $participant->role = $role;
            $participant->status = 0;
            $participant->save();
            $updated = true;
          }
          $i += 1;
        }

        if ($updated == false){
          $participants = $event->_participants()->create([
            'identifier' => $email,
            'name' => $name,
            'uid' => $this->getUserIdFromIdentifier($email, $phone),
            'role' => $role,
            'status' => 0
          ]);
        }

        UserNotifications::create([
          'uid' => Auth::user()->uid,
          'avatar' => url('images/notification_icon.png'),
          'title' => 'Participant Invited',
          'message' => $name . ' has been invited to your event "'.htmlentities($event->title).'" as a participant.',
          'timestamp' => strtotime('now'),
          'status' => 'unread'
        ]);

      }else{
        return response()->json([
          'success' => false,
          'data' => Events::find($eventId)->makeArray('details'),
          'error' => array(
            'title' => 'No participant provided',
            'message' => 'You have to provide a participant.'
          )
        ]);
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function updateParticipant($eventId, $participantId, Request $request){
      $user = Users::find(Auth::user()->uid);
      $event = Events::find($eventId);
      $participants = $event->_participants;
      $index = 0;
      foreach($participants as $participant){
        $role = EventRoles::find($request->input('role'));
        if ($participant->role == 'creator') { continue; }
        $participant->role = $role->name;
        if ($index == $participantId){
          $participant->save();
        }
        $index += 1;
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function removeParticipant($eventId, $participantId, Request $request){
      $user = Users::find(Auth::user()->uid);
      $event = Events::find($eventId);
      $participants = $event->_participants;
      foreach($participants as $participant){
        $role = EventRoles::find($request->input('role'));
        if ($participant->role == 'creator') { continue; }
        //$participant->role = $role->name;
        //$participant->save();
      }
      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function updateGuests($eventId, Request $request){
      $contacts = $request->input('contacts');
      if (sizeof($contacts) == 0){
        return response()->json([
          'success' => false,
          'data' => Events::find($eventId)->makeArray('details'),
          'error' => array(
            'title' => 'No guests provided',
            'message' => 'You must provide guests to add to your event.'
          )
        ]);
      }else{
        UserNotifications::create([
          'uid' => Auth::user()->uid,
          'avatar' => url('images/notification_icon.png'),
          'title' => 'Guests Imported',
          'message' => sizeof($contacts) . ' guests have been imported for "'.htmlentities(Events::find($eventId)->title).'".',
          'timestamp' => strtotime('now'),
          'status' => 'unread'
        ]);
        $this->inviteGuests($eventId, $request);
      }

      return response()->json([
        'success' => true,
        'data' => Events::find($eventId)->makeArray('details')
      ]);
    }

    public function startEvent($eventId, $userId){
      $openTok = new OpenTokController();
      return response()->json([
        'success' => true,
        'data' => array(
          'opentok' => $openTok->create($eventId, $userId),
          'numberOfParticipants' => sizeof(Events::find($eventId)->_participants)
        )
      ]);
    }

    public function stopEvent($eventId, $userId){
      $openTok = new OpenTokController();
      $openTok->stop($eventId, $userId);
      return response()->json([
        'success' => true,
        'data' => array()
      ]);
    }

    public function opentokWebhook(Request $request){
      Log::info('JSON INFORMATION: '.$request->getContent());
      $json = json_decode($request->getContent(),true);

      if (!isset($json['event'])){ return response()->json(array('error'=>'No event provided.')); }

      switch(strtolower($json['event'])){
        case "connectioncreated":
          $this->connectionCreated($request);
          break;

        case "connectiondestroyed":
          $this->connectionDestroyed($request);
          break;

        case "streamcreated":
          $this->streamCreated($request);
          break;

        case "streamdestroyed":
          $this->streamDestroyed($request);
          break;

        case "archive":
          $this->archiveGenerated($request);
          break;

        default:
          break;
      }
    }

    public function connectionCreated(Request $request){
      //$json = Request::json();
      // $json = '{
      //     "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
      //     "projectId": "123456",
      //     "event": "connectionCreated",
      //     "timestamp": 1470257688309,
      //     "connection": {
      //         "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
      //         "createdAt": 1470257688143,
      //         "data": "TOKENDATA"
      //     }
      // }';
      //
      // $json = json_decode($json);

      // $sessionId = $json->input('sessionId');
      // $option = new Event_options;
      // $option = $option->where('option_value', $sessionId);
      // if ($option->exists()){
      //   $option = $option->first();
      //   $event = $option->event;
      //   $event->updateOption('total_viewers', strval(intval($event->getOption('total_viewers')) + 1));
      //   $event->updateOption('current_viewers', strval(intval($event->getOption('current_viewers')) + 1));
      // }
    }

    public function connectionDestroyed(Request $request){
      $json = json_decode($request->getContent(), true);
      // return dd($json);
      // $json = '{
      //     "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
      //     "projectId": "123456",
      //     "event": "connectionDestroyed",
      //     "reason": "clientDisconnected",
      //     "timestamp": 1470258896953,
      //     "connection": {
      //         "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
      //         "createdAt": 1470257688143,
      //         "data": ""
      //     }
      // }';
      //
      // $json = json_decode($json);

      // $sessionId = $json->input('sessionId');
      // $option = new Event_options;
      // $option = $option->where('option_value', $sessionId);
      // if ($option->exists()){
      //   $option = $option->first();
      //   $event = $option->event;
      //   $event->updateOption('current_viewers', strval(intval($event->getOption('current_viewers')) - 1));
      // }
    }

    public function streamCreated(Request $request){
      if ($request->getContent() == ''){ return; }

      $json = json_decode($request->getContent(), true);

      $sessionId = $json['sessionId'];
      $option = new Event_options;
      $option = $option->where('option_value', $sessionId);
      if ($option->exists()){
        $option = $option->first();
        $event = $option->event;
        $opentok = new OpenTokController();
        $opentok->startLiveStream($event, $sessionId);
      }
    }

    public function streamDestroyed(Request $request){
      if ($request->getContent() == ''){ return; }
      // $json = '{
      //     "sessionId": "2_MX4xMzExMjU3MX5-MTQ3MDI1NzY3OTkxOH45QXRr",
      //     "projectId": "123456",
      //     "event": "streamDestroyed",
      //     "reason": "clientDisconnected",
      //     "timestamp": 1470258896953,
      //     "stream": {
      //         "id": "63245362-e00e-4834-8371-9397deb3e452",
      //         "connection": {
      //             "id": "c053fcc8-c681-41d5-8ec2-7a9e1434a21e",
      //             "createdAt": 1470257688143,
      //             "data": ""
      //         },
      //         "createdAt": 1470258845416,
      //         "name": "",
      //         "videoType": "camera"
      //     }
      // }';

      $json = json_decode($request->getContent());
    }

    public function archiveGenerated(Request $request){
      if ($request->getContent() == ''){ return; }
      $json = json_decode($request->getContent(), true);

      $sessionId = $json['sessionId'];
      $option = new Event_options;
      $option = $option->where('option_value', $sessionId);
      if ($option->exists()){
        //NEEDS FIX, DISABLES ARCHIVE AT THE MOMENT.
        if (strtolower($json['status']) != 'available'){
          return;
        }
        Log::info('OPTION EXISTS');
        $option = $option->first();
        $event = Events::where(array('id'=>$option->event))->first();
        $event->status = 2;
        $event->save();
        $event->_options()->create([
          'option_key' => 'archive_id',
          'option_value' => $json['id']
        ]);
      }else{
        Log::info('OPTION DOESNT EXIST');
      }
    }

    public function getArchive($eventId, Request $request){
      $openTok = new OpenTokController();
      return response()->json(array(
        'success' => true,
        'video' => $openTok->getArchive($eventId)
      ));
    }

    public function getMinutes($id, Request $request){
      return response()->json(array(
        'success' => true,
        'minutes' => intval(Events::find($id)->getOption('remaining_time'))
      ));
    }

    public function getEmojis(Request $request){
      return response()->json(array(
        'success' => true,
        'data' => array(
          'images' => Emojis::all()
        )
      ));
    }

    public function streamEvent(Request $request){
      return view('stream', []);
    }

    public function handleFileUpload(Request $request){
      $path = $request->file('picture')->store('files');
      return response()->json([
        "success" => true,
        "data" => array(
          "path" => $path
        )
      ]);
    }

    public function broadcast(){
      $openTok = new OpenTokController();
      $openTok->startLiveStream();
    }

}
