<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\PremiumAddons\MarriageApplicationHandlerController;
use App\Http\Controllers\Api\PremiumAddons\MarriageLicenseHandlerController;
use App\Http\Controllers\Api\PremiumAddons\MarriageEducationHandlerController;
use App\Http\Controllers\Api\PremiumAddons\MarriageRegistryHandlerController;

class AddonHandlerController extends Controller
{
    public function handle(Request $request, $addon){
      switch ($addon) {
        case 'marriage_application':
          return (new MarriageApplicationHandlerController())->handle($request);
          break;

        case 'marriage_license':
          return (new MarriageLicenseHandlerController())->handle($request);
          break;

        case 'marriage_education':
          return (new MarriageEducationHandlerController())->handle($request);
          break;

        case 'marriage_registry':
          return (new MarriageRegistryHandlerController())->handle($request);
          break;

        default:
          # code...
          break;
      }
    }
}
