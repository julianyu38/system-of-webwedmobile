<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use Avatar;
use Auth;
use App\Models\Officiants;

class NewUserController extends Controller
{

  /**
   * getUser returns the profile for a given user.
   * @param  number   $id   The public ID for a user.
   * @return Response       JSON Response
   */
  public function getUser($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      return response()->json($user->getProfile());
    }
  }

  public function getNotifications($id){
    $user = new Users;
    $user = $user->where('id','=',$id);
    if ($user->exists()){
      $user = $user->first();
      return response()->json($user->getNotifications());
    }
  }

  public function getAvatar($id){
    $user = new Users;

    if (!is_numeric($id)){
      $imgstr = Avatar::create((explode('_', $id)[0] ? explode('_', $id)[0] : $id) . ' ' . @(explode('_', $id)[1] ? explode('_', $id)[1] : ''))->__toString();
      $new_data=explode(";",$imgstr);
      $type=$new_data[0];
      $data=explode(",",$new_data[1]);
      header("Content-type:".$type);
      echo base64_decode($data[1]);
    }else{
      $user = $user->where('id','=',$id);
      if ($user->exists()){
        $user = $user->first();
        $imgstr = Avatar::create($user->first_name . ' ' . $user->last_name)->__toString();
        $new_data=explode(";",$imgstr);
        $type=$new_data[0];
        $data=explode(",",$new_data[1]);
        header("Content-type:".$type);
        echo base64_decode($data[1]);
      }
    }
  }

  public function listOfficiants(Request $request){
    $officaints = Officiants::all();

    $_officiants = array();
    foreach($officaints as $officiant){
      $user = Users::find($officiant->uid);
      if (is_null($user)){ continue; }
      $_officiants = array_merge($_officiants, array(
          $user->getProfile()
        )
      );
    }

    return response()->json([
      'success' => true,
      'data' => array(
        'officiants' => $_officiants
      )
    ]);

    // return response()->json([
    //   'success' => true,
    //   'data' => array(
    //     'officiants' => array(
    //       array(
    //         'personal' => array(
    //           'id' => 1,
    //           'avatar' => '',
    //           'name' => array(
    //             'first' => 'John',
    //             'last' => 'Doe',
    //             'full' => 'John Doe'
    //           ),
    //           'location' => array(
    //             'address' => '2067 Hailston Dr',
    //             'city' => 'Duluth',
    //             'state' => 'GA',
    //             'zip' => 30097
    //           ),
    //           'email' => 'email@email.com',
    //           'phone' => 6784678435
    //         ),
    //         'officiant' => array(
    //           'prefix' => 'pastor',
    //           'description' => 'Primarily serving the Southeast.',
    //           'questions' => array(
    //             array(
    //               'question' => 'Do you service areas outside of your hometown?',
    //               'answer' => 'I do but not outside the USA'
    //             )
    //           ),
    //           'fee' => 50,
    //           'availability' => array(
    //             'mon'=>true,
    //             'tues'=>true,
    //             'wed'=>false,
    //             'thurs'=>true,
    //             'fri'=>false,
    //             'sat'=>true,
    //             'sun'=>true
    //           )
    //         )
    //       )
    //     )
    //   )
    // ]);
  }

  public function updateAccount(Request $request) {
    $user = Users::find(Auth::user()->uid);
    if ($request->has('push_notifications')){
      $user->updateOption('push_notifications', $request->input('push_notifications'));
    }
    if ($request->has('email_notifications')){
      $user->updateOption('email_notifications', $request->input('email_notifications'));
    }
    if ($request->has('phone')){
      $user->phone = $request->input('phone');
      $user->save();
    }

    return response()->json(array(
      'success' => true,
      'data' => array(
        'user' => $user->getProfile()
      )
    ));
  }
}
