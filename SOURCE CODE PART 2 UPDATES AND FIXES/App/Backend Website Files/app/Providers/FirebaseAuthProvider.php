<?php

namespace App\Providers;

use Vinkas\Firebase\Auth\User;
use App\Auth\CustomUserProvider;
use Illuminate\Support\ServiceProvider;


class FirebaseAuthProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->app['auth']->extend('custom',function()
      {
        return new CustomUserProvider(new User);
      });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
