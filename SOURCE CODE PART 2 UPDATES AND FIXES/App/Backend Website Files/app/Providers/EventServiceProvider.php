<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [

        /*
          User Events
         */
        'App\Events\User\Created' => [
            'App\Listeners\User\CreatedNotification',
        ],
        'App\Events\User\Updated' => [
            'App\Listeners\User\UpdatedNotification',
        ],
        'App\Events\User\Deleted' => [
            'App\Listeners\User\DeletedNotification',
        ],

        /*
          'Event' Events
         */
        'App\Events\Event\Created' => [
            'App\Listeners\Event\CreatedNotification',
        ],
        'App\Events\Event\Updated' => [
            'App\Listeners\Event\UpdatedNotification',
        ],
        'App\Events\Event\Deleted' => [
            'App\Listeners\Event\DeletedNotification',
        ],

        /*
          Guest Events
         */
        'App\Events\Guests\Created' => [
            'App\Listeners\Guests\CreatedNotification',
        ],
        'App\Events\Guests\Updated' => [
            'App\Listeners\Guests\UpdatedNotification',
        ],
        'App\Events\Guests\Deleted' => [
            'App\Listeners\Guests\DeletedNotification',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
