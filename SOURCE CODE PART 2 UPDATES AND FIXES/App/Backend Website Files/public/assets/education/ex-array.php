<?php

/***************************
  Sample using a PHP array
****************************/

require('fpdm.php');

$fields = array(
	'name1'    => 'Test2',
	'name2' => 'Test3'
);

$pdf = new FPDM('certificate_of_completion.pdf');
// $pdf->Load($fields, false); // second parameter: false if field values are in ISO-8859-1, true if UTF-8
$pdf->Merge();
$pdf->Output();
?>
