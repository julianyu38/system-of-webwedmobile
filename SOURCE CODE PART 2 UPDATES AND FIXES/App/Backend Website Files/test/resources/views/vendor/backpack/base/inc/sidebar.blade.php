@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    @role('Admin')
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- <div class="user-panel">
          <div class="pull-left image">
            <img src="{{ 'https://placehold.it/160x160/00a65a/ffffff/&text='.Auth::user()->name[0] }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="{{ url(config('backpack.base.route_prefix').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a>
          </div>
        </div> -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <!-- <li><a href="{{ url(config('backpack.base.route_prefix').'/admins/1/edit') }}"><i class="fa fa-user"></i> <span>My Account</span></a></li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <li class="header">MAIN</li>
          <!-- <li><a href="{{ url('admin/firebase_users') }}"><i class="fa fa-user"></i> <span>Manage Users</span></a></li> -->
          <!-- <li class="treeview active">
              <a href="#"><i class="fa fa-newspaper-o"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu menu-open">
                <li><a href="{{ url(config('backpack.base.route_prefix').'/admins') }}"><i class="fa fa-list"></i> <span>Admins</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/firebase_users') }}"><i class="fa fa-newspaper-o"></i> <span>Normal Users</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/officiants') }}"><i class="fa fa-list"></i> <span>Officiants</span></a></li>
              </ul>
          </li> -->
          <!-- Users, Roles Permissions -->
          @role('Admin')
          <li><a href="{{ url(config('backpack.base.route_prefix').'/user') }}"><i class="fa fa-group"></i> <span>Accounts</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix').'/officiants') }}"><i class="fa fa-university"></i> <span>Officiants</span></a></li>
          @endrole
          <!-- <li class="treeview active">
            <a href="#"><i class="fa fa-group"></i> <span>Accounts, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu menu-open">
              <li><a href="{{ url(config('backpack.base.route_prefix').'/user') }}"><i class="fa fa-user"></i> <span>Accounts</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix').'/role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
              <li><a href="{{ url(config('backpack.base.route_prefix').'/permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
            </ul>
          </li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix').'/events') }}"><i class="fa fa-calendar"></i> <span>Events</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix').'/courts') }}"><i class="fa fa-gavel"></i> <span>Courts</span></a></li>

          @role('Admin')
          <li class="header">CONFIGURATION</li>
          <!-- <li class="treeview"> -->
              <!-- <a href="#"><i class="fa fa-calendar"></i> <span>Packages, Addons, Settings</span> <i class="fa fa-angle-left pull-right"></i></a> -->
              <!-- <ul class="treeview-menu"> -->
                <li><a href="{{ url(config('backpack.base.route_prefix').'/packages') }}"><i class="fa fa-gift"></i> <span>Packages</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/eventroles') }}"><i class="fa fa-black-tie"></i> <span>Event Roles</span></a></li>
                <!-- <li><a href="{{ url(config('backpack.base.route_prefix').'/preimum_addons') }}"><i class="fa fa-puzzle-piece"></i> <span>Add-ons</span></a></li> -->
                <li><a href="{{ url(config('backpack.base.route_prefix').'/coupons') }}"><i class="fa fa-tag"></i> <span>Coupons</span></a></li>
                <li class="header">SETTINGS</li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/officiant_questions') }}"><i class="fa fa-certificate"></i> <span>Officiant Questions</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/emojis') }}"><i class="fa fa-comment"></i> <span>Emojis</span></a></li>
              <!-- </ul> -->
          <!-- </li> -->
          @endrole
          <!-- <li class="treeview active">
              <a href="#"><i class="fa fa-usd"></i> <span>Sales</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu menu-open">
                <li><a href="{{ url(config('backpack.base.route_prefix').'/event_sales') }}"><i class="fa fa-calendar"></i> <span>Events</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/preimum_addon_sales') }}"><i class="fa fa-list"></i> <span>Premium Addons</span></a></li>
              </ul>
          </li> -->
          <!-- <li class="treeview active">
              <a href="#"><i class="fa fa-newspaper-o"></i> <span>Settings</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu menu-open">
                <li><a href="{{ url(config('backpack.base.route_prefix').'/setting') }}"><i class="fa fa-newspaper-o"></i> <span>System</span></a></li>
              </ul>
          </li> -->

          <!-- <li><a href="{{ url(config('backpack.base.route_prefix').'/monster') }}"><i class="fa fa-optin-monster"></i> <span>Monsters</span></a></li> -->

          <!-- <li class="treeview">
              <a href="#"><i class="fa fa-newspaper-o"></i> <span>News</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url(config('backpack.base.route_prefix').'/article') }}"><i class="fa fa-newspaper-o"></i> <span>Articles</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/category') }}"><i class="fa fa-list"></i> <span>Categories</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/tag') }}"><i class="fa fa-tag"></i> <span>Tags</span></a></li>
              </ul>
          </li> -->

          <!-- <li><a href="{{ url(config('backpack.base.route_prefix').'/page') }}"><i class="fa fa-file-o"></i> <span>Pages</span></a></li>
          <li><a href="{{ url(config('backpack.base.route_prefix').'/menu-item') }}"><i class="fa fa-list"></i> <span>Menu</span></a></li> -->



          <!-- <li class="treeview">
              <a href="#"><i class="fa fa-cogs"></i> <span>Advanced</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url(config('backpack.base.route_prefix').'/elfinder') }}"><i class="fa fa-files-o"></i> <span>File manager</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/backup') }}"><i class="fa fa-hdd-o"></i> <span>Backups</span></a></li>
                <li><a href="{{ url(config('backpack.base.route_prefix').'/log') }}"><i class="fa fa-terminal"></i> <span>Logs</span></a></li>
              </ul>
          </li> -->



          <!-- ======================================= -->
          {{-- <li class="header">{{ trans('backpack::base.user') }}</li>
          <li><a href="{{ url(config('backpack.base.route_prefix').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li> --}}
        </ul>
        
      </section>
      <!-- /.sidebar -->
    </aside>
    @endrole
@endif
