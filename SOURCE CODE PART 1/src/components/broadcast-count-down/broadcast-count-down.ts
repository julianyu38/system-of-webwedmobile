import { Component, Input } from '@angular/core';

/**
 * Generated class for the BroadcastCountDownComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'broadcast-count-down',
  templateUrl: 'broadcast-count-down.html'
})
export class BroadcastCountDownComponent {

  @Input('progress') progress = 100;
  @Input('width') width;
  @Input('track-color') trackColor = '#00bbd3';
  @Input('placeholder-color') placeholderColor = '#d6d8db';
  @Input('state') state = 1;
  @Input('max') max:any = '100';
  @Input('backgroundUrl') backgroundUrl : any;

  imageWidth : any

  constructor() {
    // console.log('Hello BroadcastCountDownComponent Component');
    this.width = 250;
  }

  ngAfterContentInit(){
    //console.log('ngAfterContentInit '+this.progress);
    this.backgroundUrl="url('assets/images/heart_active.png')";

    // if(this.state == 1){
    //   this.backgroundUrl="../../assets/images/heart_active.png";
    // }else{
    //   this.backgroundUrl="../../assets/images/heart_disable.png";
    // }
    this.imageWidth = this.width;
  }

  progressValueChanged(){
    //console.log('progress value changed called! '+this.progress);
    // if(this.progress > 101)
    //   {
    //     // this.backgroundUrl="../../assets/images/heart_broadcast.png";
    //     // this.imageWidth = this.width + 2;
    //   }

    // if (this.progress < 0.25*this.max) {
    //   // red color
    //   this.backgroundUrl="url('../../assets/images/heart_broadcast.png')";
    // } else if (this.progress < 0.5*this.max) {
    //   // amber color
    //   this.backgroundUrl="url('../../assets/images/heart_active.png')";
    // } else {
    //   // normal color
    //   this.backgroundUrl="url('../../assets/images/heart_disable.png')";
    // }

    //this.backgroundUrl="url(../../assets/images/heart_active.png)";


  }

}
