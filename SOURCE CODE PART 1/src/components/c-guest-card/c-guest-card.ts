import { Component, Input } from '@angular/core';
import { LoadingController, NavController, ModalController } from  'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { StorageProvider, EventData } from '../../providers/storage/storage';
import { AttendEventModalPage } from '../../pages/attend-event-modal/attend-event-modal';
import { BroadcastPage } from '../../pages/broadcast/broadcast';
import { ArchivePage } from '../../pages/archive/archive';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the CGuestCardComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'c-guest-card',
  templateUrl: 'c-guest-card.html',
  providers: [HttpProvider, SocialSharing]
})
export class CGuestCardComponent {
  @Input('cWedCardEvent') wedCardEvent: any
  @Input('user') user: number

  cardImage: string;
  cardHeader: string;
  cardSubHeader: string;

  type: string = 'invite';
  status: any = {
    title: '',
    color: ''
  };
  event_status: number;
  hasResponded: number;

  constructor(
    public httpProvider: HttpProvider,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private socialSharing: SocialSharing
  ) {
    // console.log('Hello CGuestCardComponent Component');
  }

  watchEvent = function(){
    let event = this.wedCardEvent;
    let guest = true;
    for (var i = 0; i < event.participants.length; i++){
      let participant = event.participants[i];
      console.log(participant);
      if (this.user == participant.personal.id){
        guest = false;
      }
    }

    console.log(event.participants);
    console.log(this.user);

    let status = event.event_status;
    this.event_status = status;

    this.hasResponded = event.hasResponded;

    if (this.event_status == 2){
      this.play();
      return;
    }

    // this.navCtrl.setRoot(BroadcastPage, {
    //   guest: guest,
    //   event: event.id
    // });
    //
    // return;
    if (guest){
      this.httpProvider.isActive(event.id).then(
        (res) => {
          if (res) {
            this.navCtrl.setRoot(BroadcastPage, {
              guest: guest,
              event: event.id,
              user: this.user
            });
          }else{
            let modal = this.modalCtrl.create(AttendEventModalPage, {
              event: event.id
            });
            let me = this;
            modal.onDidDismiss(data => {
              console.log(data);
            });
            modal.present();
          }
        },
        (err) => {
          let modal = this.modalCtrl.create(AttendEventModalPage, {
            event: event.id
          });
          let me = this;
          modal.onDidDismiss(data => {
            console.log(data);
          });
          modal.present();
        }
      );
    }else{
      this.httpProvider.getEventSessionDetails(event.id, this.user).then(
        (res) => {
          console.log(res);
          //this.sessionId = res.opentok.session_id;
          // this.sessionId =
          //this.token = res.opentok.token;
          (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', ['46014622', res.opentok.session_id, res.opentok.token]);
        },
        (err) => {
          console.error(err);
        }
      );
    }
  }

  ngAfterContentInit(){
    this.cardImage = this.wedCardEvent.photo;
    this.cardHeader = this.wedCardEvent.title;
    this.cardSubHeader = this.wedCardEvent.date;

    if (this.wedCardEvent.type != undefined){
      this.type = this.wedCardEvent.type;
    }

    if (this.wedCardEvent.status != undefined){
      this.status = this.wedCardEvent.status;
    }

    let status = this.wedCardEvent.event_status;
    this.event_status = status;

    this.hasResponded = this.wedCardEvent.hasResponded;
  }

  respondToInvite(answer){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.respondToInvite(answer, this.wedCardEvent.id).then(
      (res) => {
        this.wedCardEvent = res;
        loading.dismiss();
      },
      (err) => {

      }
    );
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  play() {
    this.navCtrl.setRoot(ArchivePage, {
      event: this.wedCardEvent.id
    });
  }

  share() {
    let message = '';
    let url = '';
    if (this.wedCardEvent.id){
      message = 'Share event with a friend.';
      url = "http://webwedmobile.net/api/event/share/"+this.wedCardEvent.id;
    }else{
      message = 'Share user with a friend.';
      url = "http://webwedmobile.net/api/user/share/"+this.wedCardEvent.id
    }

    this.socialSharing.share(message, 'Share', null, url).then(() => {
      console.log('shared');
    }).catch(() => {
      // Error!
    });
  }
}
