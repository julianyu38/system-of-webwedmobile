import { NgModule } from '@angular/core';
import { IonicPageModule, IonicApp, IonicModule } from 'ionic-angular';
import { CWedCardComponent } from './c-wed-card/c-wed-card';
import { CGuestCardComponent } from './c-guest-card/c-guest-card';
import { MainNavigationComponent } from './main-navigation/main-navigation';
import { SubNavigationComponent } from './sub-navigation/sub-navigation';
import { MyApp } from '../app/app.component'
import { CircularProgressBarComponent } from './circular-progress-bar/circular-progress-bar';
import {RoundProgressModule} from 'angular-svg-round-progressbar';
import { ShareComponent } from './share/share';
import { AvatarComponent } from './avatar/avatar';
import { OptionsComponent } from './options/options';
import { BroadcastCountDownComponent } from './broadcast-count-down/broadcast-count-down';
import { IonWalkthrough } from './ion-walkthrough';

// import {VideoRecepientComponent} from "./video-recepient/video-recepient.component";
// import {VideoCallerComponent} from "./video-caller/video-caller.component";
// import {LoadingComponent} from "./shared/loading/loading.component";
// import {VideoCallWidgetComponent} from "./shared/video-call-widget/video-call-widget.component";

@NgModule({
	declarations: [
		MainNavigationComponent,
		SubNavigationComponent,
		CircularProgressBarComponent,
    ShareComponent,
    AvatarComponent,
    OptionsComponent,
    BroadcastCountDownComponent,
		IonWalkthrough,
		// VideoRecepientComponent,
		// VideoCallerComponent,
		// LoadingComponent,
		// VideoCallWidgetComponent
	],
	imports: [
		IonicModule.forRoot(MyApp),
		RoundProgressModule
	],
	exports: [
		MainNavigationComponent,
		SubNavigationComponent,
		CircularProgressBarComponent,
    ShareComponent,
    AvatarComponent,
    OptionsComponent,
    BroadcastCountDownComponent,
		IonWalkthrough,
		// VideoRecepientComponent,
		// VideoCallerComponent,
		// LoadingComponent,
		// VideoCallWidgetComponent
	],
})

export class ComponentsModule {}
