import { Component, Input } from '@angular/core';
import { StorageProvider } from '../../providers/storage/storage';
import { HttpProvider } from '../../providers/http/http';
import { SocialSharing } from '@ionic-native/social-sharing';
/**
 * Generated class for the ShareComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'share',
  templateUrl: 'share.html',
  providers: [HttpProvider, SocialSharing]
})
export class ShareComponent {
  @Input('user') userId?: any;
  @Input('event') eventId?: any;

  event: any;
  hasTitle: boolean = true;
  hasShare: boolean = true;

  constructor(
    public httpProvider: HttpProvider,
    private socialSharing: SocialSharing
  ) {

  }

  ngAfterViewInit(){
    // if (this.eventId){
    //   let eventId = this.eventId;
    //
    //   this.httpProvider.isPublic(eventId).then(
    //     (res) => {
    //       this.hasShare = res;
    //     },
    //     (err) => {
    //       console.error(err);
    //     }
    //   );
    // }else{
    //   let userId = this.userId;
    // }
  }

  share(){
    let message = '';
    let url = '';
    if (this.eventId){
      message = 'Share event with a friend.';
      url = this.httpProvider.apiUrl+"/event/share/"+this.eventId;
    }else{
      message = 'Share user with a friend.';
      url = this.httpProvider.apiUrl+"/user/share/"+this.userId
    }

    this.socialSharing.share(message, 'Share', null, url).then(() => {
      console.log('shared');
    }).catch(() => {
      // Error!
    });
  }

}
