import { Component, Input } from '@angular/core';
import { HttpProvider } from '../../providers/http/http';
/**
 * Generated class for the AvatarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'avatar',
  templateUrl: 'avatar.html',
  providers: [HttpProvider]
})
export class AvatarComponent {

  @Input('user') userId;
  @Input('name') name?;
  @Input('status') status?;
  @Input('event') eventId?;
  @Input('showName') showName?: boolean = false;

  constructor(
    public httpProvider: HttpProvider
  ) {

  }

  avatar: string = '';

  generateAvatar() {
    if (Number.isInteger(this.userId) && typeof(this.name) == 'undefined'){
      //this.name.toString() == ''
        return this.httpProvider.apiUrl + '/user/'+this.userId.toString()+'/avatar';
    }else{
      this.name = this.name.replace(' ', '_');
      return this.httpProvider.apiUrl + '/user/'+this.name.toString()+'/avatar';
    }

    // if (this.userId){
    //   if (this.userId.toString() == '0'){
    //     var url = this.httpProvider.apiUrl+'/user/1/avatar/';
    //   }else{
    //     var url = this.httpProvider.apiUrl+'/user/'+this.userId.toString()+'/avatar/';
    //   }
    //   return url;
    // }
  }

  ngAfterContentInit(){
    this.avatar = this.generateAvatar();
  }

}
