import { Component, Input, Output, EventEmitter, ContentChild } from '@angular/core';
import { MyAccountPage } from '../../pages/my-account/my-account';
import { NotificationsPage } from '../../pages/notifications/notifications';
import { NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { DashboardPage } from '../../pages/dashboard/dashboard';

/**
 * Generated class for the MainNavigationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'main-navigation',
  templateUrl: 'main-navigation.html'
})
export class MainNavigationComponent {

  @Input('title') title?: string = '';
  @Input('tabs') tabs?: any;
  @Output('selected-tab') selected_tab? : EventEmitter<number> = new EventEmitter<number>();


  navTitle: string;
  selected_tab_identifier: string = '';

  constructor(
    public navCtrl: NavController,
    private statusBar: StatusBar
  ) {
    // console.log('MainNavigation constructed');
  }

  ngAfterViewInit(){
    this.navTitle = this.title;
    // let status bar overlay webview
    this.statusBar.overlaysWebView(false);

    //White text
    this.statusBar.styleLightContent();

    // set status bar to white
    this.statusBar.backgroundColorByHexString('#1658a0');

    if (this.tabs){
      this.selectTab(0);
    }
  }

  gotoDashboard(){
    this.navCtrl.setRoot(DashboardPage);
  }

  gotoNotifications(){
    this.navCtrl.setRoot(NotificationsPage);
  }

  gotoProfile(){
    this.navCtrl.setRoot(MyAccountPage);
  }

  selectTab(index: number){
    this.selected_tab_identifier = this.tabs[index];
    this.selected_tab.emit(index);
  }

}
