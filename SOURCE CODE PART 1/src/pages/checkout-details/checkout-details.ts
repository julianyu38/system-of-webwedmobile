import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Renderer } from '@angular/core';
import { PayPage } from '../pay/pay';
import { DashboardPage } from '../dashboard/dashboard';
/**
 * Generated class for the CheckoutDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout-details',
  templateUrl: 'checkout-details.html',
  providers: [HttpProvider]
})
export class CheckoutDetailsPage {

  eventDetails : any;
  package : any;
  addons : any;
  subtotal: number = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    private loadingCtrl: LoadingController,
    public renderer: Renderer,
    public modalCtrl: ModalController) {
    this.eventDetails = navParams.get('eventData');
    this.package = navParams.get('package');
    console.log(this.eventDetails);
    console.log(this.package);
    this.subtotal += this.package.price;
    let _addons = [];
    for (var i = 0; i < this.eventDetails.addons.length; i++){
      let addon = this.eventDetails.addons[i];
      //if (addon.selected){
        _addons = _addons.concat(addon);
        this.subtotal += addon.price;
      //}
    }
    this.addons = _addons;
    console.log(this.addons);
  }

  public checkout() {
    // this.openCheckout().then(
    //   (token) => {
        // this.eventDetails.token = 'test';
        // let loading = this.createLoading();
        // loading.present();
        //
        // this.httpProvider.createEvent(this.eventDetails, this.navParams.get('file')).then(
        //   (res) => {
        //     console.log(res);
        //     this.navCtrl.popToRoot();
        //     console.log(res);
        //     loading.dismiss();
        //   },
        //   (err) => {
        //     console.error(err);
        //     loading.dismiss();
        //   }
        // );

        let payModal = this.modalCtrl.create(PayPage, {
          amount: (this.subtotal * 100)
        });
        payModal.onDidDismiss(data => {
          if (data == null){
            return;
          }
          let token_id = data.id;
          this.eventDetails.token = token_id;
          let loading = this.createLoading();
          loading.present();

          this.httpProvider.createEvent(this.eventDetails).then(
            (res) => {

              console.log(res);
              //this.navCtrl.popToRoot();
              this.navCtrl.setRoot(DashboardPage);
              console.log(res);
              loading.dismiss();
            },
            (err) => {
              console.error(err);
              loading.dismiss();
            }
          );
        });
        payModal.present();
    //    },
    //    (err) => {
    //
    //   }
    // );
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  globalListener : any;

  openCheckout() {
    return new Promise((resolve, reject) => {
      let handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_OtZkFKLIU1WBuonz6xbk6UQB',
        locale: 'en',
        token: function (token: any) {
          resolve(token);
        }
      });

      handler.open({
        name: 'Web Wed Mobile',
        description: 'Event Package Purchase',
        amount: this.subtotal * 100,
        currency: 'usd',
        email: this.navParams.get('user').personal.email,
        image: 'assets/icon/favicon.ico',
        ['allow-remember-me']: true
      });

      this.globalListener = this.renderer.listenGlobal('window', 'popstate', () => {
        handler.close();
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutDetailsPage');
  }

}
