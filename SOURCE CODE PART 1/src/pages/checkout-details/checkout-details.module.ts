import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckoutDetailsPage } from './checkout-details';

@NgModule({
  declarations: [
    CheckoutDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckoutDetailsPage),
  ],
})
export class CheckoutDetailsPageModule {}
