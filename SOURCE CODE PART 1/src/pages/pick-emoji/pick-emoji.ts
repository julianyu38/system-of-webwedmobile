import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
/**
 * Generated class for the PickEmojiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pick-emoji',
  templateUrl: 'pick-emoji.html',
  providers: [HttpProvider]
})
export class PickEmojiPage {

  emojis = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public viewCtrl: ViewController
  ) {
    httpProvider.getEmojis().then(
      (emojis) => {
        this.emojis = emojis;
      },
      (err) => {
        console.error(err);
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PickEmojiPage');
  }

  selected(fileName) {
    this.viewCtrl.dismiss({
      image: fileName
    });
  }

  dimiss(){
    this.viewCtrl.dismiss();
  }



}
