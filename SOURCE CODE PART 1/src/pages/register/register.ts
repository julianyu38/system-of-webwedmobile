import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from './../../providers/auth/auth';
import { UserData } from '../../providers/storage/storage';
import { HomePage } from './../home/home';
import { DashboardPage } from './../dashboard/dashboard';
import { Component, ViewChild, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, Slides, LoadingController, AlertController, ModalController } from 'ionic-angular';
import * as firebase from 'firebase/app';
import {AutocompletePage} from '../autocomplete/autocomplete';
import { StorageProvider, NotificationData } from '../../providers/storage/storage';
import { LoginPage } from './../login/login';

@IonicPage({
  name: 'register'
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers: [StorageProvider]
})
export class RegisterPage implements OnInit {
  fullHeight: boolean = true;
  @ViewChild(Slides) slides: Slides;
  length: number = 6;
  form: FormGroup;
  user: firebase.User
  address = "";
  rawAddress = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private auth: AuthProvider,
    private afAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    public storageProvider: StorageProvider) {
  }

  ngOnInit() {
    this.user = this.navParams.get('user');
    this.slides.lockSwipes(true);
    let email = this.getEmail();
    this.form = new FormGroup({
      email: new FormControl({value: email, disabled: !!email},
        [Validators.email, Validators.required]),
      password: new FormControl('',
        [Validators.required, Validators.minLength(6)]),
      full_name: new FormControl(this.getName(),
        [Validators.required, Validators.maxLength(255)]),
      address: new FormControl('',
        [Validators.required, Validators.maxLength(255)]),
      birthday: new FormControl('',
        [Validators.required,]),
      phone: new FormControl('',
        [Validators.required, Validators.pattern(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)]),
    });
  }

  getEmail() {
    return this.user ? this.user.email : '';
  }

  getName() {
    return this.user ? this.user.displayName : '';
  }

  getFullName(){
    if (this.form.controls.full_name.valid){
      if (this.form.controls.full_name.value.indexOf(' ') > -1){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }

  canGoNext() {
    switch (this.slides.getActiveIndex()) {
      case 0:
        return (this.getEmail() ? true : this.form.controls.email.valid) && this.form.controls.password.valid;
      case 1:
        return this.getFullName();
      case 2:
        return this.form.controls.address.valid;
      case 3:
        return this.form.controls.birthday.valid;
      case 4:
        return this.form.controls.phone.valid;
    }
    return true;
  }

  continue() {
    if (!this.slides.isEnd()) {
      if (this.slides.getActiveIndex() === 5) {
        let loading = this.loadingCtrl.create({
          content: 'Please wait ...'
        });
        loading.present();
        this.submit().then(() => {
          loading.dismiss()
          this.slideNext();
        }, (e) => {
          console.log(e);
          loading.dismiss();
        });
      } else if (this.slides.getActiveIndex() === 0) {
        if (this.user) {
          this.slideNext();
          return;
        }
        let loading = this.loadingCtrl.create({
          content: 'Please wait ...'
        });
        loading.present();
        this.afAuth.auth.fetchProvidersForEmail(this.form.value.email)
          .then((providers) => {
            loading.dismiss();
            console.log(providers);
            if (providers.length > 0) {
              this.showAlert('User with this email already registered');
            } else {
              this.slideNext();
            }
          }, () => {
            loading.dismiss();
            this.showAlert('Can\'t validate email');
          })
      } else {
        this.slideNext();
      }
    }
  }

  back() {
    if (!this.slides.isBeginning()) {
      this.slidePrev();
    }
  }

  private showAlert(msg) {
    let alert = this.alertCtrl.create({
      message: msg,
      buttons: [{
        text: 'Ok',
        role: 'close'
      }]
    });
    alert.present();
  }

  private slideNext() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  private slidePrev() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  backToLogin() {
    this.navCtrl.canGoBack() ? this.navCtrl.pop() : this.navCtrl.setRoot('login');
  }

  close() {
    this.afAuth.auth.signOut().then(() => {
       console.log('Signed Out');
       this.storageProvider.clearAll();
       this.navCtrl.setRoot(LoginPage);
    });
  }

  getUserData() {
    return {
      dob: this.form.value.birthday,
      email: this.form.value.email,
      name: this.form.value.full_name,
      location: this.rawAddress,
      phone: this.form.value.phone,
    }
  }

  submit() {
    return new Promise((resolve, reject) => {
      if (this.user) {
        const emailCredentials = firebase.auth.EmailAuthProvider.credential(
          this.user.email, this.form.value.password);

        this.user.linkWithCredential(emailCredentials).then((user) => {
          user.getIdToken(true).then((accessToken) => {
            this.auth.setUserData(accessToken, this.getUserData()).then(() => {
              resolve();
            }, reject);
          });
        }, reject);
        return;
      }
      this.auth.signUpWithEmail(this.form.value.email, this.form.value.password)
        .then((user) => {
          user.getIdToken(true).then((accessToken) => {
            this.auth.setUserData(accessToken, this.getUserData()).then(() => {
              resolve();
            }, reject);
          });
        }, reject);
    })
  }

  showAddressModal () {
    let modal = this.modalCtrl.create(AutocompletePage);
    let me = this;
    modal.onDidDismiss(data => {
      this.address = data.description;
      console.log(data);
      this.rawAddress = data;
    });
    modal.present();
  }
}



// WEBPACK FOOTER //
// ./src/pages/register/register.ts
