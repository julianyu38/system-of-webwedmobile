import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddManualModalPage } from './add-manual-modal';

@NgModule({
  declarations: [
    AddManualModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddManualModalPage),
  ],
})
export class AddManualModalPageModule {}
