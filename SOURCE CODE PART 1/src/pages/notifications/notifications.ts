import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { StorageProvider, NotificationData } from '../../providers/storage/storage';

@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
  providers: [HttpProvider, StorageProvider]
})
export class NotificationsPage {

  notifications: [NotificationData];

  constructor(
    public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    private storageProvider: StorageProvider,
    private httpProvider: HttpProvider,
  ) {
    let loading = this.createLoading();
    loading.present();

    storageProvider.getProfile().then(profile => {
      let user_id = profile.personal.id;
      this.httpProvider.getNotifications(user_id).then(
        (res) => {
          this.notifications = res;
          loading.dismiss();
        },
        (err) => {
          console.error(err);
          loading.dismiss();
        }
      )
    }, err => {
      console.error(err);
      loading.dismiss();
    });
  }

  doRefresh(refresher) {

    this.storageProvider.getProfile().then(profile => {
      let user_id = profile.personal.id;
      this.httpProvider.getNotifications(user_id).then(
        (res) => {
          this.notifications = res;
          refresher.complete();
        },
        (err) => {
          console.error(err);
          refresher.complete();
        }
      )
    }, err => {
      console.error(err);
      refresher.complete();
    });
  }

  dismiss(notification){
    let loading = this.createLoading();
    loading.present();
    this.storageProvider.getProfile().then(profile => {
      let user_id = profile.personal.id;
      this.httpProvider.clearNotification(user_id, notification.id).then(
        (res) => {
          this.notifications = res;
          loading.dismiss();
        },
        (err) => {
          console.error(err);
        }
      );
    });
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

}
