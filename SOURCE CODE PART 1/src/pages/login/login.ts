import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from './../../providers/auth/auth';
import { UserData } from '../../providers/storage/storage';
import { Facebook } from '@ionic-native/facebook';
import { RegisterPage } from './../register/register';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as firebase from 'firebase/app';
import { DashboardPage } from '../dashboard/dashboard';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [StorageProvider]
})
export class LoginPage implements OnInit {

  form: FormGroup;
  eventId: any;

  attempts: any = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private keyboard: Keyboard,
    private auth: AuthProvider,
    private fbAuth: AngularFireAuth,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private fb: Facebook,
    public storageProvider: StorageProvider) {
      if (this.navParams.get('eventId')){
        this.eventId = this.navParams.get('eventId');
      }
  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('',
        [Validators.required, Validators.email]),
      password: new FormControl('',
        [Validators.required, Validators.minLength(6)]),
    });

    let loading = this.createLoading();
    loading.present();
    const authSubscrition = this.fbAuth.authState.subscribe((user) => {
      authSubscrition.unsubscribe();
      if (user) {
        this.continue(user, loading);
      } else {
        loading.dismiss();
      }
    }, (e) => {
      loading.dismiss();
    });

    this.storageProvider.setSeenWalkthrough(true).then(
      (res) => {
        console.log('Seen WalkthroughPage');
      },
      (err) => {

      }
    );
  }

  signInWithEmail() {
    let loading = this.createLoading();
    loading.present();
    this.auth.signInWithEmail(this.form.controls.email.value, this.form.controls.password.value)
      .then((user) => {
        this.continue(user, loading);
      }, (e) => {
        loading.dismiss();
        console.log(e);

        this.attempts += 1;
        if (this.attempts >= 3 && (<any>e).code == 'auth/wrong-password'){
          this.auth.forgotPassword(this.form.controls.email.value).then(
            (result) => {
              console.log(result);
              let alert = this.alertCtrl.create({
                message: 'A password reset has been sent to your email address.',
                buttons: [{
                  role: 'close',
                  text: 'Ok'
                }]
              });
              alert.present();
            },
            (error) => {
              this.handleError(error);
            }
          );
          this.attempts = 0;
          return;
        }

        if ((<any>e).code == 'auth/user-not-found'){
          this.navCtrl.push(RegisterPage, {
            user: {
              email: this.form.controls.email.value,
              password: this.form.controls.password.value,
              displayName: ''
            }
          });
          this.attempts = 0;
          return;
        }

        this.handleError(e);
      });
  }

  signInWithFacebook() {
    let loading = this.createLoading();
    loading.present();
    this.auth.signInWithFacebook().then((user) => {
      this.continue(user, loading);
    }, (e) => {
      loading.dismiss();
      this.handleError(e);
    });
  }

  signInWithGoogle() {
    let loading = this.createLoading();
    loading.present();
    this.auth.signInWithGoogle().then((user) => {
      console.log("THERE");
      this.continue(user, loading);
    }, (e) => {
      loading.dismiss();
      this.handleError(e);
    })
  }

  register() {
    this.navCtrl.push(RegisterPage);
  }

  private handleError(error: any) {
    let alert = this.alertCtrl.create({
      message: error && error.message ? error.message : 'Error',
      buttons: [{
        role: 'close',
        text: 'Ok'
      }]
    });
    alert.present();
  }

  private continue(user: firebase.User, loading?: any) {
    user.getIdToken(true).then((accessToken) => {
      this.auth.getUserData(accessToken).then((userData) => {
        console.log(userData);
        if (loading) loading.dismiss();
        // TODO: this should be handled better
        /*if (!userData.personal.location) {
          this.navCtrl.push(RegisterPage, {
            user: user
          });
        } else {*/
          // this.navCtrl.setRoot('home', {
          //   user: user
          // });
          this.storageProvider.setProfile(userData).then((res) => {
            this.navCtrl.setRoot(DashboardPage, {
              user: user,
              userData: userData,
              event: this.eventId
            });
          });
        //}
      }, (e) => {
        if (loading) loading.dismiss();
        console.log("ERROR", e);
      });
    }, (e) => {
      if (loading) loading.dismiss();
      console.log("ERROR", e);
    });
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }
}



// WEBPACK FOOTER //
// ./src/pages/login/login.ts
