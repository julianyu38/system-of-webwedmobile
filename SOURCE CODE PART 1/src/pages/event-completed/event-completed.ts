import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
/**
 * Generated class for the EventCompletedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-completed',
  templateUrl: 'event-completed.html',
})
export class EventCompletedPage {
  total: string = '0';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.total = this.navParams.get('total').toString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventCompletedPage');
  }

  close() {
    this.navCtrl.setRoot(DashboardPage);
  }

}
