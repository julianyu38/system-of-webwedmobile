import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, LoadingController, ModalController } from 'ionic-angular';
import { WedDetailsPage } from '../wed-details/wed-details';
import { CreateAnEventPage } from '../create-an-event/create-an-event';
// import { MyAccountPage } from '../my-account/my-account';
// import { NotificationsPage } from '../notifications/notifications';
import { HttpProvider } from '../../providers/http/http';
import { StorageProvider, UserData, EventData, EventDetailData } from '../../providers/storage/storage';
import { AttendEventModalPage } from '../attend-event-modal/attend-event-modal';
import { BroadcastPage } from '../broadcast/broadcast';
import { TourProvider } from '../../providers/tour/tour';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
  providers: [HttpProvider, StorageProvider, TourProvider]
})
export class DashboardPage {

  user?: UserData;
  events?: [EventData];
  past_events?: [EventData];

  // participants: any = ["assets/images/user-avatar1.png",
  //   "assets/images/user-avatar2.png",
  //   "assets/images/user-avatar3.png",
  //   "assets/images/user-avatar4.png",
  //   "assets/images/user-avatar5.png"];
  // //public aEventData : any;
  // public eventsArray: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private httpProvider: HttpProvider,
    public storageProvider: StorageProvider,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    public tourProvider: TourProvider) {
    // this.getUserEvents();

    if (this.navParams.get('userData')){
      console.log(this.navParams.get('userData'));
      storageProvider.setProfile(this.navParams.get('userData')).then((res) => {  });
    }

    if (this.navParams.get('event')){
      if (this.navParams.get('event') != null){
        this.findEvent_id = this.navParams.get('event');
        this.findEvent(this.navParams.get('event'));
      }
    }

    var self = this;
    // setTimeout(function(){
    //   self.tourProvider.startTour();
    // }, 3000);

    // if (this.navParams.get('userData')){
    //   storage.set('userData', JSON.stringify(this.navParams.get('userData')));
    // }

    // storage.get('userData').then((val) => {
    //   this.user = JSON.parse(val);
    //   this.events = this.user.events;
    //   console.log(this.events);
    //   console.log(val);
    // });

    storageProvider.getProfile().then(profile => {
      this.user = profile;
      if (profile.events == null){
        this.events = null;
      }else{
        this.events = profile.events;
      }
      this.httpProvider.getUserEvents(this.user.personal.id).then(
        (res) => {
          this.events = res.future;
          this.past_events = res.past;
        },
        (err) => {
            console.error(err);
        }
      );

      // setInterval(function(){
      //   httpProvider.getUserEvents(profile.personal.id).then(
      //     (res) => {
      //       this.events = res.future;
      //       this.past_events = res.past;
      //     },
      //     (err) => {
      //         console.error(err);
      //     }
      //   );
      // }, 2000);
    }, err => {
      console.error(err);
    });
  }

  ionViewWillEnter(){
    // console.log('Entering View');
  }

  gotoWedDetails = function(event) {

    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getEventDetailsById(event.id).then(
      (res) => {
        this.navCtrl.push(WedDetailsPage, {
          event: res,
          user: this.user
        });
        loading.dismiss();
      },
      (err) => {
        console.error(err);
      }
    );
  }

  hasPermissions = function(event){

  }

  watchEvent = function(event){
    if (event.status){
      if (event.status == 2){
        let modal = this.modalCtrl.create(AttendEventModalPage, {
          event: event.id,
          user: this.user.personal.id
        });
        let me = this;
        modal.onDidDismiss(data => {
          console.log(data);
        });
        modal.present();
        return;
      }
    }

    let participants = event.participants;
    var isParticipant = false;
    for (var i = 0; i < participants.length; i++){
      if (participants[i].id == this.user.personal.id){
        isParticipant = true;
      }
    }
    console.log(event.participants);
    if (isParticipant){
      this.httpProvider.getEventSessionDetails(event.details.id, this.user.personal.id).then(
        (res) => {
          console.log(res);
          //this.sessionId = res.opentok.session_id;
          // this.sessionId =
          //this.token = res.opentok.token;
          (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', ['46014622', res.opentok.session_id, res.opentok.token, false]);
        },
        (err) => {
          console.error(err);
        }
      );
    }else{
      // console.log(event);return;

      this.httpProvider.isActive(event.id).then(
        (res) => {
          if (res) {
            console.log(res);
            this.navCtrl.setRoot(BroadcastPage, {
              guest: true,
              event: event.id,
              user: this.user.personal.id
            });
            // (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', []);
          }else{
            let modal = this.modalCtrl.create(AttendEventModalPage, {
              event: event.id,
              user: this.user.personal.id
            });
            let me = this;
            modal.onDidDismiss(data => {
              console.log(data);
            });
            modal.present();
          }
        },
        (err) => {
          let modal = this.modalCtrl.create(AttendEventModalPage, {
            event: event.id,
            user: this.user.personal.id
          });
          let me = this;
          modal.onDidDismiss(data => {
            console.log(data);
          });
          modal.present();
        }
      );
    }
  }

  details() {
    this.navCtrl.push(WedDetailsPage);
  }

  createEvent() {
    this.navCtrl.push(CreateAnEventPage);
  }

  isEmpty(){
    if (typeof(this.events) == 'undefined'){
      return true;
    }else{
      if (this.events.length==0){
        return true;
      }else{
        return false;
      }
    }
  }

  pastIsEmpty(){
    if (typeof(this.past_events) == 'undefined'){
      return true;
    }else{
      if (this.past_events.length==0){
        return true;
      }else{
        return false;
      }
    }
  }

  doRefresh(refresher) {
    console.log(this.user);
    this.httpProvider.getUserEvents(this.user.personal.id).then(
      (res) => {
        this.events = res.future;
        this.past_events = res.past;
        refresher.complete();
      },
      (err) => {
          console.error(err);
      }
    );
  }

  findEvent_id: number;

  findEvent(event){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.isPublic(this.findEvent_id).then(
      (res) => {
        if (res){
          console.log(this.findEvent_id);
          this.httpProvider.getEventDetailsById(this.findEvent_id).then(
            (res) => {
              if (res){
                this.watchEvent(res.details);
                loading.dismiss();
                this.findEvent_id = null;
              }
            },
            (err) => {
              console.error(err);
            }
          );
        }
      },
      (err) => {
        loading.dismiss();
        this.findEvent_id = null;
      }
    );
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

}
