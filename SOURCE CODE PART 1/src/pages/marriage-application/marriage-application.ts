import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the MarriageApplicationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-marriage-application',
  templateUrl: 'marriage-application.html',
  providers: [HttpProvider, Camera]
})
export class MarriageApplicationPage {

  application: any;
  submitted: any = false;
  attachments: any = [];
  your_information: any = {
    id: 0
  };
  spouce_information: any = {
    id: 0
  };
  officiant_information: any = {};

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: 0
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public httpProvider: HttpProvider, public loadingCtrl: LoadingController, private camera: Camera) {
    this.refreshApplication();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MarriageApplicationPage');
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  private refreshApplication(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getMarriageApplication(this.navParams.get('id')).then(
      (application) => {
        loading.dismiss();
        console.log(application);
        this.application = application;
        this.submitted = application.submitted;
        this.your_information = application.your_information;
        this.spouce_information = application.spouce_information;
        this.officiant_information = application.officiant_information;
        // this.attachments = application.attachments;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  private addAttachment() {
    let image = '';
    this.camera.getPicture(this.options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     image=base64Image;
     let loading = this.createLoading();
     loading.present();
     this.httpProvider.addAttachment(this.navParams.get('id'), image).then(
       (application) => {
         loading.dismiss();
         console.log(application);
         this.application = application;
         this.submitted = application.submitted;
         this.attachments = application.attachments;
       },
       (err) => {
         loading.dismiss();
       }
     )
    }, (err) => {
     // Handle error
     console.error(err);
    });
  }

  private removeAttachment(id){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.removeAttachment(this.navParams.get('id'), id).then(
      (application) => {
        loading.dismiss();
        console.log(application);
        this.application = application;
        this.submitted = application.submitted;
        this.attachments = application.attachments;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  private submit(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.submitMarriageApplication(this.navParams.get('id')).then(
      (application) => {
        loading.dismiss();
        console.log(application);
        this.application = application;
        this.submitted = application.submitted;
        this.attachments = application.attachments;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

}
