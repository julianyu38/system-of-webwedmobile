import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttendEventModalPage } from './attend-event-modal';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AttendEventModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AttendEventModalPage), ComponentsModule
  ],
})
export class AttendEventModalPageModule {}
