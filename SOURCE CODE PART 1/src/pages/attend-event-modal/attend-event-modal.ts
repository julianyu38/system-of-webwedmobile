import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Renderer } from '@angular/core';
import { StorageProvider, UserData, EventData, EventDetailData } from '../../providers/storage/storage';
import { HttpProvider } from '../../providers/http/http';
import { BroadcastPage } from '../../pages/broadcast/broadcast';
/**
 * Generated class for the AttendEventModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-attend-event-modal',
  templateUrl: 'attend-event-modal.html',
  providers: [HttpProvider, StorageProvider]
})
export class AttendEventModalPage {

  event: EventDetailData;
  eventId: number;

  loadProgress: number = 0;
  State = {
    ACTIVE: 1,
    DISABLE: 2
  }
  currentState : any = 2;
  max = 100;

  currentCountDownState : any = 2;
  maxCountDown = 100;
  countdownRemaining: number = 100;
  countdownColor = '#d6d8db';
  countdownBackgroundURL = '../../assets/images/heart_active.png';
  public showCountDown: boolean = true;
  public countDownPaused: boolean = false;
  public showProgress: boolean = true;
  public remainingMinutes: number = 0;
  public totalMinutes: number = 0;
  public permissions: any;
  public admin: number = 0;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public renderer: Renderer,
    public httpProvider: HttpProvider,
    public storageProvider: StorageProvider,
    public alertCtrl: AlertController) {

    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'small-modal-popup', true);
    this.eventId = this.navParams.get('event');

    // let loading = this.createLoading();
    // loading.present();


  }

  ngAfterContentInit(){
    this.httpProvider.getEventDetailsById(this.eventId).then(
      (res) => {
        this.event = res;
        // loading.dismiss();
      },
      (err) => {
        console.error(err);
      }
    );
  }

  hasPermissions() {
    return false;
  }

  start(){
    // if (!this.hasPermissions()){
    //   let alert = this.alertCtrl.create({
    //     title: 'Request permission',
    //     message: 'Do you want to request permission to control this event?',
    //     buttons: [
    //       {
    //         text: 'No',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Cancel clicked');
    //         }
    //       },
    //       {
    //         text: 'Yes',
    //         handler: () => {
    //
    //         }
    //       }
    //     ]
    //   });
    //   alert.present();
    // }else{
      // this.navCtrl.push(BroadcastPage,{
      //   guest: false,
      //   event: this.eventId,
      //   user: this.navParams.get('user')
      // });
    // }
    let participants = this.event.participants;
    var isParticipant = false;
    for (var i = 0; i < participants.length; i++){
      if (participants[i].id == this.navParams.get('user')){
        isParticipant = true;
      }
    }
    if (isParticipant){
      this.httpProvider.getEventSessionDetails(this.eventId, this.navParams.get('user')).then(
        (res) => {
          console.log(res);
          //this.sessionId = res.opentok.session_id;
          // this.sessionId =
          //this.token = res.opentok.token;
          (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', ['46014622', res.opentok.session_id, res.opentok.token]);
        },
        (err) => {
          console.error(err);
        }
      );
    }else{
      this.navCtrl.push(BroadcastPage,{
        guest: true,
        event: this.eventId,
        user: this.navParams.get('user')
      });
    }
  }

  // private createLoading() {
  //   let loading = this.loadingCtrl.create({
  //     content: 'Please wait ...'
  //   });
  //   return loading;
  // }

}
