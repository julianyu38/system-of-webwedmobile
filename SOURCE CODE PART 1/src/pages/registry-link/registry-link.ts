import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { MarriageLicensePage } from '../marriage-license/marriage-license';

@Component({
  selector: 'page-registry-link',
  templateUrl: 'registry-link.html'
})
export class RegistryLinkPage {

  constructor(public navCtrl: NavController) {
  }

  gotoMarriageLicense(){
    this.navCtrl.push(MarriageLicensePage);
  }

  save(){
    this.navCtrl.pop();
  }

}
