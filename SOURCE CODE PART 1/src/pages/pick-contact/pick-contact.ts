import { Component, Input } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { StorageProvider, ContactData } from '../../providers/storage/storage';
/**
 * Generated class for the PickContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pick-contact',
  templateUrl: 'pick-contact.html',
  providers: [Contacts,StorageProvider]
})
export class PickContactPage {

  contactsList: Contact[];
  letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  headers = {
    'a': [],
    'b': [],
    'c': [],
    'd': [],
    'e': [],
    'f': [],
    'g': [],
    'h': [],
    'i': [],
    'j': [],
    'k': [],
    'l': [],
    'm': [],
    'n': [],
    'o': [],
    'p': [],
    'q': [],
    'r': [],
    's': [],
    't': [],
    'u': [],
    'v': [],
    'w': [],
    'x': [],
    'y': [],
    'z': []
  };
  selectedContacts = {};
  allContacts = {};
  limit = -1;

  constructor(
    public viewCtrl: ViewController,
    private contacts: Contacts,
    public navCtrl: NavController,
    public navParams: NavParams) {

      if (this.navParams.get('limit')){
        if (this.navParams.get('limit') == 1){
          this.limit = 1;
        }else{
          this.limit = -1;
        }
      }

    this.contacts.find([ 'displayName', 'name' ]).then((contacts) => {
          this.contactsList = contacts;
          console.log(this.contactsList[0]);

          for (var i = 0; i < this.contactsList.length; i++){
            let fullName = this.contactsList[i].name.formatted;
            let firstLetter = fullName.toLowerCase().charAt(0);

            //Check if phone or email is present
            if (this.contactsList[i].emails || this.contactsList[i].phoneNumbers){
              this.allContacts['contact'+this.contactsList[i].id] = false;
              this.headers[firstLetter] = this.headers[firstLetter].concat(this.contactsList[i]);
            }
          }

          console.log(this.allContacts);

          console.log(this.headers);

        }, (error) => {
          console.log(error);
        })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PickContactPage');
  }

  hasContacts(letter) {
    if (this.headers[letter].length > 0){
      return true;
    }else{
      return false;
    }
  }

  getContactData = function(contact){
    return {
      name: contact.name.formatted,
      email: (contact.emails != null ? (contact.emails.length > 0 ? contact.emails[0].value : null) : null),
      phone: (contact.phoneNumbers != null ? (contact.phoneNumbers.length > 0 ? contact.phoneNumbers[0].value : null) : null)
    }
  }

  dismiss() {
    let contacts = [];
    if (this.contactsList){
      for (var i = 0; i < this.contactsList.length; i++){
        let contact = this.contactsList[i];
        if (this.selectedContacts['contact'+contact.id]){
          contacts = contacts.concat(this.getContactData(contact));
        }
      }
    }
     this.viewCtrl.dismiss({ 'contacts': contacts });
  }

  totalSelected: number = 0;

  selectContact(id){
    if (!this.selectedContacts['contact'+id]){
      this.totalSelected += 1;
    }else{
      this.totalSelected -= 1;
    }

    this.selectedContacts['contact'+id] = !this.selectedContacts['contact'+id];
    if (this.totalSelected == this.limit){
      this.dismiss();
    }
  }

}
