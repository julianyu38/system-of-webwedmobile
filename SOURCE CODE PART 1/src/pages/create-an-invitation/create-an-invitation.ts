import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, LoadingController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { MediaCapture, MediaFile, CaptureError, CaptureVideoOptions } from '@ionic-native/media-capture';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { VideoEditor,CreateThumbnailOptions } from '@ionic-native/video-editor';

@Component({
  selector: 'page-create-an-invitation',
  templateUrl: 'create-an-invitation.html',
  providers: [HttpProvider, FileTransfer, File, MediaCapture, VideoEditor]
})
export class CreateAnInvitationPage {

  event: any;
  subject: string = '';
  body: string = '';
  video: string = '';
  thumbnail: string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    private loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    private mediaCapture: MediaCapture,
    private transfer: FileTransfer,
    private file: File,
    private videoEditor: VideoEditor
  ) {
    this.event = navParams.get('event');
  }

  pickVideo() {
    var self = this;
    let image = '';

    let loading = this.createLoading();
    // let options: CaptureVideoOptions = { limit: 1 };
    // this.mediaCapture.captureVideo(options)
    //   .then(
    //     (data: MediaFile[]) => {
    //       loading.present();
    //       let options: FileUploadOptions = {
    //         fileKey: 'picture'
    //       };
    //       let fileTransfer = this.transfer.create();
    //       fileTransfer.upload(data[0].fullPath, 'http://ec2-34-228-21-16.compute-1.amazonaws.com/api/upload/file', options)
    //        .then((response) => {
    //          var data = JSON.parse(response.response)['data'];
    //          var path = data['path'];
    //          self.video = 'http://ec2-34-228-21-16.compute-1.amazonaws.com/storage/' + (path);
    //          loading.dismiss();
    //          console.log(self.video);
    //        }, (err) => {
    //          loading.dismiss();
    //        })
    //     },
    //     (err: CaptureError) => console.error(err)
    //   );
    //

    var self = this;
    var nItem = localStorage.getItem('videoNum');
    var numstr = 0;
    if(nItem == null){
     numstr = 1;
    }
    else{
     var numstr = parseInt(nItem,10);
     numstr = numstr + 1;
    }
    let videooption:CaptureVideoOptions = {limit:1};

    this.mediaCapture.captureVideo().then((videoData:Array<MediaFile>)=>{

      let filedir = this.file.dataDirectory ;

      var path = videoData[0].fullPath.replace('/private','file://');
      var ind = (path.lastIndexOf('/')+1);
      var orgFilename = path.substring(ind);
      var orgFilePath = path.substring(0,ind);

      console.log("videopath", path);
    //SAVE FILE
      // this.file.copyFile(orgFilePath, orgFilename, filedir + 'recordvideo','sample'+numstr+'.mov').then(()=>{
      // var option:CreateThumbnailOptions = {fileUri:filedir+'recordvideo/sample'+numstr+'.mov',width:160, height:206, atTime:1, outputFileName: 'sample' + numstr, quality:50 };
      // this.videoEditor.createThumbnail(option).then(result=>{
      //     //result-path of thumbnail
      //    localStorage.setItem('videoNum',numstr.toString());
      //    self.thumbnail = numstr.toString();

         loading.present();
         let options: FileUploadOptions = {
           fileKey: 'picture'
         };
         let fileTransfer = this.transfer.create();
         fileTransfer.upload(path, 'http://ec2-34-228-21-16.compute-1.amazonaws.com/api/upload/file', options)
          .then((response) => {
            var data = JSON.parse(response.response)['data'];
            var path = data['path'];
            self.video = 'http://ec2-34-228-21-16.compute-1.amazonaws.com/storage/' + (path);
            loading.dismiss();
            console.log(self.video);
          }, (err) => {
            loading.dismiss();
          });
      // }).catch(e=>{
      //  // alert('fail video editor');
      // });
    });
  // });
  }

  send() {
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.sendInvite(this.event.details.id, this.subject, this.body, this.video).then(
      (res) => {
        this.viewCtrl.dismiss();
        loading.dismiss();
      },
      (err) => {

      }
    );
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }
}
