import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { IonWalkthroughOptions } from '../../components/ion-walkthrough';
import { LoginPage } from '../login/login';

/**
 * Generated class for the WalkthroughPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {

  @ViewChild('mySlider') slider: Slides;
  slides: Array<IonWalkthroughOptions>;
  mySlideOptions: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // customize your ionic slider if needed
    // http://ionicframework.com/docs/api/components/slides/Slides/
    this.mySlideOptions = {};

    // let's use the same buttons on each slide.
    // you can define your different button per slide if you want to
    let _this = this; // keeping a reference so we can access `this.slider` from our onClick function
    let buttons = {
      left: {
        text: 'SKIP',
        textColor: '#fff',
        onClick: function() {
          _this.navCtrl.setRoot(LoginPage);
        }
      },
      right: {
        text: 'NEXT',
        textColor: '#fff',
        onClick: function() {
          if (_this.slider.isEnd()){
            _this.navCtrl.setRoot(LoginPage);
          }else{
            _this.slider.slideNext(); //using the ionic slider to go to the next slide
          }
        }
      }
    }

    this.slides = [
      {
        title: 'H-App-y Ever After',
        description: '<b>Web Wed Mobile</b> lets you live stream your special moment and share it with the people you care about most.',
        image: './assets/screenshots/ios_1.png',
        styles: {
          background: 'linear-gradient(to right, #023c6f 0%, #5aabd3 100%)',
          titleColor: '#fff',
          descriptionColor: '#fff'
        },
        layout: {
          position: 'bottom', //top, bottom
          deviceType: 'iphone', //iphone, android
          deviceColor: 'silver' //iphone: silver, spacegrey, gold   android: white, black
        },
        buttons: buttons
      },
      {
        title: 'Near or Far',
        description: 'No one ha to skip out on your special day.',
        image: './assets/screenshots/ios_2.png',
        styles: {
          background: 'linear-gradient(to right, #023c6f 0%, #5aabd3 100%)',
          titleColor: '#fff',
          descriptionColor: '#fff'
        },
        layout: {
          position: 'top',
          deviceType: 'iphone',
          deviceColor: 'spacegrey'
        },
        buttons: buttons
      },


    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalkthroughPage');
  }

}
