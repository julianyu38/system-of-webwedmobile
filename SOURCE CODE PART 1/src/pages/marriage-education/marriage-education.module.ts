import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarriageEducationPage } from './marriage-education';

@NgModule({
  declarations: [
    MarriageEducationPage,
  ],
  imports: [
    IonicPageModule.forChild(MarriageEducationPage),
  ],
})
export class MarriageEducationPageModule {}
