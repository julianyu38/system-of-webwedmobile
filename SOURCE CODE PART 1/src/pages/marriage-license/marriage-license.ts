import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, LoadingController } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular';

import { CreateAnInvitationPage } from '../create-an-invitation/create-an-invitation';
import { PreApprovedOfficiantPage } from '../pre-approved-officiant/pre-approved-officiant';
import { OfficiantDetailsPage } from '../officiant-details/officiant-details';
import { HttpProvider } from '../../providers/http/http';
import { StorageProvider, UserData, EventData, EventDetailData, ContactData } from '../../providers/storage/storage';
import { PickContactPage } from '../pick-contact/pick-contact';
import { AddManualModalPage } from '../add-manual-modal/add-manual-modal';
import { RolePopoverPage } from '../role-popover/role-popover';
import { BroadcastPage } from '../broadcast/broadcast';
import { EmailComposer } from '@ionic-native/email-composer';
import { MarriageApplicationPage } from '../marriage-application/marriage-application';
import { MarriageEducationPage } from '../marriage-education/marriage-education';
import { RegistryLinkPage } from '../registry-link/registry-link';

@Component({
  selector: 'page-marriage-license',
  templateUrl: 'marriage-license.html',
  providers: [HttpProvider, StorageProvider, EmailComposer]
})
export class MarriageLicensePage {

  license: any;
  submitted: any = false;
  signers: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController,
    public httpProvider: HttpProvider,
    private loadingCtrl: LoadingController,
    public popoverCtrl: PopoverController,
    private emailComposer: EmailComposer
  ) {
    //this.refreshLicense();
  }

  private refreshLicense(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.getMarriageLicense(this.navParams.get('id')).then(
      (license) => {
        loading.dismiss();
        console.log(license);
        this.license = license;
        this.submitted = license.submitted;
        this.signers = license.signers;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  goHome(){
    this.navCtrl.popToRoot();
  }

  showParticipantActionSheet(index) {
    this.showActionSheet(true, false).then(
      (users) => {
        this.handleContactsToImportAsParticipant(users['contacts']);
      },
      (err) => {
        console.error(err);
      }
    );
  }

  selected_tab: number = 0;

  list_tabs(event){
    console.log(event);
    this.selected_tab = event;
  }

  actionSheet: any;

  showActionSheet(single, guests) {
    return new Promise((resolve, reject) => {
      var self = this;

      let buttons = [];

      buttons = buttons.concat({
        text: 'Add manually',
        role: 'manually',
        handler: () => {
          this.addManually().then(
            (contacts) => {
              resolve(contacts);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });

      buttons = buttons.concat({
        text: 'Add from Contacts',
        role: 'contact',
        handler: () => {
          console.log('Contacts clicked');
          if (single){
            this.pickFromContacts(1).then(
              (contacts) => {
                resolve(contacts);
              },
              (err) => {
                reject(err);
              }
            );
          }else{
            this.pickFromContacts(-1).then(
              (contacts) => {
                resolve(contacts);
              },
              (err) => {
                reject(err);
              }
            );
          }
        }
      });

      this.actionSheet = this.actionSheetCtrl.create({
        title: '',
        cssClass: 'action-sheets-basic-page',
        buttons: buttons
      });
      this.actionSheet.present();
    });
  }

  handleContactsToImportAsParticipant(contacts: ContactData[]){
    if (contacts.length > 0){
      let contact = contacts[0];
      let loading = this.createLoading();
      loading.present();


    }
  }

  pickFromContacts(limit) : Promise<any> {
    return new Promise((resolve, reject) => {
      let contactModal = this.modalCtrl.create(PickContactPage, {
        limit: limit
      });
      contactModal.onDidDismiss(data => {
        console.log(data);
        if (data){
          resolve(data);
        }
      });
      contactModal.present();

    });
  }

  addManually() : Promise<any> {
    return new Promise((resolve, reject) => {
      let manualModal = this.modalCtrl.create(AddManualModalPage);
      manualModal.onDidDismiss(data => {
        console.log(data);
        if (data){
          resolve(data);
        }
      });
      manualModal.present();

    });
  }

  private createLoading() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait ...'
    });
    return loading;
  }

  public addSigner(identifier){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.addSigner(this.navParams.get('id'), identifier).then(
      (license) => {
        loading.dismiss();
        console.log(license);
        this.license = license;
        this.submitted = license.submitted;
        this.signers = license.signers;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  public removeSigner(id){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.removeSigner(this.navParams.get('id'), id).then(
      (license) => {
        loading.dismiss();
        console.log(license);
        this.license = license;
        this.submitted = license.submitted;
        this.signers = license.signers;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

  public submitLicense(){
    let loading = this.createLoading();
    loading.present();
    this.httpProvider.submitMarriageLicense(this.navParams.get('id')).then(
      (license) => {
        loading.dismiss();
        console.log(license);
        this.license = license;
        this.submitted = license.submitted;
        this.signers = license.signers;
      },
      (err) => {
        loading.dismiss();
      }
    );
  }

}
