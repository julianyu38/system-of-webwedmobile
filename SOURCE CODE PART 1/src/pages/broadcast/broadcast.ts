import { IonicPage, NavController, NavParams, ViewController, ModalController, Platform, ToastController, AlertController } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild } from '@angular/core'
import * as moment from 'moment';
import { HttpProvider } from '../../providers/http/http';
import { EventDetailData } from '../../providers/storage/storage';
import { DashboardPage } from '../../pages/dashboard/dashboard';
import { EventCompletedPage } from '../../pages/event-completed/event-completed';
import { PickEmojiPage } from '../../pages/pick-emoji/pick-emoji';

// import {OpentokService} from "ng2-opentok/dist/opentok.service";
// import {OTSignalEvent} from "ng2-opentok/dist/models/events/signal-event.model";
// import {OTSession} from "ng2-opentok/dist/models/session.model";

/**
 * Generated class for the BroadcastPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

 export interface Message {
   created: string;
   message: string;
   avatar: string;
   visible: boolean;
 }

@IonicPage()
@Component({
  selector: 'page-broadcast',
  templateUrl: 'broadcast.html',
  providers: [HttpProvider]
})
export class BroadcastPage {

  //OT:any;
  //OT:

  guest: boolean = true;
  event: number;
  sessionId : string = '';
  token : string = '';
  role: string = 'participant1';
  numberOfParticipants: 1;
  apiKey: string = "45692092";
  isZoomed: boolean = false;

  loadProgress: number = 0;
  State = {
    ACTIVE: 1,
    DISABLE: 2
  }
  currentState : any = 2;
  max = 100;

  currentCountDownState : any = 2;
  maxCountDown = 100;
  countdownRemaining: number = 100;
  countdownColor = '#d6d8db';
  countdownBackgroundURL = 'assets/images/heart_active.png';
  public showCountDown: boolean = true;
  public countDownPaused: boolean = false;
  public showProgress: boolean = true;
  public remainingMinutes: number = 0;
  public totalMinutes: number = 0;
  public permissions: any;
  public admin: number = 0;

  user: any;
  _subscriptionFunc: any;
  messages: Message[] = [];
  message: string;
  @ViewChild('input') input: ElementRef;

  baseUrl;
  chatTimer;
  heartsTimer;
  chatIndex = 0;

  session : any;
  publisher: any;

  hostCapabilties: boolean = false;
  layout: any;

  subscribers: any = [];
  totalViewers = 0;

  hlsLink : string = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public httpProvider: HttpProvider,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController) {
      setInterval(() => {
        this.messages.forEach((message) => {
          if (moment().valueOf() - moment(message.created).valueOf() > 3000) {
            message.visible = false;
          }
        });
      }, 100);

      this.guest = this.navParams.get('guest');
      this.event = this.navParams.get('event');
      this.user = this.navParams.get('user');

      this.guest = true;
      this.hlsLink = '';

      httpProvider.getEventSpecifications(this.event).then(
        (res) => {
          console.log(res);
          this.remainingMinutes = res['remaining_time'];
          this.totalMinutes = res['total_time'];
          this.permissions = res['permissions'];
          this.admin = res['active_admin'];

          if (!this.guest){
            this.countdownRemaining = (-(this.remainingMinutes / this.totalMinutes)*100) + 100;
            this.currentCountDownState = this.State.ACTIVE;
            console.log(this.countdownRemaining);
            this.doCountDown(); //start heart button
          }else{


            // httpProvider.getBroadcastInformation(this.event).then(
            //   (res) => {
            //     if (res){
            //       this.hlsLink = res;
            //     }
            //   },
            //   (err) => {
            //     console.error(err);
            //   }
            // );
          }

          httpProvider.getEventSessionDetails(this.event, this.user).then(
            (res) => {
              console.log(res);
              this.sessionId = res.opentok.session_id;
              // this.sessionId =
              this.token = res.opentok.token;
              this.role = res.opentok.role;
              this.numberOfParticipants = res.numberOfParticipants;
              // if (!this.guest){
              //   console.log(this.platform.platforms());
              //   if (platform.is('ios')){
              //     (<any>window).cordova.plugins.iosrtc.registerGlobals();
              //     // cordova.plugins.iosrtc.selectAudioOutput('speaker');
              //     (<any>window).getUserMedia = navigator.getUserMedia.bind(navigator);
              //     (<any>window).cordova.plugins.iosrtc.debug.enable('iosrtc*');
              //     (<any>window).OT = (<any>window).cordova.require('cordova-plugin-opentokjs.OpenTokClient');
              //     this.initializeSession();
              //   }else{
                  this.initializeSession();
                  // (<any>window).cordova.exec(null, null, 'OpentokActivator', 'sampleBackgroundView', ['46014622', res.opentok.session_id, res.opentok.token, false]);
                // }
              // }else{
              //   this.initializeSession();
              // }
            },
            (err) => {
              console.error(err);
            }
          );
        },
        (err) => {
          console.error(err);
        }
      );

  }

  showLoader(){
    this.showProgress = true;
    this.loadProgress = 0;
    if(this.currentState == this.State.ACTIVE){
      var refreshIntervalId = setInterval(() => {
        if(this.loadProgress < this.max){
          this.loadProgress++;
        }else{
          this.showProgress = false;
          clearInterval(refreshIntervalId);
        }
      }, 1000);
    }
  }


  sendMessage() {
    var self = this;
    if (!self.message) return;
    self.session.signal({
        type: 'msg',
        data: {
          chat: true,
          message: self.message,
          avatar: this.httpProvider.apiUrl + "/user/"+1+"/avatar"
        }
      }, function(error) {
      if (error) {
        console.log('Error sending signal:', error.name, error.message);
      } else {
        //self.messageT(self.message,self.httpProvider.apiUrl+'/user/1/avatar');
        self.message = '';
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BroadcastPage');
  }

  ngAfterContentInit() {

  }

  messageT(text,avatar)
	{
		var old = (<any>window).$('.message');

		var msg = (<any>window).$('<div>');
		msg.addClass('message');
		msg.html('<div class="clearfix"><div class="avatar"></div><div class="info"></div></div>');
		msg.find('.avatar').html('<img src="'+avatar+'" />');
		msg.find('.info').html('<div class="text">'+text+'</div>');
		msg.appendTo('body');
		msg.css({"left": "-"+(msg.width()+20)+"px"});
		msg.animate({"left": 0}, 200);

		setTimeout(function(){
			msg.fadeOut(400,function(){
				msg.remove();
			});
		}, 7000);

		old.each(function(ind){
			(<any>window).$(this).animate({
				"bottom": parseInt((<any>window).$(this).css("bottom"))+msg.height()+15+"px",
				"opacity": (<any>window).$(this).css("opacity")-0.20
			}, 200, 'swing', function(){
				if((<any>window).$(this).css("opacity") == 0)
					(<any>window).$(this).remove();
			});
		});
	}
  messageEmoji(image,avatar)
	{
		var old = (<any>window).$('.message');

		var msg = (<any>window).$('<div>');
		msg.addClass('message');
		msg.html('<div class="clearfix"><div class="avatar"></div><div class="info"></div></div>');
		msg.find('.avatar').html('<img src="'+avatar+'" />');
		msg.find('.info').html('<div class="text"><img src="'+image+'" style="width:75px;height:75px;"/></div>');
		msg.appendTo('body');
		msg.css({"left": "-"+(msg.width()+20)+"px"});
		msg.animate({"left": 0}, 200);

		setTimeout(function(){
			msg.fadeOut(400,function(){
				msg.remove();
			});
		}, 7000);

		old.each(function(ind){
			(<any>window).$(this).animate({
				"bottom": parseInt((<any>window).$(this).css("bottom"))+msg.height()+15+"px",
				"opacity": (<any>window).$(this).css("opacity")-0.20
			}, 200, 'swing', function(){
				if((<any>window).$(this).css("opacity") == 0)
					(<any>window).$(this).remove();
			});
		});
	}

  handleError(error) {
    if (error) {
      alert(error.message);
    }
  }

  getSubscriberElement = function(subscriber){
    return "layoutContainer";
  };

  getPublisherElement = function(){
    return "publisherContainer";
  };

  updateLayout(){
    this.layout();
    // if (this.platform.is('ios')){
    //   (<any>window).cordova.plugins.iosrtc.refreshVideos();
    // }
  }

  publishStream(camera: any){
    var audioInputDevices;
    var videoInputDevices;
    var self = this;
    if (self.publisher){
        self.session.unpublish(self.publisher);
    }

    (<any>window).OT.getDevices(function(error, devices) {
      audioInputDevices = devices.filter(function(element) {
        return element.kind == "audioInput";
      });
      videoInputDevices = devices.filter(function(element) {
        return element.kind == "videoInput";
      });
      for (var i = 0; i < audioInputDevices.length; i++) {
        console.log("audio input device: ", audioInputDevices[i].deviceId);
      }
      for (i = 0; i < videoInputDevices.length; i++) {
        console.log("video input device: ", JSON.stringify(videoInputDevices[i]));
      }

      let videoDeviceId = videoInputDevices[0].deviceId;
      if (camera != null){
        if (camera == 'front'){
          if (videoInputDevices.length==1){
            let videoDeviceId = videoInputDevices[0].deviceId;
          }else{
            let videoDeviceId = videoInputDevices[1].deviceId;
          }
        }else{
          let videoDeviceId = videoInputDevices[0].deviceId;
        }
      }else{
        let videoDeviceId = videoInputDevices[0].deviceId;
      }

      var pubOptions =
      {
        audioSource: audioInputDevices[0].deviceId,
        videoSource: videoDeviceId
      };
      //self.publisher = (<any>window).OT.initPublisher(self.getPublisherElement(), {insertMode: 'append', width: '100%', height: '100%'}, (<any>window).handleError);
      self.publisher = (<any>window).OT.initPublisher(self.getPublisherElement(), pubOptions);
      self.updateLayout();
      (<any>window).cordova.plugins.iosrtc.refreshVideos();
      (<any>window).testing = self.publisher;
    });
  }

  initializeSession() {
    /*var layoutContainer = document.getElementById("layoutContainer");

    // Initialize the layout container and get a reference to the layout method
    this.layout = (<any>window).initLayoutContainer(layoutContainer, {
      maxRatio: 3/2,     // The narrowest ratio that will be used (default 2x3)
      minRatio: 9/16,      // The widest ratio that will be used (default 16x9)
      fixedRatio: false,  // If this is true then the aspect ratio of the video is maintained and minRatio and maxRatio are ignored (default false)
      bigClass: "OT_big", // The class to add to elements that should be sized bigger
      bigPercentage: 0.8,  // The maximum percentage of space the big ones should take up
      bigFixedRatio: false, // fixedRatio for the big ones
      bigMaxRatio: 3/2,     // The narrowest ratio to use for the big elements (default 2x3)
      bigMinRatio: 9/16,     // The widest ratio to use for the big elements (default 16x9)
      bigFirst: true        // Whether to place the big one in the top left (true) or bottom right
    }).layout;

    (<any>window).layout = this.layout;*/

    (<any>window).cordova.exec(null, null, 'OpentokActivator', 'sampleBackgroundView', ['45692092', this.sessionId, this.token, false]);

    (<any>window).handleError = function(error){
      if (error){
        console.error(error.message);
      }
    }

    var session = (<any>window).OT.initSession(this.apiKey, this.sessionId);
    var self = this;

    if (session.capabilities.forceDisconnect == 1){
      self.hostCapabilties = true;
    }else{
      self.hostCapabilties = false;
    }


    this.session = session;

    this.session.on('connectionCreated', function(event){ self.connectionCreated(event); });
    this.session.on('connectionDestroyed', function(event){ self.connectionDestroyed(event); });
    //this.session.on('streamCreated', function(event){ self.streamCreated(event); });
    //this.session.on('streamDestroyed', function(event){ self.streamDestroyed(event); });
    this.session.on('signal:eventCompleted', function(event){ self.eventCompleted(event); });
    this.session.on('eventCompleted', function(event){ self.eventCompleted(event); });
    this.session.on('signal:msg', function(event){ self.receivedChatMessage(event); });
    this.session.on('signal:emoji', function(event){ self.receivedEmojiMessage(event); });
    this.session.on('signal:publish', function(event){ self.eventPlayed(event); });
    this.session.on('signal:unpublish', function(event){ self.eventPaused(event); });

    /*if (!self.guest) {
      self.publishStream('front');
    }*/

    this.session.connect(this.token, function(error) {
      if (error) {
        console.log(error);
        (<any>window).handleError(error);
      } else {
        if (!self.guest){
          /*self.showCountdownToast();
          setTimeout(function(){
            self.session.publish(self.publisher, (<any>window).handleError);
            self.updateLayout();
            self.httpProvider.activateEvent(self.event).then(
              (res) => {
                console.log(res);
              },
              (err) => {
                console.error(err);
              }
            );
          }, 10000);*/
        }
      }
    });
  }

  connectionCreated = function(event){
    console.log('Connection Created');
    this.totalViewers += 1;
  }

  connectionDestroyed = function(event){
    console.log('Connection Destroyed');
    this.eventCompleted(event);
  }

  streamCreated = function(event){
    (<any>window).cordova.plugins.iosrtc.refreshVideos();
    console.log('streamCreated');
    var div = this.getSubscriberElement(event);
    var subscriber = this.session.subscribe( event.stream, div, {subscribeToAudio: true, insertMode: 'append', width: '100%', height: '100%'} );
    this.subscribers.push(subscriber);
    this.updateLayout();
  }

  streamDestroyed = function(event){
    console.log('streamDestroyed');
  }

  eventCompleted = function(event){
    console.log('eventCompleted');
    if (!this.guest){
      clearInterval(this.refreshCountdownIntervalId);
      //this.session.unpublish(this.publisher);
      this.session.disconnect();
      this.navCtrl.setRoot(EventCompletedPage, {
        total: this.totalViewers
      });
    }else{
      //this.viewCtrl.dismiss();
      this.navCtrl.setRoot(EventCompletedPage, {
        total: this.totalViewers
      });
    }

    // this.navCtrl.pop();
  }

  eventPaused = function(event) {

  }

  eventPlayed = function(event) {

  }

  receivedChatMessage = function(event){
    if (this.guest == false){
      return;
    }

    console.log('receivedChatMessage');
    if (event.data.chat){
      //if (!(event.from.connectionId === this.session.connection.connectionId)){
        this.messageT(event.data.message,event.data.avatar);
      //}
    }
  }

  receivedEmojiMessage = function(event){
    if (this.guest == false){
      return;
    }

    console.log('receivedChatMessage');
    if (event.data.image){
      //if (!(event.from.connectionId === this.session.connection.connectionId)){
        this.messageEmoji(event.data.image,event.data.avatar);
      //}
    }
  }

  startCountDown(){
    // if (this.currentCountDownState == this.State.ACTIVE){
      this.showCountDown = true;
      // this.countdownRemaining = this.maxCountDown;
      this.countDownPaused = false;
      this.doCountDown();
    // }
  }

  pauseCountDown(){
    this.showCountDown = true;
    this.countDownPaused = true;
  }
  resumeCountDown(){
    this.showCountDown = true;
    this.countDownPaused = false;
    this.doCountDown();
  }

  countdown(){
    if(this.countdownRemaining > 0){
      console.log(this.countdownRemaining);
      this.countdownRemaining--;
      if (this.countdownRemaining < 0.25*this.maxCountDown) {
        // red color
        this.countdownColor = '#ce4a30';
        this.countdownBackgroundURL="assets/images/heart_broadcast.png";
        this.showCountdownWarningToast();
      } else if (this.countdownRemaining < 0.5*this.maxCountDown) {
        // amber color
        this.countdownColor = '#f9ac00';
        this.countdownBackgroundURL="assets/images/heart_active.png";
        this.showCountdownWarningToast();
      } else {
        // normal color
        this.countdownColor = '#00bbd3';
        this.countdownBackgroundURL="assets/images/heart_disable.png";
      }

      if (this.countDownPaused == true) {
        clearInterval(this.refreshCountdownIntervalId);
      }

    }else{
      if (this.session && this.currentCountDownState == this.State.ACTIVE){
        // this.session.signal({
        //     type: 'eventCompleted',
        //     data: { }
        //   }, function(error) {
        // });
        // this.showCountDown = false;
        clearInterval(this.refreshCountdownIntervalId);
      }
    }
  }

  refreshCountdownIntervalId = null;

  doCountDown(){
    // if(this.currentCountDownState == this.State.ACTIVE){
      var self = this;
      self.refreshCountdownIntervalId = setInterval(() => {
        self.countdown();
      }, ((60000*this.totalMinutes) / 100));
      self.countdown();
    // }
  }

  isCreator(){
    if (this.role == 'participant1'){
      return true;
    }else{
      return false;
    }
  }

  showCountdownToast() {
    let toast = this.toastCtrl.create({
      message: 'You will go live in 10 seconds.',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    //toast.present();
  }

  showCountdownWarningToast() {
    let toast = this.toastCtrl.create({
      message: 'You have 5 minutes or less remaining!',
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    //toast.present();

  }

  countdownClicked(){
    if (!this.isCreator()){
      var self = this;
      let alert = this.alertCtrl.create({
        title: 'Leave event',
        message: 'Do you want to leave this event?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              self.session.unpublish(self.publisher);
              self.session.disconnect();
              self.viewCtrl.dismiss();
            }
          }
        ]
      });
      alert.present();
    }else{
      var self = this;
      let alert = this.alertCtrl.create({
        title: 'Terminate event',
        message: 'Do you want to terminate this event? You will forfeit any remaining time.',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {

              self.session.signal({
                  type: 'eventCompleted',
                  data: { }
                }, function(error) {
              });
              // this.showCountDown = false;
              clearInterval(self.refreshCountdownIntervalId);
            }
          }
        ]
      });
      alert.present();
      // if(this.countdownRemaining == this.maxCountDown) {
      //   // not started yet, please start
      //   this.doCountDown();
      //   this.session.signal({
      //       type: 'publish',
      //       data: { }
      //     }, function(error) {
      //   });
      //   return;
      // } else if (this.countDownPaused == false) {
      //   this.pauseCountDown();
      //   //this.session.unpublish(this.publisher);
      //   this.httpProvider.pauseStream(this.event, 4).then(
      //     (res) => {
      //       this.session.signal({
      //           type: 'unpublish',
      //           data: { }
      //         }, function(error) {
      //       });
      //       console.log(res);
      //     },
      //     (err) => {
      //       console.error(err);
      //     }
      //   );
      //   return;
      // } else {
      //   this.resumeCountDown();
      //   return;
      // }
    }
  }

  resize(){
    if (this.isZoomed){
      //videostream.addClass('bg-vid-zoom');
      this.isZoomed = false;
    }else{
      //videostream.removeClass('bg-vid-zoom');
      this.isZoomed = false;
    }
  }

  isFrontCamera: boolean = true;

  flipCamera(){
    if (this.isFrontCamera){
      this.publishStream('back');
      this.isFrontCamera = false;
    }else{
      this.publishStream('front');
      this.isFrontCamera = true;
    }
  }

  isMuted: boolean = false;

  mute(){
    if (this.isMuted){
      this.publisher.publishAudio(!this.isMuted);
      this.isMuted = false;
    }else{
      this.publisher.publishAudio(!this.isMuted);
      this.isMuted = true;
    }
  }

  close(){
    if (this.session){
      this.session.disconnect();
    }
    (<any>window).cordova.exec(null, null, 'OpentokActivator', 'disconnect', []);
    //this.navCtrl.setRoot(DashboardPage);
  }

  ionViewWillLeave() {
    //this.session.disconnect();
  }

  pickEmoji() {
    var self = this;
    let emojiModal = this.modalCtrl.create(PickEmojiPage);
    emojiModal.onDidDismiss(data => {
      console.log(data);
      if (data){
        self.session.signal({
            type: 'emoji',
            data: {
              image: data.image,
              avatar: self.httpProvider.apiUrl + "/user/"+1+"/avatar"
            }
          }, function(error) {
        });
      }
    });
    emojiModal.present();
  }

}
