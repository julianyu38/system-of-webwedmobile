webpackJsonp([0],{

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WedDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_an_invitation_create_an_invitation__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pre_approved_officiant_pre_approved_officiant__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pick_contact_pick_contact__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__add_manual_modal_add_manual_modal__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__marriage_license_marriage_license__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__broadcast_broadcast__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_email_composer__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__marriage_application_marriage_application__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__marriage_education_marriage_education__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__registry_link_registry_link__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__my_account_my_account__ = __webpack_require__(87);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















/**
 * Generated class for the WedDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var WedDetailsPage = (function () {
    function WedDetailsPage(navCtrl, navParams, actionSheetCtrl, modalCtrl, httpProvider, loadingCtrl, popoverCtrl, emailComposer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.modalCtrl = modalCtrl;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.popoverCtrl = popoverCtrl;
        this.emailComposer = emailComposer;
        this.wed_details = 'details';
        this.participants = [];
        this.guests = [];
        this.privacy = false;
        this.loadProgress = 0;
        this.State = {
            ACTIVE: 1,
            DISABLE: 2
        };
        this.currentState = 2;
        this.max = 100;
        this.maxCountDown = 100;
        this.countdownRemaining = 100;
        this.countdownColor = '#d6d8db';
        this.countdownBackgroundURL = 'assets/images/heart_active.png';
        this.showCountDown = true;
        this.countDownPaused = false;
        this.showProgress = true;
        this.selected_tab = 0;
        this.wait = false;
        this.event = this.navParams.get('event');
        console.log(this.event);
        this.refreshParticipants();
        this.refreshGuests();
        this.user = this.navParams.get('user');
        // this.httpProvider.isPublic(this.event.details.id).then(
        //   (res) => {
        //     this.privacy = res;
        //   },
        //   (err) => {
        //
        //   }
        // );
        // if (this.event.details.status == 0){
        //   this.currentCountDownState = this.State.ACTIVE;
        // }else{
        //   this.currentCountDownState = this.State.DISABLE;
        //   this.countdownColor = '#00bbd3';
        //   this.countdownBackgroundURL="../http://ec2-34-228-21-16.compute-1.amazonaws.com/assets/images/heart_disable.png";
        // }
    }
    WedDetailsPage.prototype.showLoader = function () {
        var _this = this;
        this.showProgress = true;
        this.loadProgress = 0;
        if (this.currentState == this.State.ACTIVE) {
            var refreshIntervalId = setInterval(function () {
                if (_this.loadProgress < _this.max) {
                    _this.loadProgress++;
                }
                else {
                    _this.showProgress = false;
                    clearInterval(refreshIntervalId);
                }
            }, 1000);
        }
    };
    WedDetailsPage.prototype.refreshParticipants = function () {
        this.participants = [];
        // console.log(this.event.participants);
        for (var i = 0; i < 5; i++) {
            if (this.event.participants[i]) {
                if (this.event.participants[i].role == 'creator') {
                    continue;
                }
                var avatar = '';
                if (typeof (this.event.users[this.event.participants[i].user].personal.avatar) != 'undefined') {
                    avatar = this.event.users[this.event.participants[i].user].personal.avatar;
                }
                else {
                    avatar = this.httpProvider.apiUrl + '/user/' + this.event.users[this.event.participants[i].user].personal.id + '/avatar';
                }
                this.participants = this.participants.concat({
                    empty: false,
                    avatar: avatar,
                    name: this.event.users[this.event.participants[i].user].personal.name.full,
                    status: this.event.participants[i].status,
                    role: (this.event.participants[i].role == 'unknown' ? 'Select a Role' : this.event.participants[i].role)
                });
            }
            else {
                this.participants = this.participants.concat({
                    empty: true,
                    avatar: 'add',
                    name: 'Add User',
                    status: '',
                    role: 'Select a Role'
                });
            }
        }
        // console.log(this.participants);
    };
    WedDetailsPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad WedDetailsPage');
    };
    WedDetailsPage.prototype.createInvite = function () {
        if (this.currentCountDownState == 2) {
            return;
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__create_an_invitation_create_an_invitation__["a" /* CreateAnInvitationPage */], {
            event: this.event
        });
    };
    WedDetailsPage.prototype.showParticipantActionSheet = function (index) {
        var _this = this;
        if (this.currentCountDownState == this.State.DISABLE) {
            return;
        }
        this.showActionSheet(true, false).then(function (users) {
            _this.handleContactsToImportAsParticipant(users['contacts']);
        }, function (err) {
            console.error(err);
        });
    };
    WedDetailsPage.prototype.showGuestActionSheet = function () {
        var _this = this;
        if (this.currentCountDownState == 2) {
            return;
        }
        this.showActionSheet(true, true).then(function (users) {
            _this.handleContactsToImportAsGuests(users['contacts']);
        }, function (err) {
            console.error(err);
        });
    };
    WedDetailsPage.prototype.list_tabs = function (event) {
        console.log(event);
        this.selected_tab = event;
    };
    WedDetailsPage.prototype.showActionSheet = function (single, guests) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var self = _this;
            var buttons = [];
            buttons = buttons.concat({
                text: 'Add manually',
                role: 'manually',
                handler: function () {
                    _this.addManually().then(function (contacts) {
                        resolve(contacts);
                    }, function (err) {
                        reject(err);
                    });
                }
            });
            buttons = buttons.concat({
                text: 'Add from Contacts',
                role: 'contact',
                handler: function () {
                    console.log('Contacts clicked');
                    if (single) {
                        _this.pickFromContacts(1).then(function (contacts) {
                            resolve(contacts);
                        }, function (err) {
                            reject(err);
                        });
                    }
                    else {
                        _this.pickFromContacts(-1).then(function (contacts) {
                            resolve(contacts);
                        }, function (err) {
                            reject(err);
                        });
                    }
                }
            });
            if (!guests) {
                buttons = buttons.concat({
                    text: 'Pre-Approved Officiants',
                    icon: 'star',
                    role: 'pre-approve',
                    handler: function () {
                        console.log('Pre-approve clicked');
                        // this.navCtrl.push(PreApprovedOfficiantPage);
                        var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pre_approved_officiant_pre_approved_officiant__["a" /* PreApprovedOfficiantPage */], {});
                        modal.onDidDismiss(function (data) {
                            if (data == null) {
                                return;
                            }
                            var loading = _this.createLoading();
                            loading.present();
                            //console.log(data);
                            _this.httpProvider.addParticipant(_this.event.details.id, {
                                name: '',
                                user: data.officiant
                            }).then(function (res) {
                                _this.event = res;
                                _this.refreshParticipants();
                                loading.dismiss();
                            }, function (err) {
                                console.error(err);
                                loading.dismiss();
                            });
                        });
                        modal.present();
                    }
                });
            }
            _this.actionSheet = _this.actionSheetCtrl.create({
                title: '',
                cssClass: 'action-sheets-basic-page',
                buttons: buttons
            });
            _this.actionSheet.present();
        });
    };
    WedDetailsPage.prototype.clearParticipant = function (index) {
        var _this = this;
        var self = this;
        // setTimeout(function(){
        //   self.actionSheet.dismiss();
        // }, 500);
        // this.actionSheet.dismiss();
        this.httpProvider.removeParticipant(this.event.details.id, index).then(function (res) {
            _this.actionSheet.dismiss();
            _this.event = res;
            _this.refreshParticipants();
        }, function (err) {
            _this.actionSheet.dismiss();
        });
    };
    WedDetailsPage.prototype.importContacts = function () {
        var _this = this;
        if (this.currentCountDownState == this.State.DISABLE) {
            return;
        }
        this.pickFromContacts(-1).then(function (contacts) {
            _this.handleContactsToImportAsGuests(contacts['contacts']);
        }, function (err) {
            console.error(err);
        });
    };
    WedDetailsPage.prototype.handleContactsToImportAsGuests = function (contacts) {
        var tmpContacts = [];
        var contactsLeft = contacts.length;
        while (contactsLeft != 0) {
            //@TODO implement catch system for preventing additional invite to the same contact.
            for (var i = 0; i < this.guests.length; i++) {
                if (this.guests[i].email == contacts[contactsLeft - 1].email && this.guests[i].phone == contacts[contactsLeft - 1].phone) {
                    contactsLeft -= 1;
                    break;
                }
            }
            tmpContacts = tmpContacts.concat(contacts[contactsLeft - 1]);
            contactsLeft -= 1;
            continue;
        }
        for (var i = 0; i < tmpContacts.length; i++) {
            this.guests = this.guests.concat(tmpContacts[i]);
        }
        this.updateGuests();
    };
    WedDetailsPage.prototype.updateGuests = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.updateGuestList(this.event.details.id, this.guests).then(function (res) {
            _this.event = res;
            _this.refreshGuests();
            loading.dismiss();
        }, function (err) {
            console.error(err);
            _this.refreshGuests();
            loading.dismiss();
        });
    };
    WedDetailsPage.prototype.refreshGuests = function () {
        this.guests = [];
        this.guests = this.event.guest;
    };
    WedDetailsPage.prototype.handleContactsToImportAsParticipant = function (contacts) {
        var _this = this;
        if (contacts.length > 0) {
            var contact = contacts[0];
            var loading_1 = this.createLoading();
            loading_1.present();
            this.httpProvider.addParticipant(this.event.details.id, contact).then(function (res) {
                _this.event = res;
                _this.refreshParticipants();
                loading_1.dismiss();
            }, function (err) {
                console.error(err);
            });
        }
    };
    WedDetailsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.httpProvider.getEventDetailsById(this.event.details.id).then(function (res) {
            _this.event = res;
            _this.refreshParticipants();
            _this.refreshGuests();
            refresher.complete();
        }, function (err) {
            console.error(err);
        });
    };
    WedDetailsPage.prototype.pickFromContacts = function (limit) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var contactModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__pick_contact_pick_contact__["a" /* PickContactPage */], {
                limit: limit
            });
            contactModal.onDidDismiss(function (data) {
                console.log(data);
                if (data) {
                    resolve(data);
                }
            });
            contactModal.present();
        });
    };
    WedDetailsPage.prototype.addManually = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var manualModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__add_manual_modal_add_manual_modal__["a" /* AddManualModalPage */]);
            manualModal.onDidDismiss(function (data) {
                console.log(data);
                if (data) {
                    resolve(data);
                }
            });
            manualModal.present();
        });
    };
    WedDetailsPage.prototype.handle = function () {
        var _this = this;
        var i = this.i;
        var roles = this.roles;
        var participantId = this.participantId;
        console.log(i);
        console.log(roles);
        var loading = this.self.createLoading();
        loading.present();
        if (typeof (roles) != 'undefined') {
            if (roles == null) {
                loading.dismiss();
                //console.log('null');
                return;
            }
            if (roles[i] == null) {
                loading.dismiss();
                return;
            }
            this.self.httpProvider.updateParticipant(this.self.event.details.id, participantId, roles[i].id).then(function (res) {
                loading.dismiss();
                _this.self.event = res;
                _this.self.refreshParticipants();
                console.log('updated');
            }, function (err) {
                console.log('error');
                loading.dismiss();
            });
        }
        else {
            loading.dismiss();
        }
    };
    WedDetailsPage.prototype.selectRole = function (participantId) {
        var _this = this;
        if (this.participants[participantId].empty) {
            return;
        }
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.getEventConfig().then(function (res) {
            console.log(res);
            loading.dismiss();
            var config = res;
            var roles = config.roles;
            var buttons = [];
            var self = _this;
            // let updateParticipant = self.httpProvider.updateParticipant.bind(self);
            // self.handle.bind(updateParticipant, self.event.details.id, participantId, roles,i)
            for (var i = 0; i < roles.length; i++) {
                var handler = {
                    self: self,
                    participantId: participantId,
                    roles: roles,
                    i: i
                };
                buttons = buttons.concat({
                    text: roles[i].name,
                    icon: '',
                    handler: self.handle.bind(handler)
                });
            }
            _this.actionSheet = _this.actionSheetCtrl.create({
                title: '',
                cssClass: 'action-sheets-basic-page',
                buttons: buttons
            });
            _this.actionSheet.present();
        }, function (err) {
        });
    };
    WedDetailsPage.prototype.viewLicense = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__marriage_license_marriage_license__["a" /* MarriageLicensePage */]);
    };
    WedDetailsPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    WedDetailsPage.prototype.startCountDown = function () {
        this.showCountDown = true;
        this.countdownRemaining = this.maxCountDown;
        this.currentCountDownState == this.State.ACTIVE;
        this.countDownPaused = false;
        this.doCountDown();
    };
    WedDetailsPage.prototype.pauseCountDown = function () {
        this.showCountDown = true;
        this.countDownPaused = true;
    };
    WedDetailsPage.prototype.resumeCountDown = function () {
        this.showCountDown = true;
        this.countDownPaused = false;
        this.doCountDown();
    };
    WedDetailsPage.prototype.doCountDown = function () {
        var _this = this;
        if (this.currentCountDownState == this.State.ACTIVE) {
            var refreshCountdownIntervalId = setInterval(function () {
                if (_this.countdownRemaining > 0) {
                    _this.countdownRemaining--;
                    if (_this.countdownRemaining < 0.25 * _this.maxCountDown) {
                        // red color
                        _this.countdownColor = '#ce4a30';
                        _this.countdownBackgroundURL = "assets/images/heart_broadcast.png";
                    }
                    else if (_this.countdownRemaining < 0.5 * _this.maxCountDown) {
                        // amber color
                        _this.countdownColor = '#f9ac00';
                        _this.countdownBackgroundURL = "assets/images/heart_active.png";
                    }
                    else {
                        // normal color
                        _this.countdownColor = '#00bbd3';
                        _this.countdownBackgroundURL = "assets/images/heart_disable.png";
                    }
                    if (_this.countDownPaused == true) {
                        clearInterval(refreshCountdownIntervalId);
                    }
                }
                else {
                    // this.showCountDown = false;
                    clearInterval(refreshCountdownIntervalId);
                }
            }, 50);
        }
    };
    WedDetailsPage.prototype.countdownClicked = function () {
        if (this.currentCountDownState == 2) {
            return;
        }
        this.httpProvider.getEventSessionDetails(this.event.details.id, this.user).then(function (res) {
            console.log(res);
            //this.sessionId = res.opentok.session_id;
            // this.sessionId =
            //this.token = res.opentok.token;
        }, function (err) {
            console.error(err);
        });
        // (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', []);
        // alert('OPENTOK SERVICE NOT AVAILABLE.');
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_9__broadcast_broadcast__["a" /* BroadcastPage */], {
            guest: false,
            event: this.event.details.id,
            user: this.user
        });
        // if(this.countdownRemaining == this.maxCountDown) {
        //   // not started yet, please start
        //   this.startCountDown();
        //   return;
        // } else if (this.countDownPaused == false) {
        //   this.pauseCountDown();
        //   return;
        // } else {
        //   this.resumeCountDown();
        //   return;
        // }
    };
    WedDetailsPage.prototype.getHelp = function () {
        window.cordova.plugins.email.open({
            app: 'mailto',
            to: 'info@riveloper.com',
            subject: 'WebWed Mobile - App Support',
            body: 'I am having technical issues with the WebWed Mobile app.  Please get back to me at when you can. My contact information is listed below:'
        });
    };
    WedDetailsPage.prototype.privacyUpdate = function (item) {
        var _this = this;
        var self = this;
        if (this.wait) {
            return;
        }
        console.log(this.privacy);
        this.httpProvider.updatePublic(this.event.details.id, this.privacy).then(function (res) {
            _this.privacy = res;
            _this.wait = true;
            setTimeout(function () {
                self.wait = false;
            }, 1500);
        }, function (err) {
        });
    };
    WedDetailsPage.prototype.gotoMarriageRegistry = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_13__registry_link_registry_link__["a" /* RegistryLinkPage */], {
            id: this.event.details.id,
            user: this.user
        });
    };
    WedDetailsPage.prototype.gotoMarriageEducation = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_12__marriage_education_marriage_education__["a" /* MarriageEducationPage */], {
            id: this.event.details.id,
            user: this.user
        });
    };
    WedDetailsPage.prototype.gotoMarriageLicense = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__marriage_license_marriage_license__["a" /* MarriageLicensePage */], {
            id: this.event.details.id,
            user: this.user
        });
    };
    WedDetailsPage.prototype.gotoMarriageApplication = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__marriage_application_marriage_application__["a" /* MarriageApplicationPage */], {
            id: this.event.details.id,
            user: this.user
        });
    };
    WedDetailsPage.prototype.gotoOrderDetails = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_14__my_account_my_account__["a" /* MyAccountPage */], {
            order: true
        });
    };
    return WedDetailsPage;
}());
WedDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-wed-details',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/wed-details/wed-details.html"*/'<ion-header>\n  <sub-navigation title="{{event.details.title || \'\'}}" [tabs]="[\'details\',\'participants\',\'guests\']" (selected-tab)="list_tabs($event)"></sub-navigation>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <div class="details-content">\n    <div [ngSwitch]="selected_tab">\n      <ion-list *ngSwitchCase="0" class="detail-section">\n        <ion-card class="wed-details-card">\n          <div class="card-overlay"></div>\n          <img [src]="event.details.photo"/>\n          <div class="card-title">{{event.details.title}}</div>\n          <div class="card-subtitle">{{event.details.date * 1000 | date:"MMM dd \' | \' h:mm a"}}</div>\n          <div class="card-bottom-subtitle">Event ID# {{event.details.id}}</div>\n        </ion-card>\n        <ion-list no-lines class="white-bg">\n          <ion-grid>\n              <ion-row text-center>\n                <ion-col col-4 style="min-height:150px;">\n                  <options (click)="gotoOrderDetails()" [event]="event.details.id"></options>\n                  <br/>\n                </ion-col>\n                <ion-col col-4 text-center>\n                  <broadcast-count-down *ngIf="showCountDown"\n                    [progress]="countdownRemaining"\n                    [state]="currentCountDownState"\n                    [width]="\'100\'"\n                    [track-color]="countdownColor"\n                    [placeholder-color]="\'#d6d8db\'"\n                    [max]="maxCountDown"\n                    [backgroundUrl]="countdownBackgroundURL"\n                    (click)="countdownClicked()"\n                    ></broadcast-count-down>\n                </ion-col>\n                <ion-col col-4 style="min-height:150px;">\n                  <share [event]="event.details.id"></share>\n                  <br/>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-list>\n          <ion-list no-lines class="white-bg details-list">\n            <ion-grid>\n              <ion-row>\n                <ion-col col-12>\n                  <ion-item class="marg-header">\n                    <ion-icon name="time" mini color="dark" item-start></ion-icon>\n\n                    <h2>{{event.details.package.name}}</h2>\n\n                  </ion-item>\n                  <p class="small">You can stream for a consecutive window of time based on the package pre-selected for your event. You will not be able to start and stop the stream at random; and the video stream may only be accessed again if you experience technical issues. Any remaining balance of time will be removed and the event will be closed</p>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <ion-grid *ngIf="event.details.addons.registry==true" (click)="gotoMarriageRegistry()">\n              <ion-row>\n                <ion-col col-12>\n                  <ion-item class="marg-header">\n                    <button class="mini-star-btn bright-color" style="margin-right:30px;" item-start> <ion-icon name="star" mini color="light"></ion-icon></button>\n                    <h2>Registry Link</h2>\n                    <ion-icon name="ios-arrow-forward" mode="ios" mini color="dark" item-end></ion-icon>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n\n            <ion-grid *ngIf="event.details.addons.marriage_education==true" (click)="gotoMarriageEducation()">\n              <ion-row>\n                <ion-col col-12>\n                  <ion-item class="marg-header">\n                    <button class="mini-star-btn bright-color" style="margin-right:30px;" item-start> <ion-icon name="star" mini color="light"></ion-icon></button>\n                    <h2>Premarital Education</h2>\n                    <ion-icon name="ios-arrow-forward" mode="ios" mini color="dark" item-end></ion-icon>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n             <ion-grid *ngIf="event.details.addons.marriage_application==true" (click)="gotoMarriageApplication()">\n              <ion-row>\n                <ion-col col-12>\n                  <ion-item class="marg-header">\n                    <button class="mini-star-btn bright-color" style="margin-right:30px;" item-start> <ion-icon name="star" mini color="light"></ion-icon></button>\n                    <h2>Marriage Application</h2>\n                    <ion-icon name="ios-arrow-forward" mode="ios" mini color="dark" item-end></ion-icon>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <ion-grid *ngIf="event.details.addons.marriage_license==true" (click)="gotoMarriageLicense()">\n              <ion-row>\n                <ion-col col-12>\n                  <ion-item class="marg-header">\n                    <button class="mini-star-btn bright-color" style="margin-right:30px;" item-start> <ion-icon name="star" mini color="light"></ion-icon></button>\n                    <h2>Marriage License</h2>\n                    <ion-icon name="ios-arrow-forward" mode="ios" mini color="dark" item-end></ion-icon>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <!-- <ion-grid>\n              <ion-row>\n                <ion-col col-12>\n                  <ion-item class="marg-header">\n                    <button class="mini-star-btn bright-color" item-start> <ion-icon name="star" mini color="light"></ion-icon></button>\n                    <h2>Registry Link</h2>\n                    <button ion-button disabled clear item-end class="edit-btn"><ion-icon name="time" mini color="dark" item-end></ion-icon></button>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-grid> -->\n          </ion-list>\n\n           <ion-item text-center class="bottom-need-help">\n            <button ion-button full clear (click)="getHelp()">Need Help ?</button>\n           </ion-item>\n      </ion-list>\n\n      <ion-list *ngSwitchCase="1" class="participant-section">\n        <ion-list-header text-center text-wrap no-lines class="participant-header-text">\n          <h2>Select up to 4 participants</h2>\n          <p><strong>Important</strong> Participants should be added for live <strong>multi-stream</strong> video events only. If you intend to use a single broadcast you should skip this step, regardless of the participants in your actualy party.</p>\n        </ion-list-header>\n        <ion-item no-lines>\n         <ion-grid>\n            <ion-row text-center>\n              <ion-col *ngFor="let index of [0,1];" col-6>\n                <div *ngIf="this.participants[index].empty==false" (click)="showParticipantActionSheet(index)" class="partcipant-avatar" text-center>\n                  <img *ngIf="this.participants[index].empty==false" [src]="this.participants[index].avatar">\n                  <ion-icon *ngIf="this.participants[index].empty==true" name="add" class="add-icon"></ion-icon>\n                  <!-- <ion-badge *ngIf="this.participants[index].status==0" text-center color="light" round class="light-badge">No Response</ion-badge> -->\n                  <ion-badge *ngIf="this.participants[index].status==1" text-center color="secondary" round class="accept-badge">Accepted</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==2" text-center color="light" round class="decline-badge">Tenative</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==3" text-center color="pink" round class="decline-badge">Declined</ion-badge>\n                  <!-- <button [attr.disabled]="currentCountDownState == 2 ? true : null" *ngIf="this.participants[index].empty==false" (click)="clearParticipant(index)" ion-button menuClose clear class="close-btn">\n                    <ion-icon name="close-circle"></ion-icon>\n                  </button> -->\n                </div>\n                <ion-item *ngIf="this.participants[index].empty==true" (click)="showParticipantActionSheet(index)" class="add-user-participant" text-center>\n                  <img *ngIf="this.participants[index].empty==false" [src]="this.participants[index].avatar">\n                  <ion-icon *ngIf="this.participants[index].empty==true" name="add" class="add-icon"></ion-icon>\n                  <!-- <ion-badge *ngIf="this.participants[index].status==0" text-center color="light" round class="light-badge">No Response</ion-badge> -->\n                  <ion-badge *ngIf="this.participants[index].status==1" text-center color="secondary" round class="accept-badge">Accepted</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==2" text-center color="light" round class="decline-badge">Tenative</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==3" text-center color="pink" round class="decline-badge">Declined</ion-badge>\n                  <!-- <button [attr.disabled]="currentCountDownState == 2 ? true : null" *ngIf="this.participants[index].empty==false" (click)="clearParticipant(index)" ion-button menuClose clear class="close-btn">\n                    <ion-icon name="close-circle"></ion-icon>\n                  </button> -->\n                </ion-item>\n                <h2 class="accept-head" (click)="showParticipantActionSheet(index)">{{this.participants[index].name}}</h2>\n                <p class="accept-para" (click)="selectRole(index)">{{this.participants[index].role}}</p>\n              </ion-col>\n            </ion-row>\n            <ion-row text-center>\n              <ion-col *ngFor="let index of [2,3];" col-6>\n                <div *ngIf="this.participants[index].empty==false" (click)="showParticipantActionSheet(index)" class="partcipant-avatar" text-center>\n                  <img *ngIf="this.participants[index].empty==false" [src]="this.participants[index].avatar">\n                  <ion-icon *ngIf="this.participants[index].empty==true" name="add" class="add-icon"></ion-icon>\n                  <!-- <ion-badge *ngIf="this.participants[index].status==0" text-center color="light" round class="light-badge">No Response</ion-badge> -->\n                  <ion-badge *ngIf="this.participants[index].status==1" text-center color="secondary" round class="accept-badge">Accepted</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==2" text-center color="light" round class="decline-badge">Tenative</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==3" text-center color="pink" round class="decline-badge">Declined</ion-badge>\n                  <!-- <button [attr.disabled]="currentCountDownState == 2 ? true : null" *ngIf="this.participants[index].empty==false" (click)="clearParticipant(index)" ion-button menuClose clear class="close-btn">\n                    <ion-icon name="close-circle"></ion-icon>\n                  </button> -->\n                </div>\n                <ion-item *ngIf="this.participants[index].empty==true" (click)="showParticipantActionSheet(index)" class="add-user-participant" text-center>\n                  <img *ngIf="this.participants[index].empty==false" [src]="this.participants[index].avatar">\n                  <ion-icon *ngIf="this.participants[index].empty==true" name="add" class="add-icon"></ion-icon>\n                  <!-- <ion-badge *ngIf="this.participants[index].status==0" text-center color="light" round class="light-badge">No Response</ion-badge> -->\n                  <ion-badge *ngIf="this.participants[index].status==1" text-center color="secondary" round class="accept-badge">Accepted</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==2" text-center color="light" round class="decline-badge">Tenative</ion-badge>\n                  <ion-badge *ngIf="this.participants[index].status==3" text-center color="pink" round class="decline-badge">Declined</ion-badge>\n                  <!-- <button [attr.disabled]="currentCountDownState == 2 ? true : null" *ngIf="this.participants[index].empty==false" (click)="clearParticipant(index)" ion-button menuClose clear class="close-btn">\n                    <ion-icon name="close-circle"></ion-icon>\n                  </button> -->\n                </ion-item>\n                <h2 class="accept-head" (click)="showParticipantActionSheet(index)">{{this.participants[index].name}}</h2>\n                <p class="accept-para" (click)="selectRole(index)">{{this.participants[index].role}}</p>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-item>\n      </ion-list>\n\n      <ion-list *ngSwitchCase="2" class="guest-section">\n        <ion-item no-lines>\n          <ion-label>Public event</ion-label>\n          <ion-toggle mode="ios" [attr.disabled]="currentCountDownState == 2 ? true : null" [(ngModel)]="privacy" (ionChange)="privacyUpdate(item)"></ion-toggle>\n        </ion-item>\n        <ion-list text-center class="guest-list" no-lines>\n          <h3>{{guests.length}} Total Guests</h3>\n          <ion-grid>\n            <ion-row text-center>\n              <ion-col col-6><button [attr.disabled]="currentCountDownState == 2 ? true : null" ion-button (click)="importContacts()" color="light" block class="import-btn">Import Contacts</button></ion-col>\n              <ion-col col-6><button [attr.disabled]="currentCountDownState == 2 ? true : null" ion-button block (click)="showGuestActionSheet(true)" class="add-guest-btn">Add Guest</button></ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-list>\n        <ion-list class="white-bg guest-invitation-list" no-lines>\n            <ion-item *ngFor="let guest of guests;" class="border-btm header-bold">\n              <!-- <ion-avatar item-start>\n                <img src="assets/images/user-avatar1.png">\n                <ion-badge text-center color="secondary" round class="guest-accept-badge">Accepted</ion-badge>\n              </ion-avatar> -->\n              <!-- Change to invite attr -->\n              <avatar item-start [user]="guest.uid" [name]="guest.name" [status]="guest.status" [event]="event.details.id"></avatar>\n              <h2>{{guest.name}}</h2>\n              <p *ngIf="guest.identifier">{{guest.identifier}}</p>\n              <!-- <ion-note item-end>\n                9:40 am\n              </ion-note> -->\n            </ion-item>\n            <!-- <ion-item class="border-btm header-bold">\n              <ion-avatar item-start>\n                <img src="assets/images/user-avatar4.png">\n              </ion-avatar>\n              <h2>Mark</h2>\n              <p>770-222-2112</p>\n            </ion-item> -->\n        </ion-list>\n      </ion-list>\n    </div>\n  </div>\n</ion-content>\n<ion-footer *ngIf="selected_tab == 2" class="footer-sticky" text-center>\n  <button (click)="createInvite()" [attr.disabled]="currentCountDownState == 2 ? true : null" ion-button clear large>VIDEO INVITE</button>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/wed-details/wed-details.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["b" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_email_composer__["a" /* EmailComposer */]]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_10__ionic_native_email_composer__["a" /* EmailComposer */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_10__ionic_native_email_composer__["a" /* EmailComposer */]) === "function" && _h || Object])
], WedDetailsPage);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=wed-details.js.map

/***/ }),

/***/ 119:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarriageLicensePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pick_contact_pick_contact__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__add_manual_modal_add_manual_modal__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_email_composer__ = __webpack_require__(190);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MarriageLicensePage = (function () {
    function MarriageLicensePage(navCtrl, navParams, actionSheetCtrl, modalCtrl, httpProvider, loadingCtrl, popoverCtrl, emailComposer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.modalCtrl = modalCtrl;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.popoverCtrl = popoverCtrl;
        this.emailComposer = emailComposer;
        this.submitted = false;
        this.signers = [];
        this.selected_tab = 0;
        //this.refreshLicense();
    }
    MarriageLicensePage.prototype.refreshLicense = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.getMarriageLicense(this.navParams.get('id')).then(function (license) {
            loading.dismiss();
            console.log(license);
            _this.license = license;
            _this.submitted = license.submitted;
            _this.signers = license.signers;
        }, function (err) {
            loading.dismiss();
        });
    };
    MarriageLicensePage.prototype.goHome = function () {
        this.navCtrl.popToRoot();
    };
    MarriageLicensePage.prototype.showParticipantActionSheet = function (index) {
        var _this = this;
        this.showActionSheet(true, false).then(function (users) {
            _this.handleContactsToImportAsParticipant(users['contacts']);
        }, function (err) {
            console.error(err);
        });
    };
    MarriageLicensePage.prototype.list_tabs = function (event) {
        console.log(event);
        this.selected_tab = event;
    };
    MarriageLicensePage.prototype.showActionSheet = function (single, guests) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var self = _this;
            var buttons = [];
            buttons = buttons.concat({
                text: 'Add manually',
                role: 'manually',
                handler: function () {
                    _this.addManually().then(function (contacts) {
                        resolve(contacts);
                    }, function (err) {
                        reject(err);
                    });
                }
            });
            buttons = buttons.concat({
                text: 'Add from Contacts',
                role: 'contact',
                handler: function () {
                    console.log('Contacts clicked');
                    if (single) {
                        _this.pickFromContacts(1).then(function (contacts) {
                            resolve(contacts);
                        }, function (err) {
                            reject(err);
                        });
                    }
                    else {
                        _this.pickFromContacts(-1).then(function (contacts) {
                            resolve(contacts);
                        }, function (err) {
                            reject(err);
                        });
                    }
                }
            });
            _this.actionSheet = _this.actionSheetCtrl.create({
                title: '',
                cssClass: 'action-sheets-basic-page',
                buttons: buttons
            });
            _this.actionSheet.present();
        });
    };
    MarriageLicensePage.prototype.handleContactsToImportAsParticipant = function (contacts) {
        if (contacts.length > 0) {
            var contact = contacts[0];
            var loading = this.createLoading();
            loading.present();
        }
    };
    MarriageLicensePage.prototype.pickFromContacts = function (limit) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var contactModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__pick_contact_pick_contact__["a" /* PickContactPage */], {
                limit: limit
            });
            contactModal.onDidDismiss(function (data) {
                console.log(data);
                if (data) {
                    resolve(data);
                }
            });
            contactModal.present();
        });
    };
    MarriageLicensePage.prototype.addManually = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var manualModal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__add_manual_modal_add_manual_modal__["a" /* AddManualModalPage */]);
            manualModal.onDidDismiss(function (data) {
                console.log(data);
                if (data) {
                    resolve(data);
                }
            });
            manualModal.present();
        });
    };
    MarriageLicensePage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    MarriageLicensePage.prototype.addSigner = function (identifier) {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.addSigner(this.navParams.get('id'), identifier).then(function (license) {
            loading.dismiss();
            console.log(license);
            _this.license = license;
            _this.submitted = license.submitted;
            _this.signers = license.signers;
        }, function (err) {
            loading.dismiss();
        });
    };
    MarriageLicensePage.prototype.removeSigner = function (id) {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.removeSigner(this.navParams.get('id'), id).then(function (license) {
            loading.dismiss();
            console.log(license);
            _this.license = license;
            _this.submitted = license.submitted;
            _this.signers = license.signers;
        }, function (err) {
            loading.dismiss();
        });
    };
    MarriageLicensePage.prototype.submitLicense = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.submitMarriageLicense(this.navParams.get('id')).then(function (license) {
            loading.dismiss();
            console.log(license);
            _this.license = license;
            _this.submitted = license.submitted;
            _this.signers = license.signers;
        }, function (err) {
            loading.dismiss();
        });
    };
    return MarriageLicensePage;
}());
MarriageLicensePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-marriage-license',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/marriage-license/marriage-license.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Marriage License\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="mrg-lcns-content">\n  <!-- <div *ngIf="submitted == false">\n    <ion-list no-lines>\n      <ion-item no-lines text-wrap>\n        <h2>Marriage License</h2>\n        <p>You must complete and submit your marriage application prior to receiving updates for your license.</p>\n      </ion-item>\n    </ion-list>\n  </div> -->\n  <div>\n    <ion-list class="mrg-lcns-container">\n      <ion-item no-lines text-wrap>\n        <h1 color="primary">Marriage License</h1><br/>\n        <p>You must complete all required documentation in your marriage application prior to being permitted to use this feature. Please read the full terms of service before completing.</p>\n      </ion-item>\n      <ion-item class="border-btm header-bold">\n        <h2>Application Submitted</h2>\n        <p>December 30th 2017</p>\n      </ion-item>\n      <ion-item class="border-btm header-bold">\n        <h2>Awaiting Your E-Signatures</h2>\n        <p>December 30th 2017</p>\n      </ion-item>\n      <ion-item class="border-btm header-bold">\n        <h2>Application Received</h2>\n        <p>December 30th 2017</p>\n      </ion-item>\n      <ion-item class="border-btm header-bold">\n        <h2>Application Approved</h2>\n        <p>December 30th 2017</p>\n      </ion-item>\n      <ion-item class="border-btm header-bold">\n        <h2>License Issued</h2>\n        <p>December 30th 2017</p>\n      </ion-item>\n    </ion-list>\n  </div>\n  <!-- <div *ngIf="submitted==false">\n    <ion-list class="mrg-lcns-container" no-lines>\n      <ion-item no-lines text-wrap>\n        <h2>Marriage License</h2>\n        <p>You must complete all required documentation in your marriage application prior to being permitted to use this feature. Please read the full terms of service before completing.</p>\n      </ion-item>\n    </ion-list>\n    <ion-list class="singer-list-container" no-lines>\n      <ion-item no-lines text-wrap>\n        <h2>Signers</h2>\n        <p class="blue-color">Powered by<a class="blue-color" style="font-weight:800; padding-left:5px;">AdobeSign</a></p>\n        <p>Along with yourself you are required to obtain signatures from other officials and witnesses including the court system.</p>\n      </ion-item>\n      <ion-item *ngFor="let signer of signers;" class="border-btm header-bold">\n        <avatar [user]="signer.user" item-start></avatar>\n        <h2>{{signer.name}}</h2>\n        <p>{{signer.identifier}}</p>\n        <ion-note item-end>\n          <button (click)="removeSigner(signer.id)" ion-button menuClose clear class="close-btn">\n            <ion-icon name="close-circle"></ion-icon>\n          </button>\n        </ion-note>\n      </ion-item>\n      <ion-item *ngIf="signers.length>0" (click)="showActionSheet(true,false)" class="border-btm header-bold">\n        <ion-avatar item-start>\n         <ion-item class="add-user-participant" text-center>\n            <input type="file" onClick="event.preventDefault();" fileinput="file" filepreview="filepreview"/>\n            <ion-icon name="add" class="add-icon"></ion-icon>\n          </ion-item>\n        </ion-avatar>\n        <h2>Add a singer</h2>\n        <p>Make a selection</p>\n      </ion-item>\n    </ion-list>\n\n     <ion-list class="review-container" no-lines>\n      <ion-item no-lines text-wrap>\n        <h2>Review and Submit</h2>\n        <p>Once your marriage application has been approved the above parties will be notified to sign via email. Once you submit this form you will not be permitted to make any edits.</p>\n        <button ion-button class="submit-btn" (click)="submitLicense()" color="secondary">Submit New</button>\n      </ion-item>\n     </ion-list>\n  </div>\n  <div *ngIf="submitted==true">\n    <ion-list class="mrg-lcns-container" no-lines>\n      <ion-item no-lines text-wrap>\n        <h2 text-center>Marriage License</h2>\n        <p text-center>Your marriage license has been submitted for signatures. Once the signatures have been collected you will be notified via email or you can return to this screen at any time to get an update.</p><br/>\n        <p class="blue-color">Powered by<a class="blue-color" style="font-weight:800; padding-left:5px;">AdobeSign</a></p>\n      </ion-item>\n      <ion-item *ngFor="let signer of signers;" class="border-btm header-bold">\n        <avatar [user]="signer.user" item-start></avatar>\n        <h2>{{signer.name}}</h2>\n        <p>{{signer.identifier}}</p>\n        <ion-note item-end>\n          <ion-badge color="green">{{signer.response.text}}</ion-badge>\n        </ion-note>\n      </ion-item>\n    </ion-list>\n  </div> -->\n</ion-content>\n<!-- <ion-footer class="footer-sticky" text-center>\n    <button ion-button clear (click)="saveclose();">Save and Close</button>\n </ion-footer> -->\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/marriage-license/marriage-license.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__["b" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_email_composer__["a" /* EmailComposer */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_email_composer__["a" /* EmailComposer */]])
], MarriageLicensePage);

//# sourceMappingURL=marriage-license.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateAnEventPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registry_link_registry_link__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__marriage_license_marriage_license__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_image_picker__ = __webpack_require__(854);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__checkout_details_checkout_details__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__ = __webpack_require__(360);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var CreateAnEventPage = (function () {
    function CreateAnEventPage(navCtrl, loadingCtrl, storageProvider, httpProvider, imagePicker, camera, statusBar, renderer, iab, transfer, file) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storageProvider = storageProvider;
        this.httpProvider = httpProvider;
        this.imagePicker = imagePicker;
        this.camera = camera;
        this.statusBar = statusBar;
        this.renderer = renderer;
        this.iab = iab;
        this.transfer = transfer;
        this.file = file;
        this.today = new Date();
        this.photo = '';
        this.package = 1;
        this.total = 0;
        this.isDisabled = true;
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: 0
        };
        this.changing = false;
        this.test = false;
        // let status bar overlay webview
        this.statusBar.overlaysWebView(false);
        //White text
        this.statusBar.styleLightContent();
        // set status bar to white
        this.statusBar.backgroundColorByHexString('#5aabd3');
        // ) {
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.getEventConfig().then(function (res) {
            console.log(res);
            _this.config = res;
            _this.packages = res.packages;
            _this.addons = res.addons;
            for (var i = 0; i < _this.addons.length; i++) {
                _this.form.addControl('addon' + i.toString(), new __WEBPACK_IMPORTED_MODULE_9__angular_forms__["a" /* FormControl */](false, []));
            }
            _this.calculateTotal();
            loading.dismiss();
        }, function (err) {
            console.error(err);
            loading.dismiss();
        });
        storageProvider.getProfile().then(function (profile) {
            _this.user = profile;
        }, function (err) {
            console.error(err);
        });
        setTimeout(function () {
            this.disabled = false;
        }, 3000);
    }
    CreateAnEventPage.prototype.ngOnInit = function () {
        this.current_iso = this.today.toISOString();
        this.current_date = this.current_iso.substring(0, 10);
        this.current_year = this.current_iso.substring(0, 4);
        var yesterday = new Date(this.today);
        yesterday.setDate(yesterday.getDate() - 1);
        this.yesterday_iso = yesterday.toISOString();
        var tomorrow = new Date(this.today);
        tomorrow.setDate(tomorrow.getDate() + 1);
        this.tomorrow_iso = tomorrow.toISOString();
        this.form = new __WEBPACK_IMPORTED_MODULE_9__angular_forms__["b" /* FormGroup */]({
            title: new __WEBPACK_IMPORTED_MODULE_9__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["g" /* Validators */].required]),
            date: new __WEBPACK_IMPORTED_MODULE_9__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["g" /* Validators */].required]),
            time: new __WEBPACK_IMPORTED_MODULE_9__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_9__angular_forms__["g" /* Validators */].required])
        });
    };
    CreateAnEventPage.prototype.pickImage = function () {
        var _this = this;
        var self = this;
        var image = '';
        var loading = this.createLoading();
        this.camera.getPicture(this.options).then(function (imageData) {
            loading.present();
            var options = {
                fileKey: 'picture'
            };
            var fileTransfer = _this.transfer.create();
            fileTransfer.upload(imageData, 'http://ec2-34-228-21-16.compute-1.amazonaws.com/api/upload/file', options)
                .then(function (response) {
                var data = JSON.parse(response.response)['data'];
                var path = data['path'];
                self.photo = 'http://ec2-34-228-21-16.compute-1.amazonaws.com/storage/' + (path);
                loading.dismiss();
            }, function (err) {
                loading.dismiss();
            });
        });
    };
    CreateAnEventPage.prototype.getBackgroundStyle = function () {
        if (this.photo != '') {
            return { 'background-image': 'url("' + this.photo + '")', 'background-size': 'cover' };
        }
        else {
            return { 'background-color': '#043f7b' };
        }
    };
    CreateAnEventPage.prototype.goToRegistryLink = function (params) {
        if (!params)
            params = {};
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__registry_link_registry_link__["a" /* RegistryLinkPage */]);
    };
    CreateAnEventPage.prototype.gotoRegistry = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__registry_link_registry_link__["a" /* RegistryLinkPage */]);
    };
    CreateAnEventPage.prototype.gotoMarrigeLicense = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__marriage_license_marriage_license__["a" /* MarriageLicensePage */]);
    };
    CreateAnEventPage.prototype.changePackage = function (id) {
        this.package = id;
        this.calculateTotal();
    };
    CreateAnEventPage.prototype.updateAdddonSelection = function (index, event) {
        console.log(index);
        console.log(event);
        // alert(JSON.stringify(this.form.controls['addon0'].value));
        // if (this.changing){
        //   return;
        // }
        //
        // this.changing = true;
        // var self = this;
        // setTimeout(function(){
        //   self.changing = false;
        // }, 5000)
        //
        // if (typeof(this.addons[index].selected) == 'undefined'){
        //   this.addons[index].selected = true;
        // }else{
        //   this.addons[index].selected = !this.addons[index].selected;
        // }
        // this.calculateTotal();
    };
    CreateAnEventPage.prototype.calculateTotal = function () {
        console.log(this.packages);
        console.log(this.package);
        var _package = this.packages[this.package - 1].price;
        var addons = 0;
        for (var i = 0; i < this.addons.length; i++) {
            var addon = this.addons[i];
            //this.addons[i].selected = this.form.controls['addon'+i].value;
            if (addon.selected) {
                addons += addon.price;
            }
        }
        var total = _package + addons;
        this.total = total;
    };
    CreateAnEventPage.prototype.dataURItoBlob = function (dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
        else
            byteString = encodeURI(dataURI.split(',')[1]);
        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ia], { type: mimeString });
    };
    CreateAnEventPage.prototype.submit = function () {
        /*this.openCheckout().then(
          (token) => {*/
        //console.log(token);
        // let loading = this.createLoading();
        // loading.present();
        var eventData = new __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["a" /* EventData */];
        eventData.token = 'test';
        eventData.title = this.form.controls.title.value;
        eventData.date = (new Date(this.form.controls.date.value)).getTime() / 1000;
        eventData.time = (new Date("1/1/2017 " + this.form.controls.time.value)).getTime() / 1000;
        eventData.photo = this.photo;
        eventData.package = {
            id: this.packages[this.package - 1].id
        };
        var _addons = [];
        for (var i = 0; i < this.addons.length; i++) {
            var addon = this.addons[i];
            var formControl = this.form.controls['addon' + i.toString()];
            if (formControl.value == false) {
                continue;
            }
            _addons = _addons.concat(addon);
        }
        eventData.addons = _addons;
        var file = null;
        if (this.photo != '') {
            file = this.dataURItoBlob(this.photo);
        }
        else {
            file = null;
        }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__checkout_details_checkout_details__["a" /* CheckoutDetailsPage */], {
            eventData: eventData,
            package: this.packages[this.package - 1],
            file: file,
            user: this.user
        });
        // this.httpProvider.createEvent(eventData, file).then(
        //   (res) => {
        //     console.log(res);
        //     this.navCtrl.pop();
        //     console.log(res);
        //     loading.dismiss();
        //   },
        //   (err) => {
        //     console.error(err);
        //     loading.dismiss();
        //   }
        // );
        /*},
        (err) => {
  
        }
      );*/
    };
    CreateAnEventPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    CreateAnEventPage.prototype.openCheckout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var handler = window.StripeCheckout.configure({
                key: 'pk_test_OtZkFKLIU1WBuonz6xbk6UQB',
                locale: 'en',
                token: function (token) {
                    resolve(token);
                }
            });
            handler.open((_a = {
                    name: 'Web Wed Mobile',
                    description: 'Event Package Purchase',
                    amount: _this.total * 100,
                    currency: 'usd',
                    email: _this.user.personal.email,
                    image: 'assets/icon/favicon.ico'
                },
                _a['allow-remember-me'] = true,
                _a));
            _this.globalListener = _this.renderer.listenGlobal('window', 'popstate', function () {
                handler.close();
            });
            var _a;
        });
    };
    CreateAnEventPage.prototype.termsOfService = function () {
        var browser = this.iab.create('http://www.webwedmobile.com/legal');
    };
    CreateAnEventPage.prototype.selectCheckBox = function (index) {
        // alert('Selected index: ' + index);
    };
    return CreateAnEventPage;
}());
CreateAnEventPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-an-event',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/create-an-event/create-an-event.html"*/'<ion-header>\n  <ion-navbar class="sub-toolbar">\n    <ion-title>\n      Create an Event\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content [formGroup]="form">\n   <ion-item no-lines class="create-event-banner" (click)="pickImage()" [ngStyle]=\'getBackgroundStyle()\' text-center>\n        <ion-item class="add-photo-content" text-center>\n          <!-- <input type="file" fileinput="file" filepreview="filepreview"/> -->\n          <ion-icon name="ios-camera-outline" class="camera-icon"></ion-icon>\n          <h2 class="photo-text">Add Photo</h2>\n        </ion-item>\n    </ion-item>\n    <ion-list class="event-details-container">\n      <ion-item text-left no-lines class="event-details-text">\n        <h2 class="photo-text" text-left>EVENT DETAILS</h2>\n      </ion-item>\n      <ion-item class="wed-event-title" padding>\n        <!-- <ion-grid> -->\n          <!-- <ion-row ion-item> -->\n            <!-- <ion-col col-6 text-left> -->\n              <ion-label>\n                <h2 class="e-details-title e-head">Title</h2>\n              </ion-label>\n            <!-- </ion-col> -->\n            <!-- <ion-col col-6 text-right> -->\n              <ion-input formControlName="title" text-right type="text" class="e-wed-title e-head" placeholder="e.g. Jane & John\'s Wedding" clearInput></ion-input>\n            <!-- </ion-col> -->\n          <!-- </ion-row> -->\n        <!-- </ion-grid> -->\n      </ion-item>\n\n      <ion-item class="wed-event-title">\n          <ion-label>Event Date</ion-label>\n          <ion-datetime formControlName="date" ion-displayFormat="MM/DD" max="2099-01-01" placeholder="Date"></ion-datetime>\n        <!-- </ion-item> -->\n         <!-- <ion-grid>\n          <ion-row>\n            <ion-col col-4 text-left>\n              <h2 class="e-details-title e-head">When</h2>\n            </ion-col>\n            <ion-col col-8>\n                <h2><a>Specfic time and date <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown"></ion-icon></a></h2>\n                <ion-note><ion-icon name="ios-calendar-outline" mini></ion-icon> July 10th 2017</ion-note>\n                <ion-note><ion-icon name="ios-time-outline" mini></ion-icon> 4:30 PM</ion-note>\n\n            </ion-col>\n          </ion-row>\n        </ion-grid> -->\n      </ion-item>\n      <ion-item class="wed-event-title">\n          <ion-label>Time</ion-label>\n          <ion-datetime formControlName="time" ion-displayFormat="hh:mm A" pickerFormat="hh:mm A" placeholder="Time"></ion-datetime>\n        <!-- </ion-item> -->\n         <!-- <ion-grid>\n          <ion-row>\n            <ion-col col-4 text-left>\n              <h2 class="e-details-title e-head">When</h2>\n            </ion-col>\n            <ion-col col-8>\n                <h2><a>Specfic time and date <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown"></ion-icon></a></h2>\n                <ion-note><ion-icon name="ios-calendar-outline" mini></ion-icon> July 10th 2017</ion-note>\n                <ion-note><ion-icon name="ios-time-outline" mini></ion-icon> 4:30 PM</ion-note>\n\n            </ion-col>\n          </ion-row>\n        </ion-grid> -->\n      </ion-item>\n       <ion-item class="wed-event-title" no-lines>\n         <ion-grid>\n          <ion-row>\n            <ion-col col-12 text-left>\n              <h2 class="e-details-title e-head">Live Stream Time</h2>\n            </ion-col>\n          </ion-row>\n           <ion-row text-center class="grp-col">\n            <ion-col *ngFor="let _package of packages;" (click)="changePackage(_package.id)" [ngClass]="{\'blue-bg\': (_package.id == package)}" col-3>\n              <h3 class="">{{_package.title}}</h3>\n              <h2 class="">${{_package.price}}</h2>\n            </ion-col>\n            <!-- <ion-col col-3 class="blue-bg">\n              <h3 class="">30 Minutes</h3>\n              <h2 class="">$50</h2>\n            </ion-col>\n             <ion-col col-3>\n             <h3 class="">1 Hours</h3>\n              <h2 class="">$60</h2>\n            </ion-col>\n            <ion-col col-2>\n              <h3 class="">2 Hours</h3>\n              <h2 class="">$65</h2>\n            </ion-col> -->\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n      <ion-item text-left no-lines class="event-details-text">\n        <h2 class="photo-text" text-left>Premium Add-ons</h2>\n      </ion-item>\n      <ion-list class="premium-feature-list">\n        <ion-item *ngFor="let addon of addons; let i = index;">\n          <ion-checkbox color="primary" item-start formControlName="addon{{i}}" clear></ion-checkbox>\n          <ion-label item-right>{{addon.title}}</ion-label>\n          <ion-label item-end text-right>{{addon.price==0 ? \'Free\' : \'$\' + addon.price}}</ion-label>\n        </ion-item>\n      </ion-list>\n      <ion-list class="premium-feature-list">\n         <ion-item no-lines text-center text-wrap>\n           <p>By continuing I have read and understand the <button ion-button clear small (click)="termsOfService()" class="terms-btn">Terms of Service</button>.</p>\n         </ion-item>\n      </ion-list>\n    </ion-list>\n</ion-content>\n <ion-footer class="footer-sticky" text-center>\n    <button ion-button [attr.disabled]="form.valid ? null : true" clear (click)="submit();">Continue</button>\n </ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/create-an-event/create-an-event.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["b" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_image_picker__["a" /* ImagePicker */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__["a" /* InAppBrowser */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["b" /* StorageProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_image_picker__["a" /* ImagePicker */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"],
        __WEBPACK_IMPORTED_MODULE_10__ionic_native_in_app_browser__["a" /* InAppBrowser */],
        __WEBPACK_IMPORTED_MODULE_12__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_13__ionic_native_file__["a" /* File */]])
], CreateAnEventPage);

//# sourceMappingURL=create-an-event.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BroadcastPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_event_completed_event_completed__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_pick_emoji_pick_emoji__ = __webpack_require__(483);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BroadcastPage = (function () {
    function BroadcastPage(navCtrl, navParams, httpProvider, modalCtrl, viewCtrl, platform, toastCtrl, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        //OT:any;
        //OT:
        this.guest = true;
        this.sessionId = '';
        this.token = '';
        this.role = 'participant1';
        this.apiKey = "45692092";
        this.isZoomed = false;
        this.loadProgress = 0;
        this.State = {
            ACTIVE: 1,
            DISABLE: 2
        };
        this.currentState = 2;
        this.max = 100;
        this.currentCountDownState = 2;
        this.maxCountDown = 100;
        this.countdownRemaining = 100;
        this.countdownColor = '#d6d8db';
        this.countdownBackgroundURL = 'assets/images/heart_active.png';
        this.showCountDown = true;
        this.countDownPaused = false;
        this.showProgress = true;
        this.remainingMinutes = 0;
        this.totalMinutes = 0;
        this.admin = 0;
        this.messages = [];
        this.chatIndex = 0;
        this.hostCapabilties = false;
        this.subscribers = [];
        this.totalViewers = 0;
        this.hlsLink = '';
        this.getSubscriberElement = function (subscriber) {
            return "layoutContainer";
        };
        this.getPublisherElement = function () {
            return "publisherContainer";
        };
        this.connectionCreated = function (event) {
            console.log('Connection Created');
            this.totalViewers += 1;
        };
        this.connectionDestroyed = function (event) {
            console.log('Connection Destroyed');
            this.eventCompleted(event);
        };
        this.streamCreated = function (event) {
            window.cordova.plugins.iosrtc.refreshVideos();
            console.log('streamCreated');
            var div = this.getSubscriberElement(event);
            var subscriber = this.session.subscribe(event.stream, div, { subscribeToAudio: true, insertMode: 'append', width: '100%', height: '100%' });
            this.subscribers.push(subscriber);
            this.updateLayout();
        };
        this.streamDestroyed = function (event) {
            console.log('streamDestroyed');
        };
        this.eventCompleted = function (event) {
            console.log('eventCompleted');
            if (!this.guest) {
                clearInterval(this.refreshCountdownIntervalId);
                //this.session.unpublish(this.publisher);
                this.session.disconnect();
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_event_completed_event_completed__["a" /* EventCompletedPage */], {
                    total: this.totalViewers
                });
            }
            else {
                //this.viewCtrl.dismiss();
                this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_event_completed_event_completed__["a" /* EventCompletedPage */], {
                    total: this.totalViewers
                });
            }
            // this.navCtrl.pop();
        };
        this.eventPaused = function (event) {
        };
        this.eventPlayed = function (event) {
        };
        this.receivedChatMessage = function (event) {
            if (this.guest == false) {
                return;
            }
            console.log('receivedChatMessage');
            if (event.data.chat) {
                //if (!(event.from.connectionId === this.session.connection.connectionId)){
                this.messageT(event.data.message, event.data.avatar);
                //}
            }
        };
        this.receivedEmojiMessage = function (event) {
            if (this.guest == false) {
                return;
            }
            console.log('receivedChatMessage');
            if (event.data.image) {
                //if (!(event.from.connectionId === this.session.connection.connectionId)){
                this.messageEmoji(event.data.image, event.data.avatar);
                //}
            }
        };
        this.refreshCountdownIntervalId = null;
        this.isFrontCamera = true;
        this.isMuted = false;
        setInterval(function () {
            _this.messages.forEach(function (message) {
                if (__WEBPACK_IMPORTED_MODULE_2_moment__().valueOf() - __WEBPACK_IMPORTED_MODULE_2_moment__(message.created).valueOf() > 3000) {
                    message.visible = false;
                }
            });
        }, 100);
        this.guest = this.navParams.get('guest');
        this.event = this.navParams.get('event');
        this.user = this.navParams.get('user');
        this.guest = true;
        this.hlsLink = '';
        httpProvider.getEventSpecifications(this.event).then(function (res) {
            console.log(res);
            _this.remainingMinutes = res['remaining_time'];
            _this.totalMinutes = res['total_time'];
            _this.permissions = res['permissions'];
            _this.admin = res['active_admin'];
            if (!_this.guest) {
                _this.countdownRemaining = (-(_this.remainingMinutes / _this.totalMinutes) * 100) + 100;
                _this.currentCountDownState = _this.State.ACTIVE;
                console.log(_this.countdownRemaining);
                _this.doCountDown(); //start heart button
            }
            else {
                // httpProvider.getBroadcastInformation(this.event).then(
                //   (res) => {
                //     if (res){
                //       this.hlsLink = res;
                //     }
                //   },
                //   (err) => {
                //     console.error(err);
                //   }
                // );
            }
            httpProvider.getEventSessionDetails(_this.event, _this.user).then(function (res) {
                console.log(res);
                _this.sessionId = res.opentok.session_id;
                // this.sessionId =
                _this.token = res.opentok.token;
                _this.role = res.opentok.role;
                _this.numberOfParticipants = res.numberOfParticipants;
                // if (!this.guest){
                //   console.log(this.platform.platforms());
                //   if (platform.is('ios')){
                //     (<any>window).cordova.plugins.iosrtc.registerGlobals();
                //     // cordova.plugins.iosrtc.selectAudioOutput('speaker');
                //     (<any>window).getUserMedia = navigator.getUserMedia.bind(navigator);
                //     (<any>window).cordova.plugins.iosrtc.debug.enable('iosrtc*');
                //     (<any>window).OT = (<any>window).cordova.require('cordova-plugin-opentokjs.OpenTokClient');
                //     this.initializeSession();
                //   }else{
                _this.initializeSession();
                // (<any>window).cordova.exec(null, null, 'OpentokActivator', 'sampleBackgroundView', ['46014622', res.opentok.session_id, res.opentok.token, false]);
                // }
                // }else{
                //   this.initializeSession();
                // }
            }, function (err) {
                console.error(err);
            });
        }, function (err) {
            console.error(err);
        });
    }
    BroadcastPage.prototype.showLoader = function () {
        var _this = this;
        this.showProgress = true;
        this.loadProgress = 0;
        if (this.currentState == this.State.ACTIVE) {
            var refreshIntervalId = setInterval(function () {
                if (_this.loadProgress < _this.max) {
                    _this.loadProgress++;
                }
                else {
                    _this.showProgress = false;
                    clearInterval(refreshIntervalId);
                }
            }, 1000);
        }
    };
    BroadcastPage.prototype.sendMessage = function () {
        var self = this;
        if (!self.message)
            return;
        self.session.signal({
            type: 'msg',
            data: {
                chat: true,
                message: self.message,
                avatar: this.httpProvider.apiUrl + "/user/" + 1 + "/avatar"
            }
        }, function (error) {
            if (error) {
                console.log('Error sending signal:', error.name, error.message);
            }
            else {
                //self.messageT(self.message,self.httpProvider.apiUrl+'/user/1/avatar');
                self.message = '';
            }
        });
    };
    BroadcastPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BroadcastPage');
    };
    BroadcastPage.prototype.ngAfterContentInit = function () {
    };
    BroadcastPage.prototype.messageT = function (text, avatar) {
        var old = window.$('.message');
        var msg = window.$('<div>');
        msg.addClass('message');
        msg.html('<div class="clearfix"><div class="avatar"></div><div class="info"></div></div>');
        msg.find('.avatar').html('<img src="' + avatar + '" />');
        msg.find('.info').html('<div class="text">' + text + '</div>');
        msg.appendTo('body');
        msg.css({ "left": "-" + (msg.width() + 20) + "px" });
        msg.animate({ "left": 0 }, 200);
        setTimeout(function () {
            msg.fadeOut(400, function () {
                msg.remove();
            });
        }, 7000);
        old.each(function (ind) {
            window.$(this).animate({
                "bottom": parseInt(window.$(this).css("bottom")) + msg.height() + 15 + "px",
                "opacity": window.$(this).css("opacity") - 0.20
            }, 200, 'swing', function () {
                if (window.$(this).css("opacity") == 0)
                    window.$(this).remove();
            });
        });
    };
    BroadcastPage.prototype.messageEmoji = function (image, avatar) {
        var old = window.$('.message');
        var msg = window.$('<div>');
        msg.addClass('message');
        msg.html('<div class="clearfix"><div class="avatar"></div><div class="info"></div></div>');
        msg.find('.avatar').html('<img src="' + avatar + '" />');
        msg.find('.info').html('<div class="text"><img src="' + image + '" style="width:75px;height:75px;"/></div>');
        msg.appendTo('body');
        msg.css({ "left": "-" + (msg.width() + 20) + "px" });
        msg.animate({ "left": 0 }, 200);
        setTimeout(function () {
            msg.fadeOut(400, function () {
                msg.remove();
            });
        }, 7000);
        old.each(function (ind) {
            window.$(this).animate({
                "bottom": parseInt(window.$(this).css("bottom")) + msg.height() + 15 + "px",
                "opacity": window.$(this).css("opacity") - 0.20
            }, 200, 'swing', function () {
                if (window.$(this).css("opacity") == 0)
                    window.$(this).remove();
            });
        });
    };
    BroadcastPage.prototype.handleError = function (error) {
        if (error) {
            alert(error.message);
        }
    };
    BroadcastPage.prototype.updateLayout = function () {
        this.layout();
        // if (this.platform.is('ios')){
        //   (<any>window).cordova.plugins.iosrtc.refreshVideos();
        // }
    };
    BroadcastPage.prototype.publishStream = function (camera) {
        var audioInputDevices;
        var videoInputDevices;
        var self = this;
        if (self.publisher) {
            self.session.unpublish(self.publisher);
        }
        window.OT.getDevices(function (error, devices) {
            audioInputDevices = devices.filter(function (element) {
                return element.kind == "audioInput";
            });
            videoInputDevices = devices.filter(function (element) {
                return element.kind == "videoInput";
            });
            for (var i = 0; i < audioInputDevices.length; i++) {
                console.log("audio input device: ", audioInputDevices[i].deviceId);
            }
            for (i = 0; i < videoInputDevices.length; i++) {
                console.log("video input device: ", JSON.stringify(videoInputDevices[i]));
            }
            var videoDeviceId = videoInputDevices[0].deviceId;
            if (camera != null) {
                if (camera == 'front') {
                    if (videoInputDevices.length == 1) {
                        var videoDeviceId_1 = videoInputDevices[0].deviceId;
                    }
                    else {
                        var videoDeviceId_2 = videoInputDevices[1].deviceId;
                    }
                }
                else {
                    var videoDeviceId_3 = videoInputDevices[0].deviceId;
                }
            }
            else {
                var videoDeviceId_4 = videoInputDevices[0].deviceId;
            }
            var pubOptions = {
                audioSource: audioInputDevices[0].deviceId,
                videoSource: videoDeviceId
            };
            //self.publisher = (<any>window).OT.initPublisher(self.getPublisherElement(), {insertMode: 'append', width: '100%', height: '100%'}, (<any>window).handleError);
            self.publisher = window.OT.initPublisher(self.getPublisherElement(), pubOptions);
            self.updateLayout();
            window.cordova.plugins.iosrtc.refreshVideos();
            window.testing = self.publisher;
        });
    };
    BroadcastPage.prototype.initializeSession = function () {
        /*var layoutContainer = document.getElementById("layoutContainer");
    
        // Initialize the layout container and get a reference to the layout method
        this.layout = (<any>window).initLayoutContainer(layoutContainer, {
          maxRatio: 3/2,     // The narrowest ratio that will be used (default 2x3)
          minRatio: 9/16,      // The widest ratio that will be used (default 16x9)
          fixedRatio: false,  // If this is true then the aspect ratio of the video is maintained and minRatio and maxRatio are ignored (default false)
          bigClass: "OT_big", // The class to add to elements that should be sized bigger
          bigPercentage: 0.8,  // The maximum percentage of space the big ones should take up
          bigFixedRatio: false, // fixedRatio for the big ones
          bigMaxRatio: 3/2,     // The narrowest ratio to use for the big elements (default 2x3)
          bigMinRatio: 9/16,     // The widest ratio to use for the big elements (default 16x9)
          bigFirst: true        // Whether to place the big one in the top left (true) or bottom right
        }).layout;
    
        (<any>window).layout = this.layout;*/
        window.cordova.exec(null, null, 'OpentokActivator', 'sampleBackgroundView', ['45692092', this.sessionId, this.token, false]);
        window.handleError = function (error) {
            if (error) {
                console.error(error.message);
            }
        };
        var session = window.OT.initSession(this.apiKey, this.sessionId);
        var self = this;
        if (session.capabilities.forceDisconnect == 1) {
            self.hostCapabilties = true;
        }
        else {
            self.hostCapabilties = false;
        }
        this.session = session;
        this.session.on('connectionCreated', function (event) { self.connectionCreated(event); });
        this.session.on('connectionDestroyed', function (event) { self.connectionDestroyed(event); });
        //this.session.on('streamCreated', function(event){ self.streamCreated(event); });
        //this.session.on('streamDestroyed', function(event){ self.streamDestroyed(event); });
        this.session.on('signal:eventCompleted', function (event) { self.eventCompleted(event); });
        this.session.on('eventCompleted', function (event) { self.eventCompleted(event); });
        this.session.on('signal:msg', function (event) { self.receivedChatMessage(event); });
        this.session.on('signal:emoji', function (event) { self.receivedEmojiMessage(event); });
        this.session.on('signal:publish', function (event) { self.eventPlayed(event); });
        this.session.on('signal:unpublish', function (event) { self.eventPaused(event); });
        /*if (!self.guest) {
          self.publishStream('front');
        }*/
        this.session.connect(this.token, function (error) {
            if (error) {
                console.log(error);
                window.handleError(error);
            }
            else {
                if (!self.guest) {
                    /*self.showCountdownToast();
                    setTimeout(function(){
                      self.session.publish(self.publisher, (<any>window).handleError);
                      self.updateLayout();
                      self.httpProvider.activateEvent(self.event).then(
                        (res) => {
                          console.log(res);
                        },
                        (err) => {
                          console.error(err);
                        }
                      );
                    }, 10000);*/
                }
            }
        });
    };
    BroadcastPage.prototype.startCountDown = function () {
        // if (this.currentCountDownState == this.State.ACTIVE){
        this.showCountDown = true;
        // this.countdownRemaining = this.maxCountDown;
        this.countDownPaused = false;
        this.doCountDown();
        // }
    };
    BroadcastPage.prototype.pauseCountDown = function () {
        this.showCountDown = true;
        this.countDownPaused = true;
    };
    BroadcastPage.prototype.resumeCountDown = function () {
        this.showCountDown = true;
        this.countDownPaused = false;
        this.doCountDown();
    };
    BroadcastPage.prototype.countdown = function () {
        if (this.countdownRemaining > 0) {
            console.log(this.countdownRemaining);
            this.countdownRemaining--;
            if (this.countdownRemaining < 0.25 * this.maxCountDown) {
                // red color
                this.countdownColor = '#ce4a30';
                this.countdownBackgroundURL = "assets/images/heart_broadcast.png";
                this.showCountdownWarningToast();
            }
            else if (this.countdownRemaining < 0.5 * this.maxCountDown) {
                // amber color
                this.countdownColor = '#f9ac00';
                this.countdownBackgroundURL = "assets/images/heart_active.png";
                this.showCountdownWarningToast();
            }
            else {
                // normal color
                this.countdownColor = '#00bbd3';
                this.countdownBackgroundURL = "assets/images/heart_disable.png";
            }
            if (this.countDownPaused == true) {
                clearInterval(this.refreshCountdownIntervalId);
            }
        }
        else {
            if (this.session && this.currentCountDownState == this.State.ACTIVE) {
                // this.session.signal({
                //     type: 'eventCompleted',
                //     data: { }
                //   }, function(error) {
                // });
                // this.showCountDown = false;
                clearInterval(this.refreshCountdownIntervalId);
            }
        }
    };
    BroadcastPage.prototype.doCountDown = function () {
        // if(this.currentCountDownState == this.State.ACTIVE){
        var self = this;
        self.refreshCountdownIntervalId = setInterval(function () {
            self.countdown();
        }, ((60000 * this.totalMinutes) / 100));
        self.countdown();
        // }
    };
    BroadcastPage.prototype.isCreator = function () {
        if (this.role == 'participant1') {
            return true;
        }
        else {
            return false;
        }
    };
    BroadcastPage.prototype.showCountdownToast = function () {
        var toast = this.toastCtrl.create({
            message: 'You will go live in 10 seconds.',
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        //toast.present();
    };
    BroadcastPage.prototype.showCountdownWarningToast = function () {
        var toast = this.toastCtrl.create({
            message: 'You have 5 minutes or less remaining!',
            duration: 3000,
            position: 'bottom'
        });
        toast.onDidDismiss(function () {
            console.log('Dismissed toast');
        });
        //toast.present();
    };
    BroadcastPage.prototype.countdownClicked = function () {
        if (!this.isCreator()) {
            var self = this;
            var alert = this.alertCtrl.create({
                title: 'Leave event',
                message: 'Do you want to leave this event?',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Yes',
                        handler: function () {
                            self.session.unpublish(self.publisher);
                            self.session.disconnect();
                            self.viewCtrl.dismiss();
                        }
                    }
                ]
            });
            alert.present();
        }
        else {
            var self = this;
            var alert = this.alertCtrl.create({
                title: 'Terminate event',
                message: 'Do you want to terminate this event? You will forfeit any remaining time.',
                buttons: [
                    {
                        text: 'No',
                        role: 'cancel',
                        handler: function () {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Yes',
                        handler: function () {
                            self.session.signal({
                                type: 'eventCompleted',
                                data: {}
                            }, function (error) {
                            });
                            // this.showCountDown = false;
                            clearInterval(self.refreshCountdownIntervalId);
                        }
                    }
                ]
            });
            alert.present();
            // if(this.countdownRemaining == this.maxCountDown) {
            //   // not started yet, please start
            //   this.doCountDown();
            //   this.session.signal({
            //       type: 'publish',
            //       data: { }
            //     }, function(error) {
            //   });
            //   return;
            // } else if (this.countDownPaused == false) {
            //   this.pauseCountDown();
            //   //this.session.unpublish(this.publisher);
            //   this.httpProvider.pauseStream(this.event, 4).then(
            //     (res) => {
            //       this.session.signal({
            //           type: 'unpublish',
            //           data: { }
            //         }, function(error) {
            //       });
            //       console.log(res);
            //     },
            //     (err) => {
            //       console.error(err);
            //     }
            //   );
            //   return;
            // } else {
            //   this.resumeCountDown();
            //   return;
            // }
        }
    };
    BroadcastPage.prototype.resize = function () {
        if (this.isZoomed) {
            //videostream.addClass('bg-vid-zoom');
            this.isZoomed = false;
        }
        else {
            //videostream.removeClass('bg-vid-zoom');
            this.isZoomed = false;
        }
    };
    BroadcastPage.prototype.flipCamera = function () {
        if (this.isFrontCamera) {
            this.publishStream('back');
            this.isFrontCamera = false;
        }
        else {
            this.publishStream('front');
            this.isFrontCamera = true;
        }
    };
    BroadcastPage.prototype.mute = function () {
        if (this.isMuted) {
            this.publisher.publishAudio(!this.isMuted);
            this.isMuted = false;
        }
        else {
            this.publisher.publishAudio(!this.isMuted);
            this.isMuted = true;
        }
    };
    BroadcastPage.prototype.close = function () {
        if (this.session) {
            this.session.disconnect();
        }
        window.cordova.exec(null, null, 'OpentokActivator', 'disconnect', []);
        //this.navCtrl.setRoot(DashboardPage);
    };
    BroadcastPage.prototype.ionViewWillLeave = function () {
        //this.session.disconnect();
    };
    BroadcastPage.prototype.pickEmoji = function () {
        var self = this;
        var emojiModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_pick_emoji_pick_emoji__["a" /* PickEmojiPage */]);
        emojiModal.onDidDismiss(function (data) {
            console.log(data);
            if (data) {
                self.session.signal({
                    type: 'emoji',
                    data: {
                        image: data.image,
                        avatar: self.httpProvider.apiUrl + "/user/" + 1 + "/avatar"
                    }
                }, function (error) {
                });
            }
        });
        emojiModal.present();
    };
    return BroadcastPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])('input'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_core__["ElementRef"]) === "function" && _a || Object)
], BroadcastPage.prototype, "input", void 0);
BroadcastPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
        selector: 'page-broadcast',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/broadcast/broadcast.html"*/'<!--\n  Generated template for the BroadcastPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content style="background:transparent;">\n  <!-- <div id="user1" class="videostream streamlarge" [ngClass]="((numberOfParticipants==1) ? \'fullstream\' : \'multiplestream\')"></div>\n    <div id="user2" *ngIf="numberOfParticipants >= 2" class="videostream streamlarge"></div> -->\n  <!--The four users below are serpated according to their role. So not all of these will be used and at most 5. Most likely drop 5 for officant -->\n  <!-- <div id="user3" *ngIf="numberOfParticipants >= 3" class="videostream streamMedium"></div>\n    <div id="user4" *ngIf="numberOfParticipants >= 4" class="videostream streamMedium"></div>\n    <div id="user5" *ngIf="numberOfParticipants >= 5" class="videostream streamMedium"></div>\n    <div id="streamOfficial"  class="videostream streamOfficial"></div> -->\n  <!-- <div id="layoutContainer">\n        <div id="publisherContainer"></div>\n    </div> -->\n  <!-- <video id="videostream" class="bg-vid isOn" [src]="hlsLink" autoplay="true" preload="auto" webkit-playsinline controls="false" [ngClass]="{active: isOn, disabled: bg-vid-zoom}" (click)="toggle(!isOn)"></video> -->\n  <div class="headerBtns" end>\n\n    <button ion-button class="mini-btn" icon-only (click)="close()">\n      <ion-icon name="close"></ion-icon>\n    </button>\n  </div>\n  <ion-grid class="bottom">\n      <ion-row text-center>\n        <ion-col col-4>\n          <ion-item no-lines text-center class="margin-item">\n            <button (click)="flipCamera()" class="mini-btn disabled">\n              <ion-icon name="refresh" class="share-icon"></ion-icon>\n            </button>\n          </ion-item>\n        </ion-col>\n        <ion-col col-4 text-center>\n          <broadcast-count-down\n            [progress]="countdownRemaining"\n            [state]="currentCountDownState"\n            [width]="\'100\'"\n            [track-color]="countdownColor"\n            [placeholder-color]="\'#d6d8db\'"\n            [max]="maxCountDown"\n            [backgroundUrl]="countdownBackgroundURL"\n            (click)="countdownClicked()"\n            ></broadcast-count-down>\n        </ion-col>\n        <ion-col col-4>\n          <ion-item no-lines text-center class="margin-item">\n            <button (click)="mute()" class="mini-btn disabled">\n              <ion-icon name="volume-off" class="share-icon"></ion-icon>\n            </button>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  <!-- <div class="message-input" ion-fixed>\n    <ion-item no-lines mode="ios">\n      <ion-label (click)="sendMessage()" tappable>\n        <ion-icon name="ios-chatbubbles"></ion-icon>\n      </ion-label>\n      <ion-input (keyup.enter)="sendMessage()" placeholder="Share a group message" [(ngModel)]="message" #input></ion-input>\n    </ion-item>\n    <ion-label class="emojiLabel" (click)="pickEmoji()" item-end tappable>\n      <ion-icon color="light" name="images"></ion-icon>\n    </ion-label>\n  </div>\n  <div class="background-overlay"></div> -->\n  <div id="brand">\n    <img src="assets/images/WebWed-Logo.svg">\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/broadcast/broadcast.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["k" /* NavController */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* NavParams */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* ModalController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* ModalController */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["q" /* ViewController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["q" /* ViewController */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* Platform */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["m" /* Platform */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* ToastController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["p" /* ToastController */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["b" /* AlertController */]) === "function" && _j || Object])
], BroadcastPage);

var _a, _b, _c, _d, _e, _f, _g, _h, _j;
//# sourceMappingURL=broadcast.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationsPage = (function () {
    function NotificationsPage(navCtrl, loadingCtrl, storageProvider, httpProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.storageProvider = storageProvider;
        this.httpProvider = httpProvider;
        var loading = this.createLoading();
        loading.present();
        storageProvider.getProfile().then(function (profile) {
            var user_id = profile.personal.id;
            _this.httpProvider.getNotifications(user_id).then(function (res) {
                _this.notifications = res;
                loading.dismiss();
            }, function (err) {
                console.error(err);
                loading.dismiss();
            });
        }, function (err) {
            console.error(err);
            loading.dismiss();
        });
    }
    NotificationsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.storageProvider.getProfile().then(function (profile) {
            var user_id = profile.personal.id;
            _this.httpProvider.getNotifications(user_id).then(function (res) {
                _this.notifications = res;
                refresher.complete();
            }, function (err) {
                console.error(err);
                refresher.complete();
            });
        }, function (err) {
            console.error(err);
            refresher.complete();
        });
    };
    NotificationsPage.prototype.dismiss = function (notification) {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.storageProvider.getProfile().then(function (profile) {
            var user_id = profile.personal.id;
            _this.httpProvider.clearNotification(user_id, notification.id).then(function (res) {
                _this.notifications = res;
                loading.dismiss();
            }, function (err) {
                console.error(err);
            });
        });
    };
    NotificationsPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return NotificationsPage;
}());
NotificationsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-notifications',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/notifications/notifications.html"*/'<ion-header>\n  <main-navigation title="Notifications"></main-navigation>\n</ion-header>\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-list id="notifications-list2">\n    <!-- <ion-item-sliding *ngFor="let notification of notifications;"> -->\n      <ion-item *ngFor="let notification of notifications;" color="balanced" id="notifications-list-item12">\n        <ion-avatar item-left>\n          <img [src]="notification.avatar"/>\n        </ion-avatar>\n        <h2>\n          {{notification.title}}\n        </h2>\n        <p style="white-space:normal;">{{notification.message}}</p>\n      </ion-item>\n      <!-- <ion-item-options side="right">\n        <button (click)="dismiss(notification)" ion-button color="assertive">\n          Dismiss\n        </button>\n      </ion-item-options> -->\n    <!-- </ion-item-sliding> -->\n    <!-- <ion-item-sliding>\n      <ion-item color="balanced" id="notifications-list-item12">\n        <ion-avatar item-left>\n          <img src="assets/images/user-avatar1.png"/>\n        </ion-avatar>\n        <h2>\n          Johnny Appleseed Replied\n        </h2>\n        <p>Accepted your invite to Our Wedding</p>\n      </ion-item>\n      <ion-item-options side="left">\n        <button ion-button color="assertive">\n          Delete\n        </button>\n      </ion-item-options>\n    </ion-item-sliding> -->\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/notifications/notifications.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__["b" /* StorageProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__["b" /* StorageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]])
], NotificationsPage);

//# sourceMappingURL=notifications.js.map

/***/ }),

/***/ 16:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_fileUpload_service__ = __webpack_require__(593);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/*
  Generated class for the HttpProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
var HttpProvider = (function () {
    function HttpProvider(http, storage, fbAuth, fbDb, alertCtrl) {
        var _this = this;
        this.http = http;
        this.storage = storage;
        this.fbAuth = fbAuth;
        this.fbDb = fbDb;
        this.alertCtrl = alertCtrl;
        // public apiUrl: string = 'http://www.riveloper.com/client_portal/webwed-admin/public/api';
        this.apiUrl = 'https://webwedmobile.net/api';
        // public apiUrl: string = 'http://localhost:8888/api';
        this.token = 'invalid_token';
        this.storageProvider = new __WEBPACK_IMPORTED_MODULE_3__storage_storage__["b" /* StorageProvider */](http, storage);
        this.storageProvider.getToken().then(function (result) {
            _this.token = atob(result);
            _this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'X-Requested-Auth': _this.token
            });
            _this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: _this.headers });
        }, function (err) {
            console.error(err);
            _this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                'X-Requested-Auth': _this.token
            });
            _this.options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: _this.headers });
        });
    }
    HttpProvider.prototype.refreshToken = function () {
        return new Promise(function (resolve, reject) {
        });
    };
    HttpProvider.prototype.getRequest = function (url) {
        console.log('url: ' + url);
        return this.http.get(this.apiUrl + url, this.options).map(function (res) { return res.json(); });
    };
    HttpProvider.prototype.postRequest = function (url, data) {
        console.log('url: ' + url);
        return this.http.post(this.apiUrl + url, data, this.options).map(function (res) { return res.json(); });
    };
    HttpProvider.prototype.handleError = function (data) {
        if (data['error']) {
            var alert_1 = this.alertCtrl.create({
                title: data['error']['title'],
                subTitle: data['error']['message'],
                buttons: ['Dismiss']
            });
            alert_1.present();
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Unknown Error',
                subTitle: 'An unknown error occurred, this could be a network error.',
                buttons: ['Dismiss']
            });
            alert_2.present();
            console.log('handlingError: ' + JSON.stringify(data));
        }
        console.log('alertCtrl');
    };
    HttpProvider.prototype.uploadRequest = function (url, data, file) {
        this.uploadService = new __WEBPACK_IMPORTED_MODULE_7__services_fileUpload_service__["a" /* UploadService */]();
        var files = [];
        if (file == null) {
            files = [];
        }
        else {
            files = [file];
        }
        this.uploadService.makeFileRequest(this.apiUrl + url, data, files, this.token).then(function (res) {
            //console.log(res);
        }, function (err) {
            console.error(err);
        });
    };
    HttpProvider.prototype.getEventDetailsById = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/event/' + id + '/details').subscribe(function (res) {
                console.log(res);
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('getEventDetailsById completed');
            });
        });
    };
    HttpProvider.prototype.getEventConfig = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/event/config').subscribe(function (res) {
                //console.log(res);
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('getUserOrders completed');
            });
        });
    };
    HttpProvider.prototype.createEvent = function (params) {
        var _this = this;
        // return this.getEventDetailsById(1);
        return new Promise(function (resolve, reject) {
            /*this.uploadRequest('/event/create', params, photo);/*.then(
              (res) => {
                //console.log(res);
              },
              (err) => {
                console.error(err);
              }
            )*/
            _this.postRequest('/event/create', params).subscribe(function (res) {
                //console.log(res);
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('createEvent completed');
            });
        });
    };
    ;
    HttpProvider.prototype.getUserEvents = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/user/' + id.toString() + '/events').subscribe(function (res) {
                console.log(res);
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('getUserEvents completed');
            });
        });
    };
    HttpProvider.prototype.getUserData = function (id) {
        return this.getRequest('/user/' + id);
    };
    HttpProvider.prototype.signIn = function (accessToken) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/auth/login?id_token=' + accessToken).subscribe(function (res) {
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                resolve(res['data']);
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getUserOrders = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/user/' + id + '/orders').subscribe(function (res) {
                console.log(res);
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                resolve(res['data']['orders']);
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('getUserOrders completed');
            });
        });
    };
    //@TODO add error handling to all requests.
    HttpProvider.prototype.getNotifications = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/user/' + id + '/notifications').subscribe(function (res) {
                //console.log(res);
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                resolve(res['notifications']);
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('getNotifications completed');
            });
        });
    };
    HttpProvider.prototype.clearNotification = function (user_id, notification_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/user/' + user_id + '/notification/' + notification_id + '/clear').subscribe(function (res) {
                //console.log(res);
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                resolve(res['notifications']);
            }, function (err) {
                console.error(err);
                _this.handleError(err);
                reject(err);
            }, function () {
                //console.log('getNotifications completed');
            });
        });
    };
    HttpProvider.prototype.requestSubmitUserData = function (accessToken, data) {
        return this.postRequest('/auth/login?id_token=' + accessToken, data);
    };
    HttpProvider.prototype.isPublic = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/public/' + eventId.toString()).subscribe(function (res) {
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                if (res['data']['public']) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.updatePublic = function (eventId, privacy) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/public/' + eventId.toString() + '/' + (privacy ? 1 : 0)).subscribe(function (res) {
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                if (res['data']['public']) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.addParticipant = function (eventId, user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/event/' + eventId + '/participants/add', { user: user }).subscribe(function (res) {
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                // //console.log(res);
                // return;
                if (res['success']) {
                    if (res['success']) {
                        resolve(res['data']);
                    }
                    else {
                        _this.handleError(res);
                        reject(res);
                    }
                }
                else {
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.updateParticipant = function (eventId, participantId, roleId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/event/' + eventId + '/participants/' + participantId + '/update', {
                role: roleId
            }).subscribe(function (res) {
                if (!res['success']) {
                    _this.handleError(res);
                    reject(res);
                }
                if (res['success']) {
                    if (res['success']) {
                        resolve(res['data']);
                    }
                    else {
                        _this.handleError(res);
                        reject(res);
                    }
                }
                else {
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.removeParticipant = function (eventId, participantId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/event/' + eventId + '/participants/' + participantId + '/remove').subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.updateGuestList = function (eventId, users) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/event/' + eventId + '/guests/update', { contacts: users }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getOfficiants = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/officiants').subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']['officiants']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.respondToInvite = function (response, eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/event/' + eventId.toString() + '/invite/' + response.toString()).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']['events']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.sendInvite = function (eventId, subject, message, video) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/event/' + eventId.toString() + '/send_invite', {
                subject: subject,
                message: message,
                video: video
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(true);
                }
                else {
                    _this.handleError(res);
                    reject(false);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getEventSessionDetails = function (eventId, userId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/start/' + eventId + '/' + userId).subscribe(function (res) {
                console.log(res);
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.activateEvent = function (eventId) {
        return new Promise(function (resolve, reject) {
            resolve();
            // this.fbDb.object(`/events/${eventId}`).set({
            //   activate: true
            // }).then(() => {
            //   resolve();
            // }, reject);
        });
    };
    HttpProvider.prototype.deactivateEvent = function (eventId) {
        return new Promise(function (resolve, reject) {
            resolve();
            // this.fbDb.object(`/events/${eventId}`).set({
            //   activate: false
            // }).then(() => {
            //   resolve();
            // }, reject);
        });
    };
    HttpProvider.prototype.isActive = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _data = _this.fbDb.object("/events/" + eventId).subscribe(function (data) {
                _data.unsubscribe();
                console.log(data);
                if (data['activate']) {
                    resolve(true);
                }
                else {
                    resolve(false);
                    // resolve(false);
                }
            }, reject);
        });
    };
    HttpProvider.prototype.getBroadcastInformation = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _data = _this.fbDb.object("/events/" + eventId).subscribe(function (data) {
                _data.unsubscribe();
                if (data['activate']) {
                    resolve(data['broadcastUrl']);
                }
                else {
                    resolve(false);
                }
            }, reject);
        });
    };
    HttpProvider.prototype.updateAccount = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/myaccount/update', data).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']['user']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.inviteOfficiant = function (eventId, officiantId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/officiant/invite', {
                event: eventId,
                officiant: officiantId
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(true);
                }
                else {
                    _this.handleError(res);
                    reject(false);
                }
            }, function (err) {
                _this.handleError(err);
                reject(false);
            });
        });
    };
    HttpProvider.prototype.updateAvatar = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/avatar/update', {
                image: data
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(true);
                }
                else {
                    _this.handleError(res);
                    reject(false);
                }
            }, function (err) {
                _this.handleError(err);
                reject(false);
            });
        });
    };
    HttpProvider.prototype.getArchive = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/archive/' + eventId.toString()).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['video']);
                }
                else {
                    _this.handleError(res);
                    reject(false);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getEventSpecifications = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/stream/' + eventId.toString()).subscribe(function (res) {
                // resolve(res['minutes']);
                console.log(res);
            }, function (err) {
                // this.handleError(err); reject(err);
            });
            var _data = _this.fbDb.object("/events/" + eventId).subscribe(function (data) {
                _data.unsubscribe();
                resolve(data);
            }, reject);
        });
    };
    HttpProvider.prototype.pauseStream = function (eventId, userId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.deactivateEvent(eventId).then(function (res) {
                _this.getRequest('/stop_stream/' + eventId.toString() + '/' + userId.toString()).subscribe(function (res) {
                    resolve(true);
                }, function (err) {
                    _this.handleError(err);
                    reject(err);
                });
            }, function (err) {
            });
        });
    };
    HttpProvider.prototype.getEmojis = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/emojis').subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']['images']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getPaymentInfo = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getRequest('/myaccount/payment_info').subscribe(function (res) {
                if (res['success']) {
                    resolve(res['card']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getMarriageApplication = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/application', {
                id: eventId
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.addAttachment = function (eventId, file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/application/attachment/add', {
                id: eventId,
                file: file
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.removeAttachment = function (eventId, file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/application/attachment/remove', {
                id: eventId,
                file: file
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.submitMarriageApplication = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/application/submit', {
                id: eventId
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.getMarriageLicense = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/license', {
                id: eventId
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.addSigner = function (eventId, signer) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/license/signer/add', {
                id: eventId,
                signer: signer
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.removeSigner = function (eventId, signer) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/license/signer/remove', {
                id: eventId,
                signer: signer
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    HttpProvider.prototype.submitMarriageLicense = function (eventId) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.postRequest('/marriage/license/submit', {
                id: eventId
            }).subscribe(function (res) {
                if (res['success']) {
                    resolve(res['data']);
                }
                else {
                    _this.handleError(res);
                    reject(res);
                }
            }, function (err) {
                _this.handleError(err);
                reject(err);
            });
        });
    };
    return HttpProvider;
}());
HttpProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_5_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_8_ionic_angular__["b" /* AlertController */]])
], HttpProvider);

//# sourceMappingURL=http.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ionic_native_facebook__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_google_plus__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase_app__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var AuthProvider = (function () {
    function AuthProvider(fbAuth, fbDb, platform, fb, googlePlus, http, storage, alertCtrl) {
        this.fbAuth = fbAuth;
        this.fbDb = fbDb;
        this.platform = platform;
        this.fb = fb;
        this.googlePlus = googlePlus;
        this.http = http;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.storageProvider = new __WEBPACK_IMPORTED_MODULE_10__providers_storage_storage__["b" /* StorageProvider */](http, storage);
        this.httpProvider = new __WEBPACK_IMPORTED_MODULE_9__http_http__["a" /* HttpProvider */](http, storage, fbAuth, fbDb, alertCtrl);
    }
    /**
     * Sign in with facebook
     */
    AuthProvider.prototype.signInWithFacebook = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                return _this.fb.login(['email', 'public_profile']).then(function (res) {
                    var facebookCredential = __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].FacebookAuthProvider.credential(res.authResponse.accessToken);
                    return __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"]().signInWithCredential(facebookCredential).then(function (res) {
                        resolve(res);
                    }, reject);
                }, reject);
            }
            else {
                return _this.fbAuth.auth
                    .signInWithPopup(new __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].FacebookAuthProvider())
                    .then(function (res) {
                    resolve(res.user);
                }, reject);
            }
        });
    };
    /**
     * Sign In with Google
     */
    AuthProvider.prototype.signInWithGoogle = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.platform.is('cordova')) {
                _this.googlePlus.login({
                    webClientId: '81627619045-811d7vrpjc2rru27diiqhalrv1h1h7jg.apps.googleusercontent.com',
                    offline: true
                }).then(function (res) {
                    var googleCredential = __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].GoogleAuthProvider.credential(res.idToken);
                    return __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"]().signInWithCredential(googleCredential).then(function (res) {
                        resolve(res);
                    }, reject);
                }, reject);
            }
            else {
                return _this.fbAuth.auth
                    .signInWithPopup(new __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].GoogleAuthProvider())
                    .then(function (res) {
                    resolve(res.user);
                }, reject);
            }
        });
    };
    /**
     * Sign in with Email and Password
     * @param email Email Address
     * @param pass Password
     */
    AuthProvider.prototype.signInWithEmail = function (email, pass) {
        var credential = __WEBPACK_IMPORTED_MODULE_8_firebase_app__["auth"].EmailAuthProvider.credential(email, pass);
        return this.fbAuth.auth.signInWithCredential(credential);
    };
    /**
     * Sign up with Email and Password
     * @param email Email Address
     * @param pass Password
     */
    AuthProvider.prototype.signUpWithEmail = function (email, pass) {
        return this.fbAuth.auth.createUserWithEmailAndPassword(email, pass);
    };
    /**
     * Get User Data
     * @param accessToken Firebase oAuth Access Token
     */
    AuthProvider.prototype.getUserData = function (accessToken) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.httpProvider.signIn(accessToken).then(function (result) {
                _this.storageProvider.setToken(result['token']).then(function () {
                    resolve(result['user']);
                });
            }, function (err) {
                reject(err);
            });
        });
    };
    /**
     * Set User Data
     * @param uid Firebase User ID
     * @param userData User Data
     */
    AuthProvider.prototype.setUserData = function (accessToken, userData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var _data = _this.httpProvider.requestSubmitUserData(accessToken, userData).subscribe(function (result) {
                _data.unsubscribe();
                _this.storageProvider.setToken(result['data']['token']).then(function () {
                    resolve(result['data']['user']);
                });
            }, function (err) {
                reject(err['message']);
            }, function () {
                console.log('setUserData completed');
            });
        });
    };
    AuthProvider.prototype.linkAccount = function () {
    };
    AuthProvider.prototype.forgotPassword = function (email) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.fbAuth.auth.sendPasswordResetEmail(email).then(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    return AuthProvider;
}());
AuthProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_0__ionic_native_facebook__["a" /* Facebook */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_native_google_plus__["a" /* GooglePlus */],
        __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
], AuthProvider);

// WEBPACK FOOTER //
// ./src/providers/auth/auth.ts
//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_contacts__ = __webpack_require__(853);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PickContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PickContactPage = (function () {
    function PickContactPage(viewCtrl, contacts, navCtrl, navParams) {
        var _this = this;
        this.viewCtrl = viewCtrl;
        this.contacts = contacts;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
        this.headers = {
            'a': [],
            'b': [],
            'c': [],
            'd': [],
            'e': [],
            'f': [],
            'g': [],
            'h': [],
            'i': [],
            'j': [],
            'k': [],
            'l': [],
            'm': [],
            'n': [],
            'o': [],
            'p': [],
            'q': [],
            'r': [],
            's': [],
            't': [],
            'u': [],
            'v': [],
            'w': [],
            'x': [],
            'y': [],
            'z': []
        };
        this.selectedContacts = {};
        this.allContacts = {};
        this.limit = -1;
        this.getContactData = function (contact) {
            return {
                name: contact.name.formatted,
                email: (contact.emails != null ? (contact.emails.length > 0 ? contact.emails[0].value : null) : null),
                phone: (contact.phoneNumbers != null ? (contact.phoneNumbers.length > 0 ? contact.phoneNumbers[0].value : null) : null)
            };
        };
        this.totalSelected = 0;
        if (this.navParams.get('limit')) {
            if (this.navParams.get('limit') == 1) {
                this.limit = 1;
            }
            else {
                this.limit = -1;
            }
        }
        this.contacts.find(['displayName', 'name']).then(function (contacts) {
            _this.contactsList = contacts;
            console.log(_this.contactsList[0]);
            for (var i = 0; i < _this.contactsList.length; i++) {
                var fullName = _this.contactsList[i].name.formatted;
                var firstLetter = fullName.toLowerCase().charAt(0);
                //Check if phone or email is present
                if (_this.contactsList[i].emails || _this.contactsList[i].phoneNumbers) {
                    _this.allContacts['contact' + _this.contactsList[i].id] = false;
                    _this.headers[firstLetter] = _this.headers[firstLetter].concat(_this.contactsList[i]);
                }
            }
            console.log(_this.allContacts);
            console.log(_this.headers);
        }, function (error) {
            console.log(error);
        });
    }
    PickContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PickContactPage');
    };
    PickContactPage.prototype.hasContacts = function (letter) {
        if (this.headers[letter].length > 0) {
            return true;
        }
        else {
            return false;
        }
    };
    PickContactPage.prototype.dismiss = function () {
        var contacts = [];
        if (this.contactsList) {
            for (var i = 0; i < this.contactsList.length; i++) {
                var contact = this.contactsList[i];
                if (this.selectedContacts['contact' + contact.id]) {
                    contacts = contacts.concat(this.getContactData(contact));
                }
            }
        }
        this.viewCtrl.dismiss({ 'contacts': contacts });
    };
    PickContactPage.prototype.selectContact = function (id) {
        if (!this.selectedContacts['contact' + id]) {
            this.totalSelected += 1;
        }
        else {
            this.totalSelected -= 1;
        }
        this.selectedContacts['contact' + id] = !this.selectedContacts['contact' + id];
        if (this.totalSelected == this.limit) {
            this.dismiss();
        }
    };
    return PickContactPage;
}());
PickContactPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-pick-contact',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pick-contact/pick-contact.html"*/'<!--\n  Generated template for the PickContactPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Import Contacts</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="done-all" color="light"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-item-group *ngFor="let letter of letters;">\n    <ion-item-divider color="light" *ngIf="hasContacts(letter)">{{letter.toUpperCase()}}</ion-item-divider>\n    <ion-item *ngFor="let contact of headers[letter];" [ngClass]="selectedContacts[\'contact\'+contact.id] ? \'selected\' : \'not-selected\'" (click)="selectContact(contact.id)">\n        <!-- <ion-checkbox [(ngModel)]="test" checked="false" [ngModelOptions]="{standalone: true}" color="primary"></ion-checkbox> -->\n        <ion-label>{{contact.name.formatted}}</ion-label>\n    </ion-item>\n  </ion-item-group>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pick-contact/pick-contact.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__ionic_native_contacts__["a" /* Contacts */], __WEBPACK_IMPORTED_MODULE_3__providers_storage_storage__["b" /* StorageProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_contacts__["a" /* Contacts */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], PickContactPage);

//# sourceMappingURL=pick-contact.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddManualModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddManualModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AddManualModalPage = (function () {
    function AddManualModalPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
    }
    AddManualModalPage.prototype.ngOnInit = function () {
        this.form = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormGroup */]({
            name: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required]),
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', []),
            phone: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormControl */]('', [])
        });
    };
    AddManualModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddManualModalPage');
    };
    AddManualModalPage.prototype.getContactData = function () {
        return {
            name: this.form.controls.name.value,
            email: this.form.controls.email.value,
            phone: this.form.controls.phone.value
        };
    };
    AddManualModalPage.prototype.blankDismiss = function () {
        this.viewCtrl.dismiss({
            'contacts': []
        });
    };
    AddManualModalPage.prototype.dismiss = function () {
        if (this.form.controls.email.value == '' && this.form.controls.phone.value == '') {
            //alert('Enter an email or phone number to continue.');
        }
        else {
            this.viewCtrl.dismiss({ 'contacts': [
                    this.getContactData()
                ] });
        }
    };
    return AddManualModalPage;
}());
AddManualModalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-manual-modal',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/add-manual-modal/add-manual-modal.html"*/'<!--\n  Generated template for the AddManualModalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Add Manually</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="blankDismiss()">\n        <ion-icon name="close" color="light"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content [formGroup]="form">\n  <ion-list>\n\n    <ion-item>\n      <ion-label fixed>Name *</ion-label>\n      <ion-input text-right formControlName="name" type="text" value=""></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label fixed>Email</ion-label>\n      <ion-input text-right formControlName="email" type="email"></ion-input>\n    </ion-item>\n\n    <ion-item style="display:none;">\n      <ion-label fixed>Phone</ion-label>\n      <ion-input text-right formControlName="phone" type="phone"></ion-input>\n    </ion-item>\n\n  </ion-list>\n</ion-content>\n<ion-footer class="footer-sticky" text-center>\n   <button full block color="light-blue" ion-button large (click)="dismiss();" [attr.disabled]="form.valid ? null : true">Save and Continue</button>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/add-manual-modal/add-manual-modal.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
], AddManualModalPage);

//# sourceMappingURL=add-manual-modal.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistryLinkPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__marriage_license_marriage_license__ = __webpack_require__(119);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegistryLinkPage = (function () {
    function RegistryLinkPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    RegistryLinkPage.prototype.gotoMarriageLicense = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__marriage_license_marriage_license__["a" /* MarriageLicensePage */]);
    };
    RegistryLinkPage.prototype.save = function () {
        this.navCtrl.pop();
    };
    return RegistryLinkPage;
}());
RegistryLinkPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-registry-link',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/registry-link/registry-link.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Registry Link\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content class="registry-content">\n  <ion-list class="registry-container" no-lines>\n    <ion-item no-lines text-wrap>\n      <h2>Registry Link</h2>\n      <p>Enter a direct link for guests to access your online registry. You can copy and paste this from your web browser or enter manually.</p>\n    </ion-item>\n     <ion-item no-lines>\n        <ion-input type="text" class="copyboard-input" value="http://google.com"></ion-input>\n     </ion-item>\n  </ion-list>\n</ion-content>\n <ion-footer class="footer-sticky" text-center>\n    <button ion-button clear (click)="save();">Save and Continue</button>\n </ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/registry-link/registry-link.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
], RegistryLinkPage);

//# sourceMappingURL=registry-link.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttendEventModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_broadcast_broadcast__ = __webpack_require__(122);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AttendEventModalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AttendEventModalPage = (function () {
    function AttendEventModalPage(navCtrl, viewCtrl, navParams, renderer, httpProvider, storageProvider, alertCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.renderer = renderer;
        this.httpProvider = httpProvider;
        this.storageProvider = storageProvider;
        this.alertCtrl = alertCtrl;
        this.loadProgress = 0;
        this.State = {
            ACTIVE: 1,
            DISABLE: 2
        };
        this.currentState = 2;
        this.max = 100;
        this.currentCountDownState = 2;
        this.maxCountDown = 100;
        this.countdownRemaining = 100;
        this.countdownColor = '#d6d8db';
        this.countdownBackgroundURL = '../../assets/images/heart_active.png';
        this.showCountDown = true;
        this.countDownPaused = false;
        this.showProgress = true;
        this.remainingMinutes = 0;
        this.totalMinutes = 0;
        this.admin = 0;
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'small-modal-popup', true);
        this.eventId = this.navParams.get('event');
        // let loading = this.createLoading();
        // loading.present();
    }
    AttendEventModalPage.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.httpProvider.getEventDetailsById(this.eventId).then(function (res) {
            _this.event = res;
            // loading.dismiss();
        }, function (err) {
            console.error(err);
        });
    };
    AttendEventModalPage.prototype.hasPermissions = function () {
        return false;
    };
    AttendEventModalPage.prototype.start = function () {
        // if (!this.hasPermissions()){
        //   let alert = this.alertCtrl.create({
        //     title: 'Request permission',
        //     message: 'Do you want to request permission to control this event?',
        //     buttons: [
        //       {
        //         text: 'No',
        //         role: 'cancel',
        //         handler: () => {
        //           console.log('Cancel clicked');
        //         }
        //       },
        //       {
        //         text: 'Yes',
        //         handler: () => {
        //
        //         }
        //       }
        //     ]
        //   });
        //   alert.present();
        // }else{
        // this.navCtrl.push(BroadcastPage,{
        //   guest: false,
        //   event: this.eventId,
        //   user: this.navParams.get('user')
        // });
        // }
        var participants = this.event.participants;
        var isParticipant = false;
        for (var i = 0; i < participants.length; i++) {
            if (participants[i].id == this.navParams.get('user')) {
                isParticipant = true;
            }
        }
        if (isParticipant) {
            this.httpProvider.getEventSessionDetails(this.eventId, this.navParams.get('user')).then(function (res) {
                console.log(res);
                //this.sessionId = res.opentok.session_id;
                // this.sessionId =
                //this.token = res.opentok.token;
                window.cordova.exec(null, null, 'OpentokActivator', 'activateNow', ['46014622', res.opentok.session_id, res.opentok.token]);
            }, function (err) {
                console.error(err);
            });
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_broadcast_broadcast__["a" /* BroadcastPage */], {
                guest: true,
                event: this.eventId,
                user: this.navParams.get('user')
            });
        }
    };
    return AttendEventModalPage;
}());
AttendEventModalPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-attend-event-modal',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/attend-event-modal/attend-event-modal.html"*/'<!--\n  Generated template for the AttendEventModalPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-content padding>\n  <ion-card class="wed-details-card">\n    <div class="card-overlay"></div>\n    <img [src]="event.details.photo" *ngIf="event"/>\n    <div class="card-title" *ngIf="event">{{event.details.title}}</div>\n    <div class="card-subtitle" *ngIf="event">{{event.details.date*1000 | date:"MMM dd yyyy \'at\' hh:mm a"}}</div>\n    <div class="card-bottom-subtitle">Event ID #{{eventId}}</div>\n  </ion-card>\n  <ion-list no-lines class="white-bg">\n    <ion-grid>\n      <!-- <ion-row text-center>\n        <p>This event is not available to watch yet. Sit tight and the stream will start automatically.</p>\n      </ion-row> -->\n      <ion-row text-center>\n        <ion-col col-6>\n          <share [event]="eventId"></share>\n        </ion-col>\n        <!--<ion-col col-4 text-center>\n      <broadcast-count-down\n            [progress]="countdownRemaining"\n            [state]="currentCountDownState"\n            [width]="\'100\'"\n            [track-color]="countdownColor"\n            [placeholder-color]="\'#d6d8db\'"\n            [max]="maxCountDown"\n            [backgroundUrl]="countdownBackgroundURL"\n            (click)="start()"\n            ></broadcast-count-down>\n        </ion-col>-->\n        <ion-col col-6>\n          <!-- <options [event]="eventId"></options> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/attend-event-modal/attend-event-modal.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_2__providers_storage_storage__["b" /* StorageProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"],
        __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_storage_storage__["b" /* StorageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
], AttendEventModalPage);

//# sourceMappingURL=attend-event-modal.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventCompletedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EventCompletedPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EventCompletedPage = (function () {
    function EventCompletedPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.total = '0';
        this.total = this.navParams.get('total').toString();
    }
    EventCompletedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventCompletedPage');
    };
    EventCompletedPage.prototype.close = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__dashboard_dashboard__["a" /* DashboardPage */]);
    };
    return EventCompletedPage;
}());
EventCompletedPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-event-completed',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/event-completed/event-completed.html"*/'<ion-content class="main-bg">\n  <div class="logo" ion-fixed>\n    <img src="assets/images/WebWed-Logo.svg" style="max-width:150px;padding:20px" alt="logo">\n  </div>\n\n  <div class="congratulations-msg">\n    <h1>Congratulations!</h1>\n    <p>Appy Ever After.</p>\n    <p>Your video is being prepared and will be available in a moment for viewing.</p>\n    <br/>\n    <!-- <p>{{total}} Viewers watched you live</p> -->\n  </div>\n</ion-content>\n\n<ion-footer class="footer-sticky" text-center>\n  <button (click)="close()" full block color="light-blue" ion-button large>Close</button>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/event-completed/event-completed.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], EventCompletedPage);

//# sourceMappingURL=event-completed.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TourProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TourProvider = (function () {
    function TourProvider() {
        this.steps = [];
        this.introJs_ = window.introJs();
    }
    TourProvider.prototype.startTour = function () {
        // console.log('startingTour');
        // this.steps = [
        //   { element: document.querySelectorAll('.join-event-banner-title')[0], intro: 'example' },
        //   { element: document.querySelectorAll('.toolbar')[1], intro: 'example' }
        // ];
        // if ((<any>window).isTouring){
        //   return;
        // }else{
        //   var self = this;
        //   (<any>window).isTouring = true;
        //   // setTimeout(function(){
        //   for (var i = 0; i < self.steps.length; i++){
        //     if (self.steps[i].element==null){
        //       continue;
        //     }else{
        //       self.introJs_.addStep(self.steps[i]);
        //     }
        //   }
        //   self.introJs_.start();
        //   // }, 3000);
        // }
    };
    return TourProvider;
}());
TourProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], TourProvider);

//# sourceMappingURL=tour.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_auth_auth__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__autocomplete_autocomplete__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__login_login__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var RegisterPage = (function () {
    function RegisterPage(navCtrl, navParams, loadingCtrl, auth, afAuth, alertCtrl, modalCtrl, storageProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.auth = auth;
        this.afAuth = afAuth;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.storageProvider = storageProvider;
        this.fullHeight = true;
        this.length = 6;
        this.address = "";
        this.rawAddress = {};
    }
    RegisterPage.prototype.ngOnInit = function () {
        this.user = this.navParams.get('user');
        this.slides.lockSwipes(true);
        var email = this.getEmail();
        this.form = new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]({ value: email, disabled: !!email }, [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]),
            password: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].minLength(6)]),
            full_name: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */](this.getName(), [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(255)]),
            address: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(255)]),
            birthday: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required,]),
            phone: new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)]),
        });
    };
    RegisterPage.prototype.getEmail = function () {
        return this.user ? this.user.email : '';
    };
    RegisterPage.prototype.getName = function () {
        return this.user ? this.user.displayName : '';
    };
    RegisterPage.prototype.getFullName = function () {
        if (this.form.controls.full_name.valid) {
            if (this.form.controls.full_name.value.indexOf(' ') > -1) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    RegisterPage.prototype.canGoNext = function () {
        switch (this.slides.getActiveIndex()) {
            case 0:
                return (this.getEmail() ? true : this.form.controls.email.valid) && this.form.controls.password.valid;
            case 1:
                return this.getFullName();
            case 2:
                return this.form.controls.address.valid;
            case 3:
                return this.form.controls.birthday.valid;
            case 4:
                return this.form.controls.phone.valid;
        }
        return true;
    };
    RegisterPage.prototype.continue = function () {
        var _this = this;
        if (!this.slides.isEnd()) {
            if (this.slides.getActiveIndex() === 5) {
                var loading_1 = this.loadingCtrl.create({
                    content: 'Please wait ...'
                });
                loading_1.present();
                this.submit().then(function () {
                    loading_1.dismiss();
                    _this.slideNext();
                }, function (e) {
                    console.log(e);
                    loading_1.dismiss();
                });
            }
            else if (this.slides.getActiveIndex() === 0) {
                if (this.user) {
                    this.slideNext();
                    return;
                }
                var loading_2 = this.loadingCtrl.create({
                    content: 'Please wait ...'
                });
                loading_2.present();
                this.afAuth.auth.fetchProvidersForEmail(this.form.value.email)
                    .then(function (providers) {
                    loading_2.dismiss();
                    console.log(providers);
                    if (providers.length > 0) {
                        _this.showAlert('User with this email already registered');
                    }
                    else {
                        _this.slideNext();
                    }
                }, function () {
                    loading_2.dismiss();
                    _this.showAlert('Can\'t validate email');
                });
            }
            else {
                this.slideNext();
            }
        }
    };
    RegisterPage.prototype.back = function () {
        if (!this.slides.isBeginning()) {
            this.slidePrev();
        }
    };
    RegisterPage.prototype.showAlert = function (msg) {
        var alert = this.alertCtrl.create({
            message: msg,
            buttons: [{
                    text: 'Ok',
                    role: 'close'
                }]
        });
        alert.present();
    };
    RegisterPage.prototype.slideNext = function () {
        this.slides.lockSwipes(false);
        this.slides.slideNext();
        this.slides.lockSwipes(true);
    };
    RegisterPage.prototype.slidePrev = function () {
        this.slides.lockSwipes(false);
        this.slides.slidePrev();
        this.slides.lockSwipes(true);
    };
    RegisterPage.prototype.backToLogin = function () {
        this.navCtrl.canGoBack() ? this.navCtrl.pop() : this.navCtrl.setRoot('login');
    };
    RegisterPage.prototype.close = function () {
        var _this = this;
        this.afAuth.auth.signOut().then(function () {
            console.log('Signed Out');
            _this.storageProvider.clearAll();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__login_login__["a" /* LoginPage */]);
        });
    };
    RegisterPage.prototype.getUserData = function () {
        return {
            dob: this.form.value.birthday,
            email: this.form.value.email,
            name: this.form.value.full_name,
            location: this.rawAddress,
            phone: this.form.value.phone,
        };
    };
    RegisterPage.prototype.submit = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.user) {
                var emailCredentials = __WEBPACK_IMPORTED_MODULE_5_firebase_app__["auth"].EmailAuthProvider.credential(_this.user.email, _this.form.value.password);
                _this.user.linkWithCredential(emailCredentials).then(function (user) {
                    user.getIdToken(true).then(function (accessToken) {
                        _this.auth.setUserData(accessToken, _this.getUserData()).then(function () {
                            resolve();
                        }, reject);
                    });
                }, reject);
                return;
            }
            _this.auth.signUpWithEmail(_this.form.value.email, _this.form.value.password)
                .then(function (user) {
                user.getIdToken(true).then(function (accessToken) {
                    _this.auth.setUserData(accessToken, _this.getUserData()).then(function () {
                        resolve();
                    }, reject);
                });
            }, reject);
        });
    };
    RegisterPage.prototype.showAddressModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__autocomplete_autocomplete__["a" /* AutocompletePage */]);
        var me = this;
        modal.onDidDismiss(function (data) {
            _this.address = data.description;
            console.log(data);
            _this.rawAddress = data;
        });
        modal.present();
    };
    return RegisterPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["o" /* Slides */])
], RegisterPage.prototype, "slides", void 0);
RegisterPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["f" /* IonicPage */])({
        name: 'register'
    }),
    Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'page-register',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/register/register.html"*/'<ion-content class="main-bg" [formGroup]="form">\n  <div class="logo" ion-fixed>\n    <img src="assets/images/WebWed-Logo.svg" style="max-width:150px;padding:20px" alt="logo">\n  </div>\n  <div class="pager" ion-fixed *ngIf="slides.getActiveIndex() !== 6">{{ slides.getActiveIndex() + 1 }} of {{ length }}</div>\n  <div class="top-btn top-btn-left" tappable ion-fixed *ngIf="slides.isBeginning() && !user" (click)="backToLogin()">\n    <ion-icon name="md-arrow-back"></ion-icon>\n  </div>\n  <div class="top-btn top-btn-right" tappable ion-fixed *ngIf="slides.isEnd()" (click)="close()">\n    <ion-icon name="md-close"></ion-icon>\n  </div>\n  <div class="buttons" ion-fixed>\n    <button\n      ion-button\n      full\n      (click)="back()"\n      mode="ios"\n      *ngIf="!slides.isBeginning() && !slides.isEnd()"\n      color="light-blue">Back</button>\n    <button\n      ion-button full\n      (click)="continue()"\n      *ngIf="!slides.isEnd()"\n      mode="ios"\n      [attr.disabled]="canGoNext() ? null : true"\n      color="primary">\n        <span *ngIf="slides.getActiveIndex() !== 5; else submit">Continue</span>\n        <ng-template #submit>Submit</ng-template>\n    </button>\n    <button\n      ion-button\n      full\n      mode="ios"\n      (click)="close()"\n      *ngIf="slides.isEnd()"\n      color="primary">Close</button>\n  </div>\n  <ion-slides #slides direction="vertical" [ngClass]="{\'fullHeight\': fullHeight}" pager>\n    <ion-slide>\n      <ion-item no-lines [ngClass]="\'ww-input-item\'" mode="ios">\n        <ion-label stacked>Email</ion-label>\n        <ion-input formControlName="email" placeholder="e.g. test@email.me" mode="ios"></ion-input>\n      </ion-item>\n      <ion-item no-lines [ngClass]="\'ww-input-item\'" mode="ios">\n        <ion-label stacked>Password</ion-label>\n        <ion-input formControlName="password" placeholder="Password" type="password" mode="ios"></ion-input>\n      </ion-item>\n    </ion-slide>\n    <ion-slide>\n      <ion-item no-lines [ngClass]="\'ww-input-item\'" mode="ios">\n        <ion-label stacked>Enter your full name</ion-label>\n        <ion-input formControlName="full_name" placeholder="e.g. John Doe" mode="ios"></ion-input>\n      </ion-item>\n    </ion-slide>\n    <ion-slide>\n      <ion-item no-lines [ngClass]="\'ww-input-item\'" mode="ios">\n        <ion-label stacked>What address shoud we use</ion-label>\n        <ion-input (click)="showAddressModal()" [(ngModel)]="address" formControlName="address" placeholder="e.g. 1234 Main Street, Atlanta GA 12345" mode="ios"></ion-input>\n      </ion-item>\n    </ion-slide>\n    <ion-slide>\n      <ion-item no-lines [ngClass]="\'ww-input-item\'" mode="ios">\n        <ion-label stacked>When where you born?</ion-label>\n        <ion-datetime formControlName="birthday" mode="ios" placeholder="Click to select"></ion-datetime>\n      </ion-item>\n      <div class="input-annotation">\n        User\'s must be at least 18 years of age to create or manage events. Accounts progibited for users under 13 years of age.\n      </div>\n    </ion-slide>\n    <ion-slide>\n      <ion-item no-lines [ngClass]="\'ww-input-item\'" mode="ios">\n        <ion-label stacked>Enter your phone number</ion-label>\n        <ion-input formControlName="phone" placeholder="e.g. (232) 123 - 456" mode="ios"></ion-input>\n      </ion-item>\n    </ion-slide>\n    <ion-slide>\n      <div class="terms">\n        <p>By continuing I certify that I have read and understand the <a href="#">terns and conditions and privacy policy</a> set forth by Web Mobile.</p>\n        <p>I also certify that all the information that I have provided is accurate.</p>\n      </div>\n    </ion-slide>\n    <ion-slide>\n      <div class="congratulations-msg">\n        <h1>Congratulations!</h1>\n        <p>Your account is now created and active.</p>\n      </div>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/register/register.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_7__providers_storage_storage__["b" /* StorageProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1__providers_auth_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_storage_storage__["b" /* StorageProvider */]])
], RegisterPage);

// WEBPACK FOOTER //
// ./src/pages/register/register.ts
//# sourceMappingURL=register.js.map

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArchivePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_event_completed_event_completed__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__ = __webpack_require__(874);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ArchivePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var ArchivePage = (function () {
    function ArchivePage(navCtrl, navParams, httpProvider, screenOrientation, platform, iab) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.screenOrientation = screenOrientation;
        this.platform = platform;
        this.iab = iab;
        this.video_url = '';
        this.numberOfParticipants = 1;
        if (!platform.is('MacIntel')) {
            if (this.numberOfParticipants == 1) {
                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
            }
            else {
                this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
            }
        }
        httpProvider.getArchive(navParams.get('event')).then(function (res) {
            console.log(res);
            _this.video_url = res;
        }, function (err) {
            _this.close();
        });
    }
    ArchivePage.prototype.ionViewWillLeave = function () {
        if (!this.platform.is('MacIntel')) {
            this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
        }
    };
    ArchivePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ArchivePage');
    };
    ArchivePage.prototype.download = function () {
        var browser = this.iab.create(this.video_url, "_system");
    };
    ArchivePage.prototype.close = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    ArchivePage.prototype.ended = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_event_completed_event_completed__["a" /* EventCompletedPage */], {
            total: 1
        });
    };
    return ArchivePage;
}());
ArchivePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-archive',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/archive/archive.html"*/'<!--\n  Generated template for the ArchivePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n<ion-content>\n  <div class="headerBtns" end>\n    <button ion-button class="mini-btn" icon-only (click)="download()">\n      <ion-icon name="cloud-download"></ion-icon>\n    </button>\n    <button ion-button class="mini-btn" icon-only (click)="close()">\n      <ion-icon name="close"></ion-icon>\n    </button>\n  </div>\n  <div *ngIf="video_url != \'\'">\n    <video class="fullscreenvideo" (ended)="ended()" autoplay fullscreen [src] = "video_url" controls>\n    Your browser does not support HTML5 video.\n    </video>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/archive/archive.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
], ArchivePage);

//# sourceMappingURL=archive.js.map

/***/ }),

/***/ 210:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 210;

/***/ }),

/***/ 253:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 253;

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export AllEvents */
/* unused harmony export ContactData */
/* unused harmony export NotificationData */
/* unused harmony export EventPackageData */
/* unused harmony export EventConfigData */
/* unused harmony export AddonData */
/* unused harmony export PackageData */
/* unused harmony export OrderData */
/* unused harmony export EventDetailData */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventData; });
/* unused harmony export OfficiantData */
/* unused harmony export UserData */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return StorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AllEvents = (function () {
    function AllEvents() {
    }
    return AllEvents;
}());

var ContactData = (function () {
    function ContactData() {
    }
    return ContactData;
}());

var NotificationData = (function () {
    function NotificationData() {
    }
    return NotificationData;
}());

var EventPackageData = (function () {
    function EventPackageData() {
    }
    return EventPackageData;
}());

var EventConfigData = (function () {
    function EventConfigData() {
    }
    return EventConfigData;
}());

var AddonData = (function () {
    function AddonData() {
        this.selected = false;
    }
    return AddonData;
}());

var PackageData = (function () {
    function PackageData() {
    }
    return PackageData;
}());

var OrderData = (function () {
    function OrderData() {
    }
    return OrderData;
}());

var EventDetailData = (function () {
    function EventDetailData() {
    }
    return EventDetailData;
}());

var EventData = (function () {
    function EventData() {
    }
    return EventData;
}());

var OfficiantData = (function () {
    function OfficiantData() {
    }
    return OfficiantData;
}());

var UserData = (function () {
    function UserData() {
    }
    return UserData;
}());

var StorageProvider = (function () {
    function StorageProvider(http, storage) {
        this.http = http;
        this.storage = storage;
    }
    StorageProvider.prototype.clearAll = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.clear();
            resolve(true);
        });
    };
    StorageProvider.prototype.setSeenWalkthrough = function (seen) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.set('walkthrough', seen);
            resolve(true);
        });
    };
    StorageProvider.prototype.getSeenWalkthrough = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('walkthrough').then(function (val) {
                resolve(val);
            }, reject);
        });
    };
    StorageProvider.prototype.setToken = function (token) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.set('token', token);
            resolve(true);
        });
    };
    StorageProvider.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (val) {
                resolve(val);
            }, reject);
        });
    };
    /**
     * Sets the current logged in users profile
     * @param  {UserData} user User instance - UserData class
     * @return {Promise}              Javascript Promise is returned
     */
    StorageProvider.prototype.setProfile = function (user) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.set('user', JSON.stringify(user));
            resolve(true);
        });
    };
    /**
     * Gets the current logged in users profile
     * @return {Promise<UserData>}  Javascript promise is returned, resolves to UserData class
     */
    StorageProvider.prototype.getProfile = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('user').then(function (val) {
                var user = JSON.parse(val);
                resolve(user);
            }, reject);
        });
    };
    return StorageProvider;
}());
StorageProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], StorageProvider);

//# sourceMappingURL=storage.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_dashboard_dashboard__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_my_account_my_account__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_keyboard__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_create_an_event_create_an_event__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_deeplinks__ = __webpack_require__(858);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_tour_tour__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_walkthrough_walkthrough__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_email_composer__ = __webpack_require__(190);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, keyboard, auth, menuCtrl, httpProvider, storageProvider, deeplinks, tourProvider, iab, emailComposer) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.keyboard = keyboard;
        this.menuCtrl = menuCtrl;
        this.httpProvider = httpProvider;
        this.storageProvider = storageProvider;
        this.deeplinks = deeplinks;
        this.tourProvider = tourProvider;
        this.iab = iab;
        this.emailComposer = emailComposer;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_walkthrough_walkthrough__["a" /* WalkthroughPage */];
        // rootPage:any = LoginPage;
        this.user = {
            avatar: '',
            name: ''
        };
        this.auth = auth;
        var self = this;
        platform.ready().then(function () {
            var authObserver = auth.authState.subscribe(function (user) {
                if (user) {
                    storageProvider.getSeenWalkthrough().then(function (seen) {
                        if (seen) {
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
                        }
                        else {
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_walkthrough_walkthrough__["a" /* WalkthroughPage */];
                        }
                    });
                    // this.rootPage = DashboardPage;
                    // authObserver.unsubscribe();
                    // if (self.rootPage == DashboardPage){
                    setTimeout(function () {
                        self.storageProvider.getProfile().then(function (profile) {
                            console.log(profile);
                            if (profile == null) {
                                return;
                            }
                            self.user = {
                                avatar: self.httpProvider.apiUrl + '/user/' + profile.personal.id + '/avatar',
                                name: profile.personal.name.full
                            };
                        }, function (err) {
                            console.error(err);
                        });
                    }, 1000);
                    // }
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
                    // authObserver.unsubscribe();
                }
            });
            keyboard.disableScroll(true);
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.deeplinks.route({
                '/event/share/:eventId': __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]
            }).subscribe(function (match) {
                // match.$route - the route we matched, which is the matched entry from the arguments to route()
                // match.$args - the args passed in the link
                // match.$link - the full link data
                console.log('Successfully matched route', match);
            }, function (nomatch) {
                // nomatch.$link - the full link data
                console.error('Got a deeplink that didn\'t match', nomatch);
            });
        });
    };
    MyApp.prototype.createEvent = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_9__pages_create_an_event_create_an_event__["a" /* CreateAnEventPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.myEvents = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_dashboard_dashboard__["a" /* DashboardPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.orderHistory = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_my_account_my_account__["a" /* MyAccountPage */], {
            order: true
        });
        this.menuCtrl.close();
    };
    MyApp.prototype.myAccount = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_my_account_my_account__["a" /* MyAccountPage */]);
        this.menuCtrl.close();
    };
    MyApp.prototype.legal = function () {
        var browser = this.iab.create('https://webwedmobile.com/legal');
    };
    MyApp.prototype.termsOfService = function () {
        var browser = this.iab.create('https://webwedmobile.com/legal');
    };
    MyApp.prototype.signOut = function () {
        var _this = this;
        this.menuCtrl.close();
        this.auth.auth.signOut().then(function () {
            console.log('Signed Out');
            _this.storageProvider.clearAll();
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
        });
    };
    MyApp.prototype.becomeOfficiant = function () {
        window.cordova.plugins.email.open({
            app: 'mailto',
            to: 'info@webwedmobile.com',
            cc: 'team@riveloper.com',
            subject: 'WebWedMobile - Become Officiant',
            body: 'Hello, I want to become an officiant.',
            isHtml: false
        });
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('content'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/app/app.html"*/'<ion-menu [content]="content">\n  <!-- <ion-header>\n    <ion-list no-lines>\n      <ion-item text-center>\n        <ion-avatar class="center-avatar" item-start>\n          <img class="large-avatar" src="assets/images/user-avatar3.png">\n        </ion-avatar>\n      </ion-item>\n      <ion-item text-center>\n        <ion-title color="primary">Jane Doe</ion-title>\n      </ion-item>\n      <ion-item text-center>\n        <a color="light-blue">Sign Out</a>\n      </ion-item>\n    </ion-list>\n  </ion-header> -->\n  <ion-content>\n    <ion-list text-center no-lines>\n      <ion-item text-center>\n        <ion-avatar class="center-avatar" item-start>\n          <img class="large-avatar" [src]="user.avatar">\n        </ion-avatar>\n        <!-- <avatar class="center-avatar" item-start [user]="1"></avatar> -->\n      </ion-item>\n      <ion-item text-center>\n        <ion-title clear color="primary" class="primary">{{user.name}}</ion-title><br/>\n        <button ion-button clear color="light-blue" (click)="signOut()">Sign Out</button>\n      </ion-item>\n      <button text-left ion-item (click)="createEvent()">\n        <ion-icon style="padding-left:10px; padding-right:10px;" name="people" color="dark"></ion-icon>\n        Create an Event\n      </button>\n      <button text-left ion-item (click)="myEvents()">\n        <ion-icon style="padding-left:10px; padding-right:10px;" name="heart" color="dark"></ion-icon>\n        My Events\n      </button>\n      <button text-left ion-item (click)="orderHistory()">\n        <ion-icon style="padding-left:10px; padding-right:10px;" name="star" color="dark"></ion-icon>\n        Order History\n      </button>\n      <button text-left ion-item (click)="myAccount()">\n        <ion-icon style="padding-left:10px; padding-right:10px;" name="contact" color="dark"></ion-icon>\n        My Account\n      </button>\n      <button text-left ion-item (click)="termsOfService()">\n        <ion-icon style="padding-left:10px; padding-right:10px;" name="star" color="dark"></ion-icon>\n        Legal and Terms\n      </button>\n      <button text-left ion-item (click)="becomeOfficiant()">\n        <ion-icon style="padding-left:10px; padding-right:10px;" name="contact" color="dark"></ion-icon>\n        Become an Officiant\n      </button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n\n<ion-nav id="nav" #content [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/app/app.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_10__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_11__providers_storage_storage__["b" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_12__ionic_native_deeplinks__["a" /* Deeplinks */], __WEBPACK_IMPORTED_MODULE_13__providers_tour_tour__["a" /* TourProvider */], __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_16__ionic_native_email_composer__["a" /* EmailComposer */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_8__ionic_native_keyboard__["a" /* Keyboard */],
        __WEBPACK_IMPORTED_MODULE_7_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* MenuController */],
        __WEBPACK_IMPORTED_MODULE_10__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_11__providers_storage_storage__["b" /* StorageProvider */],
        __WEBPACK_IMPORTED_MODULE_12__ionic_native_deeplinks__["a" /* Deeplinks */],
        __WEBPACK_IMPORTED_MODULE_13__providers_tour_tour__["a" /* TourProvider */],
        __WEBPACK_IMPORTED_MODULE_15__ionic_native_in_app_browser__["a" /* InAppBrowser */],
        __WEBPACK_IMPORTED_MODULE_16__ionic_native_email_composer__["a" /* EmailComposer */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateAnInvitationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_media_capture__ = __webpack_require__(851);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_video_editor__ = __webpack_require__(852);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CreateAnInvitationPage = (function () {
    function CreateAnInvitationPage(navCtrl, navParams, httpProvider, loadingCtrl, viewCtrl, mediaCapture, transfer, file, videoEditor) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.viewCtrl = viewCtrl;
        this.mediaCapture = mediaCapture;
        this.transfer = transfer;
        this.file = file;
        this.videoEditor = videoEditor;
        this.subject = '';
        this.body = '';
        this.video = '';
        this.thumbnail = '';
        this.event = navParams.get('event');
    }
    CreateAnInvitationPage.prototype.pickVideo = function () {
        var _this = this;
        var self = this;
        var image = '';
        var loading = this.createLoading();
        // let options: CaptureVideoOptions = { limit: 1 };
        // this.mediaCapture.captureVideo(options)
        //   .then(
        //     (data: MediaFile[]) => {
        //       loading.present();
        //       let options: FileUploadOptions = {
        //         fileKey: 'picture'
        //       };
        //       let fileTransfer = this.transfer.create();
        //       fileTransfer.upload(data[0].fullPath, 'http://ec2-34-228-21-16.compute-1.amazonaws.com/api/upload/file', options)
        //        .then((response) => {
        //          var data = JSON.parse(response.response)['data'];
        //          var path = data['path'];
        //          self.video = 'http://ec2-34-228-21-16.compute-1.amazonaws.com/storage/' + (path);
        //          loading.dismiss();
        //          console.log(self.video);
        //        }, (err) => {
        //          loading.dismiss();
        //        })
        //     },
        //     (err: CaptureError) => console.error(err)
        //   );
        //
        var self = this;
        var nItem = localStorage.getItem('videoNum');
        var numstr = 0;
        if (nItem == null) {
            numstr = 1;
        }
        else {
            var numstr = parseInt(nItem, 10);
            numstr = numstr + 1;
        }
        var videooption = { limit: 1 };
        this.mediaCapture.captureVideo().then(function (videoData) {
            var filedir = _this.file.dataDirectory;
            var path = videoData[0].fullPath.replace('/private', 'file://');
            var ind = (path.lastIndexOf('/') + 1);
            var orgFilename = path.substring(ind);
            var orgFilePath = path.substring(0, ind);
            console.log("videopath", path);
            //SAVE FILE
            // this.file.copyFile(orgFilePath, orgFilename, filedir + 'recordvideo','sample'+numstr+'.mov').then(()=>{
            // var option:CreateThumbnailOptions = {fileUri:filedir+'recordvideo/sample'+numstr+'.mov',width:160, height:206, atTime:1, outputFileName: 'sample' + numstr, quality:50 };
            // this.videoEditor.createThumbnail(option).then(result=>{
            //     //result-path of thumbnail
            //    localStorage.setItem('videoNum',numstr.toString());
            //    self.thumbnail = numstr.toString();
            loading.present();
            var options = {
                fileKey: 'picture'
            };
            var fileTransfer = _this.transfer.create();
            fileTransfer.upload(path, 'http://ec2-34-228-21-16.compute-1.amazonaws.com/api/upload/file', options)
                .then(function (response) {
                var data = JSON.parse(response.response)['data'];
                var path = data['path'];
                self.video = 'http://ec2-34-228-21-16.compute-1.amazonaws.com/storage/' + (path);
                loading.dismiss();
                console.log(self.video);
            }, function (err) {
                loading.dismiss();
            });
            // }).catch(e=>{
            //  // alert('fail video editor');
            // });
        });
        // });
    };
    CreateAnInvitationPage.prototype.send = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.sendInvite(this.event.details.id, this.subject, this.body, this.video).then(function (res) {
            _this.viewCtrl.dismiss();
            loading.dismiss();
        }, function (err) {
        });
    };
    CreateAnInvitationPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return CreateAnInvitationPage;
}());
CreateAnInvitationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-create-an-invitation',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/create-an-invitation/create-an-invitation.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>\n      Create an Invitation\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content id="page6">\n  <div ion-button full large color="primary" text-center style="min-height:200px;">\n    <ion-grid *ngIf="video != \'\'">\n      <ion-row>\n        <video loop="" style="width: 100%;" poster="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABkAAD/4gxYSUNDX1BST0ZJTEUAAQEAAAxITGlubwIQAABtbnRyUkdCIFhZWiAHzgACAAkABgAxAABhY3NwTVNGVAAAAABJRUMgc1JHQgAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLUhQICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFjcHJ0AAABUAAAADNkZXNjAAABhAAAAGx3dHB0AAAB8AAAABRia3B0AAACBAAAABRyWFlaAAACGAAAABRnWFlaAAACLAAAABRiWFlaAAACQAAAABRkbW5kAAACVAAAAHBkbWRkAAACxAAAAIh2dWVkAAADTAAAAIZ2aWV3AAAD1AAAACRsdW1pAAAD+AAAABRtZWFzAAAEDAAAACR0ZWNoAAAEMAAAAAxyVFJDAAAEPAAACAxnVFJDAAAEPAAACAxiVFJDAAAEPAAACAx0ZXh0AAAAAENvcHlyaWdodCAoYykgMTk5OCBIZXdsZXR0LVBhY2thcmQgQ29tcGFueQAAZGVzYwAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAPNRAAEAAAABFsxYWVogAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z2Rlc2MAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHZpZXcAAAAAABOk/gAUXy4AEM8UAAPtzAAEEwsAA1yeAAAAAVhZWiAAAAAAAEwJVgBQAAAAVx/nbWVhcwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAo8AAAACc2lnIAAAAABDUlQgY3VydgAAAAAAAAQAAAAABQAKAA8AFAAZAB4AIwAoAC0AMgA3ADsAQABFAEoATwBUAFkAXgBjAGgAbQByAHcAfACBAIYAiwCQAJUAmgCfAKQAqQCuALIAtwC8AMEAxgDLANAA1QDbAOAA5QDrAPAA9gD7AQEBBwENARMBGQEfASUBKwEyATgBPgFFAUwBUgFZAWABZwFuAXUBfAGDAYsBkgGaAaEBqQGxAbkBwQHJAdEB2QHhAekB8gH6AgMCDAIUAh0CJgIvAjgCQQJLAlQCXQJnAnECegKEAo4CmAKiAqwCtgLBAssC1QLgAusC9QMAAwsDFgMhAy0DOANDA08DWgNmA3IDfgOKA5YDogOuA7oDxwPTA+AD7AP5BAYEEwQgBC0EOwRIBFUEYwRxBH4EjASaBKgEtgTEBNME4QTwBP4FDQUcBSsFOgVJBVgFZwV3BYYFlgWmBbUFxQXVBeUF9gYGBhYGJwY3BkgGWQZqBnsGjAadBq8GwAbRBuMG9QcHBxkHKwc9B08HYQd0B4YHmQesB78H0gflB/gICwgfCDIIRghaCG4IggiWCKoIvgjSCOcI+wkQCSUJOglPCWQJeQmPCaQJugnPCeUJ+woRCicKPQpUCmoKgQqYCq4KxQrcCvMLCwsiCzkLUQtpC4ALmAuwC8gL4Qv5DBIMKgxDDFwMdQyODKcMwAzZDPMNDQ0mDUANWg10DY4NqQ3DDd4N+A4TDi4OSQ5kDn8Omw62DtIO7g8JDyUPQQ9eD3oPlg+zD88P7BAJECYQQxBhEH4QmxC5ENcQ9RETETERTxFtEYwRqhHJEegSBxImEkUSZBKEEqMSwxLjEwMTIxNDE2MTgxOkE8UT5RQGFCcUSRRqFIsUrRTOFPAVEhU0FVYVeBWbFb0V4BYDFiYWSRZsFo8WshbWFvoXHRdBF2UXiReuF9IX9xgbGEAYZRiKGK8Y1Rj6GSAZRRlrGZEZtxndGgQaKhpRGncanhrFGuwbFBs7G2MbihuyG9ocAhwqHFIcexyjHMwc9R0eHUcdcB2ZHcMd7B4WHkAeah6UHr4e6R8THz4faR+UH78f6iAVIEEgbCCYIMQg8CEcIUghdSGhIc4h+yInIlUigiKvIt0jCiM4I2YjlCPCI/AkHyRNJHwkqyTaJQklOCVoJZclxyX3JicmVyaHJrcm6CcYJ0kneierJ9woDSg/KHEooijUKQYpOClrKZ0p0CoCKjUqaCqbKs8rAis2K2krnSvRLAUsOSxuLKIs1y0MLUEtdi2rLeEuFi5MLoIuty7uLyQvWi+RL8cv/jA1MGwwpDDbMRIxSjGCMbox8jIqMmMymzLUMw0zRjN/M7gz8TQrNGU0njTYNRM1TTWHNcI1/TY3NnI2rjbpNyQ3YDecN9c4FDhQOIw4yDkFOUI5fzm8Ofk6Njp0OrI67zstO2s7qjvoPCc8ZTykPOM9Ij1hPaE94D4gPmA+oD7gPyE/YT+iP+JAI0BkQKZA50EpQWpBrEHuQjBCckK1QvdDOkN9Q8BEA0RHRIpEzkUSRVVFmkXeRiJGZ0arRvBHNUd7R8BIBUhLSJFI10kdSWNJqUnwSjdKfUrESwxLU0uaS+JMKkxyTLpNAk1KTZNN3E4lTm5Ot08AT0lPk0/dUCdQcVC7UQZRUFGbUeZSMVJ8UsdTE1NfU6pT9lRCVI9U21UoVXVVwlYPVlxWqVb3V0RXklfgWC9YfVjLWRpZaVm4WgdaVlqmWvVbRVuVW+VcNVyGXNZdJ114XcleGl5sXr1fD19hX7NgBWBXYKpg/GFPYaJh9WJJYpxi8GNDY5dj62RAZJRk6WU9ZZJl52Y9ZpJm6Gc9Z5Nn6Wg/aJZo7GlDaZpp8WpIap9q92tPa6dr/2xXbK9tCG1gbbluEm5rbsRvHm94b9FwK3CGcOBxOnGVcfByS3KmcwFzXXO4dBR0cHTMdSh1hXXhdj52m3b4d1Z3s3gReG54zHkqeYl553pGeqV7BHtje8J8IXyBfOF9QX2hfgF+Yn7CfyN/hH/lgEeAqIEKgWuBzYIwgpKC9INXg7qEHYSAhOOFR4Wrhg6GcobXhzuHn4gEiGmIzokziZmJ/opkisqLMIuWi/yMY4zKjTGNmI3/jmaOzo82j56QBpBukNaRP5GokhGSepLjk02TtpQglIqU9JVflcmWNJaflwqXdZfgmEyYuJkkmZCZ/JpomtWbQpuvnByciZz3nWSd0p5Anq6fHZ+Ln/qgaaDYoUehtqImopajBqN2o+akVqTHpTilqaYapoum/adup+CoUqjEqTepqaocqo+rAqt1q+msXKzQrUStuK4trqGvFq+LsACwdbDqsWCx1rJLssKzOLOutCW0nLUTtYq2AbZ5tvC3aLfguFm40blKucK6O7q1uy67p7whvJu9Fb2Pvgq+hL7/v3q/9cBwwOzBZ8Hjwl/C28NYw9TEUcTOxUvFyMZGxsPHQce/yD3IvMk6ybnKOMq3yzbLtsw1zLXNNc21zjbOts83z7jQOdC60TzRvtI/0sHTRNPG1EnUy9VO1dHWVdbY11zX4Nhk2OjZbNnx2nba+9uA3AXcit0Q3ZbeHN6i3ynfr+A24L3hROHM4lPi2+Nj4+vkc+T85YTmDeaW5x/nqegy6LzpRunQ6lvq5etw6/vshu0R7ZzuKO6070DvzPBY8OXxcvH/8ozzGfOn9DT0wvVQ9d72bfb794r4Gfio+Tj5x/pX+uf7d/wH/Jj9Kf26/kv+3P9t////7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAgICAgICAgICAgIDAwMDAwMDAwMDAQEBAQEBAQIBAQICAgECAgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwP/wAARCAGQArwDAREAAhEBAxEB/8QA4wABAAEEAwEBAQAAAAAAAAAAAAEFBgcIAwQJAgoLAQEBAAIDAQEBAAAAAAAAAAAAAQIDBAUGBwgJEAABAgUCBAMFBQQGBwIKBgsBAgMAEgQFBhEHIVFhCDGhE0GxIhQJ8HGRMhWBQlIjwdHhJBYKYnKSsjNTF0MY8aLCc7MlRrY4eIKTozRHGcPTRHSEtCbGp7cpEQABBAEDAgQDBQIICgcIAwEBABECEgMhBAUxBkFRYRNxIgeBkaEyFLEj8MHRQlJichWCkrIzQ1OzJBYI4aLCY3NENvHSg1R0JXUmk8M0o//aAAwDAQACEQMRAD8A/IzKOQj9n1K/HbqdByH4QqUcqJRyhUo6mUcoVKOko5QqUdNBy8YVKOko5D8IVKOo0HIQqjlTp0H4QqUdJRyEKlHSUcoVR0l6fhCpR1Eo5CFUcpKOUKo5Uy9OHj4QqUdRoOQhUo5TQchCpR1Mo5D8IVR1Eo5CFSjlToOQ/CFSjqJRyEKlHKSjkIVRypl6eMKlHUSjkIVRyp0HjpCpR1Eo5QqjqdByEKlHSXp9vCFSjpL0hUo6aDlCpR0l6QqUsmnSFSjpoIVKOko5QqUdJR46QqUdJR46QqjqNByEKo5U6dPt98KlHUaDlCqOu8qvrV0DdsU+s0LT6qhFPwkDqgAVeExA04DwBJ5xPbFrN8yt5Vq/yroyjl4RalSymXp0+3WFSjpp08IVKOkvTyhUo6iUcoVKOUlHIQqjlTp0hUo6jQcoVKOUlHKFUcqZfbp9vGFSjqNByEKo5TQchCqOVMvTyhUo6iUcoVKOko5QqjlJRyEKlHKmXpCpR1Gg5fb+iFUdJRyhUo5SUcoVKOvpKUzIm0lnRN/qzCbyhUo6ym9VUDtwRcbVmNPa1iipaT0HKV4JDbDKGy24pRlWgrSTxT7Y4oxyEazg4dcq4taM2K+LlYLxfWGqysyu0VtEy25UNuqW81TtMpUW3X0obppAEkEE8xpFiYQJEYEFSUZzDmQMQrdrqq12m1v2Wz1H6g/XLbXdLmGy2yUNKCm6WjSrUluYCZf7wjZGEpyvMM3QfyrWZiMaQLk+Kotjr0Wq7UVc4j1GmHFJeRxJUw8hTL4AB4qDTh0HOMsmMzgY+KxhOshJXI9jF2pa1u7Y4hV2olPiroqijKXnUaqK/RqWUq9RtadSlQ8CPAxrE4mNMmh8VnWQlfHqFfisvvDNODU4hdBVhHxSpUmnU5pxUD6CnEJJ46aHTnGj2IvpMMt3vTA1iXVkZxV1Ne1YauqbVTLqKN5a6MhYQw767qVKCXPjCloQkEnx0jfggImUR5rVmlKVSfJWBKOQjfVaHKnTpCpR1Gg5CFSjlTKOQhVHUSjlCpRyko5QqjlTL0H4QqUdJfbp9vZ90KlHTQchCpR1Eo8dIVKOko5CFUcpKOQhVHKnTX2a/shUo6aDkIVKOqlZ642u60NwCZhS1CFrT/G3rotP3ERjPHeJj5rKE6yBWzlLUU1bTtVdK4h6nfQFtuI0KSCNdD/Coe0HiDHWmBiWPVdgJOHHRWhc7dl6X1v0eSUjNDOVrFRTIYFIxrqSDK8l1LafapSSY3Q9pmMfm/atcvedxL5Va9fllZWOU+PWGrdrKh9Ypn70+hKVuqcMrhpmkJCWqdsakq01lB05xsjtwP3mQMPJazmkWhA6+avrHsWoMfbUWh8xXOjSqrXBotwkhSkNp1IbamGunifEmNGScsnXp5LbCAgP63mrl09vtjXUrY61Uk+6O7b0XUOEk+6DeiOEkg3ojhJIN6I4ST7oN6I4SSDeiOEk+6K3oq4SSI3oo4ST7oN6KuEkg3oo4SSDeiOEk+6DeiOEkg3ojhJIN6I4SSDeiOEk+6DI4ST7oN6I4ST7orKuEkiN6KOEk+3GDeiOEk+6DeiOEk+6DeiOEkg3ojhJIN6I4ST7oreirhJIjeijhJPug3ojhJIMjhJIN6I4SSDeiOEk+6DKuEkg3oo4SSDeiOEk+6DKuEkg3oo4ST7oN6I4SSDeiOEkg3ojhJIMjhJIN6I4SSDeiOEk+6DeirhTJ9tIN6KOFEkGRwkkG9EcJJBvRHCSfdFZHCSRG9EcJJBvRHCSfdBvRVwkn3Qb0UcJJBvRHCSfdBvRHCSfdBvRHCSQb0Rwkn3QZHCSQb0RwkkG9EcJJBkcJJ937YN6KuEkg3oo4SSDeiOFeCMhp0WVdrFM8HFWldvDmrZb9RdY4+tw/EFBBbXp4E69OMavZJnY+brd7saVY9GVnyfdG1vRaXCSfdBvRHCvDE66nt4uJfrE0qnfkUMpU4tIWPn6VVQoAcAUsJUSf4dY1ZYmTMH/APYt2KUYu+nT9q+Ltkl3F3uLluvNYikVVL+X9CpWGvTASkFseEplhDFGgEoh2SeU3NSWdUGvuNwui0OXCrerHG0yIW+qZSU6k6TaA+JjZGAj+UMtcpmX5tV0JPujJvRYuEk+77e6I3ojhJIN6I4SSDeiOEk+6DI4SSKyOEk+6IyrhJIN6KOEkgyOEkg3ojhJIN6I4SSDeiOEkg3ojhJIN6I4ST7oN6KuFVrZebtZ5v0+tdp0LOq2gQ4wo/xKZcCmyrrpGMscZfmDrKOQw/KrotKr5m9eKC4XJ75BhAqKtDQSy2WwqVKUtNJSgrWshIJ10110MapiGCNoj5ltgZZ5VkflVdw2x05yC83BlqSit1Q5S0CSVGRaxKSFK4rlaCgePiY155n2xE/mPVZ4YR9wyHQLJ9S8xRsO1VU6hinZQVuOrOgSkch4lSjwAHEmOLESkWA1XLMogOeixP8A9RXv1if5cfouvpejKPmpNf8A73N4+p7ZNdJescv9N8jf6T8FxP1Xzu3yLG8vTyjsWXXW9Ul6QZHSXpBkskvTygyWSXp5QZHSUcoMrYpL08oMpZJenlBkskvTygyWSUcoMrZJenlBlLJL0gyWSXpBlbJL08oMo6S9PKIyOkvTyisjoEj2D+mDK2KS9IMo6SjlBlbJL08oMpZJenlBksko5QZWyS9PKDKW9Ul6eUGSyS9PKDI6S9PKDJZJekGSySjlBkdJenvgyWSXp5QZLeqS9PL9kGR0k6Hzgytkl6eUGUsko5QZV0l6QZR0kHL3wZWySjx0gyWSXp5QZSySdPfBlbJKOUGR0lHKDJZJRygyWSUcoMjpKOUGSySjlBkskvSDKWSXp5QZHSXp5QZLJL08oMlvVJRy+3sgyrpL08oMo6SdIMllMvTygyP6qJenlBkspl/0fKDJb1US9PfBksko5QZVyko5QZHSXp5QZSyS9PKDI6SjlBkdJRr4fb7oMrZJenlBlLJL0gyOknSDJZJekGSyS9PKDJZJRy98GVsko5QZLJKOXj9uEGSyS9IMpZJenlBkskvTygyWSXpBksko5QZWySjlBkskvSDKWSXp5QZLeqSjl0gytklHL3wZLJKOUGRykuvsgyWSX2aeUGUspl6eUGR/VZC23q26e81FMvQKraSRrXh/MYWHyNeqEHQc4425g8AR4FcnbTabHxCynR0dvxu2Ohb0tM049V1NS9KFrceVMsmUCYk8EpHHXgI4srZZ+q5YIxR1OiwrlOTVGQVBbQFsW1lR+Xp/AukcPXqAOCnCPAcQkRzcWAYx/WXBy5zkLP8AKrTlHL3xuZarFdmUxvqFosElMKhLBJTCoSwSUwqEsElMKhLBJTCoSwSU8oVSwSUwqEsElMKpYJKYVCWCSmFUsElMKhLBJTCoSwSUwqEsElMKhLBJTCoSwSU8oVCWCSGFUsElMKhLBJTCoSwSQwqEsElPKFQlgkphUJYJKeUKhLBJTCoSwSUwqEsElMKhLBJTAxCWCSmFQlgkphVLBJTyhUJYJKYVCWCSmFQlgkhhUJYJKYVCWCSnlCoSwSUwqEsElMKhLBJTCoSwSUwqEsElMKhLBJTCqWCSmFQlgkphVLBJTyhUJYJIekSqWCSnlFqEsElMKhLBJTCoSwUyHpCoSwUSmFUsElMKhLBJTCqWCSnlCoSwSUwqEsElMKhLBJTCoSwSQwqlgkphUJYJKeUKhLBJDCqWCSGFQlgkphRLBJDCoSwSU8oVCWCSGFUsElMKpYJKYVCWCSmFQlgkphVLBJTCqWCSmFUsElPKFQlgkp5QqlgkphUJYJKeUKhLBJTCoSwSUwqEsElMKhLBJT0hVLBJTCoSwSUwqEsElPKFQlgkhhUJYLmp3X6V9qoYWW3mHEutLH7q0KCknT2jUeEQwBDFUTYuOquDIcmr8gUyh4BilZSkppW1EtqeA0U+4ToVqV7Nfy+yNePbwx9NT5rZkzyydeitmUxtqFqsElPKFQlguzKI31K02KScgT93EnkIlSllmyx4ZZLXQNVt8TTPVLraXXFVriU0tOFJmDbSFEJWpIPFR1OvsEcLJlySlWHT0XOhjhCLzYy9VQsgt+D1aFG2XKgt9akgJNP6iqJzj8QeQ2hSUqA4hSfuPTPH+oB+cEx/Fa8nsSHykCX4Kp2XE8LqWUtC4NXeqUkeopFcWNFe0NMNrbUkAnhrMr3RhknnBdiB8FnCGEjq5VsZdhosSUV1E447QLd9JxDuhdpXFH+WkrAHqNr8AdNQfHnG7DkOT5ZfmWrNj9sWj+VWJKI31K49ikg6/fFqUskghUpYqJIlSllMoi1KWKSCJUpYpIOsKlLFJBCpSySiLUpYpIOsSpSySiFSlikkKlLJKOsKlLFJBCpSxSQdYVKWKSiFSlikohUpZJB1hUpYpIOsKlLJIOsKlLJIIVKWSQdYVKWKSCFSlklEKlLJIOsKlLFJB1hUpZJIVKWSSFSlklEKlLFJPvhUpZJRFqUsVEkSpSymUQqUsUkHWFSlklEWpSxSQQqlikoiVSxSUQqUsUkEKlLJKItSlikg6xKlLKJBCpSymUQqUsUlEWqWKSffEqUskg6wqUsUlEKlLFJR1hUpYpKIVKWKSDrFqUsUlESqWKSRalLJIOsSpSySiFSlkkHWFSlklEKlLFJRCpSySiFSliko+3ui1KWKSj+2JUpYpKIVKWSQQqUskohUpYpKIVSxSQQqUskg6wqUsUlEKlLFJR9vCFSlklEKlLFJB90WpSySiJUpYpIIVKWKSDrCpSxSQfb7GFSlkkEKlLJIItSlikoiVKWKSDrFqUskoiVSxSSFSlkkEKlLJKItSliuzKeUbWWhwpAUCFDgQQQeREGVsxWSbTi9blLCLze7uflFCVCUuIcWhDQSmVRWfl6MJHslKuYEcSeWOE+3jGv8PvXJjA5RfJLT+H3LvqtO3FKr0XqwPOD4VLTX1StCCQZjTlLQOvTSMQd2dQNPgsiNqNCdfipcwSy3Jo1WOXXRxPxIHrpqWgoeALiJalgn+IlX3QGfJAtljp93/Qr7OOQfFJWxdblkNBQ1WOXxtTyV+kunefM60IaUZVsvjhUNHwGvFMboY8UpDLjWmeXJEHFk1VmSnlG9locJKeUGRwkp5QZHCaHlBkcJKeUGRwkp5QZHSU8oMjhNDyisjhNDyiMjhJTBkcJKeUGRwkp5eEGR0lPKDI4SU8oMjhJTygyOElPKDI4SU8oMjhJTygyOElPLygyOkp5QZHCaHlBkcJKeUGRwkp5QZHCSk+yDI6SnlBkcJKeUGRwkp5QZHCSnlBkdJTygyOElPKDI4SU8oMjhJTygyOElPKDI6SnlBkcJKeUGR0lPKDI4SU8oMjhJTygyWCSnlBkcJKeUGRwkp5QZHCSnlBkcJKeUGRwkp5fYwZHCS9PKDI6SnlBkcJKeUGRwkp5QZHCSkeyDI4SU8oMjhJfbp5QZHSU8oMjhJTygyOElJ9kGRwkp5QZHCSnlBkcJKeUGRwkp5QbVHCSnlBkcJoeUGRwkp5QZHCSnlBkcJKeUGRwkp5QZHCaHlBkcJKeUGR0lPKDI4SU8oMjhJTygyOElPLygyOkp5QZHCSnlBkcJKeUGRwkp5QZHCSnlBkcJKeUGRwkp5eUGVdJTygyjhJTygyOElPKDI4SU8oMjhJT4af1QZH8VzynlHIqtThTKYVCWCqtPT3A0PqOfPN2M1jaKpxmY04cVrOQgkNrcS2D48NdNfZGBEBLRvcb7VkCavr7b6q+kt7aUaEMrVUViilMz4Fa4riAeJZCWm1jXiEgaERxW3ci/T7lyAdoNNT96ptzo7Pa0094xS+OJfW8ltugQt1yqUrxKQAkOgJBGqXQQdfGNkLzfHnjp5+CxkccPnwyLv0Vp3S43C61Sqi4uqdfSPSlKQhLQRwKENJAS38QJIA/MTG6GKMI1iNFpnkM5WkdVTZTyjKqxcKZTCqWCSmLUJYJKeUKhLBRKYVCOFMphUJYKJTyiVCOFMphVHCSmLUJYJKYVCWCSmFQlgkphUJYJKYVCWCSmFQlgkphUJYJKYVCWCSmFQlgkp5QqEsElMKpYJKft/ZrCqWCqVNY7xWMfNUtsrKinJIDzTKltkpJCgFDgdCOMYGWMGpIB+KyEZSDgExVe/wANspxJd7X8yLh+oJpUMeKNPmVUym/SCCtS9U68Drr0jVc+/wC3/MZbKR9n3NbOrUVTVCPz076P9dl1PvQI3gRPRlpf4rhI04K+H/WOnvi1SwVfrseqaGz2y8OPNLauXFDKQoONAhZRMSJVahs66eHDxjVGUZTMA7hbJQrATJ0KoMpjbULXYJKYVCWCSmJVHCSmLUJYJKYlUsFEp5QqEcKZTFqEsFEp5QqEcIUkez8IVCOFMphUJYJKeXuiVSwUSmFUcKZTFqEsElMKhLBJTCoSwSUwqEsElP2/s1hUJYJKYlUsFEp5RahHCSnlCoRwplMSoSwSU/b7awqEsElPKFUsElPKLUJYJKYVCWCSmFQlgolPKFQjhTKYVCWCSmFUsElMKpYJKYVCWCSmFQlgolPKFQjhTKYVSwSUwqEsElMKhLBJTy90KhLBRKYlQjhJTyhVHCmUwqlgolPKLVHCmUwqEsElPKFQjhRKeUKhHCmU8vdCqWCSmFQlgkpiVSwSU8otUsElMKhLBRKeUKhHCSn7awqEdTKYVSwSUwqEsElMSqWCSnlFqEcJKYVCWCSmFQlgueSNrei02VStFrXdrlSW9BINQ6AtQ8UspBW8of6SWkkjrGGQiEDLyWeMGcxAeKuXL7okupx+3H0bTagKf0mjoiofQPjcc0/OATw6k9I04MX+ln+eX4Lbny6+3HSA/FW5bquko01Saq2U1xLzUjCn1OINK7xAcSUETDQ8RwOoHHntnjlJqkj+Nao5Ix6xBK4LfVvWyup6+nl9amdDiApIWkgHighWuoKSR98ZSxicTE9CFI5DCQkOoV15nQ07iqDIKJIRTXlqd1CRwbrEJBdBIGg8QDzWCY07cEA45dYrdnbTLHpJWPJHIZceySdYVSySQqlkk6wZLJJBvRLJJEZLJJFZLJJCqWUyCIyWSTrFZLKJIVSySQqlklgyWSSDeiWSSDJZTJBkskkRksoki1SySQqlkl4QqllsViTXp45bG+I1YcUOnquur1H+3rHV54vlkf4aLtMB/dBUuns15SaW2OtUotlHeFXb9QTUav1CPmlVaKf5WWZt2dWhVqU++MiYazD3MWb7G6rAQyOIlqCTv9r9FeykpV+ZKVa+MyUq1/EGOPRchdGuoGKikqmkUlKp1ymfQ0VU7A0dW0tLZmKPh0WRx9kZRcF9WfzWMgDEsA7KyqGnF1OP2yotdSGLM2/+qIrqYopg6EONtNIK/hqJy5MJdRoI5EhS0wQ8ujHVceJvWBifl6v0VDz+z2u3M0DlBQ09G4866lz0EemlaEpR4pBlJBPCNm2lORNySsNzGEADEM6xlJHLZcOySRapZJIVSymSDJZRJ1gyWSTrCqWSSFUskkKpZJIMlkkgyWUyRGSyiSLVLKQ2pS220JW44642y022hTjrrzq0tssstNhTjrzziglCEgqUogAEmIWiHPQLKAlkkIQBMj4L1f2a+iv35brbXXnffKNv7F27bGY/jldlt23T7jcia21tDGP0VEqtTcWMefpq/NaynrkpDdMtu2qS88tCEmZadfF7/v7tvZbyPHYckt1yMpiIx4I+4bEsz6QDdS8tAvZbLsTuHd7SXIZscdtx8Y2OTNKgYB3b85fwaLHwK8pqllhqpqWaWtZuNKzU1DNNcaZqqZprhTtPLbYr6ZmuYpK5qnrWkh1tLzTTyUKAWhKtUj2cQZRBIYkdNNPTRx9xZeOyfJMwcFizh2P3gFcEn26xlVYWVXstirL7V/KUYAlSFvvuEhqnb10ncIBJ1PAAcSeEa8ko44vJbMcZZJVishr2wbLH8m6OGqA8XKZPy6laeGiFlxA19uqvujijcl9Y6fFco7XTSXzK1KXBb9UVr9IunFKmmXK7VPlQpiD+UsrSkl+YeEo+/SN5zYhGw1f71ojhymRizAePgu5c9vrrQsLqKd5mvQ2krdbZC0PpSkaqUltfBxKRx4GbT2RjDcQkWIZZT2+SIcMVYsnP7fePYY5NVxrKJIjJZAAddCDpw4HwPI+MWqtlMkKqWUSQqlkk6wZLJJ1gyWSTrCqWSSFUskkKpZTJEqllEn2+3hFqllMkGSyiTnCqWSSFfRLJJBvRLJJCvolkk6wqlkkiMllMkGSySRapZJIVbwSyiSFUskkKpZJIVSySQZLKZIjJZRJ1hVLJJFqlkkiVSymSKyWUSQqrZTJ1hVSyiTrCqWUyQZLKJOsRksuxKI3MVqcq/Nu2kqvritNVt0Ti2gdPzlaE8Neio4+6f228HXJ2p/e+rKyakFVTUqVrMal8qJ8dfVX468o3xBqB4MuOZFz8VwyjlFYqOUlENUcrIFWjXby3FY1KLkr0Cf4FVLwc058dY4sQ26LeX8S5Mif0o+Kx/KOUcpiuM5SUcoao5SUQYo5SUQYo5SUcoMUcpKIMUcpKIMUcpKIao5SUQYo5SUcoMUcpKIMUcpKIMUcpKIMUsUlEGKOko5QYo5SUQ1RykohqjpKOUGKOUlENUcqCkaH7j7oMUcrMF6u1fYMbxo25xDLtSyhLiltNvaoTSMvABLqVAalzxEcLHjGTLO3h/KudkyzxYYU6lUGgzPK6x5NNStMVz6hqG0USSrQeKj6RQEpHM6Rslt8UfmOg+K1x3OeRaOp+CqNZmeT2pxDd0s9IwpYJQXGqlsOAeJQtNSptWnSMY7fFL8kif4fBZndZ8ek4hc4z26NsoqajHVfLK8H0uVDLSvuWuncSAefhGP6WJNRLVP1eQCxjovpO5dPLq7aXxoOIbq0OeHKZlqB2kvAhX9a/UH71mrbPt03c7u95dqthNi8WeyjcPcCqqEW+jW6KS2Wi3SUT1yyPJbosKp7Njtjpl+pVVLmoGqUICnXG0K67ecrsuC2Gbk+SmIbXEA58SdWjEeMj4D+IErsdnxe+5zfYeO4+F9xk6eQ6ayPQADUk/Y5YH9n3bV/lWezLCcOty+57Oty99NxqqiZVfRiuS1u2O3tpr1oCn6bGqCxobyyup6ZxRQKmvuBNQlIX8sxqWx8A5f6z8/uc5HD48W22gOlo+5Mj+sT8o+EY6dHPVfd+H+kXCbTADyuTJuN2R8zERgD/AFQxl9pOvkFrR32f5VrCW8NvOc9gm4WU0mZ2infrk7I7uXmkvljylpltbhtuI5/8pQXXH70sICWW7t89TVDitF1NKn4o7ftv6z7j347buXFA7eWnvYwRKPrKGokPOrEDwkup7j+kOCWGW57cySG4GvtZCCJekJACp8rOCfGK0q+mj/lo94+4Npe5ffK/m/bZtxS19VRWraqjt1Jb96sxct9UqlqKu5KvVNXUGAY2460r0XXaWrra5sBbTbLS26hXoO7vq5sOLP6Tt0Y93uyATkJfFFx0DEGcvMAgDxJLhdB2p9Kt9yP+99wGe22oJAxgNlk3ibaQj6kEnwADE+7Oa/5YD6X+Q4i7YsVse9O3eSfLenTZ3Zt3siv93TUhGiamqsmZf4gxKoSpfFbbdAwCNQko4EfNtv8AWDvDFnGXNLBlxP8AkOMAffFpfiV9H3P0q7Uz7f2cUMuLI354y+b7XBH4Bfjs+qf9Jjej6Ye4lmoslure5WyWfVVaxtlvHa7S9aKe41lG2qqexXLbQai4NY5mVPQJLwp01L7NWw246wtQbcSj712Z3tsO8NrKWGPs8hiA9zES7A6WidLRJ0dgQdD4P8L7w7M3/aW4Huy93YZCaZAGf0kNayHk5fqCdW8opRHtWK8W5SUQYo6SjlBijlJRygxRykogxR0lHKDFHK5qelqKuop6Sipamtrax9mkoqKjYcqqytrKlxLFLR0dKylb1TV1T60obbQFLWtQSkEkCJIiMTKRAiA5J0YeqyhGeWYxwDzJYBfpu21wftn+hntBge9/cjtzjPcN9T/dnGKbNtn9hMiqKa5YT2x4vc2dbRlu4DVMqobpcpfn4LE1e6tKmaFVOlFVUI+P7rPzH1H32Tj+Jyy2vaGCZhlzR0luJDrHG/WP4eMgXjFfW9th4j6e7LHv+TxR3PdeeAljwyYxwRPSeQDQSPl18IkNKS8X+7X6h3eN3vX6vu3cVvhl2WWWrrF1dHt1bK57HNrrEgrSpmks+CWlxiypapkoSEuVCKipcKAtx1bmqj9A4TtTgu3cQhxe2xwyAazItkl6mZ118gw8gF4Pme6+d53KcnI7icoE6QBaEf7MR8obza3mStK5RHodV550lEGKOVdmO36stFPVUdsoU1Nwr30em6UqdUhCW0pCEMpGq1TjXiQke0GNOXEMhEplogLfizTgDGAeRV0JxzOLwPVuF0NGFDUMKqXEkA+wU9GgoaPHwJBEafc2+PSMX+z+Vbvb3OTWcm/h6L6OGZbRfzKK+lax+VHzVWyonx0SXUqaGvVQh72GX5o/gEOHcR/LLX4lRTZdfLFVIo8monHEAj+eG0ofKQf+IhaP5FWnn4KPP2EdvDJF8JSO5zYjXMHH8PvWOby5RKuVxqKNQ+RW+t9lWhSA2pIcWSkgEfGVajSOVATEAJfmAXFmbZCIdCdPtXu/2x/To7Vu3bZfDO9H6uec3/Btvs9ok37YftAwlVS1vpvtbQkO0d+vtvo6ijvOMYRcgUKZ/nW/16dQderKZpxkP/NeY7q5rlt/k7f7HxRybrEa5t1P/M4T4xB1Epjx/Mx0ESxb6Rw3bHE8Vsoc93nkMNtkFsO3j/nco/pEaGMD4H5X0NouH0J+ob3Hdrncxu5iWTdpHadaO0XbHDsCZwpeH2uus9TVZrcaS+3a4sZxkFJY7XR0Vtvztrr2KN5HzVzcWKYLVVLKtB6btbiOZ4jYzxc3vZb7eZMlrEECAIApFySQ4JGkRqzBea7o5fiOW3scnC7OOz2sICLAgmZBkblgGJBAOsjo5K0HlEem1XmXKSjlBijlJRBijlJRyhqjlJRygxRykogxKOUlEGKOUlH2+2kGKOUlHKDFHKSiDFLFJRDVHSUcoMUcpKIMUcpKIMUcpKOUGKOUlEGKOUlENUdJRBilikogxRykogxRyko5QYo5SUQ1Ryko5QYo5SUQYo5SUQYo5SUQYo6SiDFHKSiDFHKSjlBijlJRBijlJRygxRyko+39kGKOko+39XhDVHXY0jdVaXVwYvckWi90dW4QlgrLL58AG3QUTqP8LalBR6CNObEZ4zEdVtw5KZBI9F2svsqrXdXnW0a0Feo1NG6kEtkL0Utqb+JBOvUHhE28r42P5h1We4gYZCR+U6hWroI31Wh12qKhfuNWzRUrZcfqFhCUpBOg/eWrklAOpJjGTRiZS6BWIlIiMeqvXNHqaiprXjVIpKxbWw7VqQToX1pGiT7QSf5nQK0jj7eEpGWaX87ouTuJiIjhj0HVY/0Ecmq4rpp9vGFUdNByEKo5TT2wqjqdPbFqo6jQch+EKquUlHKJVHKnSFUdRoIVR00HIfhCqOU0EKo5TQQqjqdIVUdRoOUKqup0hVR1Gg5CFVXTQch+EKo5U6awqjppFqokupA5qSPxIH9MSqqyTnnwW/Gqf/l0iVaf/wAKy3r/AOJHD2oeUz6rl7okRgPRVDbNun9C7OAJ+b9Zls+E4pfTSocPEJLxP7Yx3okDFvyt+Kz2ZDSP87+JZGq6Cir0IbraViqQ24l1tD6AtKHE+Ckg+B94jhgziXiWXLkBMNLUBdhbLTzaqd1CVsOI9FbSgPTLShKUaeCUy/hEYguOqp1DHotYnmW01zrDOhaFYplvTiCj1pABzA8P2R3QBq56sumP5mHR1+qT6Cvc/wBm3Zcnu97iu6HPbDhWSYphW0+Lbes1DCrjm2Q2jJHMzuGWY9t/j1Gh275FcbpcMYtPzDTCQ2wG21vLQ3qtPxX6m8Lz/cH6DiuGxSyYZ5Msp+EImNBGU5HSIAlJn1OrAnRfZfpty3B8Ed9yfLZI480MeOMPGRErGQgOpcxi/gGBJA1Vwd2P+aX7ld0b9UYJ2N7QWvae03CrVQY/luZ2ZG6O8d/IWpLNRasNpUVWG2J2qSoa0iqa+Op0BD6SSkauE+i/EbLENz3JnlnyAPKED7eKPxl+eTebw+C3cx9XuX32Y7bt3BHDiJYTkBPIX6MD8kSfIifTQrUihwP/ADI3d+hN+W73uPWS8hNwoXb7k1RsxibrVSVKS5ardXV+I0LFOOSGkhPhw00ju5bn6S8Cfb/+3DJHQtEZZaeZAmX+1dMNv9UubiMh/XmEtXMpYh9xMI/cF2ansi/zHuzSTk9m/wC+AKunAdmwXfxGaXYBH8z4LZac1utS8ofwhtRPhpEHcP0n5D9zkOwqf6eGg+8wAV/uD6o8efdgN6T/AFcxkfujkJVY22+vD9X7sZzCjwfuhtF13Do6J35aswXuc23uOJ5dUoSsGqVa88ttDjuQ1NwS0khDj67nTtK4qZWAUnDdfTTsPuTAdxw8o4ZHpPb5BKPo8CZRbzAqfULZtvqH3z29mGLlYnNjidY54GMm/tARmPQysPQr0F79frD9j/1RPpY784bdqWo2a7mMObwDMcT2oz9ymuNa/kdBnmN0VyuG1WY0lKzbctZcx2urmHkFmhuTdG86tdKltK1jy3bPYPcnZvem2zwI3HEZLwllhoKmEiBkiS8fmESNZRdtX0XpO5O+O3u7uztxil+45WFJxxz1+YTi/tyYW+UkHSMmcs2q/O59N3tc7eu9HcbL+23dDcvJtoN8twcaqFdreZINtqdsrjuTZ6Oqub2B7l2x+gduxpsqoKZwUlZSVVMaV1nSWocW2w59W7u5jle3tpj5fZ4YZ+OxT/3iGvuDGWF8ZdnieoILg+ABI+XdqcPxnP7mfE7rLkwcjkh+4nocfuDWuQM7SHQghj5kgHTTerZjcjt53WzrZTd7GqrEtx9ub/VY9k9kqgohqqYCHaetoX1JQmstV1oXWqmkfSJXad1KuB1A7/jt/tOW2OLkdhMT2uWNon+HiDoR4FdFyXH7rit7k4/expuccmI/h948xqNCFi/QchHNquC5U6RaqOolH2/CJVVyp0hVR17X/S8242z7edutyfqs9yePUeQ4B2+XpGF9re3N6ZJpN7u6yrpXqjHmmmHAlF0x3bBLS7rclNLS/RvUrbyZi3Kr533jut5y27xdk8RIx3O6jbcZB1w7YFpfCWT8sXDFyPFfRO0tnteI2mTvHl4iWDbkR2+OXTLuJB4DXrGA+eRB008Qy8p99N7dye5Hd3Pt8t38iqsp3E3HyCryDIbrVKEja33D8nabcwhKGaKz2eklp6VhpKG220cE6lRPtuM4vacRscfHbGAhtcUREAenifMnqT5rxnKcpu+X3s99vZmeaciST6+nh5DyAA6ALaf6ePa/tz3C7oZ1mG/94umNdrvbZtnf98e4C9WZ40V1umN2CooLbYNu7HdA0/8ApmQ7h5NdaaiYf9NZbYD6kyqSFp6TuvmN3xWzxYOMjGfMbvNHDhB1AlIEyySHjGEQZEebBdx2rxG25TeZM3ImUeK2uI5cpj1IBAjAFj805ERH2kdFl/u52t7at0+zvanvv7XtlHO3C23HfbcLt03X2bps1yTOsboLrjdkxXLsAyyxXTLKmvvVPc7zi2WtU12bL5pXK2jW8ylPqKEcDgt5y+y57N21zO4G7mNtjz4sphGEiJSlCcZCLBhKLx0dixJZdhzux4ve8Li7h4jB+mH6jJgyYxOU4vCMZwnEz+b5oFiNA4cAahamY72f7oZl2lZx3h4VV41lmAbV7j0u3+7GJ2SsrX9xNtqW6Wq23Kwbg5LYF29tgbe3uouCqNFxp33ksVVO6H0tpAUe9zc7sttzuPgNyJ49zmw+5jkQPbyMSJQjJ/zxAcxIDghnXRbfgd7u+Gyc3tjGeDFlpOAc5IOAYyMW/JImoIJ+YMWWs9oulRZa5qvpZCtsKStCx8DrSxottZHxAKHtHER3GTEMkaldPjySxzuFfdG1lOYtu1huzVvt6HC2W2HFNpSQeKSyyfU1A/ecWNR4ExxpjDtyI1ea5MTn3HzWaC7YxbKbWk1NnvprVJ4qYLqx6mnGWR5b7C9eRUNYx93DP5ckW9Vl7W4iHxydUi7Zg5XWqotV0tLX6ohz0lOLSA0ytP5nQ0T6jVSPABJlIOuvDQ7Me1rMThI0/h+C1z3BljOPJH5/4fit/Pp77J7XWe17hd+fdHZTeO2rtarLV+gYNUH0P+8P3G3NNTW7Y7M20uFlNbaKR62vXjIFNOpVTUdG02oEVSQfLd1b/eZJ4u2eGk3L70G0xr7GAMMmU+R1EYaayL/zV6ntXj9thjl7i5aL8ZtGrEuPezS1hjHT5WBlkLsIBj+Zae9zXctu53dbzZbvrvZkTl+zPK6s+hSNas2DEMep1KRYsIw61p0prJieM0EtPSUzSUiVJcXM6txavQcPw2x4Lj8fG8fERwQH2ykfzTkespSOpJ/AMF5/mOX3nN8hk5DezMsszp4CMR+WMR0jGI0AHTzJcnAeg5COzqV1jpoBCqOmghVHU6Qqo6jQQqq6aDkIVR1OghVHUaCLVHU6e2FVHUaf1/tiVVdTpCqjpppCqOo0hUqumghVHKnSFVHTQchCqrqJRyhVHKnQchCqOoGhOgIJ8AARrqfZ98KnxUdVu82GpsaqRNQ4y583TIqEFkrMgUAqRU6UmZIWPDhGvHMZHbwK25McsbWI1CougjZVa3X0htTi0NtoK3FqCUISNVKUToEpHiSYENqeiOTouxW0FVbqhdLW066eobCSppwAEJWmZJ4EghSTEi0xaJBCshKJrLQrq6fdFqo6jQchCqOmg5CFSjlTpCqOolHKFUcpoDFqjpoOQ/CJVHKnSLVHUaDkIlUcpoOUKo6afb2/j4wqjrmlHKNtStblJRy1158R92nKDFHKvq0ZTTfIizZFSm4W4CVp3QLfp0/ujiQtSW/3SkhaescbJt5GXuYTWa5OPcCvt5g8PxXZVZcCdIdbyGop2j8XoKU36qR/CA9TF3h/pamMbbsaGAJ81a7Y6iZAUuZDYbCw7T4tSrcq3UlDl1q0qKtCPFAcAWrTXgAEIB46GAwZcpfMfl8gnv48UWwj5j4lY+dUt5xbrq1OOuKUtxxZmWtaiVKUoniSSY5VWDDouLYkueq+JRyi1KOUlHKFSjlJRyhUo5SUQqjlJRyhUqOUlH2+2sKlHKSjlCqrlJRygxRykohVHKSjlCpRyko5QqUcpKIVRyko18P6oVKOUlHKFSjlJRyhVHKSjlCpRyko5QZHKSjlBio5SUcoVKrlfbSAXmRzfZGn3upGn7dYhBZ0BLhZD3HARW2qn8PTt+unDUaPOt/+THF2cflkfMrlbsm8R5BWhY7s/ZLg1WMalI0RUM6/C+wT8Tauo8Un908Y35MQywqVox5ZY52C2IoaunuNIxW0q/UYqEBaDoJgf3kLH7q0ngR7DHUSxyhKshqu2jMTiJR6FY9zHLksB60WtYU+UqarKtB1S0lQKVsMqB0LpB0Ur90cBx4xy9vtiWyTGngFxNxuW+SB18SsV0TYXW0SNCZ62lSfaTPUNg689dY50g0T8CuFEmw+IWYl2rC75u5g9j3EyeswfA6+6Y7a8wzS3WYZFX4njlbcSxd77S2VVXQ/qCrdSkuFHqp0SmbRcsiuutuMWwy5dpAZN0IyMYk1EpAaB2LOV2cIYM3IY8G6mYbaUoiUgHID9WcOw8HfwGpX9ELZLbn6N30de3bEd7rblW19qosvxe23yx775TUUme70bxtVlGKmmqcMboqOtyOparpz6dvslHT0tMVFLiEqDiz+UuR3Xf8A39y2Tjp480pY5mMsMfkxYtek3Ijp/SmST4eC/UfH7Xsjsni4b6E8QjKAkMsvny5A3WIAJb0gAB4+JXl/v/8A5tnBbPc6u2dtnaveMotLLzrdNmu9Ob02GIrUtqKUv02D4vbsgrBSvgTIL93pnwkidlCtUj2nGfQvczxjJzG+jjmR+TFCzfGcjEOPSJHqvHcj9acXunDwuxnlA/nZJN90ICWh8DYfBYu2r/zdGSKu7LO8PZ9i90sLriU1Vw2p3UrqC829mb43mLJlVgu9Fd3AnwbVcKEE/vxzN59CcBxk8dyExl8smMEH7YyBH+KVxNt9ad3iyAcpxwGI+MJyiR9k4kH/ABgvbnZX6i30nfq9Yidm8pcwbIMgvzHov7B9x+NWnH84DzqQonFXa+oq7VeK1paRLUY/dKh9tQB1SqPnPI9p98dh5/1+EZYYo/6bBIyg39ZmIHpOIC+g8f3R2b3ttxtMvtyyS/0WaIE36/KXIMvH5JWHovwp/Vm2D7R+2vvQ3D2i7Oc9vub7d4yWm8it12cavFv28zha1m8YFj2afMqrMut9l1CVOVDQdpFENGoqzq4n9K9i8lzvMdvYt9z+KOPdT/KRoZw8JygzRMvIHXq0ei/OffHG8LxPO5NpweSU9sOoLERlq8Yyf5hE6O3Vw8iCV554tlOQYHk+NZziNa5bMrwrIbLl2M3FpS0uUWQY3cqW82epBQpKlJauFE2VJ1EydR4GPV7jbY91gntswthyQMZDzEgxH3FeX2e5ntd3j3MC04TEgWdiD1HqOo9V+t365mzOKd4/YD2j/Vs25tjZy64YVh2I7y1VDTBLt/x27MVzFPfriWglhljCc2tdypUqGi1ouraFAhpMvwr6bchn4Dujf9jbs/7uMkp4nP5SGLD+3AxP+CfNfbfqHx2Pnu29j3ltokbgwjDLpqXcAkeFZxMf8IP0X5BZRyj72xXwpyko5QqjlJRygyjlXbgGB5PuhnWGbaYPbHLzmm4OU2LC8TtLU3qXPI8luVPaLNQplSog1Vwq20a6HTWNG63GHZbbJvNya7fFCU5HyjEOT9wXJ2m3zb3dY9ptxbNlnGER5mRAA+8r0v8Aqo7pYvas02u7Edmrm3VbBdiGJJ2xo6uhkZpNxt8rghir3n3YuYpHPkbvcL1fUCnpKoo9RumQ4gKKVmPG9k7DPk2+buXkItyfJT9xj1x4Rpixh9QIx1IfUn0Xse9d9jx5cPb2xkDx+xhTRgJ5X/e5DUsbTcB4ggRDaFeUcoj3LLwzleoHzTux/wBJhpukCrXkvfN3NXKmu1QlZRW1+0/azj1udpqFQ8E2HIsm3oUtY1AeetaSdZI8V7f9598GzSw8bsw3kMm5kdf7Qhh+wSXtbni+zRRxm5DdF+oNNvEfgcmb7TFZv3txOr2Y+hf2e2W6pLFV3V93G9G+1qp3UgPrseEYzgeANrCSPUaaFVZXFgHTVLoVxCgY63js0eR+pO/yQ6bLY4cJ/tTlPJ9+v4Lsd/iycb9PdlCWp3e8z5R/ZjCOIH4OPxBXP9A7dfHsb74R27bkM0t32Z70tu8l7fNwsRuVK1W2zJbjdaOpfwlmrpnwWz8rX1NUARov+cdCDD6n8fmy9u/3vtPl5Dj8sc8JAsYiJFm+I/Yr9NN/ihzZ4nd/Nsd9A4pxYEGwID/CVQNfE+JWj3dd2t1PZz3Nb8bL5ih+rpdpc8vFixVdcUqqckxh2qdfwu/1AlCZ71YF09RLoAlbh14R6Xg+YHcHDbXkNuwOfEJSb+bJvnj9knC8zzfES4LmNzsNw5GHKRHp80XeB+2LH/oWnDlUXah5SFLoaapqC64xSKcDTaVK8ENeqgOemk8AT01j0FGiPGQHivPmZMiegJ8F36h4WirH6He6uoT6QK6ltDtImZY+JsIU86HAkHx4aHwjAQ9yP72AHp1WZn7cv3UiQ3wV6YfgNw3RcsGJYNbai7bj5Hk9kxazWsLU6/fr/ll2pLJZKNvQfCKq5VzSQdD6Y118Y4253A2YnuNwRHaQhKRPhERBJ+4ArlbXb/rMmPBiBO4nkjFh1JlIRDernRb8fUszTHNva7ar6fm09xZqNqOzOwKtGZ3O3qaDG5Pc3mNJbrtvTn9zcpVBi4v2t5mgx2mK0zU4szspHqqEeY7P2ubdwz90b4Eb7fyeAPXHt4OMMA/R9ch87jyXp+791DaTw9ubMj9HsomxizTzz/zsyx8CBjAI0ENNCvLaUco9qxXinKSjlBijlJRygxRyko5QqUcpKIVKOUlEKpYpKIVRyko5QqUcpKOUKlRyko5fb9sGKOUlEKlWxSUfhBijlJRCpRyko5QqUcpKIMliko5QqjlJRCqOUlEKpYrs0TKXq2jZUkKS9V0zSkniFJceQggjkQqMZBok+hViSZAeqzzf7LSqs1WmkttOupaaYLQap2w7oy8wt0NlKQqb0kK6mOswyl7osSzrtMsR7ZrEW+CptNTW/JroKh63uVFto7YzTJNdSusD5w/nS228ELUWko0Khw4+MZm+HHUFpmROh8FgGzSch4CLajxVRcwnGXNdbYhHj/wnnmvwlcGkaxnzjof2LL2MPl+1UK54tYrK7arhTNOU0t4om3XXql51lplayFqX6q1BIPsJPCN2PLlyWjLX5StU8WPHWQ0+YKKnHbflV7vNQuqfDVIaKlZdo1sqaW4KRoupJW06FFCtR8JEBkngxxAAcudfihxxz5ZSJLBhp8FjfIrQ1Zbs/bmnlVDbTbDiXFgJWfWbC5Vy8CUeGo015RzMUjkxiZDFcTLH28hiC4VElHKNjLU5SUcoMVXKSiDJYpKIVKOUlHKFSo5SUcoVKrlJRygxRykoiMliko5RalHKSjlCpSxXPKI2sVqcpKOUGRyko5QYo5SUcoMjlJRygxRykogxRykogyOUlEGRyko5fb+mDI5SUQYo5SUcoMjlJRygyOUlHKDFHKSjlBkcpKOUGKOUlHKDI5USiDI5UyjlCpRyolEGKOVMo5QZHKSjlBkcpKOUGKOUlEGRyko5QYo5SUcoMjldmiS385SBZCUfNMTKUdEpAcSdSTwA4RJA1LdWVifmD9HWZsnxQZHXN1rFzpmg2x6AbKUupP8ANW5MHG3hwM/hpHXYcxwxqYnquwzYjllaMh0VrL2yuvi3W0Cx/pCqb/3WFjzjd+rg7EH8FpO0yeBC7lLjGaWqjqaKgqqRNPVHVaUPpCkkiVRZW82hTKlp4EjTXxiSy7echKQNgshi3MAYxIqVbTmC5Kgn+4Ic466t1dMsnroHNfb7Y3DcYT4/gtJ2+ceH4rltuJ31m50C37XUIabq2HHHNEltCW3AsqUpKyAkSxJ5cZgWIdlYYcwmCQWddnLqOru+ZC2W6kfr7hWvUduoKKlaU/U1dU+uVmnYZbCluOOLc8AOvhxjLZ45SxxjEPInRcflN5t9ljzb3eZIYtnhxmc5yIjGEYgmUpE6AALI2P4NRY/vRtfgPdzX7pbfbYW69Wi05dXWqhTkWU4Zt7ca56ruVRglnv1e1ZQw1VVjjy2mlhpClvOem44ChWvlNtyGx22eWww4/wC9DAyjGfyxnNtLmIJ8Gf0ZwuB2X3f2n33t9vyfFcjDeduGdDkwSEzACXzCpIYhzICTO4IBdj+4/Dc2/wAt59N3DcTcsl42DzK/XvHLPfKPJH8eru5PeLILZeKBi40F3vqqOw5Rc7Cq4Mvh35ZVNbGaZaihLDMsify9uNr9Xe7tzk9yO6x44yIMRIbfFEgsRH5oiTdHeRPmV+tcG4+lva+2x0lt8hIDSMTnn8wf5ixECepDQA8guC8d+v8AlvO9RY273MsuylpduAcpaC77k7B3/Zz5R14FpFTbtx2sVsLFjrApQ9N03FhxB0PARcfa/wBXe3h+q2ctzIAuRDMMr/HGZG3+KVJ9x/S3nD+m3A28ZEEWliONh4n3IgV+JkF+Uf6mXbf2jbT95n/R76cOfZ1vFZqVlurutLTXKkyu24VnAqEVLeP7ebkWmpRX5TQ2anBW/VVA9ageQEGrqCC4PvXYe77n5zho5e49tDHvJlogRMZTg35p4zpEnyGhGrR6L89fU/kOyOw82fltvyMMHB7eNsuXJOIx45P+WMx+bVgAzmXygyK8vbtbLnaLpcLVfKOut95t9W9TXSiubbzVwp61CyH26xFR/OD8/iVak+OsexljlA0kCCNG6MvK7HkNnymzxcnx2bHuNhuICePJCQlCcZaiUZDQgqnyiJUrlOV+136bxf7n/wDLf92Oyl0X85U7dq3fxS10ZcK3KWx41SYfuVZ32ptQwP1A1ZSBpopBPtj8493xHC/VzY8hANDMMUj6mRnjP4Mv0V2tky8t9Lt7tJF82I5G9ABDMD9hJ+5fidp1h2nYd0H81lpz/bQlX9Mfo5l+eMkTDJKH9GRH3FbadpXY93P98ubXPAe2Pau67iXmw0CLlkdxNbbcfxTGaN4rFIvIctyCqt9gtT9etpSaZhx8PVBSZEHSOi57uPhe2dtHdcznjixyLRDGUpHxrGIMi3iw08V3XBducz3HnO34nCcsohyXAER6ykQA/g5D+DsViPevZTc3t13TzPZbeTFKvCtysAuztmyfHax6lq1UlUj42n6WvoHqm33O21zCkvU1VTuOMVLC0uNqUlQMc/jeQ2fL7HHyPHzGTZ5YvGQfUeoOoI6EEODoV1/I8fveK3mTYb+Bx7rHIxkC3UeRDgghiCCxBcLfn6ZFBRbVVXcd325C0hu2dnO0VfcttnatoKpK/uO3SX/082Vbp/hK3a/Ecsv9JfilGiks0RWSANY8t3lKW+/R9s4vzchnAyN1GDH+8zP6ShEw+Ml6vs+A2MN13JlGmywGWNx/pp/u8JB/qzkZkeAg+i8va6sqrpXV90uDq6i4XSurLnX1DilOOP1twqXKuqdWtwqWtS33lHUkmPawxjHAY4BoRAAHoOi8VkyzyTOSZeRLldNwpQhayOCEKWfuSCf6IyqsYuSIjqSvXfuz23yDN7b9Jzs6w+kqa7Ibz2ube17zFvbU86M0343g3Cp73WJpgCoO23GMfoH3SQdWmJvAR8/4LdY9sed57cH91HezDnT5MOKDa+sjL7SvoXNbfLu8nC8VgHzy2gmwctPLlmWIHhpAE/ey2b/zCmXYxiO9XbX2RYFV0b+IdlvbvhuE3ZihSWmWtyMhoxcckqjT8PSdu9iFrrXFEBanKhRIA0jqfpRts2443edyboEZ+R3c5h/9XEtEfZKw+xc/6obrFtt/te3NqR7Gw2kIED+nL5pfeBA/avHbtGz+p2o7qe3LcuhfcpKzCN5sDv1NUsqKHWHae907QcQoEFKgHzx1j3/P7Mb7hN3s5B45NvMf9UrwvAb2ew5jb7mBaUc0D904n+Je5f8AmjsMp8Y+oHjWQU7SWnNzdnsZyS4LSmU1T+NJThlMt1X/AGhaprSlI5AR81+im4OftaeE6+zuJRHoJfP+2S+i/WTbez3JHcDT3cMCfUh4j8Ir83Vuttwu9wobTaLdXXa7XOrYoLZa7ZSVFwuVxrqp1LNLRUFDStvVVZV1LywhtttKlrUQACTH1+co44HJkIjjiHJJYADqST0AXySEcmWYx4wZZJFgBqST0ACuTO9vM82tyaswvcvCcp2+y+3M0tRXYvmViuOOX6kp65v16Koftl0p6arRT1jPxNOSyOJ4pJjTtd1td9hG52WSGXbnpKEhKOnXUaaLdutru9llODd454sw6iUTE/cQD4r0d+lpa6HAc43l7xcnpw5i/aZtddcixtp3Qs3vfLO5cH2nsVM2OLl2s9TfKvJmBw0Tj6la8I8j3tKW62+37fwn9/v84jJvDDD58pPoQBjP/iL1nZsBtc+fn84/cbLAZR/rZZ/JjA9QScn/AMNeZOQXC63nIcgvd/rHLlfr3fbze75cXlTPXC83i5VVyulc6ok6uVddVOOK6qj2WHFDFhhiwhsUYgAeQAYD7AvI7jPl3GeWbMXyyOp8yqRKOUbGWlyko5QZHKSjlBkcr0V7euwqyZptPb+5Duv3/sHZv235PfH8U20zbI8EyLc7cHeLJqVZauDW1W0OMVtqyDJsbsTgP6leV1NNb6UJVKtxYCD5Hle6Mm3354fg9rLkOXhG2SEZxxwxR8PcyyBjGUv5sGMj5Aar13F9rjPsRy3NbmOw4qRaMzCWWeQu3yY4kExB/NJxGPRzLRandxOyeQdt+++7eweWV1BdMj2ize7YTdrnaw8m3XJ+2+i41X0bb4D7TNXS1Lbki/ibKik6kax3vEcji5jjMHKYAY4c+MTAPUP4FvJdJzHGZ+G5DLx24MZZcczFx0LSIfXzZYblHKOxqV1jlJRy+3ugyOUlHKDFHKSjlCpRyko5QYo5USj7fdpCqOUlEKo5UyjlCpRyko+39sKlHKiUcvfBkcqZRygyOUlHKFUcpKOUGRyqpY2g5ebUjTX+/wBKr/YeQv8A8mMMoIxy+BWeIn3I/ELLl8zVmy3R63qty6gNJQovIqQ2SpepKQ2WVcE6eM37I6/FtTlhd2XPy7r250Z10U7k20kerbq8e34F07unM/G61qBGf6GfgQsf10fEFdtG4VgXpO3Xta+006Ff+ieXGJ2WX0WQ3mP1XYVmWJ1jS2Kl9S2XBK4zUUNQttafDRQLakmMf0u4iXiNfir+qwyDE6fBVC03nGVlq32moo2yon0aRltTEx8TKgoTqfv4xjkw5x8+QFZQzYj8mMhYhzMhzJLif4S03y09NEvlHP24PshcDcS/fSVsSjlG9lpcpKOUGRyko5QqUcpKOX2/ZCpRyko5QYo5SUcoMUcpKOUGRyko5QZHKSjlBkcpKOUGRyuWURmxWtykogxR1Mo+39sGKOVEogxRykogxRykogxR0lEGKOUlEGKOUlEGKOVMogxRyolEGKOkogxSySiDFHKSiKxSymURGKOUlEGKOkogxRyolEGKOUlEGKOkogxRyplEGKOVEogxRykogxSxSUQYo6SiDFLFJRBijr7SVI/ItxHs+Fa06fdKRpBierJY+C50VlY3+Ssq0/dVP/8A6zThEoD1A+5X3JjoSu63fb21p6d3uCNPDSpc4fiTGJwwPWMfuWQzZR0kV3W8tyNvwu1Uv/zpQ5/vpOsY/p8R/mhZfqcw/nFdxvOckb8axlwcnKOmPmGwrzjE7XD4D8Vl+rz+f4Bem/Yhs5SLpbn3H562z6qU3AYi5VtITT2632+nK7zlLbRBbmLLZaplp0UgNvDiVR6rgdjDFj/V5AwH5X8B4y/k+1fz8/5vPqryG95HH9I+3ZylPJLGd5QscmTJIDDtdOgciWSJ0NsZ8FpV3Qb8Vm/G4lRdWAlnEceVUWjDqYJT6i7a28ue6VDwEzr10d1dT4BLJQnSYEnrOS3kt7uLD/NR0j/L9v7F+kfoV9KNv9J+zYbDK8u4d4I5t5JywyGIbFEeAxBonq8xIuxAGOdlaLZqo3Swmk39rs+suzNXfqZrcO7bU0liq8/ttieWE1dxxyiyNp2y1tdTgzFt5Kp066Aq0B6LkTyMdjllxQwy5ARPtjLYQMvASMdQD6L71x8tjk3uMctLKNhYCZxsZiP9W2jj1WzffZi/09cRzrHLD9PrcPuB3YwxqzCozXON7qOxWigqrxUCZi2YZZqbAsEyQM0bRAq37jTISXwpDCFNhLium7Yzd27jazy914dpg3NvkhhMiQPOZvOOvgInp110Xb9yY+1dvuI4+18u6z4BH5p5WAMvKMaQkwHUkDXo41WJO2Hfb/u+56q/O2WjuuP3ujasuQtppWzd6K1l9DqquxVOkzNSypOqmeCKhALZKZph7Xjt4djnuQ+M6HzbzH8NV+cvrn9KB9Xu0hxGPc5Nvy+1yHNtzY+zPLUimePQxl0E+uMtMAsx9Ee7/YrG97dvqXfrbMUtfkNHYW73UVdtCfTzLEm2C+688GwPUulnp0qcS4rVZbbW3oVKBjveV2UN3gG927HIIv8A2o/yhfjn/lz+qvNfTDvDJ9Ju9/cw8Pl3ZwRhk67PdmVQA/TFmkRExGglKM3ABXjEACNdDx9hBBHQg8QR7QY8mxX9LSSNF+yL/Lm3v9R+n19TfD3/AOZSWjFr9dQypWqAcj2q3CYdIR+6VCxp4+0jpH56+ruL2+6+F3A/NLIB/i5cZ/7S+/8A0pzHL2vzGCX5Y4yfvxTH/ZX5D9ltrsz3szvbHaHbq01F9zrcm94/iOMWumbLrtRc7t6TfrLSNP7tQUyXKl88AlllZ9kfed/vdvxuxy8hvJCO2wwMpH0A/b4BfFsWx3HJ8wdjtYmWbJmIAHrJh+JGvh1Oi/X/APTj7xaDtn+oJsN9LTs/pcRuewmI1eVYz3O7nN2ahueQ9wu/tFa0s55ndJklQwLhasPwHIKV20WNqnUk1FDRIW6460tCU/Au7+38nM9q7nvbuAzHKZBGW3xuRHBgJ+SFehnOJtN+kpMGIK+3dp87Dh+5Nv2dwYiePxzruMjB8+apvN2cRhIVxsQ8Q5cELyS/zGiqJf1b+4D5VKQ6jDNi016kq1S5Vf8ASLFCkngAlaKcpSdCeI48eEe++kIl/wAB7V/9Zmb4e7P+N14X6sn/APdNw35a4/v9nG/4MsA9wQHb/wDTh7R9gWFGhy7ueyrJO87ddqncUhx3GKdiqwbYe2XLUpcqKS54bXIu7LRT6SXUpWNVAGOz4kHle8d/yh12+yhHaYj/AFtJ5iPIifyE9W06Lg8sf7s7Q2fGdM+6l+oyBy4GscX+CYiUm6AyB1OqxRiX0vvqEZzgtDuRi3aHvTcsSu1sVerHUrxk2+7ZHZ0sGrVc8bxi51FHkt+ok0qS5PS0joKOI1Edhn717U226Ozz8hto54liLOIno0pB4xL+ZC6vD2Z3TuNuN1i2Wc4ZRcGpcjzjH80h6xBC0PvNFV25N2oK+kqaG4W/5+irqGsYdpayiraT1WKmkq6Z5Db9PU077akLQoBSVAgx6aJE4icCDEhwRqCD0IXnsUZQ3UYTDSGQAjyL6r9i/Z9ttgFu77Ml78t6XF2zt6+nL2U7LXL9acaWddxMhwisONUtjQsJp66op6S53Zr0gpIRWVlPMpsLmj8/dw7vdS7Zh2xx3zcty/JZhX/u4z+Yy8ukdfISX3jgNvt4dxT7i37x4viuPwl/OcofKAOh8dP6VenVeR+Wbfds/wBVTuT3Uu/b7u5vvgndfvJkeT5zhGBd2rW3VyxXeu7KS7V0e3OI7k7aotP+BcwprHSN01ott3ttdTVLFOlo3H1EFS/ebfd812Nw2DHy232uXg9vCMJz2vuCWEdDknjyPeD6ylGUSHejLxO52nE978tnycVnz4+ZyyMow3FDHKfCEJwasvCIMTE6C4cP5KpxfJcF3TocOy2y3LGMvxHcm1Y7keP3anXSXWxZBY8tpbbdbXXU6/iZq6C4UzjSxxEyToSNDHvJZsW62B3G3kJ7fJiMoyHSUZRcEehC8Nhw5tryP6bPExz45mMgeokHBB9QV+nP/NhU6Kvu27bKtgBTtt7d7gxcgkcWfmNxby9Tz+0zB0cfYI+MfQpxwO8B6Hd6f/xx/kX1762yP987WI/+W1/x5N/lLUv6Y97wT6f2V9qG/OcbY2Hdbug7u908PxnYrB8sU+mg2b7erxllBi+Tb2uUlOlurb3D3D+dVTYp6ihT/ptQK/R1KfTX3neePdd14d9xe2zSwcLsME5Zpw65c8YGUcL9PbgwOVtbCmnUdR2hLb9rz2XJbjDHNy+9zwjihN2x4pSrLKzA+5LUY3+UAGYJcA5e/wA0r+iL7+dvH6OmpmL07sLYl39xtDaal1JulQbSmsdTotz5enLqW5vAE6eMcH6JDL/wvlEifbG6lXy6B2XK+sxxf8RY6ge8dvB/P+cy0Sz2nTsD2FdsuzAAocm7gay6d5+7aD/d6h/GxT1+FdtlHUt8HVpYstwy5biHCAVOtEJ8DHp9oTyvc+85I64dqBtMXpLSe4/EYh9hXnOQA4rt3acbFhl3BO5ysdDEA48B9QR7svtC8mX1+vUPv/8AOeedGvJxxSx5Kj3giQAPReBMiST5lcMoisUcpKIMUcreH6c3Z7dO+bu92p2AYeftuK3S4u5NuhkTK0snG9scWSLrmFzaqXR8s3cTa2VopEOFIeqCEA6mPM93dwQ7Y4DPyp+bOBXHH+lklpAebP18g69N2lwGTuTm8PHAtgMnyH+jjjrMh/KIYepHmvQfcbd7AO/L6oNkv6aelxnsJ7JrWXcXx+3MBjFMW7V+3Rv9arKG20b6/lLdkG8iMbFLT05WtVVeK1pludSkpjye047d9r9mSxuZ908kWlI/mludxoCT1McVnJ8IRJLL1m85Hb9yd1wkPk7Y2BcRANRt9uHYPoJZANA/zSyAa6Lx57ht3rx3B777v745AhDd43W3AyHNKxtsqKGhc6oijaBWAo+nQMtA6gHUR9B4jjocTxmDjcX5MOKMB9g/lXgOY5PLy3JZd/l/PknKX+MTJvsdlh2Ufb+yOxYrrHSUQYo5UyiDFHKiUQYo5SUQYo5SUQYo5SUQYo5SURWKWSURGKOUlEGKWKSiDFHKSiDFHSUQYo5VxYo0HMhtidNf5ylaf6ja16/sljVncYpFbcB/fRC5czUF5Ldj/wAt8N/7CAdNdP8ASibeJGGPwV3Ej70llfErLQUVmo3hTsuVNWyH6h9xtDillZOiAVpMraUj8o9uscHcTnLIQ+gK52CMY4wfEqHMIsTtzNxWwShQmVQCVNGp7X/ilsAHj/DrKTx0gNxlGOj/AG+KHBjM7n7vBUnMsZtYtD9wo6RmjqaIJcJYT6SHWtdFtuIT8Kjp+U+IjPb5snuCEi4K17jHH2zOIAIViYQ0HMloRprK3Uuf/VtFXlHK3IPslcbbEnMFT8mIcyG8K8dK+oQD0QsgRnhBGKI9FhmkTll8VRJRGxitblJRBijlRKIMUsUlEGKOUlH2+3hBijpLBijpKIMUskogxRykogxR1MogxRyuWWNv2LXZRL9usEsplifYlklgyWUS/bX+yGiWSWH2JZJYqWSXrBLJLEb0SySxeqWUywb0SyiXh18onT4JZTL9vD+uKyWUSw+xLKZft9hDw9EsolgyWUyxPsSySxUsksRksolislklh9iWUyxPiEsolh9iWUyxfsSyiWIllMog3mllEsVLKZREb0SySwb0SySwSy56WgeudXR2ymMtTc6ylt1MdNZaivqG6Rk6DQnR14RkIkkR8SW+9atxu8Wx2+TfZw+DBjnkl/ZxxM5fgCvcDuhuDey3aFSYhj//AKuVX2rFsAphTn03aVVypXbpdn2ZJSj1KmjeCjyeIPjHrOQkNpxntQ0cCP36lfy4+hOzn9TP+YrJ3Fy/74Yc+638raiXtzGLFEv1aM4EesH8F4bhsJAA4AAAAcAAOAH7BHkmbwX9TTMkueqSwUspliN6JZJYv7EsvW76cW6L9wtGW7QXd75tqxD/ABTjbFSQ423aa2oaorzbylyb1Wl1lS0pLY+FDYUNNDHpOC3FoS2sukdR8PEL+en/ADodiYtpyPH/AFF4+Ptz3X+67mUdCcsImeHJp0IhGQMupkxd1oR3M7bNbWb3ZzitI2pm0ruAv1gQoaH9GvZcqKYkeGnzCHgNOGg4R0+/2/6fdzxgfK7j4FfrT6Id6z79+l/Fc/uCJcgMPsZyP9dgaMv+qYddXX6Of8vjlTWJ9kn1g7tUn02LXtHiC/VUr4J7tgu+FtQAkgJSpLjieOup1HKPz99Wduc/cvb2OI1luJ/hPCV+xfpZuBh7c5+Z6RwQ/GGYLyr7G209pPaZvn9QW5JZpdybnRPdqvZ4mpQyupY3Oy+x01VuzupbqSqQC5/0twq4W9lipaJ0evTgBCkGPbdyf/fub2naeNztItud2zt7cC2LEW/1kwSQfCHqvLcFOPC8fvu6M2m4nklg23R7yBOXJFx1x4yAPAynqNFsj/lqsep719VPAqqtQKtGPbN7x3ptVQtTjv6gLbZqKlqitZK3H0Lq1rKiSSridY6j6y5JYux8gjpbPij9jk/xLsvpBTN3eLj5xjnMeWkZA/jIMFg76nVDkHdP9ZLfbAcb1rMlzTuJtWyNibIW6V1+P3ClwKjYCEmdYR+mhISOPDSO07Klh4P6ebXd5tMOPaHNL4SByH9q6jvEZub783W0wa5Zbv2h8QY4x+IC9dsLw/t7t+9nfn9ULeLFbVnmwH06W8W7VezfbW/sB7E82zXae1UO3+1d1uFL6SqCrtNPc1UdOhEoVT17oqdFlooPz/cZ+Xnx/F9k8fOWLleXtud3kifmhDKTkygauCQ7+cQ3Quvf4cXEQ3XI9372EM3GcU2DbQILSnjAx4gXDGIqCGLAzc6xIX5467uC7ze8/vEw7cWm3J3Gy3ua3C3RxlnbN+yXi70zuL5C/fqQYpasOtFpdaoMZxzGXfTUGaVlqlYpmVrcEgWY+uR4jt3tzt7JtJYcOPhsWCXuOB80amxmTrKUtXJJJJXyn++e4e4ufx7qGTJPlcmUUI6xJIqI6GoGjABo/esq/VnoMez36l/crYtn2qLJrhk24mN4ktGLGkqbZk+7lfjmNY7k6MecoFfI1KLtm7i2gprRC6lSz46mOv7Ell2vZe0y794RhhlL5nBjjtKUbPrpBvsXZd5e3uO9c0Nm2QnNEfIxBkGvVj4zv5OfiF7LfW537tvav2qbKfTY29pW7NuPulhW3O8fd7XtOqNxcqKLE7LjmP4ZVOAqQ5R3K5Y2+9UU6wk0/wCnU60AB9ZPz36a8TPnuc3PeO8NtnhyZMO1HgxlKUpt5gSAB8bSB6L3H1D5aPAcNte1doDHe5scM25Pi4jGMYP8YuR4Vj/SX5g9nckvuF7v7TZbi9wq7XkeO7m4Hc7NcKF1bFZTVzOT2tKDTutkLQt1Dimzp4pWR7Y+1clgxbjj8+DMAcU8MwQfIxK+PcPny4OSxZcJIyRmCGJBcajp6gFe6/169n7Xj31b8UulmtLFqrN+bXsXuDfbQw0ltunyKofs+N3UuU6QyE1VVUWFTz5OinX1rWozKJj5h9Ld/PN2FPFkkZR2ss8Iy/qh5D7BZh6L6T9StjDB3xHNji0tzHDKQHnIUP31c+f2rcX6zmOUXc39ZyxbGZDcv0zbHbLYqw5RvXkoAWjb3aSx2FnczcLL30lJSpNqxl9TwbJBc10T8Rjz306zS4X6dS5PFG29z7mUcMf6eWUvbxw+2Wi776gbccz36NjmlXZYNvE5Zf0MQicmSf8Agx1PxHmF549odQ73o/WR7btx2GaGiwek3nwa6bf4PSuIVb9vNrdlKiluW3239pbSXGlWTFrFZGqZpJMxbT8Ux4x67uCA7c+nm92knO5O2mMkz1yZczieQ+spSJK8nwWaXcXfuz3EWG1G5jKEPCGPEQYQH9WMYxA+CzB9ZPb68d5v1rbxtVZK4ULF7vuxvbvTVDTgdet1Qusast7uSG1atU6Leq+uVT61mVtDZUQQmOu+nm5x9u/Tcb7KHEYZs7eehkB9rADzXZ9/Y5c79QRsMDDJ72LC+h60H3R+Yl+jn1Vk5b291v1Le7vufy6yZxYtheyDtQo7Zie42++R0T9fj23m2W2FtGO2TEcXtKKiiXk+WZHerZcXbbQodZS+68SCVSNO8rbcvDszt/ZbfJiluu5t+TLHgiWlky5DaUpFjWMQYiR1ZviRw9zxGTu3nt5njkjte3NlECeaQ0x4sYaMYx0MpTIMoRcWdx1D6yYTtf8ATl7lNyldsnbvifdLh+aZDQZenaHuN3S3Jwi/23M8kxDFb5l6KfcLYvH9ubQjEMUyq245VIpXLdkFTcKF1yn+YStJek7ncb3u/htmOa5fJscm3iYe7t8eOcTCM5CP7vNLIbyiZAkSxiMgCzFl1e22PavMbyXC8Tj3kMwjIwz5MkCJGIdp4hjBjE9AY5JGJZ3Dled+y+x+6/cJulimym0OF3TNdz8yuv6PZ8XtIbdc+ZaXJX1dbWlQoqCz2oArqax5aKdlsTKVxGvruR5PYcRsZ8lyOSOLZY4uZH8AB1JPgBqV5Hj+L33K76PH8djll3EpMAP2nwAbXU6ByehXpVvH2M9inadcFbW9y/fTl+R9xVChFPnWC9rGxlv3WwjaK9LSC7Y8sz/K9xsDosmvFsWZa2ktqGn6dwKRoogFXjuO7n7n56P63heLxw4g6wnucxxTyj+lGEMczEHwMnfqvXch2z25wf8AunMclOXK9Jw2+IZIYj5SnPJCxHSVRodFvrsPtriv04vpfd6/ePgG7uK7uXju4o7F2zdsW5eGt3fHLwcRyB42vN6244lf6di+YTnuGZBU17NxoVOPobct4U0/UsqaWryfKb3N3h3txvb+6289vj2BO43OObSFo6wAlE1nCYYg6aS1ALget4zZYu0+zd/zO2zwzz3sfZ284vEmMh+8NT80JgBiHIeLgkEFeQ289Ke1rYSzds1M6aXevepjHd0e6hberdwxXHGnqfIdp9gLiQEuNVdrqEUt/wAioXkN1VtyGmTTq1QkiPoPGn+/OWlzRD8btjLHtvKUtY5M49DrDHIaSgbeK8DyRHCcRHhxpyG4EZ5/OI0lDEdBqPlnOMgJRm0X+VaDSx6z7F5CyS/b+yIlkli/YlkliaJZTLBLJLBLJLF+xLKJYmiWSWL9iWUywSyiX7fYQ/Ylklh9iWUyxEsksX1Syu3BmZ8loD4hCalX40zqR5qjj7n/ADJ+z9q37Y/vgfj+xUzJD6uQXhwn89c4eHhwSgf0RnhDYo/BYZpPll8VkDA8hDiG7FVrAcbCv09xR0C29SpVMT4TpJJTzGvKOLusOvuRHx/lXL2ufT2pfYsny9f6I4K5tliXN8nZqW12W3rDiJwK6pSdUEtk/wB3aI4KAP5j4eweGsdht9uYn3JjXwC4G53AIOKPTx/kVH2+amyFK/8AlUdWdeU7Skxs3f8AmmHmFr2p/ev6FWvdj6l1uTmus9bUK/FwnrrG7GGgA3gtM5PMn1VPljP7FhZTLD7EsksG8wllEv28P64iWSSK3ollMsRLJKIdfBLJLBLKJYqWUywSy+4ycLFIOiQcIkESCJBEg4RIOiQcIkHRIIkHRIIkHCJBEg6JB0SDokHCJBwiQcIkHCJB0SCJBEgiQRIOESDhEgiuzACgbgYAXdPTGeYWXNfD0xk1qK9ekuusbMLe9D+3H9oXn+7rf8I8vT8/907xvj+myt+K9fvqSUzrm0NgrGQTR0+4VtaWU/lSamhuyqXXThpIyrQx6bnB/u0T4X/iK/nR/wAlWfHD6i7zbZP/APTPh8hH+DkxCX4yC8VY8qv6bpBwiQcIkEW43YfXv0ncbjNM0pQaulqyGjqgk6BbLNnrq5CVcwH6ZJ++Oz4iTb6IHiD+wr83/wDNftMW4+jG+z5AL7fcbecfQnNCBb/BkVk/6kVrZp908LvCUAP3bDjSvOcNVos9U0hhJ9uiBXK0++ORzkQNxCXnH9n/ALV4X/kr32TN2HyfHSP7rb8laI8jmiTL76BeiX0qKnI6T6Wn1e7diDRfzLcFntq2twakaClv3PLM+yvKsTt9C2hoKdOn6yXF8NA0havAGPgvffsy747flnP7jD+pyzPgIwhGRP4fev6S9imcezedhAE5Mp22OIj1JnOUWHqx8dFo99UXMMexTO9p+yDbWubqNrOxXb2i2pddpHmn6HJd8r2BlO++ZF1sqU/W/wCMru5YStR1+XsTKfBIj03Y+2yZ9rn7l3YbfcnlOUP1jgj8mCHwoL/GZXnO993HFu8Xb21k+04/F7ZIZp5pG+eZ9bmnwgFth/lo7wxbPqpYXSVDjbSb3snvLbmVOKSgKqm7fY65llBVpM48mmWABxJGkdB9aIGfZGQx/m7jET95H8a7/wCjlY93RnItI4ZxH2xJ/wCyFXdubZ/hj61H1DO4fIqRDtn7QM274O5v16ttt5DOS4Jfcyu22DDbbvwuVFyyFtltgfmm/LqeEa95k976ccRxGE/PyGLZ7bT+jkjAZD8BF3W3Z4hh7+5XlssXGxzbvOD4WxyyGA+JkYMtwth9ybPYv8tZnuVVO1G3PcL+l90GQ1e723+5gy2ooqunyTe6iVbcirKzBslxPLLfdrRT11HV0tYzXtrp1N+qdUJUI85yeyyZfrJiwe/m2ltmBiyY6aVxF4gTjOBBYggx16dV6TZb7Fi+k2TcQw4twI5x7sJ3Y2MASTjlCQOvUS01JcOvBy09+eQ7W23I6LtT2Q2Z7TLrltprrBfdxdtG9wM03jXj90ZVT3Sy2DdfefONx8vwegulOtTVR+hP21x1pSm1KKFKB+q5O1cG9nCfO7ncb+GOQlHHk9uGKw1BliwwxxmQdRcSAOrL5VHuvPtMc8fC7fb7E5AQZ47yyMQxEZ5JzlBxoTjqT4lZo+nbT4p23U+UfUn3zxqlyjFNmbhcse7csNyP1HU73d1N4oqv9GSzT1RSLzi22FE9VXy/Vky001a1RMFSKiqZUOs7ulm5k4+zeLmYZ9wBLcTj/odtE/NqOkspaEB4gyOoiV2PakcXEX7v5SAnhwfLgjJz7u4I+Xw1hjj885B2+SOhkFbXfJm24Hd7tbsp365VcP8AFOTXC112wvcXeaSlbaNj3ZxbIb5lmNX2+0tKhLFhsWc4jmjVusM4SioRjNUlKlLaWY3dsbfZ9vb3c9rYh7eEEZ8AfQ4pREJRiTrIwnAyn5e5HwIWHc+bddw7bb9x65MhthzHQyE4kzgZCOgvjkBAAa+3IdQshfRW7Dsq73u8zbypqMfq6jYnZbIbXuNvJljzD7dgYpbE9+oWLEG7sGl0qciyG6U6FstfFIxTuLcCUyk8T6k91bftvtzLGMx/ee4iceKIYlzoZN/RiDqfMgLlfTrtbcc9z2OeSEhx+CQnkkXAABdnbrJmAPUEnwK2b7998rF3o/XixFrCq5i+YXjm++1eyWLVrKvWZrl4VWUFsytxpaVqZepTmlLcfRW2S24yErBOup6XtXjMnbf0umdyK7me1y5pDyuCY/bSr+RXcdy8hDuL6lCO3NttDcYsUfWkoxkfgZGTNoQxWU/rBbppwnMO+HdNupk3O73d9bh264lUBUldaO1/tJuR25za629xQDjbGc7l4hXY7VpT/Lqba0PiKVaRwPp9sBucHGbFv9z43ajPPyO43Q9yAI/qY5iYPUSK7DvrkpbefJbsyH6jfbqeLH4EYcFcU5AgO88mOpBLGEC3Uvpf/l/Upc+rH2xsrJ9JTO6Cy3MUoU41ttkbjSinUJUpLiQRr7RHpfqwf/0Tekdf3f8AtIrzP0rA/wCM9o/9Kf8Assn8gWyeRXAW/wCq59Wjuuvrjjto7WKHuNzGyv1TrhoKPNtwmWtgdq/RLuiTXWbM9xqCvpm06LnpZxwTrHSYI37G4Lgcf+c30tvAt1pj/f5fsMMcgT6su93dcfePL89Nq7THuZj+3OUsGI/ESyRb+z6LPeeWXt+2v+g12c7Wbn7s7m7L1felm957ic13Ewfbem3Ms+S5NTOXJdTiG5dop8sxnI38ZqX62lradVF82umrKJJ9AjWOp2WXld99T+Q5DZbfDuY8dijghjnkOMxiWN8ZrKNvzAuziXVdzy2HjeP7B2fHbnc59ud7uJZTljATEvbeMYZPnhKrCJBDsYjQryQwDebtS7L7ZluX9vWT553Gd0N+xPIMMwbdXKtvEbTbSbFW3MLZU2LJ8rxPGbvfsgzbPdx6rHaupt9NUVzFnt9BT1r6g2+4pJb+g7vj+d7knjwctDFs+DhkjOeKOT3cucwIlGM5CMYQxiQEiImcpEDUB3+c7XkOE7bhky8ZPJu+ZnAwjkMBjx4RIESlAGUpTyGJMQSIxiCS0izemf0+8Tvf0+fpDd1/1JBZF2LerfRNNsD205TdbczUVdjxa4POWyszCxPVaC7Ts3+/Gvp3VNlLjrtmaVqAlOviu7Nxi7s7/wBh2aJ343avn3EQS0pDUQl51jU/CZC9r2thn2x2Vvu7s0ab/dTGHDJg8YyLSnE+AJsPjAL8zr71bcKp+oqamquNzuNU6/U1ta+5VV9xuVe+px+rrKp5S3qqtrat4rddWStxxRUokkx9piIY4iMQIwiNANAAPAD0HgvjM55M+U5JkyySLnxP4r9YPezbtv8Ash7DfpfY5ndLjV8rdtdnavfzbTZGu+Vr3c47qt2GHb1W5JuVjroKndqtp3L8quqE1CTTX2sSu3pW2skj4N23Pddzd083m2xnGObcjBkzBxTbYvlEccv9blEWHjANNjovuXPx2/bfa/D4Nz7cva2pzQxSAkZ7jIRK04kE+3jJB8pl4OAZL8yrmK7172tbwb5O2PM9w6XGK1jMd69zHmai5Utlr80vrVuo7tl19qFyIrr/AH2vbaaSpannXHBokjjH2sbjjOMO34sSx4ZTFMOMMCRCLkRiPCMR8AvjctvyfJRz8pKOTLCHz5chcgXkzyP9aR+0usRR2DrrVMESDokESDokHCJBwiQcIkHCJBwiQRIIkESCK+tvETZBMfBqkcWTyBUEf+VHF3ZAxN6rk7QfvX9F1rpimRuV9a+m2POJeqXXEKaU25MlSvhIlWfERYbjAIgW8FJ4MxkTXxVJNhyGkWl39KuLLjSwttxNOvVC0EFKklOvgY2e9hlpaLLD2swL1Lq7btll/qbYihFsraKpW36dbWJZeK3kgAH0Qlr+SXR+c6k6+GgjRjwYYzvYGPgFuyZ8phRiJeJWOlNOo4KadT/rNOJ96RHLtHzXEY+qv7bhIVdq9f8AyrfqeI4TuFPGOJvCPbA9Vy9mPnJ9FYT6/Uedc/jcWr8VExyxoFxDqXXHFcIkHRIOiQdEg6JBwiQcIkESCJBwi4tTGDhbGCjUxHRgmpiujJqYjowTU/jFdGTUwcIwTUxHRk1MV0ZNTEcIyamDoymYwceKjBXFj+OVt/eUGVBilaID9U4kqSknj6baQQXXSPZqAPaRGrNnjiDnUrdiwHKdNI+av9W21v8AS0Rcq4PacFqRTqa109rIQlenD+OOJ+un/RDfauV+ig3UusbXuy1tiq/lasJUlaStioRr6b7epBKSeKVA+KTxB5xzMeaOWNoriZMUsUml9i7dlxe7XxJepkNs0oUUmqqVKbaUofmS2EoW46QfHQaDnEybiGLSWsvIK48E8usfy+aqlwwK90TKn2lU9elCSpaKZTgeSkcSUtuobK/uSSeka4bzFIsQR8VsntMkQ4YhWQdQSDqCCQQfEEcCCD4EGOTZcZlGpiujKdTB0YJqYOEYKNTB0ZNTEdGTUwdGCamDoyamK6MmpiOjJqYOjJqecHRgmpg6Mmp5mK6MFzU9ZUW+ppbhSH+92+qpq+l5fM0T6Kqn8PAes0ICRibDqFqzbbDu8GTZ7j//AD5scscv7M4mMvwJXv8A7k2Km7k+052ssZTUv3nBqDKbLVK0LTN8x6kJr1OKA0SoGlrGwCQdVCPaZ4Df8a8NSYAj4j+BX8i+yeVzfRT/AJgY7blfkxbblcm1zR8Tg3E/3YA/wsMj16Ffn5SpRSJklKtPiQdNUKHBST7NUq4R4t1/XogA6Fx4Hz9VOpiWUZNTB0YJqYOjL0G+nRhdZfN3L7l/y7irdh2OPITUpQpTYu93dbo2qQkAyuLtz7zg5hBju+DxnJuTk/mwj+J/6F+QP+czubbcX9PNp27eI3nJb2Jq7H2cIMzP1AyRhH4ldX6iuSs3Pe21Y6w4HP8ACuIUCKkp00Zrbu6t6opVjxDrCaJBUD/EInO5BLdjGP5sR+K3/wDJrwmTY/TDcczlDfr+RyGPrDCBGMh6SuW+BXsL9HrNsa2A+mX3090OXSLpNod1tv8AMsUtbym5Mo3BsmGbh4ph9lbpnlpbuDtBetxGrqlsBSkLtyVlJCSpH55+oWDNyvefGcLg/NuMGSEj/RxmeOU5ejxxmP8AhN8f6QdhZcfG9pcjy2Vv3ObFKALC2QQnGA100OQS9Kv6r8yN6vt7ye9XnJ8kr3rrkmTXe6ZHkV0qFTP3O/X2vqLrebi8r2vV1yq3XVH+JZj7Rhx4sGKODDERwwiIxA6AAMB9gXx3c5p7ncTz5CZTnIkk9T6npr5rfH6VW9tH2+fUO7UNy7vcm7RjNLupZcey+4OqCUU2K5Qo2i7OKKltp+EOtnRRCT4R5fvrjpct2nvtnjjbMcEjEf1o/MP2L0/Y3IQ4vufabrLKuEZoiXwJqfwkT9i9oPqd7cVHbHd/rXZ2llykuXcDvT217QY1Xhseg5b86asPcFnrFK8EhKnrhjeS+g+EnUDVK9TqI+cdl7yPM4+29v1jtdtnyyH9gywQcekg4X0Tu3bHiZdw5JPEbjcYoQIbrkjHPPXxcVB8tfFeWf00vqVM9lDG7+zW8W2Kd++0LuQsz9i3q2iFaxbb2lyqtblkXleGXOoLTLF8btLvouU7rrDT6EpKHqZ9KKhv3PefZx7kO35Hj836XuDZythysTHQvWY/ovq4BbxEg4Phu0O8B29HNx2/wjc8Fuo1y49InVxeJ/pAFtTqGAMSAR85tZfox2G/vZtg2f8AfhuLjFRULuVt7fq3BdsMAutGZzUt4xku+z2QXhilsiiPl11drx+ur0MnVtXqaOBts31Ey4f0u5xcXhzAMc4nlmD4WjhrFz4tLJEP100V3OL6f4cw3m3y8jmxEuMBhiifOs8plIN4EjGS3rqtQu5zufyjuUyPGS5jWObYbU7Y2A4bsnsdgaKqnwHanDvVbfeorUirccrr1kd9qGW37ze61Tlxu1S2hby5W2kI9BwnC4OFwzAnPPvs0r5s02vln5ltBGPSEA0YjoNST0PN83n5rNCU4QxbTEDHFig9McT4B9TI9ZzLymdSdABcHaF3p7u9mWZX2/7e0mGZph2cW2nsW6ezW6uN0+abS7p2GkdW9RUGX4tVrZC6y1uvOKoa+mdYrqMvOpbdCHnUL1dwdu8f3Ht4Yt2cmPcYpGWLNilTLjkephIeB8YkGJYOHAIz4DuHfdvZ55NqMeTb5ABkxZY3xZAOglE+WrENIOWLEg+l29X+YC7lsv2KuPbt257N7E9l23uQUFTbsjf2DsFfbMmrKS4smnu9NaLpU1KKHHEXNj+W5UU1J+ohv4RVBJ0jxnG/Srhtvyg5fl9xuuS3cCDH35AxDdHHWTeRNf6q9hv/AKo8vm408VxW323H7SQaXsxIkQeoBf5X8SBb+stNPpCYm9mf1Neyy1IZXUU1BvVY8iunFSi3abDS11bX1DjiiShLKAFKWo/DpqY9D9QNwNv2ZyM3YnbSA+MtB+1dJ2Bi97u3ZQZ4nPF28ACJk/dErr/VN7gaPf8A7yNx38auCK/brampVtDt6/TLnt92oMMeVbMjzmkPErd3PymlqcgqXFfE7U161kAnSM+x+KlxXb2H3otvM492b9QZ6xgf/Di2MeQiy1968tHk+czRwl9nhl7cNdJCHymfxnK0yfEyVW+jxuEztp9TXs5yCpe9Fq6bv4/gvqTSJC9wahGHtJUrwCVuXYA68NDxjX9Qtqd72ZyGIDWO3lP/ABPn/iWXYG7Gz7u2Ez0luIx/x3h/2lv59TSic2Pw/wCpGEtOUl47rfqfX7EbYp/Vtyu2h2Xst4yLIQgn4qppW5Zsa/ahKmh+9pHk+ypDkdzxBJtj2XCxmfTLmrCP/wDzE/vXru9ZjZbPkoCNMu45WWMH+njxSyZJH/HnDz/L6rEmy/dj229130+cV+nR3g7kP9v+abFZa7lfal3JV+N3TKtubfRVLNwYqNs91KHGqWoySxWBabo6pFeyzUNThDq5VMJZqOw5HguX4Duufd3b+Ebra7nHTc7cSEchIZsmMyNZS0HykjxA6uOu2HOcV3F23Htbnsp2262+S+3zkGUNXtjmIiwBMiQQJN1P5fm1CXst2d9u9a5ku7ncnt93eXe0ufMY1sh2vt507h+X1rRLlErdHenNsSw63YxhReQDV0NlpLrea1vVj+5pUp4d/wD3pz/Mw9jj9nl2GOQ+bNuaWiPH28MJzMp+RnKMB1+bouh/uzheCy/qN/usW9zw1hhwCbSPgZ5ZwgIx8xCM5f2C0h6q9vf1Bdle/Hsj33+nv3z7w4x245Vf8votwu2bd26Wx21bF4Ccetlut+IbXV9Bbm3EYri+HM2ss0ynXUGrZrFOOPOVTbin/Dcv2ryPavcm17s7Z2893gjjOPc4gXzTsSZZQT+ac3c+RDACJDe34bubj+6O39z2v3Dmjt80soyYJkNijUCuNgHjCLEA+R1JlrLzYoe37sy7Xsmt+Yb+d1u1PdtVY9fLXWWDYrs9uuX5BZM1fpLlTOevuPvlkeJ4xYMCwunS2pyoZtjF0vlUgBDbTB1VHsp8x3DzmA7fitjn2EZxIlm3QhGUNP8AR4YzkZz8jIxgPM9F5KHC8HwOf3+X3mHeZIsY4duZkT/tZZQiIxPQ1E5F+kfzD0i+q32d92HfV3yJ3/2MwqoyztE3H2k2ivO0W/j94tdl7d9tNrqPAbELyxk2aVta1ju3bWM3NuoNxonyy+qpQ4pDSyRr47sTuLgu2O2v7q5PIIc/hz5Y5cLE7jJlOSTVgBbJYNUhwzaher734Dme5ee/vPjoX4TLixyx5XbBDGMcR80/yQobWBIk7sC68t+6nfrbTD9osa7Fe1TIH8h2LwnKP8eb0bxopXrPU90++jNMu3/4wTbXUN1tDtTg9O45SYrb6oqdUwlurfSmpAI9vwXF7zcchPujnICHJZYUw4Xt+mwu9X6HLPQ5JDR/lHyrxfOcptNtx8O2eHkZ7HFO2XL+X38ocGVdCMcXIxxl4PKQMi486dTHsXXjmTU/b+qI6ME1MHRk1MHRgmpiujBNTziOjBNTzMVwjBTqecHHgjBRrB0ZNTB0ZNTEdGUzHnB0YKNTB0YJqYroy7lDcKy2vipoahymflKfUbOhKT4pVqCFJ6GMZCMxWQcKxJiXiWKuBGcZM34XEL/87TUzn++2Y1Hb4D/N/FbRuMw8fwXcb3DyJH5nKRzoqlQkf/ZlEYHbYT4H71n+py+Lfcu83uZeU/8AEo6BY146CqQf/wCYUnyjE7TH4E/h/Ishu5+IC507kvH/AI9mpXeej5T+E9O8RE/SD+bIq/q36xC5juLTBp4M2UU7zjakBTbzITMQZZyimaUpIPGJ+lL6y0/h6q/qosWix/h6LFup1J56+cc51wmCjUwdGTUwdGTWDoyamI6ME1MWyME1MHRk1MHRk1MR0ZNTFdGTUxHRlxTGMNFmwSY8IKsEmMGCjBJjBGCTQZGSYwRgpmiMlVExiowSYwZGSYw0VYKCsgE8gT+EFGWx2M0iKCx29ltIBWwiocUPFxx8epOrmZVAfcI6fNK2Ul/FdvhxiGID7VXZjGrRbWCpl2tNFeqdFNXNlbbbyXmyhUjiVJPxBKwJglxPBXMRsx5JY5WidWWE8UMgaXRc7r9HaqKd1bNHRUrYSnXRtttCRolCE+08gNSYgEsktHMiqRCEXOkQuWkrGa2mZq6VwOsPoDjLqdQFIPgoA6KB4e3jGMo1kYy6hItIWHQrCWeW9uhvXrMoCG69oVBSBokPgyvFIGgAOqeEdntZGWNj/NXXbrGIZHHQ6qyZjHJ0XGYJMYaIwSYxNFWCTGKyjBJjBGSYxNEZJjDRGSYxf2oyTGCMEmMPRGCTGIjJNDRGSYxUZJjE0RlclPmmY0loTYKPK8ko7En1ZbPSXq401tSHwA+lNIxUNtBDwHxJ0lV7RG0Zcoh7YlIQ8nLLpc3bPbm45E8vueP2WTlS376eHHLJ8v5fnlElx4HqFbevs9nTh9hGth4Lu2SYw0RgkxgowSYw0RldGOZzmeHCrGJZVf8AGhXltVamyXOqtwq1MgpaVUCncQHVNJJCSrXQGNkMuXF/m5yi/Viy6Lmu1e2u5DjPcOw2m+OIEQ9/FDJR+ojYFn8W6qkXa83a/XGqu98uVbeLrXOerW3K5VLtXW1bsoT6j9Q8pTjipQBxPsjCUjORlMkyPiV2PH8bx/E7PHx3F4MW24/EGhjxxEIQHVoxAAAW7d07sbNS/TjwzsqxOivlLfbp3M5tvvvJd6xmlYs90o6TF8Vx7am0WOqp6t2qrWqJTl9drmn2mUoW4wR6mvweWhwOSXd2TuPPKJxDZww4gHcEylLKZaMH+QRYnR+nj7XJzuMdq4+AwgjJLdSy5SQGIEIwxAavp85L+LEemicxj0+i8swXIzUPUz7FVTuFqppKhirpnR4tVNK8iop3R1bebSR90JREgYy/KRqsokwkJx0kF7r/AFKvqoYX3z9j/Z1texQ3Oh7hMSyGoyXuhr3rS5TUOV5DiWFMbaYlk7l9UhLV+ueSWq1sXBxKCsUKVhgkFJEfMOzux9z2z3NyG9eJ4ica7cPrGM5nJKNf5ojI1/rdV9N7s7z2ncfbWy2ZEhy0DbOWYSnGMccZE/zjKAJPk7eC8JJjH0/RfMGSY84uirBJjBlGCTGGiMkxgyMtvOynuapu0jdXMt4mbTdblmaNit3cH2orbWukQjFd0s5sTFmxXMbsmqfp1vWewEvLeQwovmcSpVxEee7k4Q8/ssewMojb/qcU8oL/ADY4SeUAwOstGfRej7b5rHwW6y7sxkc5wzjjlFnhklExjLUhwLEkdei1CTqlISCToANSSonQaakqJJMeh0XnSHLq6cIzO/bd5piO4GLVCaTJsHyWyZdj1UoEpp73j1wp7pbH1BJSoparKVCuBB4eMaNztsW722Ta5w+HLCUJD0kCD+C3bXPk2m4x7rCWy45iQPkYkEH7wvaL643ersh3jbi9tF32AuNFVYta9nbjuNnVHQFQ/St4t77hZchz+031BOjeWWysx1CK5vQFlSgAEhUsfOPpp25yXb+13kOVBGY7gY4P44sIMcZH9UiRqvo31H5/jecy7U8dUw9r3JMzjJl1yA+rs/gW06aeGsxj6cy+ZMEmMEYJMYIwSYwRgrlRm2at4y7hTWaZg3hT766l/DG8pvyMReqHFlxx97GUXBNkdeccJUpSmCSo6njHHO12ss/6o44Hcs16i3+Mz/iuXHebuG3/AEsckht3er6P1f4q2pjHIXEZJjBkYKZoiMomMVkZJjBGCTGDIyTGGiMEmMNFWCTGGijBJjBGCTGJojJMYqMEmMNEYJMYIyTGGirBJjBRkmMNEZJjESqTGKjBJjBGSYwYKsEmiaKMkx5xdFWCTf0RFGSYxWRkmMGRkmMRKqZoN96VUTGKjBJjDRGCTGGiMEmMEquKfpGt1sqomMHSqTGDq1SYxHSoSY9IrpUJNB1KpMf2coOrVJoOlUmMHSqTHpB0qhUSCOfCDpULYLD7s3crLSgKBfo0JpqhvUaoU3wbVpqTKtrTj4E68o6rcRMMhPgdV2mAieMeYXevLt/QhCrGzb3iAovIrFuJcUf3QzoW2gOcyhGGM4v9KSPgs8gygfu2PxVuqya5WemVUZL8izUOJPylpoklVW6eIDryy86hlnXp/VG32seQthcjxJ6LT7ssYfMz+AHVdC322vzBbd4vrpbtYUTQ2thSkIWlJ0nX7QhX8X5l9Iylkjt3x4v854lYwxS3B9zKf3fgFkhpDbDaGWW0NNNJCG2kAJQhCRoEpSBwAjiEklz1XLEAAw6LCu4VwRU3pumbIUKCnDbhHsedIW42f9JAQNfvjstpExxufErrt2xyV8grDn6RynXFqomg6VScwdKpMYOrVJjB1KpMekHVqFM/SDqVUTHpB1apMYOlQk0HSqTnpB0qEmPSDpUKZ+kHUqomMHVqkxg6lUmg6VSYwdWoUz9IOpVRPEdWqTHpFdKhd620v6hX0tF6qKf5l0Nes5xQ3qCdSNU6k6aAajUkCMJzpEyZ2VhjtIRdnVw37Equw0hrH6uldQaoUzbaJ0urSpKlJdlWAOEpmSCZeHE6xqxbmOWVQCCzrdl2ssUbEjqrQmjkOuPVJjB1aqZ+kHUqk3SDpVRMYOrUJOYOpVJjx+2kHVqEmMHSoSY9IjpUKUlSlIQNNVrQhPKZaghP3DUwdBF9Fsr3Qdpm9naBlWGYlvZYbfaK7cLb6xbmYfXWa70t9s17xa/MNPNPUlzoyWFVttdeDFYz+anf1QriI6fhef43n8OTPx0zKOLLLHIEVkJRPiD4HqD4hdxy/Aclwc4Q5CAj7mOM4kG0SJBxqNH0LjwIV2ZN2IdzmMY72pX9zb9d6qO9S1u3nt9xfGq+lvOXZTQiut9BTuV9jpj69iNau5tOI+aKAliZxwoQlRGjB3Rw2fLvsYy1jx0mzykKxiWJ0J6sxdvHTqt+47X5jbw2c5YxKW/BOGMSJSkAQOgOjuG8G16LJqfpw7l3O81+3+Gb59p+4u+1sbrkVfb3g++1gvG59Rd7VTvVV1xTHVrp6bC8yzS3t0ziV2m03asrVutqabQt0SRwv+MdjGA3Ofbb7Fxpb9/PCRjAPSUv58IFx804ANqS2q5v/Bm/J9nDn2mTe6/uo5QZkh3jEtScgx+WE5FwQASGVhbadkOe5/tRT7xZDunsDsfilfufmWzttZ383NpdsbvW7gYDS2CpyuzfJXagcTQCzHJaRt9yqWyhp1akrKZTHJ3ndGz2u9OwxYdzucwwxyk4cfuAQmZCJcHV6lgHfwWnadp8hutuNzPJgwROSUAMs6SJjGMjoRoAJxclmfVlQMm7Kt/cLv28eM5ZYrHZbzsc5tkcxaVklruNDX27eF1obeX7ELxaXq605Tj2Q0VQzWNVlK8tk0ryF666gb8PcnF7jFt82CUpY9z7lPlIIOL88ZgsYyiQQQQ7hcTN25yW3yZ8WaMYz25x2+YFxl/JKJBIlGQIIILMXWOcw2Ez/B9+aztyvqLMncehzOjwSoRSXRuosYvta8ywzLdghLaqILeEzsugGp0jl4OV2u54wctjt+kOMzDjWo9PP0XEz8TutvyUuLyV/VRyCB10ckDr5arPO7/Y5X7Ks7i0+Ud0fZzdMt2xeyC35Ftzi2+FJes+fyDGqmoo7ri9nsLdlZNxyFuuplsJpw4mZ1OmsdVsO6MXIyxexs9+MOapjOWFoVlqJE20ixd2Xc77tLdcfjyyz7nZmeJ7RjkJk8dCAKsS481ozMekendeWqkxg6VCTGDpUKZ+kHUqk/SDpVRMYOrVJoOpVJoOlUmMHVqEmMHSoSYwdKpNEdKpMekV0qkxg6VSc9IjpVJjFdKpMeUHUqkxiOrVJj0iulQk0HSqTGDpVJj0g6VCmfpB1KpNB0qomMHVqEmMHSqmfpEdSqiYwdWqmbpFdSqiaDpVTN0g6VUTHpB0quGYxpdbahJj9vt4QdKhJjB0qEmMHSoSYxXSoSYxHSqTGK6VSfpB0qkxiOlQkx6QdKhJukHSq79uu1faagVVBUKYd0lVpoptxHjI62rVDifvB09kYzjGYrIOFlEygbRLFXcvcS/uthltqhbeXohLzdOtThUo6DRDjzjQJ1/h4Rx/0uIal2XI/U5SGDOuC8Y/Wt1Nnaq6h2qvV6emqS4oqSwlRlDZUdSShX5jwA9nCMseaJjIxDY4qZMErRsXySWbaZlukpqekZTK1TMtstjx0Q2kJA6mOsMjIknqV2MYCIER0AVnZXl6LMlVDRKS5dFp4kcU0SVeC3PEF7+FPs8T4aRycGA5DaX5P2rRnzDGKw/P+z/pWD1vOOrW44ouOOKK1rWSVKUokqUo+JJJjsumi60hy6+ZjB1KhJj9vdB0qEmMHSoSYwdKhJjB0qkxiulUmMR0qkx5RXSqiYxEYKZjB0qkx6QdKhJjB0qkx6QdKhRMYOlQkxg6VCmYxXSqTGI6VCTHpB0qEmMV0qFVbVaa28OOopUtobYTPUVVQ4lmmp0Hw9V1XAE+wDUnlGueSOMOep8PFZwxHIWHQLkutlrrP6DjxZep6j4qeto3g9TuKQQSEuDRSHEn2EA+0eETHljkcDSQ8CrPDLHqenmFwVFfdb0/TtvO1Fe+Epp6Zr4lq0A0SlDadAVaDidNT7YyiIYwSGA6qG+QgEknovu4WW72pKF3GgqKVt3ghbqPgKv4J0kpC+h4xIZYT0gQUlhnD84IVKmMZusKpMYOlUmMHSqTdIrpVJ+kHSqTGI6VCTGDpUJMYOlVyMqPr0//AO803/p24E6fYsoxFgv1Cb77TW/v23Kv/bdlN8p7Bcu0XJtpN3H8prapDFTYOzbcqhtbfcWm1h9YdrKnBMiujuWvIbJLdvoXAlHCYfF+N38u2drHlsETKO+hlxVA/NuoE/p38hOI9r+0QvsvI7A9w5pcdmMYjZ5McxIn8u3NDnYPqYGfuFwRWwA6te+O7/J7hbV2w5lbai24Tk28dL9WzZztapnKmnt1qwy4XSzUeFbX7c43XvLRT2ioyiirF2W0ELTLcLu2ApJIUONPjv7rnvcExLJh28uNy7nRzMRkZZMkh4iJF5f1Yrkz3h3g2WQSjDNuMe/ht20EPcxx9vHEnoSJe2P60tCvzsbC7Edwt67ntt9pcPwHPLNvdj+6uLevQVFgvVtu+BXrGMmo7ncssyR9+mZOP2nEf0525VlxqFt07NNTreLko1P1fk+U4rHwmXe7jJjlx0sMtQQRMSiQIx/pGT1ERqSWZfMeO4vlJ89DbYIzjv45H9YEeJ8Y16l/ygOWAJHtb3ibi7F37Yi/5gnt4R3P7fXz6kvc0i10GP5juRh9DVXCzbVdtFnz24265bXuJduDGbZfb7g83UOoeS4HPWaCvUJPzvgtryMOQjgO7/R7mPEbdyYQmWOXcGAIydKRIBAbox6L33LbjZjbe+NsNzilyuYj5pQH+a29pAhh85eQkfNx1U73WOtbru7fJq9V8s3+ONlfp8ZtbdrclprfS5NsTYqbLbhiuNbP5A3bqK2NuO2OwY9TVdNUvUlLXVtBXMVFWlVU684vLjssTHY4Y1kMe430Dki9cxoJSyxcn80pEEAmIkCI/KABo5LEbb7LO0TkwbOdJNbELmMcUmYGoiCCwMoyEj8xJOjPcrj+Rp+rHlFyVjmQJtiO5THqhdwVY7qi3imFxt5U8qvNIKZFOU/9pPLofGPScRmxf8D44Xjf9JLRw/Qrz/KbbL/xjlyVNP1Q1/wor5+oFmGxVdvz3d2WxdkORYpnSd8d3Gf+sa93d6bvSM3Gmzi9ivyheI3SkGJfL3B1KniwpXyzIVokygQ7WwclHjdhkycjGe2/TYv3XtYhpSLRsDbTo/UrLufPsZ77e44ccIZvfyfvfckTpOTyq3j1Z15QzH7eEe4deEqEmPSDpUJMekHSoSYwdKhJjB0qkxg6VSYxXSqTGDpUJN0g6VSYxHSqTGDpUJMekHSoSY9IOlUmMV0qFExiOlQpmMHSoSYwdKhJjB0qkx+32MV0qkx6RHSoSYwdKhJjB0qEmMHSoSY9IOlQkx6QdKhRMYOrUJMft7oOpUKZjB0qEmPSDpUKJjB1WCmY/dB1KpMfPy5QdKrhmjU62VSfpB0qk0HVqk39n21g6lUn6QdKpMOsV0qk/SI6tUn6QdSqTiDpVJ+kHSqTQdKpN9vsYOlV2aN9LFXSvLGqWqhpah46gKGvKJLWJHiQqA0gfVbEqoGqu8Ul7LqXG6egcYpGwngTUrU4agrKtD/LXoBp11jqhIxxHF5nX7F23tCWQZT4DT7Vb2WZe3Z0mhoVJcubifiUPiTRIUOC1jwLxB+FPs8TyjZgwXNpfk/atWfMIfLD8/7FhFx5bri3XFqcdcWVuLWSpa1qOqlKUSSSY7JwAw6LrWJ1PVfE/SDpVJucHSvkk/SDpVJhyg6VSfpB1apNB1KpP0g6VSfp5wdKpP0g6VSaDpVJvt9tIrpVJojpVJoOlUn6QdKpPB1apP8AjB1KpPFdKpP/AGRHSqT9IOlVkfGNuLvfmGbhXL/RbTUI9SmqH2FP1tc2fyu0NvDjBXTq9jzrjTSh+VROgjs+M4nfctIjaRHsxLSnItAHyfUyPpEH1ZdPzPOcdwWMS30/3sg8YR1mfVnDD1JGnR1mOhwTFqG2G1qoay4sOVbdXVrr7nUMmtLSSlDDjdo/TFM03HWUOqWDpouPWYex9u99zuMksrN8gjED4WuT/DReB3H1K3L02e2xxw2c3MpEt8DED7j8V3kYlijdKugGP0q7ep41KKJ6uvbrLNQQR6zK13U1IVKSNFOKSR4gmOXHszio6ynnlPzMgC3lpED8H9V18/qLzcpERhgji8IiJIB83JJ/iXPascx6y1i6+22K1tVC6dym/vFN+oMobdlnU0zcFVSGnvh4LTosDXQ8TG89n8JLH7c45ZB3f3Jg/fEhcYfUDuOGX3cc8UdGb2oEfiDqud2zWZ+kqKFy0200tUZnmk0jSUqWNfjToNW1gnxSQYzh2jwMJiYxTMh55Jn9slqy9+dzZYmHvxjE+WPGP2RXGjH8aQy2yMWxdSWkBCVuY/a3H1AcAp6oVTF55Z9qlKKjzjM9rcIZGXtSc+U5gfdZlB313GICHvR0HWkX+9lxKxjF1664vjo1/gs9G3/uNiMx2zwg6Yf+tM/9pap969xz/wDMN8IxH8S65xDFD/7OWYfdQsj3ARtHb3DD/wAvD8f5Vxz3b3ES/wCqyfh/Ivk4bih/9nrUPupkj+mMxwPDgv8Ap8X3LWe6e4j/AOcz/wCN/wBCDDsUH/s7aT99Kk+8xDwHDH/y+P7lR3V3EP8Azmb7/wDoX2MRxUcf8N2U/wCtQMK/3kmMJdu8NLT2I/YSP41sj3d3FH/zWT7WP8S7CMaxhHhi+Nn/AF7LQufjO0qNUu2OEl/oSPhOY/7S5Ee9u44/6d/jGP8AIoqcaxiqZDC8Zx1pAWlwLpLLb6Ko1T4JNTSsNVBbPtTNKfaIke1+EgXGIn4zmf2yKyyd8dx5ICHvAMXcRiD8OnRfDOLYvTlJRjdiUUkKSXrZTPkKSdUq/mpVxSRGufaXBz648g+GXIP2SWzF373Li6ZcZ+OPGf8Asq/qrLssr8syDO67KsmqcxyvGblhWS5F/iG8UtxveG3mzP47dsUuDtDW0vr49crBUuUb1KR6S6dZQRpHCPYXbHsQ20NvXBjmJxAkWEomwlq/zCWoPV9Vzo/U7u8bie5ybi+WcTGTxAeMhWUdG+Ux0I8RorcrUC5Y7ieGV9RdqrCcCr73dsHw1eS5QjHsRuuTVlJcckueO07N6bqbVX3+voGH6p1p5KlusoUJSkRgew+D9yebH7sNzlAE5gwtMRcREngQagltPFciP1Q7j/dwze3k2+IvCErNElrM0h1YOOmgHRZwynue7o80wSr2zyPud7gK/Bq+gTZq3Hl7lXN2nrbEhKG02S41lYzVXu72xLLYR6NVWOpWgBK5hHSH6TdqYs/6zZYcePdguJHHGTS8wBUA+oY+S7uH1q7ryYf0u/yTybQ6GInKLx/okmzgDwOiwfh+9fdv274hVYdsL3Abq4ht4q53HJqrFMIyGusFPSXeup6OkuN8cslM6+ya+oorfTodqqVallDCJggJTp5juDsba48v67l9pt84AERlqJaakCTh4hyWB010JXu+2fqFl3uE7Pid1mw5XMjiMpRLsATFi0tANR82moGi1rVu7uk6/mlY9uLmlVV7kVVtr9wK2syO511bmtZZ6xdwtFXk1ZV1D1TeH7ZWuF1hTy1FpR+HTQCOENjsQMcY4sYjhBEAIhoAhiI+QI6suzlvt7I5JSyzMspBm5PzEFw/mx11/as9Xr6gHe5kdlrscv8A3U72XixXOiTbbhbLhmdbU09bQI9KSlqfUSXXGk+gjTVWolHGOsx9sdu4sozYtlt45IlwRAaFdnk7m57LiOHJus0scgxBnI6feuPLO/rvWz3H8jxPNe6PefKcazC13Cy5VZb5l9VX0OQWm6tKZudBdm3kFVZT17S1JdClarBOpi7ftnt7a5YZ9vs8EM2OQMSIgGJHQhujJn7l53c4Z4M+5zSxZIkSBnIuD1dzq/qtSJ+kd666GqT9PODqVSfpB0qk/SDpVJxB0qudhipqiU01M/UEeIZaW5p0JSCEmBmB1LKiBPQOvh1DrC/TfZdZc/gdQptX7AsAkQEgdRqhiQWOi45+kHUqonHt01++DpVTP0g6VSYcoOlUmg6VSaK6VSbpEdKpP0g6VSfpB0qk32+x5xXSqT9IjpVJ+kHSqT9IrpVJ4jpVJ4OlUng6tUm6QdSqTwdKpP0g6VSaK6VSfpEdKpP0g6VSfpB0qk/SDpVJukHSq4JjGpwt1QkxiuEqkxiOEqkxiuEqEmMHCVCTRHHgpVJjFcK1CiYwcIwUzGDhKhJjBwlQkxiOFKpMYOFahJjFcJVX3Q51WUNi/TUIKq5o+lS1iikpZpdOGqT+Z5r8qOEoABOscaWCMstv5viPVcmOeUcdP53mrGcecdWt11anHHFFbjiyVLWpR1KlKOpJJjkBug6LjMvmYxXCVCTGDhKpMYOEqEmMRwpVJjFcK1SaDhSqTGDhWoSYxHClUmMVwlQkxg4SqTGI4Vqkxg4SqTRdFKpMYOFapMYOFKpMYOlUmMHCVCT9IjhKpNFcJVZv2oxCyXe3XbIb/QfPIoa9u3WWmW84imqrmaZupWqsZQrR2it7Sw6pC0yVB1amSY7ThOJyczyIwOY7LHG2U+JD6RifOXQkF4jViul7j5vD2/xUt0QJb2Zrii46+MiNdI+GjE6Fgs4EqPFRKjKlOp0/KgSoSAAAlCE8EpGgSOAAEfZMWLFgxRw4YiGGIYABgB6L877ndbjeZpbjczlPNIuSSSfxURsXHSCJBEgiQRIIkESCJBEgiQRIIkESCJBEgi+kqUhSVoUpC0nVC0kpWhWhEyVDiDoSPuOnhElGM4mEwJQIYg6gjxBC24s2XBkGbDIxyxLggsR9ywbujhtPTUv+LLRTN0zIqWqXIKRkBLLdXVlw0V0pWtEhpqv9NaHWkTBt1ouKUA6lI+R9xcMOH3Ylhf8AQZnMf6kh1gT5avEnwNQPlX3ztPuAc/sSM7DkMIAnqPnHhMdNfCQA8ASSZLCMxjoHC9TVJjBwrVJjBwpVJjBwrUJMekHClUmMHCtQq1j1qXe7rT0IJS0ZnqlY8UU7WhcKf9I6gD7415cgxwMvFbMWL3JiPgtiKOmp6BhFNRMop2EABKGwE66fvLI+Jaz4knUkx1ZlYvJyV2kccYBo6BdO8WqjvdI5S1jaVKKT6FRKC9TuafC42vx4HxB4HlGUMhxl4qZMUckWksW2bBax64vpuqVM0FI7JMgymuPin0FfmDKh4q8fZwMcue6hX5PzH8FwobSRk09Ij8VlFqyWZhkMN2uhDQEpSadCioaaEqUsKUon744hyTd3LrmjDjAYAMsc5lidPR067tamvRZbI+cpU6lpCVEJD7I8UJCiApPEcdRpxjlYM7mk/sXD3G2jEXhoPELGExjluFxKhJjFcKVCTGI4VqkxiuEYJNBwpVJjBwrUJMYOEqEmMHCVCTGDhSqTGDhWoSaDhSqTQcJVJjBwrUJMYOEqEmMHCVCTGI4SqTGK4UqqhQ2y43MVKqClcqRSth18N6apQToAASCtRPgBqYwlkxwaxZ1nHFKf5Q7LjrqGutryWK+mcpXlIS4lDqZVKQrwI4n9vKLGcJh4lwpLGYFpAgrpzGMnClQkxiOFKpMekHCVXXm6+fsjU63Mmp5wdGCTHnB0ZJjzg6Mmp5wdGCTdYOjJqecHRgmp5wdGCamDoyawdGTU84OjBNTzg6ME1MHRk1PjB0ZNTB0ZNTzMHRgmp5wdGCannB0YJqecHRgmp8dYOjJMfDWDowSY84OjBNTzg6ME1MHRgmp5wdGCTHn0g6Mkx5wdGCannB0YJMecHRgmvXxg6Mkxg6ME1PMwdGCannB0YIVHTx+6Doy3Oxukbt+MYzb2Q36VNZKSpnQZi5UXlKbxWFxegKlt1NWpH+iBoOAj6v2btBt+HGYg+9nySlL4AmMf+qAvhn1C38t1zX6UEHDghERbwJDyf/CJVYj1a8EkESCJBEgiQRIIkESCJBEgiQRIIkESCJBEgiQRcb9GxcaS42uq0+Vutsr7dVK0JLbLrJfDregJDqHaZOhHEamOg7n2Y3nCZ4APlgBkh/aj0/Aleu7I5A7DuHCSQMWQ0n/ZPgPLVvuWjgKgAFcFAAKHUDiOXjHxx/FfoJgp19msHRkm6wdGTU84OjJqecHRk1MHRgsi7bOIF3q0KI9RdEotA+JCFau6fsUI4u7J9seTrk7X85+CzSVBIKlEJSkFRUogJAA1JJOgAAjrwSToueqQchsI11vNu4a6/wB6b9n7Yzpl/olYe5DzC+rlfLbaqIV9XUoSw4mZgJ+J2p1GoDDfAr1Ht4Ae0iEYznKoGqSnGMbHoqHj9yv96q3Li+w3QWNTRRR0ziAah9UwKX5yA5poPHgk+we2NmUY8cagvl8VhjlknKxDY/BXBeA2q03IOkBHyNUST4TBlZR/44EascjcfELZMAwIPkVrAgmVPHxSPcI7gldSy+tTzg6Mkx5/bxg6ME1PODowTUn2wdGCTHnB0YJqecHRgmp5wdGCamDowSY84OjBNTB0YJqecHRgmp5wdGCawdGSY8/wg6Mmp5wdGC7VNRV1YHDSUlTUhoauFhlbgQNNfiKQQDpx5xiZxj+YgLIQlL8oJVTsN/rcerVPsJC0rHpVVK6VIDqUk6AnQqbcbV4HQ6HxBjDJCOWLH7Cssc5YpOB8V1bveKy9VqqysWC4oSNtoErbTY/K22nUnQe08SfbGUIjHGsVMkjklaSpmvI/hGblYMkx5wdGCamDowXX1jTdbmTWF0ZJhzELIynWF1GUa9YXRk1ELqsmsLKMmo5wuqyawujJrC6jJqOcLKsmo5wsjJqOcLoyajnC6MmsLqMmo5wuqyawujJrCyMmvWF0ZNYXRk1HOF0ZTr1hdRlGohdVk16wsjJrC6MmvX/wQujJrC6MmsLqMmsLoyawuqya9YWRk1hdGTWF0ZfSPiW2kcStxtASPFRUtKQkdSTpEM9HQByt5qZDjVLR07rPyztJQUFE9TFIQad6hpGqV1lSBwSttbRCuZEfc+BjXhdrq74Yl/7Qd/tdfmruvL7vcG6cVMc04t06Fv4lzR2y88kESCJBEgiQRIIkESCJBEgiQRIIkESCJBEgiQRcjSlocQps6OaqS3yncQtpJPjoAXOPIRx95AT2eWB6HFL9hXP4vLPDyOHJj/MMkf8AKC0evFM5RXe7UTxbL1HdLhSOlpU7Rdp6x5lwtr0EzZWgynQaiPz5jyA4oyHQxH7F+qckTHJIS6glU7WM7rFk1hdGTXrCyMmsLoyawuoyqNqub9ouFNcKcgrp1gqQT8LrZ4LaX/orHmIwm04GB8VnCRhISHVZ4t+W4/dWNTW09OtSNHqStUltxOo+JJCxI6jXhqOBjrZY8sD0J9QuwjlxzHVvQq0btWYbRVTblntdJdLzMr5ZiiStdIh0/wDaPtoPouLQeIABV93jG/GdxINkJjj9eq0zOGJeEQZ+nRUvFWajKr6/c7yr51qibCpHR/dw6s6MsNtDRCENahQSOBA46xnmyDBipj0dYYYnLkM56gLNOoAAGgAGgAAAAHAAAcABHX3XOZY2zzJGKaldslK4F1dSlIqyg6/LMTBciiPB5wpHD2J119kcvbQJl7kvyjouNuMlY0H5isMajmI7C64LJqOYhdGTWF0ZNYXRk16wujJrCyjJqOcLqsmvWF1GTWF1WTWF0ZNRzELoya9YWRk1ELoya9YXRk16wsoygngeI10P4wsqxWRbhcKi1izWuhupstM3aae4OutpdAfrn0B8+uWELcVMVaJ1BSI4sWnaco2JkR9nouTIyjWETUM/2rpZfTtOC1Xc1FImqudvpXqqmRq2+46ppBNZ6IQEIaWDxJIOusZYMhFoMaglv5Fjmj0npYjVVKkpHcetNLUURtFRd7qtbgq6h6mep2KFpJMtMqo0YLh01X46cYwlkGSZErDHH7NVkIHHAGLGcvwVGv62blabXfjTsUtbU1NXQVgpkJbZqV0vFNSG0/AlRA04aDjGzFIxnLG5MQAR6LDILQGU/mOhVn6xvutLJqOcLIy68w6xpdbqlJh1g6VSYdYOlUmEHSpSaDpVJukHSqTDrB0qUmHWDpUpMOsHSqTDrB0qUmEHSpSYQdKlJh1g6VKTDrB0qUnEHSqTDlB0qk4g6VSYdYOlSkw6wdKlJhB0qkw6wdKpMIOpUpOIOrVJhB0qkwg6VSYfb7GDpVJhB0qkwg6lSkwg6tSkw6wdKlJhEdKpMIrpVJh1g6lSuVhR+YppQSo1NMEJHipRfbCQPZqVRCxDKgEFwt9n0vpqKlNU0pirRVVDdWwvT1GKpt5aKllwgkFxp4FJ0JGo8Y+/cPEQ4jaYwXjHbYwD5tAar8udwynPnd5PJExmdzkLHqHmSuKOxXTJBEgiQRIIkEWXdgNqlb6b5bSbLIviMZc3Vz7HMEayJyhXc2rI7kVwZt7VzetzdRSuVrVKt4KU2l1ClAaAx03cXLjgOC3fNnH7o2u3nlo9bUiZVBYs7dWK7/tfg/8AiTn9pwfue1+qzwx3a1byEXZw7O7OtsO9L6X/AHcdjVwra3dDA3cm2xRUrbte8u3zVZkGB1dPOpLC70tunTdMOq3kp19G5MMAn8jixxjx/ZP1V7Q76xxx8ZnGLlCPm2+VoZQfGutcg9YE+oC9x319He8exchyb3Cc/FvpnxPLGfJz1gT5TET5OvPFKkrSlaFBSFDVKkkKSoc0qGoIj6SvlJBiTGQIkOoKmCiQRIIkESCJBEgiQRIIvpIUVICfzqW2hBPgFuOJbQSR4JC1jU+wRrzN7E36Ul/klcrZRlLeYowYTOSLP06vqtJcqZepMpyakqlNrqqXIr3TVCmVFTS6hi6VTTymlqSkqbU4glJIGoj85xoIgY/yAafDw/BfrJpnXI1z1bo/i32qgzD+yMnSpSYQdKlJhB0qk/SDpVJukHSqTDrB0qVEwPs1g6VWT8IYp6ey3+9qSk1NO06w0sgEstJp/VUUa/lWsk6nlwjh7iRlkjj8FytvACEpnqFemFW39MsTCloKaivUax/UaKlc19BKh4hSGzoY07iZnk9Bot+DHTH6nVU/LsyRaAq329SXLmpP8xY0KaFKhwUfEGoP7qfZ4nw0i4cF/mn+RY5stPlj+f8AYsHreW64t1xSluOLK3FqMylqUdSpRPEkx2A06dFwGJ69V8zCK6VSYdYOlSkwiOlSkwiulSkwg6VKTCDqVKTCDq1SYdYOlSkw6wdKlJhB0qk4g6VSYQdKpMIOlVE/jwMR0qsiW7AXrlb6O4M3NlKatkPBtdOv4PiUmWdLqptCnx0EceW5EZGJB0XIjtTOIkCNV9r23u6Skorbc4ApJImqUK0BBPA05Hh1ifq4eRV/ST8w32/yKi5ofTvqqY8TS0tvpjoeGqaRgEJPjoSY2YC+N/Mk/iVrzRIyMfABfOXLArrewP8A9ns1tbI/hJpWlaePWGE/KT5yKZomwHlEJe1BFgxZn+NiuqdD7Z6jQHw/0oYz+8n8Qk4/u4D7V2aymqncUx9NPSvvAv3CocLDS3QkrWkJKpQSkqBjGM4jNMkjwVljkcUW9VaSmKlHBdLVI08Z6d5On4oEb7R8wtVJeS4vi1lkcm/hkVN/s6a6RX8VKldWYxodbqhJjB0YJMYOlQkxg4SoSY84OjBJj9v7IOjBJjzg6MFMxg6VCiYwdKhJjB0qEmMHCVCTGDpUJMecHRgkx5wdGCTGDowSYwdKhJjzhZKhJjzg6MEmPP3QcIwSY/b3QcIwSY84OjBJjBwlQkx5wdGCTHnCyMEmMLJUJMefug6MEmMHSoUzHpB1KhRMecHVYJMecHRgkx5wdGCTGFglQkxg6VC56VZFXRnlWUh/CpaMHCMBqvQa6OetdLq7/wA66XB77y7VuuE/tmj9D8XGnGbaPlt8Y/6gX5T52fu8zusnnnn/AJRXRjnLqUgiQRIIkESCLbz6fv8A8dPaH/8AMHtl/wC8tFHjPqL/AOg+X/8Ax+f/AGcl7/6X/wDr7if/AK/B/tIr+nhcLbbrxb6m13egorpbK+mXS11uuNKxXUFbSvIKHqaro6lt2nqWHUEhSFpUlQOhEfy5x5cmHIMuGUoZYlwQSCD5gjUH4L+q+TFizYjizRjPFIMYyAIIPUEHQj0K8Ae9/wDy+fbV3AuXnO+3KrY7at1K5T1a/a7XQrrtnskr3NVk3LD2FIfxV+odOqqm0KQ2nUlVK4Y/QXYv/MN3N27TY9xg8lxUdHkW3EB6ZDpNvLJqf6YX577/AP8Al27W7oEt7wLcdyx1aIfBI+RgNYf4HyjrQlfkR7s+w7uk7J8ids2/m2F1sdkcqV09l3HsiHb/ALaZIgKAbcteW0bPylM+6kgmlrRS1beuimxH7D7R+oPave+2Gbgd1CecB5YZfJmh/axnUj+tG0T4Ffi7vL6Zd3dj7k4uY2s/07/Llj82Of8AZmNH/qlpecQtP/8AwjqDxBHQiPaL58kESCJBEgiQRIIkEXI0dHmT4aPsH/ZeQr+iMMgfHIf1JfsK5O0lXdY5eUx+1aQ5m8Xcxy13XX1MoyFzXnPd6xWvnH5vkKSMPIt9y/WuIieOM/OIP3hW1MYxdbKhJjBwlQpmPSDqVCiYwcK1CTGDpUJMYOjBJjzg6VCyPt3d26esqrRUlPo3IJWyFgFCqlAlLageB9VsAAe0xxtzF43HULk7YgEwPQq68vzJNoSq3W5aF3NaSHHE6FNCkjTiBwNRp+VP7vieUacOK5tL8v7VtzZBD5Y/n/YsHrdccWpxxaluLUVrWslSlqUdSVKPEkmOeCPBcEh9fFfMx5wdKhTMekHUqFExg6rBJjzg6MEmPODowSY84OjBTMYOlQomPODowSY8/dB0YJMYOjBJjB0qEmMHSoSY84OjBJjzg6VCmY9IOpULMm3l6Wu3VFBUlKGbetv0ahxaUNpTUlclOSogBQWglPPUxwtzFpCQ6lc7bSeJiegWTCogEkhKQJlKOgSEgalRJ4BOntjjO65NVrfkVYi5ZHW1FMr1W3atpptSRqF+kG2NUaazJUUcD7RHZY/lxAHyXW5GllJHmufMl65FWJSRoy1RscNNAWKVtoj9hTEwH92FcwHuFfWSKKaPFW9eCbA0v9rykrPjExFzM/1kygViP6qzHh6pcZtEivGlSpRH8RJmB6iOHmP72XxXNwxHtBvJXGtz4SpxSZEpJUpzQpSlI1UpRVqAABx5CNThbGCwl/iel/xt+p6I/T5v06b006fL66etJppr63t/h4xz6H9PX+d1XCvD37fzeixnMOY/GLZYMUmHOFyjFJhzhZGKTDnC5Rikw5j8YXKMUmHOFkYpMOY/GFyjFJhzhZGKajnCyMUmHMQujFNRzH4wujFJhzhdGKTDnC6MUmHOFyjFNRzH4wujFJhzhcoxSYcx+MLIxSYcx+MLoxSYc4XRikw5wujFJhzhcoxSYc4XKMU1HOFkYpMOcLIxSYc4XRikw5j8YXRikw5wujFJhzhdGKTDmPxhdGKTDnC6MUmHOF0YpMOcLIxSYc4XRiuRlQ9en4+FRTn8HkGMoz+YD1H7VjMEQkf6p/YV6DLX6i1r8Sta1k8ypRUT+Jj9KbaIhtscBoBjiPwC/IW8n7m7yzOpOSR/FfMblxkgiQRIIkESCLbz6fv/AMdPaH/8we2X/vLRR4z6i/8AoPl//wAfn/2cl7/6X/8Ar7if/r8H+0iv6fqfyp+4e6P5bL+rQ6KYKq3crxHFc7x+54pm2N2LLsYvVM5RXfHsktVDerNcqV1JS5T1ttuLFRSVLaknwUg6RydpvN3sNxHd7HLkw7qBeM4SMZA+YIIIXH3ez2m/28tpvcePNtZhpQnESiR5EEEFfmz72/8ALj7R7im9Z52Z5KxsrmDxqK9W1GSrrbptPeKlU7qqWxV6Pmr9gbj6/hQlAraFJIAaZTqqP0t2N/zJcxxlOP7zxHe7IMPfg0c8R5yGkcreP5ZesivzL3//AMtXCc1fkO0Mg2XIFz7UnOGR8oy1lj9HvHwFQvya9xXat3CdpuYOYT3B7WZNtzdS+6zbLjcqX5rFcjQ2opFTjGWUJfsN8p3ANR6LxcA4KQk6gfrntvu/tzu7Zje9v7vFuMTagFpw9JwLSifiPg6/G3dPYvdHZ26O053aZcUgS0meMh5xkHjIesSW8WWvsekXkEgiQRIIkESCL7RwWg8lJP4EGB6H4H9izxlskT6j9q0PyVc2SZErXxv15Vr99yqT+yPzXu5NussfLJL/ACiv11sHlsMEj44Yf5IVEmHOOPdctikw5wuUYpMOcLoxSYcx+MLlGKTDnC/kjFJhzH4wujFJhzhZGK+kOqbWlbayhaFBSFpUUqSpJ4KSoaEEGFkYotwrWpxayta1Fa1qUVKUo8VKUo6kqJ5wsqxK+ZhzhdRikw5wujFNRzH4wujFJhzH4wujFJhzhdGKTDnC6MUmHOF0YpMOcLoxSYc4XRikw5wuUYpMOcLIxSYc4XRikw5wujFJhzH4wujFJhzhcoxSYc4XKMVdlnuNkTZ6y1XV6vY+aq2aj1aJpDhlZSsIQqebhq4Tpp+2NUzkuJxYsPFboUoYScOVXKOmtV1beoaPLr36LTCnnGKmlfLSWkKSkkmdpCgCsCUHWNZyTh80oB381sEIzFYzkzLqrax7Eq1ai+/erxR6Gnp1UxpaGmeU2lbbzyitz1VICgQEqPHxEX3MuaP9GB9dVjWGGXjKY8G0Vj1FU5VvvVT653n3VvOrPCZbiipRA8ANT4DwjeJVDDoFoNiXPUq7qZVFklsobe9WMUN5tSFU1EqqV6dNX0ZMyGPUAIbea0ABPj90aTM4pGQDwJ19CtwAyxEDpMfirpsbGbY8wqjbt9HX0c6lttKrqeZpauKgytLwUAo8Skg9NI1TngymzkSW3HDcYxVgY/Fd2qrMmraG+IvdsFtt4tbqmS0DoX0vMSBT861KKk68BonT2RInFGUTjLydZH3pRl7kWiywnMJfEfl/Z4fhHNuVwWXBPGmy31VTp7VcKm3VV1ZYCqKjWlD7hWhJ1UdPgbKp1hJPEgHSMDlgJCD/ADFZDFIxMx+UKmTxnb1WFUn6QsrVchQ8Gg+WXAypRQl4trDRWACUBwiUqGvhrEsHZ9Up4+C45zFslUn6QspVJ4WVqk/SJZKpOYtkqk8LBKpPC3qpVJ4WVqk8LJVJ+kLfelUnMHSqT9IWUqk/SFlapP0hZSqT9IWSqTwsrVJ4WSqT9IWSqT9IWSqT9IWUqk/SFkqk5iWVqk/SLZKpOYWSqTmFkqk/SFlKpPCwVqk/SFlKrkZWfXY/8+x/6VEZQkLx/tD9oWOSP7uX9mX7CvQtGsidfGUa/fpx84/TuNvbi3So/Yvxvlf3ZP1sf2r6jNa0giQRIIkESCLbz6fv/wAdPaH/APMHtl/7y0UeM+ov/oPl/wD8fn/2cl7/AOl//r7if/r8H+0iv6fqfyp+4e6P5bL+rQ6KYKpBF8OONstrddWhpppCnHXXFJQ222hJUta1qIShCEgkknQCKAZFhqSoSIi0tAF4B/Uw+r79PnA8My7Yu7YniXermVW3U225bY0DVsvG3NluAQ416mXZ7UU9fbbVV0bn7lqFVcW1DQekdFD9A/TH6PfUPkd7h53BlzcLs4kEZzaOaQ/qYgQZA/8AeVgf63Rfn76o/WP6ecTsMvD7rFh5jeFx7IaWIS1/NkYgEH/ViUgfGPVfhkyu7Wq/5Pf75YsWtWD2W7XWsr7Vhtjq7tX2bF6GpeW5T2W2V19rLheauioG1BCF1LzjqgNSY/eOywZtttMe33GWWfPCAEskhESmQNZERAiCerAAL+e/Kbvb77f5d3tcMdvgyTMo44mREQToAZEkt0cknzJVvxyV16QRIIkESCL6SNVJHMgecD0PwP7FlD84+IWg2SqlyPIU/wAN9vA8eVxqRH5m3s/99zH/AL6f+UV+wONg3Hbcf9xj/wAgKiz9I41lzKpPCytUnMLJVJ+kLKVSfpCytUnhYJVJ+kLKVSfpCyVSeFlapOYWSqTwdKpOYWSqTwslUnMLJVJ+kLJVJ+kLKVSeFlapPCyVSeFkqk8LJVJ+kLKVSfpCytUnhZKpPCyVSeFkqk8LJVVa1Xhy1Kq1NsodNVT/AC5KlFJbHqtuFSdAdSZNNOsYSAmA56LKBo/qFxXa5rutxqrgtpLJqXJ/SSorCAEhKUhRCZtAnx0EWHyREX6JP55GXmqdP0jKywquandQh9lbo1bQ62peg1MiVgq0HtOgiGThh1VEQC6vHJMjpLjSoZoXnwsXaurD8LrBDDiUoY1Vw4kE8PEe2NOKJjJ5f0QFvyyjIfKS7lWh89WSKbNVVFtY0UhVQ8pChqCQUqWQQdI3adWD/ALTr5ldefpFssarK2FY7Z7jYUVldQtVL71TUo9R2fVKGXPTSlvRQCQR46eMddmzSjNgWXY4ccJQtIAlcS3KCjtt9xxohitqb8lmktqZy+ukdq2JHGhxKmfQCjProOcQEylHIegj1QgCEsYHWXT7VW1be4+oCUVrSpRNJVFXxaCb/iIc/eifqZ+Lfcsjt8Z810Xtt7SELW3XXFMiFrlUqmWDKkq00FO2fZzijdS8gp+nj5l10l0YrcPxqyoeS2bhdwwl2QLKS1U10zhaC0TFKAJhqPZFu2aU/ER/kUq+KMPOStjI8Pex6jRWmvbq2lPpZKQyphwFf5VBJddCuPjx4Rsx7gZJVbVasmEwDuCrKnHON7rRr5J6g5++Dq6+Sep198HU1ScQsrqo9Tr7+MHU1U+p198HTVAvrB01T1Ovvg6aqPU+3GFldVPqDn74Opr5KPUHs/p/sg6uqmfrB1NUnHP3wdNU9Tr74OmvknqD7awdXVPU6++DqaoF+GnEngAASSTwAAHtJhZNVWxjuQGn+a/R6/0Cmeb0FayaazyDVcunt0jAZsbs4dbPbyM9dFRZjrIAoq1lkAMxVrpKE+M2vsjOyw1XM8zU04Sp+nqGEq0lU8y60lWvhKVpSCeEQTiejIRIdQuD1BFdNVE4+2sHTVJx9v6/CFk1UziDqap6ghZXVPU6++Dpr5KJxCyarlYX/eKYc6mmH/27Yixl84P9YftWM3MJf2T+wr0aWj01rb8JFrQR4cUKKf6I/UO2kJ7bHMag44n7wF+N97A495lxnqMkh+K+Y3LipBEgiQRIIkEW3n0/f/jp7Q//AJg9sv8A3loo8Z9Rf/QfL/8A4/P/ALOS9/8AS/8A9fcT/wDX4P8AaRX9P1P5U/cPdH8tl/VodFMFV5R97f1jOz3sq/U8Yu2Uq3b3jo2lpZ2j2yqqO63ajqxoG2svyCdyw4azMfjFS4urABKademkfWOx/o13j3uY7nDi/ScPI/5/MDGJHnjh+bJ9gEfOQXybvn6y9m9jRlh3Ob9Vysf9BhIJB8pz/LD4az8olfj272frD94nem7dccueWr2e2drXHW2dptsK6ttdLXW9R0RT5llaFU9/y5xaAPVQpVPQqOstOkeP7J7H+jHZvZQhuYYv1nMxH+fzASIPnjhrHH6EPL+sV+Ke+/rp3j3pKe2hk/R8NIsMOJw4/ry/NM+b/L5QC8qEIQ2kIbQhtCfyoQkJSNeJ0AAGpPjzj62vi0pSnK0yTI+J1X1BYpBEgiQRIIkEX23xcaHN1sfitI/pjGZaEj5RP7Ct23jfcQj5yH7VoDmH8vLsraP/AGeTX9GnH927Vaf6I/L2eds85ec5H8Sv2PtYHHtcUP6OOI+6ICt71Ov9kanW7XyUTiFldVM45++Dqap6nX3wdNVE4+2sLK6p6g5++Dpqpn6/b+mDqaqPUH21g6uqep198HU1T1Ptxg6aqZx/V7YWV1Uep198HU1ScQsrqpn6wdTVJ+vvg6aqPUEHV1Uzjn74OpqnqDn74OFdfJR6n24wdNVPqdffB1NfJROIWV1U+p9v7IOmqj1Ptxg6aqfU6++DpqnqDn74Omqj1Ovvg6mqmcfbWDq6p6njx98HU18lHqfbjB1dUnH21g6aqfUHP3wdNfJR6kHTVT6nX3wdTXyWaLBeaTHcNttXWoeLbr74SlhKVuKU85PropSBoBx8Y6qYOTMQF2MGx4hI9Fyo3CxZxxLziaht0JlDrlAVPJT7UhxsLUB0Biezk6Bm+Kvu4z16/BVFvPcWc8LlJr/zaapb/wB5oRicWQeCoyYz4rut5fjbn5bzQJPJx6Q/gsJie3PyKytBncLp0TWHsVxuNFUW/wCaWpxYPz8zTS3Tq44ww476LC3CNVFIBJjKU8pjUu3wUEcb2BDqh7l1Cf0agCSFJerZkqSoFKghCVAgjUEaKjPbEXPwWvcR+QfFYSn6ecc1wuJVJ4WCVSfpBwlUn6ef9kLBKpP084OEqk/SDhKpP084OEqk/TzhYJVJ+nnBwlUn6QcJVJ+kHCVSeFglUn6QcJVJ+kLBKpP084WCVSfpBwlVe+FPWWjqau6Xl1lAom0/KNuCdSn1n/iNNBKlOLSnUDgdCdfZGjMZSAhDxW7DGIJlM9FdtTuclTikWqz1NVoeDjzikqI9h9Bhp5YB6qB6RqGAN88gFsObVoh1Q2st+TuLt2qMPaRVPhPqVCfmWdNCZnEetTustuK1+JQAJ0GpjOgMaiegWNmNjHVXvQZnjmSN/p9aAyqoBbNJcQ2W3CRpo0/qpqfXw4pVGmWOeP5o+HktsZwnodFhrIbemz3mut6NS2y4lTRJ1IaebS80kq8VFCHANfbpHLx5BOAl4ri5MdZmKos/SNlgsKpP084OEqk8HCVSfp5wsEqk/Tzg4SqTjlCwSq56RWtXRjTxrKMD9tS0IWAL+qtH09D+xellyb9K53Rr/lXO4Mkci1Vutkfslj9QcVP3OL20/Pb4z98Avx7z+P2eb3WL+jnmP+sV0o566hIIkESCJBEgi29+n2lSu+vtBSlJUpXcJtklKUgqUpRyWi0CUjUknkI8Z9RtOw+XJ6f3fn/2cl7/AOl2vf8AxLf/AD+D/aRX79e836nXaR2NWt1ndrcBi77hrpVPWfaDBixke4t0XKfS+ZtVO+mnxyiWsaGqub1IyBxSVHgf589l/S/u7vrKDxG3MOPf5txleGGPwkzzPpASPwX9GO9Pqf2j2Lt5S5fcxlvgHGDGRLKfJw4EH85mL+Dr8gHfB9dPu17rlXjD9t697tt2arC9SjG8Furjm4GQW9Ycb0y3cNpqkrW01DK9F0lrRRsexantAqP2N2L9Be0u0xDecpEclzUWN8oHtQP/AHeLUaHpKZkfJl+Le/v+YbuzuqU9lwx/u/hjpXGT7khr+fJpLUeEaDwIl1XiaSpS3XVqW46+6t9951a3X6h91RW6/UPOKW6++6skqWslSidSY+5RjGIEYgCIDADwC/P+XNlz5DlzSMsh8SoirUkESCJBEgiQRIIkEX2hQQtCz4IcacVpxMjbqHHNB7SG0nQe2NeZ/Zm3Wkv8krlbKUIb3DLJ+QZIv4+K8/M0q6erzLL6ulKlUtVlOQ1NMpaFNrNO/eKx1gqbUAptRaWNUniDwMflWTiREmsCX+Lr9mQEZQjKH5TEN4eCtmeJYLKqT9IOEqk/Tzg4SqT9PODhKpP084WCVSeFglUn6QcJVJ+kLBKpP0hYJVJ+nn/ZCwSqT9IOEqk8HCVSfpCwSqT9PODhKpP0hYJVJ+kLBKpP0g4SqT9IWCVSfpCwSqT9IWCVSfpCwSqT9PODhKpP084WCVSfp5/2QsEqk/SDhKpP084OEqk8HCVSfp5/2QsEqk8LBKpP0hYJVJ+kLBKpP084WCVSfpBwlVkPJFejguLN66esttZHMGkU4D+Mddjkffl0XYZAfZiFTcEsNHeqypfr0h6loUoHy5JCXnnOKA7poS0lIJ0HiekZZ80oACPUrHDiEi8ugWQb5g9puTSfkG2LVUoUn+awyfScR4KS6ylSQpQHEHgdY0Q3E4n5tQt09vGX5dCoG32O/J/LFFQako0+e+YdDvq6cF+nr6Mk37sumnXjD9Tkd9G8k/TQZm1WC6+mVQ1VVRuaKXTPOMkgfmkUQlQHsmTodPZrHNjO0RLwK4hhWRHiFkbNleljmI0+uhFOFqHH96hpj5KMcfDInJMrfli2OI8VjGfqY5LlceqTiK6VKT9Yjn0SqTCDlKpP1/ZFcpVJ+p84OUqk/X3xHKVScc4WSqTDnBylSkw5wcpVJhBylSk/UxXLpVJhEcpUpMOfvg5SqT9TFcpVJ+vviWKVVQtS7cLhS/qqXV0HqgVAaWW1BB4BRUAT6aVaFQGhKQdIxmZ1NWsrGIsLdFlRzOrPblCgxizmrKBKhbDRp21eyZKUNLqXhzJCdeccX2py+bIWC5XuRjpjDqDuHdqYj9Zx11ulV+ZQD6AE68TpUocaWehUnXnF9mJ/JIP/AA8lPcn/AD46LrXljDL9aqq70FUxbKthHqOJCSytbqvyNP0SdStbhHBTYPh46RYTzQkIHWP8PFScMU43joQsUu1LtQv1X3XHXClCStxalrlbQltCSpRJIQhIA6COU7aeC41SdVxzjnBylUmHODlKpOOcLFKpOOcHKVSYc4OUqUn6++FkquanV/eaaVRCvmqaUjXUK9duUjqD4QMmDqiJJYdV6WK9ad35lxx2p9Z75p10lTr1SXFF915R4qedd1Kj7VEx+o+DmMnCbPIGAltcRbyeEdF+Ou5ccsXcO+xzJlKO6yhz1LTIUR2i6NIIkESCJBEgirmM5NkOGZDZstxK9XLG8oxy4U92sGQWaqcobvZrpSLDlJcbbWslL1JW0zgCm3EEKQoagxx91tdtvttPZ7yEcu1yRMZwkHjKJ6gg6EHxBXK2W93fHbqG92OSeLdY5CUZRJEoyGoII1BHgQuhc7lcr3dLjfL3crher5eKp2uu96u9bU3O7XSteUVu1dxuNa6/WVlQ4s6lTi1EmNmHDh22GO328I48EA0YxAEYgeAA0A+Cm73m632Y595knkzEuTIkkk9SSepPiep8V0o2LjJBEgiQRIIkESCJBEgiQRNQOKvyJ+JZ010bSCpwkDiQlAJjRujXa5T/AN1P/JK5mwAO+wg9Pdh/lBedGSP072R5C9SOB2levt4dpnUhSUuU7lxqVsuBKgFBK2lA6EA8Y/JmMyGOIl+Zg/xZftfJEXNfyvp8PBUWfr74zsVhVJhz98HKVKTCDpUpOIOUqk/WDlKpMIrpUpP1MRylUnHODlKpP1MLJVJxziuUqk45++I5SqTjnBylUmHP3wsUqUnHODlKpMOcHKVSfqYOUqkw5wcpVJhz98HKVSfqYWKVScQdKlJxzhYpVJxz98LFKpOOfv8AwhYpVJxzhYpVJ+sWyVSYc4jlKpOOcLFKpMOcHKVKTjnBylUnEV0qkw5wcpUpOOcRylUn9uvHzg5SvgsiZuotWDFKbwkp0qA5SsFH4cY6/Cf3k5eq5+WIGOI9FQMOyMWG4kVB/uNaEtVJ/wCSQfgqBzCDwV/ok+2M80bx0/MFrxSEJa/lK2GS4FpStCgpC0hSVJIKVJUNUqB9oIMcB1zmCpN6v1HYaNVZVrE3FNPTgj1ah7TUIQnh8IP5j4AfsjOEZZJMFjMxgHK1rqqt2vrXqp3QOVVSXFAeCfVc4IB4ahCToPujsQaxYdAuvIBLnxWQNw1lDWOU3Aenbmz+DYb/APIjj4JOZF/Fb88QKj0WNJjHJstFQkx5wslQkx5wslQpmMLeaVCTGFkqFEx5wslQpmMLJUKJjCyVCTGFkqEmPOFkqEmPODowSY84WSoUzGFkqEmMLJUJMYOlQomMLJUKt2Gy1d+rhS05CG0D1KqoWP5dOwD8S1H2qPgke0+Og4xhPKIB/FZwxXkwVdt+SO4nVXKgtfyV0ZU+EU9a40pCipPwkpUgocUhROks0uoGhjCURliJTcejrIS9okRYhV9eeZDQqQ3kFkZXS1KZg04w5TKdaOmpb9UvtLTor2oPj4xh7WM645Fx6rP3Jx/ONCrQrbTUV9BWZNRU1LT2/wCbWlVDSrK3KRoS6OLQSooTqfiHDQngNCI2RyiJGMkmS1nHYHIOjq15jzjbZa6hTMYWSoUTHnCyVCTGFkqEmMLeSVCTHnCyVCmYwslQpS4pKkq10KVoVqOBEqgrUHmNIlkYOvSm0L9Wz2V8OLd+ZsdmrFOuzFxxdZbaapW44pfxLWtTupUdZtddTH6f7VzjcdubOYILYIx0/qiv4My/IXeu2/S9z7yBf5s8pa9fmNv41UI9AvKpBEgiQRIIkESCJBEgiQRIIkESCJBEgiQRIIkEX0k6EkoKwGqolAISVhNHUrKApQKU6y+J4cI6zmsowcRus0i0Y4Jn8G/jXe9tYDued2uEByc0dPgQV5jvv+s+88nQB5514AHUAOuKWAD7QAqPylEtED0X7LkAZE+q45jGVlKhJjCyVCTGFvvSoUTHnEslQkx5xbJUJMYWSoSY84WSoSY84WSoUzGFkqEmMLJUKJjC3mlQpmMSyVCTGLZKhRMYWSoUzGFkqEmMLJUKJjCyVCmYwdKhRMecLJUKZjCyVCiYwslQpmMLJUKJjCyVCTGJZKhJjFsPsSoSY84WSoSY84WSoSYwsEqEmMLJUKZjC3mlQomPOFkqFMxhbySoSYwdKhZO3Co610WJumo6l9tiidS4phlbiW1+oAlKpAdCU8fujr8M2ckgfauwzwJqw8Fi5dNVt/npKtGn8VM8kfiUaRyL+o+9ceklkPGc8TZ7c5QXVqpeFMkm3qCdDKddKV1ThTK2lR1SrjoNfHhGjJjMpWDeq348lQ0lZF5vtVfK1dbWOzE6pZaSr+VTta6paaTrwA9p8SY2waIaK0ztMvJdCnUFVFMkcZqhgfi6gRkZFlBHVZD3Nclu9vYHgzbANB4A/Mvjh/8ARAjRgkan4rduI/MB6LG846xyLFaKpOOsLFKpOIlilUnHWLYpUpOIlj4JVJx1hYpVJx1iuUqk8LFKpOOsLFKpOIlilUnhYpVJx1i2KVSYRLFKlJx1i2KVSYdYOUqkwiOUqVkdTv6BgtOWAUVuSPlTryTosUcq1IAPiB6aSg8wqNDmeb0it5iYYvWSxxMI5FitFV2KitqaotmpqH6gtNhpovOKc9NtP5W0TH4UiIC3RUiR6q8sBuEl2ctLvx0d5p3aV1lXFCnpT6KtDqNRqdeHHQco1Zia2HUFbcP5qnoVaFex8lXVlGdf7tUvsJ14kpacUhCifbMgAxsjMkA+a1SgQSF1Jx1i2KlUnHWFilUni2KVSeFilUnHWFilUniWKVSYHXxi2KVW+2z2TM5Nglr/AJgNxx8Jx+6NqUA5pSIH6O+20VFZpjaA02XOCS8CkcY+6fS/mIbnjMnDZD/vG2kZRHiccy5P2TJHoF+efrBwOTb8hi57GHwbiIjI+AnAVZvWIB+9ZPj6ivi6QRIIkESCJBEgiQRIIkESCJBEgiQRIIkESCJBFQ8kuLVssdydXUqpauspXrVaVtjV/wDU6xAR61OhQkcFFTlRePH00uoJHxCPnP1N5mHHdvS2ESP1W7NAP6gYzPp4N56t0K+s/SPgcnJdwjkpg/pdoLW/rnSI9QdX8mC84q+mTQVtXRoUpxukqXqZDikyzJZcU2Cf3ZtE8dPbH59jMkAr9LyibEeq6c4jKxWNUn++LYpVJxEsUqUnHWLYpVJx1hY9Eqkw6wsUqUnESxSqTjrFsUqk8RylUnHWFilUn++LYpVJhEsUqUmELFKpOIWKVScdYWKVKTjrFsUqk8LFKpOIWKVScdYlilUmEHKVKTiFilUmEWxSqTiJYpVJ4tilUnHWFilUnhYpVJx1iOfBKpPFsUqkwiWKVSeLYpVJx1iWPVKpOOsLFKlJ+kVylVerO5mRNJSlfyL+mmqnKdSVKPNRZda0J9vhHAOKB81zRlmPJd5O6l0I0et9A5/qqqkj8HHXontR8CWWXvS8QFzp3OaV/wDeMfpXNTxIqG/91yhWfOJ7XkVfdHjEL6Oe449qanGUEk8ZW6Jzx8fFDMKZB0kl4HrFG8qwFS0OKx1TLiFhaV/I0xKVpOqVAs1KjwPSBGbzQHD/AEVaGXX6nv12NZS+qGEsNsoLqZFql+JSpQSUiZRHWNuN4RY9VqyNOTgaK15+vmYzssK+iT9YWSqT9fOFkqpn6+cLKV9En6wslVE/WFlapP1hZKpP198LJVJ+sLJVTP184WUr6KJ+sLK1SfrCyVSfrFslUn6/b9nCJZKpP198LJVTP1hZKrI+TfzsPxGrbOrbTKaZwA8EOFpxWih4DSXT9sacc/3sluyQfHEtoFjifr5xustNfRJ+v2+whZSqu3BWlv5Tayg8KZxdW4fYltpGiifYAJxGvNNsZW3FB8gVKyCpS/fLq4hWqTXVCQecjhbJHQlEZQk0B8FjOLzOniqNP184yssa+iT9fOFkqk/XzMLJX0Uz9fOLZK+iifrEslfRJ/br9vuhZKqZ+sLJVXrgWdXHA76LtRJ+apKhoUl3ta3VNM3KhmmDZWEr9Kpp1mdh2VRac+IAx2HF8ru+H5DHyWxlXc4z9kgfzRkPGMhoQuv5bidpzXHZOL38bbbKPtB8JDqHB1DgjzC3zxLL7Dmtt/UrBWfNIbAFXSupQ1cbc5pxbuNGlbhYUPYtJWyscULUOMfpftvuzi+5duDt5DHvgPnwyIsD5x/px8iB8QCvyd3b2Ry/a25PvQOTjpE0yxBMSPAS61l5g/YSrn8fCPTrxaQUSCJBEgiQRIIkESCJBEgiQRIIkESCJBFw1VTTUFKquuFQiiokeNS6NZ1cf5VIzqF1tSrT4W0a6n8xSnVQ8/3D3NxPbW1O45DIPeI+TFEg5JnyA6geciwHxYH1fa/Z3M91bwYNhjI24IvlkCIQHi58T5RGp9BqNcdwsjul6vdoulooH37NZUXBijsq6pAqUruDdMh+7OqUfRXcKs0yQ7oSENpQ2FKDYUfy7znP7zuLkcnIb8gTkwjEflhAEtAfBySfGRJ0dh+vuB7e2XbXG4+N46L4oayOgM5kAGZ8NWYDwAA16q27FQLpbeU3BinNVU1NZWVDZS2+lpVXUO1HpeopJmLaHAkkcNRHSzyPL5ei7uOJo/MNVrxd1IF2ugaCUNC4VfpoQJUob9dcqUpHAJSPCObGRqH6suFKLSOniqfP184tlK+iT9ft90LJVRP1+39EW6VSfrEslfRTP18/tpCyV9En6xbJVRP1iWSqT9YWSqT9fOFkr6KZ+vnCyV9En6/b3RbpVJ+sSylUn6xbJVJ+sSyVUT9fOFla+iFzr7/6ot0osu47t9Tv0jVZe3Hi48gOoomXPSDTahqk1DgE6nCOOiSAB7T7ONPckFofeuTDbOHn1XXyDBqJpsvWOuZ9ZJAVb6mtpyVgkAll1xwKSpOupSvUEe0acUNxL+f081jPbx6w6+S57ftqy5ThyuuylPFIJTQBp1hlRH5VPL9T1SDyCRy18YS3RB0GiyG1canVWbkuMVmOuNrW4KmifUpLNUhJRoscfSeQSfTc08OJB9h9kbMeYT9CtWTAcevUK1fU6xsutdFM/WFkqon6wslVM/X7fs4xbqVUT9YllaqZ+sLJVJ+v2MLKVSfrCytUn6+cWyVUT9ffEslUn6wslVM/X7eELKVXVn6eccey5FUn6RbJVJ4l0qk8W6VSfpEulUn6ef8AZFupVJ+nnC6tUniWSqT9POLdKpP084WUqk/SJdWqT9IWSqT9IWSqT9PP+yFkqk/SF0qk/SFkqonMLJVTP0hdKpPCyVSfp5wslVE5i3SqTmJZKrJGJXCjutrqsSuSw0Khanra8oj4X1fGWwTxnDiQoDhqkFPiY05ZGMhlH2rdjaUfal9is672e5WOpXT19OtAST6dQlJNO8jXgtt0CXjyOhBjZHLGYcFa5YjEsVTqZp+sdSxSMO1LyyEpbYSpxRJ4DgkHQdTwjIzADk6LEQJLDqsoMIbwOzVFRVlteRXVks09MFBfyrRBBKtNdAiYlahpqdANZTHHv70mH5AuQIDDFz+crFRcKiSolSlElSifiUo8VKJ9pJjkWXHqk/SFkqk8LJVJ+nn/AGRbKVSfp5xLq1Sfp5wulUnhdKpP0hdKpPCyVXftl4udkrWrjZ7hWWuvZ09KroahynfToddJ2ymZGv7p1SeUZRyzxzGTHIxyDoQWI+BGoWM8UMkDiygSxS6xIBB+IOizBQdw24lI36dU7Zrt/LCEu11pYbqAoDT1lv0Bo3Kh4+0uFWvtj12z7/7s2WMY8e8lOA/1kY5D8HkCV4vffTns7kMhy5tnGEz/AKuUsY+LRICrSe5bLRToaOP4wXkp0XWBN8Drq/8AmKYVeF0TZA9jbaE9I7KH1R7rjO0smCUfI4ogfeAD+K6nJ9I+zp4vbjjzQkB+YZC/x1cfgu+O5y+emhKsQx4OJSAp1FbfR6ih+ZxSHbg8hBVyTKkeyOTH6r9yRk5htZDyMD+0SC4k/o52rOIjGe5jIDqJR+8vA/xLu0/c2+GEisw5t2q1VO5S31VLTEa/CEU71orHkkDx1eVr0jkH6u89b5dvsq+oyv8Af7oH4LjR+inbVfm3O9t5iWJvu9o/tUK7nKkqTJhzCUTCcKvbillH7wQsWtKULI8CUqA5GN+P6vcr/pdptj8DkH7ZSXGyfRHhSf3W83QD+IxnT7IxVUV3P2zjJgd0HKfMqBf4yYS3EH1d5YHXa7b78n/vLYfonwJ/Lut0P8X+RcJ7nqT93BqrpNlbB/HTF0xsH1f5Hx2e3/xp/wAq0S+iPEn8u93AH9mBXwe55n2YO8Pvyhs//wBtiMx9YN947LD/AI81ifohxnhvs/8AiR/lUf8Aeeb9uEOfsyZA9+OmKfrBvPDZYf8AHl/IsR9EOO/nb/Mf/hx/95fQ7nqbX4sGqP2ZU0D54wYwP1f5BtNng/xprMfRHiv/AJ3P/iR/lXMnuft/7+C3A/6mX0af9/DnI1n6vcofy7TbD7cn/vBbo/RPhB+bd7k/ZD+RdSq7nQp1BocLW0yEaOorsjRWPKcmPxNu0uP21ttuXT4VNrOvGb2APq9ylWO021/N5t9z/wAaxn9EuFMwY7zcxxtqGgS/xI/iUI7nB/2mFKPP0sjDf4T2F6NEvq7zx/Lt9kB6xyn/APtC5EPop20P85ud6fhLEP8A+oqqYj3AXS+3ZVDU4vaQwGXXUqTW3NLgCdZEuqbfaCyfAlIRr7NI4+5+rncVXw4dpD/Bmf2z/lXJ2f0W7YjJs2bd5PjKA/ZBW3X9ymRUt0uDbOMYy5RtVTiKdh97ID6bSDpKqoYvTFS5z1UvWNGT6p905ID25bfGW1bGC/8AjP8AguXi+kPZ+LJKU4ZsgJ0BmQB6CoC7TW6e8V1t6au30mNW1LoW4wFW2neqnW1fk9NF0XWBASOCCrRR8SSeMdZu/qV3VuInEd17YP8Aq4RiR9oD/iu12P0s7Q2chlhtPcI/1k5TB/wSW/BY0Od7n5Fc3mqupfu9wpipDwrKWn0pADoUFUjbVMjgNANAQBHjNxm/UZDud1knPNMuZSkZSP2nVe522EbbFHa7WEMeGOgjGIjEfABdetznL7O8Ke526hadImSHad0IcRrpO2tmoSlY1HiDwjVGOKQeJLLeZ5IlpdVcmKZnW5DVVVLU0dKwlikW+XadTwOuigElDi3OB05xryRjAOCVnjmZlj5LCVW/6tVUuePqVDy9eczij56xyxJgy4pjqV15+nn/AGQupVRPC6VUz9IWSqT9It0qk/SJdKpPC6VSf7awslUnhZKpPC6VSeFkqk8LpVRPCyVUz9IWSqieFkqpnhZKr6Q7ItK9AZVJVofAykHQ8PA6QulVk+mp8yzJDb7lUbbalpSlkJUthhxCAEAtMMn1qjw/Ms6Hw1jQcmLHp1kt4x5cupLRXc/6dWhsyVd+/n+3U0jJm6oedcc8eZjH9TI9Asv0wHWS4ncJv1p/vePXlb5R8aGkOKpXFgexsh12le8PaU6xRuIS0mFDt5x1gVRbtl9bX2assd8olIuTbzCm3w36KgtlSioVDCtJFgHgpPBXL2nKMQJ2iflWEpExMJdVYM8brrVVJ4XSqT9POFkqk/SLZKpP08/7Ilkqk8LJVRPCyVSeF0qpn6QulUnhZKpP0hZKpP08/wCyF0qunOOsaLLkVScdYXSpSeFkqk4hcpVJx1hdKpOOsLlKlJxCxSqTjrCyVScQuUqk4hcpUpOOsLpVJx1hZKpOIXKVScdYXSqT/fC6VUzjrC6VScczCylVE46wuValJ/vgJpVJxC5SpScdYXKVKTwsUqpDmhBSVJIIIUCQQRxBBBBBB8DC6VKvu3biXekYTS1zNLeGECVPzqNXgkDQJLo1S5oOGqkqPWNUscCdNCtgnMBjqF2ntya1LakWy1Wy2KWkpLrTQdcTr7U6pab/ANpKontjxJKpmf5oAVhVVdU1z66qsfdqKhw6rddVMs8gPYlI9gGgEbQQAw6LUQT1XWnjK6VScdYl0qk46wuUqk46wsUqk4hcpVJxCxSqTjrC5SqTjrC5SpSeF0qk46wulUnHWFkqk4hZKpOOsLpUpOIXKVScQsUqk46wulUnHWF0qk46wulUnHWF0qVM46wulVE4hdKlJxCxSqTiF0qsl7XaKvVes66NW9KtfYJqlCfcY0Z5/KPit2GGpPosfOvoVXOvOgrbNWpx0eJU2HiVp06oGkbxItotVdftW1dJUMv0tK9TqSthynZU0pBBSUemkDQjgdNNP2R15lqX6rmAONOi5hIkqUlCUqWQVqShKVLIGgK1AArIHtOsLK1WMt0XKb9Mt6Vy/NGsWafwnDcifmNPbKfh6RvwSNj5LTmjoPNW/tooIeyGoPgza29Ndf31vA8B4xlnn0HqscMevmyxiF6gE66kAn7433K01KTjrC6VScdYWSqTiF0qk4hcpUpOOsLlKlJ/vhZKpOIXSqTiFylUnHWFilSk46wuUqUnHWLcpUpOIlilUnELlKpOIXSpScQuUqUnhYpVXFb7/eWqNu0MXN2htz1SlCn/AI0oZLx0Uk1CEKdbaIGpSkjw1jCVSbEAyWQt+UHRXsrDMUZlTc8qnrFhJKk1FGhKioBQIS6mpcKTrw1WCekavfyH8o0+1bfZj0MtVwVlPecIbaudqvjNwtDjqW/QdcSpDk3EJ+XLi0r4fvNEFOvEDWKJxy/LIfN/DxUMJYw8S8VZV/yCoyGu+eqW2mdG0NNNMj4UNpGuhWfjcJUSdVakeA4ARtg0AwWuTyLlUOcRlYrGpScQuUqk8LFKpPC6VScQuUqk46wslSk4hcpVJ/vhcpVJxC6VScQulUnHWFkqUnELlKldWbr5xx7rfX0Sbr5wujJN184XSqT9YXSqTHnC6VCTdfv4wujJN184XRkm6+cLpVJusLoyTdfOF0qkx5+cLoyTdYXRkmPhrC6VCTHnC6VSbr5wuEZJuvnC6VSbr5wulUm6+cLpVJ+vnC6V9Em6+cLoyTdfOF0ZJv8AS84XSvok3XzhdKpN184XSqTdfOF0ZJuvnC6VSbr5wulUm6+cLpVJuvnC6VSbr5wulUm6+cLoyTae3T9sLoyTdfOF0qk3XzhdKpN184XSqTdfOF0r6JP184XSqTHnC6VSbrC6VSbr5wulUm6+cLoyTHn5wujJN184XSqTdfOF0qk3WF0qkx5wulUm6+cLoyTHn4wulUm9mvnxhdKrJ+2yvTGQ1JP/AA7eE66+Gigvx/ZGjNPoPVbsUep9FjJS9VrOvi4v281mN91pqsnYBlnybiLHXu/3SoX/AHF5auFO+o/8Ak+DTx8OStAPGNGUP8w6rdiLGp6LKN/yGix+iVVVawt1WqKalSoerUugcEpHsbT+8rwH3kRpg8ywW6REA5WuV3vVbe6xytrnZlq+FtsE+kw0CSllpPABKdfvJ4mOZEiIYLiyeRcq+sGX6Nkyypm0loQnXX+EFXP/AE41ZZ/NEeq2Y4/LIrGIV4DX2c+UbrrTVTMecLpVRN7dYXRkm6+cLpVJv9LzhdK+iTHnC6VSY8/OF0ZJuvnC6Mk3XzhdGSbr5wulUm6+cLoyTdfOF0ZJ+v2++F0qk2nt84XSqTdfDr4QujJN16QujLvW2ifulfS2+nP82qdS2D4yJ8XHCP3g02Co9BEllEQZHoFYwsWCvLLrlSUDLeJWpDQpLepKq5+VKnamvCfjPqaa/CFcT4nUD2RrxyMj7kuq2ZAAKAaK07euyparP1RuvW96WtF8m6y2gvDUSv8AqNLISdQdeI0BGmukZmcnFeiwAixsC6+bTcG6G4UNRUsoraWmfDi6R8lbJSrQLUlBMswA18NDpFlJwQNCpHQuRorgzS1U9vrmK63n/wBWXhkVtLpwS2pwTraHEgcDME/ug6RhjykhpfmCzyYwC46FWbN1842XWuqTHn5wujJN1hdKpN184XSvok3XzhdGSbr5wujJN184XSqTdfOF0ZJjz84XRkn6wulUm6+cLpVNYXRl1Z449gt9VE56RLBWoUz9ItgpVRPCwVqk8LBKpPCwSqTnpCwSqmeFgpVRP90LBWqmfpCwSqT9IWClUn6QsEqk/SFgrVJ4WClUn6QsEqonhYK1SeFglVM8LBSqif7oWCtVM/SFgpVJ/uhYK1UTnpCwSqmfpCwUqk/SFglVE8LBWqmfpCwUqk/SFglVE56QsFaqZ+kLBSqT9IWCVSfpCwSqvnbLbfOt5M/xPazbHG67MNwM5uqbJieMWv0v1C93VVNUVYoqQPuNM+qaalcX8SgNEGNOfc4dtilnzGuKIclb8G1y7nIMWAWmf/Yt+F/R0+pigrQntK3AqH0TgUdJXYtVVzriASaenomL85U1FUop0S2hJWpXADWOq/4k4f8A1w+6X8i7L/h/lPDGfvH8q0td2B3upd4rd293TazNbDvddshpcUodscjsdZj+Wu5DW6mktZtl3RSONv1AHwlRCDzjsf1+1O2O7jOJ24D2GoYdei4P937n9R+mofe006HUsOrM5I6rG+QWO8Yrfr3i+Q0D1rv+OXa4WO92yplFRbrta6pyiuFC+EKUn1qWqZUhWhI1Eb4ZYZIDJAvCQBB9D0XHyYpY5nHPSUSQR6jQqkT9IzsFhVJ+kLBKpP0hYJVX7tdtlnu9W4WKbU7W4zXZjuHnNyXZ8Txa2KYFwvlzRRVdxNDR/MusMl80dC6pIUsTS6DUkA6dxucO1wyz55VxRGpW/b7XNusgw4BbIfD8P41aV4tdzx+8XjH71RPW29WC7XOxXm21SZKq23izVz9tutuqkcZKmhr6VxpxP7q0ERshlhkgJwLxIcLDJhnhmceUETHUH7/2LI1t2R3Yu+09XvnbsHu9RtLRbhWrah/OQlluz/8AUS90tFW23FWVOvIfqLlUUtyYc/loUhCXUzKEaJb7bQ3A2spAbgxs2vTo634+P3OXD+oxxJxAkPp4AyPj4AEro7u7R7lbC7iZLtLu/iNzwTcbD6pqjyXFLylpNytFS8wipaaqksOvNBTjDiVDRR4GMtvusG7wjPt5CWKXQrXuNpm2uU4s4MZjw/H9hCsO20Ndd7jQWm20y6y5XOsprfQUjWhdqqyrdSxTU7QJA9R51YSNT4mNs8kIRM5logOfgFqjjM5CEdZEsPiVdG4+32a7R59l21+5GPVuJ59gd7qccy7Gbl6f6hY73RpbXU26s9Fx1n12UvJJlURx8Yww7jFuMUc2EvjkHB9Ftz7bLtsntZgRMfxEg/iCFZU8bbBaaqZ4WClUn6QsEqk/SFglVkuh2e3Oue0V/wB+qHD7nU7QYtnFs22yDOmwz+j2vObzbGLxbMcqCXhUfPVlsqW3kaNlEqhx14Rx5bzbx3EdoZD9RKJkBr0H4LlDY7iW3O6ET7A8dPX7fA/cmAXO0UtNeqS6VzNEa5KG0F5wNTsqbLbnpuKSUzpJ8P2xcpJIMVrxiIBBK73+DMSfJNLk+sxJSFVFC54nX2IZ10/ZE9+Y6x/ar7OM9JIrbiiX8VNktOfAp1ZYcOo4gzN16TqD7dIv6jzCewP6SyTgHbJvJvrfbrYtt6Gt3Cv2OYhfMzulDRIcdqbbhuKtMPX28uzuqSigtTVS2XCNVaKHAxozchttrESzGsTIRHxPQLdi2OfcyMcXzSESfsHVYmTtfkjlL89TGkqqH+TrWtJrhSINSkqp0O1LlElhp19KSUJUoKXodAdDHI/U4wan8y0fp5tYflVcpLLXY3huVN14ZD9Sy6Wyy56ok9JpAJUANCVJPCMDkjPJGvgsxjrAiSw1P0jk2C41Un6QsEqon+6FgrVJz0hYJUKZ+kLBSqT9IWCVSfp5wsEqonhYK1UzwsFKqJ4WCtUn+6FglVM/SFgpVJ+kLBKpP0hYJVRPCwVqk8LBKrIW2yUqvz76khSqO3PPt6+xZWho6felwiNWaQp9q2Yo/M/orEqn1u1VU64ZluVD6lKPiSXVeP3CNgIAZYGOq4Z+kWwWNVE/ThCwSqyLdFGo28sL7uinKevqG2lnXUoXUOtHQ68QlKAPuEaRIDKQOi3GL4gSsdT/AHRusFqqpn6QsFKqJ4WCtVM/SFgpVRPCwVqpn6QsFKpP0hYJVJ+kLBKpP0hYJVRPCwVqk8LBKpOekLBKhdScRosuRVJxEslSk4hZKpOItkqk4hZKpOOsSyVKTiLZKpOIWSpSfpEslUnHKLZKpP0hZKr6SStSUISpS1kJQlI1UpROgSkDUkkxLJUrnepayn9T5ikqWPSWG3fVZcQG3FAFKFqKZUqUDwHiYCYPRWhHVdaccoWUqk4i2SpSccoWSqT9IWSqTiJZKlJxCyVScQslUnELJVJ+kWyVScQslUnELJVJxEslUnHWFkqUnELJUpOIWSqT9ItglUnHKFglUnESyVK9FvpGrH/5l/ZlqP8A8Xhw5/8A9IZVoP2x0/PyfiM4/qruuBif7wj8B/lRVg7v7f8AddeO47eChxDBe5mvuFx333SRjLOP45u0pyrfe3DyJy2Cyu0lIhk+oiUsKbUEkaSnTSMttm2EdnA5DjDYw7t5eq3bvDvzu5ezWrjpQ+AfQOSXd2BLv4r2s3Grcto+6T6G2FdxteLt3vYW7aKHfZ64VQr8zsWD1mcZXXbQ4juLUzmoYzOz4nU0xqaasT881MPUUQRHnYU/Schk2gI4+QNfIyqbGPo/T0XcwJ9zBj3JfeCMX6u3uwq/rV3fXU+a8sMU7ZMX7ju7jvQuu5m6dLs1spsPkG727G72cItKMmyhGO0WaXumt9gwTEDX213JssyO5NfLUzKXSGlKC1IWkSx3kuQls9htoYYe5uMkYRiOgcxGpPkujjxw3m8zZMsxDBGcyT46S9W8x19fJjU7N20dkncZgm9b/aVub3KY5uxsftZle9L+F9ymK7ZrsG5uA4Dbqm+ZuMXv+1t0qX8WySy4/Qv1jVLXM1bNV6Yb9ZsqnFO/5La5scd9DEcOWYg+MyeJPR7dQsTsNhuME8mznkGSETJpMxA1I08fwWvuddsNos/ZXsf3hYXl9xyKgzncvNdm91MUuFqpaR3brcCwUjt6xxNHV0VbWLrrDlePUFVUMvVKKdydmUJ4xycHITnyOTYZYgGMRKJD6g6a+oK0bjjYYtlDd45EuWkC2h6hm9CPUar7xjtdtdR2Qbkd4ubZhXY2zRbv41spsvilHa2a9rcnLn7Zd77m6rjWrfaes1vxGz2xBLiEOhb9S2hUsw1T5GX95Q2GGIPyGUz5DoG+J0Ujx0Y7E7vNIxJMREN1fU/ZViFrxtJuhftld09ud3sXq6qhv+2ebY3m1uqKJQTVTY/daavqqZpR0H9/oWnadXhql0jh4xy91ihudvPBNjGUSFx9jklg3UMkert9p6fcWP2Lf76u22dpxLu/r908FoKdnb7u2wLBe5/by22qkWhmkRunamjkljBbBZqLoxuHb7qlxLfxBSwCCTMrq+B3Mpcf7OX/ADmCUoEnxr0P3Ln8xtgd8MuPTHmjGQ9H6/d4rK/f0l7YzYH6ePYTQVFTbrvjuJWzuN3ffQsUt2pN0e4O9M3KyWi+25othNywrB6m1+gHglxtQ04HVR43GEbnd7nkyxBlSPlWAZwfXr9i5m8B2+2w8fEtL2zKXmDJ5EN6AGOvhJWnvt2KbjZp9T/dHtHe3vue4F3xqrrsg3E7i93lKadtG3+K40xkeZbhZosV9WVU2MWJK1+kKmd8NhCCFHSNm05XDg4aO89sRB0EIdLEkAD4laN1xmXPyUsPuEgHWUi5aoJOpcsP4ho4VU2c2K+m7uhvZhG0Wyvcd3E2Pd1GZWGiwLcDe3bnAbTsTu/mlHdqZu24rbKTF7/VZ5tqnM7khNPbKy7GtaaW8gPpB1jHc7zl8e1nm3GLEcEolxEm8QR1L6FvFllt9pxeTPHHt8k/fjIF5ANJiOnk/n4eK7Pdr22ZT3N/WC729r7PfbFhNpsW9u6WV7mbk5P827iO2W3uDWli55lmt9NA2uqqqS22+3OilYblXXVamWEqQp0KGWy32PY8JhyyBlMwiIxHWUiAwWW/2WTecl7cSBD5ySfADJNz/DzCs/Du2r6fPcRlz+xHbTvz3G0G+1xRcqHa3Jd8sD29tWym9WU2qiqKtnG7OrEbxV5rt2/mAo3E2Z66oqm3HlNMvemp1JGWTkOV2mMbveYsR2v84QJvEHx10LeLLUOP47Pk/SbbJkG68LAVJHh6P+HQ66LAfbR2i0W6H/W3Pt9M+/6G7C9sb9PQ735i3aP8T5evKq663iy2bbDbfGE1NFS5PuDkFyx6vQyhypapKZqkdefWEABXK3nKeyMePax9zc5vyDoGHUyPVg64224qU5zO5lTDjZz4m2oAHmR/Ksz2Ltb7Q+6KxZ/auyPcbuFpN89vsNvu4lBs13L43tvTq3exHFmkVOTI28zPbW6uWm05fZqBYqRarmyU1bU3pVBLZB48uQ5DZTieRhiO3nIRtjMvlJ6ODqQfMLfHj9jvIS/QSyDLEO0gNR6EdD4seraLy5beQ62h1szNuIS4hQ8FJWkKSR0KTHeCQIddLLHKEjCekgWI9QvXDbtY/wDyPu5c8vqGbSD/APxPjpjz+Y//ALHh/wDAl+0L0uKP/wBgP2/tyL5ynst7RO3vZntm3y7mt993bnT9zGzNh3KxXZjY3EcLf3PbqbglgXq7XC/5rd28SseCWOscVStuPtO3KuqAn02UtkuJsOU5DdbnNtdpjxg4shFpEsw8GGrn7lonxWy22CG53M5mEojQAO59engdOpYnoCsbbmdn+xlpPatvXtrvDml37Oe5rcZe21yzXNsWxyxbr7I5PY8htdozfH85x+ivtRjV7qLBba9y4MXChfRQ1lNTL/4apUnbi5PdSGbb5oQHIYYWABJjMeBGj69G6utWXi8ETizY5yG0ySA1ABi4fX7CCCNCD1WtPdL265N209zG6Pbes1eS3rB80bxfGrizRLol5vRXduhqcRu9spwpSVtZHTXNj0i2pbalqISo6Rzdnv4brZR3ZaIMXI8iOo+xcXdcccG9G1cmJPVvBzr66fN8CvTLBuwl/b/vJ3O7aMA7oc/xTL9uux7Kt3N2slxCjat9fb83RiNnyrLdiahNLeWk1WOqtl5oGqyoUv1FOFQLRlEdNl5U59hDdZcMJQluRGILszsJ/HyXa4eNGHdSw48khIbcmTebOY6eHgq32y23tKuP0hd8bhuTu7vni9XV77dvFVuO9iO1uN5G9iGesjdYYDi+E1dwzO1v37Er/akIdulW98m5RPAJbZfABjXvMu//AL7xyxQxk+0auTrEmLk6dX0HmFyNvttnDYZcMpyMY5ZOWHWImxAf+iLehGjrUbbDt92jt2wzHc13hbv7p4vs3nOXX7DdittNprNj973o3rqcXe9LIMoZXlla1hmEYRjz5NK/W1q6lx+vZeYaa/lzq7DNvtwdyNnsMcDuYxBmZPWL9BpqSeungurw7HCMB3W9nMbckiIA1LFn8QAPxPT1tDfntm2fZ2PtvdT2mbi5vuFsmzmjG2m5eJ7r2Cy47u9sxndfQtXGx0+TpxisrcVybFcnpX0ChutApoGoV6LjKVJKo27Xkc/6r9Dv4xhuGeJiSYyHo+oI8ite547Edv8ArNlKUsHiJDUfxEddfQjqtDpxHbWXU1SccoWSqT9Ilkqk46wslSk4i2SpScRLJVJxCyVScQslUnELJVJ+kWyVScQslSk4iWSqTiLZKpOOUSyVSfpCyVV44JdWrbkVKp5QQzVpNG4T4aucWgdeGin0pHHnGGT5o+qzxhpqm5Pa3LNe66kWkhpTqqilWfB2neUVpUk+HBRI09kWGS0X8VJwMZMqBP0jOyxquVht2peap2G1OPPuJaaQgaqUtZ0AA9uniekQzYOUESVkbNnGrVabDjDSgp2kbNVWBJBCXHfiKeHhq+VKHNJBjVjLyM/NbZxaIh4rGk8bbLVVJxyi2SqTjlCyVScQslSk4iWSpSfpFslUnHKJZKpP0hZKpOIWSqTjlCyVScRbJVJxEslV1p+ojjut1VE55+6JZWqT9YrpVJ+vug6VSfqPKDpVJ+vuiOlUn6xXSvok55xLJVJ+vhB0qk/UeUV0qpn4jiOJ0AHHUnwAHiSYOlVzsVL1HUs1DZ9N+mcQ8gOI00Uk6pmQoAlJiO4QRYur6yTP3r7bEW5ulTTB/wBNy4LUUr9R5o8BTjiUtHmrVWnD2anXGFS51W2czIMyx7P1H24/dG2y1VSfr7olkqk/X3RXSqT9R5RLJVJ+vuhZKpP1iulUn6/b+mDpX0SfrB0qk/WDpX0SfqPKDpVJ+vug6VSfr7oOlUn6+6DpVJ+sHSqT9YOlUn6+6DpX0SfrB0qpnPP3QdSq9FvpGK1+pj2Ygn/8X0keHinEcqII6giOo5yX/wBrzf2V3PBRH68fAf5UVcG/31LfqFsbv754ix3s9zDGMUW7W6uPUdhY3aypm20ljoM5yC3UNppqdquQligo6BlDLTadAhtISNABGG04vjZYMczgxmdQXqOrfBbd9yO8x7iWKE5CGn86fiAT/O8/DoPAMsXfTwvF2v31Eu0i8327XK+Xm6dwWHVVyu95r6q6XW41S3KkLqK641zr9ZVvqA0mcWpWg08BG7lYxhxmWEA0RjloPgVq4qU8m+jPISZfL/lwW3WEbW7DW2+fUg7xe4DBb7vtjnbzvy5i9i7fLFl1ywe05PkW4u5VxttHmO6WQ2Onqry3tvjSngV07IaTU1CVIccKDLHXZNzuDDa7DBIYzkxAmbOQBEaAea7DBtMd8+7kLzEpNB+vzSYdCXcaNqSQB67Zdkncqzv5tl3+0uz3YV2v9u+0mKdh3cxUZtuptDhOWv5iupc2qyeksGN3rc7MMrvjFUqtujrSnKShp6aoWjVToLcxjjbzbR2+42xybjJlzHPDSRDM/UADw83K24s2fPt8wlhGOAwzHjYaaDU9C+mgdvFaE/TZuSN6Nre73sHuK1VFR3D7Uv7pbMW5hSE3Kt7g9haF3Occs9oU+hxhuqy7ErLcrSEAJU6urSmYa8Ox5UexnwclHpjnWXlSehP2aMuv2J9/Fm2EhrKJMf7UTYa+rnr4Blz/AFOKhjZGy9rP0/7M+y2e1LaRm87zIoHW6dm59yO75ocg3EF9tjBcbTfcWt9roKMLccdcSl1YBSFEFw7555uRm75ZNF/6MdAx8j1TmDHHHHs8YAEXJAPSR1OnTR2HU6ff5OT9QemgPl7Y7110VV+j3sr2ctv1Fu1Ts0xy+l16q7Au6CrsG766W4tGup+1LOKZ/dGkzC9PvFNfS2zG8yxq7UjaG50JTU6aqBkb8nv8x43eZ5R6bjEDHT/SD5QPV+p/g/q9j/vu2xlg+KZiemkJakgeDflHXR211Hj93U9wdR3S97G5G/TjzDlt3A3xo6zFE0zS2GKfAbXlNBZNvqNtha1ln5LCrdQtEJlRMgypSNEju9ltxtOOhgDuIB/idT+JK6ncZJZ+SyZJDX5/BukSOn2L3r3opKrNfqv/AFddisXbFXurv12pbo4PtJZw8zSv5PltJt/+qOYjQVb7jLTdzyWjHy9O2paQ+4ZI8zhPt8Xs9xP/ADOPMDI+QsQ/4r0WRp7ncYYh8khID1eGMga/2Svz1dpe2O4W53dVsxtlg1ovi87/AOqWKisp6O31f6nh9PZsgo375kl9YCWaix2zFKancqq2pqPRRTNMqWtSQNY9Rv8Ac4IbLJOZFDA+PVxoPt6LzOz2W4/VQ+SQFxqxbQh2I0LeLdPFe+GK31i2fVp+tdhDGEYdu3nme4Hvdj+3u1WapvdRZd0ciw3LsEz/ACHFkUuM3rH8iudZccPxC5Lap6KsafqVJCAVBRSfOZATxOzmTKOMTxvIfzQYkPq40JHVekMgN5khEA5fbysC+v7w6aa6sei0Y7VO5Ov3e7jtsNtu3/6WXYcjfOgzW2XnGUOWjuKspwnIcFrE5Icgyevqu4BDeL23FKmy/MVb9ZI0yGSleusp5+722HDtpZc+73BwtqHgXB8AKauuvxZ95lzjHDBjsTpIiYBY9Xvr69VVrvX5b3Ddhff5jtgtmPXHdrbfvksfcpvLiG2iaqpsDuD1bO4+L5HmOF0636yuvuJYhl1QQ66lypUy3dG3lqCXQpWMDj2vIbfJJxt5YKRMurggsfVlZRy59nmxsPfGWxA6NKBiCH8H8+g1WDPo+2G6VXejh+8qa1617XduWK7g7wbyZdK4MateC2nC71aHrLfruHGrdRf4nuF6Yp2Gn1n11ggIVodOVzmaB2XsxY5ckhGI8XPiPHTrouNw+3yYdxLJmiYxjFzYEaO76+YBjr/SXmfdLjS3O53O5UFELXQ3G411fRWxKW0C20dZVO1NNQBLQS0kUbDqW9EgJEvDhHb43jjjGTmQiP2Lp88Y+9OrVsenTr4ei9YNulf/APDvuZOvh9Q3aP8A/wBTY7HSZT/+w4v/AAZftC9Bjj/9i+w/tyK2fqPrP/S76ZYmOn/cK28IBOoGqqLXQHw19ukZ8Sf953f/ANRL+JauWH+6Yf7Ef+2uPc9Db30dO1RpwBSHu7/uDacB0+JC7M+lSTzBSYxwMedzP/qR/lFZ7mUsfF4Zw0lGcSPQiEGXp/tLtzineC32PfU2zxmivOLdru0GX2rvNNaVUrdy3A7SKOkuGygRQOvU9OajPb1kFtp0PlYbqVUcpUrQy9TmzZNl+o4rG4nmmPb9BM/MT8C/2LsowjuDi35f28UJE+f5Q3T+lBnBPVwwcrUL6Wm6WRb398feZvDllXVVuQ7m9oveNmtyerVIVVNqvtLZqyjonS3q3/6rty2aVISSkIZABIGsdhyuKO343b4IBoxzYx+K4GxynNv9xM6/upD8C7ehLn7VrJtMsI+jh3bOLWltH/ex7Pk+qshKAf0XdTxWeA01jfmkBzOEkt+4P+VFZYoGWDcxiCT72TQf+HNbj7rb/Ylin0/Pp97mMdnXbX3F7aY3gm4OzeVZNu/a90bjc9rd2rXulmWS1uL1i9t9z8Mx2x0WW47fKK80prWF1tUqvUr1SnRtHDxbe3JbjFPNlx5TISFTECUSNPzRJLdNC3gqcuSOxwywY4TiImJDSJsDqPlkOvVmfqVhDO94Nzdwvp0b0ZZg/Zp2j9tfbXnm8m1GIZFnG3I3VsmdZ/neG3emyi0Wrb63Z7utl9tyW22iZJvVRS0bnytOkpLiFJMcjFi22LlIQlmz5d0IEh6mIB82iDr4LVky7zJsZz9vFDCXBewPSQLPI6jXTzZePs/UR6F15qqifr5wsrX0Sc8/KDpVJuHj+2DpVJ+sSyVSfr7orpVJ+sHSqT9fGDpVJ+sLJVJ+vug6V9En6+6DpVJ+v2/piWSqT9YrpX0Sfr9vdB0qpn6jyg6VQLIIIUQQdQQdCD7CD4jSI6VWTqLI7JkluYtOVr+WrKdIRR3hOifABI9RYSoNKIAmCgW1aewiNbSibQ+5bRWYrMa+a+VbeoWfUpsmtTlKril5ZSlcvjrKh5aFEA/xDWJ7zeBT2PIhlzt1uLYUlblBUpv9/kU2h4BPy1IojQqBTMhoA+IClrPMCDyn10irWGMONZLGtbX1NwqnqyreLtRULLji1acSeASkeCUIA0SPYBpG0EAMOi0kOXPVdWfrFdKpP1g6VSfqPKDpX0Sfr9vfB0qpn6iDqVUT+3WDq1Sfr7oOlUn6+6DpVTP1EHUqon6+6Dq19En6wdKpP7df2f2QdKrqzmOPZb6qJzB/VKqZzCyVSeFkqk5g/qlVE56QslQk56QslQk5g6VScwsrUKZzB/VSqvLHHWrba7zfyww9W0aqSltxqUB1mnfqi6XHy2rVC1oQ38Oo4RhIuRF9PFbIRYGXiq++zcMnslf89+l1N2taKatpq+ldo2p6V/1A5S1LjJbYS4kNa6K0UAQIxEhE6OAVkYmUdeoVtYvYjcnna+uaWqzW5Dr1YttaUl1baQpulTooOD1lEAqHgPbGU8jaD8xWEcb6noq6U0V6TcLXUY1T4/cKahqK62O0yVoWoUqSVM1cx0dQtKfzADX2D2xjYhiCSPFZ1EnBDFY1mMbXC1VCTnpB1KhJzC3qlQk5hZKhJzCyVCTnpB/VKhTOYWSqTwf1SqTmFkqk8H9UqonPSFvVKhJzB/VKpOekHSoScwslQpng6VSeD+qVUTnpB0qFfe2O5+ebM7gYnuptfktfh24ODXT9axPKLX6X6hZLp8rU0XzlJ67bzPq/K1jqPiSoaLPCNWbFi3GI4cwtjkNQt2DLl2+T3MJaf8D/ABK2L3fLpkl7vWSXysduN8yK8XS/3q4vy+vcLze6+oud0r35QlPrVlfVOOr0AEyjoIzgIwiIR0iAwWGSUsszOZeZVawLPsv2vzbFdx8AvlXjGb4TeqTIsVyK3+n89Zb1QlRpLhSeqhxr12CsyzJUOPhGOXHjzYziyh8cgxHossOTJgn7mItIfxEH9oBWXdo+7XuF2L3Nyvd7bLci5WLOM+Rd2dwKqrobTfrHn1Jfax64Xa35til+oLjjOUW2srqhbxYrKV1pLiipIBjRm2e1z4o4ckfkg1fAhtAxGo+9b8W83GGZyQkXkS/kX66Fxr6hZ+qvqp9972YYrmVBvk9jL2GNXxmw4lheFYFhu2DYya01ViyB257W45jVuwO/1N3s9a7TPuV1BULW04pOvGOP/dHH1MTB7M5JJlp0aRLj7Ctx5Pd2EhJgPAAAa9dABE/aH9Vnn6e1uwy0bwUn1Hu43uS2kxOybQZxk+4V22y/xc5aN/N1twrTbq2uxnHsQwKw26lbFky7JflmKh9DrVI3QKeC0hGojicjkkcP91bXHMykAAW+UB+pJ8gFy9ljHujkNxki0XLA/MdCWYAef7QvMverdnJ99d4dz96Mzql1uU7pZxkOa3qpdShDqqi81zj1O04lr4JqSgSyzw8fTjt9tihtsEcENIxDLrN3mlucxySJP/tct6OSyxjOY329VxahZw2d7lt9+3227k2nZbc7JtubfvBizeFblU2OVSKZGXYu0uqcZtF0KmnFKYZXXPlBQUrT6qtDxMcbPtdtupRlniJGBcehXJwbnPt4yhikRGQ1WFKSpdoH6OqpFFh+gqKWro3EcDT1FE83UUrqNdRMy80lQ6iOQWIY9FpBkJXc3L6/Hr96y/m3cPvbuJvNVdw+Xbl5Vcd76y90GSO7msXBdrytF9taWkW66U1xtYo10tXRpYQEKbCdJY0Y9ttsWD9LCI9hjp1GvxW6e5zzzfqJSPuu79P2fBbQZ99U3vn3IsFdj+Qb0N0JvTVPT5Rk2IYFt9g2fZpTU6kOCkzbP8Sxm0ZfldHUOICqhqtrHUVJ19UL1OvEhxHH4zYQJboCZED4AkgfYFyp8pvJBrMT1IEQT8SACftJWqN7313gyDeS5dwtw3Eyhre+65cM9q9z7Xc3rPlwzJK2XE5FTXS1mkdo7mlynQQtqX8ummmojmR2+COD9MIj2GZjqG+1cWW5zyzDPKR9wHr08SfBvEnp5rbfNfqrd9eeYZk+D3feWktdDnFtNnznIcL2623wPcHNra4lKKuky3cbEMVs+aX5u5JTpV+vWqNVx9Uq1McKHEcfjmJiBNegMpED4Akj8Fyp8pu5xMbMT1IEQT8SAD95+K0+2h3n3U2Czu07m7MZ3kW3Gd2RD7NDkeNV66OsNFWJSivtde2Q5TXWz3JtITU0lUh2nqEjRxCuEc7Pgw7nH7WcCUPX+Gn2Lh4M2bb5PcxSIl/D+Hktht8fqFd2XcNgbu124u41rpdua2vYut8wnbrb/Adp8dyu6UpUqluGY27bjHcaZyypplrUps1/rpStRUBqY42Djdlt8nuwiTkA0MiZN8LEsuTl5HdZI0tWPoIj/JAWlsxjn2XAqFk6h3o3Ptm0GQbBUGYXOm2eyrObZuVkOCN+h+j3XObNbKez2vI6kFo1BrqO20rbSNFhEqRqNY0SwYJZxuiP38QwPoVyBuc4wfpxI+z5aev2+J+9dbPN3dx9z7dt9ac9yu5ZNbtqsLodvNvaS4FktYthdskNBj9u9JpsiipigaTlSuHjFxYcOCU5Yg0pyc+pUy582aMYZJExiAB9j/ylLhu7uPdNrca2UuGV3Gq2rw7Kb3m2NYW56P6VaMqyRos3y9UwS0H/AJu4tEpXMsp08AIRwYY5zuYhs0gxPp5JPcZp4hglInGDoPsA/YArkxPuM3vwbZ7crYDEtycmsezW8NXZK/crb2irJcfyurxyp+csz9fTqQpaF0lUAs+kpHqKSmeaUaYZNrt8meO5nEHNDofEOrDc7jHiOCEiMUmcfBUjaXe3dPYm/XvJ9pMyumD37JMNyLb2+XK0egKi44ZlrLFPkePv/MMvI+SurNM2lzQBWiRoRGefBh3MRHMLREhIfEdCscObLtyTiLGQY/ArInb13fb+9rdqz3H9msutlmxrc62W61Zvi2SYdiOfYvem7MmvTZq9WP5tZr7aqS92cXSo+VrmWm6pj1TKscNNW52W23RjLMDaHQgkEejggt6Lbg3m4wGRiXuXLgFz56grt7C95/cl2z1eXubP7jO2Oz7hVQrs8wi92DG8z23zGuQ4XGK/Itu8utN5w+5V9GpX8h9dH6rKQEoUEgATcbHa7kR92LyiGBBIkP8ACBB/FXDvtzgcQl8p6gsR9xBj+C6XcX3gdxHddW43U75bh1GUW7C6V6iwzErVZrDhuA4exVEqrDjGCYhbbLitlqK5ZKn3mKVLrxJnUYu12e12b+xFpS6kuSfiS5WO43m43OmWRoPDQD7gAPwWtk5jl2XEqonMHSqmcwslUnhb1Sqicwf1SqTnpCyVCmeD+qVUTmFkqEmMHVqEnMLJUJOYWUqEnMH9Uqk56QslQpng/qlVExhZKpOYWVqEmPSFvVSoTUeGg0PiNIOlUm08ABB0qpnhZKqJzC3xSqmeD+qVUTmFvVKhJzB0qpnMH9Uqk5g6VUTmFlahJzB1KqZzB/VKqJjCytQk5hZSqTmDpVdWYc447lcipSYRbFKlJhCxSpSYc4WKVKTCI5SpSYRbFKlJhEsUqUmEWxSpVwWHHLhkRqRb1U+tKEFwPuFrULOgkMigeP3RjLJXqso4zLoqy5t3lKASmlpndP4K6mH4BxaCYnvR81famuxXWqvsWG1LFypzTVFVfaQpROhczKGKgzTNqUnSY84xE7TcHwVMDGDHxKp1GoNYTe3BqPmLrbqZRHCYSOrIPPxik/vB8FBE0+1MWc9Ohy10LUkN4+6NASElS1lKNU+BIPhyiylqPikYlivrEXDPkFSpalKpsdrlBSipRALSkJAKiToBwAiTPT4pGJ1PorMLiPaoa/fx84zssKlAtJ9v2/ZFsUqVMwiWKVKTCLYpUpMIWKVKTCJYpUpMItilSkw5wsUqUmHOFilSkwhYpUpMIWKVKTCFilSkwhYpUpMOsLFKlJhEsUqUmELFKlJhzi2KVKTCFilSkwhYpUpMIjlKlJhFsUqUmERylSvn4Jp5RPppNKJgOU3jpFsVfmar6L6mHOFipUpMIWKVKTCFilSkw5wsUqUmHWFilSkwhYpUpMIWKVKTCFilSkw5wsUqUmESxSpSYRbFKlJhCxSpSYQsUqUmELFKlJhzhYpUpMOcLFKlJhEsUqUmEWxSpSYRLFKlJh1hYpUpMItilSkw5wsUqUmELFKlJhCxSpSYc4WKVKTDnCxSpSYQsUqUmELFKlJhCxSpSYQsUqUmHOFilSkwhYpUpMIWKVKTCJYpUqJx1hYpVTMOcHSpSYRbFKlJhzhYpUpMOcLFKlJhz98LFGKTCJYpUpMOsWxSpSYRLFKlJh1i2KVKTDnCxSpSYQsUqUmELFKlJhzhYpUpMIWKVKTCFilSkwhYpUrqT9fdHGsVyKpP190LFKpP190LKVSfr7oWKtUn6+cLFKpMfDX7e+FilUn6+6FlKpP1hYq1V6YJff0e+NoXMti5BFC6EgrUlbjifRcSkcSfWCQeSSYwm5isoBj0WyBUeY+8ae+OPZb6rFG6lWhNBbaT1B6jtSt8ImGsjKQkqI11A1c4c+MbcZ1WvINFYM5bwQpUdDU5GpSf9JLNLT6H9ilmM7G/2LCvydPFLCv08ezB0n89FQU4PD9+odKh+0RZS+YJGPylXLtUlK7jeFqlUE29gBKgkgz1JCgpJBBSRGOWWiyxxGuizKujoHf+JRUS9fGalpz/APo402PqtlB5LFu5CbNb6CmpaahoWbhVvBydlhtt1ulb1KlzJAKQpzQae0GNmORJ8VhOIA6arDE/X+iN1lqqk/X3QsVKoFdYWVqk/X3QspVTP190LJVRP1hYq1Sfr5wsUqk/Xz/ohYpVJ+vuhZSqT9YWKtUn6+6FlKpP190LFKpP184WVqk/WFkqk/X3QsUqk/WFilUn6+6FlKpP18YWVqk/X3QslUn6+6FipVTOecLFKhRP190LJVJ+sLJVJ+sLJVTOefuhZKqJ+vuhZKpP190LJVJ+vnCxVqpnPP3QspVROef9ELHwSqT9fdCxSqT9fdCxSqTdfOFlapP190LFSqmc8/dCyVUT9fdCyVUznn7otkqon6+6JZKpP1i2SqT9fdEsVapP1hYpVJ+vuhZKpP190LKVSfr7oWSqmc8/dCyVUT9fOFirVJ+vuhYqVSfrCytUn6+cLJVJzz90LFSqTdYWSqT9ft+yFirVJ+vuhZSqT9fdCyVSfr7oWSqT9fOFirVJ+sLJVJ+vuhZSqT9fOFirVJ+vuhZKqZ+vuhZSqic8/dCxSqT9fdCyVSfr7oWVqk/WLYpVJ+vnEsUqk/XzhYpVJjzhZSqTnn7oWKVSfrCxVqpmPP3QspUJOefuhZKhdSc8o0WXIqk5hZKpOYWSoSfpCyVScwslUnPKFilUnMLJVJzCyVVfxatpKK/22rr1oapad5Trri0qUlJS0stEpSCr/iaeEYyJMWHVWMRbXor6o6x+lceFs3EtoRUPuOJp61lyRKnnCoJQl8PhB1UBwAEYWcahbBE+El0rti1e7ULuWRZVag16gZeq1u1LrySlIcFOxTmnbQVhtYIQFDxgMrBogusTjPUnVWxkF4o6luhtVoQ4i0WtLgYceEr9ZUOnV+rdH7oc0EqT4AfsjOJI1PUqEA6DoubGK2jIudluLyaalvdMhhurX/w6SsZWpdO654AIUpWhJ4ARJE6EeCsQOh8VW7NbsvxO5mrprM9cWFoLT3yn94pKtgq1SW6hkLlIPFJI4HxHsiGcJDUsrGEon0V7v53ckqYbbxS6tOOOIQ4uqSsNNBSgFEek0pTmg5yCMAB4lZOfJYqzV51eUXf1HFuSvhLYWoq9JqUFDCdfyoa10A9kbIFoha5RFla855RnZY1Sc8oWSqT9IWSqT9IWSqTmFkqEnMLJUJPCyVSc9IWSoScwslQk/SFkqk/SFilUnMLJVJ+kLJVJzCyVScwslQk5hZKpOeULJVJz0hZKhJzyhZKpOYWSoSfpCyVScwslUnMLJUJOYWSoSfpCyVSeFkqk5hZKhJ+kLFKpOeULJVJzCxSoSeFkqk5hZKhJzCyVCTmFkqk5hYpVJzCyVCT9IWSqTnpCyVCTnlCyVScwslQk/SFkqk56QslQk/SFkqk5hZKpOYWSqyBieK2zJKCpccublNXU1Sn1WUpbUlFFKklwtrlUSs6gLmlSRoQY1zyGJ9FnHGJDqrPurNNR3GrpaOpFZTMPKbaqeH8wJJBOqdEq0PDUcD7IzEiRqsTEA+ip85i2UqEnMLJVJzCyVCTmFkqk/SFkqk/SFilUnMLJVJzCyVCTnlCyVSfpCyVSfpCyVScwslUnMLJVJzCyVCTmFkqk8LJVJzCyVScwslUnMLJUJOeULJVJzCyVCT9IWSqT9IWSqT9IWSqT9IWSq6s3WOPYrfVJ+vvhY+SVSbrCxSqT9YWSqT9YWKVSbrCxSqT9YWKVSfrCxSqT9ffCxSq5GXktvMuK1Ult5pxSQeKktuJWUgnhqQNIWLIIq7skyilvdN6LDFQwpVzerlh30ykIVS0lO0kKQskrHoKJ1AGhEYxeKylqOnirNn6xlb0WNUm6+cLJVZRxzIaOgsNOwq6pZq2hfHVsKeWFCakWiiQkHgSXNCgD2xrk5LtpotkdIq0G8xydr8l/ugGv5TVLUn8Fa6xk0fJY/MqLV1tRXVL1XVvLfqahZcedXxUtZ8VHQAaxkCwYKVPVdefrCxUqk/WFkqk/WFilUmHOFilUm6wsUqk/X3wsUqk3WFkqk+nthYpVJ+p84WKVSfrCxSqTjnCxSqT9YWKVSfr5wsUqk/WFkqk3WFkqk/WFilUn6wsUqk/WFkqkw5wsUqk/XzhY+SVSfrCxSqT9YWSqT9fOFj5JVJ+sLFKpP1hZKpP1hYpVJuvnCxSqTdfGFilUn6++FilUn6nzhYpVJ+sLFKpN1hYpVJ+sLFKpN1hYpVJ+sLJVJ+p84WSqTdYWKVSbrCxSqT9YWSqT9YWKVSbr5wsUqk/XzhZK+iTdYWKVQKJISnUqUQlKQCSVKICUge0kmFkqr7RiNIgsUVdkVHRXuqbQtm2rZcWhCnUztM1NUHAlh1xJHCU6HnGNz4DRZ+15s6tB9NZbKqqpHFu01Q0XKWpS24pMyQSlaCpJE7S9NeREZWWBiyrNrxe53SlFahdHR0riy1TPV9UilFU6k6FunSrVS9CNCdAnX2xDkbRZDGSqNX0dXbKt2irmlMVLJAWhRBBChqlaFJJSttaeKVDgRFs6xMCCxGq6c/WFkqk/U+cLJVJ+vnCyVSfrCxSqT9YWKVSfrCyVSfrCxSqT9ffCxSqT9YWSqT9YWSqT9YWSqT9YWKVSfrCyVSfrCxSqT9YWSqT9fOFj5JVJ+sLJVJ+sLFKpP198LHySqTdYWSqT9YWKVScc4tilUn6xLFKpMOfSFijLqzmNFlvqEmMLJUJOYWCVCTnpCwSqTGFkqEmMLJUJP90LBKpOYWSoSYwsEqEnMLJUJMYWSoSYwslQkxhZKhJjCyVCiY8/dEsrUKZzFspUJOYWCVCTGFglQkxhYJUJOYWSoSc9IWCVSYwslQk56QsEqkxhZKhJjCwSoSYwsEqEnPSFglQk5hZKhJzCyVCTnpCwSoSYwslQkxhZKhJjCyVC+20uvONstJUt11aW220DVS1qOiUpHiSTEsEqsw2jbFtTCHb1WPJfWNTS0fppSzr+4484h31Fj2yhIB9p8YwOXyWwYh4rq3/bZVLTOVdkqHqktJK3KKpCFPLSkaqNO40hsLUAPyFOp9hJ4RRlHihxABwsW0tNVV1S3R0jK36p5UjbKB8RUPHXXglKdOJOgA8YysOvgsBBZIY2vu7jIW/cKCneUNfR0eeCDp+VbqEhOuvjKFCMfdCz9lWZfcfumPPhq4NJCHNfRqWVepTvBOmsjmgIUNRqlQChr4RRkBWBgR1VCmMZWUqEmMLBKhJjCwSoSc9IWCVCTGFkqEnPSFglUnPSFglUmMLJUJMYWSoScwslQk5hYJUJOYWSoSc9IWCVSc9IWCVScwsEqEnPSFglUnMLBKhJjCyVCTnpCwSqTnpCwSqqFpSXrrbGtNZ6+kGnPR9tXPkmIZaKiOquS7VwXld2bNLSVLlXeGW2qh9Din6UtPNonplIdQlKylOhmChoBwiCQEXWRHzELlzWuoHcqqCLcj06aoS1WkVLyXLhIEpUpSwQlhWnAFI14ak8YRkapKIsuzmlXbfVoKRNHUJbYstIbW0mq0boxUNJdCahtTbpfUkK1KgpKlHxJESJ8UkB01ZlTsvcUpONFwldQccoTUOL4rWsoRoVq8VEJ9pixkNfikojT4KzpjGVlhUJOYWCVCTGFglQk56QsEqEnPSFglUmMLBKhJjCwSoSc9IWSoSYwslQkxhYJUJOft74WSoScwsEqEnMLJUJOYWSoSYwsEqF3bdRVV1raa30aQuoqXA2jXglPAqWtZ9iW0AqP3RLgKiDlln614Bj1CwhNVSpudTKPVfqSpTZXp8XpMhQbQgHw8VH2mNZyErcMcR8VSsi28t1TSu1Fka+SrmkFxNOlZNLUyjUtyLKvRWQPhKTpr4gwGVuvRYnEOo6rEtksNyvlwNBTNemppRFY88lSWqMBRSr1tOM4UCAgfESI2GYAdYDG5WV2trrUGZXrlcFvy8XGxTNthWnillTLqpdfYVk9Yw91Z+yPVY6ynE63GnG3C4KqgqFFLNUlEhSscfRfb1VI4U8QdSCOXhGQyArCWOvwVozmMrLGoSYwslQk/t1ELBKrqzCNFlvYpMOcLIxSYc4WRikw5wsjFJhzhZGKTDn74lkqUmELJUpMOcWyMUmHOJZGKTCLZKlJhCyVKTCFkqUmELJUpMOfvhZKlJhziWRikw+39kWyMUmELJUpMIWSpSYcxCyMUmHOFkYpMIWSpSYc4lkYpMItkqUmELIxSYQslSkwhZKlJhzhZGKTCFkqUmHP3wslSkwhZGKTCFkYpMOcLIxSYc4lkYq/wDbakaqsjS86AoUNM7UNgjwfPwtLHVsgmMZy0WcIklbCzCNdluqUnGvj+2FkqVSKGyWq31tbcKSkbaq69U77oGpGvFSGQeDSHDxUE6AkQuVBBi4Upv1pXdP0ZusQ5cQ2XVMNhSwgJ01St1ILSHRr+UnXSDlnRtW8V18mtrN3sdwpHAmZNO5UsLI4tPU6VOpWjXwVIFJ/bFEyC6SiSGWrAWCAeYHD7422XHqVMw5xbIxSYQslSkw5++FkqUmELJUpMIlkYpMOcWyMUmHOFkYpMPt/bEsjFJhzhZGKTDnFsjFJhz98LJUpMIWSpSYc4WRikwhZKlJhziWRikwi2SpSYRLJUqUuJSpKiJglQUU/wAQB10/bppCyVK2XpscxO4UVJVIs1uW3UUzLiXUMBJXM2AozJI1+MH9saryGi30BHRfbGGYuxVMVbNtSy9TuodbU29UJSlSDqCUByUgcjwi+4U9seSwXTKFXmSAfi9bIHvwFY6R+ATGdvlWqpsunf3g/kN1Xr43apT/ALFWpvh/swEtFCC6qmbqAyBTB8KehtbAHHhpRMTD7pokZaOrIF1kG8YI9fP0+qauTVN6Vso6X0HaZapSyylJV6iXRwUR4S8OsQZW8FmcZlqrfc2tvA/4dxti/Dgo1aD+ApnBr+2L7oU9qSx1X0yrfW1VC6ttxyleUytbKitpSk6altRSkqTx5CMhN9VgYkLqTCFlKlJhzhZGKTDn74WSpSYc/fFslSkwhZKlJhzhZGKTDnEsjFJhzi2RikwiWSpSYc4tkYpMIWSpSYc/fEslSkw5xbIxWT9rW2lXeufVoXGqEpb14y+o60VLHJQl0/bGE5aLZjiXWdZhGuy21KTDnCyMV1kopKQVDyG2KYOqNRVOhKGgtYSAXnl8NTKnxMLEpVlQLVltBerpU2+3NP1DFK1O5ckp/uhcm09JJPxGYflV+97BpxilwNViNSuxlVKzXY7d2XQFBuieqUa+x2lQX2iD7NFoEBLUJKJMStW5h+2N1loqUmELJUpMPt/X4QulSunMY02CzqEmMSwSqTGK4SoSY9IjhKqZz0g4Sqic9IWCVSY9IOEqkxhYJUJMekVwlUmMSwSoSY9IrhKpMYOEqEmMLBKhJjEsEqFMx6RXCVUTGJYJUJMYtglQpmPSDhKqJj0g4SqTGDhKhJjEsEqkx6QcJVJjCwSoSYxbBKhJjEsEqkx6QsEqkxi2CVCmY9IOEqomMSwSqmYxXCVUTGDhKpMYOEqEmPSDhKq8cFvLVoyGncqVhulqkLo3lqOiG/W4NurPsQ2rx++MZEV0WUQAVsosqkUW5CspJQVEyEkapJKeJSensjRcLYysCoc3BYqP5tRj7NvTqt+5SpSxTMp1KlONPqQ9NoOA0IJ9sbLY29VGkqJW5fcL/WsY5jD4ndCm6q9OI9H1AhClPuU7aR/d2QlJ0V+ZXgNNYBgHKeivjHsYt+PMn0R8zXvD+9XB4avvKJmUlGpJaam9mup9pMYnI/wQRZdjJLm1abHcax1SQflnGGUk6Fx6oQppDaOa9FE6chEjJyqei1UCjp15xvcLSwdJz0hYJVJj0g4SqTGDhKhJj0g4SoSYxXCVCTGDhKhJj0iWCVSY9IOEqkx6QsEqEmMHCVSYxbBKpMYOEqEmMHCVCTHpEcJVJjFsEqkxiWCVCTGK4SoSYxLBKhZlxvJ2cYxq0qunzD7FwrK0M+nor5Oma9IApQrQrROr8oPDXhyjXIgnRbRpoq9c9yscZonl2+pcrqxbS0sMJYeZAcUkpC3XHUIShKCddOJOnhEGp9FS4WLMOZfqr63dndE0drW9dLjVK+FlASla5Cv8ocdcX8I8Y2SkGbxWAj4q23qoVNwdqjqE1FcuoOo+IJeqS7xB9oCooIZYtqrkzoLTk9a6R/KqE01RTLHFLtMtlCm1tnwKZeHQxjGQZWUfFbDWe4U11ttJW0TiXWnGWyQghSmVhICmnUji24gjQgxqMg7LYFSsoyJuw2t+ra0fqQsUzKW1JWlmpdQotrqJSfTSgJKtD4kae2LE2KhLLWFx5brjjrhmcdWpxaj4layVKP7SY3WC1M6+Jz0hYJVJjFcJUJMYOEqpmPSDhKqJjCwSoSY9IlglVM56QcJVJj0iuEqomMLBKhJjCwSoUzHpBwlVExhYJUJMYjhKhXXht/TYL01UvkikqEKpasgElDTikqDgHt9NxAJ/0dYkiCNOqyiGK2WnRVU09PUfy6holmqp1IXolafhdaUQtEw11GoOhjRbwK2NosbXO2XCxqTX3XO7qm0pJUWJ1IuNSocUU1OUuFDil+1QSnTlGYnE9BqpVUCnulw3CuybSlx+3WCnbL1Q22su1DrTXBv5uoUT6rz5HDXVIPM8TkTGA9Viz/BZht9vorVSt0VAwimp2/BCBxWr2uOKOqnHFHxUokxru/VZgN0VsZ5embTYalkrHzVzbXR07XCZbboKKhwjxCENE/F4TcPGMoFypLotbJj9v7Y22C1VCTGLaKVSYwcJULqzCNDrfUpMOsV0qUmEHSpSYRHSpSYdYOlSkw6xXSpSYQdKlJh1g6VKTDrEdKlJh1g6VKTCDpUpMOsV0qUmHWI6VKTCK6VKTDrB0qUmHWDpUpMOsHSpScdYWKVKTCI6VKTCDpUpMOsHSpSYdYOlSkw6xXSpSYc4OjFJhB0qUmHWDpUpMOsR0qUmHWDpUpMIOlSkw6xXSpSYdYjpUpMIrpUpMOsR0qUnHWDpVXhac8yGzsJpWapFTTITK01Wtev6Q8AltwqQ6lKR4CYgRCAVlqrjtr97z92oVeK5bNjtiPXqWKRsMNOuAFSWEpTrM6pI1mWTKPDjwiOI9OqoiTqq1tdakobuN6Wgj1XV0NGTx/lNrmfI1/eC0pGvImJKT6IIELKNfcaO2Uj1bWvJYp2EzLWrTU8kITqCtxZ4ADiTGKyYrW3Kstqclq9fjYtzClfJ0uvgPD13v4n1j9iRwHt12DRYEEq05hGTrGpSYdYjpUpMIrpUpMIOlSkw6xHSpSYdYOlSkwiulSkw6wdKlJhEdKlJh1iulSkw6xHSpSYRXSpSYQdKlJhEdGKTCK6VKTCDpUpMOsHSpSYdYjpUpMOcV0Yq6bfmd7tlIzQU7lIukp9Q0zU2+jqZNTqdFvNKc4/fGJAKy+ZZExa7/r1Op25WqzvKVdKWiSpFsYb+BymrX3SQ3oCofLjT9sYkkdCVQD4rHl9ye4XD17YDT0dsZqnkooqCnRSsLDbq0tqfCPifVKP3iRrGQ8/FQv4K1ZhFdY1KyFj11pb+q243freK9CVCnt9wQ6qnrqFrxDSnUA+vTp8Ak6adYxJbULIA+KqVxosMs3yYNZlNELlSJrG/k6hkp9FZCUh4fEZuPXh7YCRKyr9yo9wuONU2P1tss9fcq1+ur6eqc/UKcoWgMpWkkOpQlslU3HjrAEvqoY6aKw5hGbrCpSYQdKlJh1g6VKTCDpUpMOsHSpSYRHRikwiulSkw6wdKlJh1g6VKTCDpUpMIOlSkwiOlSkwg6VKTDrB0qUmHWDpUqqUF+u9sSUW+511GhXi2y+tLfHx0RqUjXoIaHqrqr6xe2fr1NdckyCoqLk1bWqhunaqXVupXVJYC1LcmV+VoOJIA4E+PhGJk2gWQiW1V6bZ2sUVkVcXU6VF1XOCR8QpmSUNoI9gU4lSxzCokpOVRFuiufI8locbofmqk+o+7MmkpEqAdqHB+MrST+ZWh0HMxBqjFa1Xe9Vt7rnK6vdLjq+CEjg0w1rqlllGpkbR+J9usbBp0WBBKpcw6wdSpSYQdGKTDrFsUqV15+kcay3VSfpFslUn6RLJVJ4WSqTwslUn6QslUn6QslUn6QslUn6RbJVJ+kSyVSeFkqk/SLZKpP0iWSqT9IWSqT9IWSqT9IWSqT9IWSqT9Itkqk/SJZKpPFslUnhZKpP0iWSqTwslUn6RbJVJ+kSyVSeLZKpP0iWSqT9IWSqT9IWSqT9Itkqk5hZKpP0iWSqTxbJVJ+kSyVSeFvVKrMW2byKu1ZHZ0uBqqqEh1tZE0jblOql9Uo1SVyOuDhqOEYSlqsxHRZKYNuxSxMtvvJao7exKt5YAU84eJUEA/G+8rgADxMSzlWoAWvuVZbWZLVzGZi3sKPylHMSlI8PWe00C31j2+CRwHt12AssCFas8WylUniWSqT9Itkqk/SFkqk8SyVSfpCyVSfpCyVSfpCyVSfpCyVSfpCyVSfpFslUn6RLJVJ+kWyVSfpCyVSfpCyVSfpEslUn6QslUn6RbJVJ+kLJVJ+kSyVSfpCyVXOzWVNPp6FQ8zoqcek6tvRYSpAXok6TBKyNfHQmLZKrhKySSeJJJJPiSeJJPMmJZKqJ+kLJVd23XOptdaxX0hQKinVM2XEBxGpGnFCuBg6MuS43eruhpDVemfkqRqiY9NEn8lkAJK+JmWdOJ4QduipDqnT9ItlKpP0iWSqT9IWSqTxbJVJ4WSqT9Ilkqk/SLZKpPEslUn6QslUn6RbJVJ+kLJVJ+kSyVSeFkqk/SFkqk/SLZKpPEslUn6Qt6pVZi22qaa4Wq9Y3UKUkVBNSQhUrrjLqGmng2rjp6YYGvKaMZS1dZiIZlkC+X+24lampkgqQ0int1ChX8x30kBCBqdSlptIEyzr+0xiC6tQFrfdrzXXqtdrq9wuOuHRKRqG2WwfhZZQSZG0DhzPidTGwEBYEOqbP0hZSqT9IWSqT9Itkqk5hZKrrTdfONFlvr6JN184WSqTdfOLZSvok3XziWVZJuvnFspVJuv/h6GJZGSb269NdYWRkmPPzi2RlEwPt6RLKspm6+PWFlGSb2a+cLIyTdfP2wsqya9fOFlGSbr5/0QsrVNevn7YtlGSbr58YlkZRN1hZVlOvXzhZRkm6+cLIyTdfOFlapNpw10hZKpN184tlGSbr5wsjJN184lkZRN7df26wsqymbr5wslUm6+cWylU16+HWFkZJuvj1hZGSbr7eftiWSqTdfHrCyMk3XzhZVkm6+HWFlGSbX2jy98WyMk3Hx4wsjKs2G+VFhudPcWCVSKKH2ptEvsL+F1s+wEpJlPsVxiEuFQFUMpyysyWrC1lTFAwSKSiC9Utg+LrpHBx9XtPgB4aCALKkOrWn6+cWyxr6Jr14ff9vbCyMk3XziWRkm09sWyVdJuv7f7YWRkm6+cLIya9fHrEsjIT48fvi2Rkm6+cSytUm6+cLIyTdfOFlKpN184WVqk3X2+yLZRkn19vnEslUm6/bw/pi2Rkm6/hEsjJNr7fOLZGSY8+X9kSyMk3Xzi2Vqk3X8DCyMk3Dx8OULKVSbr5xLJX0Sbr5wsjJN1i2SqTdfOJZGSbrw+/hCyMk3XzhZGSY8/t/TFslU18eP38ffCyMk3Xw4QsjJNr7fPlEslUm8OP9fGLZGSbr5xLJVJuvWFlapN184tlKpNp7YWRkm9mv7NYllWSfr5xbKVTXr5+yJZGTXr5+yLZVkm6+cLKMk3Xz+3KJZVkm6/1/1+yFkZJuZ8/tyi2UZVG03aqs1fT3GjWEv06iQFalDiFDRTTgBEzax4jpEMnVGim7XetvVa7X17xcecJCU66NstjglllPgltA4cz4nUwswRlTZuvn0/qhZRkm6/dxhZGSbr5xbJVJtPb5wsjJNx8eMSyraei//Z" controls="">\n          <source src="{{video}}">\n        </video>\n      </ion-row>\n    </ion-grid>\n    <ion-grid *ngIf="video == \'\'" (click)="pickVideo()">\n      <ion-row text-center>\n        <ion-icon col-12 full text-center name="videocam"></ion-icon>\n      </ion-row>\n      <ion-row text-center>\n        <p col-12 color="light-blue" full text-center>Add Video</p>\n      </ion-row>\n    </ion-grid>\n  </div>\n  <ion-list no-lines>\n\n    <ion-item>\n      <ion-label floating>Subject</ion-label>\n      <ion-input type="text" [(ngModel)]="subject"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label floating>Message</ion-label>\n      <ion-textarea type="textarea" [(ngModel)]="body"></ion-textarea>\n    </ion-item>\n\n  </ion-list>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <button ion-button full large (click)="send()" color="light-blue">Send to All Guests</button>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/create-an-invitation/create-an-invitation.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_media_capture__["a" /* MediaCapture */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_video_editor__["a" /* VideoEditor */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_media_capture__["a" /* MediaCapture */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_video_editor__["a" /* VideoEditor */]])
], CreateAnInvitationPage);

//# sourceMappingURL=create-an-invitation.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PreApprovedOfficiantPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__officiant_details_officiant_details__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PreApprovedOfficiantPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PreApprovedOfficiantPage = (function () {
    function PreApprovedOfficiantPage(navCtrl, viewCtrl, navParams, modalCtrl, httpProvider, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        var loading = this.createLoading();
        loading.present();
        httpProvider.getOfficiants().then(function (officiants) {
            console.log(officiants);
            _this.officiants = officiants;
            loading.dismiss();
        }, function (err) {
            console.error(err);
        });
    }
    PreApprovedOfficiantPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreApprovedOfficiantPage');
    };
    PreApprovedOfficiantPage.prototype.ngAfterViewInit = function () {
    };
    /*officiantSelected(){
      this.navCtrl.push('OfficiantDetailsPage');
    }*/
    PreApprovedOfficiantPage.prototype.officiantSelectedModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__officiant_details_officiant_details__["a" /* OfficiantDetailsPage */]);
        modal.present();
    };
    PreApprovedOfficiantPage.prototype.showOfficiantDetails = function (officiant) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__officiant_details_officiant_details__["a" /* OfficiantDetailsPage */], {
            officiant: officiant
        });
        modal.onDidDismiss(function (data) {
            console.log(data);
            if (data) {
                if (data.invite) {
                    //invite officiant
                    // let loading = this.createLoading();
                    // loading.present();
                    // this.httpProvider.inviteOfficiant(data.event, data.officiant).then(
                    //   (res) => {
                    //     loading.dismiss();
                    //     this.navCtrl.pop();
                    //   },
                    //   (err) => {
                    //     loading.dismiss();
                    //   }
                    // );
                    _this.viewCtrl.dismiss(data);
                }
                else {
                    //do not invite
                }
            }
        });
        modal.present();
    };
    PreApprovedOfficiantPage.prototype.selectOfficiant = function (index) {
    };
    PreApprovedOfficiantPage.prototype.getActiveClass = function (index) {
        return 'bright-bg';
    };
    PreApprovedOfficiantPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    PreApprovedOfficiantPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(null);
    };
    return PreApprovedOfficiantPage;
}());
PreApprovedOfficiantPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-pre-approved-officiant',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pre-approved-officiant/pre-approved-officiant.html"*/'<!--\n  Generated template for the PreApprovedOfficiantPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Find an Officiant</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close" color="light"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list class="officiant-container" no-lines>\n      <ion-item no-lines text-wrap text-center>\n        <h2 class="officiant-head">Invite an Officiant</h2>\n        <p>A notification will be sent to each. The first to accept your offer will be instantly asigned to your ceremony.</p>\n      </ion-item>\n      <button *ngFor="let officiant of officiants;" ion-item (click)="showOfficiantDetails(officiant)" class="border-btm header-bold officiant-list">\n        <avatar item-start [user]="officiant.personal.id"></avatar>\n      <h2 style="text-transform:capitalize;">{{officiant.personal.name.full}}</h2>\n      <!-- <p>{{officiant.officiant.description}}</p> -->\n      <!-- <ion-note item-end>\n        <button ion-button clear class="mini-btn" small>\n            <ion-icon name="star" class="star-icon bright-bg" mini></ion-icon>\n        </button>\n      </ion-note> -->\n    </button>\n    </ion-list>\n</ion-content>\n<!-- <ion-footer class="footer-sticky" text-center>\n    <button ion-button clear (click)="inviteselected();">Invite Selected</button>\n</ion-footer> -->\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pre-approved-officiant/pre-approved-officiant.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
], PreApprovedOfficiantPage);

//# sourceMappingURL=pre-approved-officiant.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfficiantDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the OfficiantDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var OfficiantDetailsPage = (function () {
    function OfficiantDetailsPage(navCtrl, navParams, viewCtrl, loadingCtrl, httpProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.httpProvider = httpProvider;
        this.event = this.navParams.get('event');
        this.officiant = this.navParams.get('officiant');
    }
    OfficiantDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OfficiantDetailsPage');
    };
    OfficiantDetailsPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss({
            invite: false
        });
    };
    OfficiantDetailsPage.prototype.invite = function () {
        // let loading = this.createLoading();
        // loading.present();
        // this.httpProvider.inviteOfficiant(this.event, this.officiant.personal.id).then(
        //   (res) => {
        //     loading.dismiss();
        this.viewCtrl.dismiss({
            invite: true,
            officiant: this.officiant.personal.id,
            event: this.event
        });
        // },
        // (err) => {
        //   loading.dismiss();
        // }
        // );
    };
    OfficiantDetailsPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return OfficiantDetailsPage;
}());
OfficiantDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-officiant-details',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/officiant-details/officiant-details.html"*/'<!--\n  Generated template for the OfficiantDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--<ion-header>\n\n  <ion-navbar>\n    <ion-title>officiant-details</ion-title>\n  </ion-navbar>\n\n</ion-header>-->\n\n\n<ion-content>\n    <ion-card class="officiant-details-card" style="background-color: #1658a0;">\n      <button ion-button item-end clear (click)="dismiss()" class="dismiss-btn" style="z-index:9999;">\n        <ion-icon name="close-circle"></ion-icon>\n      </button>\n     <img src="assets/images/Background.svg" style="opacity:.3"/>\n     <div class="card-user">\n          <!-- <img src="{{officiant.personal.avatar}}"> -->\n          <avatar item-start [user]="officiant.personal.id"></avatar>\n     </div>\n      <div class="card-subtitle">{{officiant.personal.name.full}}</div>\n    </ion-card>\n  <!-- <ion-list class="mrgn-btm">\n    <ion-grid>\n      <ion-row text-center no-lines>\n        <ion-col col-6>\n          <share [user]="officiant.personal.id"></share>\n        </ion-col>\n        <ion-col col-6>\n          <ion-item no-lines text-center item-start class="officiant-social-btn">\n              <button class="mini-btn" color="light">\n                <ion-icon name="star" class="star-icon white-icon"></ion-icon>\n              </button>\n            <h6 class="mini-text">Invited</h6>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-list> -->\n  <ion-list class="mrgn-btm">\n    <ion-item text-left class="officiant-details-general-text">\n      <h2 class="general-text" text-left>General</h2>\n    </ion-item>\n    <ion-item class="light-blue-bg">\n      <ion-icon name="ios-pin-outline" item-start></ion-icon>\n      {{officiant.personal.location.city}}, {{officiant.personal.location.state}}\n    </ion-item>\n    <ion-item class="light-blue-bg">\n     <ion-icon name="ios-mail-outline" item-start></ion-icon>\n      {{officiant.personal.email}}\n    </ion-item>\n    <ion-item class="light-blue-bg">\n      <ion-icon name="ios-call-outline" item-start></ion-icon>\n      {{officiant.personal.phone}}\n    </ion-item>\n  </ion-list>\n  <!-- <ion-item text-wrap no-lines>\n    <p>{{officiant.officiant.description}}</p>\n  </ion-item> -->\n  <!-- <ion-item text-left class="officiant-details-general-text">\n    <h2 class="general-text" text-left>About</h2>\n  </ion-item>\n  <ion-list class="qa-list">\n    <ion-item *ngFor="let question of officiant.officiant.questions;" text-wrap no-lines>\n      <p>{{question.question}}</p>\n      <h3>{{question.answer}}</h3>\n    </ion-item>\n  </ion-list> -->\n  <ion-item text-left class="officiant-details-general-text">\n    <h2 class="general-text" text-left>Other</h2>\n  </ion-item>\n  <ion-list>\n    <ion-item>\n      <h3 item-start text-left><ion-icon name="star" color="light" small class="mini-star-icon"></ion-icon> Fees</h3>\n      <h2 item-end text-right class="price-text">${{officiant.officiant.fee}}</h2>\n    </ion-item>\n    <!-- <ion-item text-right>\n      <h3 item-start text-left>Availability</h3>\n      <ion-row item-end>\n        <ion-col text-right>\n            <button *ngFor="let available of Object.keys(officiant.availability); let day = index;" ion-button clear small class="day-btn">\n              {{available}}\n            </button>\n        </ion-col>\n      </ion-row>\n    </ion-item> -->\n  </ion-list>\n</ion-content>\n<ion-footer class="footer-sticky" text-center>\n    <button ion-button clear (click)="invite();">Invite to Officiate</button>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/officiant-details/officiant-details.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]])
], OfficiantDetailsPage);

//# sourceMappingURL=officiant-details.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarriageApplicationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the MarriageApplicationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MarriageApplicationPage = (function () {
    function MarriageApplicationPage(navCtrl, navParams, httpProvider, loadingCtrl, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.submitted = false;
        this.attachments = [];
        this.your_information = {
            id: 0
        };
        this.spouce_information = {
            id: 0
        };
        this.officiant_information = {};
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: 0
        };
        this.refreshApplication();
    }
    MarriageApplicationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MarriageApplicationPage');
    };
    MarriageApplicationPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    MarriageApplicationPage.prototype.refreshApplication = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.getMarriageApplication(this.navParams.get('id')).then(function (application) {
            loading.dismiss();
            console.log(application);
            _this.application = application;
            _this.submitted = application.submitted;
            _this.your_information = application.your_information;
            _this.spouce_information = application.spouce_information;
            _this.officiant_information = application.officiant_information;
            // this.attachments = application.attachments;
        }, function (err) {
            loading.dismiss();
        });
    };
    MarriageApplicationPage.prototype.addAttachment = function () {
        var _this = this;
        var image = '';
        this.camera.getPicture(this.options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            image = base64Image;
            var loading = _this.createLoading();
            loading.present();
            _this.httpProvider.addAttachment(_this.navParams.get('id'), image).then(function (application) {
                loading.dismiss();
                console.log(application);
                _this.application = application;
                _this.submitted = application.submitted;
                _this.attachments = application.attachments;
            }, function (err) {
                loading.dismiss();
            });
        }, function (err) {
            // Handle error
            console.error(err);
        });
    };
    MarriageApplicationPage.prototype.removeAttachment = function (id) {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.removeAttachment(this.navParams.get('id'), id).then(function (application) {
            loading.dismiss();
            console.log(application);
            _this.application = application;
            _this.submitted = application.submitted;
            _this.attachments = application.attachments;
        }, function (err) {
            loading.dismiss();
        });
    };
    MarriageApplicationPage.prototype.submit = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.submitMarriageApplication(this.navParams.get('id')).then(function (application) {
            loading.dismiss();
            console.log(application);
            _this.application = application;
            _this.submitted = application.submitted;
            _this.attachments = application.attachments;
        }, function (err) {
            loading.dismiss();
        });
    };
    return MarriageApplicationPage;
}());
MarriageApplicationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-marriage-application',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/marriage-application/marriage-application.html"*/'<!--\n  Generated template for the MarriageApplicationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Marriage Application</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="registry-content">\n  <div *ngIf="submitted==true" class="registry-container">\n    <div padding text-center>\n      <h2>Marriage Application Sent</h2>\n      <p>Your marriage application has been submitted and is under review. Since this is a legal form no edits can be made at this time.<br/><br/>Your confidential information has been stored securely and is not available through this app.<br/><br/>You will be notified once the court has received and then reviewed your application.</p>\n      <button ion-button color="green" style="margin:0 auto;display:block;" text-center round>{{status}}</button>\n    </div><br/>\n    <ion-list>\n      <ion-item-divider color="light">Application History</ion-item-divider>\n      <ion-item *ngFor="let history of application.history" class="order">\n        <ion-grid>\n          <ion-row>\n            <ion-col col-12>\n              <h2 color="dark">{{history.title}}</h2>\n              <p>{{history.date*1000 | date : \'longDate\'}}</p>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-item>\n    </ion-list>\n  </div>\n  <div *ngIf="submitted==false">\n    <div class="registry-container" padding text-left>\n      <h2>1. Court</h2>\n      <p>Select a court in the county which you have residence. If you wish to allow a proxy ceremony you may skip this question.</p>\n      <ion-searchbar disabled="disabled" placeholder="Washington Court"></ion-searchbar>\n      <!-- <button ion-button color="green" style="margin:0 auto;display:block;" text-center round>Accepts Online Applications</button> -->\n    </div><br/>\n    <div class="registry-container" padding text-left>\n      <h2>2. Application Information</h2>\n      <p>For each individual upload a photo of:</p>\n      <ul>\n        <li>Government issued ID</li>\n        <li>Birth Certificate</li>\n        <li>Utility Bill</li>\n      </ul>\n\n\n      <!-- <button text-center style="margin:0 auto; display:block;" (click)="addAttachment()" round ion-button color="primary">ADD NEW</button><br/> -->\n      <ion-list class="documents-container" no-lines>\n      <ion-item *ngFor="let attachment of attachments" class="header-bold">\n        <h2>{{attachment.title}}</h2>\n        <p>{{attachment.date*1000 | date}}</p>\n        <ion-note item-end>\n         <button (click)="removeAttachment(attachment.id)" ion-button menuClose clear class="close-btn">\n            <ion-icon name="close-circle"></ion-icon>\n          </button>\n        </ion-note>\n      </ion-item>\n     </ion-list>\n    </div>\n    <div class="registry-container" text-left>\n      <ion-list class="white-bg guest-invitation-list" no-lines>\n        <ion-item-divider color="light">YOUR INFORMATION</ion-item-divider>\n        <ion-item class="border-btm header-bold">\n          <ion-avatar item-start>\n                  <img src="{{avatar}}">\n          </ion-avatar>\n          <h2>{{name}}</h2>\n          <p>{{email}}</p>\n        </ion-item>\n        <ion-item class="border-btm header-bold">\n          <div class="row">\n            <div class="col" *ngRepeat="attachment in attachments">\n              <img src="{{attachment}}" (click)="selectAttachment()" />\n            </div>\n          </div>\n        </ion-item>\n        <ion-item-divider color="light">SPOUCES INFORMATION</ion-item-divider>\n        <ion-item class="border-btm header-bold">\n          <ion-avatar item-start>\n                  <img src="{{avatar}}">\n          </ion-avatar>\n          <h2>{{name}}</h2>\n          <p>{{email}}</p>\n        </ion-item>\n        <ion-item class="border-btm header-bold">\n          <div class="row">\n            <div class="col" *ngRepeat="attachment in attachments">\n              <img src="{{attachment}}" (click)="selectAttachment()" />\n            </div>\n          </div>\n        </ion-item>\n      </ion-list>\n    </div>\n    <div class="registry-container" padding text-left>\n      <h2>3. Officiant Details</h2>\n      <ion-list class="white-bg guest-invitation-list" no-lines>\n        <ion-item class="border-btm header-bold">\n          <ion-avatar item-start>\n                  <img src="{{avatar}}">\n          </ion-avatar>\n          <h2>{{name}}</h2>\n          <p>{{email}}</p>\n        </ion-item>\n      </ion-list>\n    </div><br/>\n    <div class="registry-container" padding text-left>\n      <h2>4. Review and Submit</h2>\n      <p>Please check that you and your spouce have accurately completed your user profiles and that you have uploaded the required documents. You will not be allowed to modify this application once submitted. By pressing submit you authorize digital transmission of this information and have read and understand our Terms of Service.</p>\n      <!-- <button text-center style="margin:0 auto; display:block;" round ion-button (click)="submit()" color="green">SUBMIT NOW</button><br/> -->\n    </div><br/>\n    <button (click)="submit()" full block color="light-blue" ion-button large>SUBMIT NOW</button>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/marriage-application/marriage-application.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */]])
], MarriageApplicationPage);

//# sourceMappingURL=marriage-application.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarriageEducationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the MarriageEducationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MarriageEducationPage = (function () {
    function MarriageEducationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MarriageEducationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MarriageEducationPage');
    };
    return MarriageEducationPage;
}());
MarriageEducationPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-marriage-education',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/marriage-education/marriage-education.html"*/'<!--\n  Generated template for the MarriageEducationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Marriage Education</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="registry-container" padding text-center>\n    <h2>Marriage Educational Training</h2>\n    <p>Watch the video series by clicking the link below. When you complete the video training you will be awarded your certification for completing your free training.</p>\n  </div><br/>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/marriage-education/marriage-education.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], MarriageEducationPage);

//# sourceMappingURL=marriage-education.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the OrderDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var OrderDetailsPage = (function () {
    function OrderDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.order = this.navParams.get('order');
    }
    OrderDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrderDetailsPage');
    };
    return OrderDetailsPage;
}());
OrderDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-order-details',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/order-details/order-details.html"*/'<!--\n  Generated template for the OrderDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Order History</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-list no-lines>\n    <ion-item  class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-9>\n            <h2 color="primary">{{order.details.package.title}}</h2>\n            <p>Live Video Stream Package</p>\n          </ion-col>\n          <ion-col col-3 text-right>\n            <strong text-right>${{order.details.package.price}}</strong>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item *ngFor="let addon of order.details.addons;" class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-9>\n            <h2 color="primary">{{addon.title}}</h2>\n            <p>Premium Add-ons</p>\n          </ion-col>\n          <ion-col col-3 text-right>\n            <strong text-right>${{addon.price}}</strong>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/order-details/order-details.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], OrderDetailsPage);

//# sourceMappingURL=order-details.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pay_pay__ = __webpack_require__(367);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CheckoutDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var CheckoutDetailsPage = (function () {
    function CheckoutDetailsPage(navCtrl, navParams, httpProvider, loadingCtrl, renderer, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.renderer = renderer;
        this.modalCtrl = modalCtrl;
        this.subtotal = 0;
        this.eventDetails = navParams.get('eventData');
        this.package = navParams.get('package');
        console.log(this.eventDetails);
        console.log(this.package);
        this.subtotal += this.package.price;
        var _addons = [];
        for (var i = 0; i < this.eventDetails.addons.length; i++) {
            var addon = this.eventDetails.addons[i];
            //if (addon.selected){
            _addons = _addons.concat(addon);
            this.subtotal += addon.price;
            //}
        }
        this.addons = _addons;
        console.log(this.addons);
    }
    CheckoutDetailsPage.prototype.checkout = function () {
        // this.openCheckout().then(
        //   (token) => {
        // this.eventDetails.token = 'test';
        // let loading = this.createLoading();
        // loading.present();
        //
        // this.httpProvider.createEvent(this.eventDetails, this.navParams.get('file')).then(
        //   (res) => {
        //     console.log(res);
        //     this.navCtrl.popToRoot();
        //     console.log(res);
        //     loading.dismiss();
        //   },
        //   (err) => {
        //     console.error(err);
        //     loading.dismiss();
        //   }
        // );
        var _this = this;
        var payModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pay_pay__["a" /* PayPage */], {
            amount: (this.subtotal * 100)
        });
        payModal.onDidDismiss(function (data) {
            if (data == null) {
                return;
            }
            var token_id = data.id;
            _this.eventDetails.token = token_id;
            var loading = _this.createLoading();
            loading.present();
            _this.httpProvider.createEvent(_this.eventDetails).then(function (res) {
                console.log(res);
                //this.navCtrl.popToRoot();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
                console.log(res);
                loading.dismiss();
            }, function (err) {
                console.error(err);
                loading.dismiss();
            });
        });
        payModal.present();
        //    },
        //    (err) => {
        //
        //   }
        // );
    };
    CheckoutDetailsPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    CheckoutDetailsPage.prototype.openCheckout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var handler = window.StripeCheckout.configure({
                key: 'pk_test_OtZkFKLIU1WBuonz6xbk6UQB',
                locale: 'en',
                token: function (token) {
                    resolve(token);
                }
            });
            handler.open((_a = {
                    name: 'Web Wed Mobile',
                    description: 'Event Package Purchase',
                    amount: _this.subtotal * 100,
                    currency: 'usd',
                    email: _this.navParams.get('user').personal.email,
                    image: 'assets/icon/favicon.ico'
                },
                _a['allow-remember-me'] = true,
                _a));
            _this.globalListener = _this.renderer.listenGlobal('window', 'popstate', function () {
                handler.close();
            });
            var _a;
        });
    };
    CheckoutDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CheckoutDetailsPage');
    };
    return CheckoutDetailsPage;
}());
CheckoutDetailsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-checkout-details',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/checkout-details/checkout-details.html"*/'<!--\n  Generated template for the CheckoutDetailsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Order Details</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div text-center>\n    <h2>Complete Checkout</h2>\n    <p>All participants will be granted access to modify details and manage guest list on your behalf.</p>\n  </div><br/>\n  <ion-item-group>\n    <ion-item text-left no-lines class="event-details-text">\n      <h2 class="photo-text" text-left>Order Details</h2>\n    </ion-item>\n    <ion-item class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-9>\n            <h2 color="primary" style="color:#075193">{{package.title}}</h2>\n            <p>Event Package</p>\n          </ion-col>\n          <ion-col col-3 text-right>\n            <h2 text-right>${{package.price}}</h2>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item *ngFor="let detail of addons" class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-9>\n            <h2 color="primary" style="color:#075193">{{detail.title}}</h2>\n            <p>Premium Addon</p>\n          </ion-col>\n          <ion-col col-3 text-right>\n            <h2 text-right>${{detail.price}}</h2>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n    <ion-item class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-8></ion-col>\n          <ion-col col-4 text-right>\n            <h2 text-right><strong text-right>Sub Total ${{subtotal}}</strong></h2>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-item-group>\n  <!-- <ion-item-group>\n    <ion-item text-left no-lines class="event-details-text">\n      <h2 class="photo-text" text-left>Payment Details</h2>\n    </ion-item>\n    <ion-item class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6>\n            Card Number\n          </ion-col>\n          <ion-col col-6 text-right>\n            **** **** **** 1234\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-item-group> -->\n  <ion-item-group>\n    <ion-item text-left no-lines class="event-details-text">\n      <h2 class="photo-text" text-left>Order Total</h2>\n    </ion-item>\n    <!-- <ion-item class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6>\n            Discount Code\n          </ion-col>\n          <ion-col col-6 text-right>\n\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item> -->\n    <ion-item class="order">\n      <ion-grid>\n        <ion-row>\n          <ion-col col-6></ion-col>\n          <ion-col col-6 text-right>\n            <!-- <p color="green" style="color:green;">Discount Applied - $10</p><br/> -->\n            <h2><strong>Total ${{subtotal}}</strong></h2>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-item>\n  </ion-item-group>\n  <div text-center>\n    <p>Payments are powered by Stripe. Read the full <a color="primary">Terms of Service</a> before checkout.</p>\n  </div><br/>\n</ion-content>\n<ion-footer class="footer-sticky" text-center>\n   <button ion-button clear (click)="checkout()">Submit</button>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/checkout-details/checkout-details.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */]])
], CheckoutDetailsPage);

//# sourceMappingURL=checkout-details.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__noodliopay__ = __webpack_require__(855);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PayPage = (function () {
    function PayPage(navCtrl, navParams, viewCtrl, NoodlioPay) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.NoodlioPay = NoodlioPay;
        /**
        * Init
        */
        this.inputForm = {
            currency: 'USD',
            amountCents: 500,
        };
        this.status = {
            message: '',
            loading: false,
            success: null,
        };
        this.inputForm.amountCents = navParams.get('amount');
    }
    /**
    * fn Charge Card
    */
    PayPage.prototype.charge = function () {
        var _this = this;
        // obtain the exp_month and exp_year
        var split = this.inputForm['date'].split('-');
        this.inputForm['exp_month'] = split[1];
        this.inputForm['exp_year'] = split[0];
        // validate the card details and process the payment
        this.status['message'] = '';
        this.status['loading'] = true;
        this.status['success'] = null;
        this.NoodlioPay.charge(this.inputForm).subscribe(function (data) {
            console.log(data);
            _this.status['message'] = data.message || 'Payment processed!';
            _this.status['loading'] = false;
            if (data.hasOwnProperty('id')) {
                _this.status['success'] = true;
            }
            _this.viewCtrl.dismiss(data);
        }, function (error) {
            console.log(error);
            _this.status['message'] = 'Oops... something went wrong.';
            _this.status['loading'] = false;
            _this.status['success'] = false;
        });
    };
    ;
    PayPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss(null);
    };
    /*
    * Helper functions
    */
    PayPage.prototype.todayFormatted = function () {
        var d = new Date();
        var m = d.getMonth() + 1;
        var y = d.getFullYear();
        if (m < 10) {
            return y + '-0' + m;
        }
        else {
            return y + '-' + m;
        }
    };
    return PayPage;
}());
PayPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-pay',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pay/pay.html"*/'<ion-header>\n  <ion-navbar color="primary">\n    <ion-title>\n      Make a Payment\n    </ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close" color="light"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <form #customForm="ngForm" (ngSubmit)="charge()">\n    <ion-list>\n\n      <ion-item>\n        <ion-label stacked>Card holder</ion-label>\n        <ion-input type="text" [(ngModel)]="inputForm.name" name="name" required></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label stacked>Card number</ion-label>\n        <ion-input type="text" pattern="[0-9]{13,16}" [(ngModel)]="inputForm.number" name="number" required></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label stacked>Expiry month & year</ion-label>\n        <ion-datetime min="2017" max="2030" displayFormat="MM/YYYY" [(ngModel)]="inputForm.date" name="date" required></ion-datetime>\n      </ion-item>\n\n      <ion-item>\n        <ion-label stacked>CVC</ion-label>\n        <ion-input type="number" [(ngModel)]="inputForm.cvc" name="cvc" required></ion-input>\n      </ion-item>\n\n    </ion-list>\n\n    <div padding>\n      <button [disabled]="!customForm.form.valid" type="submit" ion-button full color="secondary">Pay ${{inputForm.amountCents/100}} {{inputForm.currency}}</button>\n    </div>\n  </form>\n\n\n  <div padding text-center *ngIf="!status.message && status.loading"><ion-spinner></ion-spinner></div>\n\n  <div padding text-center text-wrap *ngIf="status.message">\n    <p>\n      <ion-icon name="information-circle" color="danger" *ngIf="status.success != true"></ion-icon>\n      <ion-icon name="checkmark-circle" color="secondary" *ngIf="status.success == true"></ion-icon>\n    </p>\n\n    <p>\n      {{status.message}}\n    </p>\n  </div>\n  <div text-center>\n    <p>Checkout User Terms of Service are a legal agreement between the Payment Processing and you, By using the Checkout service, you agree to be bound by these terms and conditions.</p>\n  </div>\n  <!-- <div text-center>\n    <p color="primary" small>Powered by Noodlio Pay (noodliopay.com)</p>\n  </div> -->\n\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pay/pay.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__noodliopay__["a" /* NoodlioPay */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__noodliopay__["a" /* NoodlioPay */]])
], PayPage);

//# sourceMappingURL=pay.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wed_details_wed_details__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__create_an_event_create_an_event__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__attend_event_modal_attend_event_modal__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__broadcast_broadcast__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_tour_tour__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { MyAccountPage } from '../my-account/my-account';
// import { NotificationsPage } from '../notifications/notifications';





var DashboardPage = (function () {
    // participants: any = ["assets/images/user-avatar1.png",
    //   "assets/images/user-avatar2.png",
    //   "assets/images/user-avatar3.png",
    //   "assets/images/user-avatar4.png",
    //   "assets/images/user-avatar5.png"];
    // //public aEventData : any;
    // public eventsArray: any[];
    function DashboardPage(navCtrl, navParams, httpProvider, storageProvider, loadingCtrl, modalCtrl, tourProvider) {
        // this.getUserEvents();
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.storageProvider = storageProvider;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.tourProvider = tourProvider;
        this.gotoWedDetails = function (event) {
            var _this = this;
            var loading = this.createLoading();
            loading.present();
            this.httpProvider.getEventDetailsById(event.id).then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__wed_details_wed_details__["a" /* WedDetailsPage */], {
                    event: res,
                    user: _this.user
                });
                loading.dismiss();
            }, function (err) {
                console.error(err);
            });
        };
        this.hasPermissions = function (event) {
        };
        this.watchEvent = function (event) {
            var _this = this;
            if (event.status) {
                if (event.status == 2) {
                    var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__attend_event_modal_attend_event_modal__["a" /* AttendEventModalPage */], {
                        event: event.id,
                        user: this.user.personal.id
                    });
                    var me = this;
                    modal.onDidDismiss(function (data) {
                        console.log(data);
                    });
                    modal.present();
                    return;
                }
            }
            var participants = event.participants;
            var isParticipant = false;
            for (var i = 0; i < participants.length; i++) {
                if (participants[i].id == this.user.personal.id) {
                    isParticipant = true;
                }
            }
            console.log(event.participants);
            if (isParticipant) {
                this.httpProvider.getEventSessionDetails(event.details.id, this.user.personal.id).then(function (res) {
                    console.log(res);
                    //this.sessionId = res.opentok.session_id;
                    // this.sessionId =
                    //this.token = res.opentok.token;
                    window.cordova.exec(null, null, 'OpentokActivator', 'activateNow', ['46014622', res.opentok.session_id, res.opentok.token, false]);
                }, function (err) {
                    console.error(err);
                });
            }
            else {
                // console.log(event);return;
                this.httpProvider.isActive(event.id).then(function (res) {
                    if (res) {
                        console.log(res);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__broadcast_broadcast__["a" /* BroadcastPage */], {
                            guest: true,
                            event: event.id,
                            user: _this.user.personal.id
                        });
                        // (<any>window).cordova.exec(null, null, 'OpentokActivator', 'activateNow', []);
                    }
                    else {
                        var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__attend_event_modal_attend_event_modal__["a" /* AttendEventModalPage */], {
                            event: event.id,
                            user: _this.user.personal.id
                        });
                        var me = _this;
                        modal.onDidDismiss(function (data) {
                            console.log(data);
                        });
                        modal.present();
                    }
                }, function (err) {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__attend_event_modal_attend_event_modal__["a" /* AttendEventModalPage */], {
                        event: event.id,
                        user: _this.user.personal.id
                    });
                    var me = _this;
                    modal.onDidDismiss(function (data) {
                        console.log(data);
                    });
                    modal.present();
                });
            }
        };
        if (this.navParams.get('userData')) {
            console.log(this.navParams.get('userData'));
            storageProvider.setProfile(this.navParams.get('userData')).then(function (res) { });
        }
        if (this.navParams.get('event')) {
            if (this.navParams.get('event') != null) {
                this.findEvent_id = this.navParams.get('event');
                this.findEvent(this.navParams.get('event'));
            }
        }
        var self = this;
        // setTimeout(function(){
        //   self.tourProvider.startTour();
        // }, 3000);
        // if (this.navParams.get('userData')){
        //   storage.set('userData', JSON.stringify(this.navParams.get('userData')));
        // }
        // storage.get('userData').then((val) => {
        //   this.user = JSON.parse(val);
        //   this.events = this.user.events;
        //   console.log(this.events);
        //   console.log(val);
        // });
        storageProvider.getProfile().then(function (profile) {
            _this.user = profile;
            if (profile.events == null) {
                _this.events = null;
            }
            else {
                _this.events = profile.events;
            }
            _this.httpProvider.getUserEvents(_this.user.personal.id).then(function (res) {
                _this.events = res.future;
                _this.past_events = res.past;
            }, function (err) {
                console.error(err);
            });
            // setInterval(function(){
            //   httpProvider.getUserEvents(profile.personal.id).then(
            //     (res) => {
            //       this.events = res.future;
            //       this.past_events = res.past;
            //     },
            //     (err) => {
            //         console.error(err);
            //     }
            //   );
            // }, 2000);
        }, function (err) {
            console.error(err);
        });
    }
    DashboardPage.prototype.ionViewWillEnter = function () {
        // console.log('Entering View');
    };
    DashboardPage.prototype.details = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__wed_details_wed_details__["a" /* WedDetailsPage */]);
    };
    DashboardPage.prototype.createEvent = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__create_an_event_create_an_event__["a" /* CreateAnEventPage */]);
    };
    DashboardPage.prototype.isEmpty = function () {
        if (typeof (this.events) == 'undefined') {
            return true;
        }
        else {
            if (this.events.length == 0) {
                return true;
            }
            else {
                return false;
            }
        }
    };
    DashboardPage.prototype.pastIsEmpty = function () {
        if (typeof (this.past_events) == 'undefined') {
            return true;
        }
        else {
            if (this.past_events.length == 0) {
                return true;
            }
            else {
                return false;
            }
        }
    };
    DashboardPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log(this.user);
        this.httpProvider.getUserEvents(this.user.personal.id).then(function (res) {
            _this.events = res.future;
            _this.past_events = res.past;
            refresher.complete();
        }, function (err) {
            console.error(err);
        });
    };
    DashboardPage.prototype.findEvent = function (event) {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.isPublic(this.findEvent_id).then(function (res) {
            if (res) {
                console.log(_this.findEvent_id);
                _this.httpProvider.getEventDetailsById(_this.findEvent_id).then(function (res) {
                    if (res) {
                        _this.watchEvent(res.details);
                        loading.dismiss();
                        _this.findEvent_id = null;
                    }
                }, function (err) {
                    console.error(err);
                });
            }
        }, function (err) {
            loading.dismiss();
            _this.findEvent_id = null;
        });
    };
    DashboardPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return DashboardPage;
}());
DashboardPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-dashboard',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/dashboard/dashboard.html"*/'<!--\n  Generated template for the DashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <main-navigation></main-navigation>\n\n</ion-header>\n\n\n<ion-content style="background:#d3dbdf;">\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-item no-lines class="join-event-banner" style="background-color: rgb(94, 172, 209); height:200px;" text-center>\n    <!-- <img src="assets/images/Background.svg" style="opacity:.3;"> -->\n    <div class="join-event-banner-title">\n      <h2>Join An Event</h2>\n      <ion-searchbar [(ngModel)]="findEvent_id" (search)="findEvent($event)" placeholder="Know the Event ID#?"></ion-searchbar>\n    </div>\n  </ion-item>\n  <div class="join-event-body-content">\n    <ion-list class="card-main-list {{ (isEmpty() ? \'no-events\' : \'\') }}">\n      <c-wed-card *ngFor="let aEvent of events;" [cWedCardEvent]="aEvent" [user]="user.personal.id"></c-wed-card>\n      <c-guest-card *ngFor="let aEvent of events;" [cWedCardEvent]="aEvent" [user]="user.personal.id"></c-guest-card>\n    </ion-list>\n\n    <ion-item class="{{ (pastIsEmpty() ? \'empty-events\' : \'has-events\') }}" text-center no-lines class="center-head">\n      <h2>Past Events</h2>\n    </ion-item>\n\n    <ion-list class="card-main-list" no-lines>\n      <c-wed-card *ngFor="let aEvent of past_events;" [cWedCardEvent]="aEvent" (click)="gotoWedDetails(aEvent);"></c-wed-card>\n      <c-guest-card *ngFor="let aEvent of past_events;" [cWedCardEvent]="aEvent" (click)="watchEvent(aEvent);"></c-guest-card>\n    </ion-list>\n\n  </div>\n</ion-content>\n\n<ion-footer class="bar-balanced">\n  <ion-toolbar>\n    <button (click)="createEvent()" ion-button full large color="light-blue">CREATE AN EVENT</button>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/dashboard/dashboard.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["b" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_8__providers_tour_tour__["a" /* TourProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_storage_storage__["b" /* StorageProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_tour_tour__["a" /* TourProvider */]])
], DashboardPage);

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickEmojiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PickEmojiPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var PickEmojiPage = (function () {
    function PickEmojiPage(navCtrl, navParams, httpProvider, viewCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.viewCtrl = viewCtrl;
        this.emojis = [];
        httpProvider.getEmojis().then(function (emojis) {
            _this.emojis = emojis;
        }, function (err) {
            console.error(err);
        });
    }
    PickEmojiPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PickEmojiPage');
    };
    PickEmojiPage.prototype.selected = function (fileName) {
        this.viewCtrl.dismiss({
            image: fileName
        });
    };
    PickEmojiPage.prototype.dimiss = function () {
        this.viewCtrl.dismiss();
    };
    return PickEmojiPage;
}());
PickEmojiPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-pick-emoji',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pick-emoji/pick-emoji.html"*/'<!--\n  Generated template for the PickEmojiPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Pick Emoji</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="dismiss()">\n        <ion-icon name="close" color="light"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item *ngFor="let emoji of emojis;" (click)="selected(emoji.filename)">\n      <img [src]="emoji.filename" style="width:100px;height:100px;" />\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/pick-emoji/pick-emoji.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */]])
], PickEmojiPage);

//# sourceMappingURL=pick-emoji.js.map

/***/ }),

/***/ 484:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompletePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AutocompletePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var AutocompletePage = (function () {
    function AutocompletePage(viewCtrl, zone, renderer) {
        this.viewCtrl = viewCtrl;
        this.zone = zone;
        this.renderer = renderer;
        this.service = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'my-popup', true);
    }
    AutocompletePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AutocompletePage.prototype.chooseItem = function (item) {
        this.viewCtrl.dismiss(item);
    };
    AutocompletePage.prototype.updateSearch = function () {
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var me = this;
        this.service.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: { country: 'US' } }, function (predictions, status) {
            me.autocompleteItems = [];
            me.zone.run(function () {
                predictions.forEach(function (prediction) {
                    me.autocompleteItems.push(prediction);
                });
            });
        });
    };
    return AutocompletePage;
}());
AutocompletePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-autocomplete',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/autocomplete/autocomplete.html"*/'<ion-header>\n  <ion-toolbar>\n    <ion-searchbar [(ngModel)]="autocomplete.query" [showCancelButton]="true" (ionInput)="updateSearch()" (ionCancel)="dismiss()"></ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor="let item of autocompleteItems" tappable (click)="chooseItem(item)">\n      {{ item.description }}\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/autocomplete/autocomplete.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_0__angular_core__["Renderer"]])
], AutocompletePage);

//# sourceMappingURL=autocomplete.js.map

/***/ }),

/***/ 485:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkthroughPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the WalkthroughPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var WalkthroughPage = (function () {
    function WalkthroughPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // customize your ionic slider if needed
        // http://ionicframework.com/docs/api/components/slides/Slides/
        this.mySlideOptions = {};
        // let's use the same buttons on each slide.
        // you can define your different button per slide if you want to
        var _this = this; // keeping a reference so we can access `this.slider` from our onClick function
        var buttons = {
            left: {
                text: 'SKIP',
                textColor: '#fff',
                onClick: function () {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
                }
            },
            right: {
                text: 'NEXT',
                textColor: '#fff',
                onClick: function () {
                    if (_this.slider.isEnd()) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
                    }
                    else {
                        _this.slider.slideNext(); //using the ionic slider to go to the next slide
                    }
                }
            }
        };
        this.slides = [
            {
                title: 'H-App-y Ever After',
                description: '<b>Web Wed Mobile</b> lets you live stream your special moment and share it with the people you care about most.',
                image: './assets/screenshots/ios_1.png',
                styles: {
                    background: 'linear-gradient(to right, #023c6f 0%, #5aabd3 100%)',
                    titleColor: '#fff',
                    descriptionColor: '#fff'
                },
                layout: {
                    position: 'bottom',
                    deviceType: 'iphone',
                    deviceColor: 'silver' //iphone: silver, spacegrey, gold   android: white, black
                },
                buttons: buttons
            },
            {
                title: 'Near or Far',
                description: 'No one ha to skip out on your special day.',
                image: './assets/screenshots/ios_2.png',
                styles: {
                    background: 'linear-gradient(to right, #023c6f 0%, #5aabd3 100%)',
                    titleColor: '#fff',
                    descriptionColor: '#fff'
                },
                layout: {
                    position: 'top',
                    deviceType: 'iphone',
                    deviceColor: 'spacegrey'
                },
                buttons: buttons
            },
        ];
    }
    WalkthroughPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WalkthroughPage');
    };
    return WalkthroughPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('mySlider'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Slides */])
], WalkthroughPage.prototype, "slider", void 0);
WalkthroughPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-walkthrough',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/walkthrough/walkthrough.html"*/'<ion-content>\n  <ion-slides #mySlider>\n    <ion-slide *ngFor="let slide of slides">\n      <!-- insert ion-walkthrough component here -->\n      <ion-walkthrough [options]="slide"></ion-walkthrough>\n    </ion-slide>\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/walkthrough/walkthrough.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], WalkthroughPage);

//# sourceMappingURL=walkthrough.js.map

/***/ }),

/***/ 488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(493);



Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 493:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_plus__ = __webpack_require__(297);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2__ = __webpack_require__(850);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angularfire2_database__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_component__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_intro_intro__ = __webpack_require__(859);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_dashboard_dashboard__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_login_login__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login_module__ = __webpack_require__(860);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_my_account_my_account__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_notifications_notifications__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_notifications_notifications_module__ = __webpack_require__(861);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_register_register__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_register_register_module__ = __webpack_require__(871);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_dashboard_dashboard_module__ = __webpack_require__(872);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_wed_details_wed_details__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_wed_details_wed_details_module__ = __webpack_require__(876);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_create_an_event_create_an_event__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_create_an_event_create_an_event_module__ = __webpack_require__(877);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_registry_link_registry_link__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_marriage_license_marriage_license__ = __webpack_require__(119);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_order_details_order_details__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_create_an_invitation_create_an_invitation__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_pre_approved_officiant_pre_approved_officiant__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_officiant_details_officiant_details__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__ionic_storage__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__components_components_module__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__angular_platform_browser_animations__ = __webpack_require__(878);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_pick_contact_pick_contact_module__ = __webpack_require__(881);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_autocomplete_autocomplete_module__ = __webpack_require__(882);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_attend_event_modal_attend_event_modal_module__ = __webpack_require__(883);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_add_manual_modal_add_manual_modal_module__ = __webpack_require__(884);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_role_popover_role_popover_module__ = __webpack_require__(885);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_broadcast_broadcast_module__ = __webpack_require__(887);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_archive_archive_module__ = __webpack_require__(888);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_event_completed_event_completed_module__ = __webpack_require__(889);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_pick_emoji_pick_emoji_module__ = __webpack_require__(890);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_walkthrough_walkthrough_module__ = __webpack_require__(891);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_checkout_details_checkout_details_module__ = __webpack_require__(892);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_marriage_application_marriage_application_module__ = __webpack_require__(893);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_marriage_education_marriage_education_module__ = __webpack_require__(894);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__providers_tour_tour__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_pay_pay__ = __webpack_require__(367);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















































// import { OpentokModule } from "ng2-opentok/dist/opentok.module"


var firebaseConfig = {
    apiKey: "AIzaSyBHLBBX-AwKAfE2VX2K1twMo14QuUh86F8",
    authDomain: "playground-dd1d8.firebaseapp.com",
    databaseURL: "https://playground-dd1d8.firebaseio.com",
    projectId: "playground-dd1d8",
    storageBucket: "playground-dd1d8.appspot.com",
    messagingSenderId: "328542486655"
};
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_14__pages_intro_intro__["a" /* IntroPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_my_account_my_account__["a" /* MyAccountPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_registry_link_registry_link__["a" /* RegistryLinkPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_marriage_license_marriage_license__["a" /* MarriageLicensePage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_order_details_order_details__["a" /* OrderDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_create_an_invitation_create_an_invitation__["a" /* CreateAnInvitationPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_pre_approved_officiant_pre_approved_officiant__["a" /* PreApprovedOfficiantPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_officiant_details_officiant_details__["a" /* OfficiantDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_52__pages_pay_pay__["a" /* PayPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */], {
                mode: 'md'
            }, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_34__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_12__angular_forms__["f" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_22__pages_register_register_module__["a" /* RegisterPageModule */],
            __WEBPACK_IMPORTED_MODULE_17__pages_login_login_module__["a" /* LoginPageModule */],
            __WEBPACK_IMPORTED_MODULE_23__pages_dashboard_dashboard_module__["a" /* DashboardPageModule */],
            __WEBPACK_IMPORTED_MODULE_25__pages_wed_details_wed_details_module__["a" /* WedDetailsPageModule */],
            __WEBPACK_IMPORTED_MODULE_27__pages_create_an_event_create_an_event_module__["a" /* CreateAnEventPageModule */],
            __WEBPACK_IMPORTED_MODULE_9_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
            __WEBPACK_IMPORTED_MODULE_10_angularfire2_database__["b" /* AngularFireDatabaseModule */],
            __WEBPACK_IMPORTED_MODULE_11_angularfire2_auth__["b" /* AngularFireAuthModule */],
            __WEBPACK_IMPORTED_MODULE_20__pages_notifications_notifications_module__["a" /* NotificationsPageModule */],
            __WEBPACK_IMPORTED_MODULE_36__components_components_module__["a" /* ComponentsModule */],
            __WEBPACK_IMPORTED_MODULE_37__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_38__pages_pick_contact_pick_contact_module__["a" /* PickContactPageModule */],
            __WEBPACK_IMPORTED_MODULE_39__pages_autocomplete_autocomplete_module__["a" /* AutocompletePageModule */],
            __WEBPACK_IMPORTED_MODULE_40__pages_attend_event_modal_attend_event_modal_module__["a" /* AttendEventModalPageModule */],
            __WEBPACK_IMPORTED_MODULE_41__pages_add_manual_modal_add_manual_modal_module__["a" /* AddManualModalPageModule */],
            __WEBPACK_IMPORTED_MODULE_42__pages_role_popover_role_popover_module__["a" /* RolePopoverPageModule */],
            __WEBPACK_IMPORTED_MODULE_43__pages_broadcast_broadcast_module__["a" /* BroadcastPageModule */],
            __WEBPACK_IMPORTED_MODULE_44__pages_archive_archive_module__["a" /* ArchivePageModule */],
            __WEBPACK_IMPORTED_MODULE_45__pages_event_completed_event_completed_module__["a" /* EventCompletedPageModule */],
            __WEBPACK_IMPORTED_MODULE_46__pages_pick_emoji_pick_emoji_module__["a" /* PickEmojiPageModule */],
            __WEBPACK_IMPORTED_MODULE_47__pages_walkthrough_walkthrough_module__["a" /* WalkthroughPageModule */],
            __WEBPACK_IMPORTED_MODULE_48__pages_checkout_details_checkout_details_module__["a" /* CheckoutDetailsPageModule */],
            __WEBPACK_IMPORTED_MODULE_49__pages_marriage_application_marriage_application_module__["a" /* MarriageApplicationPageModule */],
            __WEBPACK_IMPORTED_MODULE_50__pages_marriage_education_marriage_education_module__["a" /* MarriageEducationPageModule */],
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_13__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_14__pages_intro_intro__["a" /* IntroPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_dashboard_dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_16__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_18__pages_my_account_my_account__["a" /* MyAccountPage */],
            __WEBPACK_IMPORTED_MODULE_19__pages_notifications_notifications__["a" /* NotificationsPage */],
            __WEBPACK_IMPORTED_MODULE_21__pages_register_register__["a" /* RegisterPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_wed_details_wed_details__["a" /* WedDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_create_an_event_create_an_event__["a" /* CreateAnEventPage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_registry_link_registry_link__["a" /* RegistryLinkPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_marriage_license_marriage_license__["a" /* MarriageLicensePage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_order_details_order_details__["a" /* OrderDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_create_an_invitation_create_an_invitation__["a" /* CreateAnInvitationPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_pre_approved_officiant_pre_approved_officiant__["a" /* PreApprovedOfficiantPage */],
            __WEBPACK_IMPORTED_MODULE_33__pages_officiant_details_officiant_details__["a" /* OfficiantDetailsPage */],
            __WEBPACK_IMPORTED_MODULE_52__pages_pay_pay__["a" /* PayPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_keyboard__["a" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_google_plus__["a" /* GooglePlus */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_35__providers_storage_storage__["b" /* StorageProvider */],
            __WEBPACK_IMPORTED_MODULE_51__providers_tour_tour__["a" /* TourProvider */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__ = __webpack_require__(594);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UploadService = (function () {
    function UploadService() {
        var _this = this;
        this.progress = __WEBPACK_IMPORTED_MODULE_1_rxjs_Rx__["Observable"].create(function (observer) {
            _this.progressObserver = observer;
        }).share();
    }
    UploadService.prototype.makeFileRequest = function (url, params, files, token) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var formData = new FormData(), xhr = new XMLHttpRequest();
            // for (let i = 0; i < files.length; i++) {
            //   formData.append("uploads", files[i], files[i].name);
            // }
            for (var i = 0; i < Object.keys(params).length; i++) {
                formData.append(Object.keys(params)[i], params[i]);
            }
            formData.append('file', files[0], 'file');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        console.log(xhr.response);
                        resolve(xhr.response);
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.upload.onprogress = function (event) {
                _this.progress = Math.round(event.loaded / event.total * 100);
                //this.progressObserver.next(this.progress);
                console.log('progress: ' + _this.progress);
            };
            xhr.open('POST', url, true);
            // xhr.setRequestHeader('Content-Type', 'multipart/form-data');
            xhr.setRequestHeader("X-Requested-Auth", "eyJhbGciOiJSUzI1NiIsImtpZCI6ImY3ZGFjOGY4MzYwNjVjODgwODNkMzkwNzZhODU4OWEzMTk5NWZjNDQifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcGxheWdyb3VuZC1kZDFkOCIsImF1ZCI6InBsYXlncm91bmQtZGQxZDgiLCJhdXRoX3RpbWUiOjE1MDM1ODI5MjYsInVzZXJfaWQiOiJDSHBPOUdQcGFGZ2tzMU4wN2syOThNVmQ4dVYyIiwic3ViIjoiQ0hwTzlHUHBhRmdrczFOMDdrMjk4TVZkOHVWMiIsImlhdCI6MTUwMzU4MzEyMCwiZXhwIjoxNTAzNTg2NzIwLCJlbWFpbCI6InN3ZWF0LmF1c3RpbkByaXZlbG9wZXIuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInN3ZWF0LmF1c3RpbkByaXZlbG9wZXIuY29tIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.Y1d8CKLdM6aBw9-mBKSVAOlZEpoVeHMBFXyn2RxqW-BuDpS7etkPMTlKj8CkpP5D_90dhPbOc_RTezC7TX4UbiUEKBtBaaQzSGUbB5h8lb8zKot7L-GuKYqQjUpMIhyBUkmoTs2FUKCf3L3jPoEH5165IenkehjNaNU9H8mDelbnVo0KGfPnkiFa6A4qKGnFWF0PEBpdmvjzBk2wrbb3kPC2mzni9WSEwVISyquuO79fv3GRzdf4ydOIZiL_elle64bj3fZHVyj9pmTeGOqs0LrQ9dROqUDgGdZdfQyOFRV8jZOCcES1Rly0-70sxKghVvRVEJxJR1_2w8_ao7dd4A");
            xhr.send(formData);
        });
    };
    return UploadService;
}());
UploadService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], UploadService);

//# sourceMappingURL=fileUpload.service.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__main_navigation_main_navigation__ = __webpack_require__(862);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__sub_navigation_sub_navigation__ = __webpack_require__(863);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_component__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__circular_progress_bar_circular_progress_bar__ = __webpack_require__(864);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__ = __webpack_require__(865);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__share_share__ = __webpack_require__(866);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__avatar_avatar__ = __webpack_require__(867);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__options_options__ = __webpack_require__(868);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__broadcast_count_down_broadcast_count_down__ = __webpack_require__(869);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ion_walkthrough__ = __webpack_require__(870);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












// import {VideoRecepientComponent} from "./video-recepient/video-recepient.component";
// import {VideoCallerComponent} from "./video-caller/video-caller.component";
// import {LoadingComponent} from "./shared/loading/loading.component";
// import {VideoCallWidgetComponent} from "./shared/video-call-widget/video-call-widget.component";
var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    return ComponentsModule;
}());
ComponentsModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__main_navigation_main_navigation__["a" /* MainNavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_3__sub_navigation_sub_navigation__["a" /* SubNavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_5__circular_progress_bar_circular_progress_bar__["a" /* CircularProgressBarComponent */],
            __WEBPACK_IMPORTED_MODULE_7__share_share__["a" /* ShareComponent */],
            __WEBPACK_IMPORTED_MODULE_8__avatar_avatar__["a" /* AvatarComponent */],
            __WEBPACK_IMPORTED_MODULE_9__options_options__["a" /* OptionsComponent */],
            __WEBPACK_IMPORTED_MODULE_10__broadcast_count_down_broadcast_count_down__["a" /* BroadcastCountDownComponent */],
            __WEBPACK_IMPORTED_MODULE_11__ion_walkthrough__["a" /* IonWalkthrough */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_app_component__["a" /* MyApp */]),
            __WEBPACK_IMPORTED_MODULE_6_angular_svg_round_progressbar__["RoundProgressModule"]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__main_navigation_main_navigation__["a" /* MainNavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_3__sub_navigation_sub_navigation__["a" /* SubNavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_5__circular_progress_bar_circular_progress_bar__["a" /* CircularProgressBarComponent */],
            __WEBPACK_IMPORTED_MODULE_7__share_share__["a" /* ShareComponent */],
            __WEBPACK_IMPORTED_MODULE_8__avatar_avatar__["a" /* AvatarComponent */],
            __WEBPACK_IMPORTED_MODULE_9__options_options__["a" /* OptionsComponent */],
            __WEBPACK_IMPORTED_MODULE_10__broadcast_count_down_broadcast_count_down__["a" /* BroadcastCountDownComponent */],
            __WEBPACK_IMPORTED_MODULE_11__ion_walkthrough__["a" /* IonWalkthrough */],
        ],
    })
], ComponentsModule);

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 855:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoodlioPay; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);



var NoodlioPay = (function () {
    function NoodlioPay(http) {
        this.http = http;
        /**
        * Noodlio Pay Settings
        *
        *   Obtain your 'stripe_account' id by visting (both links):
        *   - https://www.noodl.io/pay/connect
        *   - https://www.noodl.io/pay/connect/test
        *
        *   Obtain your 'mashape_key' by visiting:
        *   - https://noodlio-pay.p.mashape.com (press on get 'Get your API keys and start hacking')
        */
        this.stripe_account = "acct_16bvtBHW84OuTX9V";
        this.mashape_key = "3fEagjJCGAmshMqVnwTR70bVqG3yp1lerJNjsnTzx5ODeOa99V";
        this.test = 'true';
        this.URL = 'https://noodlio-pay.p.mashape.com';
        //URL = 'https://m-s-api-sibizavic.c9users.io';
        this.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'X-Mashape-Key': this.mashape_key,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        });
    }
    Object.defineProperty(NoodlioPay, "parameters", {
        /**
        * Init other (do not change)
        */
        get: function () { return [[__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]]]; },
        enumerable: true,
        configurable: true
    });
    /**
    * Main wrapper for charging the client
    * Validates the credit card first (A), and then charges the client using the obtained 'source' (B)
    */
    NoodlioPay.prototype.charge = function (data) {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"].create(function (observer) {
            // A. Validate Card
            _this.validateCard(data).subscribe(function (resA) {
                if (resA.hasOwnProperty('id')) {
                    // B. Charge Card
                    var source = resA.id;
                    _this.chargeCard(source, data).subscribe(function (resB) {
                        observer.next(resB);
                    });
                    //observer.next(source);
                }
                else {
                    // error
                    observer.next(resA);
                }
            });
        });
    };
    /**
    * POST /tokens/create
    */
    NoodlioPay.prototype.validateCard = function (data) {
        var valUrl = this.URL + '/tokens/create';
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: this.headers });
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* URLSearchParams */]();
        params.append('number', data.number);
        params.append('exp_month', data.exp_month);
        params.append('exp_year', data.exp_year);
        params.append('cvc', data.cvc);
        params.append('test', this.test);
        return this.http.post(valUrl, params, options)
            .map(function (res) { return res.json(); });
    };
    /**
    * POST /charge/token
    */
    NoodlioPay.prototype.chargeCard = function (source, data) {
        var valUrl = this.URL + '/charge/token';
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({ headers: this.headers });
        var params = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* URLSearchParams */]();
        params.append('amount', data.amountCents); // In cents
        params.append('currency', data.currency);
        params.append('description', data.name); // or custom description
        params.append('source', source);
        params.append('stripe_account', this.stripe_account);
        params.append('test', this.test);
        return this.http
            .post(valUrl, params, options)
            .map(function (res) { return res.json(); });
    };
    return NoodlioPay;
}());

//# sourceMappingURL=noodliopay.js.map

/***/ }),

/***/ 857:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 368,
	"./af.js": 368,
	"./ar": 369,
	"./ar-dz": 370,
	"./ar-dz.js": 370,
	"./ar-kw": 371,
	"./ar-kw.js": 371,
	"./ar-ly": 372,
	"./ar-ly.js": 372,
	"./ar-ma": 373,
	"./ar-ma.js": 373,
	"./ar-sa": 374,
	"./ar-sa.js": 374,
	"./ar-tn": 375,
	"./ar-tn.js": 375,
	"./ar.js": 369,
	"./az": 376,
	"./az.js": 376,
	"./be": 377,
	"./be.js": 377,
	"./bg": 378,
	"./bg.js": 378,
	"./bn": 379,
	"./bn.js": 379,
	"./bo": 380,
	"./bo.js": 380,
	"./br": 381,
	"./br.js": 381,
	"./bs": 382,
	"./bs.js": 382,
	"./ca": 383,
	"./ca.js": 383,
	"./cs": 384,
	"./cs.js": 384,
	"./cv": 385,
	"./cv.js": 385,
	"./cy": 386,
	"./cy.js": 386,
	"./da": 387,
	"./da.js": 387,
	"./de": 388,
	"./de-at": 389,
	"./de-at.js": 389,
	"./de-ch": 390,
	"./de-ch.js": 390,
	"./de.js": 388,
	"./dv": 391,
	"./dv.js": 391,
	"./el": 392,
	"./el.js": 392,
	"./en-au": 393,
	"./en-au.js": 393,
	"./en-ca": 394,
	"./en-ca.js": 394,
	"./en-gb": 395,
	"./en-gb.js": 395,
	"./en-ie": 396,
	"./en-ie.js": 396,
	"./en-nz": 397,
	"./en-nz.js": 397,
	"./eo": 398,
	"./eo.js": 398,
	"./es": 399,
	"./es-do": 400,
	"./es-do.js": 400,
	"./es.js": 399,
	"./et": 401,
	"./et.js": 401,
	"./eu": 402,
	"./eu.js": 402,
	"./fa": 403,
	"./fa.js": 403,
	"./fi": 404,
	"./fi.js": 404,
	"./fo": 405,
	"./fo.js": 405,
	"./fr": 406,
	"./fr-ca": 407,
	"./fr-ca.js": 407,
	"./fr-ch": 408,
	"./fr-ch.js": 408,
	"./fr.js": 406,
	"./fy": 409,
	"./fy.js": 409,
	"./gd": 410,
	"./gd.js": 410,
	"./gl": 411,
	"./gl.js": 411,
	"./gom-latn": 412,
	"./gom-latn.js": 412,
	"./he": 413,
	"./he.js": 413,
	"./hi": 414,
	"./hi.js": 414,
	"./hr": 415,
	"./hr.js": 415,
	"./hu": 416,
	"./hu.js": 416,
	"./hy-am": 417,
	"./hy-am.js": 417,
	"./id": 418,
	"./id.js": 418,
	"./is": 419,
	"./is.js": 419,
	"./it": 420,
	"./it.js": 420,
	"./ja": 421,
	"./ja.js": 421,
	"./jv": 422,
	"./jv.js": 422,
	"./ka": 423,
	"./ka.js": 423,
	"./kk": 424,
	"./kk.js": 424,
	"./km": 425,
	"./km.js": 425,
	"./kn": 426,
	"./kn.js": 426,
	"./ko": 427,
	"./ko.js": 427,
	"./ky": 428,
	"./ky.js": 428,
	"./lb": 429,
	"./lb.js": 429,
	"./lo": 430,
	"./lo.js": 430,
	"./lt": 431,
	"./lt.js": 431,
	"./lv": 432,
	"./lv.js": 432,
	"./me": 433,
	"./me.js": 433,
	"./mi": 434,
	"./mi.js": 434,
	"./mk": 435,
	"./mk.js": 435,
	"./ml": 436,
	"./ml.js": 436,
	"./mr": 437,
	"./mr.js": 437,
	"./ms": 438,
	"./ms-my": 439,
	"./ms-my.js": 439,
	"./ms.js": 438,
	"./my": 440,
	"./my.js": 440,
	"./nb": 441,
	"./nb.js": 441,
	"./ne": 442,
	"./ne.js": 442,
	"./nl": 443,
	"./nl-be": 444,
	"./nl-be.js": 444,
	"./nl.js": 443,
	"./nn": 445,
	"./nn.js": 445,
	"./pa-in": 446,
	"./pa-in.js": 446,
	"./pl": 447,
	"./pl.js": 447,
	"./pt": 448,
	"./pt-br": 449,
	"./pt-br.js": 449,
	"./pt.js": 448,
	"./ro": 450,
	"./ro.js": 450,
	"./ru": 451,
	"./ru.js": 451,
	"./sd": 452,
	"./sd.js": 452,
	"./se": 453,
	"./se.js": 453,
	"./si": 454,
	"./si.js": 454,
	"./sk": 455,
	"./sk.js": 455,
	"./sl": 456,
	"./sl.js": 456,
	"./sq": 457,
	"./sq.js": 457,
	"./sr": 458,
	"./sr-cyrl": 459,
	"./sr-cyrl.js": 459,
	"./sr.js": 458,
	"./ss": 460,
	"./ss.js": 460,
	"./sv": 461,
	"./sv.js": 461,
	"./sw": 462,
	"./sw.js": 462,
	"./ta": 463,
	"./ta.js": 463,
	"./te": 464,
	"./te.js": 464,
	"./tet": 465,
	"./tet.js": 465,
	"./th": 466,
	"./th.js": 466,
	"./tl-ph": 467,
	"./tl-ph.js": 467,
	"./tlh": 468,
	"./tlh.js": 468,
	"./tr": 469,
	"./tr.js": 469,
	"./tzl": 470,
	"./tzl.js": 470,
	"./tzm": 471,
	"./tzm-latn": 472,
	"./tzm-latn.js": 472,
	"./tzm.js": 471,
	"./uk": 473,
	"./uk.js": 473,
	"./ur": 474,
	"./ur.js": 474,
	"./uz": 475,
	"./uz-latn": 476,
	"./uz-latn.js": 476,
	"./uz.js": 475,
	"./vi": 477,
	"./vi.js": 477,
	"./x-pseudo": 478,
	"./x-pseudo.js": 478,
	"./yo": 479,
	"./yo.js": 479,
	"./zh-cn": 480,
	"./zh-cn.js": 480,
	"./zh-hk": 481,
	"./zh-hk.js": 481,
	"./zh-tw": 482,
	"./zh-tw.js": 482
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 857;

/***/ }),

/***/ 859:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the IntroPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var IntroPage = (function () {
    function IntroPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IntroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IntroPage');
    };
    return IntroPage;
}());
IntroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-intro',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/intro/intro.html"*/'<!--\n  Generated template for the IntroPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n<ion-content padding>\n  \n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/intro/intro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
], IntroPage);

//# sourceMappingURL=intro.js.map

/***/ }),

/***/ 860:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(88);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
        ],
    })
], LoginPageModule);

// WEBPACK FOOTER //
// ./src/pages/login/login.module.ts
//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 861:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notifications__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NotificationsPageModule = (function () {
    function NotificationsPageModule() {
    }
    return NotificationsPageModule;
}());
NotificationsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notifications__["a" /* NotificationsPage */]), __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], NotificationsPageModule);

//# sourceMappingURL=notifications.module.js.map

/***/ }),

/***/ 862:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainNavigationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_my_account_my_account__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_notifications_notifications__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_dashboard_dashboard__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the MainNavigationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var MainNavigationComponent = (function () {
    function MainNavigationComponent(navCtrl, statusBar) {
        this.navCtrl = navCtrl;
        this.statusBar = statusBar;
        this.title = '';
        this.selected_tab = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selected_tab_identifier = '';
        // console.log('MainNavigation constructed');
    }
    MainNavigationComponent.prototype.ngAfterViewInit = function () {
        this.navTitle = this.title;
        // let status bar overlay webview
        this.statusBar.overlaysWebView(false);
        //White text
        this.statusBar.styleLightContent();
        // set status bar to white
        this.statusBar.backgroundColorByHexString('#1658a0');
        if (this.tabs) {
            this.selectTab(0);
        }
    };
    MainNavigationComponent.prototype.gotoDashboard = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_dashboard_dashboard__["a" /* DashboardPage */]);
    };
    MainNavigationComponent.prototype.gotoNotifications = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_notifications_notifications__["a" /* NotificationsPage */]);
    };
    MainNavigationComponent.prototype.gotoProfile = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_my_account_my_account__["a" /* MyAccountPage */]);
    };
    MainNavigationComponent.prototype.selectTab = function (index) {
        this.selected_tab_identifier = this.tabs[index];
        this.selected_tab.emit(index);
    };
    return MainNavigationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('title'),
    __metadata("design:type", String)
], MainNavigationComponent.prototype, "title", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('tabs'),
    __metadata("design:type", Object)
], MainNavigationComponent.prototype, "tabs", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('selected-tab'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
], MainNavigationComponent.prototype, "selected_tab", void 0);
MainNavigationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'main-navigation',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/main-navigation/main-navigation.html"*/'<ion-navbar color="primary">\n  <button ion-button icon-only menuToggle>\n    <ion-icon name="menu"></ion-icon>\n  </button>\n\n  <ion-title *ngIf="navTitle==\'\'">\n    <!-- {{navTitle}} -->\n    <img src="assets/images/WebWed-Logo.svg" style="max-width:80px;padding-top:5px;"/>\n  </ion-title>\n  <ion-title *ngIf="navTitle!=\'\'">\n    {{navTitle}}\n  </ion-title>\n\n  <ion-buttons end>\n    <button ion-button icon-only (click)="gotoDashboard()">\n      <ion-icon name="heart"></ion-icon>\n    </button>\n    <button ion-button icon-only (click)="gotoNotifications()">\n      <ion-icon name="notifications"></ion-icon>\n    </button>\n    <button ion-button icon-only (click)="gotoProfile()">\n      <ion-icon name="contact"></ion-icon>\n    </button>\n  </ion-buttons>\n</ion-navbar>\n<div class="segment-tab" *ngIf="tabs!=false">\n   <ion-segment [(ngModel)]="selected_tab_identifier">\n     <ion-segment-button *ngFor="let tab of tabs; let i = index;" (click)="selectTab(i)" value="{{tab}}">\n       {{tab}}\n     </ion-segment-button>\n   </ion-segment>\n </div>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/main-navigation/main-navigation.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */]])
], MainNavigationComponent);

//# sourceMappingURL=main-navigation.js.map

/***/ }),

/***/ 863:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubNavigationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_my_account_my_account__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_notifications_notifications__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the MainNavigationComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var SubNavigationComponent = (function () {
    function SubNavigationComponent(navCtrl, statusBar) {
        this.navCtrl = navCtrl;
        this.statusBar = statusBar;
        this.selected_tab = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
        this.selected_tab_identifier = '';
    }
    SubNavigationComponent.prototype.ngAfterViewInit = function () {
        this.navTitle = this.title;
        // let status bar overlay webview
        this.statusBar.overlaysWebView(false);
        //White text
        this.statusBar.styleLightContent();
        // set status bar to white
        this.statusBar.backgroundColorByHexString('#1658a0');
        if (this.tabs) {
            this.selectTab(0);
        }
    };
    SubNavigationComponent.prototype.gotoNotifications = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__pages_notifications_notifications__["a" /* NotificationsPage */]);
    };
    SubNavigationComponent.prototype.gotoProfile = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_1__pages_my_account_my_account__["a" /* MyAccountPage */]);
    };
    SubNavigationComponent.prototype.selectTab = function (index) {
        this.selected_tab_identifier = this.tabs[index];
        this.selected_tab.emit(index);
    };
    return SubNavigationComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('title'),
    __metadata("design:type", Object)
], SubNavigationComponent.prototype, "title", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('tabs'),
    __metadata("design:type", Object)
], SubNavigationComponent.prototype, "tabs", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('selected-tab'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
], SubNavigationComponent.prototype, "selected_tab", void 0);
SubNavigationComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'sub-navigation',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/sub-navigation/sub-navigation.html"*/'<ion-navbar>\n  <ion-title *ngIf="navTitle==\'\'">\n    <!-- {{navTitle}} -->\n    <img src="assets/images/WebWed-Logo.svg" style="max-width:80px;padding-top:5px;"/>\n  </ion-title>\n  <ion-title *ngIf="navTitle!=\'\'">\n    {{navTitle}}\n  </ion-title>\n</ion-navbar>\n<div class="segment-tab" *ngIf="tabs!=false">\n   <ion-segment [(ngModel)]="selected_tab_identifier">\n     <ion-segment-button *ngFor="let tab of tabs; let i = index;" (click)="selectTab(i)" value="{{tab}}">\n       {{tab}}\n     </ion-segment-button>\n   </ion-segment>\n </div>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/sub-navigation/sub-navigation.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */]])
], SubNavigationComponent);

//# sourceMappingURL=sub-navigation.js.map

/***/ }),

/***/ 864:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CircularProgressBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the CircularProgressBarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var CircularProgressBarComponent = (function () {
    function CircularProgressBarComponent() {
        this.progress = 0;
        this.color = '#00bbd3';
        this.placeholderColor = '#d6d8db';
        this.state = 1;
        this.max = '100';
        // console.log('Hello CircularProgressBarComponent Component');
        this.width = 250;
    }
    CircularProgressBarComponent.prototype.ngAfterContentInit = function () {
        if (this.state == 1) {
            this.backgroundUrl = "assets/images/heart_active.png";
        }
        else {
            this.backgroundUrl = "assets/images/heart_disable.png";
        }
        this.imageWidth = this.width;
    };
    CircularProgressBarComponent.prototype.progressValueChanged = function () {
        if (this.progress > 101) {
            this.backgroundUrl = "assets/images/heart_broadcast.png";
            this.imageWidth = this.width + 2;
        }
    };
    return CircularProgressBarComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('progress'),
    __metadata("design:type", Object)
], CircularProgressBarComponent.prototype, "progress", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('width'),
    __metadata("design:type", Object)
], CircularProgressBarComponent.prototype, "width", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('color'),
    __metadata("design:type", Object)
], CircularProgressBarComponent.prototype, "color", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('placeholder-color'),
    __metadata("design:type", Object)
], CircularProgressBarComponent.prototype, "placeholderColor", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('state'),
    __metadata("design:type", Object)
], CircularProgressBarComponent.prototype, "state", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('max'),
    __metadata("design:type", Object)
], CircularProgressBarComponent.prototype, "max", void 0);
CircularProgressBarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'heart-button',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/circular-progress-bar/circular-progress-bar.html"*/'<!-- Generated template for the CircularProgressBarComponent component -->\n<div class="outerDiv" [style.background]="\'url(\' + backgroundUrl +\')\'">\n  <div class="overlayDiv"></div>\n  <round-progress style="margin:0 auto;" class="round-progress child" [style.background-size]=" imageWidth + \'px \' + imageWidth + \'px\'"\n    [stroke]="width/2<125 ? 7 : 15" [current]="progress" [radius]="width/2" [max]="max" [color]="color" [background]="placeholderColor"\n    (onRender)="progressValueChanged()">\n  </round-progress>\n</div>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/circular-progress-bar/circular-progress-bar.html"*/
    }),
    __metadata("design:paramtypes", [])
], CircularProgressBarComponent);

//# sourceMappingURL=circular-progress-bar.js.map

/***/ }),

/***/ 866:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShareComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__ = __webpack_require__(487);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ShareComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var ShareComponent = (function () {
    function ShareComponent(httpProvider, socialSharing) {
        this.httpProvider = httpProvider;
        this.socialSharing = socialSharing;
        this.hasTitle = true;
        this.hasShare = true;
    }
    ShareComponent.prototype.ngAfterViewInit = function () {
        // if (this.eventId){
        //   let eventId = this.eventId;
        //
        //   this.httpProvider.isPublic(eventId).then(
        //     (res) => {
        //       this.hasShare = res;
        //     },
        //     (err) => {
        //       console.error(err);
        //     }
        //   );
        // }else{
        //   let userId = this.userId;
        // }
    };
    ShareComponent.prototype.share = function () {
        var message = '';
        var url = '';
        if (this.eventId) {
            message = 'Share event with a friend.';
            url = this.httpProvider.apiUrl + "/event/share/" + this.eventId;
        }
        else {
            message = 'Share user with a friend.';
            url = this.httpProvider.apiUrl + "/user/share/" + this.userId;
        }
        this.socialSharing.share(message, 'Share', null, url).then(function () {
            console.log('shared');
        }).catch(function () {
            // Error!
        });
    };
    return ShareComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('user'),
    __metadata("design:type", Object)
], ShareComponent.prototype, "userId", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('event'),
    __metadata("design:type", Object)
], ShareComponent.prototype, "eventId", void 0);
ShareComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'share',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/share/share.html"*/'<!-- Generated template for the ShareComponent component -->\n<ion-item no-lines text-center class="margin-item" (click)="share()">\n  <button color="primary" class="mini-btn" [ngClass]="hasShare ? \'active\' : \'disabled\'">\n    <ion-icon name="share-alt" class="share-icon"></ion-icon>\n  </button>\n  <h6 class="mini-text" *ngIf="hasTitle">Share</h6>\n</ion-item>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/share/share.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_1__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_social_sharing__["a" /* SocialSharing */]])
], ShareComponent);

//# sourceMappingURL=share.js.map

/***/ }),

/***/ 867:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AvatarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_http_http__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AvatarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var AvatarComponent = (function () {
    function AvatarComponent(httpProvider) {
        this.httpProvider = httpProvider;
        this.showName = false;
        this.avatar = '';
    }
    AvatarComponent.prototype.generateAvatar = function () {
        if (Number.isInteger(this.userId) && typeof (this.name) == 'undefined') {
            //this.name.toString() == ''
            return this.httpProvider.apiUrl + '/user/' + this.userId.toString() + '/avatar';
        }
        else {
            this.name = this.name.replace(' ', '_');
            return this.httpProvider.apiUrl + '/user/' + this.name.toString() + '/avatar';
        }
        // if (this.userId){
        //   if (this.userId.toString() == '0'){
        //     var url = this.httpProvider.apiUrl+'/user/1/avatar/';
        //   }else{
        //     var url = this.httpProvider.apiUrl+'/user/'+this.userId.toString()+'/avatar/';
        //   }
        //   return url;
        // }
    };
    AvatarComponent.prototype.ngAfterContentInit = function () {
        this.avatar = this.generateAvatar();
    };
    return AvatarComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('user'),
    __metadata("design:type", Object)
], AvatarComponent.prototype, "userId", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('name'),
    __metadata("design:type", Object)
], AvatarComponent.prototype, "name", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('status'),
    __metadata("design:type", Object)
], AvatarComponent.prototype, "status", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('event'),
    __metadata("design:type", Object)
], AvatarComponent.prototype, "eventId", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('showName'),
    __metadata("design:type", Boolean)
], AvatarComponent.prototype, "showName", void 0);
AvatarComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'avatar',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/avatar/avatar.html"*/'<ion-avatar item-start>\n  <img [src]="avatar">\n  <ion-badge item_start *ngIf="status == 1" text-center color="secondary" round class="sign-badge">Accepted</ion-badge>\n  <ion-badge item_start *ngIf="status == 2" text-center color="secondary" round class="sign-badge">Tenative</ion-badge>\n  <ion-badge item_start *ngIf="status == 3" text-center color="pink" round class="pink-badge">Declined</ion-badge>\n  <!-- <span *ngIf="showName">{{ name.replace(\'_\', \' \') }}</span> -->\n</ion-avatar>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/avatar/avatar.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_1__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_http_http__["a" /* HttpProvider */]])
], AvatarComponent);

//# sourceMappingURL=avatar.js.map

/***/ }),

/***/ 868:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OptionsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the OptionsComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var OptionsComponent = (function () {
    function OptionsComponent() {
        this.hasShare = false;
        // console.log('Hello OptionsComponent Component');
    }
    return OptionsComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('event'),
    __metadata("design:type", Object)
], OptionsComponent.prototype, "eventId", void 0);
OptionsComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'options',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/options/options.html"*/'<ion-item no-lines text-center class="margin-item">\n  <button class="mini-btn" [ngClass]="hasOptions ? \'bright-color\' : \'disabled\'">\n    <ion-icon name="star" class="share-icon"></ion-icon>\n  </button>\n  <h6 class="mini-text">Options</h6>\n</ion-item>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/options/options.html"*/
    }),
    __metadata("design:paramtypes", [])
], OptionsComponent);

//# sourceMappingURL=options.js.map

/***/ }),

/***/ 869:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BroadcastCountDownComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the BroadcastCountDownComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var BroadcastCountDownComponent = (function () {
    function BroadcastCountDownComponent() {
        this.progress = 100;
        this.trackColor = '#00bbd3';
        this.placeholderColor = '#d6d8db';
        this.state = 1;
        this.max = '100';
        // console.log('Hello BroadcastCountDownComponent Component');
        this.width = 250;
    }
    BroadcastCountDownComponent.prototype.ngAfterContentInit = function () {
        //console.log('ngAfterContentInit '+this.progress);
        this.backgroundUrl = "url('assets/images/heart_active.png')";
        // if(this.state == 1){
        //   this.backgroundUrl="../../assets/images/heart_active.png";
        // }else{
        //   this.backgroundUrl="../../assets/images/heart_disable.png";
        // }
        this.imageWidth = this.width;
    };
    BroadcastCountDownComponent.prototype.progressValueChanged = function () {
        //console.log('progress value changed called! '+this.progress);
        // if(this.progress > 101)
        //   {
        //     // this.backgroundUrl="../../assets/images/heart_broadcast.png";
        //     // this.imageWidth = this.width + 2;
        //   }
        // if (this.progress < 0.25*this.max) {
        //   // red color
        //   this.backgroundUrl="url('../../assets/images/heart_broadcast.png')";
        // } else if (this.progress < 0.5*this.max) {
        //   // amber color
        //   this.backgroundUrl="url('../../assets/images/heart_active.png')";
        // } else {
        //   // normal color
        //   this.backgroundUrl="url('../../assets/images/heart_disable.png')";
        // }
        //this.backgroundUrl="url(../../assets/images/heart_active.png)";
    };
    return BroadcastCountDownComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('progress'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "progress", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('width'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "width", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('track-color'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "trackColor", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('placeholder-color'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "placeholderColor", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('state'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "state", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('max'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "max", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('backgroundUrl'),
    __metadata("design:type", Object)
], BroadcastCountDownComponent.prototype, "backgroundUrl", void 0);
BroadcastCountDownComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'broadcast-count-down',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/broadcast-count-down/broadcast-count-down.html"*/'<!-- Generated template for the BroadcastCountDownComponent component -->\n<div class="outerDiv">\n  <div class="overlayDiv"></div>\n  <round-progress class="round-progress child" [style.background]="backgroundUrl" [style.background-size]=" imageWidth + \'px \' + imageWidth + \'px\'"\n    [stroke]="width/2<125 ? 7 : 15" [current]="progress" [radius]="width/2" [max]="max" [color]="trackColor" [background]="placeholderColor"\n    (onRender)="progressValueChanged()">\n  </round-progress>\n</div>'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/broadcast-count-down/broadcast-count-down.html"*/
    }),
    __metadata("design:paramtypes", [])
], BroadcastCountDownComponent);

//# sourceMappingURL=broadcast-count-down.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyAccountPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_details_order_details__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_storage_storage__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__ = __webpack_require__(120);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyAccountPage = (function () {
    function MyAccountPage(navCtrl, navParams, storageProvider, httpProvider, camera, loadingCtrl, iab) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storageProvider = storageProvider;
        this.httpProvider = httpProvider;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.iab = iab;
        this.selected_tab = 0;
        this.location = '';
        this.name = '';
        this.email = '';
        this.birthday = '';
        this.phone = 0;
        this.avatar = '';
        this.notifications_push = false;
        this.notifications_email = false;
        this.password = '';
        this.confirm_password = '';
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: 0
        };
        var loading = this.createLoading();
        loading.present();
        storageProvider.getProfile().then(function (profile) {
            _this.user = profile;
            console.log(_this.user);
            _this.avatar = _this.httpProvider.apiUrl + '/user/' + _this.user.personal.id + '/avatar';
            var dob = new Date(_this.user.personal.dob * 1000);
            _this.birthday = dob.getMonth() + '/' + dob.getDate() + '/' + dob.getFullYear();
            _this.name = _this.user.personal.name.full;
            _this.email = _this.user.personal.email;
            _this.location = _this.user.personal.location.city + ', ' + _this.user.personal.location.state;
            _this.phone = _this.user.personal.phone;
            _this.notifications_push = _this.user.personal.privacy.notifications.push;
            _this.notifications_email = _this.user.personal.privacy.notifications.email;
            var user_id = _this.user.personal.id;
            _this.httpProvider.getUserOrders(user_id).then(function (res) {
                _this.orders = res;
                if (_this.navParams.get('order')) {
                    _this.selected_tab = 1;
                }
                // this.httpProvider.getPaymentInfo().then(
                //   (card) => {
                //     this.card = card;
                //     loading.dismiss();
                //   },
                //   (err) => {
                //     loading.dismiss();
                //   }
                // );
                loading.dismiss();
            }, function (err) {
                console.error(err);
                loading.dismiss();
            });
        }, function (err) {
            console.error(err);
            loading.dismiss();
        });
    }
    MyAccountPage.prototype.gotoOrder = function (order) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__order_details_order_details__["a" /* OrderDetailsPage */], {
            order: order
        });
    };
    MyAccountPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    MyAccountPage.prototype.list_tabs = function (event) {
        console.log(event);
        this.selected_tab = event;
    };
    MyAccountPage.prototype.updateAvatar = function () {
        var _this = this;
        var image = '';
        var self = this;
        this.camera.getPicture(this.options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            image = base64Image;
            var loading = _this.createLoading();
            loading.present();
            self.httpProvider.updateAvatar(image).then(function (status) {
                self.avatar = image;
                loading.dismiss();
            }, function (err) {
                loading.dismiss();
                console.error(err);
            });
        }, function (err) {
            // Handle error
            console.error(err);
        });
    };
    MyAccountPage.prototype.updateProfile = function () {
        var loading = this.createLoading();
        loading.present();
        var data = {
            push_notifications: this.notifications_push,
            email_notifications: this.notifications_email,
            phone: this.phone
        };
        var self = this;
        // self.httpProvider.updateAccount(data);
        self.httpProvider.updateAccount(data).then(function (user) {
            self.storageProvider.setProfile(user).then(function (res) { });
            self.user = user;
            if (self.password != '' && self.password == self.confirm_password) {
                //update password to firebase
            }
            loading.dismiss();
        }, function (err) {
            console.error(err);
        });
    };
    MyAccountPage.prototype.storePaymentMethod = function () {
    };
    MyAccountPage.prototype.termsOfService = function () {
        var browser = this.iab.create('https://webwedmobile.com/legal');
    };
    return MyAccountPage;
}());
MyAccountPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-my-account',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/my-account/my-account.html"*/'<ion-header>\n  <main-navigation title="My Account" [tabs]="[\'profile\',\'orders\']" (selected-tab)="list_tabs($event)"></main-navigation>\n</ion-header>\n\n<ion-content id="page4">\n  <div class="details-content">\n    <div [ngSwitch]="selected_tab">\n      <div *ngSwitchCase="0">\n\n        <ion-list id="myAccount-list5">\n          <ion-item text-center>\n            <ion-avatar (click)="updateAvatar()" padding class="center-avatar" item-start>\n              <img class="large-avatar" [src]="avatar">\n            </ion-avatar>\n          </ion-item>\n          <ion-item-divider class="section-header-text" color="light" id="myAccount-list-item-divider9">\n            GENERAL\n          </ion-item-divider>\n          <ion-item id="myAccount-input8">\n            <ion-label color="dark">\n              City and State\n            </ion-label>\n            <ion-input readonly text-right type="text" class="right-g-text" [(ngModel)]="location" placeholder=""></ion-input>\n          </ion-item>\n          <ion-item id="myAccount-input9">\n            <ion-label color="dark">\n              Name\n            </ion-label>\n            <ion-input readonly text-right type="text" class="right-g-text" [(ngModel)]="name" placeholder=""></ion-input>\n          </ion-item>\n          <ion-item id="myAccount-input10">\n            <ion-label color="dark">\n              Email Address\n            </ion-label>\n            <ion-input readonly text-right type="text" class="right-g-text" [(ngModel)]="email" placeholder=""></ion-input>\n          </ion-item>\n          <ion-item id="myAccount-input11">\n            <ion-label color="dark">\n              Birthday\n            </ion-label>\n            <ion-input readonly text-right type="text" class="right-g-text" [(ngModel)]="birthday" placeholder=""></ion-input>\n          </ion-item>\n          <ion-item id="myAccount-input12">\n            <ion-label color="dark">\n              Phone\n            </ion-label>\n            <ion-input text-right type="text" class="right-g-text" [(ngModel)]="phone" placeholder=""></ion-input>\n          </ion-item>\n          <!-- <ion-item-divider  class="section-header-text" color="light" id="myAccount-list-item-divider10">\n            PRIVACY\n          </ion-item-divider>\n          <ion-item id="myAccount-toggle3">\n            <ion-label color="dark">\n              Push notifications\n            </ion-label>\n            <ion-toggle color="calm" mode="ios" [(ngModel)]="notifications_push" checked="true"></ion-toggle>\n          </ion-item>\n          <ion-item id="myAccount-toggle4">\n            <ion-label color="dark">\n              Subscribe to Email Notifications\n            </ion-label>\n            <ion-toggle color="calm" mode="ios" [(ngModel)]="notifications_email" checked="true"></ion-toggle>\n          </ion-item> -->\n          <ion-item-divider  class="section-header-text" color="light" id="myAccount-list-item-divider11">\n            SECURITY\n          </ion-item-divider>\n          <ion-item id="myAccount-input14">\n            <ion-label color="dark">\n              Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="password" text-right placeholder=""></ion-input>\n          </ion-item>\n          <ion-item id="myAccount-input15">\n            <ion-label color="dark">\n              Confirm Password\n            </ion-label>\n            <ion-input type="password" [(ngModel)]="confirm_password" text-right placeholder=""></ion-input>\n          </ion-item>\n        </ion-list>\n        <p padding text-center>\n            Changes made to your account could impact your events. Read <button ion-button clear small class="terms-btn">Terms of Service</button> before updating your account information.\n        </p>\n      </div>\n      <div *ngSwitchCase="2" padding>\n        <ion-list class="payment-section">\n          <ion-item class="payment-pic" no-lines text-center text-wrap>\n              <ion-icon name="card"></ion-icon>\n              <h1 class="banner-head">Enter your card details for payment.</h1>\n          </ion-item>\n          <ion-list class="payment-text-list" no-lines>\n            <ion-item no-lines class="border-btm">\n                <ion-label class="left-g-text" text-left item-start>Card number</ion-label>\n                <!-- <ion-label class="right-g-text" text-right item-end></ion-label> -->\n                <ion-input type="text" [(ngModel)]="card.number" text-right></ion-input>\n            </ion-item>\n            <ion-item no-lines class="border-btm">\n                <ion-label class="left-g-text" text-left item-start>Name on Card</ion-label>\n                <!-- <ion-label class="right-g-text" text-right item-end></ion-label> -->\n                <ion-input type="text" [(ngModel)]="card.name" text-right></ion-input>\n            </ion-item>\n            <ion-item no-lines class="border-btm">\n                <ion-label class="left-g-text" text-left item-start>Expires</ion-label>\n                <ion-input type="date" [(ngModel)]="card.expires" text-right></ion-input>\n            </ion-item>\n            <ion-item no-lines class="border-btm">\n                <ion-label class="left-g-text" text-left item-start>CCV</ion-label>\n                <ion-input type="date" [(ngModel)]="card.ccv" text-right></ion-input>\n            </ion-item>\n            <ion-item no-lines class="border-btm">\n                <ion-label class="left-g-text text" text-left item-start>Zip</ion-label>\n                <ion-input type="string" [(ngModel)]="card.zip" text-right></ion-input>\n            </ion-item>\n            <ion-item no-lines text-center text-wrap>\n              <ion-label>Payment are powered by Stripe. Read the Full <button ion-button clear small (click)="termsOfService()" class="terms-btn">Terms of Service</button> before adding payment.</ion-label>\n            </ion-item>\n          </ion-list>\n\n       </ion-list>\n      </div>\n      <div *ngSwitchCase="1">\n        <ion-list no-lines>\n          <ion-item *ngFor="let order of orders;" (click)="gotoOrder(order)"  class="order">\n            <ion-grid>\n              <ion-row>\n                <ion-col col-9>\n                  <h2 class="order-header-color" color="primary">{{order.title}}</h2>\n                  <p>{{order.date * 1000 | date}}</p>\n                </ion-col>\n                <ion-col col-3 text-right>\n                  <strong text-right>${{order.price}}</strong>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-item>\n        </ion-list>\n      </div>\n    </div>\n  </div>\n</ion-content>\n<ion-footer *ngIf="selected_tab == 0" class="footer-sticky" text-center>\n  <button (click)="updateProfile()" full block color="light-blue" ion-button large>Update and Save</button>\n</ion-footer>\n<!-- <ion-footer *ngIf="selected_tab == 1" class="footer-sticky" text-center>\n  <button disabled="disabled" (click)="storePaymentMethod()" full block color="light-blue" ion-button large>Store Payment Method</button>\n</ion-footer> -->\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/my-account/my-account.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_4__providers_storage_storage__["b" /* StorageProvider */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__providers_storage_storage__["b" /* StorageProvider */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
], MyAccountPage);

//# sourceMappingURL=my-account.js.map

/***/ }),

/***/ 870:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IonWalkthrough; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var IonWalkthrough = (function () {
    function IonWalkthrough() {
    }
    IonWalkthrough.prototype.ngOnInit = function () {
        if (this.options === undefined || this.options === null) {
            console.error('[IonWalkthrough] options are not defined.');
            return;
        }
    };
    IonWalkthrough.prototype.isTopLayout = function () {
        return this.options.layout && this.options.layout.position === 'top';
    };
    IonWalkthrough.prototype.getDeviceType = function () {
        return this.options.layout && this.options.layout.deviceType === 'iphone' ? 'iphone' : 'android';
    };
    IonWalkthrough.prototype.getDeviceColor = function () {
        if (this.getDeviceType() === 'iphone') {
            if (this.options.layout && this.options.layout.deviceColor === 'silver') {
                return 'silver';
            }
            else if (this.options.layout && this.options.layout.deviceColor === 'gold') {
                return 'gold';
            }
            else {
                return 'spacegrey';
            }
        }
        else {
            return this.options.layout && this.options.layout.deviceColor === 'white' ? 'white' : 'black';
        }
    };
    return IonWalkthrough;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], IonWalkthrough.prototype, "options", void 0);
IonWalkthrough = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'ion-walkthrough',
        template: "\n      <div\n       class=\"ion-walkthrough-slide-container {{getDeviceType()}}-layout {{getDeviceColor()}}\"\n       [ngClass]=\"{'top-layout': isTopLayout()}\"\n       [ngStyle]=\"options.styles && options.styles.background && {'background': options.styles.background}\">\n        <ion-toolbar transparent class=\"ion-walkthrough-slide-buttons\" [ngStyle]=\"{'order': isTopLayout() ? 3 : 1 }\">\n          <ion-buttons left>\n            <button \n              ion-button\n              clear \n              *ngIf=\"options.buttons && options.buttons.left\"\n              (click)=\"options.buttons.left.onClick()\"\n              [ngStyle]=\"options.buttons.left.textColor && {'color': options.buttons.left.textColor}\"\n              class=\"ion-walkthrough-slide-nav-button ion-walkthrough-slide-nav-button-left\"\n            >{{options.buttons.left.text}}</button>\n          </ion-buttons>\n          <ion-buttons right>\n            <button \n              ion-button\n              clear \n              *ngIf=\"options.buttons && options.buttons.right\"\n              (click)=\"options.buttons.right.onClick()\"\n              [ngStyle]=\"options.buttons.right.textColor && {'color': options.buttons.right.textColor}\"\n              class=\"ion-walkthrough-slide-nav-button ion-walkthrough-slide-nav-button-right\"\n            >{{options.buttons.right.text}}</button>\n           </ion-buttons>\n        </ion-toolbar>\n        <div [ngStyle]=\"{'order': 2}\">\n          <h1\n            class=\"ion-walkthrough-slide-title\"\n            [innerHTML]=\"options.title\"\n            [ngStyle]=\"options.styles && options.styles.titleColor && {'color': options.styles.titleColor}\">\n          </h1>\n          <p \n            class=\"ion-walkthrough-slide-description\" \n            [innerHTML]=\"options.description\" \n            [ngStyle]=\"options.styles && options.styles.descriptionColor && {'color': options.styles.descriptionColor}\">\n          </p>\n        </div>\n        <div class=\"ion-walkthrough-slide-device-section\" [ngStyle]=\"{'order': isTopLayout() ? 1 : 3 }\">\n          <div class=\"ion-walkthrough-slide-image\" \n          [ngStyle]=\"{\n            'background-image': 'url(' + options.image + ')'\n          }\">\n          </div>\n        </div>\n      </div>\n  ",
        styles: ["\n    .ion-walkthrough-slide-container {\n      display: flex;\n      flex-direction: column;\n      justify-content: space-between; \n      height: 100%;\n    }\n    .ion-walkthrough-slide-container .ion-walkthrough-slide-nav-button {\n      font-size: 1.4rem;\n      font-weight: 500;\n      padding: 0 8px;\n    }\n    .ion-walkthrough-slide-container h1 {\n      margin: .4rem;\n    }\n    .ion-walkthrough-slide-container div p.ion-walkthrough-slide-description {\n      padding: 0 40px;\n      font-size: 14px;\n      line-height: 1.5;\n    }\n    .ion-walkthrough-slide-container div.ion-walkthrough-slide-device-section {\n      overflow: hidden;\n      background-repeat: no-repeat;\n      background-position: center top;\n    }\n    .ion-walkthrough-slide-container.top-layout div.ion-walkthrough-slide-device-section {\n      background-position: center bottom;\n    }\n    .ion-walkthrough-slide-container div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n      margin: 0 auto;\n      height: 100%;\n      background-repeat: no-repeat;\n      background-size: cover;\n      background-position: center top;\n    }\n    .ion-walkthrough-slide-container.top-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n      background-position: center bottom;\n    }\n\n    .ion-walkthrough-slide-container.android-layout.black div.ion-walkthrough-slide-device-section {\n      background-image: url('./assets/ion-walkthrough/android-black.png');\n    }\n    .ion-walkthrough-slide-container.android-layout.white div.ion-walkthrough-slide-device-section {\n      background-image: url('./assets/ion-walkthrough/android-white.png');\n    }\n    .ion-walkthrough-slide-container.iphone-layout.silver div.ion-walkthrough-slide-device-section {\n      background-image: url('./assets/ion-walkthrough/iphone-silver.png');\n    }\n    .ion-walkthrough-slide-container.iphone-layout.gold div.ion-walkthrough-slide-device-section {\n      background-image: url('./assets/ion-walkthrough/iphone-gold.png');\n    }\n    .ion-walkthrough-slide-container.iphone-layout.spacegrey div.ion-walkthrough-slide-device-section {\n      background-image: url('./assets/ion-walkthrough/iphone-spacegrey.png');\n    }\n\n\n    @media (max-height: 568px) {\n      .ion-walkthrough-slide-container div.ion-walkthrough-slide-device-section  {\n        height: 340px;\n        background-size: 275px 540px;\n      }\n      .ion-walkthrough-slide-container.android-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        width: 238px;\n        margin-top: 52px;\n      }\n      .ion-walkthrough-slide-container.iphone-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        width: 235px;\n        margin-top: 65px;\n      }\n      .ion-walkthrough-slide-container.top-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        margin-top: -67px;\n      }\n    }\n\n    @media (min-height: 569px) and (max-height: 667px) {\n      .ion-walkthrough-slide-container div.ion-walkthrough-slide-device-section  {\n        height: 460px;\n        background-size: 300px 590px;\n      }\n      .ion-walkthrough-slide-container.android-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        width: 260px;\n        margin-top: 56px;\n      }\n      .ion-walkthrough-slide-container.iphone-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        width: 256px;\n        margin-top: 70px;\n      }\n      .ion-walkthrough-slide-container.top-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        margin-top: -72px;\n      }\n    }\n\n    @media (min-height: 668px) {\n      .ion-walkthrough-slide-container div.ion-walkthrough-slide-device-section  {\n        height: 520px;\n        background-size: 330px 648px;\n      }\n      .ion-walkthrough-slide-container.android-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        width: 282px;\n        margin-top: 62px;\n      }\n      .ion-walkthrough-slide-container.iphone-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        width: 288px;\n        margin-top: 78px;\n      }\n      .ion-walkthrough-slide-container.top-layout div.ion-walkthrough-slide-device-section .ion-walkthrough-slide-image {\n        margin-top: -80px;\n      }\n    }\n  "]
    }),
    __metadata("design:paramtypes", [])
], IonWalkthrough);

//# sourceMappingURL=ion-walkthrough.js.map

/***/ }),

/***/ 871:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterPageModule = (function () {
    function RegisterPageModule() {
    }
    return RegisterPageModule;
}());
RegisterPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
        ],
    })
], RegisterPageModule);

// WEBPACK FOOTER //
// ./src/pages/register/register.module.ts
//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 872:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_c_wed_card_c_wed_card__ = __webpack_require__(873);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_c_guest_card_c_guest_card__ = __webpack_require__(875);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_components_module__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var DashboardPageModule = (function () {
    function DashboardPageModule() {
    }
    return DashboardPageModule;
}());
DashboardPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */],
            __WEBPACK_IMPORTED_MODULE_3__components_c_wed_card_c_wed_card__["a" /* CWedCardComponent */],
            __WEBPACK_IMPORTED_MODULE_4__components_c_guest_card_c_guest_card__["a" /* CGuestCardComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */]), __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_6__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], DashboardPageModule);

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CWedCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_archive_archive__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_wed_details_wed_details__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CWedCardComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var CWedCardComponent = (function () {
    function CWedCardComponent(navCtrl, loadingCtrl, httpProvider, modalCtrl) {
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.httpProvider = httpProvider;
        this.modalCtrl = modalCtrl;
        this.type = 'invite';
        this.gotoWedDetails = function () {
            var _this = this;
            if (this.event_status == 2) {
                return;
                // let modal = this.modalCtrl.create(AttendEventModalPage, {
                //   event: this.wedCardEvent.id
                // });
                // let me = this;
                // modal.onDidDismiss(data => {
                //   console.log(data);
                // });
                // modal.present();
            }
            var loading = this.createLoading();
            loading.present();
            this.httpProvider.getEventDetailsById(this.wedCardEvent.id).then(function (res) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_wed_details_wed_details__["a" /* WedDetailsPage */], {
                    event: res,
                    user: _this.user
                });
                loading.dismiss();
            }, function (err) {
                console.error(err);
            });
        };
    }
    CWedCardComponent.prototype.ngAfterContentInit = function () {
        this.cardImage = this.wedCardImage;
        //this.cardHeader = this.wedCardHeader;
        this.cardSubHeader = this.wedCardSubHeader;
        this.cardParticipants = this.wedCardParticipants;
        // console.log('participants');
        // console.log(this.wedCardEvent.participants);
        var status = this.wedCardEvent.event_status;
        this.event_status = status;
        if ((this.wedCardEvent != undefined) &&
            (this.wedCardEvent.photo != undefined) &&
            (this.wedCardEvent.photo.length > 1)) {
            //TODO: convert cover_photo to image-url to show here
            this.cardImage = this.wedCardEvent.photo;
        }
        else {
            this.cardImage = this.wedCardImage;
        }
        if ((this.wedCardEvent != undefined) &&
            (this.wedCardEvent.title != undefined) &&
            (this.wedCardEvent.title.length > 0)) {
            this.cardHeader = this.wedCardEvent.title;
        }
        else {
            this.cardHeader = "";
        }
        if ((this.wedCardEvent != undefined) &&
            (this.wedCardEvent.date != undefined)) {
            this.cardSubHeader = this.wedCardEvent.date;
        }
        else {
            this.cardSubHeader = "";
        }
        if ((this.wedCardEvent.participants != undefined) &&
            (this.wedCardEvent.participants.length > 0)) {
            this.cardParticipants = [];
            for (var aa = 0; aa < this.wedCardEvent.participants.length; aa++) {
                var aParticipant = this.wedCardEvent.participants[aa];
                if ((aParticipant != undefined) &&
                    (aParticipant.personal != undefined)) {
                    this.cardParticipants.push(aParticipant);
                }
            }
            // console.log(this.cardParticipants);
        }
        else {
            this.cardParticipants = [];
        }
        if (this.wedCardEvent.type != undefined) {
            this.type = this.wedCardEvent.type;
        }
        //this.cardImage = this.wedCardEvent.details.title;
        //this.cardSubHeader = this.wedCardEvent.;
        //this.cardParticipants = this.wedCardEvent.;
    };
    CWedCardComponent.prototype.play = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__pages_archive_archive__["a" /* ArchivePage */], {
            event: this.wedCardEvent.id
        });
    };
    CWedCardComponent.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return CWedCardComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cWedCardImage'),
    __metadata("design:type", Object)
], CWedCardComponent.prototype, "wedCardImage", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cWedCardHeader'),
    __metadata("design:type", Object)
], CWedCardComponent.prototype, "wedCardHeader", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cWedCardSubHeader'),
    __metadata("design:type", Object)
], CWedCardComponent.prototype, "wedCardSubHeader", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cWedCardParticipants'),
    __metadata("design:type", Object)
], CWedCardComponent.prototype, "wedCardParticipants", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cWedCardEvent'),
    __metadata("design:type", Object)
], CWedCardComponent.prototype, "wedCardEvent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('user'),
    __metadata("design:type", Number)
], CWedCardComponent.prototype, "user", void 0);
CWedCardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'c-wed-card',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/c-wed-card/c-wed-card.html"*/'<!-- Generated template for the CWedCardComponent component -->\n<!-- <ion-item-sliding #item>\n  <ion-item class="card-main-item"> -->\n<ion-card *ngIf="type==\'event\'">\n  <img [src]="cardImage" (click)="gotoWedDetails();" style="min-height:150px;"/>\n\n  <button ion-button large color="primary" class="actv-btn btn-pad play-btn" *ngIf="event_status==2" (click)="play()">\n    <ion-icon name="play" class=""></ion-icon>\n  </button>\n  <ion-card-content class="card-text-content">\n    <ion-card-title class="card-color-title">\n      {{cardHeader}}\n    </ion-card-title>\n    <p class="card-subtitle-text">\n      {{cardSubHeader*1000 | date:"MMM dd yyyy \'at\' hh:mm a"}}\n    </p>\n\n  </ion-card-content>\n  <ion-row>\n    <ion-col>\n      <ion-item class="user-avatar-list">\n        <avatar item-start *ngFor="let participant of cardParticipants" [user]="participant.personal.id" [name]="participant.personal.name.full" [showName]="true"></avatar>\n        <!-- <ion-avatar item-start *ngFor="let participant of cardParticipants">\n          <img src={{participant.personal.avatar}}>\n        </ion-avatar> -->\n      </ion-item>\n    </ion-col>\n  </ion-row>\n</ion-card>\n<!-- </ion-item>\n    <ion-item-options side="right" class="right-slide-items" no-lines text-center>\n      <div class="slide-item-details">\n        <h6>Respond to this <br />invitation with</h6>\n        <button ion-button class="actv-btn btn-pad">Accepted</button>\n        <button ion-button class="outln-btn btn-pad">tenative</button>\n        <button ion-button class="outln-btn">Unable to attend</button>\n      </div>\n    </ion-item-options>\n</ion-item-sliding> -->\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/c-wed-card/c-wed-card.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["j" /* ModalController */]])
], CWedCardComponent);

//# sourceMappingURL=c-wed-card.js.map

/***/ }),

/***/ 875:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CGuestCardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_attend_event_modal_attend_event_modal__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_broadcast_broadcast__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_archive_archive__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(487);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the CGuestCardComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var CGuestCardComponent = (function () {
    function CGuestCardComponent(httpProvider, loadingCtrl, navCtrl, modalCtrl, socialSharing) {
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.socialSharing = socialSharing;
        this.type = 'invite';
        this.status = {
            title: '',
            color: ''
        };
        this.watchEvent = function () {
            var _this = this;
            var event = this.wedCardEvent;
            var guest = true;
            for (var i = 0; i < event.participants.length; i++) {
                var participant = event.participants[i];
                console.log(participant);
                if (this.user == participant.personal.id) {
                    guest = false;
                }
            }
            console.log(event.participants);
            console.log(this.user);
            var status = event.event_status;
            this.event_status = status;
            this.hasResponded = event.hasResponded;
            if (this.event_status == 2) {
                this.play();
                return;
            }
            // this.navCtrl.setRoot(BroadcastPage, {
            //   guest: guest,
            //   event: event.id
            // });
            //
            // return;
            if (guest) {
                this.httpProvider.isActive(event.id).then(function (res) {
                    if (res) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_broadcast_broadcast__["a" /* BroadcastPage */], {
                            guest: guest,
                            event: event.id,
                            user: _this.user
                        });
                    }
                    else {
                        var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_attend_event_modal_attend_event_modal__["a" /* AttendEventModalPage */], {
                            event: event.id
                        });
                        var me = _this;
                        modal.onDidDismiss(function (data) {
                            console.log(data);
                        });
                        modal.present();
                    }
                }, function (err) {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__pages_attend_event_modal_attend_event_modal__["a" /* AttendEventModalPage */], {
                        event: event.id
                    });
                    var me = _this;
                    modal.onDidDismiss(function (data) {
                        console.log(data);
                    });
                    modal.present();
                });
            }
            else {
                this.httpProvider.getEventSessionDetails(event.id, this.user).then(function (res) {
                    console.log(res);
                    //this.sessionId = res.opentok.session_id;
                    // this.sessionId =
                    //this.token = res.opentok.token;
                    window.cordova.exec(null, null, 'OpentokActivator', 'activateNow', ['46014622', res.opentok.session_id, res.opentok.token]);
                }, function (err) {
                    console.error(err);
                });
            }
        };
        // console.log('Hello CGuestCardComponent Component');
    }
    CGuestCardComponent.prototype.ngAfterContentInit = function () {
        this.cardImage = this.wedCardEvent.photo;
        this.cardHeader = this.wedCardEvent.title;
        this.cardSubHeader = this.wedCardEvent.date;
        if (this.wedCardEvent.type != undefined) {
            this.type = this.wedCardEvent.type;
        }
        if (this.wedCardEvent.status != undefined) {
            this.status = this.wedCardEvent.status;
        }
        var status = this.wedCardEvent.event_status;
        this.event_status = status;
        this.hasResponded = this.wedCardEvent.hasResponded;
    };
    CGuestCardComponent.prototype.respondToInvite = function (answer) {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.httpProvider.respondToInvite(answer, this.wedCardEvent.id).then(function (res) {
            _this.wedCardEvent = res;
            loading.dismiss();
        }, function (err) {
        });
    };
    CGuestCardComponent.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    CGuestCardComponent.prototype.play = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_archive_archive__["a" /* ArchivePage */], {
            event: this.wedCardEvent.id
        });
    };
    CGuestCardComponent.prototype.share = function () {
        var message = '';
        var url = '';
        if (this.wedCardEvent.id) {
            message = 'Share event with a friend.';
            url = "http://webwedmobile.net/api/event/share/" + this.wedCardEvent.id;
        }
        else {
            message = 'Share user with a friend.';
            url = "http://webwedmobile.net/api/user/share/" + this.wedCardEvent.id;
        }
        this.socialSharing.share(message, 'Share', null, url).then(function () {
            console.log('shared');
        }).catch(function () {
            // Error!
        });
    };
    return CGuestCardComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('cWedCardEvent'),
    __metadata("design:type", Object)
], CGuestCardComponent.prototype, "wedCardEvent", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('user'),
    __metadata("design:type", Number)
], CGuestCardComponent.prototype, "user", void 0);
CGuestCardComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'c-guest-card',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/c-guest-card/c-guest-card.html"*/'<!-- Generated template for the CGuestCardComponent component -->\n<ion-item-sliding #item *ngIf="type==\'invite\'">\n  <ion-item class="card-main-item">\n    <ion-card>\n      <img [src]="cardImage" (click)="watchEvent()" />\n      <button ion-button large color="primary" class="actv-btn btn-pad play-btn" *ngIf="event_status==2" (click)="play()">\n        <ion-icon name="play" class=""></ion-icon>\n      </button>\n      <ion-card-content class="guest-card-text-content">\n        <ion-card-title class="guest-card-color-title">\n          {{cardHeader}}\n        </ion-card-title>\n        <p class="guest-card-subtitle-text">\n          {{cardSubHeader*1000 | date:"MMM dd yyyy \'at\' hh:mm a"}}\n        </p>\n      </ion-card-content>\n      <ion-item class="bottom-social-section">\n        <!-- <button ion-button color="light" round item-start ([ngClass])="status.color">{{status.title}}</button> -->\n        <ion-badge item-start text-center round class="sign-badge {{status.color}}-badge" ([ngClass])="status.color">{{status.title}}</ion-badge>\n        <ion-row item-end>\n          <ion-col>\n            <!-- <button ion-button color="danger" item-end class="tag-btn"><ion-icon name="pricetags" class="material-icons"></ion-icon></button> -->\n            <button ion-button (click)="share()" color="primary" class="share-btn"><ion-icon name="ios-redo-outline"></ion-icon></button>\n            <!-- <share [event]="1"></share> -->\n          </ion-col>\n        </ion-row>\n      </ion-item>\n    </ion-card>\n  </ion-item>\n  <ion-item-options side="right" class="right-slide-items" no-lines text-center>\n    <div class="slide-item-details">\n      <h6>Respond to this <br />invitation with</h6>\n\n      <p *ngIf="hasResponded != 0"><br/>You have already responded to this invite.</p>\n      <button (click)="respondToInvite(1)" *ngIf="hasResponded == 0" ion-button class="outln-btn btn-pad">Accepted</button>\n      <!-- <button (click)="respondToInvite(2)" ion-button class="outln-btn btn-pad">Tenative</button> -->\n      <button (click)="respondToInvite(3)" *ngIf="hasResponded == 0" ion-button class="outln-btn">Unable to attend</button>\n    </div>\n  </ion-item-options>\n</ion-item-sliding>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/components/c-guest-card/c-guest-card.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */]])
], CGuestCardComponent);

//# sourceMappingURL=c-guest-card.js.map

/***/ }),

/***/ 876:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WedDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wed_details__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var WedDetailsPageModule = (function () {
    function WedDetailsPageModule() {
    }
    return WedDetailsPageModule;
}());
WedDetailsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__wed_details__["a" /* WedDetailsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__wed_details__["a" /* WedDetailsPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], WedDetailsPageModule);

//# sourceMappingURL=wed-details.module.js.map

/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateAnEventPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_an_event__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CreateAnEventPageModule = (function () {
    function CreateAnEventPageModule() {
    }
    return CreateAnEventPageModule;
}());
CreateAnEventPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__create_an_event__["a" /* CreateAnEventPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__create_an_event__["a" /* CreateAnEventPage */]),
        ],
    })
], CreateAnEventPageModule);

//# sourceMappingURL=create-an-event.module.js.map

/***/ }),

/***/ 88:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_auth_auth__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__register_register__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_keyboard__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_storage_storage__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, keyboard, auth, fbAuth, alertCtrl, loadingCtrl, platform, fb, storageProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.keyboard = keyboard;
        this.auth = auth;
        this.fbAuth = fbAuth;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.fb = fb;
        this.storageProvider = storageProvider;
        this.attempts = 0;
        if (this.navParams.get('eventId')) {
            this.eventId = this.navParams.get('eventId');
        }
    }
    LoginPage.prototype.ngOnInit = function () {
        var _this = this;
        this.form = new __WEBPACK_IMPORTED_MODULE_7__angular_forms__["b" /* FormGroup */]({
            email: new __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["g" /* Validators */].email]),
            password: new __WEBPACK_IMPORTED_MODULE_7__angular_forms__["a" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_7__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_7__angular_forms__["g" /* Validators */].minLength(6)]),
        });
        var loading = this.createLoading();
        loading.present();
        var authSubscrition = this.fbAuth.authState.subscribe(function (user) {
            authSubscrition.unsubscribe();
            if (user) {
                _this.continue(user, loading);
            }
            else {
                loading.dismiss();
            }
        }, function (e) {
            loading.dismiss();
        });
        this.storageProvider.setSeenWalkthrough(true).then(function (res) {
            console.log('Seen WalkthroughPage');
        }, function (err) {
        });
    };
    LoginPage.prototype.signInWithEmail = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.auth.signInWithEmail(this.form.controls.email.value, this.form.controls.password.value)
            .then(function (user) {
            _this.continue(user, loading);
        }, function (e) {
            loading.dismiss();
            console.log(e);
            _this.attempts += 1;
            if (_this.attempts >= 3 && e.code == 'auth/wrong-password') {
                _this.auth.forgotPassword(_this.form.controls.email.value).then(function (result) {
                    console.log(result);
                    var alert = _this.alertCtrl.create({
                        message: 'A password reset has been sent to your email address.',
                        buttons: [{
                                role: 'close',
                                text: 'Ok'
                            }]
                    });
                    alert.present();
                }, function (error) {
                    _this.handleError(error);
                });
                _this.attempts = 0;
                return;
            }
            if (e.code == 'auth/user-not-found') {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */], {
                    user: {
                        email: _this.form.controls.email.value,
                        password: _this.form.controls.password.value,
                        displayName: ''
                    }
                });
                _this.attempts = 0;
                return;
            }
            _this.handleError(e);
        });
    };
    LoginPage.prototype.signInWithFacebook = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.auth.signInWithFacebook().then(function (user) {
            _this.continue(user, loading);
        }, function (e) {
            loading.dismiss();
            _this.handleError(e);
        });
    };
    LoginPage.prototype.signInWithGoogle = function () {
        var _this = this;
        var loading = this.createLoading();
        loading.present();
        this.auth.signInWithGoogle().then(function (user) {
            console.log("THERE");
            _this.continue(user, loading);
        }, function (e) {
            loading.dismiss();
            _this.handleError(e);
        });
    };
    LoginPage.prototype.register = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__register_register__["a" /* RegisterPage */]);
    };
    LoginPage.prototype.handleError = function (error) {
        var alert = this.alertCtrl.create({
            message: error && error.message ? error.message : 'Error',
            buttons: [{
                    role: 'close',
                    text: 'Ok'
                }]
        });
        alert.present();
    };
    LoginPage.prototype.continue = function (user, loading) {
        var _this = this;
        user.getIdToken(true).then(function (accessToken) {
            _this.auth.getUserData(accessToken).then(function (userData) {
                console.log(userData);
                if (loading)
                    loading.dismiss();
                // TODO: this should be handled better
                /*if (!userData.personal.location) {
                  this.navCtrl.push(RegisterPage, {
                    user: user
                  });
                } else {*/
                // this.navCtrl.setRoot('home', {
                //   user: user
                // });
                _this.storageProvider.setProfile(userData).then(function (res) {
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__dashboard_dashboard__["a" /* DashboardPage */], {
                        user: user,
                        userData: userData,
                        event: _this.eventId
                    });
                });
                //}
            }, function (e) {
                if (loading)
                    loading.dismiss();
                console.log("ERROR", e);
            });
        }, function (e) {
            if (loading)
                loading.dismiss();
            console.log("ERROR", e);
        });
    };
    LoginPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["f" /* IonicPage */])({
        name: 'login'
    }),
    Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/login/login.html"*/'<ion-content class="main-bg" [formGroup]="form">\n  <div class="content-wrap" style="margin:0 auto;">\n    <div class="logo">\n      <img src="assets/images/WebWed-Logo.svg" style="max-width:150px;padding:20px" alt="logo">\n    </div>\n    <div class="register-msg">\n      Sign in to exciting account or <a tappable color="light-blue" style="color:#5aabd3"(click)="register()">register</a> for a new one\n    </div>\n    <ion-item no-lines [ngClass]="\'ww-input-item\'">\n      <ion-input placeholder="Email Address" mode="ios" formControlName="email"></ion-input>\n    </ion-item>\n    <ion-item no-lines [ngClass]="\'ww-input-item\'">\n      <ion-input placeholder="Password" type="password" mode="ios" formControlName="password"></ion-input>\n    </ion-item>\n    <button ion-button color="primary" mode="ios" [attr.disabled]="form.valid ? null : true" (click)="signInWithEmail()">Continue</button>\n\n    <div class="social-msg" color="primary">or use social login instead</div>\n\n    <button ion-button color="facebook" (click)="signInWithFacebook()" [ngClass]="\'social-btn\'" mode="ios" icon-start>\n      <ion-icon name="logo-facebook"></ion-icon>\n      Log in with Facebook\n    </button>\n    <button ion-button color="google" (click)="signInWithGoogle()" [ngClass]="\'social-btn\'" mode="ios" icon-start>\n      <ion-icon name="logo-google"></ion-icon>\n      Sign in with Google\n    </button>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/login/login.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_9__providers_storage_storage__["b" /* StorageProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_keyboard__["a" /* Keyboard */],
        __WEBPACK_IMPORTED_MODULE_1__providers_auth_auth__["a" /* AuthProvider */],
        __WEBPACK_IMPORTED_MODULE_0_angularfire2_auth__["a" /* AngularFireAuth */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["h" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__["a" /* Facebook */],
        __WEBPACK_IMPORTED_MODULE_9__providers_storage_storage__["b" /* StorageProvider */]])
], LoginPage);

// WEBPACK FOOTER //
// ./src/pages/login/login.ts
//# sourceMappingURL=login.js.map

/***/ }),

/***/ 881:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickContactPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pick_contact__ = __webpack_require__(188);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PickContactPageModule = (function () {
    function PickContactPageModule() {
    }
    return PickContactPageModule;
}());
PickContactPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__pick_contact__["a" /* PickContactPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pick_contact__["a" /* PickContactPage */]),
        ],
    })
], PickContactPageModule);

//# sourceMappingURL=pick-contact.module.js.map

/***/ }),

/***/ 882:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutocompletePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__autocomplete__ = __webpack_require__(484);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AutocompletePageModule = (function () {
    function AutocompletePageModule() {
    }
    return AutocompletePageModule;
}());
AutocompletePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__autocomplete__["a" /* AutocompletePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__autocomplete__["a" /* AutocompletePage */]),
        ],
    })
], AutocompletePageModule);

//# sourceMappingURL=autocomplete.module.js.map

/***/ }),

/***/ 883:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AttendEventModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__attend_event_modal__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var AttendEventModalPageModule = (function () {
    function AttendEventModalPageModule() {
    }
    return AttendEventModalPageModule;
}());
AttendEventModalPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__attend_event_modal__["a" /* AttendEventModalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__attend_event_modal__["a" /* AttendEventModalPage */]), __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], AttendEventModalPageModule);

//# sourceMappingURL=attend-event-modal.module.js.map

/***/ }),

/***/ 884:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddManualModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__add_manual_modal__ = __webpack_require__(189);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddManualModalPageModule = (function () {
    function AddManualModalPageModule() {
    }
    return AddManualModalPageModule;
}());
AddManualModalPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__add_manual_modal__["a" /* AddManualModalPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__add_manual_modal__["a" /* AddManualModalPage */]),
        ],
    })
], AddManualModalPageModule);

//# sourceMappingURL=add-manual-modal.module.js.map

/***/ }),

/***/ 885:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RolePopoverPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__role_popover__ = __webpack_require__(886);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RolePopoverPageModule = (function () {
    function RolePopoverPageModule() {
    }
    return RolePopoverPageModule;
}());
RolePopoverPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__role_popover__["a" /* RolePopoverPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__role_popover__["a" /* RolePopoverPage */]),
        ],
    })
], RolePopoverPageModule);

//# sourceMappingURL=role-popover.module.js.map

/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RolePopoverPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_http_http__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the RolePopoverPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RolePopoverPage = (function () {
    function RolePopoverPage(navCtrl, navParams, httpProvider, viewCtrl, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.httpProvider = httpProvider;
        this.viewCtrl = viewCtrl;
        this.loadingCtrl = loadingCtrl;
        this.roles = [];
        var loading = this.createLoading();
        loading.present();
        httpProvider.getEventConfig().then(function (res) {
            console.log(res);
            loading.dismiss();
            _this.config = res;
            _this.roles = _this.config.roles;
        }, function (err) {
        });
    }
    RolePopoverPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RolePopoverPage');
    };
    RolePopoverPage.prototype.dismiss = function (roleId) {
        this.viewCtrl.dismiss({
            role: roleId
        });
    };
    RolePopoverPage.prototype.createLoading = function () {
        var loading = this.loadingCtrl.create({
            content: 'Please wait ...'
        });
        return loading;
    };
    return RolePopoverPage;
}());
RolePopoverPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-role-popover',template:/*ion-inline-start:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/role-popover/role-popover.html"*/'<!--\n  Generated template for the RolePopoverPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>Select Role</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n\n<ion-content>\n  <ion-list>\n    <ion-item *ngFor="let role of roles; let i = index;" (click)="dismiss(role.id)">\n      <p>{{role.name}}</p>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/austinsweat/Desktop/Projects/webwed/WebWedMobile/App/src/pages/role-popover/role-popover.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_http_http__["a" /* HttpProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["q" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
], RolePopoverPage);

//# sourceMappingURL=role-popover.js.map

/***/ }),

/***/ 887:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BroadcastPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__broadcast__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var BroadcastPageModule = (function () {
    function BroadcastPageModule() {
    }
    return BroadcastPageModule;
}());
BroadcastPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__broadcast__["a" /* BroadcastPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__broadcast__["a" /* BroadcastPage */]),
            __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], BroadcastPageModule);

//# sourceMappingURL=broadcast.module.js.map

/***/ }),

/***/ 888:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ArchivePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__archive__ = __webpack_require__(200);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ArchivePageModule = (function () {
    function ArchivePageModule() {
    }
    return ArchivePageModule;
}());
ArchivePageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__archive__["a" /* ArchivePage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__archive__["a" /* ArchivePage */]),
        ],
    })
], ArchivePageModule);

//# sourceMappingURL=archive.module.js.map

/***/ }),

/***/ 889:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventCompletedPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__event_completed__ = __webpack_require__(194);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EventCompletedPageModule = (function () {
    function EventCompletedPageModule() {
    }
    return EventCompletedPageModule;
}());
EventCompletedPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__event_completed__["a" /* EventCompletedPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__event_completed__["a" /* EventCompletedPage */]),
        ],
    })
], EventCompletedPageModule);

//# sourceMappingURL=event-completed.module.js.map

/***/ }),

/***/ 890:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PickEmojiPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pick_emoji__ = __webpack_require__(483);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PickEmojiPageModule = (function () {
    function PickEmojiPageModule() {
    }
    return PickEmojiPageModule;
}());
PickEmojiPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__pick_emoji__["a" /* PickEmojiPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pick_emoji__["a" /* PickEmojiPage */]),
        ],
    })
], PickEmojiPageModule);

//# sourceMappingURL=pick-emoji.module.js.map

/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkthroughPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__walkthrough__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var WalkthroughPageModule = (function () {
    function WalkthroughPageModule() {
    }
    return WalkthroughPageModule;
}());
WalkthroughPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__walkthrough__["a" /* WalkthroughPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__walkthrough__["a" /* WalkthroughPage */]), __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */]
        ],
    })
], WalkthroughPageModule);

//# sourceMappingURL=walkthrough.module.js.map

/***/ }),

/***/ 892:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__checkout_details__ = __webpack_require__(366);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CheckoutDetailsPageModule = (function () {
    function CheckoutDetailsPageModule() {
    }
    return CheckoutDetailsPageModule;
}());
CheckoutDetailsPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__checkout_details__["a" /* CheckoutDetailsPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__checkout_details__["a" /* CheckoutDetailsPage */]),
        ],
    })
], CheckoutDetailsPageModule);

//# sourceMappingURL=checkout-details.module.js.map

/***/ }),

/***/ 893:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarriageApplicationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__marriage_application__ = __webpack_require__(363);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MarriageApplicationPageModule = (function () {
    function MarriageApplicationPageModule() {
    }
    return MarriageApplicationPageModule;
}());
MarriageApplicationPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__marriage_application__["a" /* MarriageApplicationPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__marriage_application__["a" /* MarriageApplicationPage */]),
        ],
    })
], MarriageApplicationPageModule);

//# sourceMappingURL=marriage-application.module.js.map

/***/ }),

/***/ 894:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MarriageEducationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__marriage_education__ = __webpack_require__(364);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MarriageEducationPageModule = (function () {
    function MarriageEducationPageModule() {
    }
    return MarriageEducationPageModule;
}());
MarriageEducationPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__marriage_education__["a" /* MarriageEducationPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__marriage_education__["a" /* MarriageEducationPage */]),
        ],
    })
], MarriageEducationPageModule);

//# sourceMappingURL=marriage-education.module.js.map

/***/ })

},[488]);
//# sourceMappingURL=main.js.map